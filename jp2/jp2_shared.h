/*****************************************************************************/
// File: jp2.h [scope = APPS/JP2]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Defines classes and other elements which are shared by the implementations
of multiple JP2-family file formats, including JP2, JPX, JPM and MJ2.
These definitions could be promoted to a very public level, but there
is little point in doing so.  For the moment then, they are provided here
in a semi-local header file.
******************************************************************************/

/******************************************************************************
Notes on Memory Management
   This file defines `j2_memsafe', the analog of `kd_coremem' which is private
to the core codestream management machinery.  As `kd_coremem' manages most
memory allocation/deallocation processes in the Kakadu core system,
`j2_memsafe' manages most memory allocation at the file-format level -- in
particular for the parsing and generation of JP2-family files.
   Both `j2_memsafe' and `kd_coremem' are private to the Kakadu toolkit, not
meant to be shared directly with applications, but instances of both classes
can be attached to a common `kdu_membroker' hierarchy, which is public and
derivable.
   The design patterns for classes/structs that are allocated via `j2_memsafe'
instances are the same as those for the core classes/structs that are
allocated via `kd_coremem', as outlined at the start of "compressed_local.h".
In particular, we use placement new/delete overrides for single instantiation,
while objects that are instantiated in arrays offer `create_n' and `destroy_n'
member functions.  Objects that allocate memory themselves keep a
`j2_memsafe' member as a reference, and this is retrieved by a `destroy'
function that directly instantiates the destructor and releases the memory,
rather than overriding the non-placement delete operator.  Objects that
do not need to allocate memory themselves have the `j2_memsafe' reference
passed to their `destroy' function.  In this way, a global plain text
search of the source files should reveal that there are almost no cases
in which operator delete is invoked or in which a naked (non-placement) new
operator is used.
   Managing all memory through a well defined interface has a number of
advantages, but the most important one is that it allows the application to
strictly limit the amount of memory that can be allocated for certain tasks.
This can prevent a larger system from being halted by a malicious file or
an ill-conceived employment of the enormous flexibility offered by the
JPEG2000 standards.
******************************************************************************/

#ifndef JP2_SHARED_H
#define JP2_SHARED_H

#include <stdio.h> // The C I/O functions can be a lot faster than C++ ones.
#include <assert.h>
#include "jp2.h"

// Classes defined here
namespace kd_supp_local { 
  class j2_memsafe;

  class j2_dimensions;
  class j2_palette;
  class j2_component_map;
  class j2_channels;
  class j2_resolution;
  class j2_colour;
  class jp2_header;
  class j2_data_references;
}

// Classes defined elsewhere
namespace kd_supp_local { 
  class j2_icc_profile;
  class j2_header;
  class jx_registration;
}

namespace kd_supp_local { 
  using namespace kdu_supp;

/* Note Carefully:
      If you want to be able to use the "kdu_text_extractor" tool to
   extract text from calls to `kdu_error' and `kdu_warning' so that it
   can be separately registered (possibly in a variety of different
   languages), you should carefully preserve the form of the definitions
   below, starting from #ifdef KDU_CUSTOM_TEXT and extending to the
   definitions of KDU_WARNING_DEV and KDU_ERROR_DEV.  All of these
   definitions are expected by the current, reasonably inflexible
   implementation of "kdu_text_extractor".
      The only things you should change when these definitions are ported to
   different source files are the strings found inside the `kdu_error'
   and `kdu_warning' constructors.  These strings may be arbitrarily
   defined, as far as "kdu_text_extractor" is concerned, except that they
   must not occupy more than one line of text.
      When defining these macros in header files, be sure to undefine
   them at the end of the header.
*/
#ifdef KDU_CUSTOM_TEXT
#  define KDU_ERROR(_name,_id) \
     kdu_error _name("E(jp2_shared.h)",_id);
#  define KDU_WARNING(_name,_id) \
     kdu_warning _name("W(jp2_shared.h)",_id);
#  define KDU_TXT(_string) "<#>" // Special replacement pattern
#else // !KDU_CUSTOM_TEXT
#  define KDU_ERROR(_name,_id) \
     kdu_error _name("Error in Kakadu File Format Support:\n");
#  define KDU_WARNING(_name,_id) \
     kdu_warning _name("Warning in Kakadu File Format Support:\n");
#  define KDU_TXT(_string) _string
#endif // !KDU_CUSTOM_TEXT

#define KDU_ERROR_DEV(_name,_id) KDU_ERROR(_name,_id)
 // Use the above version for errors which are of interest only to developers
#define KDU_WARNING_DEV(_name,_id) KDU_WARNING(_name,_id)
 // Use the above version for warnings which are of interest only to developers


/* ========================================================================= */
/*                                 Classes                                   */
/* ========================================================================= */

/*****************************************************************************/
/*                                j2_memsafe                                 */
/*****************************************************************************/

class j2_memsafe { 
  /* Notes:
       This is a private allocator, shared between various JP2-family source
       readers and writers.  It is not thread safe, and does not currently
       need to be, but it obtains any required allocation permissions from the
       thread safe `kdu_membroker' interface, which is public and
       inheritable.
  */
  public: // Management functions
    j2_memsafe(const char *phrase)
      { /* The `phrase' string is a constant resource that is inserted into
           error/warning messages that describe memory allocation problems
           associated with JP2-family file.  Recommended strings to use in
           the relevant context are:
           1. "JP2 file"; 2. "JP2/JPX file"; 3. "MJ2 file".
        */
        broker = NULL; limit_bytes = (kdu_uint64)KDU_INT64_MAX;
        alloc_bytes = 0;  free_failures = 0;  msg_phrase = phrase;
      }
    ~j2_memsafe(); // Implemented in "jp2.cpp" -- may generate a warning. */
    kdu_int64 attach_to_broker(kdu_membroker *new_broker);
      /* Analogous to `kdu_membroker::attach_to_parent'.  Returns 0 if
         successful, or else the number of bytes of already allocated
         content for which permission could not be granted by `new_broker';
         in this event, no attachment actually takes place and any memory
         allocation permission granted to us by `new_broker' is returned.
         If `new_broker' is NULL, the function is equivalent to
         `detach_from_broker'. */
    bool detach_from_broker();
      /* Similar to `kdu_membroker::detach_from_parent', except that once
         detached, the current object is left free from any allocation
         constraints, rather than being prevented from allocating any further
         memory.  Does nothing if not currently attached. */
    kdu_int64 get_allocated_bytes() const { return alloc_bytes; }
      /* Analogous to `kdu_membroker::get_allocated_bytes'. */
  public: // Overflow-safe arithmetic for use in preparing for alloc calls
    size_t safe_add(size_t x, size_t y)
      { /* Use before allocating memory based on a sum that might overflow */
        size_t result = x+y;
        if (result < y) handle_failed_alloc(0);
        return result;
      }
    size_t safe_mul(size_t x, size_t y)
      { /* Use before allocating memory based on a product that might
           overflow.  If one multiplicand is a constant, try to make this y. */
        if (((x | y) > KDU_SAFE_ROOT_SIZE_MAX) && (y != 0) &&
            (x > (KDU_SIZE_MAX / y)))
          handle_failed_alloc(0);
        return x*y;
      }
    size_t safe_mulo(size_t x, size_t y, size_t off)
      { /* As above, but for a product, followed by an offset. */
        return safe_add(safe_mul(x,y),off);
      }
  public: // Inline alloc functions
    void *alloc(size_t elt_bytes, size_t alignment, size_t num_elts=1)
      { /* Currently supports `alignment' values up to 8 and refuses to
           allocate more 2GB in a single request -- the latter provides
           security against the risk of overflow during index calculations
           that might be performed using signed 32-bit arithmetic.  The
           constraint can be removed if we are sure that all such calculations
           are performed using `kdu_idx_t' rather than `int' or `kdu_int32',
           but there is no real harm in preventing ridiculously large
           allocations here, where all memory is for file-format structures,
           as opposed to quantities that may scale with the area of an
           image. */
        if (num_elts != 1)
          { // Allocating an array -- need special overflow check
            if (num_elts > (KDU_SIZE_MAX/elt_bytes)) // RHS usually a constant
              handle_failed_alloc(0);
            elt_bytes *= num_elts;
          }
        kdu_uint64 old_alloc_bytes = this->alloc_bytes;
        if (alignment > 8) abort();
        void *result=NULL;
        if ((alignment == 1) && (elt_bytes < 256))
          { // Record size in 1-byte prefix
            kdu_uint64 new_bytes = ((kdu_uint64)elt_bytes) + 1;
            if ((new_bytes < 1) || (new_bytes > (kdu_uint64)KDU_INT64_MAX))
              handle_failed_alloc(0);
            else if (((alloc_bytes+=new_bytes) > limit_bytes) ||
                     (alloc_bytes < old_alloc_bytes))
              handle_overlimit_alloc(new_bytes);
            if ((new_bytes > (size_t)KDU_INT32_MAX) || // 2GB limit; see above
                ((result = malloc((size_t)new_bytes)) == NULL))
              handle_failed_alloc(new_bytes);
            assert(!(_addr_to_kdu_int32(result) & 7));
            result = ((kdu_byte *)result) + 1;
            ((kdu_byte *)result)[-1] = (kdu_byte)elt_bytes;
          }
        else if ((alignment <= 4) && (elt_bytes < (size_t)0xFFFFFFFF))
          { // Record size in 4-byte prefix
            kdu_uint64 new_bytes = ((kdu_uint64)elt_bytes) + 4;
             if ((new_bytes < 4) || (new_bytes > (kdu_uint64)KDU_INT64_MAX))
              handle_failed_alloc(0);
            else if (((alloc_bytes+=new_bytes) > limit_bytes) ||
                     (alloc_bytes < old_alloc_bytes))
              handle_overlimit_alloc(new_bytes);
            if ((new_bytes > (size_t)KDU_INT32_MAX) || // 2GB limit; see above
                ((result = malloc((size_t)new_bytes)) == NULL))
              handle_failed_alloc(new_bytes);
            assert(!(_addr_to_kdu_int32(result) & 7));
            result = ((kdu_byte *)result) + 4;
            ((kdu_uint32 *)result)[-1] = (kdu_uint32)elt_bytes;
          }
        else
          { // Record size in 8-byte prefix
            kdu_uint64 new_bytes = ((kdu_uint64)elt_bytes) + 8;
             if ((new_bytes < 8) || (new_bytes > (kdu_uint64)KDU_INT64_MAX))
              handle_failed_alloc(0);
            else if (((alloc_bytes+=new_bytes) > limit_bytes) ||
                     (alloc_bytes < old_alloc_bytes))
              handle_overlimit_alloc(new_bytes);
            if ((new_bytes > (size_t)KDU_INT32_MAX) || // 2GB limit; see above
                ((result = malloc((size_t)new_bytes)) == NULL))
              handle_failed_alloc(new_bytes);
            assert(!(_addr_to_kdu_int32(result) & 7));
            result = ((kdu_byte *)result) + 8;
            ((size_t *)result)[-1] = elt_bytes;
          }
        return result;
      }
    kdu_byte *alloc_uint8(size_t num_bytes)
      { return (kdu_byte *)alloc(num_bytes,1); }
    kdu_int16 *alloc_int16(size_t num_shorts)
      { 
        if (num_shorts > (KDU_SIZE_MAX/sizeof(kdu_int16)))
          handle_failed_alloc(0);
        return (kdu_int16 *)alloc(num_shorts*sizeof(kdu_int16),
                                  sizeof(kdu_int16));
      }
    kdu_uint16 *alloc_uint16(size_t num_shorts)
      { 
        if (num_shorts > (KDU_SIZE_MAX/sizeof(kdu_uint16)))
          handle_failed_alloc(0);
        return (kdu_uint16 *)alloc(num_shorts*sizeof(kdu_uint16),
                                  sizeof(kdu_uint16));
      }
    kdu_int32 *alloc_int32(size_t num_ints)
      { 
        if (num_ints > (KDU_SIZE_MAX/sizeof(kdu_int32)))
          handle_failed_alloc(0);
        return (kdu_int32 *)alloc(num_ints*sizeof(kdu_int32),
                                  sizeof(kdu_int32));
      }
    kdu_uint32 *alloc_uint32(size_t num_ints)
      { 
        if (num_ints > (KDU_SIZE_MAX/sizeof(kdu_uint32)))
          handle_failed_alloc(0);
        return (kdu_uint32 *)alloc(num_ints*sizeof(kdu_uint32),
                                  sizeof(kdu_uint32));
      }
    float *alloc_float(size_t num_floats)
      { 
        if (num_floats > (((size_t)KDU_INT64_MAX)/sizeof(float)))
          handle_failed_alloc(0);
        return (float *)alloc(num_floats*sizeof(float),sizeof(float));
      }
    char *alloc_string(size_t length, const char *copy_src=NULL)
      { /* NB: `length' is the length of the string, so one extra character is
         allocated.  For convenience, if `copy_src' is non-NULL, we also
         copy up to a maximum of `length' characters to the allocated string
         before returning.  The returned character array is always
         null-terminated. */
        if (length >= (KDU_SIZE_MAX/sizeof(char)))
          handle_failed_alloc(0);
        char *buf = (char *)alloc((length+1)*sizeof(char),sizeof(char));
        buf[0] = '\0';
        if (copy_src != NULL)
          { strncpy(buf,copy_src,length); buf[length]='\0'; }
        return buf;
      }
    char **calloc_stringptr(size_t num_ptrs)
      { /* This function initializes all pointers to NULL. */
        if (num_ptrs > (KDU_SIZE_MAX/sizeof(char *)))
          handle_failed_alloc(0);
        char **buf = (char **)alloc(num_ptrs*sizeof(char *),sizeof(char *));
        memset(buf,0,num_ptrs*sizeof(char *));
        return buf;
      }
    kdu_int32 **calloc_int32ptr(size_t num_ptrs)
      { /* This function initializes all pointers to NULL. */
        if (num_ptrs > (KDU_SIZE_MAX/sizeof(kdu_int32 *)))
          handle_failed_alloc(0);
        kdu_int32 **buf = (kdu_int32 **)alloc(num_ptrs*sizeof(kdu_int32 *),
                                              sizeof(kdu_int32 *));
        memset(buf,0,num_ptrs*sizeof(kdu_int32 *));
        return buf;
      }
    void *calloc_structs(size_t elt_size, size_t num_elts)
      { /* This function allocates and clears an array of structures (objects
           that do not have constructors that do anything other than clear
           all members) and do not have destructors.  Alignment is always 8
           bytes. */
        void *buf = alloc(elt_size,8,num_elts);
        memset(buf,0,elt_size*num_elts);
        return buf;
      }
  public: // Functions to free or query memory blocks
    void free(void *ptr)
      { 
        kdu_int32 align_probe = _addr_to_kdu_int32(ptr) & 7;
        kdu_uint64 num_bytes=0, hdr_bytes=0;
        if (align_probe == 1)
          { // Size is recorded in 1 byte
            hdr_bytes = 1;
            num_bytes = (kdu_uint64) ((kdu_byte *)ptr)[-1];
            ::free(((kdu_byte *)ptr) - 1);
          }
        else if (align_probe == 4)
          { // Size is recorded in 4 bytes
            hdr_bytes = 4;
            num_bytes = (kdu_uint64) ((kdu_uint32 *)ptr)[-1];
            ::free(((kdu_byte *)ptr) - 4);
          }
        else if (align_probe == 0)
          { // Size is recorded in 8 bytes
            hdr_bytes = 8;
            num_bytes = (kdu_uint64) ((size_t *)ptr)[-1];
            ::free(((kdu_byte *)ptr) - 8);
          }
        else
          handle_failed_free(ptr);
        num_bytes += hdr_bytes;
        if ((num_bytes < hdr_bytes) || (alloc_bytes < num_bytes))
          handle_failed_free(ptr);
        alloc_bytes -= num_bytes;
      }
    size_t get_num_elts(void *ptr, size_t elt_bytes)
      { 
        kdu_int32 align_probe = _addr_to_kdu_int32(ptr) & 7;
        size_t num_bytes = 0;
        if (align_probe == 1)
          num_bytes = (size_t) ((kdu_byte *)ptr)[-1];
        else if (align_probe == 4)
          num_bytes = (size_t) ((kdu_uint32 *)ptr)[-1];
        else if (align_probe == 0)
          num_bytes = (size_t) ((size_t *)ptr)[-1];
        size_t num_elts = num_bytes / elt_bytes;
        if (num_bytes != (num_elts * elt_bytes))
          handle_failed_free(ptr);
        return num_elts;
      }
  private: // Helper functions
    void handle_overlimit_alloc(kdu_uint64 new_bytes);
      /* Called if the `alloc_bytes' member has been incremented to a value
         exceeding `limit_bytes'.  If the problem cannot be corrected
         by a successful call to `responder->overlimit', the function generates
         an error, never returning, but first removes `new_bytes' from the
         tally found in `alloc_bytes'. */
    void handle_failed_alloc(kdu_uint64 new_bytes);
      /* Similar to `handle_overlimit_request', but called in the event that
         a memory allocation request fails; in this case, the function always
         removes `new_bytes' from `alloc_bytes' before returning.  If the
         function is called with `new_bytes'=0, the allocation failed
         immediately due to invalid arguments -- e.g., overflow in numerical
         calculation of the number of bytes required. */
    void handle_failed_free(void *ptr);
      /* Called if it appears that more memory is being released than was
         actually allocated, or if there is an alignment problem with the
         memory block being released.  This function never returns. */
  private: // Data
    kdu_membroker *broker;
    kdu_uint64 limit_bytes; // Max bytes allowed; won't exceed KDU_INT64_MAX
    kdu_uint64 alloc_bytes; // Currently use non-interlocked updates
    kdu_int64 free_failures; // Avoids excessive warning/error messages
    const char *msg_phrase;
  };

/*****************************************************************************/
/*                               j2_dimensions                               */
/*****************************************************************************/

class j2_dimensions { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_dimensions,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_dimensions(j2_memsafe *msafe)
      { 
        this->memsafe = msafe;
        compression_type=-1; num_components=0; bit_depths=NULL;
        colour_space_unknown = true; ipr_box_available = false;
        compatibility_known = false;
        jp2orh_brand = 0; profile=Sprofile_PART1; scap_val=0;
        part2_extensions=0; part2_mct_attributes=0; part2_caps=0;
      }
    ~j2_dimensions()
      { 
        if (bit_depths != NULL)
          memsafe->free(bit_depths);
      }
    void destroy()
      { /* MUST use this instead of `delete'!! */
        j2_memsafe *msafe = this->memsafe;
        this->~j2_dimensions();
        msafe->free(this);
      }
    bool is_initialized() { return (compression_type >= 0); }
      /* Returns true if the box has been initialized, either by
         the `j2_dimensions::init' or by `jp2_dimensions::init'. */
    bool compare(j2_dimensions *src);
      /* Returns true if the object's contents are identical to those of
         the `src' object. */
    void copy(j2_dimensions *src);
      /* Copies the contents of `src' to the present object, which must not
         have been initialized yet. */
    void init(jp2_input_box *ihrd_box, kdu_uint32 jp2orh_compatible_brand);
      /* Initializes the object from the information recorded in an image
         header box, supplying information about whether the file's
         compatible brand information identifies known compatibility
         with the JP2 or JPH file formats.  The `jp2orh_compatible_brand'
         argument should be 0 if full compatibility with JP2 or JPH is not
         indicated in the file compatibility list; otherwise it should be
         one of `jp2_brand' or `jph_brand'.  Note that the construction might
         not be complete (`finalize' may generate an error) if components have
         different bit-depths or signed/unsigned characteristics (see below).
         Note also that the function closes the `ihdr_box' when done. */
    void process_bpcc_box(jp2_input_box *bpcc_box);
      /* This function is called if a bits per component box is encountered
         while parsing a JP2/JPX file.  The function closes the box before
         returning.  It is legal to call this function at any time and any
         number of times -- this can be useful when parsing JPX codestream
         header boxes. */
    void finalize();
      /* Checks that the object has been completely initialized.  Generates
         an appropriate error otherwise. */
    void save_boxes(jp2_output_box *super_box);
      /* Creates an image header box and, if necessary, a bits per component
         box, saving them as sub-boxes of the supplied `super_box'. */
    int get_compression_type(int &profile, int &scap_val) const
      { profile = this->profile;  scap_val = this->scap_val;
        return compression_type; }
      /* Used to complete compatibility information for JP2 and JPX files.
         If the `profile' information is not yet known
         (`jp2_dimensions::finalize_compatibility' has not yet been
         successfully called), the returned `profile' will be 0, as will
         the `scap_val'.   The `scap_val' is the value of `Scap' which
         consists of flags identifying extensions belonging to different
         parts of the JPEG 2000 family of standards -- these can be tested
         using macros like `Scap_P2', `Scap_P15' and `Scap_P17'.  The
         value of `scap_val' should be 0 for Part-1 codestreams. */
    bool get_part2_info(int &extensions, int &mct_attributes,
                        int &ccap2) const
      { /* If the codestream conforms to Part-2, the function returns true,
           setting `extensions', `mct_attributes' and `ccap2' to values that
           quality the type of Part-2 codestream that we have, as explained
           with `jp2_dimensions::finalize_compatibility' (second form).  Here
           `ccap2' is the Ccap2 field of any CAP marker segment that
           contains extended Part-2 capabilities.  If the codestream does
           not conform to Part-2 or there is (or would be) no Ccap2 field in
           a CAP marker segment, the function returns false after setting
           all arguments to 0. */
        if (scap_val & Scap_P2)
          { 
            extensions = part2_extensions;
            mct_attributes = part2_mct_attributes;
            ccap2 = part2_caps;
            return true;
          }
        else
          { 
            extensions = mct_attributes = ccap2 = 0;
            return false;
          }
      }
    bool get_jpxb_compatible() const
      { /* Used to complete compatibility information for JPX files.  If
           no compatibility information is known at all, this function will
           return true. */
        return ((part2_extensions & ~(Sextensions_MCT | Sextensions_NLT)) ||
                ((part2_mct_attributes != 0) &&
                 (part2_extensions & Sextensions_MCT)) ||
                (scap_val & ~Scap_P2) || (part2_caps > 0) ||
                (jp2orh_brand == jph_brand))?false:true;
      }
  private: // Data
    friend class kdu_supp::jp2_dimensions;
    j2_memsafe *memsafe;
    kdu_coords size;
    int compression_type;
    int num_components;
    kdu_int32 *bit_depths;
    bool colour_space_unknown;
    bool ipr_box_available;
  private: // Members that keep track of compatibility information; this info
           // is not directly embedded within the Image Header box.
    bool compatibility_known; // If any fields here have been set to known vals
    kdu_uint32 jp2orh_brand; // 0, `jp2_brand' or `jph_brand'
    int profile;
    int scap_val; // Contains the part flags: `Scap_P2', `Scap_P15', etc.
    int part2_extensions;
    int part2_mct_attributes;
    int part2_caps;
  };
  /* Notes:
        The `bit_depths' array holds one element for each image component
     with the magnitude identifying the actual number of bits used to
     represent the samples.  Negative values mean that the relevant
     component has a signed representation. */

/*****************************************************************************/
/*                                 j2_palette                                */
/*****************************************************************************/

class j2_palette { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_palette,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_palette(j2_memsafe *msafe)
      { 
        initialized = false; num_components = 0; num_entries = 0;
        bit_depths = NULL; luts = NULL;
        this->memsafe = msafe;
      }
    ~j2_palette()
      { 
        if (bit_depths != NULL)
          { memsafe->free(bit_depths); bit_depths = NULL; }
        if (luts != NULL)
          { 
            for (int c=0; c < num_components; c++)
              memsafe->free(luts[c]);
            memsafe->free(luts);
          }
      }
    void destroy()
      { /* MUST use this instead of `delete'!! */
        j2_memsafe *msafe = this->memsafe;
        this->~j2_palette();
        msafe->free(this);
      }
    bool is_initialized() { return initialized; }
      /* Returns true if `init' or `jp2_palette::init' has been used. */
    bool compare(j2_palette *src);
      /* Returns true if the object's contents are identical to those of
         the `src' object. */
    void copy(j2_palette *src);
      /* Copies the contents of `src' to the present object, which must not
         have been initialized yet. */
    void init(jp2_input_box *pclr_box);
      /* Initializes the object from the information recorded in a palette
         box.  Note that the function closes the `pclr_box' when done. */
    void finalize();
      /* Checks that the object has been completely initialized.  Generates
         an appropriate error otherwise. */
    void save_box(jp2_output_box *super_box);
      /* Creates a palette box and saves it as a sub-box of the supplied
         `super-box'.  Does nothing if no palette information has been set. */
  private: // Data
    friend class kdu_supp::jp2_palette;
    j2_memsafe *memsafe;
    bool initialized;
    int num_components;
    int num_entries;
    kdu_int32 *bit_depths; // Magnitude = bit-depth; -ve values mean signed
    kdu_int32 **luts; // One LUT array for each component.
  };
  /* Notes:
        The values stored in the LUT's all have a signed representation and
     the binary values are all located in the most significant bit positions
     of the 32-bit signed words.
        The `bit_depths' entries should never specify bit-depths in excess of
     32 bits.  If the original values have a larger bit-depth, some of the
     least significant bits will be discarded to make them fit into the 32-bit
     word size. */

/*****************************************************************************/
/*                              j2_component_map                             */
/*****************************************************************************/

class j2_component_map { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_component_map,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_component_map(j2_memsafe *msafe)
      { 
        max_cmap_channels = num_cmap_channels = 0; cmap_channels=NULL;
        use_cmap_box = false;
        this->memsafe = msafe;
      }
    ~j2_component_map()
      { 
        if (cmap_channels != NULL)
          { cmap_channels->destroy_n(memsafe); cmap_channels = NULL; }
      }
    void destroy()
      { /* MUST use this instead of `delete'!! */
        j2_memsafe *msafe = this->memsafe;
        this->~j2_component_map();
        msafe->free(this);
      }
    bool is_initialized() { return (num_cmap_channels > 0); }
      /* Returns true if the object has been initialized either by the
         `init' function (which reads a cmap box) or by at least one
         call to `add_cmap_channel' (used when writing a JP2/JPX/MJ2 file). */
    bool compare(j2_component_map *src);
      /* Returns true if the object's contents are identical to those of
         the `src' object. */
    void copy(j2_component_map *src);
      /* Copies the contents of `src' to the present object, which must not
         have been initialized yet. */
    void init(jp2_input_box *cmap_box);
      /* Initializes the object from the information recorded in a "Component
         Mapping" box.  The function closes the `cmap_box' when done.  You
         should not call `add_cmap_channel' to add any further cmap-channel
         definitions to the object. */
    void save_box(jp2_output_box *super_box, bool force_generation=false);
      /* Creates a "Component Mapping" box, saving it as a sub-box of the
         supplied `super_box'.  If there is no palette box, the function
         does not write any component mapping box unless `force_generation'
         is true.  The `force_generation' flag must be set to true when
         writing a codestream header box which contains no palette box of
         its own, if the default JP2 header box contains a palette box and
         its component mapping box differs from the present one.  These
         conventions seem to be the only way to write a JPX file which
         conforms to the standard. */
    int add_cmap_channel(int component_idx, int lut_idx=-1);
      /* You don't call this function directly, but is is used by
         `j2_channels::add_cmap_channels' to create cmap-channels as
         required to implement the channel bindings associated with a
         `j2_channels' object.  The function searches through the existing
         set of cmap-channels, for one which uses the indicated
         component and palette LUT indices.  If none is found, a new
         cmap-channel is appended to the internal list.  In any event,
         the function returns the index of the cmap-channel. */
    int get_num_cmap_channels() { return num_cmap_channels; }
      /* Returns the number of cmap-channels described by this object.  For
         reliable results, do not call this function until after `finalize'. */
    int get_cmap_component(int cmap_channel)
      { verify_channel_query_idx(cmap_channel);
        return cmap_channels[cmap_channel].component_idx; }
    int get_cmap_lut(int cmap_channel)
      { verify_channel_query_idx(cmap_channel);
        return cmap_channels[cmap_channel].lut_idx; }
    int get_cmap_bit_depth(int cmap_channel)
      { verify_channel_query_idx(cmap_channel);
        return cmap_channels[cmap_channel].bit_depth; }
    bool get_cmap_signed(int cmap_channel)
      { verify_channel_query_idx(cmap_channel);
        return cmap_channels[cmap_channel].is_signed; }
      /* You should not need to call these two functions yourself; they are
         used by `j2_channels::find_cmap_channels' to complete the description
         of each colour channel when reading a JP2/JPX/MJ2 file.  The last
         two are also used by `j2_channels::add_cmap_channels' to recover
         bit-depth and signed/unsigned information.  None of the functions
         should be called until after `finalize' has been invoked. */
    void finalize(j2_dimensions *dimensions, j2_palette *palette);
      /* When reading a JP2/JPX file, this function should be called once
         all boxes associated with the JP2 header (JP2 files) or codestream
         header (JPX files) box have been parsed, passing in the
         `j2_dimensions' and `j2_palette' boxes which are to be used for
         the codestream in question.  When writing a JP2/JPX file, the
         function should be called immediately before any call to the
         `add_cmap_channel' function.  In practice, this means that it must
         be called before the present object can be passed to
         `j2_channels::add_cmap_channels'.
            The function extracts information about the bit-depth of each
         cmap-channel and checks for legal constructs.  The function checks
         and sets the state of the `use_cmap_box' flag based on whether or not
         there is a Palette box.  If a Component mapping box was read by
         `init', the `use_cmap_box' will have been set to true and the present
         function checks to see if a Palette box is indeed present; if not,
         the file is illegal and an appropriate error is generated through
         `kdu_error'.  For cases where a Component Mapping box was not read
         or will not be written, the present function fills in the default set
         of cmap-channel bindings, so that each image component is associated
         with a cmap-channel of the same index.
            The function leaves behind copies of the `dimensions' and
         `palette' pointers so that subsequent calls to `add_cmap_channel'
         during JP2/JPX file creation will be able to recover bit-depth
         information for each of the cmap-channels. */
  private: // Helper functions
    void verify_channel_query_idx(int idx)
      { 
        if ((idx < 0) || (idx >= num_cmap_channels))
          { KDU_ERROR(e,0); e <<
              KDU_TXT("Attempting to associate a reproduction "
              "function (e.g., colour intensity, opacity, etc.) with a "
              "non-existent image channel in a JP2-family file.  The problem "
              "may be a missing or invalid Component Mapping (cmap) box, or "
              "a corrupt or illegal Channel Definitions (cdef) box.");
          }
        assert(dimensions.exists()); // If not, you have forgotten to call
                                     // `finalize' first.
      }
  private: // Structure definitions
      struct j2_cmap_channel { 
        private: // Prevent explicit new/delete calls.
          j2_cmap_channel() {}
          ~j2_cmap_channel() {}
        public: // Array create/destroy funcs
          static j2_cmap_channel *create_n(j2_memsafe *msafe, size_t num_elts)
            { // No need to call constructors
              return (j2_cmap_channel *)
                msafe->alloc(sizeof(j2_cmap_channel),4,num_elts);
            }
          void destroy_n(j2_memsafe *msafe)
            { // No need to call destructors
              msafe->free(this);
            }
        public: // Data
          kdu_int32 component_idx;
          kdu_int32 lut_idx;
          kdu_int32 bit_depth; // Filled in by `finalize'
          bool is_signed; // Filled in by `finalize'
        };
  private: // Data
    j2_memsafe *memsafe;
    bool use_cmap_box; // True if a cmap box was parsed, or is to be written
    jp2_dimensions dimensions; // Filled in by `finalize'
    jp2_palette palette; // Filled in by `finalize'
    int max_cmap_channels; // Num elements in the `cmap_channels' array
    int num_cmap_channels; // Num valid elements in the `cmap_channels' array
    j2_cmap_channel *cmap_channels;
  };
  /* Notes:
       This object serves to describe what we term "cmap-channels".  The
     purpose of "cmap-channels" is to augment the collection of image
     components offered by a code-stream with additional components which
     are generated by means of palette lookup tables.  Each cmap-channel
     is described by the `j2_cmap_channel' structure, in terms of a
     component index and an lut_idx.  If the component is mapped directly
     to the channel, the `lut_idx' member should be negative.  Otherwise
     `lut_idx' represents the index (starting from 0) of the lookup table
     found within a corresponding `j2_palette' object.
       In JPX files, both the default JP2 header and each codestream
     header box may contain a "Component Mapping" (cmap) box.  The
     cmap-channels described here are used by the `j2_channels' object
     to build a relationship between the colour channels within each
     compositing layer and the image components and palette lookup tables
     used to create the samples for those colour channels.  Whereas
     each `j2_component_map' object is associated with exactly one
     code-stream, the `j2_channels' object may use cmap-channels from
     multiple code-streams.  To create the appropriate associations while
     reading or creating a JPX file, the relevant `j2_cmap_channel'
     objects must be supplied to `j2_channels::associate_cmap' in exactly
     the same order as the corresponding code-streams appear within the
     "Codestream Registration" box.
  */

/*****************************************************************************/
/*                                j2_channels                                */
/*****************************************************************************/

class j2_channels { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_channels,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_channels(j2_memsafe *msafe)
      { 
        max_entries = num_entries = num_colour_entries = 0;  channels=NULL;
        need_pxfm_fixpoint=need_pxfm_float=need_pxfm_split_exp=false;
        num_pxfm_descriptors=0;  pxfm_descriptors=NULL;
        num_cdef_triplets=0;  cdef_triplets=NULL;
        have_chroma_key = opct_opacity = opct_premult = false;
        chroma_key_buf = NULL;  chroma_key_len = 0;
        resolved_cmap_channels=0;
        this->memsafe = msafe;
      }
    ~j2_channels()
      { 
        if (channels != NULL)
          { channels->destroy_n(memsafe); channels = NULL; }
        if (pxfm_descriptors != NULL)
          { memsafe->free(pxfm_descriptors); pxfm_descriptors = NULL; }
        if (cdef_triplets != NULL)
          { memsafe->free(cdef_triplets); cdef_triplets = NULL; }
        if (chroma_key_buf != NULL)
          { memsafe->free(chroma_key_buf); chroma_key_buf = NULL; }
      }
    void destroy()
      { /* MUST use this instead of `delete'!! */
        j2_memsafe *msafe = this->memsafe;
        this->~j2_channels();
        msafe->free(this);
      }
    bool is_initialized() const
      { /* Returns true if a CDEF, OPCT or PXFM box has been parsed, or if
           the `channels' arrays is non-NULL.  The `finalized' function need
           not have been called.  This is most useful for recovering default
           parsed information from an image header box and applying it to
           individual compositing layers that do not have their own channel
           binding information. */
        return ((num_pxfm_descriptors > 0) || (num_cdef_triplets > 0) ||
                have_chroma_key || opct_opacity || opct_premult ||
                (num_entries > 0));
      }
    bool is_fully_parsed(bool pxfm_allowed) const
      { /* Returns true if all boxes that could possibly be parsed via
           `parse_cdef', `parse_opct' and `parse_pxfm' in a given context
           (compositing layer header or image header box) have already been
           parsed.  In practice, this requires either a cdef or opct box to
           have been parsed and either `pxfm_allowed' to be false or a pxfm
           box to have been parsed. */
        return (((num_pxfm_descriptors > 0) || !pxfm_allowed) &&
                ((num_cdef_triplets > 0) || have_chroma_key ||
                 opct_opacity || opct_premult));
      }
    bool needs_opacity_box() const { return have_chroma_key; }
      /* Returns true if the channel bindings will need to be stored using
         an opacity (opct) box, regardless of whether the `save_boxes' is
         called with its `avoid_opct_if_possible' argument equal to true or
         false.  This test forms an important part of the test to see if
         a JPX file will be JP2 compatible. */
    bool needs_pixel_format_fixpoint() const { return need_pxfm_fixpoint; }
    bool needs_pixel_format_float() const { return need_pxfm_float; }
    bool needs_pixel_format_split_exp() const { return need_pxfm_split_exp; }
      /* These return true if the channel binding information involves
         non-default data formats, in which case a JPX pixel format (pxfm) box
         must be written to capture this information.   This function is called
         to determine whether or not a written file will be JP2 compatible. */
    bool uses_palette_colour() const;
      /* Returns true if any of the colour channels uses a palette
         lookup table.  Ignores the opacity information. */
    bool has_opacity() const;
      /* Returns true if any of the channels has associated opacity
         information (not premultiplied opacity). */
    bool has_premultiplied_opacity() const;
      /* Returns true if any of the channels has associated pre-multiplied
         opacity information. */
    bool has_complex_opacity() const;
      /* Returns true if different opacity or pre-multiplied opacity
         definitions apply to different colour channels.  This is not
         compatible with the JPH file format, which requires at most one global
         opacity or pre-multiplied opacity definition. */
    bool has_non_colour_channels() const
      { return (num_entries > num_colour_entries); }
    bool compare(j2_channels *src);
      /* Returns true if the box which would be written using `save_boxes' is
         identical to that which would be written using `src->save_boxes'.
         The objects being compared should have been finalized and have had
         `add_cmap_channels' invoked upon them.  To avoid all kinds of
         ambiguous implications, if either the current or `src' object
         requires a pixel format box, the function returns false. */
    void copy(j2_channels *src, const int *stream_map=NULL,
              int stream_map_elts=0, int stream_base=0);
      /* Copies the contents of `src' to the present object, which must not
         have been initialized yet.  The remaining arguments are provided to
         support transcription of codestream indices, as explained with
         `jp2_channels::copy'. */
    int get_codestreams_used(int min_idx, int *indices, int max_indices) const;
      /* May be called multiple times from `jp2_channels::get_codestreams_used'
         if the `stream_indices' array passed to that function is too small.
         This function writes at most `max_indices' to the `indices' array,
         returning the number of indices written, which is thus necessarily
         no larger than `max_indices'.  Moreover, all codestream indices
         smaller than `min_idx' are not written to the `indices' array. */
    void parse_cdef(jp2_input_box *cdef_box);
      /* Initializes the object from the information recorded in the
         supplied "Channel Definitions" (cdef) box.  The function closes the
         box when done.  You should not call `finalize', however, until after
         any CDEF, OPCT or PXFM boxes have been parsed.  */
    void parse_opct(jp2_input_box *opct_box);
      /* Initializes the object from the information recorded in the
         supplied "Opacity" (opct) box.  The function closes the
         box when done.  You should not call `finalize', however, until after
         any CDEF, OPCT or PXFM boxes have been parsed.  */
    void parse_pxfm(jp2_input_box *pxfm_box);
      /* Initializes the object from the information recorded in the
         supplied "Pixel Format" (pxfm) box.  The function closes the
         box when done.  You should not call `finalize', however, until after
         any CDEF, OPCT or PXFM boxes have been parsed.  */
    void save_boxes(jp2_output_box *super_box, bool avoid_opct_if_possible);
      /* Stores the channel bindings represented by this object in a
         JP2 "Channel Definitions" (cdef) box or, if possible, within a
         JPX "Opacity" (opct) box, also writing a JPX "Pixel Format" (pxfm)
         box, if required.  If the channel mapping rules correspond
         to those defined as defaults, no box will be written at all.  It
         is important that the file format writer call `finalize' and
         then `add_cmap_channels' before invoking this function.
         [//]
         If `avoid_opct_if_possible' is true, the function will not attempt
         to store the relevant information in the more compact "Opacity" box.
         This flag should generally be asserted when writing JP2 files
         and when writing the first compositing layer box of a JPX file
         so as to maximize compatibility with JP2. */
    void add_cmap_channels(j2_component_map *cmap, int codestream_idx);
      /* Use this function to build and discover a complete set of
         associations between colour reproduction channels and cmap (component
         mapped) channels during file writing.  When writing a JPX file, the
         `j2_component_map' object associated with each successive code-stream
         used by the relevant compositing layer should be passed into this
         function.  The function should be invoked once for each codestream,
         in the same order that the code-streams will appear within the
         "Codestream Registration" box, if any.  These calls should be
         performed after the call to `finalize', but prior to `save_box'.
         Calls to `j2_component_map::finalize' should be delayed until after
         all calls to the present function. */
    void find_cmap_channels(j2_component_map *map, int codestream_idx,
                            bool last_call);
      /* Use this function to incorporate information from individual
         code-streams' "Component Mapping" boxes after reading all relevant
         boxes.  This function should be called after `j2_channels::finalize'
         and `j2_component_map::finalize'.
            The function should be called once for each codestream which is
         associated with the compositing layer to which this `j2_channels'
         object belongs, in the same order as those codestreams appear within
         a codestream registration box, if any, passing `last_call'=true only
         in the last such call.  For regular JP2 or MJ2 files, there will be
         only one call, and so `last_call' should always be true.  For JPX
         files, however, there can be many calls.  The `last_call' argument
         is used only to determine when the function should check for
         channel definitions that have no associated codestream components,
         reporting an error if such a condition is found.
            If the internal object is found still to have 0 colour channels
         when the function is called, the `finalize' function must have been
         invoked using a `num_colours' argument of 0, which can only happen
         if the only colour space definition is an inscrutinable
         vendor-specific colour space.  In this case, the present function
         will first invoke `finalize' with a number of colours which is equal
         to the number of components associated with the indicated codestream.
         This is the best guess we can make. */
    bool all_cmap_channels_found();
      /* Use this function to check that all explicitly or implicitly
         defined channels have been mapped to some codestream image
         component. */
    void finalize(int num_colours, bool writing);
      /* When reading a JP2/JPX file, the `writing' argument should be false,
         and this function should be called once the relevant boxes have been
         parsed, but before the `find_cmap_channels' function is called.  If
         the object has not already been initialized through the reading of a
         Channel Definitions (cdef) or Opacity (opct) box, it will
         automatically be initialized here to associate the first
         `num_colours' cmap-channels with the corresponding colour intensity
         channels.
            When writing a JP2/JPX file, this function should be called with
         `writing' equal to true, prior to the `add_cmap_channels' function,
         which in turn should be called prior to the `save_box' function.  It
         serves to verify that the number of colours is consistent with the
         value passed to `jp2_channels::init' and that all colour intensity
         channels have been associated with some image component of some
         codestream; if not, they are associated with the initial set of
         components of codestream 0. */
    int get_bit_depth(int c)
      { // Used by `j2_colour::finalize'
        assert(c < num_entries);
        return channels[c].precision[0];
      }
  private: // Structure definitions
      struct j2_channel { 
        private: // Prevent explicit new/delete calls.
          j2_channel()
            { 
              for (int i=0; i < 4; i++)
                { 
                  cmap_channel[i] = codestream_idx[i] =
                    component_idx[i] = lut_idx[i] = data_format[i] = -1;
                  all_channels[i] = false;
                  precision[i] = -1;  signed_val[i] = false;  pxfm_desc[i] = 0;
                }
              chroma_key = 0;
              base_typ = 0;
              non_colour_asoc = 0;
            }
          ~j2_channel() {}
        public: // Array create/destroy funcs
          static j2_channel *create_n(j2_memsafe *msafe, size_t num_elts)
            { 
              j2_channel *result = (j2_channel *)
                msafe->alloc(sizeof(j2_channel),4,num_elts);
              for (size_t n=0; n < num_elts; n++)
                new(result+n) j2_channel; // Each element needs construction
              return result;
            }
          void destroy_n(j2_memsafe *msafe)
            { // No need to call destructors
              msafe->free(this);
            }
        public: // Member functions
          void add_split_exponent_mapping(const int *params, bool global);
            /* Called each time the application supplies a split-exponent data
               format for any of the channel's 3 roles (intensity, opacity or
               premultiplied opacity).  This function installs the exponent
               mapping into the fourth entry of the various member arrays and
               also checks consistency with any previously defined
               split-exponent mapping. */
        public: // Data
          kdu_int32 cmap_channel[4]; // See below
          kdu_int32 codestream_idx[4];
          kdu_int32 component_idx[4];
          kdu_int32 lut_idx[4];
          kdu_int32 data_format[4]; // See below
          bool all_channels[4]; // If definition holds for all colour channels
          kdu_int32 precision[4]; // Populated using `j2_component_map' objects
          bool signed_val[4];     // and used to write chroma keys and to
                                  // interpret and generate pixel format codes.
          kdu_uint32 pxfm_desc[4]; // Used by `save_boxes' only
          kdu_int32 chroma_key; // Returned by `jp2_channels::get_chroma_key'
          kdu_uint16 base_typ; // 0 for regular colour channels
          kdu_uint16 non_colour_asoc; // CDEF box ASOC field if `base_typ' != 0
        };
  private: // Data members
    friend class kdu_supp::jp2_channels;
    friend class kd_supp_local::jx_registration;
    j2_memsafe *memsafe;
    int max_entries; // Maximum number of entries in the `channels' array
    int num_entries; // Actual number of valid entries in the `channels' array
    int num_colour_entries; // Initial entries that belong to the colour space
    j2_channel *channels;

    bool need_pxfm_fixpoint;
    bool need_pxfm_float;
    bool need_pxfm_split_exp;
    int num_pxfm_descriptors;
    kdu_uint32 *pxfm_descriptors; // See below

    int num_cdef_triplets;
    kdu_uint16 *cdef_triplets; // See below

    bool have_chroma_key; // If false, `j2_channel::chroma_key' is ignored
    bool opct_opacity; // If an "opct" box was read, having OTyp=0
    bool opct_premult; // If an "opct" box was read, having OTyp=1
    int chroma_key_len; // Length of `chroma_key_buf'
    kdu_byte *chroma_key_buf; // See below

    int resolved_cmap_channels; // See below
  };
  /* Notes:
        The `j2_channel' structure describes 3 types of channel mappings
     for each colour (and potentially non-colour) channel.  These three
     mappings are: 1) intensity; 2) opacity; and 3) premultiplied opacity.
     They correspond to the first three entries in each of the `j2_channel'
     member arrays.
        The fourth entry in each array allows a second channel mapping to be
     provided where a primary mapping uses the `JP2_CHANNEL_FORMAT_SPLIT_EXP'
     data format.  This secondary mapping is for the exponent part of the
     dual channel `JP2_CHANNEL_FORMAT_SPLIT_EXP' format.  Importantly, only
     one mapping is allowed to an exponent part for any colour channel.  The
     exponent can be shared by any or all of the colour intensity,
     opacity and premultiplied opacity attributes of the channel, but of
     course this makes very little sense.  The primary intent is really to
     provide exponents for intensity channels, but that is not
     an official restriction in IS15444-2/Amd3, which describes the
     split-exponent format.
        The `j2_channel' structure is also used to describe non-colour
     mappings -- these correspond to channels that have a non-zero
     `base_typ' field.  The `base_typ' is the TYP field to be used in the
     CDEF (Channel Definitions) box when describing the intensity channel.
     This is necessarily 0 when describing the intensity associated with
     one of the colours defined by a colour space.  We use the term
     "non-colour channel" for all other channels, even though they may
     have custom colour interpretations that are defined elsewhere.
     Non-colour channels have `base_typ' values ranging from 3 to 65534.
     The values 1 and 2 are used for opacity and pre-multiplied opacity (not
     intensity) so cannot be used for `base_typ'.  The value 65535 is reserved
     to mean there the type is not specified, so it should be used here.  In
     practice, the only currently defined value other than 0 is 3, which is
     defined by the JPH file format (JPEG 2000 Part-15) to mean that the
     channel has an application-defined colour association, identified via
     `j2_channel::non_colour_asoc'.  The `non_colour_asoc' value is ignored
     (but should be 0) unless `base_typ' is non-zero, since the ASOC field
     for colour intensity channels in the CDEF box is identical to the
     ordinal position of the `j2_channel' structure within the `channels'
     array that contains it.
        Non-colour channels can have opacity or pre-multiplied opacity, but
     in that case the relevant entry in the `j2_channel::all_channels' array
     must be true, and the same opacity/premult mapping must appear in all
     channels, whether they be colour or non-colour channels.
        Unused or invalid entries in each integer-typed member array of the
     `j2_channel' structure have negative values (-1 in practice).
        The `data_format' array contains values that are related to those
     passed across the `jp2_channels::set_colour_mapping',
     `jp2_channels::get_colour_mapping' and other related external functions,
     except that the data format codes used by those functions are stored in
     the least significant 16 bit positions, leaving the 16 MMSB's to hold
     the number of integer bits associated with `JP2_CHANNEL_FORMAT_FIXPOINT'
     formats or the number of exponent bits associated with
     `JP2_CHANNEL_FORMAT_FLOAT' formats.
        The `pxfm_descriptors' and `cdef_triplets' arrays are used to keep
     track of partially parsed results from PXFM and CDEF boxes,
     respectively, since these boxes work together and may be encountered
     in any order during file reading.  After all reading boxes from a global
     JP2 header or from an individual compositing layer header box, the
     `finalize' function uses the information found in these two arrays to
     allocate the `channels' array and fill out each channel's
     `j2_channels::cmap_channels' array.
        The `cdef_triplets' array contains three entries (a triplet) for each
     description in the CDEF box.  The first entry holds the TYP field; the
     next entry holds the ASOC field; and the last entry holds the mapped
     channel index for the description.
        The `pxfm_descriptors' array holds one entry for each distinct triplet
     found in the `cdef_triplets' array; the 16 LSB's of each entry hold
     the pixel format code; while the 16 MSB's hold the mapped channel
     index for the description, that indexes either the component mapping
     box or the codestream components directly.
        File readers fill in the `channels' array in a three step process.
     First the relevant boxes are parsed: cdef or opct and perhaps pxfm.
     Second, the `finalize' function allocates the `channels' array and
     fills in its `j2_channel::cmap_channels' entries, based on the parsed
     information recorded in `pxfm_descriptors', `cdef_triplets' and
     the opct-related fields `have_chroma_key' through `chroma_key_buf'.
     Finally, the `find_cmap_channels' function is invoked once for each
     codestream associated with the relevant compositing layer, using
     the codestream's finalized `j2_component_map' object to discover the
     values for all the other `j2_channel' member arrays.
        When preparing to write a JP2 or JPX file, the application's calls
     to `jp2_channels::init', `jp2_channels::set_colour_mapping',
     `jp2_channels::set_opacity_mapping' and
     `jp2_channels::set_premult_mapping' serve to set entries in the
     `j2_channel::codestream_idx', `j2_channel::component_idx',
     `j2_channel::lut_idx' and `j2_channel::data_formats' arrays, but the
     `j2_channel::cmap_channel' entries are left negative.  After `finalize',
     `add_cmap_channels' is invoked for each of the codestreams
     associated with the compositing layer; these calls populate the
     `j2_channel::cmap_channel' arrays.  Finally, `save_boxes' writes the
     relevant set of boxes based on the information that is found in the
     completed `j2_channel' records.
        The `resolved_cmap_channels' member is used to maintain state
     information between calls to `add_cmap_channels' or `find_cmap_channels'.
     This is needed because the channel mappings associated with consecutive
     code-streams which are referenced by the present object are essentially
     stacked on top of one another.
        The `chroma_key_buf' member will be non-NULL only if an 'opct'
     box is parsed from a JPX file and that box contains a chroma-key
     description.  The chroma-key description cannot be unpacked until the
     precision of each colour intensity channel is known, which will not
     happen until later when the `find_cmap_channels' function is called.
     Until that point, the packed chroma-key data is stored in the
     `chroma_key_buf' array points to the start of that buffer.  The chroma
     key description is unpacked during the call to `find_cmap_channels'
     in which all channel descriptions are finally resolved.
  */

/*****************************************************************************/
/*                               j2_resolution                               */
/*****************************************************************************/

class j2_resolution { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_resolution,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_resolution()
      { display_ratio = capture_ratio = 0.0F;
        display_res = capture_res = 0.0F; }
      /* Does the work of the parallel `jp2_resolution::init' function. */
    bool is_initialized() { return (display_ratio > 0.0F); }
      /* Returns true if the object has been initialized either by
         `j2_resolution::init' or by `jp2_resolution::init'. */
    bool compare(j2_resolution *src)
      { 
        return ((display_ratio == src->display_ratio) &&
                (capture_ratio == src->capture_ratio) &&
                (display_res == src->display_res) &&
                (capture_res == src->capture_res));
      }
      /* Returns true if the object's contents are identical to those of
         the `src' object. */
    void copy(j2_resolution *src)
      { display_ratio=src->display_ratio; capture_ratio=src->capture_ratio;
        display_res=src->display_res; capture_res=src->capture_res; }
      /* Copies the contents of `src' to the present object, which must not
         have been initialized yet. */
    void init(float aspect_ratio = 1.0F);
    bool init(jp2_input_box *res_box);
      /* Initializes the object from the information recorded in a resolution
         box.  Note that the function closes the `res_box' and returns true
         when done. If the function finds that one or more sub-boxes are
         incomplete (can only happen with a caching information source
         derived from `kdu_cache2'), the function returns false, resetting
         `res_box' to point to the beginning of the resolution box, but
         leaving it open.  This allows the function to be called again when
         there is more information available in the cache. */
    void finalize();
      /* If the object has not been explicitly initialized by a previous
         call to `init' or `jp2_resolution::init', this function fills in
         default resolution parameters. */
    void save_box(jp2_output_box *super_box);
      /* Creates a resolution box and appropriate sub-boxes inside the
         supplied `super-box'. */
  private: // Helper functions
    void parse_sub_box(jp2_input_box *box);
      /* Parses resolution values from either type of resolution sub-box.
         Closes the box for us. */
    void save_sub_box(jp2_output_box *super_box, kdu_uint32 box_type,
                      double v_res, double h_res);
      /* Creates a capture or display resolution box and writes the supplied
         vertical and horizontal resolution values into that box. */
  private: // Data
    friend class kdu_supp::jp2_resolution;
    float display_ratio;
    float capture_ratio;
    float display_res; // > 0 if and only if display info available
    float capture_res; // > 0 if and only if capture info available
  };

/*****************************************************************************/
/*                                 j2_colour                                 */
/*****************************************************************************/

class j2_colour { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_colour,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_colour(j2_memsafe *msafe);
    ~j2_colour();
    void destroy()
      { /* MUST use this instead of `delete'!! */
        j2_memsafe *msafe = this->memsafe;
        this->~j2_colour();
        msafe->free(this);
      }
    bool is_initialized() { return initialized; }
      /* Returns true if the object has been initialized either by
         `j2_colour::init' or by `jp2_colour::init'. */
    bool compare(j2_colour *src);
      /* Returns true if the object's contents are identical to those of
         the `src' object. */
    void copy(j2_colour *src);
      /* Copies the contents of `src' to the present object, which must not
         have been initialized yet. */
    void init(jp2_input_box *colr_box);
      /* Initializes the object from the information recorded in a colour
         description box.  Note that the constructor closes the
         `colr_box' when done.  If a colour box has already been encountered,
         the current box is closed immediately and its contents ignored. */
    void finalize(j2_channels *channels);
      /* Checks that the object has been correctly initialized, generating
         an error if not.  Also extracts bit-depth information and the
         actual number of colours (if we did not know the number already)
         from the supplied `channels' object.  The `channels' object must
         have been finalized and must also have had its `add_cmap_channels'
         or `find_cmap_channels' function called sufficiently often to
         find all the channel associations, along with precision information
         for all of the channels. */
    void save_box(jp2_output_box *super_box);
      /* Creates a colour description box and saves it as a sub-box of the
         supplied `super-box'.  Does nothing if the object has not been
         initialized, or if `num_colours' is 0. */
    bool is_jp2_compatible() const;
      /* Returns true if the colour representation embedded in this object is
         compatible with the limited set of colour descriptions supported by
         the elementary JP2 file format. */
    bool is_jph_compatible() const;
      /* Returns true if the colour representation embedded in this object is
         compatible with the set of colour descriptions supported by the
         JPH file format, which is a superset of that supported by the
         JP2 format, but not as rich as those offered by the JPX format.
         This function also returns true if there has been no call to
         `init', since it is allowable for a JPH file to have no colour
         space at all. */
    int get_num_colours() { return num_colours; }
      /* This function should return the correct number of colours at any
         point after `init' or `jp2_colour::init' has been called.  There
         is no need to wait until `finalize' has been called.  This is
         important, since `finalize' needs to be supplied with a finalized
         `j2_channels' object, and that object requires the number of
         colours, as returned here.  The function may return 0 only if the
         object has not been initialized, or a vendor colour space has
         been defined -- vendor colour spaces are inscrutinable. */
    friend class kdu_supp::jp2_colour;
    friend class kd_supp_local::j2_colour_converter;
    j2_memsafe *memsafe;
    bool initialized; // If `init' or `jp2_colour::init' has been called.
    jp2_colour_space space;
    int num_colours; // If `space_valid', can be 0 only if vendor colour space
    int precision[3]; // Number of bits for first 3 channel components
    float zeta[3]; // Unsigned natural relative zero point for 1st 3 channels
  private: // Data members specific to ICC and vendor profiles
    j2_icc_profile *icc_profile;
    kdu_byte vendor_uuid[16];
    int vendor_buf_length;
    kdu_byte *vendor_buf;
  private: // Data members specific to Lab and Jab colour spaces
    int range[3], offset[3]; // Lrange, Loff, etc.
    kdu_uint32 illuminant;
    kdu_uint16 temperature;
  public: // Links for including in a list
    int precedence; // Configured by `init' or `jpx_layer_target::add_colour'
    kdu_byte approx; // Configured by `init' or `jpx_layer_target::add_colour'.
    j2_colour *next;
  };

/*****************************************************************************/
/*                               jp2_header                                  */
/*****************************************************************************/

#define JP2_HEADER_BOX_FOR_JPH  ((int) 1)

class jp2_header { 
  /* This object is used to parse and generate JP2 and JPH files, as well
     as MJ2 video tracks.  JPX files use their own mechanisms to manage the
     elements which might appear within a JP2 image header found within a JPX
     file. */
  public: // Lifecycle member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(jp2_header,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    jp2_header(j2_memsafe *memsafe); // Allocates storage via `memsafe'
    jp2_header(const jp2_header &rhs); // Deliberately left unimplemented
    jp2_header &operator=(jp2_header &rhs); // Deliberately not implemented
    ~jp2_header(); // Destroys the internal heap storage.
    void destroy(j2_memsafe *msafe)
      { /* MUST use this instead of `delete'!! */
        this->~jp2_header();
        msafe->free(this);
      }
  public: // Header transfer functions
    bool read(jp2_input_box *input, kdu_uint32 jporh_compatible_brand);
      /* [SYNOPSIS]
           Parses information from an open JP2 Header box.  The supplied
           `input' box must already have a type code equal to
           `jp2_header_4cc'.  Generates an error (through `kdu_error') if
           a parsing error occurs.
           [//]
           The `jp2orh_compatible_brand' should be one of `jp2_brand',
           `jph_brand' or 0.  It is not possible for a file to be fully
           compatible with both JP2 and JPH, but it is possible that it
           contains imagery that is JP2 or JPH compatible, but global
           compatibility cannot be signalled at the file level.
           [//]
           Upon successful completion, the function automatically closes the
           open `input' box which is received on entry and returns true.
           [//]
           If the `input' box derives its information from a `kdu_cache2'
           object, one or more of the required sub-boxes might not yet be
           available from the cache.  In this event, the function returns
           false, leaving the `input' box open.  In this case, you may call
           the function again, with the same open `input' box, not altering
           its state in any way, once more information is available in the
           cache.  You may continue calling the function in this way, until
           it returns true, or a parsing error is encountered, causing
           a message to be delivered through `kdu_error'.
      */
    void write(jp2_output_box *open_box, int flags);
      /* [SYNOPSIS]
           Writes the contents of a JP2 header, including all sub-boxes, into
           the supplied `open_box'.  Note carefully, that unlike most
           box writing functions which form part of the Kakadu support for
           the JP2 file format family, this function accepts a box which has
           already been opened and assigned the relevant box type-code,
           `jp2_header_4cc'.  The function does not check the type code and
           it does not close the box upon exit.  The reason for this
           behavioural variation is that JP2 header boxes appear at the
           top level of a JP2 file, but at subordinate levels in other
           JP2-family file formats, such as MJ2.
         [ARG: flags]
           If this argument is zero, colour space and channel mapping must
           conform to the JP2 format, as defined in JPEG 2000 Part-1, which
           is somewhat limited.  Currently, the only flag that can be supplied
           here is `JP2_HEADER_BOX_FOR_JPH' -- if present, the richer colour
           spaces and custom non-colour channel mappings defined for the
           JPH file format are allowed, as defined in Part-15 of the JPEG 2000
           family of standards, but complex opacity (different opacity mappings
           or types for different colour or non-colour channels) is not
           allowed.
      */
  public: // Access to component data interfaces.
    bool is_jp2_compatible();
      /* [SYNOPSIS]
           This function can only reliably be used if either of the
           `jp2_dimensions::init' functions was used to configure the
           `jp2_dimensions' information during file generation; one of the
           `jp2_dimensions::finalize_compatibility' functions might also
           need to have been called.
      */
    bool is_jph_compatible();
      /* [SYNOPSIS]
           Same as `is_jp2_compatible', but determines whether the JP2 header
           contents are compatible with the JPH brand defined in JPEG 2000
           Part-15.
      */
    jp2_dimensions access_dimensions();
      /* [SYNOPSIS]
           Provides an interface which may be used for setting up the
           information in the JP2 "Image Header" box.
           [//]
           You ARE REQUIRED to complete this initialization before calling
           `write'.  The most convenient way to initialize the
           dimensions is usually to use the second form of the overloaded
           `jp2_dimensions::init' function, passing in the finalized
           `siz_params' object returned by `kdu_codestream::access_siz'.
      */
    jp2_colour access_colour();
      /* [SYNOPSIS]
           Provides an interface which may be used for setting up the
           information in the JP2 "Color" box.
           [//]
           You ARE REQUIRED to complete this initialization before calling
           `write'.
      */
    jp2_palette access_palette();
      /* [SYNOPSIS]
           Provides an interface which may be used for setting up a
           JP2 "Palette" box.
           [//]
           It is NOT NECESSARY to access or initialize any palette
           information; the default behaviour is to not use a palette.
      */
    jp2_channels access_channels();
      /* [SYNOPSIS]
           Provides an interface which may be used to specify the
           relationship between code-stream image components and colour
           reproduction channels (colour intensity channels, opacity
           channels, and pre-multiplied opacity channels).  This information
           is used to construct appropriate JP2 "Component Mapping" and
           "Channel Definition" boxes.
           [//]
           It is NOT NECESSARY to access or initialize the `jp2_channels'
           object directly; the default behaviour is to assign the colour
           intensity channels to the initial code-stream image components.
           That is, the first code-stream component is assigned to the
           luminance or red intensity, the second is assigned to the
           green intensity, and the third is assigned to the blue intensity.
           The mandatory information configured in the `jp2_colour' object
           is used to determine whether a luminance or RGB colour space is
           involved.
      */
    jp2_resolution access_resolution();
      /* [SYNOPSIS]
           Provides an interface which may be used to specify the
           aspect ratio and physical resolution of the resolution canvas
           grid.
           [//]
           It is NOT NECESSARY to access or initialize the `jp2_resolution'
           object.
      */
  private: // Data
    j2_header *state;
  };

/*****************************************************************************/
/*                             j2_data_references                            */
/*****************************************************************************/

class j2_data_references { 
  public: // Member functions
    static void *operator new(size_t nbytes, j2_memsafe *msafe)
      { return msafe->alloc(nbytes,KDU_ALIGNOF(j2_data_references,8)); }
    static void operator delete(void *ptr, j2_memsafe *msafe)
      { return msafe->free(ptr); } // Called only if construct throws
    j2_data_references(j2_memsafe *msafe)
      { 
        num_refs = max_refs = 0;  refs = file_refs = NULL;
        this->memsafe = msafe;
      }
    ~j2_data_references();
    void destroy()
      { /* MUST use this instead of `delete'!! */
        j2_memsafe *msafe = this->memsafe;
        this->~j2_data_references();
        msafe->free(this);
      }
    void init(jp2_input_box *box);
      /* Initializes the object from the information recorded in the
         supplied open `box', which may be of type `dtbl' or `dref'.
         Closes the box before returning. */
    void save_box(jp2_output_box *box);
      /* Writes to the supplied open data references `box'.  The format
         of the data which is written depends upon whether its type is
         `dtbl' or `dref'.  The box is automatically closed before the
         function returns. */
  private: // data
    friend class kdu_supp::jp2_data_references;
    j2_memsafe *memsafe;
    int num_refs; // Number of valid entries in the `refs' array.
    int max_refs; // Size of `refs' array
    char **refs;
    char **file_refs;
  };


/* ========================================================================= */
/*                         Other JP2-family Constants                        */
/* ========================================================================= */

static const kdu_uint32 jp2_signature       = 0x0D0A870A;

#undef KDU_ERROR
#undef KDU_ERROR_DEV
#undef KDU_WARNING
#undef KDU_WARNING_DEV
#undef KDU_TXT

} // namespace kd_supp_local

#endif // JP2_SHARED_H
