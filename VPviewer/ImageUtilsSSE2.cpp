#include <stdafx.h>
#include <tmmintrin.h>

void	MixLayerSSE(int data_len, unsigned char* in_src1, unsigned char* in_src2, unsigned char* out_dst, int alpha)
{
	int sse_len		= data_len/16;
	int sse_remaind = data_len%16;
	
	__m128i src, alpha1, alpha2, zero;
	__m128i temp1_1, temp1_2, temp2_1, temp2_2;

	alpha = (64 * alpha) / 100;
	
	alpha1	= _mm_set1_epi16(alpha);
	alpha2	= _mm_set1_epi16(64-alpha);
	zero	= _mm_set1_epi8(0);

	for (int i = 0; i < sse_len; i++){
		src		= _mm_stream_load_si128((__m128i*)in_src1);
		temp1_1	= _mm_unpacklo_epi8(src, zero);
		temp1_1 = _mm_mullo_epi16(temp1_1, alpha1);
		
		temp1_2	= _mm_unpackhi_epi8(src, zero);
		temp1_2 = _mm_mullo_epi16(temp1_2, alpha1);

		src = _mm_stream_load_si128((__m128i*)in_src2);
		temp2_1 = _mm_unpacklo_epi8(src, zero);
		temp2_1 = _mm_mullo_epi16(temp2_1, alpha2);

		temp2_2 = _mm_unpackhi_epi8(src, zero);
		temp2_2 = _mm_mullo_epi16(temp2_2, alpha2);

		temp1_1 = _mm_adds_epu16(temp1_1, temp2_1);
		temp1_2 = _mm_adds_epu16(temp1_2, temp2_2);

		temp1_1 = _mm_srli_epi16(temp1_1, 6);
		temp1_2 = _mm_srli_epi16(temp1_2, 6);

		temp1_1 = _mm_packus_epi16(temp1_1, temp1_2);
		
		_mm_stream_si128((__m128i*)out_dst, temp1_1);

		in_src1 += 16;
		in_src2 += 16;
		out_dst += 16;

	}
}

