// ImageInfoTab.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "ImageInfoTab.h"


// CImageInfoTab dialog

IMPLEMENT_DYNAMIC(CImageInfoTab, CDialog)

CImageInfoTab::CImageInfoTab(CWnd* pParent /*=NULL*/)
	: CDialog(CImageInfoTab::IDD, pParent)
{	
}

CImageInfoTab::~CImageInfoTab()
{
}

void CImageInfoTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATICRES, m_staticRes);
	DDX_Control(pDX, IDC_WELL, m_staticWell);
	DDX_Control(pDX, IDC_DEPTH, m_staticDepth);
	DDX_Control(pDX, IDC_COMMENTS, m_staticComments);
	DDX_Control(pDX, IDC_RESOLUTION, m_staticResolution);
}



BEGIN_MESSAGE_MAP(CImageInfoTab, CDialog)

	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CImageInfoTab message handlers

BOOL CImageInfoTab::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (m_hAccel != NULL)
		if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
			return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CImageInfoTab::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CImageInfoTab::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Add your message handler code here
	ImageExternalInfo* info = app->m_config.GetImageExternalInfo();
	scanningRes.Format(_T("%4.2f"), info->scanningRes);

	well = CA2T(info->well.c_str());
	depth = CA2T(info->depth.c_str());
	comments = CA2T(info->comment.c_str());
	resolution.Format(_T("%d �� %d"), app->m_config.getResolution().width, app->m_config.getResolution().height);

	m_staticRes.SetWindowText(scanningRes);
	m_staticWell.SetWindowText(well);
	m_staticDepth.SetWindowText(depth);
	m_staticComments.SetWindowText(comments);
	m_staticResolution.SetWindowText(resolution);

}
