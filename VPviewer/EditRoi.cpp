// EditRoi.cpp : implementation file
//

#include "stdafx.h"
#include "EditRoi.h"
#include "VPViewerConf.h"

//extern static const struct colors[];
// CEditRoi dialog

IMPLEMENT_DYNAMIC(CEditRoi, CDialog)

CEditRoi::CEditRoi(CWnd* pParent /*=NULL*/)
	: CDialog(CEditRoi::IDD, pParent)
{
	m_bRoiDelete = false;
}

CEditRoi::~CEditRoi()
{
}

void CEditRoi::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DESCRIPTION, m_description);
	DDX_Control(pDX, IDC_COLOR, m_color);
	DDX_Text(pDX, IDC_DESCRIPTION, m_descriptionStr);
	DDX_CBIndex(pDX, IDC_COLOR, m_colorIndex);
}


BEGIN_MESSAGE_MAP(CEditRoi, CDialog)
END_MESSAGE_MAP()


// CEditRoi message handlers

BOOL CEditRoi::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	for (int i = 0; i < 6; i++)
		m_color.AddString(g_PENColorString[i]);
	m_color.SetCurSel(m_colorIndex);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CEditRoi::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class
	CDialog::OnOK();
}
