// EditLabel.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "kdu_show.h"
#include "EditLabel.h"
#include "afxdialogex.h"


COLORREF ToolValues::FgColor = RGB(0, 0, 0);
COLORREF ToolValues::BgColor = RGB(255, 255, 255);
int ToolValues::LineWidth = 1;
int ToolValues::FontSize = 200;
CString ToolValues::FontName = "tahoma";
int ToolValues::FontMode = TRANSPARENT;

ToolValues::ToolValues(void) {}
ToolValues::~ToolValues(void) {}

// EditLabel 대화 상자입니다.

IMPLEMENT_DYNAMIC(CEditLabel, CDialogEx)

CEditLabel::CEditLabel(CWnd* pParent /*=NULL*/)
: CDialogEx(CEditLabel::IDD, pParent)
{
	
}

CEditLabel::~CEditLabel()
{
}

void CEditLabel::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FONT_COLOR, m_fgColorBox);
	DDX_Control(pDX, IDC_BG_COLOR, m_bgColorBox);
	DDX_Control(pDX, IDC_FONT_BUTTON, m_btnFont);
	DDX_Control(pDX, IDC_TRANSPARENT_CHECK, m_cbTransparent);
}


BEGIN_MESSAGE_MAP(CEditLabel, CDialogEx)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_LABEL_CHANGE, &CEditLabel::OnBnChangeLabel)
	ON_BN_CLICKED(IDC_FONT_BUTTON, OnBtnFontClicked)
	ON_BN_CLICKED(IDC_TRANSPARENT_CHECK, OnCheckBoxClicked)
	ON_COMMAND(IDC_FONT_COLOR, OnFgClick)
	ON_COMMAND(IDC_BG_COLOR, OnBgClick)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// EditLabel 메시지 처리기입니다.

void CEditLabel::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CLabelObj* label = (CLabelObj*)mgr->GetSelectObj();
	SetDlgItemText(IDC_EDIT_LABEL, mgr->GetSelectObj()->GetDescription());
	ToolValues::FgColor = label->getInfo()->fontColor;
	ToolValues::BgColor = label->getInfo()->bgColor;
	
	if (m_isFirst){
		ToolValues::FontMode = label->getInfo()->transparent;
		m_isFirst = false;
	}
	
	if (ToolValues::FontMode == TRANSPARENT){
		CheckDlgButton(IDC_TRANSPARENT_CHECK, true);
	}
	else{
		CheckDlgButton(IDC_TRANSPARENT_CHECK, false);
	}
}

void CEditLabel::OnBnChangeLabel()
{
	TCHAR strTemp[256];
	GetDlgItemText(IDC_EDIT_LABEL, strTemp, 256);
	mgr->GetSelectObj()->EditDescription(strTemp);
	CLabelObj* label = (CLabelObj*) mgr->GetSelectObj();
	label->getInfo()->bgColor = ToolValues::BgColor;
	label->getInfo()->fontColor = ToolValues::FgColor;
	label->getInfo()->font = ToolValues::FontName;
	label->getInfo()->size = ToolValues::FontSize;
	label->getInfo()->transparent = ToolValues::FontMode;
	
	m_isFirst = true;
	::SendMessage(this->m_hWnd, WM_CLOSE, NULL, NULL);
}


HBRUSH CEditLabel::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	TRACE("ToolbarDialog::OnCtlColor()\n");
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (m_fgColorBox.GetSafeHwnd() == pWnd->GetSafeHwnd()) {
		hbr = ::CreateSolidBrush(ToolValues::FgColor);
	}
	if (m_bgColorBox.GetSafeHwnd() == pWnd->GetSafeHwnd()) {
		hbr = ::CreateSolidBrush(ToolValues::BgColor);
	}
	return hbr;
}

void CEditLabel::OnBgClick(void)
{
	TRACE("ToolbarDialog::OnBgClick\n");

	CColorDialog dlg;
	if (dlg.DoModal() == IDOK) {
		ToolValues::BgColor = dlg.GetColor();
		Invalidate(TRUE);
	}
}

void CEditLabel::OnFgClick(void)
{
	TRACE("ToolbarDialog::OnFgClick\n");
	CColorDialog dlg;

	if (dlg.DoModal()) {
		ToolValues::FgColor = dlg.GetColor();
		Invalidate(TRUE);
	}
}

void CEditLabel::OnBtnFontClicked() {
	TRACE("ToolbarDialog::OnBtnFontClicked()\n");
	LOGFONT logFont;
	int size;
	CFontDialog dlg;
	if (dlg.DoModal() == IDOK) {

		dlg.GetCurrentFont(&logFont);

		ToolValues::FontName = logFont.lfFaceName;
		ToolValues::FontSize = dlg.GetSize();
		ToolValues::FgColor = dlg.GetColor();
		Invalidate(TRUE);
	}
}

void CEditLabel::OnCheckBoxClicked(){
	
	if (IsDlgButtonChecked(IDC_TRANSPARENT_CHECK))
		ToolValues::FontMode = OPAQUE;
	else
		ToolValues::FontMode = TRANSPARENT;
	CheckDlgButton(IDC_TRANSPARENT_CHECK, !IsDlgButtonChecked(IDC_TRANSPARENT_CHECK));

}