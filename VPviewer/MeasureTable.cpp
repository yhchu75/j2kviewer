// MeasureTable.cpp : implementation file
//

#include "stdafx.h"
#include "MeasureTable.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CMeasureTable dialog

IMPLEMENT_DYNAMIC(CMeasureTable, CDialog)

CMeasureTable::CMeasureTable(CWnd* pParent /*=NULL*/)
	: CDialog(CMeasureTable::IDD, pParent)
{
	Create(CMeasureTable::IDD, pParent);
	//ShowWindow(SW_SHOW);
}

CMeasureTable::~CMeasureTable()
{
}

void CMeasureTable::PostNcDestroy() 
{

  CDialog::PostNcDestroy();

  delete this;
}

BOOL CMeasureTable::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
// insert columns


	ListView_SetExtendedListViewStyleEx(m_listCtrl.GetSafeHwnd(), LVS_EX_FULLROWSELECT , LVS_EX_FULLROWSELECT);

	LV_COLUMN lvc;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	for(int i = 0; i<NUM_MTABLE_COL; i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = ColumnLabel[i];
		lvc.cx = ColumnWidth[i];
		lvc.fmt = ColumnFmt[i];
		m_listCtrl.InsertColumn(i,&lvc);
	}

	LV_ITEM lvi;

	for(int i = 0; i < NUM_MTABLE_ROW; i++)
	{
		lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_STATE;
		lvi.iItem = i;
		lvi.iSubItem = 0;
		lvi.pszText = _T("");
		lvi.stateMask = LVIS_STATEIMAGEMASK;
		lvi.state = INDEXTOSTATEIMAGEMASK(1);

		m_listCtrl.InsertItem(&lvi);
	}

	for(int i = 0; i<NUM_MTABLE_ROW; i++)
	{
		for(int j = 0; j<NUM_MTABLE_COL; j++)
		{
			itemText[i][j][0] = '\0';
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CMeasureTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MLIST, m_listCtrl);

}


BEGIN_MESSAGE_MAP(CMeasureTable, CDialog)
	ON_WM_SHOWWINDOW()
//	ON_NOTIFY(NM_CLICK, IDC_MLIST, &CMeasureTable::OnNMClickMlist)
ON_BN_CLICKED(IDC_EXPORT, &CMeasureTable::OnBnClickedExport)
	ON_BN_CLICKED(IDC_REFRESH, &CMeasureTable::OnBnClickedRefresh)
	ON_BN_CLICKED(IDC_SAVEMEASURE, &CMeasureTable::OnBnClickedSavemeasure)
	ON_NOTIFY(NM_CLICK, IDC_MLIST, &CMeasureTable::OnNMClickMlist)
	ON_NOTIFY(NM_DBLCLK, IDC_MLIST, &CMeasureTable::OnNMDblclkMlist)
END_MESSAGE_MAP()


// CMeasureTable message handlers

void CMeasureTable::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
		
	int i, j;
	for(i = 0; i<NUM_MTABLE_ROW; i++)
	{
		for(j = 0; j<NUM_MTABLE_COL; j++)
		{
			m_listCtrl.SetItemText(i, j, itemText[i][j]);
		}
	}
}


void CMeasureTable::OnBnClickedExport()
{
	TCHAR Filter[] = _T("csv File(*.csv) |*.csv|All Files(*.*)|*.*|");
	CFileDialog Dlg(FALSE, _T("csv file(*.csv)"), _T("*.csv"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, Filter, NULL);

	if (Dlg.DoModal() != IDOK) return;

	CString	FileName = Dlg.GetPathName();
	FILE *fp = _tfopen(FileName, _T("wt,ccs=UNICODE"));
			
	_ftprintf(fp, _T("%s \t   %s \t  %s \t  %s \n"), _T("No"), _T("Type"), _T("Length"), _T("Unit"));

	for (int i = 0; i < NUM_MTABLE_ROW; i++){
		if (itemText[i][0][0] == '\0') break;
		_ftprintf(fp, _T("%s \t  %s \t  %s \t  %s \n"), itemText[i][0], itemText[i][1], itemText[i][2], itemText[i][3]);
	}

	fclose(fp);
	
}


void CMeasureTable::OnBnClickedRefresh()
{
	AfxGetMainWnd()->OnCmdMsg(ID_MEASUREMENT_REFRESHTABLE, 0, NULL, NULL);

	int i, j;
	for(i = 0; i<NUM_MTABLE_ROW; i++)
	{
		for(j = 0; j<NUM_MTABLE_COL; j++)
		{
			m_listCtrl.SetItemText(i,j,itemText[i][j]);
		}
	}
	Invalidate();

}

void CMeasureTable::OnBnClickedSavemeasure()
{
	// TODO: Add your control notification handler code here
	AfxGetApp()->OnCmdMsg(ID_MEASUREMENT_SAVEMEASUREMENT, 0, NULL, NULL);

}


void CMeasureTable::OnNMClickMlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	std::vector<CMeasureObj*>& MeasurementList = m_pDrawObjMgr->GetMeasureObjList();
	if (index >= MeasurementList.size())return;

	m_pDrawObjMgr->SetActiveObj(MeasurementList[index]);
}


void CMeasureTable::OnNMDblclkMlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	std::vector<CMeasureObj*>& MeasurementList = m_pDrawObjMgr->GetMeasureObjList();
	if (index >= MeasurementList.size())return;

	m_pDrawObjMgr->SetActiveObj(MeasurementList[index]);
	
	POINTF center_ptf;
	float max_x = 0;
	float min_x = 1;
	float max_y = 0;
	float min_y = 1;
	for (int i = 0; i < MeasurementList[index]->getInfo()->pts.size(); i++){
		float x = MeasurementList[index]->getInfo()->pts.at(i).x;
		float y = MeasurementList[index]->getInfo()->pts.at(i).y;

		max_x = (x > max_x) ? x : max_x;
		min_x = (x < min_x) ? x : min_x;
		max_y = (y > max_y) ? y : max_y;
		min_y = (y < min_y) ? y : min_y;
	}
	center_ptf.x = (max_x + min_x) / 2;
	center_ptf.y = (max_y + min_y) / 2;

	m_app->set_center_pos(center_ptf.x, center_ptf.y, m_app->rendering_scale);
}
