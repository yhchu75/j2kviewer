// Grid.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "kdu_show.h"
#include "Grid.h"
#include "afxdialogex.h"

COLORREF GridValues::LineColor = RGB(0, 255, 0);
int GridValues::LineWidth = 2;
int GridValues::interval = 100;

GridValues::GridValues(void) {}
GridValues::~GridValues(void) {}


// Grid 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGrid, CDialogEx)

CGrid::CGrid(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGrid::IDD, pParent)
{

}

CGrid::~CGrid()
{
}

void CGrid::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_COLOR, m_ColorBox);
	DDX_Control(pDX, IDC_THICK_EDIT, m_thickEdit);
	DDX_Control(pDX, IDC_INTERVAL_SLIDER, m_sliderInterval);
	DDX_Control(pDX, IDC_INTERVAL_LABEL, m_intervlaLabel);
}


BEGIN_MESSAGE_MAP(CGrid, CDialogEx)
	ON_WM_SHOWWINDOW()
	ON_WM_HSCROLL()
	ON_EN_CHANGE(IDC_THICK_EDIT, &CGrid::OnChangeWidthEdit)
	ON_BN_CLICKED(IDC_BUTTON1, &CGrid::OnBnClickedApply)
	ON_COMMAND(IDC_GRID_COLOR, &CGrid::OnColorClick)
	ON_WM_CTLCOLOR()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_INTERVAL_SLIDER, &CGrid::OnNMReleasedcapture)
	ON_NOTIFY(UDN_DELTAPOS, IDC_THICK_SPIN, &CGrid::OnDeltaposThickSpin)
END_MESSAGE_MAP()


// Grid 메시지 처리기입니다.

int getGridScale(int value)
{
	int m_scale;
	if (value >= 500.0f){
		m_scale = 500;
	}
	else if (value >= 100.0f){
		m_scale = 100;
	}
	else if (value >= 50.0f)	{
		m_scale = 50;
	}
	else if (value >= 10.0f){
		m_scale = 10;
	}
	else if (value >= 1.0f){
		m_scale = 1;
	}
	else{
		m_scale = 0;
	}

	return m_scale;
	
}

int getNext(int value)
{
	return value + getGridScale(value);
}

int getPrev(int value)
{
	return value - getGridScale(value - 1);
}

std::vector<int> CGrid::setIntervals(int interval)
{
	std::vector<int> intervals;

	int c_scale = getGridScale(interval);

	int c_center = (interval / c_scale)*c_scale;

	int counter;
	int current_val;
	if (c_center == 1)
	{
		counter = 10;
	}
	else
	{		
		current_val = getPrev(c_center);
		intervals.push_back(current_val);
		counter = 9;

		while (1)
		{
			if (current_val == 1)
				break;
			if (counter == 5)
				break;

			current_val = getPrev(current_val);
			intervals.insert(intervals.begin(), current_val);
			counter--;
		}

	}
	intervals.push_back(c_center);
	current_val = c_center;
	m_sliderInterval.SetPos(10 - counter);
	while (counter != 0)
	{
		current_val = getNext(current_val);
		intervals.push_back(current_val);
		counter--;
	}

	return intervals;
}

void CGrid::setIntervalLabel(int interval)
{
	CString label;

	if (mgr->GetGridObjList()[0]->getInfo()->ratio != 1.0){
		if (interval > 10000)
		{
			label.Format(_T("%.2f"), (float)interval / 10000);
			m_unit = _T("cm");
		}
		else if (interval > 1000)
		{
			label.Format(_T("%.2f"), (float)interval / 1000);
			m_unit = _T("mm");
		}
		else{
			label.Format(L"%.1f", (float)interval);
			m_unit = _T("μm");
		}
	}
	else
	{
		label.Format(L"%d", interval);
		m_unit = _T("pixel");
	}

	m_intervlaLabel.SetWindowTextW(label+m_unit);
}

void CGrid::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CGridObj* grid = (CGridObj*)mgr->GetGridObjList()[0];
	
	GridValues::LineColor = grid->getInfo()->color;
	GridValues::interval = grid->getInfo()->interval;

	CString thick;
	thick.Format(_T("%d"), mgr->GetGridObjList()[0]->getInfo()->thick);

	m_thickEdit.SetWindowTextW(thick);
	
	m_sliderInterval.SetRange(0, 10);

	m_intervals = setIntervals(mgr->GetGridObjList()[0]->getInfo()->interval);

	setIntervalLabel(mgr->GetGridObjList()[0]->getInfo()->interval);
	mgr->GetGridObjList()[0]->UpdateGridInterval();
}


void CGrid::OnColorClick(void)
{
	TRACE("ToolbarDialog::OnColorClick\n");
	CColorDialog dlg;

	if (dlg.DoModal()) {
		GridValues::LineColor = dlg.GetColor();
		Invalidate(TRUE);
	}
	mgr->GetGridObjList()[0]->getInfo()->color = GridValues::LineColor;
	app->child_wnd->UpdateViewRegion();
}

HBRUSH CGrid::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	TRACE("ToolbarDialog::OnCtlColor()\n");
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (m_ColorBox.GetSafeHwnd() == pWnd->GetSafeHwnd()) {
		hbr = ::CreateSolidBrush(GridValues::LineColor);
	}
	return hbr;
}

void CGrid::OnChangeWidthEdit()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString thick;
	m_thickEdit.GetWindowTextW(thick);
	mgr->GetGridObjList()[0]->getInfo()->thick = _ttoi(thick);
	app->child_wnd->UpdateViewRegion();
}


void CGrid::OnBnClickedApply()
{
	DestroyWindow();
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CGrid::OnHScroll(UINT /*nSBCode*/, UINT /*nPos*/, CScrollBar* pScrollBar)
{
	CSliderCtrl *p_sliderCtrl = (CSliderCtrl *)pScrollBar;

	int pos = p_sliderCtrl->GetPos();

	mgr->GetGridObjList()[0]->getInfo()->interval = m_intervals[pos];
	setIntervalLabel(mgr->GetGridObjList()[0]->getInfo()->interval);

	mgr->GetGridObjList()[0]->UpdateGridInterval();
	app->child_wnd->UpdateViewRegion();
}

void CGrid::OnNMReleasedcapture(NMHDR* pNMHDR, LRESULT* pResult) {
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR; // TODO: Add your control
	// notification handler    
	
	m_intervals = setIntervals(mgr->GetGridObjList()[0]->getInfo()->interval);
	setIntervalLabel(mgr->GetGridObjList()[0]->getInfo()->interval);
	
	*pResult = 0;
}

void CGrid::OnDeltaposThickSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if (pNMUpDown->iDelta < 0)
	{
		CString thick;
		m_thickEdit.GetWindowTextW(thick);
		int thickness = _ttoi(thick);
		mgr->GetGridObjList()[0]->getInfo()->thick = thickness +1 ;
		thick.Format(_T("%d"), thickness + 1);
		m_thickEdit.SetWindowTextW(thick);
		app->child_wnd->UpdateViewRegion();
	}
	else
	{
		CString thick;
		m_thickEdit.GetWindowTextW(thick);
		int thickness = _ttoi(thick);
		if (thickness > 1){
			mgr->GetGridObjList()[0]->getInfo()->thick = thickness - 1;
			thick.Format(_T("%d"), thickness - 1);
			m_thickEdit.SetWindowTextW(thick);
		}
		app->child_wnd->UpdateViewRegion();
	}

	*pResult = 0;
}
