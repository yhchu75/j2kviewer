/******************************************************************************/
// File: ChildView.cpp [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/******************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/******************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/*******************************************************************************
Description:
   Implementation of the child view window of the interactive JPEG2000 image
viewer application, "kdu_show".  Client-area windows messages are
processed (at least initially) here.
*******************************************************************************/

#include "stdafx.h"
#include <afxpriv.h>
#include "kdu_show.h"
#include "ChildView.h"
#include "Overview.h"
#include "MeasureTable.h"
#include "AdjustmentsTab.h"
#include "EditLabel.h"
#include "Grid.h"
#include "../expander/ExpenderDlg.h"
#include "ToolPlugInsMgr.h"
#include "MeasureTable.h"
#include "RGBMeasureTable.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildView

/******************************************************************************/
/*                          CChildView::CChildView                            */
/******************************************************************************/

CChildView::CChildView()
{
  app = NULL;
  pixel_scale = last_pixel_scale = 1;
  max_view_size = kdu_coords(10000,10000);
  sizing = false;
  last_size = kdu_coords(0,0);
  scroll_step = kdu_coords(1,1);
  scroll_page = scroll_step;
  scroll_end = kdu_coords(100000,100000);
  screen_size.x = GetSystemMetrics(SM_CXSCREEN);
  screen_size.y = GetSystemMetrics(SM_CYSCREEN);

  focusing = false;
  panning = false;
  crop = false;
  cropping = false;
  cropped = false;
  analyze = false;

  HINSTANCE hinst = AfxGetInstanceHandle();
  normal_cursor = LoadCursor(NULL,IDC_ARROW);
  cross_cursor = LoadCursor(NULL,IDC_CROSS);
  panning_cursor = LoadCursor(NULL,IDC_SIZEALL);
  overlay_cursor = LoadCursor(hinst,MAKEINTRESOURCE(IDC_META_CURSOR));
  beam_cursor = LoadCursor(NULL, IDC_IBEAM);

  //MJY_020509 : measurement
  m_bMeasureTable = false;
  m_pMeasureTable = NULL;

  m_bRGBMeasureTable = false;
  m_pRGBMeasureTable = NULL;

  m_BackbufferDC.CreateCompatibleDC(NULL);
  m_bFullScreen = false;

  m_adjustment = false;
  m_pToolPlugInsMgr = NULL;
  m_pDrawObjMgr = NULL;
}

/******************************************************************************/
/*                          CChildView::~CChildView                           */
/******************************************************************************/

CChildView::~CChildView()
{
	
}

/******************************************************************************/
/*                            CChildView::set_app                             */
/******************************************************************************/

void
  CChildView::set_app(CKdu_showApp *app)
{
  this->app = app;
  m_pDrawObjMgr = &app->m_DrawObjMgr;
  m_pToolPlugInsMgr = app->frame_wnd->GetToolPlugInsMgr();
}

/******************************************************************************/
/*                       CChildView::set_max_view_size                        */
/******************************************************************************/

void
  CChildView::set_max_view_size(kdu_coords size, int pscale)
{
  max_view_size = size;
  pixel_scale = pscale;
  max_view_size.x*=pixel_scale;
  max_view_size.y*=pixel_scale;
  //if ((last_size.x > 0) && (last_size.y > 0))
    { /* Otherwise, the windows message loop is bound to send us a
         WM_SIZE message some time soon, which will call the
         following function out of the 'OnSize' member function. */
      check_and_report_size();
    }
  last_pixel_scale = pixel_scale;
}

/******************************************************************************/
/*                     CChildView::check_and_report_size                      */
/******************************************************************************/

void
  CChildView::check_and_report_size()
  /* The code here is very delicate.  The problem is that the framework
     enters into its own layout recalculations whenever the SetWindowPos
     function is called on the frame window.  This asserts certain internal
     guards against recursive re-entry into the layout recalculation code.
     To skirt around some of these complications, we override the frame
     object's OnSize function, letting it process an initial layout for the
     child and then calling this function here, which has to recover the
     layout generated by the frame (i.e., the parent).  This allows the code
     to re-enter the layout generation process -- unless somebody goes and
     modifies the implementation of some parts of the MFC framework.  Shame
     that MFC does not really protect the user against hidden implementation
     choices like this. */
{

	kdu_coords tmp;
	CRect rect;
	GetClientRect(&rect);
	tmp.x = rect.right-rect.left;
	tmp.y = rect.bottom-rect.top;
	
	scroll_step.x = (int)((float)tmp.x / 15.0F);
	scroll_step.y = (int)((float)tmp.x / 10.0F);

	app->set_view_size(tmp);
}


/////////////////////////////////////////////////////////////////////////////
// CChildView message handlers

BEGIN_MESSAGE_MAP(CChildView,CWnd )
	//{{AFX_MSG_MAP(CChildView)
	ON_WM_PAINT()
	ON_WM_KEYDOWN()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_KILLFOCUS()
	ON_WM_MOUSEACTIVATE()
	ON_WM_KEYUP()
	ON_COMMAND(ID_EDIT_DELETE, &CChildView::OnDelete)
	ON_COMMAND(ID_SCOOP_LEFT, &CChildView::OnScoopLeft)
	ON_COMMAND(ID_SCOOP_RIGHT, &CChildView::OnScoopRight)
	ON_COMMAND(ID_SCOOP_DOWN, &CChildView::OnScoopDown)
	ON_COMMAND(ID_SCOOP_UP, &CChildView::OnScoopUp)
	ON_COMMAND(ID_EDIT_COPYVIEWTOCLIPBOARD, &CChildView::OnEditCopyviewtoclipboard)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPYVIEWTOCLIPBOARD, &CChildView::OnUpdateEditCopyviewtoclipboard)
	ON_COMMAND(ID_EDIT_EXPORTASJPEG, &CChildView::OnEditExportASJPEG)
	ON_UPDATE_COMMAND_UI(ID_EDIT_EXPORTASJPEG, &CChildView::OnUpdateEditCopyviewtoclipboard)
	ON_COMMAND(ID_EDIT_CROPIMAGE, &CChildView::OnEditCropImage)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CROPIMAGE, &CChildView::OnUpdateEditCropImage)
	ON_COMMAND(ID_GRID_GRIDANALYSIS, &CChildView::OnAnalyzeImage)
	ON_UPDATE_COMMAND_UI(ID_GRID_GRIDANALYSIS, &CChildView::OnUpdateAnalyzeImage)
	ON_COMMAND(ID_MEASUREMENT_SCALEBAR, &CChildView::OnMeasurementScaleBar)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_SCALEBAR, &CChildView::OnUpdateMeasurementScaleBar)
	ON_COMMAND(ID_MEASUREMENT_TABLE, &CChildView::OnMeasurementTable)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_TABLE, &CChildView::OnUpdateMeasurementTable)
	ON_COMMAND(ID_MEASUREMENT_SHOW, &CChildView::OnMeasurementShow)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_SHOW, &CChildView::OnUpdateMeasurementShow)
	ON_COMMAND(ID_MEASUREMENT_SELECT, &CChildView::OnMeasurementSelect)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_SELECT, &CChildView::OnUpdateMeasurementSelect)
	ON_COMMAND(ID_MEASUREMENT_LINE, &CChildView::OnMeasurementLine)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_LINE, &CChildView::OnUpdateMeasurementLine)
	ON_COMMAND(ID_MEASUREMENT_CURVE, &CChildView::OnMeasurementCurve)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_CURVE, &CChildView::OnUpdateMeasurementCurve)
	ON_COMMAND(ID_MEASUREMENT_AREA, &CChildView::OnMeasurementArea)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_AREA, &CChildView::OnUpdateMeasurementArea)
	ON_COMMAND(ID_LABEL_ADDLABEL, &CChildView::OnMeasurementLabel)
	ON_UPDATE_COMMAND_UI(ID_LABEL_ADDLABEL, &CChildView::OnUpdateMeasurementLabel)
	ON_COMMAND(ID_LABEL_SHOWLABEL, &CChildView::OnShowLabel)
	ON_UPDATE_COMMAND_UI(ID_LABEL_SHOWLABEL, &CChildView::OnUpdateShowLabel)
	ON_COMMAND(ID_VIEW_FULLSCREEN, &CChildView::OnViewFullscreen)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FULLSCREEN, &CChildView::OnUpdateViewFullscreen)
	ON_COMMAND(ID_TOOLS_TOOLBAR, &CChildView::OnShowToolbar)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_TOOLBAR, &CChildView::OnUpdateToolbar)
	ON_COMMAND(ID_VIEW_BACKFROMFULL, &CChildView::OnViewBackfromfull)
	ON_COMMAND(ID_MEASUREMENT_REFRESHTABLE, &CChildView::OnMeasurementRefreshtable)
	ON_COMMAND(ID_AUTOUNIT, &CChildView::OnAutoUnit)
	ON_UPDATE_COMMAND_UI(ID_AUTOUNIT, &CChildView::OnUpdateAutoUnit)
	
	ON_WM_CREATE()
	ON_COMMAND(ID_MEASUREMENT_DELETEALL, &CChildView::OnMeasurementDeleteall)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_DELETEALL, &CChildView::OnUpdateMeasurementDeleteall)
	ON_COMMAND(ID_MEASUREMENT_RGB_TABLE, &CChildView::OnMeasurementRgbTable)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_RGB_TABLE, &CChildView::OnUpdateMeasurementRgbTable)


	ON_COMMAND(ID_SCREEN_FIT, &CChildView::OnFitScreen)
	ON_UPDATE_COMMAND_UI(ID_SCREEN_FIT, &CChildView::OnUpdateFitScreen)
	ON_COMMAND(ID_EXPORT_MEASUREMENT_AS_SVG, &CChildView::OnExportMeasurementAsSVG)
	ON_UPDATE_COMMAND_UI(ID_EXPORT_MEASUREMENT_AS_SVG, &CChildView::OnUpdateExportMeasurementAsSVG)
	ON_COMMAND(ID_DELETE_ALL_MEASUREMENT, &CChildView::OnMeasurementDeleteall)
	ON_UPDATE_COMMAND_UI(ID_DELETE_ALL_MEASUREMENT, &CChildView::OnUpdateMeasurementDeleteall)
	ON_COMMAND(ID_MEASUREMENT_RGB, &CChildView::OnShowRGBMeasurement)
	ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_RGB, &CChildView::OnUpdateShowRGBMeasurement)
END_MESSAGE_MAP()



// new toolbars
void CChildView::OnExportMeasurementAsSVG()
{
	app->OnEditExportmetadataassvg();
}

void CChildView::OnUpdateExportMeasurementAsSVG(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL);
}

void CChildView::OnShowRGBMeasurement()
{
	//((CMainFrame *)AfxGetMainWnd())->OnMeasurementRgb();
	OnMeasurementRgb();
}

void CChildView::OnUpdateShowRGBMeasurement(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_nShowRGB);
}

void CChildView::OnFitScreen()
{
	app->FitImageToClient();
}

void CChildView::OnUpdateFitScreen(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL);
}

// RGB MEASUREMENT
// ============================================================================
void CChildView::OnMeasurementRgb()
{
	// TODO: Add your command handler code here
	CWinApp* myapp = AfxGetApp();
	CMenu *main_menu = ((CMainFrame *)AfxGetMainWnd())->GetMenu();

	int show_rgb = main_menu->GetMenuState(ID_MEASUREMENT_RGB, MF_BYCOMMAND);
	main_menu->CheckMenuItem(ID_MEASUREMENT_RGB, (show_rgb == MF_CHECKED ? MF_UNCHECKED : MF_CHECKED) | MF_BYCOMMAND);
	show_rgb = main_menu->GetMenuState(ID_MEASUREMENT_RGB, MF_BYCOMMAND);
	myapp->WriteProfileInt(L"Settings", L"SWRGB", show_rgb);

	m_nShowRGB = show_rgb > 0 ? 1 : 0;

	std::vector<CMeasureObj*> measure_list = app->child_wnd->m_pDrawObjMgr->GetMeasureObjList();

	for (int i = 0; i < measure_list.size(); i++)
		measure_list[i]->Draw(&app->child_wnd->m_BackbufferDC, false);

	UpdateViewRegion();
}
// ============================================================================



/******************************************************************************/
/*                        CChildView::PreCreateWindow                         */
/******************************************************************************/

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
  if (!CWnd::PreCreateWindow(cs))
    return FALSE;

	// TOOLBAR
	// ============================================================================
	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	// ============================================================================

  cs.style &= ~WS_BORDER;
  cs.lpszClass = 
    AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
                        ::LoadCursor(NULL, IDC_ARROW),
                        HBRUSH(COLOR_WINDOW+1), NULL);
   // Note: we deliberately do not enable double clicks (CS_DBLCLK missing) --> MJY added CS_DBLCLKS
   // so as to facilitate user interaction with the focus box on various
   // platforms -- desktop and laptop.
  return TRUE;
}


int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
		
	m_caHalf.caSize = sizeof(m_caHalf);
	GetColorAdjustment(GetDC()->m_hDC, &m_caHalf);
	return 0;
}


void CChildView::UpdateViewRegion(CDC *dc)
{
	if (app == NULL) return;

	CDC *TargetDC;
	if (dc == NULL){
		TargetDC = GetDC();
	}
	else{
		TargetDC = dc;
	}

	kdu_dims region;
	CRect rect;
	GetClientRect(&rect);
	region.pos.x = rect.left;
	region.pos.y = rect.top;
	region.size.x = rect.right;
	region.size.y = rect.bottom;

	HBITMAP bitmap = app->update_region(region);

	if (bitmap ){
		m_BackbufferDC.SelectObject(bitmap);
		
		if (m_adjustment){	//Color adjustment 
			SetStretchBltMode(m_BackbufferDC, HALFTONE);
			SetColorAdjustment(m_BackbufferDC, &m_caHalf);
			
			m_BackbufferDC.StretchBlt(0, 0, region.size.x, region.size.y, &m_BackbufferDC, 0, 0, region.size.x, region.size.y, SRCCOPY);
		}

		//Draw items
		m_pDrawObjMgr->DrawObjs(&m_BackbufferDC);
		if (cropping || cropped)
		{
			Graphics G(m_BackbufferDC);
			G.SetSmoothingMode(SmoothingModeHighQuality);
			Color C1(100, 0, 0, 0);
			SolidBrush B1(C1);
			GraphicsPath path;

			int x1 = crop_point.x < mouse_point.x ? crop_point.x : mouse_point.x;
			int y1 = crop_point.y < mouse_point.y ? crop_point.y : mouse_point.y;
			int x2 = crop_point.x > mouse_point.x ? crop_point.x : mouse_point.x;
			int y2 = crop_point.y > mouse_point.y ? crop_point.y : mouse_point.y;

			G.FillRectangle(&B1, 0, 0, app->view_dims.size.x, y1 - app->y_offset);
			G.FillRectangle(&B1, 0, y1 - app->y_offset, x1 - app->x_offset, y2 - y1);
			G.FillRectangle(&B1, x2 - app->x_offset, y1 - app->y_offset, app->view_dims.size.x + app->x_offset - x2, y2 - y1);
			G.FillRectangle(&B1, 0, y2 - app->y_offset, app->view_dims.size.x, app->view_dims.size.y + app->y_offset - y2);
		}
		TargetDC->BitBlt(app->x_offset + region.pos.x, app->y_offset + region.pos.y, region.size.x, region.size.y,
			&m_BackbufferDC, 0, 0, SRCCOPY);
	}

	if (crop)
	{
		Graphics G(*TargetDC);
		Color C1(100, 0, 0, 0);
		Color C2(180, 0, 255, 0);
		SolidBrush B1(C1);
		SolidBrush B2(C2);

		Gdiplus::Font ft(L"tahoma", 20, FontStyleRegular, UnitPixel);

		G.FillRectangle(&B1, app->x_offset, app->y_offset, app->view_dims.size.x, app->view_dims.size.y);
		G.DrawString(L"Drag the cursor around the area\nyou want to capture.", -1, &ft, PointF(app->x_offset + 30, app->y_offset + 30), &B2);
	}
	
	if (dc == NULL){
		ReleaseDC(TargetDC);
	}
}


void CChildView::UpdateSelectRegion(POINTF	pt1, POINTF	pt2)
{
	POINT old_crop_point, old_mouse_point;
	old_crop_point = crop_point;
	old_mouse_point = mouse_point;

	crop_point = m_pDrawObjMgr->GetSelectObj()->ConvertPixelPoint(pt1);
	mouse_point = m_pDrawObjMgr->GetSelectObj()->ConvertPixelPoint(pt2);
	
	crop_point.x += app->x_offset;
	mouse_point.x += app->x_offset;
	
	crop_point.y += app->y_offset;
	mouse_point.y += app->y_offset;
	
	TRACE("Currnte cp[%d:%d] mp[%d:%d] =>  cp[%d:%d (%f:%f)]  mp[%d:%d (%f:%f)]\n", 
		old_crop_point.x, old_crop_point.y, old_mouse_point.x, old_mouse_point.y ,
		crop_point.x, crop_point.y, pt1.x, pt1.y, mouse_point.x, mouse_point.y, pt2.x, pt2.y);

	UpdateViewRegion();
}


/******************************************************************************/
/*                            CChildView::OnPaint                             */
/******************************************************************************/

void CChildView::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  UpdateViewRegion(&dc);
}


/******************************************************************************/
/*                           CChildView::OnKeyDown                            */
/******************************************************************************/

void CChildView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (app != NULL)
	{ // Look out for keys which are not bound to the menu -- currently the only
		//keys of this form are those with obvious scrolling functions and the
		//escape key.

		if (nChar == VK_LEFT)
		{
			app->set_scroll_pos(-scroll_step.x, 0, true);
		}
		else if (nChar == VK_RIGHT)
		{
			app->set_scroll_pos(scroll_step.x, 0, true);
		}
		else if (nChar == VK_UP)
		{
			app->set_scroll_pos(0, -scroll_step.y, true);
		}
		else if (nChar == VK_DOWN)
		{
			app->set_scroll_pos(0, scroll_step.y, true);
		}




		//   else if (nChar == VK_PRIOR)
		//     app->set_vscroll_pos(-scroll_page.y,true);
		//   else if (nChar == VK_NEXT)
		//{
		//app->set_vscroll_pos(scroll_page.y,true);
		//}
		if (nChar == VK_CONTROL)
		{
			if (panning)
			{
				SetCursor(normal_cursor);
				panning = false;
			}
			kdu_coords tmp = last_mouse_point;
			tmp.x /= pixel_scale;   tmp.y /= pixel_scale;
		}

	}
	CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

/******************************************************************************/
/*                             CChildView::OnKeyUp                            */
/******************************************************************************/

void
  CChildView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
  CWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

/******************************************************************************/
/*                          CChildView::OnLButtonDown                         */
/******************************************************************************/

void
  CChildView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SetCapture();
  last_mouse_point.x = point.x;
  last_mouse_point.y = point.y;
  
  if (m_pToolPlugInsMgr){
	  if (m_pToolPlugInsMgr->OnLButtonDown(GetSafeHwnd(), nFlags, point) != 0){
		  return;
	  }
  }

  if (crop)
  {
	  crop = false;
	  cropping = true;
	  crop_point = point;
	  return;
  }


  if (m_pDrawObjMgr)
	  if (m_pDrawObjMgr->OnLButtonDown(nFlags, point)) return;
  

  if ((nFlags & MK_CONTROL) != 0)
    {
      if ((nFlags & MK_SHIFT) != 0)
        {
          kdu_coords mouse_point;
          mouse_point.x = point.x;  mouse_point.y = point.y;
          app->set_compositing_layer(kdu_coords(mouse_point.x/pixel_scale,
                                                mouse_point.y/pixel_scale));
        }
    }

  else
  { 
		panning = true;
		mouse_pressed_point = point;
  }
}

void CChildView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

}

/******************************************************************************/
/*                          CChildView::OnRButtonDown                         */
/******************************************************************************/

void CChildView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if (app->buffer != NULL){
		if (m_pDrawObjMgr != NULL){
			m_pDrawObjMgr->OnRButtonDown(nFlags, point);

			CMenu menu;

			menu.LoadMenuW(IDR_RIGHTCLICK_MENU);


			if (m_pDrawObjMgr->GetSelectObj() == NULL){
				menu.EnableMenuItem(ID_PROPERTY_DELETE, MF_GRAYED | MF_DISABLED | MF_BYCOMMAND);
				menu.EnableMenuItem(ID_PROPERTY_PROPERTY, MF_GRAYED | MF_DISABLED | MF_BYCOMMAND);

				// RGB MEASUREMENT
				// ============================================================================
				menu.EnableMenuItem(ID_PROPERTY_RGBHISTOGRAM, MF_GRAYED | MF_DISABLED | MF_BYCOMMAND);
				// ============================================================================
			}

			else if (m_pDrawObjMgr->GetSelectObj()->GetObjType() != LABEL)
				menu.EnableMenuItem(ID_PROPERTY_PROPERTY, MF_GRAYED | MF_DISABLED | MF_BYCOMMAND);

			// RGB MEASUREMENT
			// ============================================================================
			if (m_pDrawObjMgr->GetSelectObj() != NULL)
			{
				if (m_pDrawObjMgr->GetSelectObj()->GetObjType() != MEASURE_AREA)
					menu.EnableMenuItem(ID_PROPERTY_RGBHISTOGRAM, MF_GRAYED | MF_DISABLED | MF_BYCOMMAND);
			}
			// ============================================================================

			CMenu *p_sub_menu = menu.GetSubMenu(0);

			CPoint pos;
			GetCursorPos(&pos);

			int menuId = p_sub_menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, pos.x, pos.y, this);

			CRect rect;

			int deltaX, deltaY;

			switch (menuId)
			{
			case ID_PROPERTY_BRINGTOCENTER:

				GetWindowRect(rect);

				deltaX = point.x - app->x_offset;
				deltaY = point.y - app->y_offset;

				//app->set_scroll_pos(deltaX, deltaY, false);
				app->bring_to_center(deltaX, deltaY);

				break;
			case ID_PROPERTY_DELETE:
				m_pDrawObjMgr->DeleteObj();
				app->UpdateViewContent();
				break;
			case ID_PROPERTY_PROPERTY:
				m_pDrawObjMgr->SetLabelProperty();
				break;

				// RGB MEASUREMENT
				// ============================================================================
			case ID_PROPERTY_RGBHISTOGRAM:
				m_pDrawObjMgr->RGBHistogram(m_pDrawObjMgr->m_pActiveObj);
				break;
				// ============================================================================

			}

			/*if (m_pDrawObjMgr->GetSelectObj())
				m_pDrawObjMgr->GetSelectObj()->setStatus(m_pDrawObjMgr->GetSelectObj()->NORMAL);*/
		}
	}
  //if (focusing)
  //  cancel_focus_	drag();
	/*
	if (m_bMeasuring)
	{
		CDrawTool* pTool = CDrawTool::FindTool(CDrawTool::c_drawShape);
		if (pTool != NULL)
			pTool->OnRButtonDown(this, nFlags, point);
	}
	*/
}

/******************************************************************************/
/*                           CChildView::OnLButtonUp                          */
/******************************************************************************/
HRESULT Imaging_SaveToFile(HBITMAP handle, LPCWSTR filename, LPCSTR format, float ImageQuality = 1);
void
  CChildView::OnLButtonUp(UINT nFlags, CPoint point) 
{	
	if (m_pToolPlugInsMgr) {
		ReleaseCapture();
		if (m_pToolPlugInsMgr->OnLButtonUp(GetSafeHwnd(), nFlags, point) != 0) {
			return;
		}
	}
	
	if (cropping)
	{
		cropped = true;
		cropping = false;
		
		CPoint cp;
		cp.SetPoint(crop_point.x, crop_point.y);
		CPoint pt;
		pt.SetPoint(point.x, point.y);

		int real_x0 = cp.x - app->x_offset;
		int real_y0 = cp.y - app->y_offset;

		int real_x1 = pt.x - app->x_offset;
		int real_y1 = pt.y - app->y_offset;

		if (real_x0 < 0)
			cp.x -= real_x0;
		if (real_y0 < 0)
			cp.y -= real_y0;
		if (real_x1 < 0)
			pt.x -= real_x1;
		if (real_y1 < 0)
			pt.y -= real_y1;

		if (real_x0 > app->view_dims.size.x)
			cp.x = app->image_dims.size.x + app->x_offset;
		if (real_y0 > app->view_dims.size.y)
			cp.y = app->image_dims.size.y + app->y_offset;
		if (real_x1 > app->view_dims.size.x)
			pt.x = app->image_dims.size.x + app->x_offset;
		if (real_y1 > app->view_dims.size.y)
			pt.y = app->image_dims.size.y + app->y_offset;

		cp.x -= app->x_offset;
		cp.y -= app->y_offset;
		pt.x -= app->x_offset;
		pt.y -= app->y_offset;

		int swap;

		if (pt.x < cp.x){
			swap = pt.x;
			pt.x = cp.x;
			cp.x = swap;
		}

		if (pt.y < cp.y){
			swap = pt.y;
			pt.y = cp.y;
			cp.y = swap;
		}

		POINTF pt1 = m_pDrawObjMgr->GetSelectObj()->ConvertRelativePoint(cp);
		POINTF pt2 = m_pDrawObjMgr->GetSelectObj()->ConvertRelativePoint(pt);
				
		CExpenderDlg ExpanderDlg;
		ExpanderDlg.SetCropImageInfo(app, app->GetOpenPathName(), app->m_config.getResolution().width, app->m_config.getResolution().height,
									pt1, pt2, app->max_compositing_layer_idx, app->single_layer_idx);

		ExpanderDlg.DoModal();
		cropped = false;
		app->RestoreViewRegion();
		UpdateViewRegion();
		ReleaseCapture();
		SetCursor(normal_cursor);
		return;
	}

	  if (m_pDrawObjMgr){
		  ReleaseCapture();		  
		  if (m_pDrawObjMgr->OnLButtonUp(nFlags, point)){
			  if (m_pMeasureTable != NULL)
			  {
				  updateMeasurementTableData();
				  m_pMeasureTable->OnBnClickedRefresh();
			  }
			  if (m_pRGBMeasureTable != NULL)
			  {
				  updateRGBMeasurementTableData();
				  m_pRGBMeasureTable->OnRefresh();
			  }
			  return;
		  }
	  }

	if (panning){
      SetCursor(normal_cursor);
      panning = false;
	  ReleaseCapture();
    }
	
    CWnd ::OnLButtonUp(nFlags, point);
}

/******************************************************************************/
/*                        CChildView::cancel_focus_drag                       */
/******************************************************************************/

void
  CChildView::cancel_focus_drag()
{
  if (!focusing)
    return;
  SIZE border; border.cx = border.cy = 2;
  SIZE zero_border; zero_border.cx = zero_border.cy = 0;
  CDC *dc = GetDC();
  dc->DrawDragRect(&focus_rect,zero_border,&focus_rect,border);
  ReleaseDC(dc);
  focusing = false;
  app->suspend_processing(false);
  ReleaseCapture();
  SetCursor(normal_cursor);
}

/******************************************************************************/
/*                           CChildView::OnMouseMove                          */
/******************************************************************************/

void
  CChildView::OnMouseMove(UINT nFlags, CPoint point) 
{
  last_mouse_point.x = point.x;
  last_mouse_point.y = point.y;

  //SetFocus();

  if (m_pToolPlugInsMgr){
	  if (m_pToolPlugInsMgr->OnLButtonMove(GetSafeHwnd(), nFlags, point) != 0){
		  return;
	  }
  }

  if (cropping)
  {
	  CPoint cp;
	  cp.SetPoint(crop_point.x, crop_point.y);
	  CPoint pt;
	  pt.SetPoint(point.x, point.y);

	  int real_x0 = cp.x - app->x_offset;
	  int real_y0 = cp.y - app->y_offset;

	  int real_x1 = pt.x - app->x_offset;
	  int real_y1 = pt.y - app->y_offset;

	  if (real_x0 < 0)
		  cp.x -= real_x0;
	  if (real_y0 < 0)
		  cp.y -= real_y0;
	  if (real_x1 < 0)
		  pt.x -= real_x1;
	  if (real_y1 < 0)
		  pt.y -= real_y1;

	  if (real_x0 > app->view_dims.size.x)
		  cp.x = app->image_dims.size.x + app->x_offset;
	  if (real_y0 > app->view_dims.size.y)
		  cp.y = app->image_dims.size.y + app->y_offset;
	  if (real_x1 > app->view_dims.size.x)
		  pt.x = app->image_dims.size.x + app->x_offset;
	  if (real_y1 > app->view_dims.size.y)
		  pt.y = app->image_dims.size.y + app->y_offset;

	  mouse_point.x = pt.x;
	  mouse_point.y = pt.y;

	  UpdateViewRegion();
	  return;
  }
  if (m_pDrawObjMgr){
	  if (m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_ROI || 
		  m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_MEASURE_LINE || 
		  m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_MEASURE_CURVE || 
		  m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_MEASURE_AREA)
			SetCursor(cross_cursor);

	  if (m_pDrawObjMgr->OnMouseMove(nFlags, point)) return;
  }
  
  if (nFlags & MK_CONTROL)
    {
      if (panning)
        {
          SetCursor(normal_cursor);
          panning = false;
        }
      }
  else if (panning)
    {
      SetCursor(panning_cursor);
      if (app != NULL)
        {
          app->set_scroll_pos(mouse_pressed_point.x-point.x, mouse_pressed_point.y-point.y, true);
          mouse_pressed_point = point;
        }
    }
}

/******************************************************************************/
/*                          CChildView::OnKillFocus                           */
/******************************************************************************/

void
  CChildView::OnKillFocus(CWnd* pNewWnd) 
{
  CWnd ::OnKillFocus(pNewWnd);

  if (panning)
    {
      SetCursor(normal_cursor);
      panning = false;
    }
}

/******************************************************************************/
/*                         CChildView::OnMouseActivate                        */
/******************************************************************************/

int
  CChildView::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
  if (this != GetFocus())
    { // Mouse click is activating window; prevent further mouse messages
      CWnd::OnMouseActivate(pDesktopWnd,nHitTest,message);
      return MA_ACTIVATEANDEAT;
    }
  else
    return CWnd::OnMouseActivate(pDesktopWnd,nHitTest,message);
}

/******************************************************************************/
/*                         CChildView::OnMouseWheel                           */
/******************************************************************************/

BOOL
  CChildView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
  if (app == NULL)
    return false;
  if ( nFlags & MK_CONTROL )
  {
	  if (zDelta > 0 )
		  app->OnImageNext();
	  else
		  app->OnImagePrev();
  }
  else  //Zoom
  {
	  
	  if (zDelta > 0)
		  app->OnViewZoomIn();
	  else
		  app->OnViewZoomOut();
	  if (m_pToolPlugInsMgr){
		  POINTF p = m_pDrawObjMgr->GetGridObjList()[0]->GetCenter();
		  if ((p.x > 0) && (p.y > 0)){
			  TRACE("center : %f, %f\n", p.x, p.y);
			  app->set_center_pos(p.x, p.y, app->rendering_scale);
			  UpdateViewRegion();
		  }
	  }
  }


  return TRUE;
}

void CChildView::OnDelete()
{
	if (app != NULL)
	{
		app->m_DrawObjMgr.DeleteObj();
	}
	if (m_pMeasureTable != NULL)
	{
		updateMeasurementTableData();
		m_pMeasureTable->OnBnClickedRefresh();
	}
	if (m_pRGBMeasureTable != NULL)
	{
		updateRGBMeasurementTableData();
		m_pRGBMeasureTable->OnRefresh();
	}
	app->UpdateViewContent();
	app->m_pOverview->m_roisTab.UpdateROIList();

}

void CChildView::OnScoopLeft()
{
	if (app!=NULL && !app->m_Analyzing) 
	{
		app->set_scroll_pos(-scroll_step.x, 0, true);
	}
}


void CChildView::OnScoopRight()
{
	if (app != NULL && !app->m_Analyzing)
	{
		app->set_scroll_pos(scroll_step.x, 0, true);
	}

}


void CChildView::OnScoopDown()
{
	if (app != NULL && !app->m_Analyzing)
	{
		app->set_scroll_pos(0, scroll_step.y, true);
	}

}


void CChildView::OnScoopUp()
{
	// TODO: Add your command handler code here
	if (app != NULL && !app->m_Analyzing)
	{
		app->set_scroll_pos(0, -scroll_step.y, true);
	}
}


void CChildView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CWnd::OnRButtonUp(nFlags, point);
}


void CChildView::OnEditCopyviewtoclipboard()
{
	CDC         memDC;
	CClientDC   dc(this);
	CBitmap     bitmap;

	RECT rc;
	GetClientRect(&rc);
	// Create memDC
	memDC.CreateCompatibleDC(&dc);

	bitmap.CreateCompatibleBitmap(&dc, rc.right, rc.bottom);
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);

	// Fill in memDC
	memDC.BitBlt(0, 0, rc.right, rc.bottom, &m_BackbufferDC, 0, 0, SRCCOPY);

	// Copy contents of memDC to clipboard
	OpenClipboard();
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, bitmap.GetSafeHandle());
	CloseClipboard();

	memDC.SelectObject(pOldBitmap);
	bitmap.Detach();
}


void CChildView::OnEditExportASJPEG()
{
	CString m_strPath;
	CStdioFile file;
	// CFile file;
	CFileException ex;

	kdu_uint32 *buf;
	int row_gap;
	HBITMAP bitmap = app->view_buffer->access_bitmap(buf, row_gap);

	m_strPath.Format(L"%s_%0.2fx_%d_%d.jpg", app->open_filename, app->rendering_scale, app->view_dims.pos.x, app->view_dims.pos.y);
	CFileDialog dlg(FALSE, _T("*.jpg"), m_strPath, OFN_OVERWRITEPROMPT, _T("JPEG Images(*.jpg)|*.jpg|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".jpg")
		{
			m_strPath += ".jpg";
		}
		
		CoUninitialize();
		Imaging_SaveToFile(bitmap, m_strPath, "jpeg");
		CoInitialize(NULL);
	}
}

void CChildView::OnEditCropImage()
{
	SetCursor(cross_cursor);
	crop = true;
	cropping = false;
	SetFocus();
	UpdateViewRegion();
	return;	
}

void CChildView::OnAnalyzeImage()
{
	if (analyze)
		m_pDrawObjMgr->ResetSelectMode();
	else
		m_pDrawObjMgr->SetCurStatus(CDrawObjMgr::GRID_ANALYZE);
	
	analyze = !analyze;

	SetFocus();
	return;
}



void CChildView::DocToClient(CPoint& point)
{
	CClientDC dc(this);
	//OnPrepareDC(&dc, NULL);
	dc.LPtoDP(&point);
}

void CChildView::DocToClient(CRect& rect)
{
	CClientDC dc(this);
	//OnPrepareDC(&dc, NULL);
	dc.LPtoDP(rect);
	rect.NormalizeRect();
}

void CChildView::ClientToDoc(CPoint& point)
{
	CClientDC dc(this);
	//OnPrepareDC(&dc, NULL);
	dc.DPtoLP(&point);
}

void CChildView::ClientToDoc(CRect& rect)
{
	CClientDC dc(this);
	//OnPrepareDC(&dc, NULL);
	dc.DPtoLP(rect);
	ASSERT(rect.left <= rect.right);
	//ASSERT(rect.bottom <= rect.top);
}

void CChildView::ClientToFullImage(CPoint& point)
{
	point.x = (int)((float)(point.x - app->x_offset + app->view_dims.pos.x) / app->rendering_scale);
	point.y = (int)((float)(point.y - app->y_offset + app->view_dims.pos.y) / app->rendering_scale);
}

void CChildView::ClientToFullImage(CRect& rect)
{
	rect.left = (int)((float)(rect.left - app->x_offset + app->view_dims.pos.x) / app->rendering_scale);
	rect.right = (int)((float)(rect.right - app->x_offset + app->view_dims.pos.x) / app->rendering_scale);
	rect.top = (int)((float)(rect.top - app->y_offset + app->view_dims.pos.y) / app->rendering_scale);
	rect.bottom = (int)((float)(rect.bottom - app->y_offset + app->view_dims.pos.y) / app->rendering_scale);
}

void CChildView::FullImageToClient(CPoint& point)
{
	point.x = (int)(point.x * app->rendering_scale) + app->x_offset - app->view_dims.pos.x;
	point.y = (int)(point.y * app->rendering_scale) + app->y_offset - app->view_dims.pos.y;
}

void CChildView::FullImageToClient(CRect& rect)
{
	rect.left = (int)(rect.left * app->rendering_scale) + app->x_offset - app->view_dims.pos.x;
	rect.right = (int)(rect.right * app->rendering_scale) + app->x_offset - app->view_dims.pos.x;
	rect.top = (int)(rect.top * app->rendering_scale) + app->y_offset - app->view_dims.pos.y;
	rect.bottom = (int)(rect.bottom * app->rendering_scale) + app->y_offset - app->view_dims.pos.y;
}


/*
void CChildView::OnUpdate(LPARAM lHint, CObject* pHint)
{
	switch (lHint)
	{
	case HINT_UPDATE_WINDOW:    // redraw entire window
		Invalidate(FALSE);
		break;

	case HINT_UPDATE_DRAWOBJ:   // a single object has changed
		InvalObj((CDrawObj*)pHint);
		break;

	case HINT_UPDATE_SELECTION: // an entire selection has changed
		{
			CDrawObjList* pList = pHint != NULL ?
				(CDrawObjList*)pHint : &m_selection;
			POSITION pos = pList->GetHeadPosition();
			while (pos != NULL)
				InvalObj(pList->GetNext(pos));
		}
		break;

	case HINT_DELETE_SELECTION: // an entire selection has been removed
		if (pHint != &m_selection)
		{
			CDrawObjList* pList = (CDrawObjList*)pHint;
			POSITION pos = pList->GetHeadPosition();
			while (pos != NULL)
			{
				CDrawObj* pObj = pList->GetNext(pos);
				InvalObj(pObj);
				Remove(pObj);   // remove it from this view's selection
			}
		}
		break;


	default:
		ASSERT(FALSE);
		break;
	}
}

void CChildView::Remove(CDrawObj* pObj)
{
	POSITION pos = m_selection.Find(pObj);
	if (pos != NULL)
		m_selection.RemoveAt(pos);
}

void CChildView::Deselect(CDrawObj* pObj)
{
	POSITION pos = m_selection.Find(pObj);
	if (pos != NULL)
	{
		InvalObj(pObj);
		m_selection.RemoveAt(pos);
	}
}

void CChildView::CloneSelection()
{
	POSITION pos = m_selection.GetHeadPosition();
	while (pos != NULL)
	{
		CDrawObj* pObj = m_selection.GetNext(pos);
		pObj->Clone(pObj->m_pDocument);
			// copies object and adds it to the document
	}
}
void CChildView::InvalObj(CDrawObj* pObj)
{
	CRect rect = pObj->m_position;
	//DocToClient(rect); // MJY comment out
	//CRect rect = CRect(pObj->left, pObj->top, pObj->right, pObj->bottom);
	//FullImageToClient(rect);
	TRACE("CChildView::InvalObj ... rect : %d %d %d %d\n", rect.left, rect.top, rect.right, rect.bottom);
	if (IsSelected(pObj))
	{
		// MJY_042109 : invalidate handle and text
		CPoint point = CPoint(rect.left-3, rect.top-3);
		SIZE	size;
		size.cx = 7; size.cy = 7;
		CRect handleRect = CRect(point, size);
		InvalidateRect(handleRect, FALSE);

		point = CPoint(rect.right-3, rect.bottom-3);
		handleRect = CRect(point, size);
		InvalidateRect(handleRect, FALSE);

		//for string... later..
		point = rect.CenterPoint();
		size.cx = 300;
		size.cy = 30;
		handleRect = CRect(point, size);
		InvalidateRect(handleRect, FALSE);

	}
	//rect.InflateRect(1, 1); // handles CDrawOleObj objects

	InvalidateRect(rect, FALSE);
}

BOOL CChildView::IsSelected(const CObject* pDocItem) const
{
	CDrawObj* pDrawObj = (CDrawObj*)pDocItem;
	//if (pDocItem->IsKindOf(RUNTIME_CLASS(CDrawItem)))
	//	pDrawObj = ((CDrawItem*)pDocItem)->m_pDrawObj;
	return m_selection.Find(pDrawObj) != NULL;
}
*/

void CChildView::OnMeasurementSelect()
{
	if (!m_pDrawObjMgr) return;

	m_pDrawObjMgr->ResetSelectMode();
	SetCursor(normal_cursor);
	SetFocus();
}


void CChildView::OnMeasurementLine()
{
	if (!m_pDrawObjMgr) return;

	if (m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_MEASURE_LINE)
	{
		OnMeasurementSelect();
		return;
	}

	char strTemp[32];
	size_t index = m_pDrawObjMgr->GetMeasureObjList().size();
	sprintf(strTemp, "Measure Line %02d", index);
	
	m_pDrawObjMgr->AddObj(MEASURE_LINE, BLUE, strTemp);
	SetFocus();
}

void CChildView::OnMeasurementCurve()
{
	if (!m_pDrawObjMgr) return;

	if (m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_MEASURE_CURVE)
	{
		OnMeasurementSelect();
		return;
	}

	char strTemp[32];
	size_t index = m_pDrawObjMgr->GetMeasureObjList().size();
	sprintf(strTemp, "Measure Curve %02d", index);

	m_pDrawObjMgr->AddObj(MEASURE_CURVE, RED, strTemp);
	SetFocus();
}

void CChildView::OnMeasurementArea()
{
	if(!m_pDrawObjMgr) return;

	if (m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_MEASURE_AREA)
	{
		OnMeasurementSelect();
		return;
	}

	char strTemp[32];
	size_t index = m_pDrawObjMgr->GetMeasureObjList().size();
	sprintf(strTemp, "Measure Curve %02d", index);

	m_pDrawObjMgr->AddObj(MEASURE_AREA, GREEN, strTemp);
	SetFocus();
}

void CChildView::OnMeasurementLabel()
{
	if (!m_pDrawObjMgr) return;

	if (m_pDrawObjMgr->GetStatus() == m_pDrawObjMgr->NEW_LABEL)
	{
		OnMeasurementSelect();
		return;
	}

	char strTemp[32];
	size_t index = m_pDrawObjMgr->GetLableList().size();
	sprintf(strTemp, "Label %02d", index);
	m_pDrawObjMgr->AddObj(LABEL, BLOCK, strTemp);
	SetFocus();
}


void CChildView::OnUpdateMeasurementSelect(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_pDrawObjMgr->NONE == m_pDrawObjMgr->GetStatus());
}


void CChildView::OnUpdateMeasurementLine(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck( m_pDrawObjMgr->NEW_MEASURE_LINE == m_pDrawObjMgr->GetStatus());
//	pCmdUI->SetRadio(CDrawTool::c_drawShape == line);
}

void CChildView::OnUpdateMeasurementCurve(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_pDrawObjMgr->NEW_MEASURE_CURVE == m_pDrawObjMgr->GetStatus());
//	pCmdUI->SetRadio(CDrawTool::c_drawShape == curve);
}

void CChildView::OnUpdateMeasurementArea(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_pDrawObjMgr->NEW_MEASURE_AREA == m_pDrawObjMgr->GetStatus());
//	pCmdUI->SetRadio(CDrawTool::c_drawShape == area);
}

void CChildView::OnUpdateMeasurementLabel(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_pDrawObjMgr->NEW_LABEL == m_pDrawObjMgr->GetStatus());
	//	pCmdUI->SetRadio(CDrawTool::c_drawShape == area);
}

void CChildView::OnEditClear()
{
	/*
	// TODO: Add your command handler code here
	// now remove the selection from the document
	POSITION pos = m_selection.GetHeadPosition();
	while (pos != NULL)
	{
		CDrawObj* pObj = m_selection.GetNext(pos);
		m_pDoc->Remove(pObj);
		pObj->Remove();
	}
	//Cleanup Tool members such as CPolyTool::m_pDrawObj, that should be NULL at this point.
	CDrawTool* pTool = CDrawTool::FindTool(CDrawTool::c_drawShape);
	if (pTool != NULL)
	{
		pTool->OnCancel();
	}
	m_selection.RemoveAll();

	Invalidate();

	if (m_pMeasureTable != NULL)
		m_pMeasureTable->OnBnClickedRefresh();
		*/
}


void CChildView::OnMeasurementTable()
{
	if (m_pMeasureTable == NULL)
		m_pMeasureTable = new CMeasureTable(this);

	m_pMeasureTable->SetApp(app, m_pDrawObjMgr);

	updateMeasurementTableData();
	m_pMeasureTable->ShowWindow(SW_SHOW);
}


void CChildView::updateMeasurementTableData()
{
	if (m_pDrawObjMgr == NULL) return;

	std::vector<CMeasureObj*>& MeasureList = m_pDrawObjMgr->GetMeasureObjList();

	for (int i = 1; i < MeasureList.size(); i++)
	{
		if (MeasureList[i - 1] == MeasureList[i])
			MeasureList.erase(MeasureList.begin() + i);
	}

	int i;
	for (i = 0; i < MeasureList.size(); i++){
		MeasureInfo *pMeasureInfo = MeasureList[i]->getInfo();

		_swprintf(m_pMeasureTable->itemText[i][0], L"%d", i + 1);
		if (pMeasureInfo->type == MEASURE_LINE){
			_swprintf(m_pMeasureTable->itemText[i][1], L"%s", L"Line");
			MeasureList[i]->getLineLengthStr(m_pMeasureTable->itemText[i][2]);
			MeasureList[i]->getUnit(m_pMeasureTable->itemText[i][3]);
		}
		else if (pMeasureInfo->type == MEASURE_CURVE){
			_swprintf(m_pMeasureTable->itemText[i][1], L"%s", L"Curve");
			MeasureList[i]->getLineLengthStr(m_pMeasureTable->itemText[i][2]);
			MeasureList[i]->getUnit(m_pMeasureTable->itemText[i][3]);
		}
		else if (pMeasureInfo->type == MEASURE_AREA){
			_swprintf(m_pMeasureTable->itemText[i][1], L"%s", L"Area");
			MeasureList[i]->getAreaStr(m_pMeasureTable->itemText[i][2]);
			MeasureList[i]->getUnit(m_pMeasureTable->itemText[i][3]);
			
		}
		else 
			_swprintf(m_pMeasureTable->itemText[i][1], L"%s", L"None");

	}

	for (; i<100; i++){
		m_pMeasureTable->itemText[i][0][0] = '\0';
		m_pMeasureTable->itemText[i][1][0] = '\0';
		m_pMeasureTable->itemText[i][2][0] = '\0';
		m_pMeasureTable->itemText[i][3][0] = '\0';
	}
}

void CChildView::updateRGBMeasurementTableData()
{
	if (m_pDrawObjMgr == NULL) return;

	std::vector<CMeasureObj*>& MeasureList = m_pDrawObjMgr->GetMeasureObjList();
	int i;
	int count = 0;
	for (i = 0; i < MeasureList.size(); i++){
		MeasureInfo *pMeasureInfo = MeasureList[i]->getInfo();

		if (pMeasureInfo->type == MEASURE_AREA){
			_swprintf(m_pRGBMeasureTable->itemText[count][0], L"%d", i+1);			
			_swprintf(m_pRGBMeasureTable->itemText[count][1], L"%d", pMeasureInfo->red);
			_swprintf(m_pRGBMeasureTable->itemText[count][2], L"%d", pMeasureInfo->green);
			_swprintf(m_pRGBMeasureTable->itemText[count][3], L"%d", pMeasureInfo->blue);
			count++;
		}

	}

	for (; count<100; count++){
		m_pRGBMeasureTable->itemText[count][0][0] = '\0';
		m_pRGBMeasureTable->itemText[count][1][0] = '\0';
		m_pRGBMeasureTable->itemText[count][2][0] = '\0';
		m_pRGBMeasureTable->itemText[count][3][0] = '\0';
	}
}


void CChildView::OnUpdateMeasurementTable(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(!m_bMeasureTable && app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_bMeasureTable);
}


void CChildView::OnMeasurementShow()
{
	// TODO: Add your command handler code here
	m_pDrawObjMgr->ToggleShowMeasureObj();
	UpdateViewRegion();
}


void CChildView::OnUpdateMeasurementShow(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_pDrawObjMgr->IsShowMeasureObj());
}


void CChildView::OnShowLabel()
{
	// TODO: Add your command handler code here
	m_pDrawObjMgr->ToggleShowLabelObj();
	UpdateViewRegion();
}


void CChildView::OnUpdateShowLabel(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_pDrawObjMgr->IsShowLabelObj());
	//	pCmdUI->SetRadio(CDrawTool::c_drawShape == area);
}

void CChildView::OnMeasurementScaleBar()
{
	m_pDrawObjMgr->ToggleShowScaleBarObj();
	UpdateViewRegion();
}


void CChildView::OnUpdateMeasurementScaleBar(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
  pCmdUI->SetCheck(m_pDrawObjMgr->IsShowScaleBarObj());
}

void CChildView::OnAutoUnit()
{
	CDrawObj::setAutoUnit(!CDrawObj::getAutoUnit());
	UpdateViewRegion();
}

void CChildView::OnUpdateAutoUnit(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(CDrawObj::getAutoUnit());
}

void CChildView::OnShowToolbar()
{
	app->frame_wnd->ShowControlBar(app->frame_wnd->get_tool_bar(), !app->frame_wnd->get_tool_bar()->IsVisible(), FALSE);
}

void CChildView::OnUpdateToolbar(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(false);
	pCmdUI->SetCheck(CDrawObj::getAutoUnit());
}

void CChildView::OnViewFullscreen()
{
	// TODO: Add your command handler code here
	if(!m_bFullScreen && !app->m_Analyzing)
	{
		m_bFullScreen=true;
		m_ParentWnd=this->GetParent();
		this->SetParent(GetDesktopWindow());
		CRect rect;
		GetDesktopWindow()->GetWindowRect(&rect);
		this->SetWindowPos(&wndTopMost,rect.left,rect.top,rect.right,rect.bottom,SWP_SHOWWINDOW);
		check_and_report_size();
	}
	else
	{
		m_bFullScreen=false;
		this->SetParent( m_ParentWnd);
		((CMainFrame *)AfxGetMainWnd())->RecalcLayout();
		check_and_report_size();
	}
}

void CChildView::OnViewBackfromfull()
{
	// TODO: Add your command handler code here
	if (m_bFullScreen) OnViewFullscreen();
}


void CChildView::OnUpdateEditCopyviewtoclipboard(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
  pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
}

//MJY_041309
void CChildView::OnUpdateViewFullscreen(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	//pCmdUI->Enable(false);
}

void CChildView::OnUpdateEditCropImage(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
}

void CChildView::OnUpdateAnalyzeImage(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && m_pDrawObjMgr->IsShowGridObj() && !app->m_Analyzing);
	pCmdUI->SetCheck(analyze);
}

//MJY_042709
void CChildView::OnMeasurementRefreshtable()
{
	// TODO: Add your command handler code here
	if (m_pMeasureTable != NULL)
	{
		updateMeasurementTableData();
		m_pMeasureTable->m_listCtrl.Invalidate();
	}
	if (m_pRGBMeasureTable != NULL)
	{
		updateRGBMeasurementTableData();
		m_pRGBMeasureTable->m_ListControl.Invalidate();
	}

}


BOOL CChildView::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	{
		OnMeasurementSelect();
	}

	return CWnd::PreTranslateMessage(pMsg);
}


void CChildView::OnMeasurementDeleteall()
{
	// TODO: Add your command handler code here
	int ans = AfxMessageBox(L"Delete All Measurements?", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON1 | MB_APPLMODAL);
	if (ans == IDYES)
	{
		m_pDrawObjMgr->DeleteAll();
		if (m_pMeasureTable != NULL)
		{
			updateMeasurementTableData();
			m_pMeasureTable->OnBnClickedRefresh();
		}
		if (m_pRGBMeasureTable != NULL)
		{
			updateRGBMeasurementTableData();
			m_pRGBMeasureTable->OnRefresh();
		}
	}
}


void CChildView::OnUpdateMeasurementDeleteall(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
}


void CChildView::OnMeasurementRgbTable()
{
	// TODO: Add your command handler code here
	if (m_pRGBMeasureTable == NULL)
		m_pRGBMeasureTable = new CRGBMeasureTable(this);
	
	m_pRGBMeasureTable->SetApp(app, m_pDrawObjMgr);

	updateRGBMeasurementTableData();
	m_pRGBMeasureTable->ShowWindow(SW_SHOW);
}


void CChildView::OnUpdateMeasurementRgbTable(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(app->buffer != NULL && !app->m_Analyzing);
	pCmdUI->SetCheck(m_bRGBMeasureTable);
}
