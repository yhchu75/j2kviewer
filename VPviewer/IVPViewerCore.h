#pragma once

#include <vector>

typedef enum { CELL_STATUS_NONE = -1, CELL_STATUS_SELECT, CELL_STATUS_NONSELECT} eGridCellInfo;

typedef struct {
	eGridCellInfo status;
	POINTF	pt;
	int		pos_type;
	DWORD	color;
	DWORD	info;
}GridCellInfo;

class IVPViewerCore
{
public:
	typedef enum { AnalyzeMode_NONE, AnalyzeMode_GRID, AnalyzeMode_SECTOR }eAnalyzeMode;
	typedef enum { SectorMode_RegionDraw, SectorMode_SectorAnalyze	}eSectorRegionMode;
	typedef enum { SectorMode_RegionDrawModeAll, SectorMode_RegionDrawModeSelect}eSectorRegionDrawMode;
	typedef enum { SectorRegionType_NONE, SectorRegionType_RECT, SectorRegionType_ELLISPE, SectorRegionType_FREE }eSectorRegionType;

	virtual CDC* GetDC() = 0;
	virtual int	ZoomByIndex(int id) = 0;
	virtual eAnalyzeMode	GetAnalyzeMode() = 0;
	virtual float GetScale() = 0;
	
	virtual void	SetAnalyzeMode(eAnalyzeMode mode) = 0;
	virtual void	GridOnButtonMove(UINT nFlags, CPoint point) = 0;
	virtual void	GridOnButtonDown(UINT nFlags, CPoint point) = 0;
	virtual void	GridOnButtonUp(UINT nFlags, CPoint point) = 0;
	virtual void	GetGridBox(POINT &pos, POINT &size) = 0;
	virtual void	SetGridBox(POINT pos, POINT size) = 0;
	virtual void	SetCenterPos(POINT p) = 0;
	
	virtual void	SetGridBoxArray(int *counterArray) = 0;
	virtual void	SetGridCrossHair(bool draw) = 0;
	virtual void	SetGridRedCrossHair(bool draw) = 0;

	virtual void	ShowGrid(bool bShow) = 0;
	virtual TCHAR*	GetFileName() = 0;
	virtual bool	CropGridImage(POINT pt, const TCHAR* filename, int width = 1 , int height = 1) = 0;
	virtual int		GetRotate() = 0;
	virtual int		GetInterval() = 0;
	virtual void	SetGridProperty(int rotation, float scale, int interval) = 0;
	virtual RECT	GetFrameRect() = 0;

	virtual void	ShowSector(bool bShow) = 0;
	virtual void	SetGridRatio(int ratio) = 0;
	virtual void	SetSectorZoomLevel(int ZoomLevel) = 0;
	virtual void	SetSectorRegionMode(eSectorRegionMode mode) = 0;
	virtual void	SetSectorRegionDrawMode(eSectorRegionDrawMode mode) = 0;
	virtual void	AddSectorRegion(eSectorRegionType type) = 0;

	virtual int		GetRegionCount() = 0;
	
	virtual std::vector<GridCellInfo>& GetSectorCellInfo() = 0;
	virtual int		MoveSector(int index, int init) = 0;

	virtual int 	SetSectorCellInfo(CPoint point, int fossil_index, DWORD color) = 0;
	virtual int 	SetSectorCellInfo(int cell_index, int fossil_index, DWORD color) = 0;

	virtual int 	GetTotalSectorCount() = 0;
	virtual int 	GetTotalCellCount() = 0;

	virtual void 	ResetAnalyze() = 0;

}; 

