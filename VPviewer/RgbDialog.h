#pragma once

#include "resource.h"

// CRgbDialog dialog

class CRgbDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CRgbDialog)

public:
	CRgbDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRgbDialog();

// Dialog Data
	enum { IDD = IDD_RGB_HISTOGRAM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
public:

	// RGB MEASUREMENT
	// ============================================================================
	unsigned int *m_arrRgbRed;
	unsigned int *m_arrRgbGreen;
	unsigned int *m_arrRgbBlue;
	// ============================================================================

	void SetData(unsigned int r_data[], unsigned int g_data[], unsigned int b_data[]);
	void DrawGraph(CPaintDC *dc, int x, int y, unsigned int data[], unsigned int max_x, const COLORREF rgb);
	unsigned int GetMaxBin(unsigned int data1[], unsigned int data2[], unsigned int data3[]);
	//CDrawObjMgr* m_pDrawManager;
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
};
