// drawdoc.h : interface of the CDrawDoc class
//

#include "drawobj.h"
#include <afxole.h>

class CChildView;

class CDrawDoc : public CObject
{
public: // create from serialization only
	CDrawDoc();
	//DECLARE_DYNCREATE(CDrawDoc)

// Attributes
public:
	CDrawObjList* GetObjects() { return &m_objects; }
	int GetMapMode() const { return m_nMapMode; }

// Operations
public:
	CDrawObj* ObjectAt(const CPoint& point, CChildView *pView);
	void Draw(CDC* pDC, CChildView* pView);
	void Add(CDrawObj* pObj);
	void Remove(CDrawObj* pObj);
	void Cleanup();

// Implementation
public:
	virtual ~CDrawDoc();
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:

	CDrawObjList m_objects;
	int m_nMapMode;

// Generated message map functions
protected:
	//{{AFX_MSG(CDrawDoc)
	//}}AFX_MSG
};

/////////////////////////////////////////////////////////////////////////////
