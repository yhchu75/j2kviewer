/*****************************************************************************/
// File: kdu_show.h [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/*****************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/******************************************************************************
Description:
   Defines the main application object for the interactive JPEG2000 viewer,
"kdu_show".
******************************************************************************/
#ifndef KDU_SHOW_H
#define KDU_SHOW_H

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#if !(defined _MSC_VER && (_MSC_VER >= 1300)) // If not .NET
#  define DWORD_PTR DWORD
#  define UINT_PTR kdu_uint32
#  define INT_PTR kdu_int32
#endif // Not .NET

#include "resource.h"       // main symbols
#include "kdu_messaging.h"
#include "kdu_compressed.h"
#include "kdu_file_io.h"
#include "jpx.h"
#include "kdu_client.h"
#include "kdu_clientx.h"
#include "kdu_region_compositor.h"
#include "MainFrm.h"
#include "kd_metashow.h"
#include "Overview.h"
#include "EditMetadata.h"
#include "Scaling.h"
#include "VPViewerConf.h"
#include "drawobjmgr.h"
#include "Grid.h"
#include "InnovaplexCustomHeader.h"
#include "FastPixel.h"


// OK to have "using" statement in this header because it is intended to
// be private to the "kdu_winshow" demo app.
using namespace kdu_supp; // Also includes the `kdu_core' namespace

// Declared here:
class kdws_string;
class kdws_settings;
class kdws_notification_manager;
class kdws_frame_presenter;
class kdws_client_notifier;
struct kdws_window_list;
struct kdws_open_file_record;
class kdws_manager;


/////////////////////////////////////////////////////////////////////////////
// CKdu_showApp:
// See kdu_show.cpp for the implementation of this class
//

enum kds_status_id {
    KDS_STATUS_LAYER_RES=0,
    KDS_STATUS_DIMS=1,
    KDS_STATUS_MEM=2,
    KDS_STATUS_CACHE=3
  };


/*****************************************************************************/
/*                                 kdu_notifier                              */
/*****************************************************************************/

class kdu_notifier : public kdu_client_notifier {
  public: // Member functions
    kdu_notifier() { wnd = NULL; }
    kdu_notifier(HWND wnd) { this->wnd = wnd; }
    void set_wnd(HWND wnd)
      { assert(this->wnd == NULL); this->wnd = wnd; }
    void notify()
      { // Pump the message queue to ensure idle time processing occurs
        if (wnd != NULL)
          PostMessage(wnd,WM_NULL,0,0);
      }
  private: // Data
    HWND wnd;
  };

/*****************************************************************************/
/*                                 kd_settings                               */
/*****************************************************************************/

class kd_settings {
  public: // Member functions
    kd_settings();
    ~kd_settings();
    void save_to_registry(CWinApp *app);
    void load_from_registry(CWinApp *app);
	const TCHAR *get_open_save_dir()
      {
        return ((open_save_dir==NULL)?_T(""):open_save_dir);
      }
    int get_open_idx() { return open_idx; }
    int get_save_idx() { return save_idx; }
	const TCHAR *get_jpip_server()
      {
		return ((jpip_server == NULL) ? _T("") : jpip_server);
      }
	const TCHAR *get_jpip_proxy()
      {
		return ((jpip_proxy == NULL) ? _T("") : jpip_proxy);
      }
	const TCHAR *get_jpip_cache()
      {
		return ((jpip_cache == NULL) ? _T("") : jpip_cache);
      }
	const TCHAR *get_jpip_request()
      {
		return ((jpip_request == NULL) ? _T("") : jpip_request);
      }
	const TCHAR *get_jpip_channel_type()
      {
		return ((jpip_channel == NULL) ? _T("") : jpip_channel);
      }
	const  TCHAR *get_jpip_login_type()
	{
		return ((jpip_login == NULL) ? _T("") : jpip_login);
	}
	const TCHAR *get_jpip_id()
	{
		return ((jpip_id == NULL) ? _T("") : jpip_id);
	}
	const TCHAR *get_jpip_pw()
	{
		return ((jpip_pw == NULL) ? _T("") : jpip_pw);
	}

	void set_open_save_dir(const TCHAR *string)
      {
        if (open_save_dir != NULL) delete[] open_save_dir;
		open_save_dir = new TCHAR[_tcslen(string) + 1];
        _tcscpy(open_save_dir,string);
      }
    void set_open_idx(int idx) {open_idx = idx; }
    void set_save_idx(int idx) {save_idx = idx; }
	void set_jpip_server(const TCHAR *string)
      {
        if (jpip_server != NULL) delete[] jpip_server;
        jpip_server = new TCHAR[_tcslen(string)+1];
        _tcscpy(jpip_server,string);
      }
	void set_jpip_proxy(const TCHAR *string)
      {
        if (jpip_proxy != NULL) delete[] jpip_proxy;
		jpip_proxy = new TCHAR[_tcslen(string) + 1];
        _tcscpy(jpip_proxy,string);
      }
	void set_jpip_cache(const TCHAR *string)
      {
        if (jpip_cache != NULL) delete[] jpip_cache;
		jpip_cache = new TCHAR[_tcslen(string) + 1];
        _tcscpy(jpip_cache,string);
      }
	void set_jpip_request(const TCHAR *string)
      {
        if (jpip_request != NULL) delete[] jpip_request;
		jpip_request = new TCHAR[_tcslen(string) + 1];
        _tcscpy(jpip_request,string);
      }
	void set_jpip_channel_type(const TCHAR *string)
      {
        if (jpip_channel != NULL) delete[] jpip_channel;
		jpip_channel = new TCHAR[_tcslen(string) + 1];
        _tcscpy(jpip_channel,string);
      }
	void set_jpip_login_type(const TCHAR *string)
	{
		if (jpip_login != NULL) delete[] jpip_login;
		jpip_login = new TCHAR[_tcslen(string) + 1];
		_tcscpy(jpip_login, string);
	}
	void set_jpip_id(const TCHAR *string)
	{
		if (jpip_id != NULL) delete[] jpip_id;
		jpip_id = new TCHAR[_tcslen(string) + 1];
		_tcscpy(jpip_id, string);
	}
	void set_jpip_pw(const TCHAR *string)
	{
		if (jpip_pw != NULL) delete[] jpip_pw;
		jpip_pw = new TCHAR[_tcslen(string) + 1];
		_tcscpy(jpip_pw, string);
	}


	bool is_use_cache;
	int  jpip_buffer_len;

  private: // Data
	  TCHAR *open_save_dir;
    int open_idx, save_idx;
    TCHAR *jpip_server;
	TCHAR *jpip_proxy;
	TCHAR *jpip_cache;
	TCHAR *jpip_request;
	TCHAR *jpip_channel;
	TCHAR *jpip_login;
	TCHAR *jpip_id;
	TCHAR *jpip_pw;
  };


/*****************************************************************************/
/*                               kd_bitmap_buf                               */
/*****************************************************************************/

class kd_bitmap_buf : public kdu_compositor_buf {
  public: // Member functions and overrides
    kd_bitmap_buf()
      { bitmap=NULL; next=NULL; }
    virtual ~kd_bitmap_buf()
      {
        if (bitmap != NULL) DeleteObject(bitmap);
        buf = NULL; // Remember to do this whenever overriding the base class
      }
    virtual void create_bitmap(kdu_coords size);
      /* If this function finds that a bitmap buffer is already available,
         it returns without doing anything. */
	virtual HBITMAP access_bitmap(kdu_uint32 * &buf, int &row_gap)
      { buf=this->buf; row_gap=this->row_gap; return bitmap; }

	void copy_bitmap( kd_bitmap_buf* src );

  public: // Additional data
    friend class kd_bitmap_compositor;
    HBITMAP bitmap; // NULL if not using a bitmap
    BITMAPINFO bitmap_info; // Description for `bitmap' if non-NULL
    kdu_coords size;
    kd_bitmap_buf *next;
  };

/*****************************************************************************/
/*                            kd_bitmap_compositor                           */
/*****************************************************************************/

class kd_bitmap_compositor : public kdu_region_compositor {
  /* This class augments the platorm independent `kdu_region_compositor'
     class with display buffer management logic which allocates system
     bitmaps.  You can take this as a template for designing richer
     buffer management systems -- e.g. ones which utilize display hardware
     through DirectDraw. */
  public: // Member functions
    kd_bitmap_compositor(kdu_thread_env *env=NULL,
                         kdu_thread_queue *env_queue=NULL)
                         : kdu_region_compositor(env,env_queue)
      { active_list = free_list = NULL; is_virtaul_mem = false;}
    ~kd_bitmap_compositor()
      {
        pre_destroy(); // Destroy all buffers before base destructor called
        assert(active_list == NULL);
        while ((active_list=free_list) != NULL)
          { free_list=active_list->next; delete active_list; }
      }
    virtual kdu_compositor_buf *
      allocate_buffer(kdu_coords min_size, kdu_coords &actual_size,
                      bool read_access_required);
    virtual void delete_buffer(kdu_compositor_buf *buffer);
    kd_bitmap_buf *get_composition_bitmap(kdu_dims &region, const TCHAR * map_file_name = NULL)
      {	
		  if (map_file_name){
			  is_virtaul_mem = true;
			  _tcscpy(m_map_file, map_file_name);
		  }
		  else{
			  is_virtaul_mem = false;
		  }
		  return (kd_bitmap_buf *) get_composition_buffer(region); }
      /* Use this instead of `kdu_compositor::get_composition_buffer'. */
  private: // Data
    kd_bitmap_buf *active_list;
    kd_bitmap_buf *free_list;
	bool is_virtaul_mem;
	TCHAR m_map_file[MAX_PATH];
  };

/*****************************************************************************/
/*                                 CKdu_showApp                              */
/*****************************************************************************/

class CKdu_showApp : public CWinApp, public IDrawObjNotifyReceive
{
public:
  CKdu_showApp();
  ~CKdu_showApp();

  enum{ SCALE_1X, SCALE_2X, SCALE_5X, SCALE_10X, SCALE_20X, SCALE_40X, SCALE_80X, MAX_RENDER_SCALE};
  enum{ MAX_WAIT_TIME = 5000};	//
  enum{ ANALYZE_MODE_NONE = 0, ANALYZE_MODE_V1, ANALYZE_MODE_V2_ALL, ANALYZE_MODE_V2_CUSTOME};
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKdu_showApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CKdu_showApp)
	afx_msg void OnAppAbout();
	afx_msg void OnRegisterFileExtension();
	afx_msg void OnFileClose();
	afx_msg void OnUpdateFileClose(CCmdUI* pCmdUI);
	afx_msg void OnFileOpen();
	afx_msg void OnViewZoomOut();
	afx_msg void OnUpdateViewZoomOut(CCmdUI* pCmdUI);
	afx_msg void OnViewZoomIn();
	afx_msg void OnUpdateViewZoomIn(CCmdUI* pCmdUI);
	afx_msg void OnViewRefresh();
		afx_msg void OnJpipOpen();
	afx_msg void OnImageNext();
	afx_msg void OnUpdateImageNext(CCmdUI* pCmdUI);
	afx_msg void OnImagePrev();
	afx_msg void OnUpdateImagePrev(CCmdUI* pCmdUI);
	afx_msg void OnCompositingLayer();
	afx_msg void OnUpdateCompositingLayer(CCmdUI* pCmdUI);
	afx_msg void OnRedisplayRotate(eRotateStatus degree);
	//}}AFX_MSG

	// NUMPAD
	// ============================================================================
	afx_msg void OnNumpadKey();
	// ============================================================================

	DECLARE_MESSAGE_MAP()
// ----------------------------------------------------------------------------
public: // Access methods for use by the window objects
  void open_file(TCHAR *filename);
    /* This function enters with `filename'=NULL if it is being called to
       complete an overlapped opening operation.  This happens with the
       "kdu_client" compressed data source, which must first be connected
       to a server and then, after some time has passed, becomes ready
       to actually open a code-stream or JP2-family data source. */
  void close_file();
  void suspend_processing(bool suspend);
    /* Call this function to suspend or resume idle time processing of
       compressed image data.  This is useful when drawing onto the display
       surface, since idle time processing may cause its own calls to
       `paint_region'. */
  void refresh_display();
    /* Called from `initialize_regions' and also from any point where the
       image needs to be decompressed again.  The function is public so
       that it can be invoked while processing the WM_TIMER message received
       by the frame window. */
  void flash_overlays();
    /* Called when the overlay flash timer goes off. */

  void set_view_size(kdu_coords size);
    /* This function should be called whenever the client view window's
       dimensions may have changed.  The function should be called at least
       whenever the WM_SIZE message is received by the client view window.
       The current object expects to receive this message before it can begin
       processing a newly opened image.  If `size' defines a larger region
       than the currently loaded image, the actual `view_dims' region used
       for rendering purposes will be smaller than the client view window's
       dimensions. */
  
  void set_scroll_pos(int rel_xpos, int rel_ypos, bool relative_to_last=false);
  bool validation_check(int rel_xpos, int rel_ypos);
  void bring_to_center(int rel_xpos, int rel_ypos);
    /* This is a combined scrolling function that allows scrolling in two
      directions simultaneously */
  HBITMAP update_region(kdu_dims& region);
  
  void set_center_pos(float center_x, float center_y, float scale);

void set_codestream(int codestream_idx);
    /* Called from `kd_metashow' when the user clicks on a code-stream box,
       this function switches to single component mode to display the
       image components of the indicated code-stream. */
  void set_compositing_layer(int layer_idx);
    /* Called from `kd_metashow' when the user clicks on a compositing layer
       header box, this function switches to single layer mode to display the
       indicated compositing layer. */
  void set_compositing_layer(kdu_coords point);
    /* This form of the function determines the compositing layer index
       based on the supplied location.  It searches for the top-most
       compositing layer which falls underneath the current mouse position
       (given by `point', relative to the upper left corner of the view
       window).  If none is found, no action is taken. */


  bool show_metainfo(kdu_coords point);
    /* This function is called when the mouse moves over the view window while
       the control key is held down.  It searches for spatially sensitive
       metadata which falls underneath the current mouse position (given by
       `point' relative to the upper left corner of the view window).  If
       none is found, the function returns false.  Otherwise, it returns true
       after first displaying any appropriate label information.  The caller
       uses the return value to adjust the cursor. */
  void kill_metainfo();
    /* Called when the control key is lifted, if the last call to
       `show_metainfo' returned true, this function removes any metadata
       labels which are currently being displayed. */
  bool edit_metainfo(kdu_coords point);
    /* Similar to `show_metainfo' except that the metadata is edited.  The
       function returns true if metadata editing was initiated, in which case
       the editing cursor should be switched off. */
private: // Private implementation functions
  float increase_scale(float from_scale);
    /* This function uses `kdu_region_compositor::find_optimal_scale' to
       find a suitable scale for rendering the current focus or view which
       is at least a little (e.g. 30%) larger than `from_scale'. */
  float decrease_scale(float to_scale);
    /* Same as `increase_scale' but decreases the scale by at least a
       fixed small fraction (e.g. 30%), unless a known minimum scale
       factor is encountered. */
  void change_frame_idx(int new_frame_idx);
    /* This function is used to manipulate the `frame', `frame_idx',
       `frame_iteration' and `frame_instant' members.  It does not do anything
       else. */
  public:
  void invalidate_configuration();
    /* Call this function if the compositing configuration parameters
       (`single_component_idx', `single_codestream_idx', `single_layer_idx')
       are changed in any way.  The function removes all compositing layers
       from the current configuration installed into the `compositor' object.
       It sets the `configuration_complete' flag to false, the `bitmap'
       member to NULL, the image dimensions to 0, and posts a new client
       request to the `kdu_client' object if required.   After calling this
       function, you should generally call `initialize_regions'. */
  void initialize_regions(bool bUpdateOverlay = false);
    /* Called by `open_file' after first opening an image (or JPIP client
       session) and also whenever the composition is changed (e.g., a change
       in the JPX compositing layers which are to be used, or a change to
       single-component mode rendering).  In the former case, `first_time'
       will be set to true, meaning that the function should attempt to select
       a scale which fits the image roughly to the display (or a typical
       display). */
  private:
  void calculate_view_anchors();
    /* Call this function to determine the centre of the current view relative
       to the entire image, the centre of the focus box, if any, relative
       to the entire image, and the dimensions of the focus box, if any,
       relative to the dimensions of the entire image.  The function sets
       `view_centre_known' and `focus_anchors_known' to true.  The anchor
       coordinates and dimensions are used by the next calls to
       `set_view_size' and `get_new_focus_box' calls to position the
       view port and anchor pox.  The most common use of this function is
       to preserve (or modify) the key properties  of the view port and
       focus box when the image is zoomed, rotated, flipped, or specialized
       to a different image component.  */
  void  display_status();
    /* Displays information in the status bar.  The particular type of
       information displayed depends upon the value of the `status_id'
       member variable. */
  bool check_one_time_request_compatibility();
    /* Checks the server response from a one-time JPIP request (if the
       response is available yet) to determine if it is compatible with the
       mode in which we are currently trying to open the image.  If so, the
       function returns true.  Otherwise, the function returns false after
       first downgrading the image opening mode.  If we are currently trying
       to open an animation frame, we downgrade the operation to one in which
       we are trying to open a single compositing layer.  If this is also
       incompatible, we may downgrade the opening mode to one in which we
       are trying to open only a single image component of a single
       codestream. */
  void update_client_window_of_interest();
    /* Called if the compressed data source is a "kdu_client"
       object, whenever the viewing region and/or resolution may
       have changed.  This function computes and passes information
       about the client's window of interest to the remote server. */
  void change_client_focus(kdu_coords actual_resolution,
                           kdu_dims actual_region);
    /* Called if the compressed data source is a "kdu_client" object,
       whenever the server's reply to a region of interest request indicates
       that it is processing a different region to that which was requested
       by the client.  The changes are reflected by redrawing the focus
       box, as necessary, while being careful not to issue a request for a
       new region of interest from the server in the process. */
  

public:
  CMainFrame *frame_wnd;
  CChildView *child_wnd; // Null until the frame is created.
  TCHAR *open_filename; // Points to the name part of `open_file_pathname'

private:
  CProgressCtrl *jpip_progress; // NULL unless JPIP progress in status bar

private: // Compressed data source and rendering machinery
  TCHAR open_file_pathname[MAX_PATH+1]; // Used to avoid overwriting file in use
  bool allow_seeking;
  int error_level; // 0 = fast, 1 = fussy, 2 = resilient, 3 = resilient_sop
  kdu_simple_file_source file_in;
  jp2_family_src jp2_family_in;
  jpx_source jpx_in;
  jpx_composition composition_rules; // Composition/rendering info, if available

public:
  kdu_client* jpip_client;
  kdu_cache cache_in; // If open, the cache is bound to a JPIP client
  int	client_request_queue_id;

private:
  kdu_clientx jpx_client_translator;
  kdu_notifier client_notifier; // For `client_input.install_notifier'.
  kd_settings settings; // Manages load/save and JPIP settings.
  kd_bitmap_compositor *compositor;
  kdu_thread_env thread_env; // Multi-threaded environment
  int num_threads; // If 0 use single-threaded machinery with `thread_env'=NULL
  int num_recommended_threads; // If 0, recommend single-threaded machinery
  bool in_idle; // Indicates we are inside the `OnIdle' function
  bool processing_suspended;

public:
  float rendering_scale; // Supplied to `kdu_region_compositor::set_scale'
  
private: // Modes and status which may change while the image is open
  int max_display_layers;
  bool transpose, vflip, hflip; // Current geometry flags.
  float min_rendering_scale; // Negative if unknown
  float rendering_scaleX[MAX_RENDER_SCALE];
    
  int single_component_idx; // Negative when performing a composite rendering
  int max_components; // -ve if not known
  int single_codestream_idx; // Used with `single_component'
  int max_codestreams; // -ve if not known

public:
  	
  int single_layer_idx; // For rendering one layer at a time; -ve if not in use
  int second_mix_layer_idx;	//for mix channel idx;
  int save_layer_idx;
  int max_compositing_layer_idx; // -ve if not known

private:
	int frame_idx; // Index of current frame
	double frame_start; // Frame start time (secs) w.r.t. track/animation start
	double frame_end; // Frame end time (secs) w.r.t. track/animation start
	jx_frame *frame; // NULL, if unable to open the above frame yet
	int frame_iteration; // Iteration within `frame' object, if repeated
	int num_frames; // -ve if not known
	jpx_frame_expander frame_expander; // Used to find frame membership

  bool configuration_complete; // If required compositing layers are installed
  bool fit_scale_to_window; // Used when opening file for first time
  kds_status_id status_id; // Info currently displayed in status window


public: // Image and viewport dimensions
  int pixel_scale; // Number of display pels per image pel
  kdu_dims image_dims; // Dimensions & location of complete composed image
  kdu_dims buffer_dims; // Uses same coordinate system as `image_dims'
  kdu_dims view_dims; // Uses same coordinate system as `image_dims'
  
  int		x_offset, y_offset; // MJY_092408
  kdu_dims	whole_image_dims; //MJY
  float		view_centre_x, view_centre_y;
  bool		view_centre_known;

private: // Focus parameters
  bool enable_focus_box; // If disabled, focus box is identical to view port.
  bool highlight_focus; // If true, focus region is highlighted.
  bool focus_anchors_known; // See `calculate_view_anchors'.
  float focus_centre_x, focus_centre_y, focus_size_x, focus_size_y;
  int focus_codestream, focus_layer; // If both -ve, focus is w.r.t. frame
  CPen focus_pen;
  kdu_dims focus_box; // Uses same coordinate system as `view_dims'.
  bool enable_region_posting; // True if focus changes can be sent to server.
  kdu_window client_roi; // Last posted/in-progress client-server window.
  kdu_window tmp_roi; // Temp work space to avoid excessive new/delete usage

private: // Overlay parameters
  bool overlays_enabled;
  bool overlay_flashing_enabled;
  int overlay_painting_intensity;
  int overlay_painting_colour;
  int overlay_size_threshold; // Min composited size for an overlay to be shown

public: // Image buffer and painting resources
  kd_bitmap_buf *buffer; // Get from `compositor' object; NULL if invalid, active_buffer
  kd_bitmap_buf *second_buffer; // temporary buffer for mix two image.
  kd_bitmap_buf *mix_buffer;	//for mix operation
  kd_bitmap_buf *view_buffer;	//for mix operation
  
public: 
	bool			m_first_open;
	CVPViewerConf	m_config;
	CDrawObjMgr		m_DrawObjMgr;
	CFastPixel		m_BackBufPixel;

	eRotateStatus	m_rotationStatus;
		
	bool			m_bMixing;		
	int				m_MixingRatio;	
	bool			m_Analyzing;

private: // Periodic event machinery
# define KDU_SHOW_REFRESH_TIMER_IDENT 10392 // Random identifier
# define KDU_SHOW_FLASH_TIMER_IDENT   10393 // ID for overlay flashing timer
# define KDU_SHOW_REQUEST_REFRESH_TIMER_IDENT   10394 // ID for overlay flashing timer

  UINT_PTR refresh_timer_id; // 0 if no refresh timer installed.
  UINT_PTR flash_timer_id; // 0 if no overlay flash timer
  int last_transferred_bytes; // Used to check for cache updates.
  clock_t cumulative_transmission_time;
  clock_t last_transmission_start;

  
public: // dialogs
  kd_metashow *metashow; // NULL if not active
  COverview		*m_pOverview; // MJY:090308   NULL if not active 
  CEditMetadata *m_pEditMetadata; //MJY:120108
  CScaling		*m_pScaling; //MJY:010609

  afx_msg void OnToolOverviewwindow();
  afx_msg void OnToolOverviewReset();
  afx_msg void OnToolOverviewAttach();

  // true if the mouse point is in the overview image
  afx_msg void OnChangeRenderScale(UINT nID);

  afx_msg void OnToolEditmetadata();
  afx_msg void OnMeasurementScaling();

  afx_msg void OnFileSaveasOtherType();
  afx_msg void OnMeasurementSavemeasurement();
  afx_msg void OnUpdateToolOverviewwindow(CCmdUI *pCmdUI);
  afx_msg void OnUpdateToolOverviewResetwindow(CCmdUI *pCmdUI);
  afx_msg void OnUpdateToolOverviewAttach(CCmdUI *pCmdUI);
  afx_msg void OnUpdateToolEditmetadata(CCmdUI *pCmdUI);
  afx_msg void OnUpdateMeasurementScaling(CCmdUI *pCmdUI);
  afx_msg void OnUpdateFileSaveasOtherType(CCmdUI *pCmdUI);
  afx_msg void OnUpdateMeasurementSavemeasurement(CCmdUI *pCmdUI);

  void ChangeSingleLayerIndex(int order, int layer_idx, bool redraw = true, bool del_permanent = false);
  void UpdateViewDim(kdu_dims new_buffer_dims, bool need_redraw = true);
  void MixingLayer();

  double	jp_decode_progress;
  int		WaitTransmitComplete(int wait_time = MAX_WAIT_TIME);

  void	UpdateViewContent(){ if (child_wnd)child_wnd->UpdateViewRegion(); }
  void	UpdateROIObj(){ if (m_pOverview) m_pOverview->m_roisTab.UpdateROIList(); }
  void	UpdateMeasureObj(){}
  bool	UpdateCropImage(POINTF p1, POINTF p2, int layer_num, const TCHAR *sz_file_name);
  void	RestoreViewRegion();
  BOOL	PeekAndPump();
  
  afx_msg void OnFileLoadConf();
  afx_msg void OnUpdateFileLoadConf(CCmdUI *pCmdUI);
  afx_msg void OnFileSaveConfAs();
  afx_msg void OnUpdateFileSaveConfAs(CCmdUI *pCmdUI);

  afx_msg void OnMeasurementGrid();
  afx_msg void OnUpdateMeasurementGrid(CCmdUI *pCmdUI);
  afx_msg void OnMeasurementGridProperty();
  afx_msg void OnUpdateMeasurementGridProperty(CCmdUI *pCmdUI);

  afx_msg void OnEditExportmetadataassvg();
  afx_msg void OnUpdateExportmetadataassvg(CCmdUI *pCmdUI);


  TCHAR* GetOpenPathName(){ return open_file_pathname; }

  void FitImageToClient();
  void SetExportMode(BOOL bExportMode){ m_bExportMode = bExportMode; }

private:
	CGrid *m_pGrid;

	SystemInfo			m_GeneratedSystemInfo;
	INNOVAPLEXBoxHeader m_InnovaplexCustomHeader;
	BOOL	m_bExportMode;
	
	
};

const float FIX_SCALE_VALUE[CKdu_showApp::MAX_RENDER_SCALE] = {
	0.025F,
	0.05F,
	0.125F,
	0.25F,
	0.5F,
	1.0F,
	2.0F
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // KDU_SHOW_H
