#pragma once

#include <vector>

typedef enum{ NONEOBJ, GRID, SCALEBAR, ROI, LABEL, MEASURE_LINE, MEASURE_CURVE, MEASURE_AREA, 
		REGSION_RECT, REGSION_ELLIPSE, REGSION_FREE, GRID_ANALYZE, GRID_ANALYZE_V2}eOBJTYPE;
typedef enum{ BLOCK, WHITE, RED, GREEN, BLUE, YELLOW, NONE, MAX_PEN_TYPE }ePENCOLOR;
extern TCHAR *g_PENColorString[MAX_PEN_TYPE];


struct  ScaleBarInfo
{
	ScaleBarInfo() : color(RED), bar_length(100), pt{ 0.8f, 0.1f } {}
	
	ePENCOLOR	color;
	int			bar_length;
	POINTF		pt;
};


struct  ROIInfo
{
	ROIInfo() :color(BLOCK), scale(0), layer(0){
		memset(pts, 0, sizeof(POINTF) *4);
	}
	std::string		description;
	ePENCOLOR	color;
	POINTF		pts[4];	//RECT
	float		scale;
	int			layer;
};


struct  MeasureInfo
{
	MeasureInfo() :type(NONEOBJ), color(BLOCK), scale(0), layer(0), length(0), red(0), green(0), blue(0){}

	std::string		description;
	eOBJTYPE		type;
	ePENCOLOR		color;
	std::vector<POINTF>	pts;
	float			scale;
	int				layer;
	float			length;
	int				red;
	int				green;
	int				blue;
};

struct	LabelInfo
{
	LabelInfo() :font(L"tahoma"),fontColor(RGB(255,0,0)), bgColor(RGB(0,0,0)), transparent(TRANSPARENT), size(100), layer(0){}

	std::string		description;
	CString			font;
	COLORREF		fontColor;
	COLORREF		bgColor;
	int				transparent;
	int				size;
	POINTF			pts[4];
	int				layer;
};


struct  GridInfo
{
	GridInfo() : color(RGB(0, 255, 0)), interval(200), thick(1){}

	COLORREF			color;
	std::vector<POINTF>	pts;

	int			interval;
	int			thick;
	float		ratio;
};

struct LayerInfo {
	int			index;
	std::string	name;
};


struct ImageAdjustInfo {
	int			brightness;
	int			contrast;
	int			saturation;
	int			imageLayer;
};


struct ImageExternalInfo{
	ImageExternalInfo() :scanningRes(1.0f), brightness(50), contrast(50), saturation(50){}
	float		scanningRes;
	float		viewingScale;
	std::string	well;
	std::string	depth;
	std::string comment;

	std::vector<LayerInfo>	layers;

	int			brightness;
	int			contrast;
	int			saturation;
};

struct ImageResolutionInfo{
	int		width;
	int		height;
};

struct ImageViewingInfo{
	float	zoom;
	int		rotation;
	POINT	position;
};

class CDrawObjMgr;
class CVPViewerConf
{
public:
	CVPViewerConf();
	~CVPViewerConf();

	bool	Open(const TCHAR *szImagePath, CDrawObjMgr *pDrawObjMgr);
	void	LoadDefault();	//add default 

	void	Close();

	ImageExternalInfo *GetImageExternalInfo(){ return &m_ImageExternalInfo; }

	void	setAppliedScale(float scale);
	float	getAppliedScale(){ return m_appliedScale;}
	void	writeResolution(int width, int height)
	{
		m_resolution.width = width;
		m_resolution.height = height;
	}
	ImageResolutionInfo getResolution(){ return m_resolution; };
	bool				SaveConf(const TCHAR *strConfFile = NULL);

	void	writeViewingInfo(int rotation, float zoom, int posX, int posY)
	{
		m_viewing.rotation = rotation;
		m_viewing.zoom = zoom;
		m_viewing.position.x = posX;
		m_viewing.position.y = posY;
	}
	ImageViewingInfo* getViewingInfo(){ return &m_viewing; }

private:

	CDrawObjMgr *		m_pDrawObjMgr;
	TCHAR				m_strConfFile[MAX_PATH];
	ImageExternalInfo	m_ImageExternalInfo;
	float				m_appliedScale;
	ImageResolutionInfo	m_resolution;
	ImageViewingInfo	m_viewing;
	
};

