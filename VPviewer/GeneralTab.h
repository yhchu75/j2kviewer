#pragma once
#include "afxwin.h"



// CGeneralTab dialog

class CGeneralTab : public CDialog
{
	DECLARE_DYNAMIC(CGeneralTab)

public:
	CGeneralTab(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGeneralTab();

// Dialog Data
	enum { IDD = IDD_GENERAL};
	void	UpdateCurrentScaleStatus();
	void	SetAnalyzeMode(bool mode);
	void	UpdateFocusInfo(int index);

	CKdu_showApp *app;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedButtonRotate(UINT id);
	afx_msg void OnBnClickedButtonZoom(UINT id);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnClickedButtonFit();
	afx_msg void OnCbnSelchangeComboLayer1();
	afx_msg void OnCbnSelchangeComboLayer2();

private:
		
	CSliderCtrl	m_sliderMix;
	CSliderCtrl	m_sliderFocus;

	CComboBox	m_LayerList1;
	CComboBox	m_LayerList2;
			
	bool		start_mixing;
	HACCEL		m_hAccel;
	bool		m_bInit;
	int			m_nLastSelectLayer;

};
