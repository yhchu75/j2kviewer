#pragma once
#include "resource.h"

// EditLabel 대화 상자입니다.

class ToolValues
{
public:
	ToolValues(void);
	~ToolValues(void);

	static COLORREF FgColor;
	static COLORREF BgColor;
	static int LineWidth;
	static int FontSize;
	static CString FontName;
	static int FontMode;
};

class CEditLabel : public CDialogEx
{
	DECLARE_DYNAMIC(CEditLabel)

public:
	CEditLabel(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CEditLabel();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_EDITLABEL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	void OnBgClick(void);
	void OnFgClick(void);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnChangeLabel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBtnFontClicked();
	afx_msg void OnCheckBoxClicked();
private:

	CStatic m_fgColorBox;
	CStatic m_bgColorBox;

	CButton m_btnFont;
	CButton m_cbTransparent;

	bool m_isFirst = true;

public:
	CDrawObjMgr *mgr;
};

