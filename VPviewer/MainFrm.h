/******************************************************************************/
// File: MainFrm.h [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/*****************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/*******************************************************************************
Description:
   MFC-based class definitions and message mapping macros for the single frame
window in the interactive JPEG2000 image viewer, "kdu_show".
*******************************************************************************/

#if !defined(AFX_MAINFRM_H__F2E6C859_6A9A_4E1D_B7E6_B3196130B999__INCLUDED_)
#define AFX_MAINFRM_H__F2E6C859_6A9A_4E1D_B7E6_B3196130B999__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ChildView.h"
#include "kdu_compressed.h"
#include "ToolPlugInsMgr.h"
#include "VPViewerCore.h"

// TOOLBAR
// ============================================================================
#define TOOLBAR_VER 2

/*
#define TOOLBAR_ICON_WIDTH 16
#define TOOLBAR_ICON_HEIGHT 15
#define TOOLBAR_BUTTON_WIDTH 24
#define TOOLBAR_BUTTON_HEIGHT 22

#define TOOLBAR_ICON_WIDTH 24
#define TOOLBAR_ICON_HEIGHT 24
#define TOOLBAR_BUTTON_WIDTH 32
#define TOOLBAR_BUTTON_HEIGHT 32
*/

#define TOOLBAR_ICON_WIDTH 16
#define TOOLBAR_ICON_HEIGHT 16
#define TOOLBAR_BUTTON_WIDTH 24
#define TOOLBAR_BUTTON_HEIGHT 24
// ============================================================================

class CKdu_showApp; // Forward declaration.

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:
	bool	m_attach_overview;
	CToolPlugInsMgr* GetToolPlugInsMgr(){ return &m_ToolPlugInsMgr; }
	void	EnablePlugInsMenu(bool enable);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
  virtual ~CMainFrame();
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // embedded windows
  CStatusBar	m_wndStatusBar;
  CToolBar m_wndToolBar;
  CChildView    m_wndView;

private:
  CKdu_showApp *app;
  CReBar m_wndReBar;
  CVPViewerCore		m_VPViewerCore;
  CToolPlugInsMgr	m_ToolPlugInsMgr;


	// TOOLBAR
	// ============================================================================
	//LPCTSTR m_strToolbarText = L"Open Local Image\0Open Remote Image\0Close\0Load Config\0Save Config As\0Copy View To Clipboard\0Export as JPEG\0Crop Image Export\0Zoom In\0Zoom Out\0Overview Window\0Attach Overview\0Reset Overview Position\0Edit Metadata\0Scale\0Automatic Unit Conversion\0Show Measurement\0Show Table\0Fit to Screen\0Select\0Line\0Curve\0Area\0Scale bar\0Add Label\0Show Label\0Grid\0Grid Property\0Grid Property\0About VP Viewer\0Register JPX(JP2) File Extension\0";
	CArray<CString> m_arrToolbarText;
	BOOL m_bUseToolbar = TRUE;
	TBBUTTON m_TBinfo[256];
	int m_iTBCount;
	CMenu *m_pToolbarContext = NULL;

	void GetToolbarButtonInfo();
	int PopupToolbarContext();
	// ============================================================================


// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnMoving(UINT nType, LPRECT lpRect);
	afx_msg void OnMenuCommand(UINT nID);

#if (defined _MSC_VER && (_MSC_VER>=1300))
  afx_msg void OnTimer(UINT_PTR nIDEvent);
#else
  afx_msg void OnTimer(UINT nIDEvent);
#endif // _MSC_VER < 1300
	//}}AFX_MSG


	// TOOLBAR
	// ============================================================================
	afx_msg BOOL OnToolTipText(UINT nID, NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void QueryInsert(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void QueryDelete(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void QueryInfo(NMHDR * pNotifyStruct, LRESULT * result);
	void SaveLoadToolBar(BOOL bSave);
	afx_msg void OnShowToolBar();
	// ============================================================================


    DECLARE_MESSAGE_MAP()
// ----------------------------------------------------------------------------
public: // Access methods for use by the application object
  CChildView *get_child()
    {
      return &m_wndView;
    }
  CStatusBar *get_status_bar()
    {
      return &m_wndStatusBar;
    }
  void set_app(CKdu_showApp *app)
    {
      this->app = app;
	  m_VPViewerCore.set_app(app);
    }
  CToolBar *get_tool_bar()
  {
	  return &m_wndToolBar;
  }
  CToolPlugInsMgr* get_ToolPlguInsMgr(){ return &m_ToolPlugInsMgr; }


	// TOOLBAR
	// ============================================================================
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual BOOL DestroyWindow();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	afx_msg void OnNcRButtonUp(UINT nHitTest, CPoint point);
	afx_msg void OnToolbarShowtoolbar();
	afx_msg void OnToolbarCustomizetoolbar();
	afx_msg void OnNcPaint();

	void SetClientSize();
	// ============================================================================

	// RGB MEASUREMENT
	// ============================================================================
	afx_msg void OnMeasurementRgb();
	// ============================================================================
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__F2E6C859_6A9A_4E1D_B7E6_B3196130B999__INCLUDED_)
