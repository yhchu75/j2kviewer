#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "kdu_show.h"
#include "drawobjmgr.h"

// CRGBMeasureTable dialog

#define NUM_RTABLE_COL 4
#define NUM_RTABLE_ROW 100

static _TCHAR *RGBColumnLabel[NUM_RTABLE_COL] =
{
	_T("No"), _T("Red"), _T("Green"), _T("Blue")
};

static int RGBColumnFmt[NUM_RTABLE_COL] =
{
	LVCFMT_CENTER, LVCFMT_CENTER, LVCFMT_CENTER, LVCFMT_CENTER
};

static int RGBColumnWidth[NUM_RTABLE_COL] =
{
	30, 80, 80, 80
};

class CRGBMeasureTable : public CDialogEx
{
	DECLARE_DYNAMIC(CRGBMeasureTable)

public:
	CRGBMeasureTable(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRGBMeasureTable();

// Dialog Data
	enum { IDD = IDD_RGB_MEASURE_TABLE };
	CDrawObjMgr *m_pDrawObjMgr;
	CKdu_showApp *m_app;
	void	SetApp(CKdu_showApp* app, CDrawObjMgr* pDrawObjMgr){
		m_app = app;
		m_pDrawObjMgr = pDrawObjMgr;
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_ListControl;
	wchar_t		itemText[NUM_RTABLE_ROW][NUM_RTABLE_COL][64];
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	void	OnRefresh();
	afx_msg void OnNMClickListRgb(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListRgb(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonRgbexport();
};
