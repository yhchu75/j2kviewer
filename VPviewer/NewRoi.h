#pragma once
#include "resource.h"

#define	NR_NEWROI	WM_APP

// CNewRoi dialog
class CNewRoi : public CDialog
{
	DECLARE_DYNAMIC(CNewRoi)

public:
	CNewRoi(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNewRoi();

// Dialog Data
	enum { IDD = IDD_NEWROI };
	void SetNextROIIndex(int index){ m_NextROIIndex = index; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	CEdit		m_description;
	CComboBox	m_color;
	CString		m_descriptionStr;
	int			m_Colorindex;
	int			m_NextROIIndex;
};
