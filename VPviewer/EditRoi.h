#pragma once
//#include "resource.h"
#include "NewRoi.h"


// CEditRoi dialog

class CEditRoi : public CDialog
{
	DECLARE_DYNAMIC(CEditRoi)

public:
	CEditRoi(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditRoi();

// Dialog Data
	enum { IDD = IDD_EDITROI };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	virtual BOOL	OnInitDialog();
	virtual void	OnOK();

public:

	CEdit			m_description;
	CComboBox		m_color;
	CString			m_descriptionStr;
	int				m_colorIndex;
	bool			m_bRoiDelete;
	
};
