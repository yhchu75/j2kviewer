/******************************************************************************/
// File: kdu_show.cpp [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/******************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/******************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/*******************************************************************************
Description:
   Implements the application object of the interactive JPEG2000 viewer,
"kdu_show".  Menu processing and idle-time decompressor processing are all
controlled from here.  The "kdu_show" application demonstrates some of the
support offered by the Kakadu framework for interactive applications,
including persistence and incremental region-based decompression.
*******************************************************************************/

#include "stdafx.h"
#include <math.h>
#include "kdu_show.h"
#include "MainFrm.h"
#include "kdu_arch.h"
#include "region_compositor_local.h"
#include "kdu_messaging.h"
#include "kdu_threads.h"
#include "kdu_utils.h"
#include ".\kdu_show.h"
#include "Overview.h"
#include "EditMetadata.h"
#include "Scaling.h"
#include "ROISTab.h"
#include "NewRoi.h"
#include "DlgSaveFile.h"
#include "RegisterUtils.h"
#include "../expander/kd_bitmap_file_buf.h"
#include "../expander/SVGExporter.h"
#include "mmsystem.h"


BOOL	CheckLicenseFile();

ULONG_PTR gdiplusToken;

using namespace kd_supp_local;
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define RELEASE_BUFFER(buffer) {if(buffer){delete buffer ; buffer = NULL;}}

static const TCHAR *jpip_channel_types[4] = { _T("http"), _T("http-tcp"), _T("none"), NULL };
static const TCHAR *jpip_login_types[3] = { _T("Anonymous"), _T("ID/Password"), NULL };

CKdu_showApp theApp; // There can only be one application object.


/* ========================================================================== */
/*                            Internal Functions                              */
/* ========================================================================== */

/******************************************************************************/
/* INLINE                         find_scale                                  */
/******************************************************************************/

static inline float
  find_scale(int expand, int discard_levels)
  /* Returns `expand'*2^{-`discard_levels'}. */
{
  float scale = (float) expand;
  for (; discard_levels > 0; discard_levels--)
    scale *= 0.5F;
  return scale;
}



/******************************************************************************/
/* STATIC                          copy_tile                                  */
/******************************************************************************/

static void
  copy_tile(kdu_tile tile_in, kdu_tile tile_out)
  /* Walks through all code-blocks of the tile in raster scan order, copying
     them from the input to the output tile. */
{
  int c, num_components = tile_out.get_num_components();
  for (c=0; c < num_components; c++)
    {
      kdu_tile_comp comp_in;  comp_in  = tile_in.access_component(c);
      kdu_tile_comp comp_out; comp_out = tile_out.access_component(c);
      int r, num_resolutions = comp_out.get_num_resolutions();
      for (r=0; r < num_resolutions; r++)
        {
          kdu_resolution res_in;  res_in  = comp_in.access_resolution(r);
          kdu_resolution res_out; res_out = comp_out.access_resolution(r);
          int b, min_band;
          int num_bands = res_in.get_valid_band_indices(min_band);
          for (b=min_band; num_bands > 0; num_bands--, b++)
            {
              kdu_subband band_in;  band_in = res_in.access_subband(b);
              kdu_subband band_out; band_out = res_out.access_subband(b);
              kdu_dims blocks_in;  band_in.get_valid_blocks(blocks_in);
              kdu_dims blocks_out; band_out.get_valid_blocks(blocks_out);
              if ((blocks_in.size.x != blocks_out.size.x) ||
                  (blocks_in.size.y != blocks_out.size.y))
                { kdu_error e; e << "Transcoding operation cannot proceed: "
                  "Code-block partitions for the input and output "
                  "code-streams do not agree."; }
              kdu_coords idx;
              for (idx.y=0; idx.y < blocks_out.size.y; idx.y++)
                for (idx.x=0; idx.x < blocks_out.size.x; idx.x++)
                  {
                    kdu_block *in  = band_in.open_block(idx+blocks_in.pos);
                    kdu_block *out = band_out.open_block(idx+blocks_out.pos);
                    if (in->K_max_prime != out->K_max_prime)
                      { kdu_error e;
                        e << "Cannot copy blocks belonging to subbands with "
                             "different quantization parameters."; }
                    if ((in->size.x != out->size.x) ||
                        (in->size.y != out->size.y))  
                      { kdu_error e; e << "Cannot copy code-blocks with "
                        "different dimensions."; }
                    out->missing_msbs = in->missing_msbs;
                    if (out->max_passes < (in->num_passes+2))
                      out->set_max_passes(in->num_passes+2,false);
                    out->num_passes = in->num_passes;
                    int num_bytes = 0;
                    for (int z=0; z < in->num_passes; z++)
                      {
                        num_bytes += (out->pass_lengths[z]=in->pass_lengths[z]);
                        out->pass_slopes[z] = in->pass_slopes[z];
                      }
                    if (out->max_bytes < num_bytes)
                      out->set_max_bytes(num_bytes,false);
                    memcpy(out->byte_buffer,in->byte_buffer,(size_t) num_bytes);
                    band_in.close_block(in);
                    band_out.close_block(out);
                  }
            }
        }
    }
}

/* ========================================================================== */
/*                     Error and Warning Message Handlers                     */
/* ========================================================================== */

/******************************************************************************/
/* CLASS                         core_messages_dlg                            */
/******************************************************************************/

class core_messages_dlg : public CDialog
{
  public:
    core_messages_dlg(const char *src, CWnd* pParent = NULL)
      : CDialog(core_messages_dlg::IDD, pParent)
      {
        int num_chars;
        const char *sp;
        for (sp=src, num_chars=0; *sp != '\0'; sp++, num_chars++)
          if ((*sp == '&') || (*sp == '\\'))
            num_chars++; // Need to replicate these special characters
        string = new char[num_chars+1];
        for (sp=src, num_chars=0; *sp != '\0'; sp++)
          {
            string[num_chars++] = *sp;
            if ((*sp == '&') || (*sp == '\\'))
              string[num_chars++] = *sp;
          }
        string[num_chars] = '\0';
      }
    ~core_messages_dlg()
      {
        if (string != NULL)
          delete[] string;
      }
// Dialog Data
	//{{AFX_DATA(core_messages_dlg)
	enum { IDD = IDD_CORE_MESSAGES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(core_messages_dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
private:
  CStatic *get_static()
    {
      return (CStatic *) GetDlgItem(IDC_MESSAGE);
    }
private:
  char *string;
protected:
	// Generated message map functions
	//{{AFX_MSG(core_messages_dlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/******************************************************************************/
/*                      core_messages_dlg::DoDataExchange                     */
/******************************************************************************/

void
  core_messages_dlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(core_messages_dlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(core_messages_dlg, CDialog)
	//{{AFX_MSG_MAP(core_messages_dlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************************************************************************/
/*                      core_messages_dlg::OnInitDialog                       */
/******************************************************************************/

BOOL core_messages_dlg::OnInitDialog() 
{
  CDialog::OnInitDialog();
  
  USES_CONVERSION;
  while (*string == '\n')
    string++; // skip leading empty lines, if any.
  get_static()->SetWindowText(A2T(string));

  // Find the height of the displayed text
  int text_height = 0;
  SIZE text_size;
  CDC *dc = get_static()->GetDC();
  const char *scan = string;
  while (*scan != '\0')
    {
      const char *sp = strchr(scan,'\n');
      if (sp == NULL)
        sp = scan + strlen(scan);
      if (scan != sp)
        text_size = dc->GetTextExtent(A2T(scan),(int)(sp-scan));
      text_height += text_size.cy;
      scan = (*sp != '\0')?sp+1:sp;
    }
  get_static()->ReleaseDC(dc);

  // Resize windows to fit the text height

  WINDOWPLACEMENT dialog_placement, static_placement;
  GetWindowPlacement(&dialog_placement);
  get_static()->GetWindowPlacement(&static_placement);
  int dialog_width = dialog_placement.rcNormalPosition.right -
    dialog_placement.rcNormalPosition.left;
  int static_width = static_placement.rcNormalPosition.right -
    static_placement.rcNormalPosition.left;
  int dialog_height = dialog_placement.rcNormalPosition.bottom -
    dialog_placement.rcNormalPosition.top;
  int static_height = static_placement.rcNormalPosition.bottom -
    static_placement.rcNormalPosition.top;

  get_static()->SetWindowPos(NULL,0,0,static_width,text_height,
                             SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW);
  SetWindowPos(NULL,0,0,dialog_width,text_height+8+dialog_height-static_height,
               SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW);  
  return TRUE;
}

/******************************************************************************/
/* CLASS                     kd_message_collector                             */
/******************************************************************************/

class kd_message_collector : public kdu_message {
  /* This object is used to collect message text within the
     `CPropertiesDlg' object, so as to format and print descriptions of
     codestream parameter attributes.  It derives from the non-thread-safe
     `kdu_message' object, since multiple threads cannot access the same
     object simultaneously within `CPropertiesDlg'. */
  public: // Member functions
    kd_message_collector()
      {
        max_chars = 10; num_chars = 0;
        buffer = new char[max_chars+1]; *buffer = '\0';
      }
    ~kd_message_collector() { delete[] buffer; }
    const char *get_text() { return buffer; }
    void put_text(const char *string)
      {
        int new_chars = (int) strlen(string);
        if ((num_chars+new_chars) > max_chars)
          {
            max_chars = (num_chars+new_chars)*2;
            char *buf = new char[max_chars+1];
            strcpy(buf,buffer);
            delete[] buffer;
            buffer = buf;
          }
        num_chars += new_chars;
        strcat(buffer,string);
      }
    void flush(bool end_of_message)
      {
        if (end_of_message)
          { num_chars = 0; *buffer = '\0'; }
      }
  private: // Data
    int num_chars, max_chars;
    char *buffer;
  };

/******************************************************************************/
/* CLASS                   kd_core_message_collector                          */
/******************************************************************************/

class kd_core_message_collector : public kdu_thread_safe_message {
  /* This object is used to implement error and warning message services
     which are registered globally for use by all threads which may
     use `kdu_error' or `kdu_warning'.  Since this may happen from multiple
     threads, we derive this object from the thread-safe messaging service,
     `kdu_thread_safe_message'.  It is important that we explicitly call the
     base object's `flush' function at the end of the derived object's
     `flush' implementation, since this will release the mutex which
     serializes access to the messaging service. */
  public: // Member functions
    kd_core_message_collector(bool for_errors)
      {
        max_chars = 10; num_chars = 0;
        buffer = new char[max_chars+1]; *buffer = '\0';
        raise_end_of_message_exception = for_errors;
      }
    ~kd_core_message_collector() { delete[] buffer; }
    void put_text(const char *string)
      {
        int new_chars = (int) strlen(string);
        if ((num_chars+new_chars) > max_chars)
          {
            max_chars = (num_chars+new_chars)*2;
            char *buf = new char[max_chars+1];
            strcpy(buf,buffer);
            delete[] buffer;
            buffer = buf;
          }
        num_chars += new_chars;
        strcat(buffer,string);
      }
    void flush(bool end_of_message)
      {
        if (end_of_message)
          {
            //core_messages_dlg messages(buffer,theApp.frame_wnd);
			  bool bNetworkError = false;
			  bool bWarning = false;
			  core_messages_dlg messages(buffer, NULL);
			  if (strstr(buffer, "HTTP")){	//Checkk connection error.
				  bNetworkError = true;
			  }
			  
			  if (strstr(buffer, "Warning")){	//Checkk connection error.
				  bWarning = true;
				  return; // do not show waring meesage
			  }
			  
            num_chars = 0;   *buffer = '\0';
            messages.DoModal();

			if (!bNetworkError && !bWarning){//Network connection error가 아닐 경우 강제 종료 시킨다.
				ExitProcess(-1);
			}

			kdu_thread_safe_message::flush(true); // May release other threads
            if (raise_end_of_message_exception)
              throw (int) 0;
          }
        else
          kdu_thread_safe_message::flush(false);
      }
  private: // Data
    int num_chars, max_chars;
    char *buffer;
    bool raise_end_of_message_exception;
  };

static kd_core_message_collector warn_collector(false);
static kd_core_message_collector err_collector(true);
static kdu_message_formatter warn_formatter(&warn_collector,50);
static kdu_message_formatter err_formatter(&err_collector,50);


/* ========================================================================== */
/*                                 kd_settings                                */
/* ========================================================================== */

/******************************************************************************/
/*                           kd_settings::kd_settings                         */
/******************************************************************************/

kd_settings::kd_settings()
{
  open_save_dir = NULL; open_idx = save_idx = 1;
  jpip_server = jpip_proxy = jpip_cache = NULL;
  jpip_request = jpip_channel = NULL;
  set_jpip_channel_type(jpip_channel_types[0]);
  set_jpip_login_type(jpip_login_types[0]);
  jpip_id = NULL;
  jpip_pw = NULL;
}

/******************************************************************************/
/*                          kd_settings::~kd_settings                         */
/******************************************************************************/

kd_settings::~kd_settings()
{
  if (open_save_dir != NULL)
    delete[] open_save_dir;
  if (jpip_server != NULL)
    delete[] jpip_server;
  if (jpip_proxy != NULL)
    delete[] jpip_proxy;
  if (jpip_cache != NULL)
    delete[] jpip_cache;
  if (jpip_request != NULL)
    delete[] jpip_request;
  if (jpip_channel != NULL)
    delete[] jpip_channel;
  if (jpip_login != NULL)
	  delete[] jpip_login;
  if (jpip_id != NULL)
	  delete[] jpip_id;
  if (jpip_pw != NULL)
	  delete[] jpip_pw;
}

/******************************************************************************/
/*                        kd_settings::save_to_registry                       */
/******************************************************************************/

void
  kd_settings::save_to_registry(CWinApp *app)
{
	  USES_CONVERSION;
	  app->WriteProfileString(_T("files"), _T("directory"), get_open_save_dir());
	  app->WriteProfileInt(_T("files"), _T("open-index"), open_idx);
	  app->WriteProfileInt(_T("files"), _T("save-index"), save_idx);
	  app->WriteProfileString(_T("jpip"), _T("cache-dir"), get_jpip_cache());
	  app->WriteProfileString(_T("jpip"), _T("server"), get_jpip_server());
	  app->WriteProfileString(_T("jpip"), _T("request"), get_jpip_request());
	  app->WriteProfileString(_T("jpip"), _T("channel-type"), get_jpip_channel_type());
	  app->WriteProfileInt(_T("jpip"), _T("use_cache"), is_use_cache);
	  app->WriteProfileInt(_T("jpip"), _T("buffer_len"), jpip_buffer_len);
	  app->WriteProfileString(_T("jpip"), _T("login-type"), get_jpip_login_type());
	  app->WriteProfileString(_T("jpip"), _T("id"), get_jpip_id());

}

/******************************************************************************/
/*                       kd_settings::load_from_registry                      */
/******************************************************************************/

void
  kd_settings::load_from_registry(CWinApp *app)
{
	  set_open_save_dir(app->GetProfileString(_T("files"), _T("directory")).GetBuffer(0));
	  open_idx = app->GetProfileInt(_T("files"), _T("open-index"), 1);
	  save_idx = app->GetProfileInt(_T("files"), _T("save-index"), 1);
  
  CString existing_cache_dir =
	  app->GetProfileString(_T("jpip"), _T("cache-dir"), _T("***")).GetBuffer(0);
  if (existing_cache_dir.Compare(_T("***")) == 0)
    {
      TCHAR temp_path[MAX_PATH+1];
      int len = GetTempPath(MAX_PATH,temp_path);
      if (len > 0)
		  set_jpip_cache(temp_path);
    }
  else
	  set_jpip_cache(existing_cache_dir);
  set_jpip_server(app->GetProfileString(_T("jpip"), _T("server")).GetBuffer(0));
  set_jpip_request(app->GetProfileString(_T("jpip"), _T("request")).GetBuffer(0));
  CString c_string = app->GetProfileString(_T("jpip"), _T("channel-type"));
  const TCHAR *string = c_string.GetBuffer(0);
  if ((string == NULL) || (*string == '\0'))
	  string = jpip_channel_types[0];

  CString c_string2 = app->GetProfileString(_T("jpip"), _T("login-type"));
  const TCHAR *string2 = c_string2.GetBuffer(0);
  if ((string2 == NULL) || (*string2 == '\0'))
	  string2 = jpip_login_types[0];

  set_jpip_id(app->GetProfileString(_T("jpip"), _T("id")).GetBuffer(0));

  is_use_cache = app->GetProfileInt(_T("jpip"), _T("use_cache"), 1);
  jpip_buffer_len = app->GetProfileInt(_T("jpip"), _T("buffer_len"), 512);

  set_jpip_channel_type(string);
  set_jpip_login_type(string2);
}


/* ========================================================================== */
/*                               kd_bitmap_buf                                */
/* ========================================================================== */

/******************************************************************************/
/*                        kd_bitmap_buf::create_bitmap                        */
/******************************************************************************/

void  kd_bitmap_buf::create_bitmap(kdu_coords size)
{
  if (bitmap != NULL)
    {
      assert(this->size == size);
      return;
    }

  memset(&bitmap_info,0,sizeof(bitmap_info));
  bitmap_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  bitmap_info.bmiHeader.biWidth = size.x;
  bitmap_info.bmiHeader.biHeight = -size.y;
  bitmap_info.bmiHeader.biPlanes = 1;
  bitmap_info.bmiHeader.biBitCount = 32;
  bitmap_info.bmiHeader.biCompression = BI_RGB;
  void *buf_addr;
  bitmap = CreateDIBSection(NULL,&bitmap_info,DIB_RGB_COLORS,&buf_addr,NULL,0);
  if (bitmap == NULL)
    { kdu_error e; e << "Unable to allocate sufficient bitmap surfaces "
      "to service `kdu_region_compositor'."; }
  this->buf = (kdu_uint32 *) buf_addr;
  this->row_gap = size.x;
  this->size = size;
}


void kd_bitmap_buf::copy_bitmap(kd_bitmap_buf* src)
{
	create_bitmap(src->size);

	int src_bitmap_row_gap;
	kdu_uint32 *src_bitmap_buffer;
	src->access_bitmap(src_bitmap_buffer, src_bitmap_row_gap);
		
	memcpy(this->buf, src_bitmap_buffer, src->size.x * src->size.y * 4);
}


/* ========================================================================== */
/*                            kd_bitmap_compositor                            */
/* ========================================================================== */

/******************************************************************************/
/*                    kd_bitmap_compositor::allocate_buffer                   */
/******************************************************************************/

kdu_compositor_buf *
  kd_bitmap_compositor::allocate_buffer(kdu_coords min_size,
                                        kdu_coords &actual_size,
                                        bool read_access_required)
{
  if (min_size.x < 1) min_size.x = 1;
  if (min_size.y < 1) min_size.y = 1;
  actual_size = min_size;
  int row_gap = actual_size.x;

  kd_bitmap_buf *prev=NULL, *elt=NULL;

  // Start by trying to find a compatible buffer on the free list.
  for (elt=free_list, prev=NULL; elt != NULL; prev=elt, elt=elt->next)
    if (elt->size == actual_size)
      break;

  bool need_init = false;
  if (elt != NULL)
    { // Remove from the free list
      if (prev == NULL)
        free_list = elt->next;
      else
        prev->next = elt->next;
      elt->next = NULL;
    }
  else
    {
      // Delete the entire free list: one way to avoid accumulating buffers
      // whose sizes are no longer helpful
      while ((elt=free_list) != NULL)
        { free_list = elt->next;  delete elt; }

      // Allocate a new element
	  if (is_virtaul_mem){
		  kd_bitmap_file_buf* bitmap_file = new kd_bitmap_file_buf;
		  bitmap_file->set_file_path(m_map_file);
		  elt = (kd_bitmap_buf *)bitmap_file;
		  	  }
	  else{
		  elt = new kd_bitmap_buf;
		}
      need_init = true;
    }
  elt->next = active_list;
  active_list = elt;
  elt->set_read_accessibility(read_access_required);
  if (need_init)
  {
//	  //TRACE("do create_bitmap\n");
	  elt->create_bitmap(actual_size);
  }
  return elt;
}

/******************************************************************************/
/*                    kd_bitmap_compositor::delete_buffer                     */
/******************************************************************************/

void
  kd_bitmap_compositor::delete_buffer(kdu_compositor_buf *_buffer)
{
  kd_bitmap_buf *buffer = (kd_bitmap_buf *) _buffer;
  kd_bitmap_buf *scan, *prev;
  for (prev=NULL, scan=active_list; scan != NULL; prev=scan, scan=scan->next)
    if (scan == buffer)
      break;
  assert(scan != NULL);
  if (prev == NULL)
    active_list = scan->next;
  else
    prev->next = scan->next;

  scan->next = free_list;
  free_list = scan;
}


/* ========================================================================== */
/*                               CAboutDlg Class                              */
/* ========================================================================== */

class CAboutDlg : public CDialog {
  public:
	  CAboutDlg() :CDialog(CAboutDlg::IDD){}

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

  protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
  };

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/* ========================================================================== */
/*                               CActivationWarningDlg Class                              */
/* ========================================================================== */

class CActivationWarningDlg : public CDialog {
public:
	CActivationWarningDlg():CDialog(CActivationWarningDlg::IDD){}

	// Dialog Data
	//{{AFX_DATA(CActivationWarningDlg)
	enum { IDD = IDD_ACTIVATION_WARNNING };
	//}}AFX_DATA

	
protected:
	virtual void DoDataExchange(CDataExchange* pDX){
		CDialog::DoDataExchange(pDX);
		DDX_Control(pDX, IDC_RICHEDIT21, m_Edit);
	}
	DECLARE_MESSAGE_MAP()

	virtual BOOL	OnInitDialog(){
		CDialog::OnInitDialog();
		m_Edit.SetOptions(ECOOP_XOR, ECO_SAVESEL);
		
		COLORREF color = 0;
		AddText(_T("   License Required ! \r"), color, 12, _T("ARIAL"));
		AddText(_T(" To activate your demo copy of VPViewer, \r"), color, 10, _T("ARIAL"));
		AddText(_T(" please request the activation license by calling +1-832-372-8489\r"), color, 10, _T("ARIAL"));
		AddText(_T(" or send an email to blee@innovaplex.com."), color, 10, _T("ARIAL"));

		return FALSE;
	}

	void AddText(const TCHAR* strTextIn, COLORREF &crNewColor, int sSize, const TCHAR* lpszFontName)
	{
		//리치에디트 컨트롤의 끝에 문자열 추가하기
		int iTotalTextLength = m_Edit.GetWindowTextLength();
		m_Edit.SetSel(iTotalTextLength, iTotalTextLength);
		m_Edit.ReplaceSel((LPCTSTR)strTextIn);
		int iStartPos = iTotalTextLength;

		//인자로 넘어온 색상값과 폰트크기로, CHARFORMAT 설정
		CHARFORMAT cf;
		cf.cbSize = sizeof(CHARFORMAT);
		cf.dwMask = CFM_COLOR | CFM_UNDERLINE | CFM_BOLD | CFM_FACE | CFM_SIZE;
		cf.dwEffects = (unsigned long)~(CFE_AUTOCOLOR | CFE_UNDERLINE | CFE_BOLD);
		cf.crTextColor = crNewColor;//RGB(0, 0, 0);
		cf.yHeight = sSize * 20;
		_tcscpy(cf.szFaceName, lpszFontName);

		//새로추가된 부분에 색상및 폰트 속성 적용
		int iEndPos = m_Edit.GetWindowTextLength();
		m_Edit.SetSel(iStartPos, iEndPos);
		m_Edit.SetSelectionCharFormat(cf);
		m_Edit.HideSelection(TRUE, FALSE);
		m_Edit.LineScroll(1);
	}
		
	CRichEditCtrl m_Edit;
};

BEGIN_MESSAGE_MAP(CActivationWarningDlg, CDialog)
	//{{AFX_MSG_MAP(CActivationWarningDlg)
	// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/* ========================================================================== */
/*                              CJpipOpenDlg Class                            */
/* ========================================================================== */

class CJpipOpenDlg : public CDialog {
  public:
	  CJpipOpenDlg(TCHAR *server_name, int server_len,
		  TCHAR *request, int request_len,
		  bool is_use_cache, int buffer_len,
		  TCHAR *channel_type, int channel_len,
		  TCHAR *proxy_name, int proxy_len,
		  TCHAR *cache_dir_name, int cache_dir_len,
		  TCHAR *connect_type, int connect_len,
		  TCHAR *id, int id_len,
		  TCHAR *pw, int pw_len,
		  CWnd *pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CJpipOpenDlg)
	enum { IDD = IDD_JPIP_OPEN };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


	bool	use_cache(){ return is_use_cache; }
	int		get_memory_len(){ return buffer_len; }
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJpipOpenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
  protected:
	// Generated message map functions
	//{{AFX_MSG(CJpipOpenDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

  private: // Base object overrides
    BOOL OnInitDialog();
    void OnOK();

  private: // Control access
    CEdit *get_server()
      { return (CEdit *) GetDlgItem(IDC_JPIP_SERVER); }
    CEdit *get_request()
      { return (CEdit *) GetDlgItem(IDC_JPIP_REQUEST); }
    CComboBox *get_channel_type()
      { return (CComboBox *) GetDlgItem(IDC_JPIP_CHANNEL_TYPE); }
    CEdit *get_proxy()
      { return (CEdit *) GetDlgItem(IDC_JPIP_PROXY); }
    CEdit *get_cache_dir()
      { return (CEdit *) GetDlgItem(IDC_JPIP_CACHE_DIR); }
	CEdit *get_memory()
	{
		return (CEdit *)GetDlgItem(IDC_JPIP_MEMORY);
	}
	CComboBox *get_login_type()
	{
		return (CComboBox *) GetDlgItem(IDC_JPIP_LOGIN_TYPE);
	}
	CEdit *get_id()
	{
		return (CEdit*)GetDlgItem(IDC_JPIP_ID);
	}
	CEdit *get_pw()
	{
		return (CEdit*)GetDlgItem(IDC_JPIP_PW);
	}

	
  private:
    TCHAR *server_name, *request, *channel_type;
	TCHAR *proxy_name, *cache_dir_name;
	bool is_use_cache;
	int server_len, request_len, channel_len, proxy_len, cache_dir_len, buffer_len;

	TCHAR *login_type, *id, *pw;
	int login_len, id_len, pw_len;
public:
	afx_msg void OnCbnSelchangeJpipLoginType();
};

/******************************************************************************/
/*                          CJpipOpenDlg::CJpipOpenDlg                        */
/******************************************************************************/

CJpipOpenDlg::CJpipOpenDlg(TCHAR *server_name, int server_len,
	TCHAR *request, int request_len,
	bool is_use_cache, int buffer_len,
	TCHAR *channel_type, int channel_len,
	TCHAR *proxy_name, int proxy_len,
	TCHAR *cache_dir_name, int cache_dir_len,
	TCHAR *login_type, int login_len,
	TCHAR *id, int id_len,
	TCHAR *pw, int pw_len,
                           CWnd *pParent /*=NULL*/)
	: CDialog(CJpipOpenDlg::IDD, pParent)
{
  this->server_name = server_name; this->server_len = server_len;
  this->request = request; this->request_len = request_len;
  this->channel_type = channel_type; this->channel_len = channel_len;
  this->proxy_name = proxy_name; this->proxy_len = proxy_len;
  this->cache_dir_name = cache_dir_name; this->cache_dir_len = cache_dir_len;
  this->is_use_cache = is_use_cache;
  this->buffer_len = buffer_len;

  this->login_type = login_type; this->login_len = login_len;
  this->id = id, this->id_len = id_len;
  this->pw = pw, this->pw_len = pw_len;
	//{{AFX_DATA_INIT(CJpipOpenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************************************************************************/
/*                          CJpipOpenDlg::OnInitDialog                        */
/******************************************************************************/

BOOL
  CJpipOpenDlg::OnInitDialog()
{

	  if (is_use_cache){
		  CheckDlgButton(IDC_RADIO_CACHE, TRUE);
	  }
	  else
	  {
		  CheckDlgButton(IDC_RADIO_MEMORY, TRUE);
	  }

	  get_server()->SetWindowText(server_name);
	  get_request()->SetWindowText(request);
	  get_proxy()->SetWindowText(proxy_name);
	  get_cache_dir()->SetWindowText(cache_dir_name);
	  SetDlgItemInt(IDC_JPIP_MEMORY, buffer_len);

	  get_id()->SetWindowText(id);

	  const TCHAR **scan;
	  for (scan = jpip_channel_types; *scan != NULL; scan++)
		  get_channel_type()->AddString(*scan);
	  scan = jpip_channel_types;
	  if (channel_type[0] == '\0')
		  _tcsncpy(channel_type, *scan, channel_len - 1);
	  get_channel_type()->SelectString(-1, channel_type);

	  const TCHAR **scan2;
	  for (scan2 = jpip_login_types; *scan2 != NULL; scan2++)
		  get_login_type()->AddString(*scan2);
	  scan2 = jpip_login_types;
	  if (login_type[0] == '\0')
		  _tcsncpy(login_type, *scan2, login_len - 1);
	  get_login_type()->SelectString(-1, login_type);

	  if (*server_name == '\0')
		  GotoDlgCtrl(GetDlgItem(IDC_JPIP_SERVER));
	  else
		  GotoDlgCtrl(GetDlgItem(IDC_JPIP_REQUEST));

	  if (get_login_type()->GetCurSel() == 0)
	  {
		  get_id()->EnableWindow(FALSE);
		  get_pw()->EnableWindow(FALSE);
	  }
	  else
	  {
		  get_id()->EnableWindow(TRUE);
		  get_pw()->EnableWindow(TRUE);
	  }


	  return 0;
}

/******************************************************************************/
/*                         CJpipOpenDlg::DoDataExchange                       */
/******************************************************************************/

void
  CJpipOpenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJpipOpenDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CJpipOpenDlg, CDialog)
	//{{AFX_MSG_MAP(CJpipOpenDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_JPIP_LOGIN_TYPE, &CJpipOpenDlg::OnCbnSelchangeJpipLoginType)
END_MESSAGE_MAP()

/******************************************************************************/
/*                              CJpipOpenDlg::OnOK                            */
/******************************************************************************/

void
  CJpipOpenDlg::OnOK()
{
  int string_len;
  string_len = get_server()->GetLine(0,server_name,server_len-1);
  server_name[string_len] = '\0';
  string_len = get_request()->GetLine(0,request,request_len-1);
  request[string_len] = '\0';
  string_len = get_proxy()->GetLine(0,proxy_name,proxy_len-1);
  proxy_name[string_len] = '\0';
  string_len = get_cache_dir()->GetLine(0,cache_dir_name,cache_dir_len-1);
  cache_dir_name[string_len] = '\0';
  int cb_idx = get_channel_type()->GetCurSel();
  if (cb_idx != CB_ERR)
    {
      string_len = get_channel_type()->GetLBTextLen(cb_idx);
      if (string_len < channel_len)
        get_channel_type()->GetLBText(cb_idx,channel_type);
    }


  int cb_idx2 = get_login_type()->GetCurSel();
  if (cb_idx2 != CB_ERR)
  {
	  string_len = get_login_type()->GetLBTextLen(cb_idx2);
	  if (string_len < login_len)
		  get_login_type()->GetLBText(cb_idx2, login_type);
  }

  string_len = get_id()->GetLine(0, id, id_len - 1);
  id[string_len] = '\0';

  string_len = get_pw()->GetLine(0, pw, pw_len - 1);
  pw[string_len] = '\0';

  if (*server_name == '\0')
    {
      MessageBox(_T("You must enter a server name or IP address!"),
		  _T("Dialog entry error"), MB_OK | MB_ICONERROR);
      GotoDlgCtrl(GetDlgItem(IDC_JPIP_SERVER));
      return;
    }
  if (*request == '\0')
    {
	  MessageBox(_T("You must enter an object (e.g., file name) to be served!"),
		  _T("Dialog entry error"), MB_OK | MB_ICONERROR);
      GotoDlgCtrl(GetDlgItem(IDC_JPIP_REQUEST));
      return;
    }
  if (*channel_type == '\0')
    {
	  MessageBox(_T("You must select a channel type\n")
		  _T("(or \"none\" for stateless communications)!"),
				 _T("Dialog entry error"), MB_OK | MB_ICONERROR);
      GotoDlgCtrl(GetDlgItem(IDC_JPIP_CHANNEL_TYPE));
      return;
    }

  is_use_cache = IsDlgButtonChecked(IDC_RADIO_CACHE);
  buffer_len = GetDlgItemInt(IDC_JPIP_MEMORY);


  EndDialog(IDOK);
}


void CJpipOpenDlg::OnCbnSelchangeJpipLoginType()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (get_login_type()->GetCurSel() == 0)
	{
		get_id()->EnableWindow(FALSE);
		get_pw()->EnableWindow(FALSE);
	}
	else
	{
		get_id()->EnableWindow(TRUE);
		get_pw()->EnableWindow(TRUE);
	}
}


/* ========================================================================== */
/*                       CKdu_showApp Class Implementation                    */
/* ========================================================================== */

BEGIN_MESSAGE_MAP(CKdu_showApp, CWinApp)
	//{{AFX_MSG_MAP(CKdu_showApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_REGISTERFILEEXTENSION, OnRegisterFileExtension)
	ON_COMMAND(ID_FILE_CLOSE, OnFileClose)
	ON_UPDATE_COMMAND_UI(ID_FILE_CLOSE, OnUpdateFileClose)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_VIEW_ZOOM_OUT, OnViewZoomOut)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_OUT, OnUpdateViewZoomOut)
	ON_COMMAND(ID_VIEW_ZOOM_IN, OnViewZoomIn)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_IN, OnUpdateViewZoomIn)
	ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
	//for presentation
	ON_COMMAND(ID_JPIP_OPEN, OnJpipOpen)
	ON_COMMAND(ID_IMAGE_NEXT, OnImageNext)
	ON_UPDATE_COMMAND_UI(ID_IMAGE_NEXT, OnUpdateImageNext)
	ON_COMMAND(ID_IMAGE_PREV, OnImagePrev)
	ON_UPDATE_COMMAND_UI(ID_IMAGE_PREV, OnUpdateImagePrev)
	ON_COMMAND(ID_COMPOSITING_LAYER, OnCompositingLayer)
	ON_UPDATE_COMMAND_UI(ID_COMPOSITING_LAYER, OnUpdateCompositingLayer)
	//}}AFX_MSG_MAP
  ON_COMMAND(ID_TOOL_OVERVIEWWINDOW, &CKdu_showApp::OnToolOverviewwindow)
  ON_COMMAND(ID_TOOLS_RESET, &CKdu_showApp::OnToolOverviewReset)
  ON_COMMAND(ID_TOOLS_ATTACHOVERVIEW, &CKdu_showApp::OnToolOverviewAttach)
  ON_COMMAND_RANGE(ID_RENDERING_1X, ID_RENDERING_80X, &CKdu_showApp::OnChangeRenderScale)
  ON_COMMAND(ID_TOOL_EDITMETADATA, &CKdu_showApp::OnToolEditmetadata)
  ON_COMMAND(ID_MEASUREMENT_SCALING, &CKdu_showApp::OnMeasurementScaling)
//  ON_MESSAGE(NR_NEWROI, OnNewRoi)
ON_COMMAND(ID_FILE_SAVEAS, &CKdu_showApp::OnFileSaveasOtherType)
ON_UPDATE_COMMAND_UI(ID_TOOL_OVERVIEWWINDOW, &CKdu_showApp::OnUpdateToolOverviewwindow)
ON_UPDATE_COMMAND_UI(ID_TOOLS_RESET, &CKdu_showApp::OnUpdateToolOverviewResetwindow)
ON_UPDATE_COMMAND_UI(ID_TOOLS_ATTACHOVERVIEW, &CKdu_showApp::OnUpdateToolOverviewAttach)
ON_UPDATE_COMMAND_UI(ID_TOOL_EDITMETADATA, &CKdu_showApp::OnUpdateToolEditmetadata)
ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_SCALING, &CKdu_showApp::OnUpdateMeasurementScaling)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVEAS, &CKdu_showApp::OnUpdateFileSaveasOtherType)
ON_UPDATE_COMMAND_UI(ID_MEASUREMENT_SAVEMEASUREMENT, &CKdu_showApp::OnUpdateMeasurementSavemeasurement)
ON_COMMAND(ID_FILE_LOAD_CONF, &CKdu_showApp::OnFileLoadConf)
ON_UPDATE_COMMAND_UI(ID_FILE_LOAD_CONF, &CKdu_showApp::OnUpdateFileLoadConf)
ON_COMMAND(ID_FILE_SAVE_CONF_AS, &CKdu_showApp::OnFileSaveConfAs)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_CONF_AS, &CKdu_showApp::OnUpdateFileSaveConfAs)
ON_COMMAND(ID_GRID_SHOW, &CKdu_showApp::OnMeasurementGrid)
ON_UPDATE_COMMAND_UI(ID_GRID_SHOW, &CKdu_showApp::OnUpdateMeasurementGrid)
ON_COMMAND(ID_GRID_PROPERTY, &CKdu_showApp::OnMeasurementGridProperty)
ON_UPDATE_COMMAND_UI(ID_GRID_PROPERTY, &CKdu_showApp::OnUpdateMeasurementGridProperty)
ON_COMMAND(ID_EDIT_EXPORTMETADATAASSVG, &CKdu_showApp::OnEditExportmetadataassvg)
ON_UPDATE_COMMAND_UI(ID_EDIT_EXPORTMETADATAASSVG, &CKdu_showApp::OnUpdateExportmetadataassvg)

// NUMPAD
// ============================================================================
ON_COMMAND(ID_NUMPAD0, OnNumpadKey)
ON_COMMAND(ID_NUMPAD1, OnNumpadKey)
ON_COMMAND(ID_NUMPAD2, OnNumpadKey)
ON_COMMAND(ID_NUMPAD3, OnNumpadKey)
ON_COMMAND(ID_NUMPAD4, OnNumpadKey)
ON_COMMAND(ID_NUMPAD5, OnNumpadKey)
ON_COMMAND(ID_NUMPAD6, OnNumpadKey)
ON_COMMAND(ID_NUMPAD7, OnNumpadKey)
ON_COMMAND(ID_NUMPAD8, OnNumpadKey)
ON_COMMAND(ID_NUMPAD9, OnNumpadKey)
// ============================================================================

END_MESSAGE_MAP()


// NUMPAD
// ============================================================================
void CKdu_showApp::OnNumpadKey()
{
	if (child_wnd->m_pToolPlugInsMgr){
		if (child_wnd->m_pToolPlugInsMgr->OnKeyUp(child_wnd->GetSafeHwnd(), 0, 0, 0) != 0){
		}
	}
}
// ============================================================================


/******************************************************************************/
/*                         CKdu_showApp::CKdu_showApp                         */
/******************************************************************************/
CKdu_showApp::CKdu_showApp()
{

  child_wnd = NULL;

  open_file_pathname[0] = '\0';
  open_filename = NULL;
  compositor = NULL;
  num_recommended_threads = kdu_get_num_processors();
  if (num_recommended_threads < 2)
    num_recommended_threads = 0;
  num_threads = num_recommended_threads;
  if (num_threads > 0)
    {
      thread_env.create();
      for (int k=1; k < num_threads; k++)
        if (!thread_env.add_thread())
          num_threads = k;
    }
  in_idle = false;
  processing_suspended = false;

  allow_seeking = true;
  error_level = 0;
  max_display_layers = 1<<16;
  transpose = vflip = hflip = false;
  min_rendering_scale = -1.0F;
  rendering_scale = 1.0F;
  single_layer_idx = 0;
  second_mix_layer_idx = -1;
  save_layer_idx = -1;

  max_components = max_codestreams = max_compositing_layer_idx = -1;
  single_component_idx = single_codestream_idx = single_layer_idx = 0;

  fit_scale_to_window = false;
  configuration_complete = false;

  jpip_progress = NULL;
  status_id = KDS_STATUS_LAYER_RES;

  pixel_scale = 1;
  image_dims.pos = image_dims.size = kdu_coords(0,0);
  buffer_dims = view_dims = image_dims;
  view_centre_x = view_centre_y = 0.0F;
  view_centre_known = false;

  enable_focus_box = false;
  highlight_focus = true;
  focus_anchors_known = false;
  focus_centre_x = focus_centre_y = focus_size_x = focus_size_y = 0.0F;
  focus_pen.CreatePen(PS_DOT,0,0x000000FF);
  focus_box.pos = focus_box.size = kdu_coords(0,0);
  enable_region_posting = false;

  overlays_enabled = overlay_flashing_enabled = false;
  overlay_painting_intensity = overlay_painting_colour = 0;
  overlay_size_threshold = 1;

  view_buffer = buffer = second_buffer = mix_buffer = NULL;
	m_rotationStatus = ROTATE_NONE;
	
	// MJY : for mixing
	m_bMixing = false;

	m_Analyzing = false;
	
  refresh_timer_id = 0;
  flash_timer_id = 0;
  last_transferred_bytes = 0;

  m_pEditMetadata = NULL; //MJY
  m_pScaling = NULL;

  CDrawObj::InitDrawTools();
  m_pGrid = new CGrid;

  m_bExportMode = FALSE;
}

/******************************************************************************/
/*                        CKdu_showApp::~CKdu_showApp                         */
/******************************************************************************/

CKdu_showApp::~CKdu_showApp()
{
	GdiplusShutdown(gdiplusToken);

	m_config.writeViewingInfo(m_rotationStatus, rendering_scale, view_dims.pos.x, view_dims.pos.y);
  if (jpip_progress != NULL)
    { delete jpip_progress; jpip_progress = NULL; }
  if (compositor != NULL)
    { delete compositor; compositor = NULL; }
  if (thread_env.exists())
    thread_env.destroy();

  if (m_pEditMetadata != NULL)
	  delete m_pEditMetadata;
  if (m_pScaling != NULL)
	  delete m_pScaling;
  if (m_pOverview != NULL)
    delete m_pOverview;
  
	RELEASE_BUFFER(second_buffer);
	RELEASE_BUFFER(mix_buffer);
	RELEASE_BUFFER(view_buffer);
	
  m_config.Close();
  file_in.close();
  jpx_in.close();
  jp2_family_in.close();

  if (jpip_client){
	  jpip_client->close();
	  jpip_client->install_context_translator(NULL);

	  delete jpip_client;
	  jpip_client = NULL;
  }
  delete m_pGrid;
}

/******************************************************************************/
/*                         CKdu_showApp::InitInstance                         */
/******************************************************************************/

BOOL CKdu_showApp::InitInstance()
{
  AfxEnableControlContainer();
#if _MFC_VER < 0x0500
#  ifdef _AFXDLL
     Enable3dControls(); // Call this when using MFC in a shared DLL
#  else
     Enable3dControlsStatic(); // Call this when linking to MFC statically
#  endif
#endif // _MFC_VER
	 AfxInitRichEdit();

	//CHeck license
//#ifndef _DEBUG
#if 0
	 if (!CheckLicenseFile()){
		 CActivationWarningDlg dlg;
		 dlg.DoModal();
		 return FALSE;
	 }
#endif

	 GdiplusStartupInput GdiplusStartupInput;
	 if (::GdiplusStartup(&gdiplusToken, &GdiplusStartupInput, NULL) != Ok)
	 {
		 AfxMessageBox(_T("Error : Failed to initialize GDI+ library!"));
		 return FALSE;
	 }

  SetRegistryKey(_T("InnovaPlex"));
  LoadStdProfileSettings(4);
  settings.load_from_registry(this);

  frame_wnd = new CMainFrame;
  m_pMainWnd = frame_wnd;
  child_wnd = frame_wnd->get_child();
  child_wnd->set_app(this);
  m_DrawObjMgr.SetPixelBackBuf(&m_BackBufPixel);

  frame_wnd->LoadFrame(IDR_MAINFRAME,
                   WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU |
                   WS_THICKFRAME | WS_MINIMIZEBOX ,
                   NULL,NULL);
  frame_wnd->ShowWindow(SW_SHOW);
  frame_wnd->UpdateWindow();
  frame_wnd->set_app(this);
  frame_wnd->SetWindowText(_T("VP Viewer")); //MJY
  frame_wnd->DragAcceptFiles(TRUE);
  client_notifier.set_wnd(frame_wnd->m_hWnd);
      
  m_DrawObjMgr.SetNotifyReceiver(this);

  kdu_customize_errors(&err_formatter);
  kdu_customize_warnings(&warn_formatter);

    TCHAR *filename = m_lpCmdLine;
  while ((*filename != '\0') && ((*filename == ' ') || (*filename == '\t')))
    filename++;
  if (*filename != '\0')
    {
      if (*filename == '\"')
        {
		  TCHAR *ch = _tcsrchr(filename + 1, '\"');
          if (ch != NULL)
            {
              filename++;
              *ch = '\0';
            }
        }
      open_file(filename);
    }
  return TRUE;
}

/******************************************************************************/
/*                          CKdu_showApp::close_file                          */
/******************************************************************************/

void
CKdu_showApp::close_file()
{
	if (compositor != NULL)
	{
		delete compositor; compositor = NULL;
	}
	m_config.writeViewingInfo(m_rotationStatus, rendering_scale, view_dims.pos.x, view_dims.pos.y);
	file_in.close();
	jpx_in.close();
	jp2_family_in.close();
	if (jpip_client){
		jpip_client->close();
		delete jpip_client;
		jpip_client = NULL;
	}

	open_file_pathname[0] = '\0';
	open_filename = NULL;

	processing_suspended = false;
	fit_scale_to_window = false;
	configuration_complete = false;
	transpose = vflip = hflip = false;
	image_dims.pos = image_dims.size = kdu_coords(0, 0);
	buffer_dims = view_dims = image_dims;
	view_centre_known = false;
	focus_anchors_known = false;
	enable_focus_box = false;
	overlays_enabled = overlay_flashing_enabled = false;
	overlay_painting_intensity = overlay_painting_colour = 0;
	buffer = NULL;
	RELEASE_BUFFER(second_buffer);
	RELEASE_BUFFER(mix_buffer);
	RELEASE_BUFFER(view_buffer);
	
  if (refresh_timer_id != 0)
    m_pMainWnd->KillTimer(refresh_timer_id);
  if (flash_timer_id != 0)
    m_pMainWnd->KillTimer(flash_timer_id);
  refresh_timer_id = 0;
  flash_timer_id = 0;
  last_transferred_bytes = 0;
  if (child_wnd != NULL)
    {
      child_wnd->set_max_view_size(kdu_coords(20000,20000),pixel_scale);
      if (child_wnd->GetSafeHwnd() != NULL)
        child_wnd->Invalidate();
    }
  // MJY_102308 : destroy overview window 
  if (m_pOverview != NULL)
  {
	  m_pOverview->DestroyWindow();
	  m_pOverview = NULL;
  }
  if (m_pEditMetadata != NULL)
  {
	  m_pEditMetadata->DestroyWindow();
	  m_pEditMetadata = NULL;
  }
    
  m_rotationStatus = ROTATE_NONE;
  	
  m_bMixing = false;
  child_wnd->m_adjustment = false;
  //m_DrawObjMgr.DeleteObj();
  m_DrawObjMgr.m_pActiveObj = NULL;
   m_config.Close();
   m_pMainWnd->SetWindowText(_T("<no data>"));
   if (m_pGrid != NULL)
	   m_pGrid->DestroyWindow();

   frame_wnd->get_ToolPlguInsMgr()->OnClose();
}

/******************************************************************************/
/*                          CKdu_showApp::open_file                           */
/******************************************************************************/

void
  CKdu_showApp::open_file(TCHAR *filename)
{
  if (filename != NULL) close_file();
  assert(compositor == NULL);
  enable_focus_box = false;
  focus_box.size = kdu_coords(0,0);
  client_roi.init();
  processing_suspended = false;
  enable_region_posting = false;
  bool try_open_stream = false;

  USES_CONVERSION;
  try {
      if (filename != NULL)
        {
          if ((((toupper(filename[0]) == (int) 'J') &&
                (toupper(filename[1]) == (int) 'P') &&
                (toupper(filename[2]) == (int) 'I') &&
                (toupper(filename[3]) == (int) 'P')) ||
               ((toupper(filename[0]) == (int) 'H') &&
                (toupper(filename[1]) == (int) 'T') &&
                (toupper(filename[2]) == (int) 'T') &&
                (toupper(filename[3]) == (int) 'P'))) &&
              (filename[4] == ':'))
            { // Open as an interactive client-server application
              TCHAR *prefix = filename + 5;
              while ((*prefix == '/') || (*prefix == '\\'))
                prefix++;
              TCHAR *fstart;
              for (fstart=prefix; *fstart != '\0'; fstart++)
                if ((*fstart == '/') || (*fstart == '\\'))
                  break;
              if (*fstart == '\0')
                { kdu_error e;
                  e << "Illegal JPIP request, \"" << T2A(filename)
                    << "\".  General form is \"<prot>://<hostname>[:<port>]/"
                       "<resource>[?<query>]\"\nwhere <prot> is \"jpip\" or "
                       "\"http\" and forward slashes may be substituted "
                       "for back slashes if desired.";
                }
              *fstart = '\0';

			  jpip_client = new kdu_client();
			  
			  const TCHAR* user_id;
			  const TCHAR* user_passwd;

			  if (_tcscmp(settings.get_jpip_login_type(), _T("Anonymous")) == 0){
				  user_id = NULL;
				  user_passwd = NULL;
			  }
			  else{
				  user_id = settings.get_jpip_id();
				  user_passwd = settings.get_jpip_pw();
			  }
			  
			  if (settings.is_use_cache){
				  jpip_client->connect(T2A(prefix), T2A(settings.get_jpip_proxy()),
					  //T2A(user_id), T2A(user_passwd), add name and passwd to request, nned up update request 
					  T2A(fstart + 1), T2A(settings.get_jpip_channel_type()),
					  T2A(settings.get_jpip_cache()), KDU_CLIENT_MODE_INTERACTIVE);
			  }
			  else{
				  client_request_queue_id = jpip_client->connect(T2A(prefix), T2A(settings.get_jpip_proxy()),
					  //T2A(user_id), T2A(user_passwd), add name and passwd to request, nned up update request
					  T2A(fstart + 1), T2A(settings.get_jpip_channel_type()),
					  NULL, KDU_CLIENT_MODE_INTERACTIVE);
				  if (settings.jpip_buffer_len < 128) settings.jpip_buffer_len = 128;
				  jpip_client->set_preferred_memory_limit(settings.jpip_buffer_len * 1024*1024);
			  }
			  			  

              *fstart = '/';
              status_id = KDS_STATUS_CACHE;
              jpip_client->install_notifier(&client_notifier);
              cumulative_transmission_time = 0;
              last_transmission_start = 0;

              //jp2_family_in.open(&jpip_client);

			  try_open_stream = true;
		  }
          else
            { // Open as a local file
              status_id = KDS_STATUS_LAYER_RES;
			  jp2_family_in.open(T2A(filename), allow_seeking);
              compositor = new kd_bitmap_compositor(&thread_env);
              if (jpx_in.open(&jp2_family_in,true) >= 0)
                { // We have a JP2/JPX-compatible file.
                  compositor->create(&jpx_in);
                }
              
              else
                { // Incompatible with JP2/JPX. Try opening as raw stream
                  jp2_family_in.close();
				  file_in.open(T2A(filename), allow_seeking);
                  compositor->create(&file_in);
                }
              compositor->set_error_level(error_level);
            }
        }
    }
  catch (int)
    {
      close_file();
    }


  //network open일 경우 jpip_client가 activate될 때까지 기다린다. 
  if (try_open_stream && jpip_client){
	  try {

		  { // Complete initialization for the case where we have opened a
			  // JPIP cache file or a connection to a server
			  bool one_time_jpip_request = jpip_client->is_one_time_request();
			  //if ((client_request_queue_id == 0) && (!one_time_jpip_request) &&
			//	  jpip_client.connect_request_has_non_empty_window())
				
			  const char *open_filename = jpip_client->get_target_name();
			  const char *jpip_client_status = jpip_client->get_status(client_request_queue_id);
			  status_id = KDS_STATUS_CACHE;
			  cache_in.attach_to(jpip_client);
			  jp2_family_in.open(&cache_in);
			  //window->set_progress_bar(0.0);
		  }


		  for (int i = 0; i < 10 ; i++){	//MAX Wait 5 second
			  TRACE("OPne count [%d] \n", i);

			  Sleep(200);
			  if (jpip_client->is_active())
			  { // See if we can open the client yet
				  assert((compositor == NULL) && jp2_family_in.exists());
				  bool bin0_complete = false;
				  if (cache_in.get_databin_length(KDU_META_DATABIN, 0, 0, &bin0_complete) > 0)
				  { // Open as a JP2/JPX family file
					  if (jpx_in.open(&jp2_family_in, false))
					  {
						  compositor = new kd_bitmap_compositor(&thread_env);
						  compositor->create(&jpx_in);
					  }
					  jpip_client->install_context_translator(&jpx_client_translator);
				  }
				  else if (bin0_complete){ // Open as a raw file
					  assert(!jpx_in.exists());
					  bool hdr_complete;
					  cache_in.get_databin_length(KDU_MAIN_HEADER_DATABIN, 0, 0,
						  &hdr_complete);
					  if (hdr_complete)
					  {
						  cache_in.set_read_scope(KDU_MAIN_HEADER_DATABIN, 0, 0);
						  compositor = new kd_bitmap_compositor(&thread_env);
						  compositor->create(&cache_in);
					  }
				  }

				  if (compositor == NULL)
				  {
					   continue; // Come back later
					  /*
					  if (!jpip_client->is_alive())
					  {
						  kdu_error e; e << "Unable to recover sufficient information "
							  "from remote server (or a local cache) to open the image.";
					  }
					  */
				  }
				  compositor->set_error_level(error_level);
				  client_roi.init();
				  enable_region_posting = true;
			  }
			  else{
				  break;
			  }

			  if (compositor){
				  TRACE("OPne Succes \n");
				  break;
			  }

		  }

	  }
	  catch (int)
	  {
		  close_file();
	  }
  }
  
  //check read box header 
  if (compositor != NULL)
  {
	  if (!ReadInnovaplexCustomHeader(&jp2_family_in, &m_GeneratedSystemInfo, &m_InnovaplexCustomHeader)){
		  //fail read Innovaplex JP2K header.. 
		  TCHAR errMsg[256];
		  _stprintf(errMsg, _T("%s is not generated by Innovaplex."), filename);
		  //AfxMessageBox(errMsg);
		  //close_file();
		  //return;
	  }
  }


  if (compositor != NULL)
    {
	  max_display_layers = 1<<16;
      transpose = vflip = hflip = false;
      min_rendering_scale = -1.0F;
      rendering_scale = 1.0F;
      
	  single_component_idx = -1;
	  single_codestream_idx = 0;
	  max_components = -1;
	  frame_idx = 0;
	  frame_start = frame_end = 0.0;
	  num_frames = -1;
	  frame = NULL;
	  composition_rules = jpx_composition(NULL);

	  if (jpx_in.exists())
	  {
		  max_codestreams = -1;  // Unknown as yet
		  max_compositing_layer_idx = -1; // Unknown as yet
		  single_layer_idx = -1; // Try starting in composite frame mode
	  }
	  else
	  {
		  max_codestreams = 1;
		  max_compositing_layer_idx = 0;
		  num_frames = 0;
		  single_layer_idx = 0; // Start in composed single layer mode
	  }

	  save_layer_idx = -1;

      overlays_enabled = true; // Default starting position
      overlay_flashing_enabled = true;
      overlay_painting_intensity = overlay_painting_colour = 0;
      compositor->configure_overlays(overlays_enabled,overlay_size_threshold,
           (overlay_painting_colour<<8)+(overlay_painting_intensity & 0xFF));
	  
	  invalidate_configuration();
	  fit_scale_to_window = true;
	  m_first_open = true; //MJY_101308
      image_dims = buffer_dims = view_dims = kdu_dims();
      buffer = NULL;
	  RELEASE_BUFFER(second_buffer);
	  RELEASE_BUFFER(mix_buffer);
	  RELEASE_BUFFER(view_buffer);
	  
	  //load config file
	  if (!m_config.Open(filename, &m_DrawObjMgr)){
		  //m_config.LoadDefault();

		  initialize_regions();

		  m_config.GetImageExternalInfo()->layers.clear();
		  for (int i = 0; i < max_compositing_layer_idx + 1; i++){
			  LayerInfo info;
			  info.index = i + 1;
			  info.name = std::string("");
			  m_config.GetImageExternalInfo()->layers.push_back(info);
		  }
	  }else
		  initialize_regions();
	  /*기능 임시 disable
	  m_rotationStatus = eRotateStatus(m_config.getViewingInfo()->rotation);
	  rendering_scale = m_config.getViewingInfo()->zoom;
	  initialize_regions();
	  set_scroll_pos(m_config.getViewingInfo()->position.x, m_config.getViewingInfo()->position.y);
	  */
    }

  if (filename != NULL)
    {
      TCHAR title[256];
	  _tcscpy(title, _T("VP Viewer: "));
	  TCHAR *delim = _tcsrchr(filename, '\\');
	  if ((delim == NULL) || (delim[1] == '\0') || (jpip_client && jpip_client->is_active()))
        delim = filename-1;
	  _tcsncat(title, delim + 1, 255 - _tcslen(title));
      m_pMainWnd->SetWindowText(title);
	  int path_len;
	  if (jpip_client && jpip_client->is_active()){
		  _tcscpy(open_file_pathname, filename);
		  path_len = _tcslen(open_file_pathname);
	  }
	  else{
		  path_len = GetFullPathName(filename, MAX_PATH, open_file_pathname, &open_filename);
	  }

      if (path_len >= MAX_PATH)
        path_len--;
      open_file_pathname[path_len] = '\0';
      if (open_filename > open_file_pathname)
        {
          char sep = open_filename[-1];
          open_filename[-1] = '\0';
          settings.set_open_save_dir(open_file_pathname);
          open_filename[-1] = sep;
        }

    }
  
}

/******************************************************************************/
/*                        CKdu_showApp::increase_scale                        */
/******************************************************************************/

float
  CKdu_showApp::increase_scale(float from_scale)
{
  float min_scale = from_scale*1.30F;
  float max_scale = from_scale*2.70F;
  if (compositor == NULL)
    return min_scale;
  kdu_dims region_basis;
  if (configuration_complete)
    region_basis = (enable_focus_box)?focus_box:view_dims;
  return compositor->find_optimal_scale(region_basis,from_scale,
                                        min_scale,max_scale);
}

/******************************************************************************/
/*                        CKdu_showApp::decrease_scale                        */
/******************************************************************************/

float
  CKdu_showApp::decrease_scale(float from_scale)
{
  float max_scale = from_scale/1.3F;
  float min_scale = from_scale/2.7F;
  if (min_rendering_scale > 0.0F)
    {
      if (min_scale < min_rendering_scale)
        min_scale = min_rendering_scale;
      if (max_scale < min_rendering_scale)
        max_scale = min_rendering_scale;
    }
  if (compositor == NULL)
    return max_scale;
  kdu_dims region_basis;
  if (configuration_complete)
    region_basis = (enable_focus_box)?focus_box:view_dims;
  float new_scale = compositor->find_optimal_scale(region_basis,from_scale,
                                                   min_scale,max_scale);
  if (new_scale > max_scale)
    min_rendering_scale = new_scale; // This is as small as we can go
  return new_scale;
}

/******************************************************************************/
/*                       CKdu_showApp::change_frame_idx                       */
/******************************************************************************/

void
  CKdu_showApp::change_frame_idx(int new_frame_idx)
  /* Note carefully that, on entry to this function, only the `frame_start'
     time can be relied upon.  The function sets `frame_end' from scratch,
     rather than basing the newly calculated value on a previous one. */
{
  if (new_frame_idx < 0)
    new_frame_idx = 0;
  if ((num_frames >= 0) && (new_frame_idx >= (num_frames-1)))
    new_frame_idx = num_frames-1;
  if ((new_frame_idx == frame_idx) && (frame_end > frame_start))
    return; // Nothing to do

  if (composition_rules.exists() && (frame != NULL))
    {
      int num_instructions, duration, repeat_count, delta;
      bool is_persistent;

      if (new_frame_idx == 0)
        {
          frame = composition_rules.get_next_frame(NULL);
          frame_iteration = 0;
          frame_idx = 0;
          frame_start = 0.0;
          composition_rules.get_frame_info(frame,num_instructions,duration,
                                           repeat_count,is_persistent);
          frame_end = frame_start + 0.001*duration;
        }

      while (frame_idx < new_frame_idx)
        {
          composition_rules.get_frame_info(frame,num_instructions,duration,
                                           repeat_count,is_persistent);
          delta = repeat_count - frame_iteration;
          if (delta > 0)
            {
              if ((frame_idx+delta) > new_frame_idx)
                delta = new_frame_idx - frame_idx;
              frame_iteration += delta;
              frame_idx += delta;
              frame_start += delta * 0.001*duration;
              frame_end = frame_start + 0.001*duration;
            }
          else
            {
              jx_frame *new_frame = composition_rules.get_next_frame(frame);
              frame_end = frame_start + 0.001*duration; // Just in case
              if (new_frame == NULL)
                {
                  num_frames = frame_idx+1;
                  break;
                }
              else
                { frame = new_frame; frame_iteration=0; }
              composition_rules.get_frame_info(frame,num_instructions,duration,
                                               repeat_count,is_persistent);
              frame_start = frame_end;
              frame_end += 0.001*duration;
              frame_idx++;
            }
        }

      while (frame_idx > new_frame_idx)
        {
          composition_rules.get_frame_info(frame,num_instructions,duration,
                                           repeat_count,is_persistent);
          if (frame_iteration > 0)
            {
              delta = frame_idx - new_frame_idx;
              if (delta > frame_iteration)
                delta = frame_iteration;
              frame_iteration -= delta;
              frame_idx -= delta;
              frame_start -= delta * 0.001*duration;
              frame_end = frame_start + 0.001*duration;
            }
          else
            {
              frame = composition_rules.get_prev_frame(frame);
              assert(frame != NULL);
              composition_rules.get_frame_info(frame,num_instructions,duration,
                                               repeat_count,is_persistent);
              frame_iteration = repeat_count;
              frame_idx--;
              frame_start -= 0.001*duration;
              frame_end = frame_start + 0.001*duration;
            }
        }
    }

}

/******************************************************************************/
/*                   CKdu_showApp::invalidate_configuration                   */
/******************************************************************************/

void
  CKdu_showApp::invalidate_configuration()
{
  configuration_complete = false;
  max_components = -1; // Changed config may have different num image components
  buffer = NULL;
    
  if (compositor != NULL)
	  compositor->remove_ilayer(kdu_ilayer_ref(), false);
}


/******************************************************************************/
/*                      CKdu_showApp::initialize_regions                      */
/******************************************************************************/

void
CKdu_showApp::initialize_regions(bool bUpdateOverlay)
{
	
  if (child_wnd != NULL)
    child_wnd->cancel_focus_drag();

  bool first_time_through = fit_scale_to_window;
        // Use this flag later to determine if we need to fire off the play
        // controls.

  // Reset the buffer and view dimensions to zero size.

  buffer = NULL;
  frame_expander.reset();
  while (!configuration_complete)
    { // Install configuration
      try {
// //TRACE("inside configuratoin_complete is not true\n");
          if (single_component_idx >= 0)
            { // Check for valid codestream index before opening
              if (jpx_in.exists())
                {
                  int count=max_codestreams;
                  if ((count < 0) && !jpx_in.count_codestreams(count))
                    { // Actual number not yet known, but have at least `count'
                      if (single_codestream_idx >= count)
                        return; // Come back later once more data is in cache
                    }
                  else
                    { // Number of codestreams is now known
                      max_codestreams = count;
                      if (single_codestream_idx >= max_codestreams)
                        single_codestream_idx = max_codestreams-1;
                    }
                }
           
              else
                { // Raw codestream
                  single_codestream_idx = 0;
                }
			  if (!compositor->add_primitive_ilayer(single_codestream_idx,
                                        single_component_idx,
										KDU_WANT_CODESTREAM_COMPONENTS, kdu_dims(), kdu_dims()))
              { // Cannot open codestream yet; waiting for cache
                  update_client_window_of_interest();
                  return;
                }
			  kdu_istream_ref str;
			  str = compositor->get_next_istream(str);
              kdu_codestream codestream = compositor->access_codestream(str);
              codestream.apply_input_restrictions(0,0,0,0,NULL);
              max_components = codestream.get_num_components();
              if (single_component_idx >= max_components)
                single_component_idx = max_components-1;
            }
          else if (single_layer_idx >= 0)
            { // Check for valid compositing layer index before opening
              if (jpx_in.exists())
                {
                  frame_idx = 0;
                  frame_start = frame_end = 0.0;
                  int count=max_compositing_layer_idx+1;
                  if ((count <= 0) && !jpx_in.count_compositing_layers(count))
                    { // Actual number not yet known, but have at least `count'
                      if (single_layer_idx >= count)
                        return; // Come back later once more data is in cache
                    }
                  else
                    { // Number of compositing layers is now known
                      max_compositing_layer_idx = count-1;
                      if (single_layer_idx > max_compositing_layer_idx)
                        single_layer_idx = max_compositing_layer_idx;
                    }
                }
              
              else
                { // Raw codestream
                  single_layer_idx = 0;
                }
			  //TRACE("Initialize_regions::single_layer_idx = %d \n", single_layer_idx);
			  if (!compositor->add_ilayer(single_layer_idx,
											kdu_dims(), kdu_dims()))
                { // Cannot open compositing layer yet; waiting for cache
                  if (jpip_client->is_one_time_request())
                    { // See if request is compatible with opening a layer
                      if (!check_one_time_request_compatibility())         
                        continue; // Mode changed to single component
                    }
                  else
                    update_client_window_of_interest();
                  return;
                }
			  ////TRACE ("single_layer_idx = %d\n", single_layer_idx);
            }
          else
            { // Attempt to open frame
              if (num_frames == 0)
                { // Downgrade to single layer mode
                  single_layer_idx = 0;
                  frame_idx = 0;
                  frame_start = frame_end = 0.0;
                  frame = NULL;
                  continue;
                }
              assert(jpx_in.exists());
              if (frame == NULL)
                {
                  if (!composition_rules)
                    composition_rules = jpx_in.access_composition();
				  if (!composition_rules){
					  TRACE("=== initialize_regions return 4 === \n");
					  Sleep(200);
					  continue;
                    //return; // Cannot open composition rules yet; wait for cache
				  }
                  assert((frame_idx == 0) && (frame_iteration == 0));
                  frame = composition_rules.get_next_frame(NULL);
                  if (frame == NULL)
                    { // Downgrade to single layer mode
                      single_layer_idx = 0;
                      frame_idx = 0;
                      frame_start = frame_end = 0.0;
                      num_frames = 0;
                      continue;
                    }
                }

              if (!frame_expander.construct(&jpx_in,frame,frame_iteration,true))
                {
                  if (jpip_client->is_one_time_request())
                    { // See if request is compatible with opening a frame
                      if (!check_one_time_request_compatibility())         
                        continue; // Mode changed to single layer or component
                    }
                  else
                    update_client_window_of_interest();
                  return; // Can't open all frame members yet; waiting for cache
                }
          
              if (frame_expander.get_num_members() == 0)
                { // Requested frame does not exist
                  if ((num_frames < 0) || (num_frames > frame_idx))
                    num_frames = frame_idx;
                  if (frame_idx == 0)
                    { kdu_error e; e << "Cannot render even the first "
                      "composited frame described in the JPX composition "
                      "box due to unavailability of the required compositing "
                      "layers in the original file.  Viewer will fall back "
                      "to single layer rendering mode."; }
                  change_frame_idx(frame_idx-1);
                  continue; // Loop around to try again
                }

            }
		  if (fit_scale_to_window && (jpip_client && jpip_client->is_one_time_request()) &&
              !check_one_time_request_compatibility())
            { // Check we are opening the image in the intended manner
			  compositor->remove_ilayer(kdu_ilayer_ref(), false);
              continue; // Open again in different mode
            }
          compositor->set_max_quality_layers(max_display_layers);
		  compositor->cull_inactive_ilayers(3); // Really an arbitrary amount of
												// culling in this demo app.
          configuration_complete = true;
          min_rendering_scale=-1.0F; // Need to discover from scratch each time
		}
      catch (int) { // Try downgrading to a more primitive viewing mode
          if ((single_component_idx >= 0) ||
              !(jpx_in.exists()))
            { // Already in single component mode; nothing more primitive exists
              close_file();
              return;
            }
          else if (single_layer_idx >= 0)
            { // Downgrade from single layer mode to single component mode
              single_component_idx = 0;
              single_codestream_idx = 0;
              single_layer_idx = -1;
              if (enable_region_posting)
                update_client_window_of_interest();
            }
          else
            { // Downgrade from frame animation mode to single layer mode
              frame = NULL;
              num_frames = 0;
              frame_idx = 0;
              frame_start = frame_end = 0.0;
              single_layer_idx = 0;
            }
        }
    }


  // Get size at current scale, possibly adjusting the scale if this is the
  // first time through

  float new_rendering_scale = rendering_scale;
  ////TRACE("before while loop %5.2f single_layer_idx %d\n", rendering_scale, single_layer_idx);
  kdu_dims new_image_dims = image_dims;
  try {
      bool found_valid_scale=false;
      while (fit_scale_to_window || !found_valid_scale )
        {
          if ((min_rendering_scale > 0.0F) &&
              (new_rendering_scale < min_rendering_scale))
            new_rendering_scale = min_rendering_scale;
//		  //TRACE("inside while loop new rendering scale %5.2f\n", new_rendering_scale);
          compositor->set_scale(transpose,vflip,hflip,new_rendering_scale);
          found_valid_scale = 
            compositor->get_total_composition_dims(new_image_dims);
          if (!found_valid_scale)
            { // Increase scale systematically before trying again
              int invalid_scale_code = compositor->check_invalid_scale_code();
              if (invalid_scale_code & KDU_COMPOSITOR_SCALE_TOO_SMALL)
                {
                  min_rendering_scale = increase_scale(new_rendering_scale);
                  if (new_rendering_scale > 1000.0F)
                    {
                      if (single_component_idx >= 0)
                        { kdu_error e;
                          e << "Cannot display the image.  Seems to "
                          "require some non-trivial scaling.";
                        }
                      else
                        {
                        { kdu_warning w; w << "Cannot display the image.  "
                        "Seems to require some non-trivial scaling.  "
                        "Downgrading to single component mode.";
                        }
                          single_component_idx = 0;
                        invalidate_configuration();
						if (!compositor->add_primitive_ilayer(0,
									single_component_idx,
									KDU_WANT_CODESTREAM_COMPONENTS,
									kdu_dims(), kdu_dims()))
							return;
                        configuration_complete = true;
                    }
                    }
                }
              else if ((invalid_scale_code & KDU_COMPOSITOR_CANNOT_FLIP) &&
                       (vflip || hflip))
                {
                  kdu_warning w; w << "This image cannot be decompressed "
                    "with the requested geometry (horizontally or vertically "
                    "flipped), since it employs a packet wavelet transform "
                    "in which horizontally (resp. vertically) high-pass "
                    "subbands are further decomposed in the horizontal "
                    "(resp. vertical) direction.  Only transposed decoding "
                    "will be permitted.";
                  vflip = hflip = false;
                }
              else
                {
                  if (single_component_idx >= 0)
                    { kdu_error e;
                      e << "Cannot display the image.  Unexplicable error "
                        "code encountered in call to "
                        "`kdu_region_compositor::get_total_composition_dims'.";
                    }
                  else
                    {
                    { kdu_warning w; w << "Cannot display the image.  "
                    "Seems to require some incompatible set of geometric "
                    "manipulations for the various composed codestreams.";
                    }
                      single_component_idx = 0;
                    invalidate_configuration();
					if (!compositor->add_primitive_ilayer(0,
														single_component_idx,
														KDU_WANT_CODESTREAM_COMPONENTS,
														kdu_dims(), kdu_dims()))
						return;
                    configuration_complete = true;
                }
            }
            }
          else if (fit_scale_to_window)
            {
				kdu_coords max_tgt_size;

				max_tgt_size = kdu_coords(700/pixel_scale,700/pixel_scale);

				if (jpip_client && jpip_client->is_one_time_request() &&
				  jpip_client->get_window_in_progress(&tmp_roi) &&
                  (tmp_roi.resolution.x > 0) && (tmp_roi.resolution.y > 0))
                { // Roughly fit scale to match the resolution
                  max_tgt_size = tmp_roi.resolution;
                  max_tgt_size.x += (3*max_tgt_size.x)/4;
                  max_tgt_size.y += (3*max_tgt_size.y)/4;
                  if (!tmp_roi.region.is_empty())
                    { // Configure view and focus box based on one-time request
                      focus_centre_x = (((float) tmp_roi.region.pos.x) +
                                        0.5F*((float) tmp_roi.region.size.x)) /
                                       ((float) tmp_roi.resolution.x);
                      focus_centre_y = (((float) tmp_roi.region.pos.y) +
                                        0.5F*((float) tmp_roi.region.size.y)) /
                                       ((float) tmp_roi.resolution.y);
                      focus_size_x = ((float) tmp_roi.region.size.x) /
                                     ((float) tmp_roi.resolution.x);
                      focus_size_y = ((float) tmp_roi.region.size.y) /
                                     ((float) tmp_roi.resolution.y);
                      focus_codestream = focus_layer = -1;
                      if (single_component_idx >= 0)
                        focus_codestream = single_codestream_idx;
                      else if (single_layer_idx >= 0)
                        focus_layer = single_layer_idx;
                      enable_focus_box = true;
                      view_centre_x = focus_centre_x;
                      view_centre_y = focus_centre_y;
                      view_centre_known = true;
                    }
                }

              float max_x = new_rendering_scale *
                ((float) max_tgt_size.x) / ((float) new_image_dims.size.x);
              float max_y = new_rendering_scale *
                ((float) max_tgt_size.y) / ((float) new_image_dims.size.y);
			  ////TRACE("max_x max_y new_rendering_scale : %5.2f %5.2f %5.2f\n", max_x, max_y, new_rendering_scale);
              while (((min_rendering_scale < 0.0) ||
                      (new_rendering_scale > min_rendering_scale)) &&
                     ((new_rendering_scale > max_x) ||
                      (new_rendering_scale > max_y)))
                {
					////TRACE("decresing...scale...\n");
                  new_rendering_scale = decrease_scale(new_rendering_scale);
	  			  ////TRACE("max_x max_y new_rendering_scale : %5.2f %5.2f %5.2f\n", max_x, max_y, new_rendering_scale);

                  found_valid_scale = false;
                }
              fit_scale_to_window = false;
            }
//			//TRACE("fit_scale_to_window %d found_valid_scale %d\n", fit_scale_to_window, found_valid_scale);
		} // end of while
    }
  catch (int) { // Some error occurred while parsing code-streams
      close_file();
      return;
    }


  // Install the dimensioning parameters we just found, checking to see
  // if the window needs to be resized.
  rendering_scale = new_rendering_scale;
//  //TRACE("rendering_scale %5.2f\n", new_rendering_scale);
  if (m_pOverview == NULL)
  {
	  m_pOverview = new COverview(this);
  }

  if (bUpdateOverlay)
  {
	  compositor->set_scale(transpose, vflip, hflip, 1.0F);
	  compositor->get_total_composition_dims(whole_image_dims);
	  m_config.writeResolution(whole_image_dims.size.get_x(), whole_image_dims.size.get_y());

	  float x_rate = (float)(m_pOverview->GetOverviewFrameRect()->Width()) / (float)whole_image_dims.size.x;
	  float y_rate = (float)(m_pOverview->GetOverviewFrameRect()->Height()) / (float)whole_image_dims.size.y;
	  float overview_scale = (x_rate > y_rate) ? y_rate : x_rate;
	  kdu_dims new_region_dims, overview_image_dims;

	  compositor->set_scale(transpose, vflip, hflip, overview_scale);
	  compositor->get_total_composition_dims(overview_image_dims);
	  compositor->set_buffer_surface(overview_image_dims);
	  buffer = compositor->get_composition_bitmap(overview_image_dims);

	  int decode_size = overview_image_dims.size.x * overview_image_dims.size.y * 4;
	
	  if (jpip_client && jpip_client->is_active()){
		  //request send... 
		  update_client_window_of_interest();
	
		  //wait transmit complete 
		  WaitTransmitComplete();
	  }
		  
	  while (compositor->process(decode_size, new_region_dims)){
		  Sleep(10);
	  }


	  int bitmap_row_gap;
	  kdu_uint32 *bitmap_buffer;
	  HBITMAP bitmap = buffer->access_bitmap(bitmap_buffer, bitmap_row_gap);

	  m_pOverview->SetOverviewData(
		  overview_image_dims.size.x,
		  overview_image_dims.size.y,
		  (char *)bitmap_buffer
		  );
  }
  compositor->set_scale(transpose, vflip, hflip, new_rendering_scale);
    
  // MJY : for overview window
	if (m_first_open)
	{
		m_first_open = false;

		//find matched scale
		if (rendering_scale < FIX_SCALE_VALUE[SCALE_1X]) rendering_scale = FIX_SCALE_VALUE[SCALE_1X];
		else if (rendering_scale < FIX_SCALE_VALUE[SCALE_2X]) rendering_scale = FIX_SCALE_VALUE[SCALE_2X];
		else if (rendering_scale < FIX_SCALE_VALUE[SCALE_5X]) rendering_scale = FIX_SCALE_VALUE[SCALE_5X];
		else if (rendering_scale < FIX_SCALE_VALUE[SCALE_10X]) rendering_scale = FIX_SCALE_VALUE[SCALE_10X];
		else if (rendering_scale < FIX_SCALE_VALUE[SCALE_20X]) rendering_scale = FIX_SCALE_VALUE[SCALE_20X];
		else rendering_scale = FIX_SCALE_VALUE[SCALE_40X];

		if (jpip_client && jpip_client->is_active()){
			
			//network로 접속으로 할 경우는 먼저 mainview의 보여 줄 데이타를 가져오고 다음으로 overview 창에 보여 줄 데이타를 가져온다.
			compositor->set_buffer_surface(buffer_dims);
			////TRACE("WHEN we set buffer surface.....................\n");
			buffer = compositor->get_composition_bitmap(buffer_dims);
			UpdateViewDim(buffer_dims);

			m_pMainWnd->SetTimer(KDU_SHOW_REQUEST_REFRESH_TIMER_IDENT, 1, NULL);
			return;
		}else{
			initialize_regions(TRUE); 
		}
	}
	else {

		compositor->set_scale(transpose, vflip, hflip, new_rendering_scale);

		if ((image_dims == new_image_dims) && (!view_dims.is_empty()) && (!buffer_dims.is_empty()))
		{ // No need to resize the window

			//TRACE("initialize_regions:if part..............\n");
			compositor->set_buffer_surface(buffer_dims);
			////TRACE("WHEN we set buffer surface.....................\n");
			buffer = compositor->get_composition_bitmap(buffer_dims);
			////TRACE("buffer_dims %d %d %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y, buffer_dims.size.x, buffer_dims.size.y);
			////TRACE("image_dims %d %d %d %d\n", image_dims.pos.x, image_dims.pos.y, image_dims.size.x, image_dims.size.y);

			UpdateViewDim(buffer_dims);
		}
		else
		{ // Send a message to the child view window identifying the
			// maximum allowable image dimensions.  We expect to hear back (possibly
			// after some windows message processing) concerning
			// the actual view dimensions via the `set_view_size' function.
			////TRACE("CHILD INVALIDATE...............\n");
			//TRACE("initialize_regions:else part..............\n");

			view_dims = buffer_dims = kdu_dims();
			image_dims = new_image_dims;

			//request send... 
			//update_client_window_of_interest();

			//wait transmit complete 
			//WaitTransmitComplete();

			child_wnd->set_max_view_size(image_dims.size, pixel_scale);
			child_wnd->Invalidate();

		}
	}
		
	// MJY : ratio of overview image to viewing image
	//overview_rate = (float)overview_image_dims.size.x/(float)image_dims.size.x;
	display_status();
}

/******************************************************************************/
/*                      CKdu_showApp::suspend_processing                      */
/******************************************************************************/

void
  CKdu_showApp::suspend_processing(bool suspend)
{
  processing_suspended = suspend;
}

/******************************************************************************/
/*                       CKdu_showApp::refresh_display                        */
/******************************************************************************/

void
  CKdu_showApp::refresh_display()
{
	if ((compositor == NULL))  
    return;
  if (configuration_complete && !compositor->refresh())
    { // Can no longer trust buffer surfaces
      kdu_dims new_image_dims;
      bool have_valid_scale = 
        compositor->get_total_composition_dims(new_image_dims);
      if ((!have_valid_scale) || (new_image_dims != image_dims))
        initialize_regions();
      else
        buffer = compositor->get_composition_bitmap(buffer_dims);
    }
  if (refresh_timer_id != 0)
    { // Kill outstanding requests for a delayed refresh.
      m_pMainWnd->KillTimer(refresh_timer_id);
      refresh_timer_id = 0;
    }

  buffer = compositor->get_composition_bitmap(buffer_dims);
   UpdateViewDim(buffer_dims);
}

/******************************************************************************/
/*                       CKdu_showApp::flash_overlays                         */
/******************************************************************************/

void
  CKdu_showApp::flash_overlays()
{
  if (flash_timer_id != 0)
    { // Kill outstanding requests for overlay flashing
      m_pMainWnd->KillTimer(flash_timer_id);
      flash_timer_id = 0;
    }
  if (!(overlays_enabled && overlay_flashing_enabled && (compositor != NULL)))
    return;
  overlay_painting_colour++;
  compositor->configure_overlays(overlays_enabled,overlay_size_threshold,
    (overlay_painting_colour<<8)+(overlay_painting_intensity & 0xFF));
}

/******************************************************************************/
/*                    CKdu_showApp::calculate_view_anchors                    */
/******************************************************************************/

void
  CKdu_showApp::calculate_view_anchors()
{
  if ((buffer == NULL) || !image_dims)
    return;
  view_centre_known = true;
  view_centre_x =
    (float)(view_dims.pos.x + view_dims.size.x/2 - image_dims.pos.x) /
      ((float) image_dims.size.x);
  view_centre_y =
    (float)(view_dims.pos.y + view_dims.size.y/2 - image_dims.pos.y) /
      ((float) image_dims.size.y);
  focus_codestream = focus_layer = -1;
  if (single_component_idx >= 0)
    focus_codestream = single_codestream_idx;
  else if (single_layer_idx >= 0)
    focus_layer = single_layer_idx;
}


/******************************************************************************/
/*                        CKdu_showApp::set_view_size                         */
/******************************************************************************/

void
  CKdu_showApp::set_view_size(kdu_coords size)
{
  if ((compositor == NULL) || !configuration_complete || m_bExportMode )
    return;
  if (child_wnd != NULL)
    child_wnd->cancel_focus_drag();
    
  x_offset = y_offset = 0;
  if (image_dims.size.x < size.x)	  x_offset = size.x/2 - image_dims.size.x/2;
  if (image_dims.size.y < size.y)	  y_offset = size.y/2 - image_dims.size.y/2;
  
  // Set view region to the largest subset of the image region consistent with
  // the size of the new viewing region.
  kdu_dims new_view_dims = view_dims;
  new_view_dims.size = size;
  if (view_centre_known)
    {
      new_view_dims.pos.x = image_dims.pos.x - (size.x / 2) +
        (int) floor(0.5 + image_dims.size.x*view_centre_x);
      new_view_dims.pos.y = image_dims.pos.y - (size.y / 2) +
        (int) floor(0.5 + image_dims.size.y*view_centre_y);
      view_centre_known = false;
    }
  if (new_view_dims.pos.x < image_dims.pos.x)
    new_view_dims.pos.x = image_dims.pos.x;
  if (new_view_dims.pos.y < image_dims.pos.y)
    new_view_dims.pos.y = image_dims.pos.y;
  kdu_coords view_lim = new_view_dims.pos + new_view_dims.size;
  kdu_coords image_lim = image_dims.pos + image_dims.size;
  if (view_lim.x > image_lim.x)
    new_view_dims.pos.x -= view_lim.x-image_lim.x;
  if (view_lim.y > image_lim.y)
    new_view_dims.pos.y -= view_lim.y-image_lim.y;
  new_view_dims &= image_dims;
  bool need_redraw = new_view_dims.pos != view_dims.pos;
  view_dims = new_view_dims;

  // Get preferred minimum dimensions for the new buffer region.
  size = view_dims.size;
  
  if (size.x > image_dims.size.x)
    size.x = image_dims.size.x;
  if (size.y > image_dims.size.y)
    size.y = image_dims.size.y;
  kdu_dims new_buffer_dims;
  new_buffer_dims.size = size;
  new_buffer_dims.pos = buffer_dims.pos;

  // Make sure the buffer region is contained within the image
  kdu_coords buffer_lim = new_buffer_dims.pos + new_buffer_dims.size;
  if (buffer_lim.x > image_lim.x)
    new_buffer_dims.pos.x -= buffer_lim.x-image_lim.x;
  if (buffer_lim.y > image_lim.y)
    new_buffer_dims.pos.y -= buffer_lim.y-image_lim.y;
  if (new_buffer_dims.pos.x < image_dims.pos.x)
    new_buffer_dims.pos.x = image_dims.pos.x;
  if (new_buffer_dims.pos.y < image_dims.pos.y)
    new_buffer_dims.pos.y = image_dims.pos.y;
  assert(new_buffer_dims == (new_buffer_dims & image_dims));

  // See if the buffered region includes any new locations at all.
  if ((new_buffer_dims.pos != buffer_dims.pos) ||
      (new_buffer_dims != (new_buffer_dims & buffer_dims)) ||
      (view_dims != (view_dims & new_buffer_dims)))
    { // We will need to reshuffle or resize the buffer anyway, so might
      // as well get the best location for the buffer.
      new_buffer_dims.pos.x = view_dims.pos.x;
      new_buffer_dims.pos.y = view_dims.pos.y;
      new_buffer_dims.pos.x -= (new_buffer_dims.size.x-view_dims.size.x)/2;
      new_buffer_dims.pos.y -= (new_buffer_dims.size.y-view_dims.size.y)/2;
      if (new_buffer_dims.pos.x < image_dims.pos.x)
        new_buffer_dims.pos.x = image_dims.pos.x;
      if (new_buffer_dims.pos.y < image_dims.pos.y)
        new_buffer_dims.pos.y = image_dims.pos.y;
      buffer_lim = new_buffer_dims.pos + new_buffer_dims.size;
      if (buffer_lim.x > image_lim.x)
        new_buffer_dims.pos.x -= buffer_lim.x - image_lim.x;
      if (buffer_lim.y > image_lim.y)
        new_buffer_dims.pos.y -= buffer_lim.y - image_lim.y;
      assert(view_dims == (view_dims & new_buffer_dims));
      assert(new_buffer_dims == (image_dims & new_buffer_dims));
      assert(view_dims == (new_buffer_dims & view_dims));
    }

  UpdateViewDim(new_buffer_dims, need_redraw);
   
  if (jpip_progress != NULL)
    { delete jpip_progress;  jpip_progress = NULL; }
}


/******************************************************************************/
/*                       CKdu_showApp::set_scroll_pos                         */
/******************************************************************************/

void  CKdu_showApp::set_scroll_pos(int pos_x, int pos_y, bool relative_to_last)
{
	//TRACE("set_scroll_pos %d %d\n", pos_x, pos_y);
		
  if ((compositor == NULL) || (buffer == NULL))   return;

  view_centre_known = false;
  if (relative_to_last)
    {
      pos_y += view_dims.pos.y;
      pos_x += view_dims.pos.x;
    }
  else
    {
      pos_y += image_dims.pos.y;
      pos_x += image_dims.pos.x;
    }
  if (pos_y < image_dims.pos.y)
    pos_y = image_dims.pos.y;
  if ((pos_y+view_dims.size.y) > (image_dims.pos.y+image_dims.size.y))
    pos_y = image_dims.pos.y+image_dims.size.y-view_dims.size.y;
  if (pos_x < image_dims.pos.x)
    pos_x = image_dims.pos.x;
  if ((pos_x+view_dims.size.x) > (image_dims.pos.x+image_dims.size.x))
    pos_x = image_dims.pos.x+image_dims.size.x-view_dims.size.x;

  if ((pos_y != view_dims.pos.y) || (pos_x != view_dims.pos.x))
    {
	  view_dims.pos.x = pos_x;
      view_dims.pos.y = pos_y;
      if (view_dims != (view_dims & buffer_dims))
        { // The view is no longer fully contained in the buffered region.
          buffer_dims.pos.x =
            view_dims.pos.x - (buffer_dims.size.x-view_dims.size.x)/2;
          buffer_dims.pos.y =
            view_dims.pos.y - (buffer_dims.size.y-view_dims.size.y)/2;
          if (buffer_dims.pos.x < image_dims.pos.x)
            buffer_dims.pos.x = image_dims.pos.x;
          if (buffer_dims.pos.y < image_dims.pos.y)
            buffer_dims.pos.y = image_dims.pos.y;
          kdu_coords image_lim = image_dims.pos + image_dims.size;
          kdu_coords buf_lim = buffer_dims.pos + buffer_dims.size;
          if (buf_lim.y > image_lim.y)
            buffer_dims.pos.y -= (buf_lim.y - image_lim.y);
          if (buf_lim.x > image_lim.x)
            buffer_dims.pos.x -= (buf_lim.x - image_lim.x);

		  kdu_dims new_buffer_dims = buffer_dims;
		  UpdateViewDim(new_buffer_dims);
        }
    }
}

bool CKdu_showApp::validation_check(int pos_x, int pos_y)
{
	//validation check
	bool valid = false;

	if (rendering_scale > 0.99)
		return true;

	return valid;
}

void  CKdu_showApp::bring_to_center(int pos_x, int pos_y)
{
	if ((compositor == NULL) || (buffer == NULL))   return;

	float posX = float(pos_x + view_dims.pos.x) / float(image_dims.size.x);
	float posY = float(pos_y + view_dims.pos.y) / float(image_dims.size.y);

	pos_x += (view_dims.pos.x - view_dims.size.x / 2);
	pos_y += (view_dims.pos.y - view_dims.size.y / 2);

	//update rendering scale

	//validation check
	while (!validation_check(pos_x, pos_y))
	{		
		rendering_scale = increase_scale(rendering_scale);
		calculate_view_anchors();
		if (focus_anchors_known)
		{
			view_centre_x = focus_centre_x; view_centre_y = focus_centre_y;
		}
		initialize_regions();

		pos_x = posX * image_dims.size.x- view_dims.size.x / 2;
		pos_y = posY * image_dims.size.y- view_dims.size.y / 2;

	}

	if (pos_y < image_dims.pos.y)
		pos_y = image_dims.pos.y;
	if ((pos_y + view_dims.size.y) >(image_dims.pos.y + image_dims.size.y))
		pos_y = image_dims.pos.y + image_dims.size.y - view_dims.size.y;
	if (pos_x < image_dims.pos.x)
		pos_x = image_dims.pos.x;
	if ((pos_x + view_dims.size.x) >(image_dims.pos.x + image_dims.size.x))
		pos_x = image_dims.pos.x + image_dims.size.x - view_dims.size.x;

	if ((pos_y != view_dims.pos.y) || (pos_x != view_dims.pos.x))
	{
		view_dims.pos.x = pos_x;
		view_dims.pos.y = pos_y;
		if (view_dims != (view_dims & buffer_dims))
		{ // The view is no longer fully contained in the buffered region.
			buffer_dims.pos.x =
				view_dims.pos.x - (buffer_dims.size.x - view_dims.size.x) / 2;
			buffer_dims.pos.y =
				view_dims.pos.y - (buffer_dims.size.y - view_dims.size.y) / 2;
			if (buffer_dims.pos.x < image_dims.pos.x)
				buffer_dims.pos.x = image_dims.pos.x;
			if (buffer_dims.pos.y < image_dims.pos.y)
				buffer_dims.pos.y = image_dims.pos.y;
			kdu_coords image_lim = image_dims.pos + image_dims.size;
			kdu_coords buf_lim = buffer_dims.pos + buffer_dims.size;
			if (buf_lim.y > image_lim.y)
				buffer_dims.pos.y -= (buf_lim.y - image_lim.y);
			if (buf_lim.x > image_lim.x)
				buffer_dims.pos.x -= (buf_lim.x - image_lim.x);

			kdu_dims new_buffer_dims = buffer_dims;
			UpdateViewDim(new_buffer_dims);
		}
	}
}

void CKdu_showApp::set_center_pos(float center_x, float center_y, float scale)
{
	if ((compositor == NULL) || !configuration_complete)
		return;
	
	if (rendering_scale == scale){
		//reset 
		view_dims = buffer_dims = kdu_dims();
	}
	else{
		rendering_scale = scale;
	}
		
	view_centre_known = true;
	view_centre_x = center_x;
	view_centre_y = center_y;

	initialize_regions();
	if (m_pOverview != NULL) m_pOverview->Invalidate();
}



HBITMAP  CKdu_showApp::update_region(kdu_dims& region)
{

  if (m_bExportMode) return NULL;

  region.pos += view_dims.pos;
  kdu_dims region_on_buffer = region;
  region_on_buffer &= buffer_dims; // Intersect with buffer region

//TRACE("buffer_dims %d %d %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y, buffer_dims.size.x, buffer_dims.size.y);

  region_on_buffer.pos -= buffer_dims.pos; // Make relative to buffer region
//TRACE("region_on_buffer %d %d %d %d\n", region_on_buffer.pos.x, region_on_buffer.pos.y, region_on_buffer.size.x, region_on_buffer.size.y);

  if (region_on_buffer.size != region.size)
    { 
      region = region_on_buffer;
      region.pos += buffer_dims.pos; // Convert to absolute region
    }

  region.pos -= view_dims.pos; // Make relative to curent view
  if ((!region_on_buffer) || (buffer == NULL))
    return NULL;
  assert(region.size == region_on_buffer.size);

  int bitmap_row_gap;
  kdu_uint32 *image_bitmap_buffer;
  kdu_uint32 *view_bitmap_buffer;
  HBITMAP bitmap; 
  if (m_bMixing)
  {
	  mix_buffer->access_bitmap(image_bitmap_buffer, bitmap_row_gap);
  }
  else
  {
	  buffer->access_bitmap(image_bitmap_buffer, bitmap_row_gap);
  }
  bitmap = view_buffer->access_bitmap(view_bitmap_buffer, bitmap_row_gap);
  if (bitmap == NULL) return NULL; 

  //Copy data to view 
#if 1
  int i, line_size;
  kdu_uint32 *source_bitmap_buffer = image_bitmap_buffer + (region_on_buffer.pos.y * buffer_dims.size.x + region_on_buffer.pos.x);
  kdu_uint32 *target_bitmap_buffer = view_bitmap_buffer;
  
  line_size = (view_dims.size.x > buffer_dims.size.x) ? buffer_dims.size.x : view_dims.size.x;
  for (i = 0; i < region_on_buffer.size.y; i++){
	  memcpy(target_bitmap_buffer, source_bitmap_buffer, line_size * 4);
	  source_bitmap_buffer += buffer_dims.size.x;
	  target_bitmap_buffer += view_dims.size.x;
  }
#else
  int i, line_size;
  kdu_uint32 *source_bitmap_buffer = image_bitmap_buffer ;
  kdu_uint32 *target_bitmap_buffer = view_bitmap_buffer;

  line_size = (view_dims.size.x > buffer_dims.size.x) ? buffer_dims.size.x : view_dims.size.x;
  for (i = 0; i < region_on_buffer.size.y; i++){
	  memcpy(target_bitmap_buffer, source_bitmap_buffer, line_size * 4);
	  source_bitmap_buffer += buffer_dims.size.x;
	  target_bitmap_buffer += view_dims.size.x;
  }

  
  TRACE(" buffer_dims [%d:%d] [%d:%d]  \n", buffer_dims.pos.x, buffer_dims.pos.y, buffer_dims.size.x, buffer_dims.size.y);
  TRACE(" view_dims [%d:%d] [%d:%d]  \n", view_dims.pos.x, view_dims.pos.y, view_dims.size.x, view_dims.size.y);

  TRACE(" region_dims [%d:%d] [%d:%d]  \n", region.pos.x, region.pos.y, region.size.x, region.size.y);
  TRACE(" region_on_buffer_dims [%d:%d] [%d:%d]  \n", region_on_buffer.pos.x, region_on_buffer.pos.y, region_on_buffer.size.x, region_on_buffer.size.y);
  

#endif 

  
//	memcpy(view_bitmap_buffer, image_bitmap_buffer, buffer->size.x * buffer->size.y * 4);
  m_BackBufPixel.UpdateBufInfo((DWORD *)view_bitmap_buffer, view_dims.size.y, view_dims.size.x * 4);
  
  return bitmap;
}

/******************************************************************************/
/*                        CKdu_showApp::display_status                        */
/******************************************************************************/

void
  CKdu_showApp::display_status()
{

  char string[128]; string[0] = '\0';
  char *sp = string;

  kdu_dims basis_region = (enable_focus_box)?focus_box:view_dims;
  float component_scale_x=-1.0F, component_scale_y=-1.0F;
  if ((compositor != NULL) && configuration_complete)
    {
	  kdu_ilayer_ref layer =
		  compositor->get_next_visible_ilayer(kdu_ilayer_ref(), basis_region);

	  if (layer.exists() &&
		  !compositor->get_next_visible_ilayer(layer, basis_region))
	  {
		  int cs_idx;
		  kdu_istream_ref stream = compositor->get_ilayer_stream(layer, 0);
		  compositor->get_istream_info(stream, cs_idx, NULL, NULL, 4, NULL, &component_scale_x, &component_scale_y);
		  component_scale_x *= rendering_scale;
		  component_scale_y *= rendering_scale;
		  if (transpose)
            {
              float tmp=component_scale_x;
              component_scale_x=component_scale_y;
              component_scale_y=tmp;
            }
        }
    }

  if ((status_id == KDS_STATUS_CACHE) && jpip_client && !jpip_client->is_active())
    status_id = KDS_STATUS_LAYER_RES;

  if ((jpip_progress != NULL) && (status_id != KDS_STATUS_CACHE))
    { delete jpip_progress; jpip_progress = NULL; }
  if ((compositor == NULL) && jpip_client && !jpip_client->is_active())
    string[0] = '\0';
  else if ((status_id == KDS_STATUS_LAYER_RES) ||
           (status_id == KDS_STATUS_DIMS))
    {
      float res_percent = 100.0F*rendering_scale;
      float x_percent = 100.0F*component_scale_x;
      float y_percent = 100.0F*component_scale_y;
      if ((x_percent > (0.99F*res_percent)) &&
          (x_percent < (1.01F*res_percent)) &&
          (y_percent > (0.99F*res_percent)) &&
          (y_percent < (1.01F*res_percent)))
        x_percent = y_percent = -1.0F;
      if (x_percent < 0.0F)
        sprintf(sp,"Res=%1.1f%%",res_percent);
      else if ((x_percent > (0.99F*y_percent)) &&
               (x_percent < (1.01F*y_percent)))
        sprintf(sp,"Res=%1.1f%% (%1.1f%%)",res_percent,x_percent);
      else
        sprintf(sp,"Res=%1.1f%% (x:%1.1f%%,y:%1.1f%%)",
                res_percent,x_percent,y_percent);
      sp += strlen(sp);

      if (single_component_idx >= 0)
        {
          sprintf(sp,", Comp=%d/",single_component_idx+1);
          sp += strlen(sp);
          if (max_components <= 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",max_components); sp += strlen(sp); }
          sprintf(sp,", Stream=%d/",single_codestream_idx+1);
          sp += strlen(sp);
          if (max_codestreams <= 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",max_codestreams); sp += strlen(sp); }
        }
      else if ((single_layer_idx >= 0)) 
        { // Report compositing layer index
          sprintf(sp,", C.Layer=%d/",single_layer_idx+1);
          sp += strlen(sp);
          if (max_compositing_layer_idx < 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",max_compositing_layer_idx+1); sp += strlen(sp); }
        }
      
      else
        {
          sprintf(sp,", Frame=%d/",frame_idx+1);
          sp += strlen(sp);
          if (num_frames <= 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",num_frames); sp += strlen(sp); }
        }

      *(sp++) = '\t';
      if (compositor != NULL)
        {
          if (!compositor->is_processing_complete())
            { strcpy(sp,"WORKING"); sp += strlen(sp); }
          if (status_id == KDS_STATUS_DIMS)
            sprintf(sp,"\tHxW=%dx%d ",image_dims.size.y,image_dims.size.x);
          else        
            {
              int max_layers = compositor->get_max_available_quality_layers();
              if (max_display_layers >= max_layers)
                sprintf(sp,"\tQ.Layers=all ");
              else
                sprintf(sp,"\tQ.Layers=%d/%d ",max_display_layers,max_layers);
            }
        }
    }
  else if (status_id == KDS_STATUS_MEM)
    {
      kdu_long bytes = 0;
      if (compositor != NULL)
        {
		  kdu_long bytes = 0;
		  kdu_istream_ref str;
		  while ((str = compositor->get_next_istream(str, false, true)).exists())
		  {
			  kdu_codestream ifc = compositor->access_codestream(str);
			  bytes += ifc.get_compressed_data_memory()
				  + ifc.get_compressed_state_memory();
		  }
        }
	  if (jpip_client && jpip_client->is_active())
		  bytes += jpip_client->get_peak_cache_memory();
      sprintf(sp,"Compressed working/cache memory = %5g MB",1.0E-6*bytes);
      sp += strlen(sp);
      if ((compositor != NULL) && !compositor->is_processing_complete())
        { strcpy(sp,"\t\tWORKING "); sp += strlen(sp); }
    }
  else if (status_id == KDS_STATUS_CACHE)
    {
      float received_kbytes = ((float) 1.0E-3) *
		  jpip_client->get_received_bytes();
	  strcpy(sp, jpip_client->get_status());
      sp += strlen(sp);
      sprintf(sp,"\t\t%8.1f kB",received_kbytes);
      sp += strlen(sp);
      if ((compositor != NULL) && !compositor->is_processing_complete())
        { strcpy(sp,"WORKING "); sp += strlen(sp); }
      if (cumulative_transmission_time || last_transmission_start)
        {
          double time = cumulative_transmission_time;
          if (last_transmission_start)
            time += (double)(clock() - last_transmission_start);
          time *= (1.0 / CLOCKS_PER_SEC);
          if (time > 0.0)
            sprintf(sp,"; %7.1f kB/s ",received_kbytes / time);
        }
      if (jpip_progress == NULL)
        {
          RECT rc;
          frame_wnd->get_status_bar()->GetItemRect(0,&rc);
          int size = rc.right-rc.left;
          rc.left += (3*size) >> 3;
          rc.right -= (3*size) >> 3;
          jpip_progress = new CProgressCtrl();
          jpip_progress->Create(WS_CHILD|WS_VISIBLE,rc,
                                frame_wnd->get_status_bar(),1);
          jpip_progress->SetRange(0,100);
          jpip_progress->SetPos(0);
          jpip_progress->SetStep(1);
        }
    }
  USES_CONVERSION;
  frame_wnd->SetMessageText(A2T(string));
  if ((jpip_progress != NULL) && (compositor != NULL) && compositor->is_processing_complete())
    {
	  kdu_long samples = 0, packet_samples = 0, max_packet_samples = 0, s, p, m;
	  kdu_ilayer_ref lyr;
	  
	  kdu_dims roi = basis_region;

	  roi.size.x = roi.size.x / 32;
	  roi.size.y = roi.size.y / 32;
	   while ((compositor != NULL) &&
		  ((lyr = compositor->get_next_visible_ilayer(lyr, roi)).exists()))
	  {
		  kdu_istream_ref str;
		  int w = 0;
		  while ((str = compositor->get_ilayer_stream(lyr, w++)).exists())
		  {
			  compositor->get_codestream_packets(str, roi, s, p, m);
			  samples += s;  packet_samples += p;  max_packet_samples += m;
		  }
	  }
	  	  
	  TRACE("image[%d;%d:%d:%d] roi[%d;%d:%d:%d] samples %d packet_samples %d max_packet_samples %d  %f\n",
		  image_dims.pos.x, image_dims.pos.y, image_dims.size.x, image_dims.size.y,
		  roi.pos.x, roi.pos.y, roi.size.x, roi.size.y,
		  samples, packet_samples, max_packet_samples, (100.0f*packet_samples) / max_packet_samples);
      
      if (packet_samples > 0)
		  jp_decode_progress = (int)floor(0.01 + 100.0 * (((double)packet_samples) /
                                       ((double) max_packet_samples)));
	  jpip_progress->SetPos(jp_decode_progress);
    }
}

/******************************************************************************/
/*            CKdu_showApp::check_one_time_request_compatibility              */
/******************************************************************************/

bool
  CKdu_showApp::check_one_time_request_compatibility()
{
  if ((!jpx_in.exists()) || (single_component_idx >= 0))
    return true;
  if (!jpip_client->get_window_in_progress(&tmp_roi))
    return true; // Window information not available yet

  int c;
  kdu_sampled_range *rg;
  bool compatible = true;
  if ((single_component_idx < 0) && (single_layer_idx < 0) &&
      (frame_idx == 0) && (frame != NULL))
    { // Check compatibility of default animation frame
      int m, num_members = frame_expander.get_num_members();
      for (m=0; m < num_members; m++)
        {
          int instruction_idx, layer_idx;
          bool covers_composition;
          kdu_dims source_dims, target_dims;
          int remapping_ids[2];
		  jpx_composited_orientation orientation;
		  instruction_idx  = frame_expander.get_member(m,layer_idx,covers_composition,source_dims, target_dims, orientation);
          composition_rules.get_original_iset(frame,instruction_idx,
                                           remapping_ids[0],remapping_ids[1]);
          for (c=0; (rg=tmp_roi.contexts.access_range(c)) != NULL; c++)
            if ((rg->from <= layer_idx) && (rg->to >= layer_idx) &&
                (((layer_idx-rg->from) % rg->step) == 0))
              {
                if (covers_composition &&
                    (rg->remapping_ids[0] < 0) && (rg->remapping_ids[1] < 0))
                  break; // Found compatible layer
                if ((rg->remapping_ids[0] == remapping_ids[0]) &&
                    (rg->remapping_ids[1] == remapping_ids[1]))
                  break; // Found compatible layer
              }
          if (rg == NULL)
            { // No appropriate context request for layer; see if it needs one
              if (covers_composition && (layer_idx == 0) &&
                  jpx_in.access_compatibility().is_jp2_compatible())
                {
                  for (c=0; (rg=tmp_roi.codestreams.access_range(c))!=NULL; c++)
                    if (rg->from == 0)
                      break;
                }
            }
          if (rg == NULL)
            { // No response for this frame layer; must downgrade
              compatible = false;
              frame_idx = 0;
              frame_start = frame_end = 0.0;
              frame = NULL;
              single_layer_idx = 0; // Try this one
              break;
            }
        }
    }

  if (compatible)
    return true;
 
  bool have_codestream0 = false;
  for (c=0; (rg=tmp_roi.codestreams.access_range(c)) != NULL; c++)
    {
      if (rg->from == 0)
        have_codestream0 = true;
      if (rg->context_type == KDU_JPIP_CONTEXT_TRANSLATED)
        { // Use the compositing layer translated here
          int range_idx = rg->remapping_ids[0];
          int member_idx = rg->remapping_ids[1];
          rg = tmp_roi.contexts.access_range(range_idx);
          if (rg == NULL)
            continue;
          single_layer_idx = rg->from + rg->step*member_idx;
          if (single_layer_idx > rg->to)
            continue;
          return false; // Found a suitable compositing layer
        }
    }

  if (have_codestream0 && jpx_in.access_compatibility().is_jp2_compatible())
    {
      single_layer_idx = 0;
      return false; // Compositing layer 0 is suitable
    }

  // If we get here, we could not find a compatible layer
  single_layer_idx = -1;
  single_component_idx = 0;
  if ((rg=tmp_roi.codestreams.access_range(0)) != NULL)
    single_codestream_idx = rg->from;
  else
    single_codestream_idx = 0;

  return compatible;
}

/******************************************************************************/
/*              CKdu_showApp::update_client_window_of_interest                */
/******************************************************************************/

void
  CKdu_showApp::update_client_window_of_interest()
{
	  if ((compositor == NULL) || (jpip_client &&!jpip_client->is_alive()) || (jpip_client &&jpip_client->is_one_time_request()))
    return;

  // Begin by trying to find the location, size and resolution of the
  // view window.  We might have to guess at these parameters here if we
  // do not currently have an open image.
  kdu_dims res_dims, roi_dims;
  if (!image_dims.is_empty())
    {
      res_dims = image_dims;
      res_dims.from_apparent(transpose,vflip,hflip);
    }
  else
    { // Try to guess what the image dimensions would be at the current scale
      if (single_component_idx >= 0)
        { // Use size of the codestream, if we can determine it
          if (!jpx_in.exists())
            {
			  kdu_istream_ref istr;
			  istr = compositor->get_next_istream(istr);
			  if (istr.exists())
			  {
				  compositor->access_codestream(istr).get_dims(-1, res_dims);
			  }
            }
          else
            {
              jpx_codestream_source stream =
                jpx_in.access_codestream(single_codestream_idx);
              if (stream.exists())
                res_dims.size = stream.access_dimensions().get_size();
            }
        }
      else if (single_layer_idx >= 0)
        { // Use size of compositing layer, if we can evaluate it
          if (!jpx_in.exists())
            {
			  kdu_istream_ref istr;
			  istr = compositor->get_next_istream(istr);
			  if (istr.exists())
                {
				  compositor->access_codestream(istr).get_dims(-1, res_dims);
				  res_dims.pos = kdu_coords(0,0);
                }
            }
          else
            {
              jpx_layer_source layer = jpx_in.access_layer(single_layer_idx);
              if (layer.exists())
                res_dims.size = layer.get_layer_size();
            }
        }
      else
        { // Use size of composition surface, if we can find it
          if (composition_rules.exists())
            composition_rules.get_global_info(res_dims.size);
        }
      res_dims.pos = kdu_coords(0,0);
      res_dims.size.x = (int) ceil(res_dims.size.x * rendering_scale);
      res_dims.size.y = (int) ceil(res_dims.size.y * rendering_scale);
    }

  if (enable_focus_box && !focus_box.is_empty())
    {
      roi_dims = focus_box;
      roi_dims.from_apparent(transpose,vflip,hflip);
    }
  else if (focus_anchors_known && !res_dims.is_empty())
    {
      res_dims.to_apparent(transpose,vflip,hflip);
      //convert_focus_anchors_to_region(roi_dims,res_dims);
      roi_dims.from_apparent(transpose,vflip,hflip);
      res_dims.from_apparent(transpose,vflip,hflip);
    }
  else if (!view_dims.is_empty())
    {
      roi_dims = view_dims;
      roi_dims.from_apparent(transpose,vflip,hflip);
    }
  else
    roi_dims = res_dims;
  if (roi_dims.is_empty())
    roi_dims = res_dims;
  roi_dims &= res_dims;

  // Now, we have as much information as we can guess concerning the
  // intended view window.  Let's use it to create a window request.
  tmp_roi.init();
  roi_dims.pos -= res_dims.pos; // Make relative to current resolution
  tmp_roi.resolution = res_dims.size;
  tmp_roi.region = roi_dims;
  tmp_roi.round_direction = 0;

  if (configuration_complete && (max_display_layers < (1<<16)) &&
      (max_display_layers < compositor->get_max_available_quality_layers()))
    tmp_roi.max_layers = max_display_layers;

  if (single_component_idx >= 0)
    {
      tmp_roi.components.add(single_component_idx);
      tmp_roi.codestreams.add(single_codestream_idx);
    }
  else if (single_layer_idx >= 0)
    { // single compositing layer
      if ((single_layer_idx == 0) &&
          ((!jpx_in) || jpx_in.access_compatibility().is_jp2_compatible()))
        tmp_roi.codestreams.add(0); // Maximize chance of success even if
                                    // server cannot translate contexts
      kdu_sampled_range rg;
      rg.from = rg.to = single_layer_idx;
      rg.context_type = KDU_JPIP_CONTEXT_JPXL;
      tmp_roi.contexts.add(rg);
    }
  else if (frame != NULL)
    { // Request whole animation frame
      kdu_dims scaled_roi_dims;
      if (configuration_complete &&
          !(roi_dims.is_empty() || res_dims.is_empty()))
        {
          kdu_coords comp_size; composition_rules.get_global_info(comp_size);
          double scale_x = ((double) comp_size.x) / ((double) res_dims.size.x);
          double scale_y = ((double) comp_size.y) / ((double) res_dims.size.y);
          kdu_coords min = roi_dims.pos;
          kdu_coords lim = min + roi_dims.size;
          min.x = (int) floor(scale_x * min.x);
          lim.x = (int) ceil(scale_x * lim.x);
          min.y = (int) floor(scale_y * min.y);
          lim.y = (int) ceil(scale_y * lim.y);
          scaled_roi_dims.pos = min;
          scaled_roi_dims.size = lim - min;
        }
      kdu_sampled_range rg;
      frame_expander.construct(&jpx_in,frame,frame_iteration,true,
                               scaled_roi_dims);
      int m, num_members = frame_expander.get_num_members();
      for (m=0; m < num_members; m++)
        {
          int instruction_idx, layer_idx;
          bool covers_composition;
          kdu_dims source_dims, target_dims;
		  jpx_composited_orientation orientation;
		  instruction_idx =frame_expander.get_member(m,layer_idx,
                                      covers_composition,source_dims,
									  target_dims, orientation);
          rg.from = rg.to = layer_idx;
          rg.context_type = KDU_JPIP_CONTEXT_JPXL;
          if (covers_composition)
            {
              if ((layer_idx == 0) && (num_members == 1) &&
                  jpx_in.access_compatibility().is_jp2_compatible())
                tmp_roi.codestreams.add(0); // Maximize chance that server will
                      // respond correctly, even if it can't translate contexts
              rg.remapping_ids[0] = rg.remapping_ids[1] = -1;
            }
          else
            {
              composition_rules.get_original_iset(frame,instruction_idx,
                                                  rg.remapping_ids[0],
                                                  rg.remapping_ids[1]);
            }
          tmp_roi.contexts.add(rg);
        }
    }
  
  // Post the window
  if (jpip_client && jpip_client->post_window(&tmp_roi))
    {
      client_roi.copy_from(tmp_roi);
      if (last_transmission_start == 0)
        { // Server was idle; start timing server responses again
          last_transmission_start = clock();
        }
	  last_transferred_bytes = 0;
    }
}

/******************************************************************************/
/*                     CKdu_showApp::change_client_focus                      */
/******************************************************************************/

void
  CKdu_showApp::change_client_focus(kdu_coords actual_resolution,
                                    kdu_dims actual_region)
{
  // Find the relative location and dimensions of the new focus box
  focus_centre_x =
    (((float) actual_region.pos.x) + 0.5F*((float) actual_region.size.x)) /
    ((float) actual_resolution.x);
  focus_centre_y =
    (((float) actual_region.pos.y) + 0.5F*((float) actual_region.size.y)) /
    ((float) actual_resolution.y);
  focus_size_x = ((float) actual_region.size.x) / ((float) actual_resolution.x);
  focus_size_y = ((float) actual_region.size.y) / ((float) actual_resolution.y);

  // Correct for geometric transformations
  if (transpose)
    {
      float tmp=focus_size_x; focus_size_x=focus_size_y; focus_size_y=tmp;
      tmp=focus_centre_x; focus_centre_x=focus_centre_y; focus_centre_y=tmp;
    }
  if (image_dims.pos.x < 0)
    focus_centre_x = 1.0F - focus_centre_x;
  if (image_dims.pos.y < 0)
    focus_centre_y = 1.0F - focus_centre_y;

  // Draw the focus box from its relative coordinates.
  focus_anchors_known = true;
  enable_focus_box = true;
  bool posting_state = enable_region_posting;
  enable_region_posting = false;
  enable_region_posting = posting_state;
}






/******************************************************************************/
/*                            CKdu_showApp::OnIdle                            */
/******************************************************************************/

BOOL CKdu_showApp::OnIdle(LONG lCount) 
  /* Note: this function implements a very simple heuristic for scheduling
     regions for processing.  It is intended primarily to demonstrate
     capabilities of the underlying framework -- in particular the
     `kdu_region_compositor' object and the rest of the Kakadu JPEG2000
     platform on which it is built. */
{
#if 0
  if (processing_suspended)
    return CWinApp::OnIdle(lCount);
  if (in_idle)
    return FALSE;
  if (compositor == NULL)
    {
      if (jpip_client.is_active())
        open_file(NULL); // Try to complete the client source opening operation.
      display_status();
      return FALSE; // Don't need to be called from recursive loop again.
    }
  in_idle = true;

  if (jpip_client.is_active() && last_transmission_start &&
      jpip_client.is_idle())
    {
      cumulative_transmission_time += (clock() - last_transmission_start);
      last_transmission_start = 0;
    }

  
  kdu_dims new_region;
  bool need_more_processing = (configuration_complete && (buffer != NULL));
  try {
      if (need_more_processing && !compositor->process(16384,new_region))
        {
          need_more_processing = false;
          if (!compositor->get_total_composition_dims(image_dims))
            { // Must have invalid scale
              compositor->flush_composition_queue();
              buffer = NULL;
              initialize_regions(); // This call will find a satisfactory scale
              in_idle = false;
              return FALSE;
            }
          //else if (in_playmode && !pushed_last_frame)
          //  { // See if we should push this frame onto the queue
          //    double adjusted_end_instant, adjusted_start_instant;
          //    if (use_native_timing)
          //      {
          //        adjusted_start_instant = frame_start/rate_multiplier;
          //        adjusted_end_instant = frame_end/rate_multiplier;
          //      }
          //    else
          //      {
          //        adjusted_start_instant = adjusted_end_instant =
          //          ((double) frame_idx) / (custom_fps*rate_multiplier);
          //        adjusted_end_instant += 1.0 / (custom_fps*rate_multiplier);
          //      }
          //    adjusted_start_instant += frame_time_offset;
          //    adjusted_end_instant += frame_time_offset;
          //    if ((!compositor->inspect_composition_queue(max_queue_size-1)) &&
          //        ((adjusted_start_instant-time) <= max_queue_period))
          //      {
          //        compositor->set_surface_initialization_mode(false);
          //             // Avoids wasting time initializing new buffer surfaces
          //        compositor->push_composition_buffer(
          //          (kdu_long)(adjusted_end_instant*1000),frame_idx);
          //        if (!set_frame(frame_idx+1))
          //          {
          //            if (playmode_repeat)
          //              {
          //                set_frame(0);
          //                frame_time_offset = adjusted_end_instant;
          //              }
          //            else
          //              pushed_last_frame = true;
          //          }
          //        need_more_processing = true;
          //      }
          //  }
        }
    }
  catch (int) { // Problem occurred.  Only safe thing is to close the file.
      close_file();
      in_idle = false;
      return FALSE;
    }

  if ((compositor != NULL) && !compositor->inspect_composition_queue(0))
    { // Paint any newly decompressed region right away.
      new_region &= view_dims; // No point in painting invisible regions.
      if (!new_region.is_empty())
        {
          CDC *dc = child_wnd->GetDC();
		  //TRACE("pain_region call in OnIdle..new_region.............%d, %d, %d, %d\n", 
			 // new_region.pos.x, new_region.pos.y, new_region.size.x, new_region.size.y);

          paint_region(dc,new_region);
          child_wnd->ReleaseDC(dc);

        }
    }

  if (need_more_processing)
    {
      in_idle = false;
      return TRUE;
    }

    display_status(); // All processing done, keep status up to date

  if (jpip_client.is_active())
    {
      // See if focus box needs to be changed
      if (jpip_client.get_window_in_progress(&tmp_roi))
        {
          if ((client_roi.region != tmp_roi.region) ||
              (client_roi.resolution != tmp_roi.resolution))
            change_client_focus(tmp_roi.resolution,tmp_roi.region);
          client_roi.copy_from(tmp_roi);
        }
    
      // See if cache has been updated.
      int new_bytes = (int)
        (jpip_client.get_transferred_bytes(KDU_MAIN_HEADER_DATABIN) +
         jpip_client.get_transferred_bytes(KDU_TILE_HEADER_DATABIN) +
         jpip_client.get_transferred_bytes(KDU_PRECINCT_DATABIN) +
         jpip_client.get_transferred_bytes(KDU_META_DATABIN));
      if (new_bytes != last_transferred_bytes)
        {
          if (!configuration_complete)
            initialize_regions();
          if ((refresh_timer_id == 0)) 
            refresh_timer_id =
              m_pMainWnd->SetTimer(KDU_SHOW_REFRESH_TIMER_IDENT,1000,NULL);
          last_transferred_bytes = new_bytes;
        }
    }

  if (overlays_enabled && overlay_flashing_enabled &&
      (flash_timer_id == 0)) // PLAY && !in_playmode)
    flash_timer_id =
      m_pMainWnd->SetTimer(KDU_SHOW_FLASH_TIMER_IDENT,800,NULL);

  in_idle = false;
#endif
  return CWinApp::OnIdle(lCount); // Give low-level idle processing a go.
}

/******************************************************************************/
/*                          CKdu_showApp::OnAppAbout                          */
/******************************************************************************/

void CKdu_showApp::OnAppAbout()
{
  CAboutDlg aboutDlg;
  aboutDlg.DoModal();
}

/******************************************************************************/
/*                          CKdu_showApp::OnRegisterFile Extion                          */
/******************************************************************************/


void CKdu_showApp::OnRegisterFileExtension()
{
	char exec_path[MAX_PATH];
	GetModuleFileNameA(NULL, exec_path, MAX_PATH);


	if (RegisterFileExtension(exec_path, ".jp2") && RegisterFileExtension(exec_path, ".jpx")){
		AfxMessageBox(_T("Success register JPX(JP2) File extension"), MB_OK | MB_ICONINFORMATION);
	}
	else{
		AfxMessageBox(_T("Fail register JPX(JP2) File extension"), MB_OK | MB_ICONINFORMATION);
	}

	if (!RegisterProtocols(exec_path)){
		AfxMessageBox(_T("Fail register JPIP Protocol"), MB_OK | MB_ICONINFORMATION);
	}
}


/******************************************************************************/
/*                          CKdu_showApp::OnFileOpen                          */
/******************************************************************************/

void CKdu_showApp::OnFileOpen() 
{
	TCHAR filename[MAX_PATH];
#ifndef _DEBUG
  
  TCHAR initial_dir[MAX_PATH];
  OPENFILENAME ofn;
  memset(&ofn,0,sizeof(ofn)); ofn.lStructSize = sizeof(ofn);
  _tcscpy(initial_dir, settings.get_open_save_dir());
  if (*initial_dir == '\0')
    GetCurrentDirectory(MAX_PATH,initial_dir);

  ofn.hwndOwner = m_pMainWnd->GetSafeHwnd();
  ofn.lpstrFilter =
    _T("JP2-family file (*.jp2, *.jpx, *.jpf, *.mj2)\0*.jp2;*.jpx;*.jpf;*.mj2\0")
	_T("JP2 files (*.jp2)\0*.jp2\0")
	_T("JPX files (*.jpx, *.jpf)\0*.jpx;*.jpf\0")
	_T("MJ2 files (*.mj2)\0*.mj2\0")
	_T("Uwrapped code-streams (*.j2c, *.j2k)\0*.j2c;*.j2k\0")
	_T("All files (*.*)\0*.*\0\0");
  ofn.nFilterIndex = settings.get_open_idx();
  if ((ofn.nFilterIndex > 5) || (ofn.nFilterIndex < 1))
    ofn.nFilterIndex = 1;
  ofn.lpstrFile = filename; filename[0] = '\0';
  ofn.nMaxFile = MAX_PATH-1;
  ofn.lpstrTitle = _T("Open JPEG2000 Compressed Image");
  ofn.lpstrInitialDir = initial_dir;
  ofn.Flags = OFN_FILEMUSTEXIST;
  if (!GetOpenFileName(&ofn))
    return;

  _tcscpy(initial_dir, filename);
  if ((ofn.nFileOffset > 0) &&
      (initial_dir[ofn.nFileOffset-1] == '/') ||
      (initial_dir[ofn.nFileOffset-1] == '\\'))
    ofn.nFileOffset--;
  initial_dir[ofn.nFileOffset] = '\0';
  settings.set_open_save_dir(initial_dir);
  settings.set_open_idx(ofn.nFilterIndex);

  settings.save_to_registry(this);

#else
	//_tcscpy(filename, _T("C:\\Users\\bughyuk\\Desktop\\Demo_1502_90m_plxn.jpx"));
	//_tcscpy(filename, _T("d:\\4Layertest_1234.jpx"));
	_tcscpy(filename, _T("d:\\Demo_1502_90m_plxn.jpx"));
	//_tcscpy(filename, _T("d:\\16.1.A10.2278m-2279m.F32_fl.jp2"));
	//_tcscpy(filename, _T("C:\\innovaplex\\JPX\\GUT304VI_00_04.jpx"));
	//_tcscpy(filename, _T("d:\\core1.jp2"));
	//_tcscpy(filename, _T("d:\\color_chart.jp2"));
#endif
	//create meun if tool plugin exist
	frame_wnd->EnablePlugInsMenu(true);
	open_file(filename);
}

/******************************************************************************/
/*                          CKdu_showApp::OnFileClose                         */
/******************************************************************************/

void CKdu_showApp::OnFileClose() 
{
	frame_wnd->EnablePlugInsMenu(false);
  close_file();
  if (jpip_progress) jpip_progress->SetPos(0);

}

/******************************************************************************/
/*                       CKdu_showApp::OnUpdateFileClose                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateFileClose(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable((compositor != NULL) || (jpip_client && jpip_client->is_active()));
}

/******************************************************************************/
/*                           CKdu_showApp::OnJpipOpen                         */
/******************************************************************************/

void CKdu_showApp::OnJpipOpen() 
{
  TCHAR server_name[MAX_PATH];
  TCHAR proxy_name[MAX_PATH];
  TCHAR request[MAX_PATH];
  TCHAR channel_type[15];
  TCHAR cache_dir_name[MAX_PATH];
  TCHAR login_type[15];
  TCHAR id[MAX_PATH];
  TCHAR pw[MAX_PATH];

  // Initialize fields
  _tcsncpy(server_name, settings.get_jpip_server(), MAX_PATH);
  server_name[MAX_PATH-1] = '\0';
  _tcsncpy(proxy_name, settings.get_jpip_proxy(), MAX_PATH);
  proxy_name[MAX_PATH-1] = '\0';
  _tcsncpy(request, settings.get_jpip_request(), MAX_PATH);
  request[MAX_PATH-1] = '\0';
  _tcsncpy(channel_type, settings.get_jpip_channel_type(), 15);
  channel_type[14] = '\0';
  _tcsncpy(cache_dir_name, settings.get_jpip_cache(), MAX_PATH);
  cache_dir_name[MAX_PATH-1] = '\0';

  _tcsncpy(login_type, settings.get_jpip_login_type(), 15);
  login_type[14] = '\0';
  _tcsncpy(id, settings.get_jpip_id(), MAX_PATH);
  id[MAX_PATH - 1] = '\0';
  _tcsncpy(pw, settings.get_jpip_pw(), MAX_PATH);
  pw[MAX_PATH - 1] = '\0';

  // Run dialog
  USES_CONVERSION;
  CJpipOpenDlg open_dlg(server_name, MAX_PATH, request, MAX_PATH,
	  settings.is_use_cache, settings.jpip_buffer_len,
	  channel_type, 15, proxy_name, MAX_PATH,
	  cache_dir_name, MAX_PATH,
	  login_type, 15,
	  id, MAX_PATH,
	  pw, MAX_PATH);
  if (open_dlg.DoModal() != IDOK)
    return;

  // Save settings
  settings.set_jpip_server(server_name);
  settings.set_jpip_request(request);
  settings.set_jpip_channel_type(channel_type);
  settings.set_jpip_proxy(proxy_name);
  settings.set_jpip_cache(cache_dir_name);
  settings.is_use_cache = open_dlg.use_cache();
  settings.jpip_buffer_len = open_dlg.get_memory_len();

  settings.set_jpip_login_type(login_type);
  settings.set_jpip_id(id);
  settings.set_jpip_pw(pw);

  settings.save_to_registry(this);
 
  
  // Create string.
  TCHAR url_string[2*MAX_PATH+30]; // Plenty of space.
  _tcscpy(url_string, _T("jpip://"));
  _tcscat(url_string, server_name);
  _tcscat(url_string, _T("/"));
  _tcscat(url_string, request);
  open_file(url_string);
}



/******************************************************************************/
/*                         CKdu_showApp::OnViewZoomOut                        */
/******************************************************************************/

void CKdu_showApp::OnViewZoomOut() 
{
  if (buffer == NULL)
    return;
  if (rendering_scale == min_rendering_scale)
    return;

  rendering_scale = decrease_scale(rendering_scale);

  calculate_view_anchors();
  initialize_regions();

  if (m_pOverview != NULL) m_pOverview->Invalidate(); // MJY
}

/******************************************************************************/
/*                      CKdu_showApp::OnUpdateViewZoomOut                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewZoomOut(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((buffer != NULL) &&
                 ((min_rendering_scale < 0.0F) ||
                  (rendering_scale > min_rendering_scale)));
}

/******************************************************************************/
/*                         CKdu_showApp::OnViewZoomIn                         */
/******************************************************************************/

void CKdu_showApp::OnViewZoomIn() 
{
  if (buffer == NULL)
    return;
  rendering_scale = increase_scale(rendering_scale);
  
  calculate_view_anchors();
  if (focus_anchors_known)
    { view_centre_x = focus_centre_x; view_centre_y = focus_centre_y; }

  initialize_regions();
  if (m_pOverview != NULL) m_pOverview->Invalidate(); 
}

/******************************************************************************/
/*                      CKdu_showApp::OnUpdateViewZoomIn                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewZoomIn(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(buffer != NULL);
}


/******************************************************************************/
/*                        CKdu_showApp::set_codestream                        */
/******************************************************************************/

void
CKdu_showApp::set_codestream(int codestream_idx)
{
	calculate_view_anchors();
	if ((single_component_idx<0) && focus_anchors_known && !image_dims.is_empty())
	{ // Try to map focus region to this codestream
		kdu_dims region; //convert_focus_anchors_to_region(region,image_dims);
		kdu_dims stream_dims, stream_region;

		stream_dims = compositor->find_codestream_region(codestream_idx, true);
		if (stream_dims.is_empty()){
			focus_anchors_known = enable_focus_box = false;
		}
		else
		{
			stream_region = stream_dims & region;
			//convert_region_to_focus_anchors(stream_region,stream_dims);
			focus_layer = -1;
			focus_codestream = codestream_idx;
			view_centre_x = focus_centre_x; view_centre_y = focus_centre_y;
		}
	}
	single_component_idx = 0;
	single_codestream_idx = codestream_idx;
	single_layer_idx = -1;
	frame = NULL;
	invalidate_configuration();
	initialize_regions();
}

/******************************************************************************/
/*                    CKdu_showApp::set_compositing_layer                     */
/******************************************************************************/

void
  CKdu_showApp::set_compositing_layer(int layer_idx)
{
  calculate_view_anchors();
  if ((frame != NULL) && focus_anchors_known && !image_dims.is_empty())
    { // Try to map focus region to this compositing layer
      kdu_dims region; //convert_focus_anchors_to_region(region,image_dims);
      kdu_dims layer_dims, layer_region;
            
	  layer_dims = compositor->find_layer_region(layer_idx, true);
	  if (layer_dims.is_empty())
        focus_anchors_known = enable_focus_box = false;
      else
        {
          layer_region = layer_dims & region;
          //convert_region_to_focus_anchors(layer_region,layer_dims);
          focus_layer = layer_idx;
          focus_codestream = -1;
          view_centre_x = focus_centre_x; view_centre_y = focus_centre_y;
        }
    }
  int translate_codestream =
    (single_component_idx < 0)?-1:single_codestream_idx;
  if (layer_idx != single_layer_idx)
    {
      frame_idx = 0;
      frame_start = frame_end = 0.0;
    }
  else
    translate_codestream = -1;
  single_component_idx = -1;
  single_layer_idx = layer_idx;
  frame = NULL;
    
  invalidate_configuration();
  initialize_regions();
}

/******************************************************************************/
/*                    CKdu_showApp::set_compositing_layer                     */
/******************************************************************************/

void
  CKdu_showApp::set_compositing_layer(kdu_coords point)
{
  if (compositor == NULL)
    return;
  point += view_dims.pos;
  kdu_ilayer_ref ilyr = compositor->find_point(point);
  int layer_idx, cs_idx; bool is_opaque;
  if ((compositor->get_ilayer_info(ilyr, layer_idx, cs_idx, is_opaque) > 0) &&
	  (layer_idx >= 0))
	  set_compositing_layer(layer_idx);
}

	
/******************************************************************************/
/*                    CKdu_showApp::OnCompositingLayer                        */
/******************************************************************************/

void
 CKdu_showApp::OnCompositingLayer() 
{
  if ((compositor == NULL) || (single_layer_idx >= 0)) return;
  set_compositing_layer(0);
}

/******************************************************************************/
/*                 CKdu_showApp::OnUpdateCompositingLayer                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateCompositingLayer(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) && (single_layer_idx < 0));
  pCmdUI->SetCheck((compositor != NULL) && (single_layer_idx >= 0));
}


/******************************************************************************/
/*                       CKdu_showApp::OnImageNext                            */
/******************************************************************************/
void
  CKdu_showApp::OnImageNext() 
{
  if (compositor == NULL)
    return;
  if ((single_layer_idx >= 0))// && !mj2_in)
    { // Advance to next compositing layer
      if ((max_compositing_layer_idx >= 0) &&
          (single_layer_idx >= max_compositing_layer_idx))
        return;
	  
	  int layer_idx = single_layer_idx+1;

	  
    }
}

/******************************************************************************/
/*                    CKdu_showApp::OnUpdateImageNext                         */
/******************************************************************************/

void
  CKdu_showApp::OnUpdateImageNext(CCmdUI* pCmdUI) 
{
  if ((single_layer_idx >= 0) ) 
    pCmdUI->Enable((compositor != NULL) &&
                   ((single_layer_idx < max_compositing_layer_idx)));

  //for presentation
  pCmdUI->Enable(0);
  
}

/******************************************************************************/
/*                       CKdu_showApp::OnImagePrev                            */
/******************************************************************************/
void
  CKdu_showApp::OnImagePrev() 
{
  if (compositor == NULL)
    return;
 if ((single_layer_idx >= 0) )//&& !mj2_in)
    { // Go to previous compositing layer
      if (single_layer_idx == 0)
        return;
	  	
    }
  
}

/******************************************************************************/
/*                    CKdu_showApp::OnUpdateImagePrev                         */
/******************************************************************************/

void
  CKdu_showApp::OnUpdateImagePrev(CCmdUI* pCmdUI) 
{
  if ((single_layer_idx >= 0))
    pCmdUI->Enable((compositor != NULL) && (single_layer_idx > 0));

  //for presentation
  pCmdUI->Enable(0);
}

/******************************************************************************/
/*                        CKdu_showApp::OnViewRefresh                         */
/******************************************************************************/

void CKdu_showApp::OnViewRefresh() 
{
  refresh_display();
}



/******************************************************************************/
/*                       CKdu_showApp::OnRedisplayRotate                      */
/******************************************************************************/

void CKdu_showApp::OnRedisplayRotate(eRotateStatus eRoate)
{
	if (buffer == NULL)	return;
	
	switch (eRoate)
	{
	case ROTATE_NONE:
		transpose = false;
		hflip = false;
		vflip = false;
		break;
	case ROTATE_90:
		transpose = true;
		hflip = true;
		vflip = false;
		break;
	case ROTATE_180:
		transpose = false;
		hflip = true;
		vflip = true;
		break;
	case ROTATE_270:
		transpose = true;
		hflip = false;
		vflip = true;
		break;
	}
	
	calculate_view_anchors();

	float f_tmp;
	if (ROTATE_NONE == m_rotationStatus){
		if (ROTATE_90 == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = view_centre_x;
			view_centre_x = 1.0f-f_tmp;
		}
		else if (ROTATE_180 == eRoate){
			view_centre_x = 1.0F - view_centre_x;
			view_centre_y = 1.0F - view_centre_y;
		}
		else if (ROTATE_270 == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = 1.0f - view_centre_x;
			view_centre_x = f_tmp;
		}
	}
	else if (ROTATE_90 == m_rotationStatus){
		if (ROTATE_NONE == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = 1.0f - view_centre_x;
			view_centre_x = f_tmp;
		}
		else if (ROTATE_180 == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = view_centre_x;
			view_centre_x = 1.0F - f_tmp;
		}
		else if (ROTATE_270 == eRoate){
			view_centre_x = 1.0F - view_centre_x;
			view_centre_y = 1.0F - view_centre_y;
		}
	}
	else if (ROTATE_180 == m_rotationStatus){
		if (ROTATE_NONE == eRoate){
			view_centre_x = 1.0F - view_centre_x;
			view_centre_y = 1.0F - view_centre_y;
		}
		else if (ROTATE_90 == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = 1.0F - view_centre_x;
			view_centre_x = f_tmp;
		}
		else if (ROTATE_270 == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = view_centre_x;
			view_centre_x = 1.0f - f_tmp;
		}
	}
	else if (ROTATE_270 == m_rotationStatus){
		if (ROTATE_NONE == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = view_centre_x;
			view_centre_x = 1.0f - f_tmp;
		}
		else if (ROTATE_90 == eRoate){
			view_centre_x = 1.0F - view_centre_x;
			view_centre_y = 1.0F - view_centre_y;
		}
		else if (ROTATE_180 == eRoate){
			f_tmp = view_centre_y;
			view_centre_y = 1.0f - view_centre_x;
			view_centre_x = f_tmp;
		}
	}

	m_rotationStatus = eRoate;
	initialize_regions( true );
}


void CKdu_showApp::OnToolOverviewwindow()
{
	if (m_pOverview != NULL) m_pOverview->ShowWindow(SW_SHOW);
}

void CKdu_showApp::OnToolOverviewReset()
{
	if (m_pOverview != NULL){
		m_pOverview->m_position.x = 0;
		m_pOverview->m_position.y = 80;

		CRect mainR;
		CRect overR;
		CPoint pos = m_pOverview->m_position;
		m_pMainWnd->GetWindowRect(mainR);
		m_pOverview->GetWindowRect(overR);

		m_pOverview->MoveWindow(mainR.left + pos.x, mainR.top + pos.y, overR.Width(), overR.Height() );
	}
}

void CKdu_showApp::OnToolOverviewAttach()
{
	frame_wnd->m_attach_overview = !frame_wnd->m_attach_overview;
	m_pOverview->CheckDlgButton(IDC_CHECK_ATTACH, frame_wnd->m_attach_overview);
}

void CKdu_showApp::ChangeSingleLayerIndex(int order, int layer_idx, bool redraw, bool del_permanent)
{
	if (compositor == NULL) return;
	if (max_compositing_layer_idx < 0) return;
	//if (order == 0 && (single_layer_idx == layer_idx)) return;
	//if (order == 1 && (second_mix_layer_idx == layer_idx)) return;
	
	TRACE("ChangeSingleLayerIndex order [%d] %d => %d second layer %d\n", order, single_layer_idx, layer_idx, second_mix_layer_idx);
	if (order == 0){
		single_layer_idx = layer_idx;
		if (compositor != NULL){
			compositor->remove_ilayer(kdu_ilayer_ref(), del_permanent);
			compositor->add_ilayer(single_layer_idx, kdu_dims(), kdu_dims());
		}
	}
	else{
		//decode and copy second buffer
		second_mix_layer_idx = layer_idx;

		//copy current bitmap if not change
		if (second_buffer && (second_buffer->size != buffer->size)) {
			RELEASE_BUFFER(second_buffer);
		}
		if (second_buffer == NULL){
			second_buffer = new kd_bitmap_buf();
		}

		if (compositor != NULL){
			compositor->remove_ilayer(kdu_ilayer_ref(), del_permanent);
			compositor->add_ilayer(second_mix_layer_idx, kdu_dims(), kdu_dims());
		}
	}
			
	UpdateViewDim(buffer_dims, redraw);
}


void	MixLayer(int data_len, unsigned char* in_src1, unsigned char* in_src2, unsigned char* out_dst, int alpha);
void CKdu_showApp::MixingLayer()
{
	if (buffer == NULL || second_buffer == NULL || m_bMixing == FALSE){
		TRACE("Not ready for mix layer \n");
		return;
	}

	if (buffer->size != second_buffer->size){
		TRACE("Must same size of buffer & secondary buffer \n");
		return;
	}

	//Recreate MIX Buffer
	if (mix_buffer && (mix_buffer->size != buffer->size)){
		RELEASE_BUFFER(mix_buffer);
	}
	if (mix_buffer == NULL) {
		mix_buffer = new kd_bitmap_buf();
		mix_buffer->create_bitmap(buffer->size);
	}

	//Mix two layer....
	kdu_uint32 *layer1_data, *layer2_data, *mix_data;
	int row_gap;
	buffer->access_bitmap(layer1_data, row_gap);
	second_buffer->access_bitmap(layer2_data, row_gap);
	mix_buffer->access_bitmap(mix_data, row_gap);

	int total_size = mix_buffer->size.y * mix_buffer->size.x *4;
	int alpha = m_MixingRatio;

	MixLayer(total_size, (unsigned char*)layer1_data, (unsigned char*)layer2_data, (unsigned char*)mix_data, alpha);
}


BOOL CKdu_showApp::PeekAndPump()
{
	MSG msg;

	while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) {

		if (!AfxGetApp()->PumpMessage()) {
			::PostQuitMessage(0);
			return FALSE;
		}
	}

	LONG lIdle = 0;
	while (AfxGetApp()->OnIdle(lIdle++));
	return TRUE;
}


void	OnCallbackDecompressStatus(int precent, int decode_size);
bool CKdu_showApp::UpdateCropImage(POINTF p1, POINTF p2, int layer_num, const TCHAR *sz_file_name)
{
	kdu_dims new_dim, new_region_dims;
	kdu_dims old_view_dims, old_image_dims;
	int old_layer_index;
	int x1 = p1.x * whole_image_dims.size.x;
	int y1 = p1.y * whole_image_dims.size.y;
	int x2 = p2.x * whole_image_dims.size.x;
	int y2 = p2.y * whole_image_dims.size.y;

	if (x1 > x2)
	{
		x1 = x2;
		x2 = p1.x * whole_image_dims.size.x;
	}
	if (y1 > y2)
	{
		y1 = y2;
		y2 = p1.y * whole_image_dims.size.y;
	}

	//change layer index
	//if (layer_num != single_layer_idx){
	compositor->remove_ilayer(kdu_ilayer_ref(), false);
	compositor->add_ilayer(layer_num, kdu_dims(), kdu_dims());
	//}
	old_layer_index = single_layer_idx;
	save_layer_idx = single_layer_idx = layer_num;
	
	compositor->set_scale(transpose, vflip, hflip, 1.0);
	if (x1 % 2) x1 += 1;
	new_dim.pos.set_x(x1);
	new_dim.pos.set_y(y1);
	new_dim.size.set_x(x2-x1);
	new_dim.size.set_y(y2-y1);
	if (_tcsstr(sz_file_name, _T(".tiff")))	//tiff foramt
		compositor->set_colour_order(KDU_ABGR_ORDER);
	compositor->set_buffer_surface(new_dim);

	kd_bitmap_file_buf *crop_buffer = (kd_bitmap_file_buf *)compositor->get_composition_bitmap(new_dim, sz_file_name);
	if (!crop_buffer){
		return false;
	}

	old_image_dims = image_dims;
	old_view_dims = view_dims;

	//request send... 
	if (jpip_client && jpip_client->is_active()){
		view_dims = new_dim;
		image_dims = whole_image_dims;
		update_client_window_of_interest();
		
		Sleep(1000);
		//wait depend on file size ....
		status_id = KDS_STATUS_CACHE;
		WaitTransmitComplete(MAX_WAIT_TIME * 100);	//최대 연결 시간을 길게 가져감 

		Sleep(1000);
		//다시 process하기 위해서 refresh을 해줌 
		compositor->refresh();
	}
		
	//if jpip
	//int decode_size = new_dim.size.x * abs(new_dim.size.y) * 4;
	int decode_size = (new_dim.size.y / 100) * new_dim.size.x;
	int i = 0;
	TCHAR	strTemp[128];
	while (compositor->process(decode_size, new_region_dims)){
		decode_size = new_region_dims.size.x * new_region_dims.size.y;
		OnCallbackDecompressStatus(i, decode_size);
		_stprintf(strTemp, _T("decoding process[%d] decode_size[%d : %d]\n"), i++, decode_size, new_dim.size.y * new_dim.size.x);
		OutputDebugString(strTemp);
		if (!PeekAndPump()){
			TRACE("break PeekAndPump \n");
			break;
		}
	}

	OnCallbackDecompressStatus(100, 0);
	TRACE("End decoding process \n");
	
	image_dims = old_image_dims;
	view_dims = old_view_dims;
	single_layer_idx = old_layer_index;
	//releae buffer
	//compositor->
	//UpdateViewDim(buffer_dims);
	if (layer_num != single_layer_idx){
		compositor->remove_ilayer(kdu_ilayer_ref(), true);
	}
	if (crop_buffer) crop_buffer->destory_filemap();
	return true;
}


void CKdu_showApp::RestoreViewRegion()
{
	compositor->set_colour_order(KDU_ARGB_ORDER);
	compositor->set_scale(transpose, vflip, hflip, rendering_scale);
	compositor->set_buffer_surface(buffer_dims);
	buffer = compositor->get_composition_bitmap(buffer_dims);

	if (save_layer_idx != single_layer_idx){
		compositor->remove_ilayer(kdu_ilayer_ref(), false);
		compositor->add_ilayer(single_layer_idx, kdu_dims(), kdu_dims());
		save_layer_idx = -1;
	}

	UpdateViewDim(buffer_dims);
}


bool b_first_request = 1;
void CKdu_showApp::UpdateViewDim(kdu_dims new_buffer_dims, bool need_redraw)
{
	if (new_buffer_dims.size.x < 8 || new_buffer_dims.size.y < 8) return;
	// Set surface and get buffer
	if (new_buffer_dims.size.x % 2) new_buffer_dims.size.x += 1;
	if (new_buffer_dims.size.y % 2) new_buffer_dims.size.y += 1;
	if (new_buffer_dims.pos.x % 2) new_buffer_dims.pos.x += 1;
	if (new_buffer_dims.pos.y % 2) new_buffer_dims.pos.y += 1;
		
	
	//need check right increase 
	TRACE("layer_idx %d buffer_dims %d,%d new_buffer_dims %d, %d \n", single_layer_idx, buffer_dims.pos.x, buffer_dims.size.x,
				new_buffer_dims.pos.x, new_buffer_dims.size.x);
	if (buffer_dims.pos.x > new_buffer_dims.pos.x && new_buffer_dims.size.x >= buffer_dims.size.x){
		if (((buffer_dims.pos.x - new_buffer_dims.pos.x) < 4) && ((new_buffer_dims.size.x - buffer_dims.size.x) < 4)){
			int new_pos_x = buffer_dims.pos.x > 4 ? buffer_dims.pos.x - 4: 0;
			int increase_size_x = (new_buffer_dims.size.x - buffer_dims.size.x);
			increase_size_x += 4;
			new_buffer_dims.pos.x = new_pos_x;
			new_buffer_dims.size.x = buffer_dims.size.x + increase_size_x;

			TRACE("new_buffer_dims %d, %d \n", new_buffer_dims.pos.x, new_buffer_dims.size.x);
		}
	}
	
	if (jpip_client){
		const char * status = jpip_client->get_status();
		OutputDebugStringA(status);
		if (strstr(status, "primary connection")){
			return;
		}
	}
	
	int wait_count = 0;
	int max_wait_count = 10;

	if (m_bMixing) {
		max_wait_count = 20;	//5secod
		compositor->remove_ilayer(kdu_ilayer_ref(), false);
		compositor->add_ilayer(single_layer_idx, kdu_dims(), kdu_dims());
	}
	
	//request send... 
	update_client_window_of_interest();
			
	//wait transmit complete 
	compositor->set_buffer_surface(new_buffer_dims);
	TRACE(" set buffer_dims %d, %d \n", new_buffer_dims.pos.x, new_buffer_dims.size.x);
	buffer = compositor->get_composition_bitmap(buffer_dims);
	TRACE(" get buffer_dims %d, %d \n", buffer_dims.pos.x, buffer_dims.size.x);
	new_buffer_dims.size.x = buffer->bitmap_info.bmiHeader.biWidth; // store release buffer dims
	new_buffer_dims.size.y = abs(buffer->bitmap_info.bmiHeader.biHeight); // store release buffer dims
	//new_buffer_dims = buffer_dims;
	int size_buffer = (abs(buffer->bitmap_info.bmiHeader.biHeight) + 4)*buffer->bitmap_info.bmiHeader.biWidth * 4;
	TRACE(" real buffer_dims %d, %d \n", buffer->bitmap_info.bmiHeader.biWidth, abs(buffer->bitmap_info.bmiHeader.biHeight));
		
	//if (jpip_client == NULL) return 0;
	jp_decode_progress = 0;
	int no_more_transfer_data = 0;
	if (jpip_client && jpip_client->is_active()){
		status_id = KDS_STATUS_CACHE;
		OutputDebugStringA("transfer data.............\n");

		while (1){
			while (compositor->process(size_buffer, buffer_dims));
			if ((jp_decode_progress > 98) || no_more_transfer_data) {
				break;
			}
			else{
				if (WaitTransmitComplete(200) == 0){
					no_more_transfer_data = 1;
					OutputDebugStringA("no more transfer data.............\n");
					break;
				}
				wait_count++;
				if (wait_count > max_wait_count) break;
			}
		}
	}
	else{
		while (compositor->process(size_buffer, buffer_dims));
	}

	//decoding 
	if (!compositor->get_total_composition_dims(image_dims))
	{ // Must have invalid scale
		compositor->flush_composition_queue();
		buffer = NULL;
		initialize_regions(); // This call will find a satisfactory scale
		in_idle = false;
	}
	
	if (jpip_client && jpip_client->is_active() )
	{
		// See if focus box needs to be changed
		if (jpip_client->get_window_in_progress(&tmp_roi))
		{
			if ((client_roi.region != tmp_roi.region) ||
				(client_roi.resolution != tmp_roi.resolution))
				change_client_focus(tmp_roi.resolution, tmp_roi.region);
			client_roi.copy_from(tmp_roi);
		}

		// See if cache has been updated.
		int new_bytes = (int)
			(jpip_client->get_transferred_bytes(KDU_MAIN_HEADER_DATABIN) +
			jpip_client->get_transferred_bytes(KDU_TILE_HEADER_DATABIN) +
			jpip_client->get_transferred_bytes(KDU_PRECINCT_DATABIN) +
			jpip_client->get_transferred_bytes(KDU_META_DATABIN));
		if (new_bytes != last_transferred_bytes && !m_bMixing)
		{
			if (!configuration_complete)
				initialize_regions();
			if (refresh_timer_id == 0){
				OutputDebugStringA("set timer refresh_timer_id.............\n");
				refresh_timer_id = m_pMainWnd->SetTimer(KDU_SHOW_REFRESH_TIMER_IDENT, 200, NULL);
			}
				
			last_transferred_bytes = new_bytes;
		}
		else{
			jpip_progress->SetPos(100);
			status_id = KDS_STATUS_LAYER_RES;
		}
	}

	buffer_dims = new_buffer_dims;
	//decodeing second index
	if (m_bMixing){
		if (second_buffer && (second_buffer->size != buffer->size)) {
			RELEASE_BUFFER(second_buffer);
		}
		if (second_buffer == NULL)	second_buffer = new kd_bitmap_buf();

		//copy main buffer
		second_buffer->copy_bitmap(buffer);

		//toggle buffer index 
		//int temp_index = second_mix_layer_idx;
		//second_mix_layer_idx = single_layer_idx;
		//single_layer_idx = temp_index;

		//change layer index
		compositor->remove_ilayer(kdu_ilayer_ref(), false);
		compositor->add_ilayer(second_mix_layer_idx, kdu_dims(), kdu_dims());

		//request send... 
		update_client_window_of_interest();

		compositor->set_buffer_surface(new_buffer_dims);
		buffer = compositor->get_composition_bitmap(buffer_dims);
		
		jp_decode_progress = 0;
		wait_count = 0;
		no_more_transfer_data = 0;
		if (jpip_client && jpip_client->is_active()){
			while (1){
				while (compositor->process(size_buffer, buffer_dims));
				if ((jp_decode_progress > 98) || no_more_transfer_data ) {
					break;
				}
				else{
					if (WaitTransmitComplete(200) == 0) break;
					no_more_transfer_data = 1;
					wait_count++;
					if (wait_count > max_wait_count) break;
				}
			}
		}
		else{
			while (compositor->process(size_buffer, buffer_dims));
		}

		buffer_dims = new_buffer_dims;
		
		//Mix layer 
		MixingLayer();
		
		if (jpip_client && jpip_client->is_active()){
			if (b_first_request){
				int new_bytes = (int)
					(jpip_client->get_transferred_bytes(KDU_MAIN_HEADER_DATABIN) +
					jpip_client->get_transferred_bytes(KDU_TILE_HEADER_DATABIN) +
					jpip_client->get_transferred_bytes(KDU_PRECINCT_DATABIN) +
					jpip_client->get_transferred_bytes(KDU_META_DATABIN));
				if (new_bytes != last_transferred_bytes)
				{
					if (!configuration_complete)
						initialize_regions();
					if (refresh_timer_id == 0){
						OutputDebugStringA("set timer refresh_timer_id.............\n");
						refresh_timer_id = m_pMainWnd->SetTimer(KDU_SHOW_REFRESH_TIMER_IDENT, 200, NULL);
						b_first_request = 0;
					}

					last_transferred_bytes = new_bytes;
				}
			}
			else{
				status_id = KDS_STATUS_LAYER_RES;
				b_first_request = 1;
			}
		}
	}
	else{
		//Second Mix Layer가 변경이 됨.
		second_mix_layer_idx = -1; 
	}
	
	//Update View 
	if (view_buffer && (view_buffer->size != view_dims.size)) {
		RELEASE_BUFFER(view_buffer);
	}
	if (view_buffer == NULL){
		view_buffer = new kd_bitmap_buf();
		view_buffer->create_bitmap(view_dims.size);
	}

	//Update 
	//SIZE size = { buffer->size.x, buffer->size.y };
	SIZE size = { image_dims.size.x, image_dims.size.y };
	RECT rcCanvas = { view_dims.pos.x, view_dims.pos.y, view_dims.pos.x + view_dims.size.x, view_dims.pos.y + view_dims.size.y };
	RECT rcFrame = { x_offset, y_offset, x_offset + view_dims.size.x, y_offset + view_dims.size.y };
	m_DrawObjMgr.UpdateViewImageNCanvase(size, rcCanvas, m_rotationStatus, rendering_scale, rcFrame);
		
	// Update related display properties
	if (need_redraw){
		child_wnd->UpdateViewRegion();
	}

	if (m_pOverview != NULL){
		m_pOverview->UpdateViewPos((float)view_dims.pos.x / image_dims.size.x, (float)view_dims.pos.y / image_dims.size.y,
			(float)view_dims.size.x / image_dims.size.x, (float)view_dims.size.y / image_dims.size.y, image_dims.size.x, image_dims.size.y);
	}

	display_status();
}


void CKdu_showApp::OnChangeRenderScale(UINT nID)
{
	// TODO: Add your command handler code here
	if (buffer == NULL) return;

	if (nID == -1) {
		FitImageToClient();
		return;
	}

  int scale_index = nID;

  rendering_scale = FIX_SCALE_VALUE[scale_index];
  calculate_view_anchors();
  if (focus_anchors_known)
    { view_centre_x = focus_centre_x; view_centre_y = focus_centre_y; }
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
  //get_layer(single_layer_idx); //MJY
  if (m_pOverview != NULL) m_pOverview->Invalidate(); // MJY
  //get_layer(single_layer_idx); //MJY
}


void CKdu_showApp::OnToolEditmetadata()
{
	if (m_pEditMetadata == NULL)
	{
		m_pEditMetadata = new CEditMetadata(this);
		m_pEditMetadata->ShowWindow(SW_SHOW);
	}
	else{
		m_pEditMetadata->ShowWindow(SW_SHOW);
	}
}


void CKdu_showApp::OnMeasurementScaling()
{
	// TODO: Add your command handler code here
	if (m_pScaling == NULL){
		m_pScaling = new CScaling(this);
	}
	else{
		m_pScaling->ShowWindow(SW_SHOW);
	}
}


int CKdu_showApp::WaitTransmitComplete(int total_wait_time)
{
	if (jpip_client == NULL) return 0;
	CWaitCursor wait;

	int remain = 1;
	int pos = 0;
	int wait_time = 50;
	float bit_rate = 100000;//500KByte/sec

	kdu_long previous_transfer = 0;
	for (int i = 0; i< total_wait_time / wait_time; i++) {
		double active_secs;
		kdu_long global_bytes = jpip_client->get_received_bytes(-1);
		kdu_long queue_bytes = jpip_client->get_received_bytes(client_request_queue_id, &active_secs);
		bit_rate = queue_bytes / active_secs;

		TRACE("Wait %d => global_bytes : %d , queue_bytes : %d previous_transfer : %d  diff : %d active_secs : %f (%f) status (%s)\n",
			i, global_bytes, queue_bytes, previous_transfer, global_bytes - previous_transfer, active_secs, bit_rate, jpip_client->get_status());

		if (1000000 < bit_rate) wait_time = 40;
		else if (750000 < bit_rate) wait_time = 80;
		else if (500000 < bit_rate) wait_time = 120;
		else if (250000 < bit_rate) wait_time = 160;
		else if (100000 < bit_rate) wait_time = 200;
		else if (50000 < bit_rate) wait_time = 300;
		else wait_time = 400;
		
		if (global_bytes != previous_transfer ){
			previous_transfer = global_bytes;
			//if (jpip_progress != NULL) jpip_progress->SetPos(pos+=10);
			display_status();
			Sleep(wait_time);
		}
		else{
			if (total_wait_time == MAX_WAIT_TIME){
				wait_time = wait_time * 10;
				if (wait_time < 800){ //조금 늦더라도 충분한 값을 넣어 줌 (가끔 transfer가 overview가 깨지는 경우가 발생함)
					wait_time = 800;
				} 
				Sleep(wait_time);	
			}
			else{
				Sleep(wait_time);
			}
			remain = 0;
			break;
		}
	}

	wait.Restore();

	return remain;

}


void CKdu_showApp::OnFileSaveasOtherType()
{
    TCHAR     szPath[_MAX_PATH] ;
    //{
    //    TCHAR   szDrive[_MAX_DRIVE],
    //            szDir[_MAX_DIR],
    //            szFname[_MAX_FNAME] ;
    //    _tsplitpath (GetPathName(), szDrive, szDir, szFname, NULL) ;
    //    _tmakepath (szPath, szDrive, szDir, szFname, _T("")) ;
    //}

    // set title to path
	memset(szPath, 0, _MAX_PATH);
    if (CString(szPath).IsEmpty())
    {
        TCHAR     szCurrDir[MAX_PATH] ;
        ::GetCurrentDirectory (MAX_PATH, szCurrDir) ;

        CString   strTmp (szCurrDir) ;
        //if (strTmp[strTmp.GetLength()-1] == _T('\\'))
        //    strTmp += GetTitle() ;
        //else
        //    strTmp += _T("\\") + GetTitle() ;
        lstrcpy (szPath, strTmp) ;
    }

    CString       Filter ;
    Filter.LoadString (IDS_SAVEFILE_FORMAT) ; // file format
    DlgSaveFile   dlgFile (FALSE, NULL, szPath, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, Filter) ;
    if (dlgFile.DoModal() == IDCANCEL)
        return ;

    bstr_t      strFile = (LPCTSTR)dlgFile.GetPathName() ;
	FCObjImage	image;

	  int bitmap_row_gap;
  kdu_uint32 *bitmap_buffer;
  HBITMAP bitmap = buffer->access_bitmap(bitmap_buffer,bitmap_row_gap);

 
	BITMAPINFO	bmpInfo;
	kdu_uint32	*bmp_buffer;
	HBITMAP		bmp;

  	  memset(&bmpInfo,0,sizeof(bmpInfo));
	  bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	  bmpInfo.bmiHeader.biWidth = view_dims.size.x;
	  bmpInfo.bmiHeader.biHeight = view_dims.size.y;
	  //bmpInfo.bmiHeader.biSizeImage = view_dims.size.x * view_dims.size.y*4;
	  bmpInfo.bmiHeader.biPlanes = 1;
	  bmpInfo.bmiHeader.biBitCount = 32;
	  bmpInfo.bmiHeader.biCompression = BI_RGB;

	  bmp = CreateDIBSection(NULL,&bmpInfo,DIB_RGB_COLORS,(void**)&bmp_buffer,NULL,0);
	  if (bmp == NULL)
		{ kdu_error e; e << "Unable to allocate sufficient bitmap surfaces "
			"to service."; }

  	  kdu_uint32 *dptr, *ddptr = bmp_buffer;
	  kdu_uint32 *sptr, *ssptr = bitmap_buffer;
	  dptr = ddptr; sptr = ssptr;

	  for (int i=0; i<view_dims.size.x*view_dims.size.y; i++) 
	  	*(dptr++) = *(sptr++);

    BITMAPINFOHEADER *DIBStream = (BITMAPINFOHEADER *)GlobalAlloc(GHND, bmpInfo.bmiHeader.biSize+view_dims.size.x*view_dims.size.y);

  memcpy((void*)DIBStream, (void*)&bmpInfo.bmiHeader, bmpInfo.bmiHeader.biSize);
  BYTE *p = (BYTE *)DIBStream + bmpInfo.bmiHeader.biSize;
  memcpy((void*)p, (void *)bmp_buffer, view_dims.size.x*view_dims.size.y);

  image.LoadDIBStream((void *)&DIBStream, bmpInfo.bmiHeader.biSize+view_dims.size.x*view_dims.size.y );
    if (!image.Save (strFile))
    {
        ::AfxMessageBox (_T("Save image failed"), MB_ICONEXCLAMATION) ;
    }
}


void CKdu_showApp::OnUpdateToolOverviewwindow(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
  pCmdUI->Enable(buffer != NULL);
}

void CKdu_showApp::OnUpdateToolOverviewResetwindow(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(buffer != NULL);
}

void CKdu_showApp::OnUpdateToolOverviewAttach(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(buffer != NULL);
	pCmdUI->SetCheck(frame_wnd->m_attach_overview);
}

void CKdu_showApp::OnUpdateToolEditmetadata(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(buffer != NULL  && !m_Analyzing);
}

void CKdu_showApp::OnUpdateMeasurementScaling(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(buffer != NULL && !m_Analyzing);//&& m_pScaling == NULL);

}

void CKdu_showApp::OnUpdateFileSaveasOtherType(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	//pCmdUI->Enable(buffer != NULL);
	pCmdUI->Enable(false);
}

void CKdu_showApp::OnUpdateMeasurementSavemeasurement(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	//pCmdUI->Enable(buffer != NULL);

	//for presentation
	pCmdUI->Enable(0);
}


void CKdu_showApp::OnFileLoadConf()
{
	TCHAR initial_dir[MAX_PATH];
	TCHAR filename[MAX_PATH];
	OPENFILENAME ofn;
	memset(&ofn, 0, sizeof(ofn)); ofn.lStructSize = sizeof(ofn);
	_tcscpy(initial_dir, settings.get_open_save_dir());
	if (*initial_dir == '\0')
		GetCurrentDirectory(MAX_PATH, initial_dir);

	ofn.hwndOwner = m_pMainWnd->GetSafeHwnd();
	ofn.lpstrFilter = _T("VPViewer Config (*.conf)\0*.conf\0\0");
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = filename; filename[0] = '\0';
	ofn.nMaxFile = MAX_PATH - 1;
	ofn.lpstrTitle = _T("Load VPViewer Config File");
	ofn.lpstrInitialDir = initial_dir;
	ofn.Flags = OFN_FILEMUSTEXIST;
	if (!GetOpenFileName(&ofn))
		return;

	_tcscpy(initial_dir, filename);

	//Remove current DrawObjMgr
	m_DrawObjMgr.DeleteObj();
	m_DrawObjMgr.ResetSelectMode();

	m_config.Open(filename, &m_DrawObjMgr);
	UpdateViewDim(buffer_dims);

}


void CKdu_showApp::OnUpdateFileLoadConf(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(buffer != NULL && !m_Analyzing);
}


void CKdu_showApp::OnFileSaveConfAs()
{
	
	CString       Filter = "JPViewer Config (*.conf)|*.conf||";
	DlgSaveFile   dlgFile(FALSE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, Filter);
	if (dlgFile.DoModal() == IDCANCEL) return;

	m_config.SaveConf( dlgFile.GetPathName() );
}


void CKdu_showApp::OnUpdateFileSaveConfAs(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(buffer != NULL && !m_Analyzing);
}

void CKdu_showApp::OnMeasurementGrid()
{
	if (m_pGrid->m_hWnd != NULL)
		m_pGrid->DestroyWindow();

	m_DrawObjMgr.ToggleShowGridObj();
	if (m_DrawObjMgr.IsShowGridObj())
	{
		m_DrawObjMgr.ShowGridObj(true);
		m_pGrid->mgr = &m_DrawObjMgr;
		m_pGrid->app = this;
		if (m_pGrid->m_hWnd == NULL)
			m_pGrid->Create(IDD_GRID);
		m_pGrid->ShowWindow(SW_SHOW);
	}
	child_wnd->UpdateViewRegion();
}

void CKdu_showApp::OnMeasurementGridProperty()
{
	if (m_pGrid->m_hWnd != NULL)
		m_pGrid->DestroyWindow();
	if (m_DrawObjMgr.IsShowGridObj())
	{
		m_pGrid->mgr = &m_DrawObjMgr;
		m_pGrid->app = this;
		if (m_pGrid->m_hWnd == NULL)
			m_pGrid->Create(IDD_GRID);
		m_pGrid->ShowWindow(SW_SHOW);
	}
	child_wnd->UpdateViewRegion();
}


void CKdu_showApp::OnUpdateMeasurementGrid(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(buffer != NULL && !m_Analyzing);
	pCmdUI->SetCheck(m_DrawObjMgr.IsShowGridObj());
}

void CKdu_showApp::OnUpdateMeasurementGridProperty(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(buffer != NULL && m_DrawObjMgr.IsShowGridObj());
}


// FIT TO SCREEN
// ============================================================================
void CKdu_showApp::FitImageToClient()
{
	if (buffer == NULL) return;

	CRect client_rect;
	::GetClientRect(child_wnd->GetSafeHwnd(), client_rect);

	float new_renderscale = rendering_scale * client_rect.Height() / image_dims.size.y;

	if (image_dims.size.x * (new_renderscale / rendering_scale) > client_rect.Width())
		new_renderscale = rendering_scale * client_rect.Width() / image_dims.size.x;

	rendering_scale = new_renderscale;

	calculate_view_anchors();
	if (focus_anchors_known)
	{
		view_centre_x = focus_centre_x; view_centre_y = focus_centre_y;
	}
	view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
	initialize_regions();

	if (m_pOverview != NULL) m_pOverview->Invalidate(); // MJY
}
// ============================================================================


void CKdu_showApp::OnEditExportmetadataassvg()
{
	CString file_name;
	
	file_name.Format(L"%s.svg", open_filename);
	CSVGFileDialog dlg(FALSE, _T("*.svg"), file_name, OFN_OVERWRITEPROMPT, _T("Vector Grahpic(*.svg)|*.svg|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		file_name = dlg.GetPathName();
		if (file_name.Right(4) != ".svg")
		{
			file_name += ".svg";
		}
		SVGExport(file_name, open_file_pathname, &m_DrawObjMgr, dlg.m_bMeasureCheck, dlg.m_RatioIndex,
						m_config.getResolution().width, m_config.getResolution().height);
	}
}


void CKdu_showApp::OnUpdateExportmetadataassvg(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(buffer != NULL && !m_Analyzing);
}
