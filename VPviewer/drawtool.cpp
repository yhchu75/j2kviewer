// drawtool.cpp - implementation for drawing tools
//


#include "stdafx.h"
#include "kdu_show.h"
#include "drawdoc.h"
#include "ChildView.h"
#include "drawobj.h"
#include "drawtool.h"
#include <math.h>

/////////////////////////////////////////////////////////////////////////////
// CDrawTool implementation

CPtrList CDrawTool::c_tools;

static CSelectTool selectTool;
static CRectTool lineTool(line);
static CRectTool rectTool(rect);
static CRectTool roundRectTool(roundRect);
static CRectTool ellipseTool(ellipse);
static CPolyTool areaTool(area);
static CPolyTool curveTool(curve);
static CPolyTool grainPerimeterTool(grainPerimeter);
static CPolyTool grainCoatingTool(grainCoating);

CPoint CDrawTool::c_down;
UINT CDrawTool::c_nDownFlags;
CPoint CDrawTool::c_last;
DrawShape CDrawTool::c_drawShape = selection;

CDrawTool::CDrawTool(DrawShape drawShape)
{
	m_drawShape = drawShape;
	c_tools.AddTail(this);
}

CDrawTool* CDrawTool::FindTool(DrawShape drawShape)
{
	POSITION pos = c_tools.GetHeadPosition();
	while (pos != NULL)
	{
		CDrawTool* pTool = (CDrawTool*)c_tools.GetNext(pos);
		if (pTool->m_drawShape == drawShape)
			return pTool;
	}

	return NULL;
}

void CDrawTool::OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point)
{
	 //deactivate any in-place active item on this view!
	// MJY : comment out:see this later
	//COleClientItem* pActiveItem = pView->GetDocument()->GetInPlaceActiveItem(pView);
	//if (pActiveItem != NULL)
	//{
	//	pActiveItem->Close();
	//	ASSERT(pView->GetDocument()->GetInPlaceActiveItem(pView) == NULL);
	//}


	pView->SetCapture();
	c_nDownFlags = nFlags;
	c_down = point;
	c_last = point;
	//TRACE("OnLButtonDown : c_down %d %d\n", c_down.x, c_down.y);
}

void CDrawTool::OnLButtonDblClk(CChildView* /*pView*/, UINT /*nFlags*/, const CPoint& /*point*/)
{
}
void CDrawTool::OnRButtonDown(CChildView* /*pView*/, UINT /*nFlags*/, const CPoint& /*point*/)
{
}
void CDrawTool::OnLButtonUp(CChildView* /*pView*/, UINT /*nFlags*/, const CPoint& point)
{
	ReleaseCapture();

	if (point == c_down)
		c_drawShape = selection;
}

void CDrawTool::OnMouseMove(CChildView* /*pView*/, UINT /*nFlags*/, const CPoint& point)
{
	c_last = point;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}

//void CDrawTool::OnEditProperties(CChildView* /*pView*/)
//{
//}

void CDrawTool::OnCancel()
{
	c_drawShape = selection;
}

////////////////////////////////////////////////////////////////////////////
// CResizeTool

enum SelectMode
{
	none,
	netSelect,
	move,
	size
};

SelectMode selectMode = none;
int nDragHandle;

CPoint lastPoint;

CSelectTool::CSelectTool()
	: CDrawTool(selection)
{
}

void CSelectTool::OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point)
{
	CPoint local = point;
	//pView->ClientToDoc(local); //MJY comment out

	CDrawObj* pObj;
	selectMode = none;

	//// Check for resizing (only allowed on single selections)
	//if (pView->m_selection.GetCount() == 1)
	//{
	//	pObj = pView->m_selection.GetHead(); 
	//	nDragHandle = pObj->HitTest(local, pView, TRUE);
	//	if (nDragHandle != 0)
	//		selectMode = size;
	//}

	// See if the click was on an object, select and start move if so
	if (selectMode == none)
	{
		pObj = pView->m_pDoc->ObjectAt(local, pView);

		if (pObj != NULL)
		{
			selectMode = move;

			if (!pView->IsSelected(pObj))
			{
				TRACE("is about to select notSelectedObj................\n");
				pView->Select(pObj, (nFlags & MK_SHIFT) != 0);
			}

			// Ctrl+Click clones the selection...
			if ((nFlags & MK_CONTROL) != 0)
				pView->CloneSelection();
		}
	}

	// Click on background, start a net-selection
	if (selectMode == none)
	{
		if ((nFlags & MK_SHIFT) == 0)
			pView->Select(NULL);

		selectMode = netSelect;

		CClientDC dc(pView);
		CRect rect(point.x, point.y, point.x, point.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);
	}

	lastPoint = local;
	CDrawTool::OnLButtonDown(pView, nFlags, point);
}

void CSelectTool::OnLButtonDblClk(CChildView* pView, UINT nFlags, const CPoint& point)
{
	if ((nFlags & MK_SHIFT) != 0)
	{
		// Shift+DblClk deselects object...
		CPoint local = point;
		pView->ClientToDoc(local); // MJY comment out
		CDrawObj* pObj = pView->m_pDoc->ObjectAt(local, pView);
		if (pObj != NULL)
			pView->Deselect(pObj);
	}
	else
	{
		//// "Normal" DblClk opens properties, or OLE server...
		//if (pView->m_selection.GetCount() == 1)
		//	pView->m_selection.GetHead()->OnOpen(pView);
	}

	CDrawTool::OnLButtonDblClk(pView, nFlags, point);
}

//void CSelectTool::OnEditProperties(CChildView* pView)
//{
//	if (pView->m_selection.GetCount() == 1)
//		pView->m_selection.GetHead()->OnEditProperties();
//}

void CSelectTool::OnLButtonUp(CChildView* pView, UINT nFlags, const CPoint& point)
{
	//TRACE("hihihhiih........\n");
	if (pView->GetCapture() == pView)
	{
		TRACE("CSelectTool::ONLButtonUp GetCapture() == pView...........\n");
		if (selectMode == netSelect)
		{
			TRACE("CSelectTool::OnLButtonUp......selectMode == netSelect............\n");
			CClientDC dc(pView);
			CRect rect(c_down.x, c_down.y, c_last.x, c_last.y);
			rect.NormalizeRect();
			dc.DrawFocusRect(rect);

			pView->SelectWithinRect(rect, TRUE);
		}
		else if (selectMode != none)
		{
			TRACE("CSelectTool::OnLButtonUp.......selectMode != none %d\n", selectMode);
			//pView->GetDocument()->UpdateAllViews(pView);
			pView->OnUpdate(HINT_UPDATE_SELECTION, NULL);//MJY : watch out
		}
	}

	CDrawTool::OnLButtonUp(pView, nFlags, point);
}

void CSelectTool::OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point)
{
	//consider this LATER
	if (pView->GetCapture() != pView)
	{
		if (c_drawShape == selection && pView->m_selection.GetCount() == 1)
		{
			CDrawObj* pObj = pView->m_selection.GetHead();
			CPoint local = point;
			//pView->ClientToDoc(local); //MJY comment out
			TRACE("CSelectTool::OnMouseMove... GetCount == 1, is about to HitTest... local %d %d\n", local.x, local.y);
			int nHandle = pObj->HitTest(local, pView, TRUE);
			//TRACE("after HistTest, nhandle = %d\n", nHandle);
			if (nHandle != 0)
			{
				SetCursor(pObj->GetHandleCursor(nHandle));
				return; // bypass CDrawTool
			}
		}
		if (c_drawShape == selection)
		{
			//TRACE("c_drawShape ==  selection... getCount != 1.......\n");
			TRACE("CSelectTool::OnMouseMove... GetCount !=1 ....\n");
			CDrawTool::OnMouseMove(pView, nFlags, point);
		}
		return;
	}

	if (selectMode == netSelect)
	{
		CClientDC dc(pView);
		CRect rect(c_down.x, c_down.y, c_last.x, c_last.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);
		rect.SetRect(c_down.x, c_down.y, point.x, point.y);
		rect.NormalizeRect();
		dc.DrawFocusRect(rect);

		CDrawTool::OnMouseMove(pView, nFlags, point);
		return;
	}

	CPoint local = point;
	//pView->ClientToDoc(local); // MJY comment out
	TRACE("CSelectTool::OnMouseMove ... lastPoint(%d %d) local(%d %d)\n", lastPoint.x, lastPoint.y, point.x, point.y);
	CPoint delta = (CPoint)(local - lastPoint);

	POSITION pos = pView->m_selection.GetHeadPosition();
	while (pos != NULL)
	{
		CDrawObj* pObj = pView->m_selection.GetNext(pos);
		CRect position = pObj->m_position;

		if (selectMode == move)
		{
			position += delta;
			TRACE("CChildView::OnMouseMove...... selectMode==move, is about to MoveTo\n");
			pObj->MoveTo(position, pView);
		}
		else if (nDragHandle != 0)
		{
			pObj->MoveHandleTo(nDragHandle, local, pView);
		}
	}

	lastPoint = local;

	if (selectMode == size && c_drawShape == selection)
	{
		c_last = point;
		SetCursor(pView->m_selection.GetHead()->GetHandleCursor(nDragHandle));
		return; // bypass CDrawTool
	}

	c_last = point;

	if (c_drawShape == selection)
		CDrawTool::OnMouseMove(pView, nFlags, point);
}

////////////////////////////////////////////////////////////////////////////
// CRectTool (does rectangles, round-rectangles, and ellipses)

CRectTool::CRectTool(DrawShape drawShape)
	: CDrawTool(drawShape)
{
}

void CRectTool::OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point)
{

	CDrawTool::OnLButtonDown(pView, nFlags, point);

	CPoint local = point;
	//pView->ClientToDoc(local); // MJY comment out

	CDrawRect* pObj = new CDrawRect(CRect(local, CSize(0, 0)));
	switch (m_drawShape)
	{
	default:
		ASSERT(FALSE); // unsuported shape!

	case rect:
		pObj->m_nShape = CDrawRect::rectangle;
		break;

	case roundRect:
		pObj->m_nShape = CDrawRect::roundRectangle;
		break;

	case ellipse:
		pObj->m_nShape = CDrawRect::ellipse;
		break;

	case line:
		pObj->m_nShape = CDrawRect::line;
		break;
	}
	//pObj->m_currentScale = pView->app->rendering_scale; //MJY
	pView->m_pDoc->Add(pObj);
	pView->Select(pObj);

	//CDrawRect *ptr = (CDrawRect *)pView->m_selection.GetTail();
	//TRACE("just got in : %d %d %d %d\n", ptr->m_position.left, ptr->m_position.right, ptr->m_position.top, ptr->m_position.bottom);

	selectMode = size;
	nDragHandle = 1;
	lastPoint = local;
}

void CRectTool::OnLButtonDblClk(CChildView* pView, UINT nFlags, const CPoint& point)
{
	CDrawTool::OnLButtonDblClk(pView, nFlags, point);
}

void CRectTool::OnLButtonUp(CChildView* pView, UINT nFlags, const CPoint& point)
{
	//MJY
	CDrawRect *ptr = (CDrawRect *)pView->m_pDoc->m_objects.GetTail();
	TRACE("???? : %d %d %d %d\n", ptr->m_position.left, ptr->m_position.right, ptr->m_position.top, ptr->m_position.bottom);

	int i,j;
	//i = ptr->m_position.right - ptr->m_position.left;
	//j = ptr->m_position.bottom - ptr->m_position.top;
	//
	//float length = (float)sqrt((double)(i*i+j*j));
	//ptr->m_lengthFull = length / pView->app->rendering_scale;

	//if (pView->app->appliedScale != 1.0f)
	//	ptr->m_length = ptr->m_length * pView->app->scanningRes / pView->app->rendering_scale ;


	//TRACE("???? length = %5.2f\n", ptr->m_length);

	// MJY position in full image for m_position
	ptr->left = (int)((float)(ptr->m_position.left - pView->app->x_offset + pView->app->view_dims.pos.x) / pView->app->rendering_scale);
	ptr->right = (int)((float)(ptr->m_position.right - pView->app->x_offset + pView->app->view_dims.pos.x) / pView->app->rendering_scale);
	ptr->top = (int)((float)(ptr->m_position.top - pView->app->y_offset + pView->app->view_dims.pos.y) / pView->app->rendering_scale);
	ptr->bottom = (int)((float)(ptr->m_position.bottom - pView->app->y_offset + pView->app->view_dims.pos.y) / pView->app->rendering_scale);

	i = ptr->right - ptr->left;
	j = ptr->bottom - ptr->top;
	float length = (float)sqrt((double)(i*i + j*j));
	ptr->m_lengthFull = length;


	//POSITION pos = pView->m_selection.Find(ptr);
	//if ( pos != NULL)
	//{
	//	CDrawObj *pObj = pView->m_selection.GetAt(pos);
	//	TRACE("selection fullposition (%d %d %d %d)\n", pObj->left, pObj->top, pObj->right, pObj->bottom);
	//}


	//CDC *pdc = pView->GetDC();
	//CString str;
	//str.Format(_T("%5.2f"), ptr->m_length);
	//pdc->TextOutA( (ptr->m_position.right + ptr->m_position.left)/2 + 10, 
	//	(ptr->m_position.top + ptr->m_position.bottom)/2+10, str);
	//pView->ReleaseDC(pdc);


	//MJY

	//TRACE("c_down %d %d............\n", c_down.x, c_down.y);
	//TRACE("point %d %d..............\n", point.x, point.y);
	if (point == c_down)
	{
		// Don't create empty objects...
		//TRACE("remove empty object.......\n");
		CDrawObj *pObj = pView->m_selection.GetTail();
		pView->m_pDoc->Remove(pObj);
		pObj->Remove();
		selectTool.OnLButtonDown(pView, nFlags, point); // try a select!
	}

	//CString pixel(_T("pixel")); CString mmeter(_T("��m"));
	//if (pView->app->appliedScale == 1.0) ptr->unit = pixel;
	//else 	ptr->unit = mmeter;

	//if (pView->m_pMeasureTable != NULL)  // MJY_042109
	//	pView->m_pMeasureTable->OnBnClickedRefresh();

	selectTool.OnLButtonUp(pView, nFlags, point);
}

void CRectTool::OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
	selectTool.OnMouseMove(pView, nFlags, point);
}


////////////////////////////////////////////////////////////////////////////
// CPolyTool

CPolyTool::CPolyTool(DrawShape drawShape)
	: CDrawTool(drawShape)
{
	m_pDrawObj = NULL;
}

void CPolyTool::OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point)
{

	CDrawTool::OnLButtonDown(pView, nFlags, point);

	CPoint local = point;
	//pView->ClientToDoc(local); //MJY comment out

	if (m_pDrawObj == NULL)
	{
		pView->SetCapture();

		m_pDrawObj = new CDrawPoly(CRect(local, CSize(0, 0)));
		switch (m_drawShape)
		{
		default:
			ASSERT(FALSE); // unsuported shape!

		case area:
			m_pDrawObj->m_nType = CDrawPoly::area;
			break;
		case curve:
			m_pDrawObj->m_nType = CDrawPoly::curve;
			break;
		case grainPerimeter:
			m_pDrawObj->m_nType = CDrawPoly::grainPerimeter;
			break;
		case grainCoating:
			m_pDrawObj->m_nType = CDrawPoly::grainCoating;
			break;
		}

		pView->m_pDoc->Add(m_pDrawObj);
		pView->Select(m_pDrawObj);
		m_pDrawObj->AddPoint(local, pView);
	}
	else if (local == m_pDrawObj->m_points[0])
	{
		// Stop when the first point is repeated...
		ReleaseCapture();
		m_pDrawObj->m_nPoints -= 1;
		if (m_pDrawObj->m_nPoints < 2)
		{
			m_pDrawObj->Remove();
		}
		else
		{
			pView->InvalObj(m_pDrawObj);
		}
		m_pDrawObj = NULL;
		c_drawShape = selection;
		return;
	}

	local.x += 1; // adjacent points can't be the same!
	m_pDrawObj->AddPoint(local, pView);

	selectMode = size;
	nDragHandle = m_pDrawObj->GetHandleCount();
	lastPoint = local;
}

void CPolyTool::OnLButtonUp(CChildView* /*pView*/, UINT /*nFlags*/, const CPoint& /*point*/)
{
	// Don't release capture yet!
}

void CPolyTool::OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point)
{
	if (m_pDrawObj != NULL && (nFlags & MK_LBUTTON) != 0)
	{
		CPoint local = point;
		//pView->ClientToDoc(local); // MJY comment out
		TRACE("CPolyTool::OnMouseMove  before addpoint (%d %d)\n");
		m_pDrawObj->AddPoint(local, pView);
		//nDragHandle = m_pDrawObj->GetHandleCount();
		lastPoint = local;
		c_last = point;
//		SetCursor(AfxGetApp()->LoadCursor(IDC_PENCIL));
	}
	else
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_CROSS));
		TRACE("CPolyTool::OnMouseMove before selectTool.OnMouseMove...\n");
		selectTool.OnMouseMove(pView, nFlags, point);
	}
}

void CPolyTool::OnRButtonDown(CChildView* pView, UINT , const CPoint& )
{
	ReleaseCapture();

	int nPoints = m_pDrawObj->m_nPoints;
	//TRACE("m_nPoints = %d\n", nPoints);
	if (nPoints > 2 )
		//&&		(m_pDrawObj->m_points[nPoints - 1] == m_pDrawObj->m_points[nPoints - 2] ||
		//m_pDrawObj->m_points[nPoints - 1].x - 1 == m_pDrawObj->m_points[nPoints - 2].x &&
		//m_pDrawObj->m_points[nPoints - 1].y == m_pDrawObj->m_points[nPoints - 2].y))

	{
		// Nuke the last point if it's the same as the next to last...
		for (int i=1; i<nPoints-1; i++)
		{
			m_pDrawObj->m_points[i].x = m_pDrawObj->m_points[i+1].x;
			m_pDrawObj->m_points[i].y = m_pDrawObj->m_points[i+1].y;
			m_pDrawObj->m_pointsFull[i].x = m_pDrawObj->m_pointsFull[i+1].x;
			m_pDrawObj->m_pointsFull[i].y = m_pDrawObj->m_pointsFull[i+1].y;
		}

		m_pDrawObj->m_nPoints -= 1;
		nPoints = m_pDrawObj->m_nPoints;

//TRACE("inside............type %d\n", m_pDrawObj->m_nType);
		//m_pDrawObj->m_nPoints -= 1;
		//int tempSum = 0;
		//float length = 0.0F;

		switch(c_drawShape)
		{
		case area :	// MJY : compute the area of polygon
			{
			int tempSum = 0;
			for (int i=0; i < m_pDrawObj->m_nPoints-2; i++)
				tempSum += (m_pDrawObj->m_points[i].x * m_pDrawObj->m_points[i+1].y - 
							m_pDrawObj->m_points[i+1].x * m_pDrawObj->m_points[i].y);
			// last point and first point
			tempSum += ( m_pDrawObj->m_points[m_pDrawObj->m_nPoints-1].x * m_pDrawObj->m_points[0].y -
                        m_pDrawObj->m_points[0].x * m_pDrawObj->m_points[m_pDrawObj->m_nPoints-1].y );
			if (tempSum < 0) tempSum *= (-1);

			double area = (double) tempSum / 2.0;
			m_pDrawObj->m_areaFull = (float) area / (pView->app->rendering_scale*pView->app->rendering_scale);
			//for test
			//for (int i=0; i< m_pDrawObj->m_nPoints-1; i++)
			//	TRACE("point%d (%d %d)\n", i, m_pDrawObj->m_points[i].x, m_pDrawObj->m_points[i].y);

			//TRACE("area :::::::::  %d %5.2f\n", m_pDrawObj->m_nPoints, m_pDrawObj->m_area);


			//if (pView->app->appliedScale != 1.0f)
			//	m_pDrawObj->m_area = m_pDrawObj->m_area * (pView->app->scanningRes*pView->app->scanningRes) / (pView->app->rendering_scale*pView->app->rendering_scale);

			//TRACE("area :::::::::  %d %5.2f\n", m_pDrawObj->m_nPoints, m_pDrawObj->m_area);
			}
			break;

		case curve :
		case grainCoating :
		case grainPerimeter :
			{
			float length = 0.0F;
			//TRACE("number of points = %d......\n", m_pDrawObj->m_nPoints);
			for (int i = 0; i < m_pDrawObj->m_nPoints -1; i++)
			{
				int xx = (m_pDrawObj->m_points[i+1].x - m_pDrawObj->m_points[i].x)
						*(m_pDrawObj->m_points[i+1].x - m_pDrawObj->m_points[i].x);
				int yy = (m_pDrawObj->m_points[i+1].y - m_pDrawObj->m_points[i].y)
						*(m_pDrawObj->m_points[i+1].y - m_pDrawObj->m_points[i].y);
				length += ((float)(sqrt((double)(xx+yy))));
				//TRACE("length %5.2f\n", length);
			}
			if (c_drawShape == grainPerimeter)
			{
				int n = m_pDrawObj->m_nPoints-1;
				int xx = (m_pDrawObj->m_points[0].x - m_pDrawObj->m_points[n].x)
						*(m_pDrawObj->m_points[0].x - m_pDrawObj->m_points[n].x);
				int yy = (m_pDrawObj->m_points[0].y - m_pDrawObj->m_points[n].y)
						*(m_pDrawObj->m_points[0].y - m_pDrawObj->m_points[n].y);
				length += ((float)(sqrt((double)(xx+yy))));
				//TRACE("length %5.2f\n", length);
			}
			m_pDrawObj->m_lengthFull = length / pView->app->rendering_scale;

			//if (pView->app->appliedScale != 1.0f)
			//	m_pDrawObj->m_length = m_pDrawObj->m_length * pView->app->scanningRes / pView->app->rendering_scale;

			//TRACE("length ::::::::: %5.2f\n", m_pDrawObj->m_length);

			}
			break;

		}

		{//test
			for (int i=0; i<m_pDrawObj->m_nPoints; i++)
				TRACE("%d (%d %d)\n", i, m_pDrawObj->m_points[i].x, m_pDrawObj->m_points[i].y);
		}

		pView->InvalObj(m_pDrawObj);

		//if (pView->app->appliedScale == 1.0) m_pDrawObj->unit = "pixel2" ;
		//else 	m_pDrawObj->unit = "��m2";

		//if (pView->m_pMeasureTable != NULL)  // MJY_042109
		//	//pView->m_pMeasureTable->OnBnClickedRefresh();
		//	pView->OnMeasurementTable();
	}

	m_pDrawObj = NULL;
	c_drawShape = selection;
}

void CPolyTool::OnCancel()
{
	CDrawTool::OnCancel();

	m_pDrawObj = NULL;
}

/////////////////////////////////////////////////////////////////////////////
