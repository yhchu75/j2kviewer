#pragma once

#include "resource.h"
#include "kdu_show.h"
#include "drawobjmgr.h"

// CMeasureTable dialog
#define MT_REFRESH	WM_APP

#define NUM_MTABLE_COL 4
#define	NUM_MTABLE_ROW	100

static _TCHAR *ColumnLabel[NUM_MTABLE_COL] =
{
	_T("No"), _T("Type"), _T("Length"), _T("Unit")
};

static int ColumnFmt[NUM_MTABLE_COL] =
{
	LVCFMT_CENTER, LVCFMT_CENTER, LVCFMT_CENTER
};

static int ColumnWidth[NUM_MTABLE_COL] =
{
	30, 80, 120, 50
};

//class CXLEzAutomation;

class CMeasureTable : public CDialog
{
	DECLARE_DYNAMIC(CMeasureTable)

public:
	CMeasureTable(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMeasureTable();
	
// Dialog Data
	enum { IDD = IDD_MEASURE_TABLE };
	
	CDrawObjMgr *m_pDrawObjMgr;
	CKdu_showApp *m_app;
	void	SetApp(CKdu_showApp* app, CDrawObjMgr* pDrawObjMgr){
		m_app = app;
		m_pDrawObjMgr = pDrawObjMgr;
	}

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();


	DECLARE_MESSAGE_MAP()

public:
	CListCtrl	m_listCtrl;
	wchar_t		itemText[NUM_MTABLE_ROW][NUM_MTABLE_COL][64];
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
//	afx_msg void OnNMClickMlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedExport();
	afx_msg void OnBnClickedRefresh();
	afx_msg void OnBnClickedSavemeasure();
	afx_msg void OnNMClickMlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkMlist(NMHDR *pNMHDR, LRESULT *pResult);
};
