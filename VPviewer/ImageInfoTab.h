#pragma once

// CImageInfoTab dialog

class CImageInfoTab : public CDialog
{
	DECLARE_DYNAMIC(CImageInfoTab)

public:
	CImageInfoTab(CWnd* pParent = NULL);   // standard constructor
	virtual ~CImageInfoTab();

// Dialog Data
	enum { IDD = IDD_IMAGEINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	HACCEL		m_hAccel;

public:
	CKdu_showApp* app;
	CStatic		m_staticRes, m_staticWell, m_staticDepth, m_staticComments, m_staticResolution;
	CString		scanningRes, well, depth, comments, resolution ;

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
