// drawobjmgr.h : interface of the CDrawDoc class
//
#pragma once

#include "drawobj.h"

// RGB MEASUREMENT
// ============================================================================
#include "RgbDialog.h"
// ============================================================================

class IDrawObjNotifyReceive
{
public : 
	virtual void	UpdateViewContent() = 0;
	virtual void	UpdateROIObj() = 0;
	virtual void	UpdateMeasureObj() = 0;
};

class CDrawObjMgr
{
public: // create from serialization only
	CDrawObjMgr();
	~CDrawObjMgr();

	typedef enum{ NONE,NEW_GRID , NEW_SCALE, NEW_ROI, NEW_LABEL, NEW_MEASURE_LINE, NEW_MEASURE_CURVE, NEW_MEASURE_AREA, SELECT, GRID_ANALYZE, SECTOR_ANALYZE,
		SELECT_SECTOR, NEW_SECTOR_RECT_REGION, NEW_SECTOR_ELLIPSE_REGION, NEW_SECTOR_FREE_REGION }eSTATUS;

	void	SetNotifyReceiver(IDrawObjNotifyReceive *pNotifyReceive){ m_pNotifyReceiver = pNotifyReceive; }

	void	AddObj(eOBJTYPE type, ePENCOLOR color, std::string description);
	void	SetActiveObj(CDrawObj *pActiveObj);
	eSTATUS	GetStatus(){ return m_curStatus; }

	void	DrawObjs(CDC *pDC);

	void	UpdateViewImageNCanvase(SIZE image_size, RECT rcCanvas, eRotateStatus eRotate, float rendering_scale, RECT rcFrame);
	void	SetPixelBackBuf(CFastPixel* pFastPixelBackBuf){ CDrawObj::setFastPixelBackBuf(pFastPixelBackBuf); }

	void	UpdateAppliedScale(float applied_scale);
	
	void	ToggleShowScaleBarObj(){ m_bShowScaleBarObj = !m_bShowScaleBarObj; NotifyUpdateViewContent(); }
	void	ToggleShowROIObj(){ m_bShowROIObj = !m_bShowROIObj; NotifyUpdateViewContent(); }
	void	ToggleShowMeasureObj(){ m_bShowMeasureObj = !m_bShowMeasureObj; NotifyUpdateViewContent(); }
	void	ToggleShowLabelObj(){ m_bShowLabelObj = !m_bShowLabelObj; NotifyUpdateViewContent(); }
	void	ToggleShowGridObj() { m_bShowGridObj = !m_bShowGridObj; NotifyUpdateViewContent(); }

	void	ToggleAutoUnit(){ m_bAutoUnit = !m_bAutoUnit; NotifyUpdateViewContent(); }
		
	bool	IsShowScaleBarObj(){ return m_bShowScaleBarObj; }
	bool	IsShowROIObj(){ return m_bShowROIObj; }
	bool	IsShowMeasureObj(){ return m_bShowMeasureObj; }
	bool	IsShowLabelObj(){ return m_bShowLabelObj; }
	bool	IsShowGridObj(){ return m_bShowGridObj; }
	
	bool	IsAutoUnit(){ return m_bAutoUnit; }

	void	ResetSelectMode(){ m_curStatus = NONE; }
	CDrawObj *GetSelectObj(){ return m_pActiveObj; }
	bool	AddObj(CDrawObj* obj);
	//bool	DeleteObj(CDrawObj *pObj = NULL);
	bool	DeleteObj();
	void	Clear();
	
	bool	DeleteAll();

	bool	OnLButtonDown(UINT nFlags, CPoint point);
	bool	OnLButtonUp(UINT nFlags, CPoint point);
	bool	OnMouseMove(UINT nFlags, CPoint point);
	bool	OnRButtonDown(UINT nFlags, CPoint point);
	void	SetLabelProperty();
	
	std::vector<CScaleBarObj*>& GetScaleBarObjList(){return	m_ScaleBarList;}
	std::vector<CROIObj*>& GetROIObjList(){ return m_ROIList; }
	std::vector<CMeasureObj*>& GetMeasureObjList(){ return	m_MeasureList; }
	std::vector<CLabelObj*>& GetLableList(){ return	m_LabelList; }
	std::vector<CGridObj*>& GetGridObjList(){ return	m_GridList; }


	CDrawObj *m_pActiveObj;

	void	SetCurStatus(eSTATUS status) { m_curStatus = status; }

	// RGB MEASUREMENT
	// ============================================================================
	CRgbDialog *m_pRgbDialog;
	void RGBHistogram(CDrawObj *draw_obj);
	// ============================================================================

	RECT GetFrameRect()
	{
		return m_FrameRect;
	}

	void	ShowGridObj(bool bShowGridObj);
private:

	bool	m_bShowScaleBarObj; 
	bool	m_bShowROIObj;
	bool	m_bShowMeasureObj;
	bool	m_bShowLabelObj;
	bool	m_bShowGridObj;

	bool	m_bAutoUnit;
	
	RECT	m_FrameRect;	//실제 보여 주는 windows(ChildWnd) 내에서 Image가 보여주는 영역, 마우스 좌표를 Canvas좌표로 변환하는데 사용 
	eSTATUS	m_curStatus;
	std::string m_descripton;
	ePENCOLOR  m_selectPenColor;

	std::vector<CScaleBarObj*>		m_ScaleBarList;
	std::vector<CROIObj*>			m_ROIList;
	std::vector<CMeasureObj*>		m_MeasureList;
	std::vector<CLabelObj*>			m_LabelList;
	std::vector<CGridObj*>			m_GridList;
	
	bool	ConvertCanvasPoint(POINT &in, POINT &out);

	IDrawObjNotifyReceive*	m_pNotifyReceiver;

	void	NotifyUpdateViewContent(){ if (m_pNotifyReceiver) m_pNotifyReceiver->UpdateViewContent(); }
};

/////////////////////////////////////////////////////////////////////////////
