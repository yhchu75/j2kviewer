/******************************************************************************/
// File: MainFrm.cpp [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/*****************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/*******************************************************************************
Description:
   Implementation of a small number of frame window functions for the
interactive JPEG2000 image viewer application, "kdu_show".  Client-area windows
messages are processed by the child view object, implemented in
 "ChildView.cpp".
*******************************************************************************/

#include "stdafx.h"
#include "kdu_show.h"

#include "MainFrm.h"
#include ".\mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame


CString GetModulePath()
{
CString strPath;

	TCHAR szpath[1024], szdrive[64], szdir[512];
	::GetModuleFileName(NULL, szpath, 255);
	_tsplitpath(szpath, szdrive, szdir, NULL, NULL);
	strPath.Format(_T("%s%s"), szdrive, szdir);

	return strPath;
}


IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_DROPFILES()
	ON_WM_TIMER()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
	ON_WM_MOVING()
	ON_COMMAND_RANGE(ID_TOOLPLUGINBASEINDEX, ID_TOOLPLUGINBASEINDEX + 100, OnMenuCommand)


	// TOOLBAR
	// ============================================================================
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY(TBN_QUERYINSERT, AFX_IDW_TOOLBAR, QueryInsert)
	ON_NOTIFY(TBN_QUERYDELETE, AFX_IDW_TOOLBAR, QueryDelete)
	ON_NOTIFY(TBN_GETBUTTONINFO, AFX_IDW_TOOLBAR, QueryInfo)
	ON_COMMAND(ID_VIEW_TOOLBAR, &CMainFrame::OnShowToolBar)
	ON_WM_CONTEXTMENU()
	ON_WM_NCRBUTTONUP()
	ON_COMMAND(ID_TOOLBAR_SHOWTOOLBAR, &CMainFrame::OnToolbarShowtoolbar)
	ON_COMMAND(ID_TOOLBAR_CUSTOMIZETOOLBAR, &CMainFrame::OnToolbarCustomizetoolbar)
	ON_WM_NCPAINT()
	// ============================================================================

	// RGB MEASUREMENT
	// ============================================================================
	ON_COMMAND(ID_MEASUREMENT_RGB, &CMainFrame::OnMeasurementRgb)
	// ============================================================================

//ON_WM_WINDOWPOSCHANGED()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	// ID_INDICATOR_CAPS,
	// ID_INDICATOR_NUM,
	// ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

/******************************************************************************/
/*                          CMainFrame::CMainFrame                            */
/******************************************************************************/

CMainFrame::CMainFrame()
{
  app = NULL;
  memset(m_TBinfo, 0, sizeof(m_TBinfo));
}

/******************************************************************************/
/*                          CMainFrame::~CMainFrame                           */
/******************************************************************************/

CMainFrame::~CMainFrame()
{
}


// TOOLBAR
// ============================================================================
void CMainFrame::SetClientSize()
{
	CRect window_rect;
	int width_fix = 10, height_fix = 2;
	m_wndView.GetClientRect(window_rect);

	if (m_wndView.GetSafeHwnd())
	{
		if (m_bUseToolbar == TRUE && m_wndToolBar.IsVisible() == TRUE)
		{
			m_wndView.MoveWindow(CRect(window_rect.left, window_rect.top + TOOLBAR_BUTTON_HEIGHT, window_rect.right + width_fix, window_rect.bottom + TOOLBAR_BUTTON_HEIGHT + height_fix * 2));
			//m_wndView.MoveWindow(CRect(window_rect.left, window_rect.top + TOOLBAR_BUTTON_HEIGHT, window_rect.right + width_fix, status_rect.top));
		}
		else
		{
			m_wndView.MoveWindow(CRect(window_rect.left, window_rect.top - height_fix, window_rect.right + width_fix, window_rect.bottom + height_fix * 2));
		}

		Invalidate();
	}
}

void CMainFrame::OnShowToolBar()
{
	if (m_bUseToolbar == FALSE)
		return;

	ShowControlBar(&m_wndToolBar, !m_wndToolBar.IsVisible(), FALSE);
	m_wndView.check_and_report_size();
}

void CMainFrame::QueryInsert(NMHDR * pNotifyStruct, LRESULT * result)
{
	*result = TRUE;
}

void CMainFrame::QueryDelete(NMHDR * pNotifyStruct, LRESULT * result)
{
	*result = TRUE;
}

void CMainFrame::GetToolbarButtonInfo()
{
	CToolBarCtrl& myTBCtrl = m_wndToolBar.GetToolBarCtrl();
	CString toolbar_text = L"";

	m_iTBCount = myTBCtrl.GetButtonCount();

	for (int i = 0; i < m_arrToolbarText.GetCount(); i++)
	{
		toolbar_text += m_arrToolbarText.GetAt(i);
		toolbar_text += '\0';
	}

	myTBCtrl.AddStrings(toolbar_text);

	for (int i = 0; i <= m_iTBCount; i++)
	{
		myTBCtrl.GetButton(i, &m_TBinfo[i]);
		m_TBinfo[i].iString = m_TBinfo[i].iBitmap;
	}
}

void CMainFrame::QueryInfo(NMHDR * pNotifyStruct, LRESULT * result)
{
	TBNOTIFY* pTBntf = (TBNOTIFY *)pNotifyStruct;

	if ((pTBntf->iItem >= 0) && (pTBntf->iItem < m_iTBCount))
	{
		pTBntf->tbButton = m_TBinfo[pTBntf->iItem];
		*result = TRUE;
	}
	else
	{
		*result = FALSE;
	}
}

void CMainFrame::SaveLoadToolBar(BOOL bSave)
{
	if (m_bUseToolbar == FALSE)
		return;

	CString myToolBarRegStr = "ToolBar";
	CString mysubkey = "SOFTWARE\\";
	CWinApp* myapp = AfxGetApp();
	CToolBarCtrl & myToolBarCtrl = m_wndToolBar.GetToolBarCtrl();

	mysubkey = mysubkey + myapp->m_pszRegistryKey + "\\" + myapp->m_pszAppName + "\\" + "Settings";

	int show_toolbar = 0;
	int toolbar_ver = 0;

	if (bSave)
	{
		show_toolbar = m_wndToolBar.IsVisible();
		toolbar_ver = TOOLBAR_VER;
		myToolBarCtrl.SaveState(HKEY_CURRENT_USER, (LPCTSTR)mysubkey, (LPCTSTR)myToolBarRegStr);
		myapp->WriteProfileInt(L"Settings", L"SWToolBar", show_toolbar);
		myapp->WriteProfileInt(L"Settings", L"ToolBarVer", toolbar_ver);
	}
	else
	{
		show_toolbar = AfxGetApp()->GetProfileIntW(L"Settings", L"SWToolBar", 1);
		toolbar_ver = AfxGetApp()->GetProfileIntW(L"Settings", L"ToolBarVer", 0);

		if (toolbar_ver == TOOLBAR_VER)
		{
			myToolBarCtrl.RestoreState(HKEY_CURRENT_USER, (LPCTSTR)mysubkey, (LPCTSTR)myToolBarRegStr);
			myToolBarCtrl.SetButtonWidth(TOOLBAR_BUTTON_WIDTH, TOOLBAR_BUTTON_WIDTH);
			m_wndToolBar.SetSizes(CSize(TOOLBAR_BUTTON_WIDTH, TOOLBAR_BUTTON_HEIGHT), CSize(TOOLBAR_ICON_WIDTH, TOOLBAR_ICON_HEIGHT));
		}

		if (show_toolbar == 0)
			ShowControlBar(&m_wndToolBar, FALSE, FALSE);

		m_wndView.check_and_report_size();
	}
}
// ============================================================================


/******************************************************************************/
/*                           CMainFrame::OnCreate                             */
/******************************************************************************/

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;

  CString path = GetModulePath();
  path += _T("Tools");

  //create meun if tool plugin exist
  if (m_ToolPlugInsMgr.Load(path, &m_VPViewerCore)){
	  m_ToolPlugInsMgr.InsertMenu(GetMenu(), ID_TOOLPLUGINBASEINDEX);
  }
  EnablePlugInsMenu(false);
  
   // create a view to occupy the client area of the frame
  if (!m_wndView.Create(NULL, NULL,
                        AFX_WS_DEFAULT_VIEW,
                        //AFX_WS_DEFAULT_VIEW | WS_HSCROLL | WS_VSCROLL, // MJY
                        CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
    {
      TRACE0("Failed to create view window\n");
      return -1;
    }


	// TOOLBAR
	// ============================================================================
	if (m_bUseToolbar == TRUE)
	{
		// create a toolbar
		//toolbar_scale_x2 = false;
		//if ((!m_wndToolBar.Create(this, CBRS_TOP | WS_VISIBLE)) ||
		////if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_VISIBLE | CBRS_TOP) ||
			(!m_wndToolBar.LoadToolBar(IDR_TOOLBAR1)))
		{
			TRACE0("Failed to create toolbar\n");
			return -1;      // fail to create
		}

		m_wndToolBar.ModifyStyle(0, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT);

		m_arrToolbarText.Add(L"Open Local Image");
		m_arrToolbarText.Add(L"Open Remote Image");
		m_arrToolbarText.Add(L"Close Image");
		m_arrToolbarText.Add(L"Load Configuration");
		m_arrToolbarText.Add(L"Save Configuration As");
		m_arrToolbarText.Add(L"Copy View To Clipboard");
		m_arrToolbarText.Add(L"Export View as JPEG");
		m_arrToolbarText.Add(L"Export Measurements as SVG(vector)");
		m_arrToolbarText.Add(L"Export Image in 1:1 ratio");
		m_arrToolbarText.Add(L"Zoom In");
		m_arrToolbarText.Add(L"Zoom Out");
		m_arrToolbarText.Add(L"Fit to Screen");
		m_arrToolbarText.Add(L"Overview Window");
		m_arrToolbarText.Add(L"Attach Overview");
		m_arrToolbarText.Add(L"Reset Overview Position");
		m_arrToolbarText.Add(L"Edit Metadata");
		m_arrToolbarText.Add(L"Scaling");
		m_arrToolbarText.Add(L"Automatic Unit Conversion");
		m_arrToolbarText.Add(L"Show Measurement");
		m_arrToolbarText.Add(L"Delete All Measurement");
		m_arrToolbarText.Add(L"Show RGB Measurement");
		m_arrToolbarText.Add(L"Show Measurement Table");
		m_arrToolbarText.Add(L"Show RGB Table");
		m_arrToolbarText.Add(L"Select");
		m_arrToolbarText.Add(L"Line");
		m_arrToolbarText.Add(L"Curve");
		m_arrToolbarText.Add(L"Area");
		m_arrToolbarText.Add(L"Scale bar");
		m_arrToolbarText.Add(L"Add Label");
		m_arrToolbarText.Add(L"Show Label");
		m_arrToolbarText.Add(L"Show Grid");
		m_arrToolbarText.Add(L"Grid Property");
		m_arrToolbarText.Add(L"About VP Viewer");
		m_arrToolbarText.Add(L"Register as default viewer for JP2 & JPX");

		GetToolbarButtonInfo();
		SaveLoadToolBar(FALSE);

		m_wndToolBar.SetBarStyle(CBRS_ALIGN_TOP | CBRS_TOOLTIPS | CBRS_FLYBY);
		RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);

		/*CRect toolbar_rect;
		m_wndToolBar.GetItemRect(0, &toolbar_rect);
		m_wndToolBar.SetSizes(CSize(toolbar_rect.Width(), toolbar_rect.Height()), CSize(16, 15));*/

		/*CImageList img;
		img.Create(IDB_TOOLBAR_TEST_1, 24, 0, RGB(255, 0, 255));
		m_wndToolBar.GetToolBarCtrl().SetHotImageList(&img);
		img.Detach();
		img.Create(IDB_TOOLBAR_TEST_1, 24, 0, RGB(255, 0, 255));
		m_wndToolBar.GetToolBarCtrl().SetImageList(&img);
		img.Detach();*/

		/*CImageList img;
		img.Create(IDB_TOOLBAR_TEST_2, 24, 0, RGB(245, 246, 247));
		m_wndToolBar.GetToolBarCtrl().SetImageList(&img);
		img.Detach();*/

		//CImageList *pImgList = m_wndToolBar.GetToolBarCtrl().GetImageList();
		//pImgList->DeleteImageList();
		CImageList *pImgList = new CImageList;
		pImgList->Create(TOOLBAR_ICON_WIDTH, TOOLBAR_ICON_HEIGHT, ILC_COLOR8 | ILC_MASK, 0, 10);

		CBitmap ToolBarBm;
		ToolBarBm.LoadBitmap(IDB_TOOLBAR_TEST_5);
		pImgList->Add(&ToolBarBm, RGB(245, 246, 247));

		m_wndToolBar.GetToolBarCtrl().SetImageList(pImgList);
	}
	// ============================================================================

	// RGB MEASUREMENT
	// ============================================================================
	int show_rgb = AfxGetApp()->GetProfileIntW(L"Settings", L"SWRGB", MF_CHECKED);
	GetMenu()->CheckMenuItem(ID_MEASUREMENT_RGB, (show_rgb == MF_CHECKED ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);
	get_child()->m_nShowRGB = show_rgb > 0 ? 1 : 0;
	// ============================================================================


  // create a status bar
  HICON m_hIcon;
  m_hIcon = ::LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
  SetIcon(m_hIcon, FALSE);

  if (!m_wndStatusBar.Create(this) ||
      !m_wndStatusBar.SetIndicators(indicators,
      sizeof(indicators)/sizeof(UINT)))
    {
      TRACE0("Failed to create status bar\n");
      return -1;      // fail to create
    }


  /*m_wndToolBar.EnableDocking(CBRS_ALIGN_TOP);
  m_wndToolBar2.EnableDocking(CBRS_ALIGN_TOP);
  EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(&m_wndToolBar);
  DockControlBar(&m_wndToolBar2);*/

  return 0;
}

/******************************************************************************/
/*                           CMainFrame::OnClose                              */
/******************************************************************************/

void CMainFrame::OnClose()
{
	CRect r;
	GetWindowRect(&r);
	CWinApp* pApp = AfxGetApp();
	pApp->WriteProfileInt(L"Settings", L"Left", r.left);
	pApp->WriteProfileInt(L"Settings", L"Top", r.top);
	pApp->WriteProfileInt(L"Settings", L"Width", r.Width());
	pApp->WriteProfileInt(L"Settings", L"Height", r.Height());
	pApp->WriteProfileInt(L"Settings", L"Attach", m_attach_overview);
	CFrameWnd::OnClose();
}

/******************************************************************************/
/*                        CMainFrame::PreCreateWindow                         */
/******************************************************************************/

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	
	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE
		| WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_SYSMENU;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	
	if (!CFrameWnd::PreCreateWindow(cs))
		return FALSE;

	CWinApp* pApp = AfxGetApp();
	cs.x = pApp->GetProfileInt(L"Settings", L"Left", 0);
	cs.y = pApp->GetProfileInt(L"Settings", L"Top", 0);
	cs.cx = pApp->GetProfileInt(L"Settings", L"Width", 800);
	cs.cy = pApp->GetProfileInt(L"Settings", L"Height", 600);

	m_attach_overview = pApp->GetProfileInt(L"Settings", L"Attach", 1);

	//check position
	int screen_x = GetSystemMetrics(SM_CXFULLSCREEN);
	if (cs.x > screen_x || cs.x < 0) cs.x = 0;
	if (cs.cx < 0) cs.cx = 800;
	if ((cs.cx + cs.x) > screen_x) cs.cx = screen_x - cs.x;

	int screen_y = GetSystemMetrics(SM_CYFULLSCREEN);
	if (cs.y > screen_y || cs.y < 0 ) cs.y = 0;
	if (cs.cy < 0) cs.cy = 600;
	if ((cs.cy + cs.y) > screen_y) cs.cy = screen_y - cs.y;

	//cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
	//return CFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
  CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
  CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

/******************************************************************************/
/*                           CMainFrame::OnSetFocus                           */
/******************************************************************************/

void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
  m_wndView.SetFocus();
}

/******************************************************************************/
/*                            CMainFrame::OnCmdMsg                            */
/******************************************************************************/

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra,
                          AFX_CMDHANDLERINFO* pHandlerInfo)
{
  // let the view have first crack at the command
  if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;

  // otherwise, do default handling
  return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

/******************************************************************************/
/*                             CMainFrame::OnSize                             */
/******************************************************************************/

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
  CFrameWnd::OnSize(nType, cx, cy); // Let the framework resize the client first

  //if (nType == SIZE_RESTORED) // MJY
  //	m_wndView.check_and_report_size();

  // MJY_102308 : 
  if (app != NULL)
  {
	//Recalurate client size with toolbar before calcualte view client region
	SetClientSize();

	m_wndView.check_and_report_size();
	m_wndView.Invalidate();
	if (app->m_pOverview != NULL) app->m_pOverview->Invalidate();

	if (app->m_pOverview != NULL){
		if (m_attach_overview)
		{
			CRect r;
			CRect mainR;
			CPoint pos = app->m_pOverview->m_position;
			app->m_pOverview->GetWindowRect(r);
			GetWindowRect(mainR);
			app->m_pOverview->MoveWindow(mainR.left + pos.x, mainR.top + pos.y, r.Width(), r.Height());
		}
		else
		{
			CRect mainR;
			CRect overviewR;
			GetWindowRect(mainR);
			app->m_pOverview->GetWindowRect(overviewR);

			app->m_pOverview->m_position.x = overviewR.left - mainR.left;
			app->m_pOverview->m_position.y = overviewR.top - mainR.top;
		}
	}
  }

  

}

/******************************************************************************/
/*                           CMainFrame::OnDropFiles                          */
/******************************************************************************/

void CMainFrame::OnDropFiles(HDROP hDropInfo) 
{
  TCHAR fname[1024];
  DragQueryFile(hDropInfo,0,fname,1023);
  DragFinish(hDropInfo);
  if (app != NULL)
    app->open_file(fname);
}

/******************************************************************************/
/*                             CMainFrame::OnTimer                            */
/******************************************************************************/
void CMainFrame::OnTimer(UINT_PTR nIDEvent) 
{
	if (nIDEvent == KDU_SHOW_REFRESH_TIMER_IDENT)
		app->refresh_display();
	else if (nIDEvent == KDU_SHOW_FLASH_TIMER_IDENT)
		app->flash_overlays();
	else if (nIDEvent == KDU_SHOW_REQUEST_REFRESH_TIMER_IDENT){
		app->initialize_regions(TRUE);
		KillTimer(nIDEvent);
	}
}

/******************************************************************************/
/*                             CMainFrame::OnMoving                           */
/******************************************************************************/

void CMainFrame::OnMoving(UINT nSide, LPRECT lpRect)
{
	if (app->m_pOverview != NULL){
		if (m_attach_overview)
		{
			CRect r;
			CPoint pos = app->m_pOverview->m_position;
			app->m_pOverview->GetWindowRect(r);

			app->m_pOverview->MoveWindow(lpRect->left + pos.x, lpRect->top + pos.y, r.Width(), r.Height());
		}
		else
		{
			CRect mainR;
			CRect overviewR;
			GetWindowRect(mainR);
			app->m_pOverview->GetWindowRect(overviewR);

			app->m_pOverview->m_position.x = overviewR.left - mainR.left;
			app->m_pOverview->m_position.y = overviewR.top - mainR.top;
		}
	}

}


void CMainFrame::OnMenuCommand(UINT nID)
{
	m_ToolPlugInsMgr.OnMenuCommand(nID - ID_TOOLPLUGINBASEINDEX);
}

void CMainFrame::EnablePlugInsMenu(bool enable)
{
	m_ToolPlugInsMgr.EnableMenu(GetMenu(),ID_TOOLPLUGINBASEINDEX, enable);
	DrawMenuBar();
}


// TOOLBAR
// ============================================================================
int CMainFrame::PopupToolbarContext()
{
	if (m_pToolbarContext == NULL)
	{
		m_pToolbarContext = new CMenu;
		m_pToolbarContext->LoadMenuW(IDR_CONTEXT_TOOLBAR);
	}

	CMenu *sub_menu = m_pToolbarContext->GetSubMenu(0);
	CPoint cur_pos;
	GetCursorPos(&cur_pos);

	sub_menu->CheckMenuItem(ID_TOOLBAR_SHOWTOOLBAR, (m_wndToolBar.IsVisible() == TRUE ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);

	return sub_menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, cur_pos.x, cur_pos.y, this);
}

BOOL CMainFrame::OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
	CWnd* pWnd = NULL;

	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);

	pWnd = GetRoutingFrame();
	if ((pWnd != NULL) && (pWnd->GetSafeHwnd() != m_hWnd))
		return FALSE;

	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	TCHAR szFullText[512] = L"";
	CString strTipText;
	UINT nID = pNMHDR->idFrom;

	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
		pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
		nID = ::GetDlgCtrlID((HWND)nID);

	if (nID != 0)
	{
		int string_index = 0;

		switch (nID)
		{
		case ID_FILE_OPEN: string_index = 0; break;
		case ID_JPIP_OPEN: string_index = 1; break;
		case ID_FILE_CLOSE: string_index = 2; break;
		case ID_FILE_LOAD_CONF: string_index = 3; break;
		case ID_FILE_SAVE_CONF_AS: string_index = 4; break;
		case ID_EDIT_COPYVIEWTOCLIPBOARD: string_index = 5; break;
		case ID_EDIT_EXPORTASJPEG: string_index = 6; break;
		case ID_EXPORT_MEASUREMENT_AS_SVG: string_index = 7; break;
		case ID_EDIT_CROPIMAGE: string_index = 8; break;
		case ID_VIEW_ZOOM_IN: string_index = 9; break;
		case ID_VIEW_ZOOM_OUT: string_index = 10; break;
		case ID_SCREEN_FIT: string_index = 11; break;
		case ID_TOOL_OVERVIEWWINDOW: string_index = 12; break;
		case ID_TOOLS_ATTACHOVERVIEW: string_index = 13; break;
		case ID_TOOLS_RESET: string_index = 14; break;
		case ID_TOOL_EDITMETADATA: string_index = 15; break;
		case ID_MEASUREMENT_SCALING: string_index = 16; break;
		case ID_AUTOUNIT: string_index = 17; break;
		case ID_MEASUREMENT_SHOW: string_index = 18; break;
		case ID_DELETE_ALL_MEASUREMENT: string_index = 19; break;
		case ID_MEASUREMENT_RGB: string_index = 20; break;
		case ID_MEASUREMENT_TABLE: string_index = 21; break;
		case ID_MEASUREMENT_RGB_TABLE: string_index = 22; break;
		case ID_MEASUREMENT_SELECT: string_index = 23; break;
		case ID_MEASUREMENT_LINE: string_index = 24; break;
		case ID_MEASUREMENT_CURVE: string_index = 25; break;
		case ID_MEASUREMENT_AREA: string_index = 26; break;
		case ID_MEASUREMENT_SCALEBAR: string_index = 27; break;
		case ID_LABEL_ADDLABEL: string_index = 28; break;
		case ID_LABEL_SHOWLABEL: string_index = 29; break;
		case ID_GRID_SHOW: string_index = 30; break;
		case ID_GRID_PROPERTY: string_index = 31; break;
		case ID_APP_ABOUT: string_index = 32; break;
		case ID_REGISTERFILEEXTENSION: string_index = 33; break;
		}

		CString toolbar_text = m_arrToolbarText.GetAt(string_index);

		//AfxLoadString(nID, szFullText);
		lstrcpy(szFullText, toolbar_text);
		strTipText = szFullText;

#ifndef _UNICODE
		if (pNMHDR->code == TTN_NEEDTEXTA)
		{
			lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
		}
		else
		{
			_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
		}
#else
		if (pNMHDR->code == TTN_NEEDTEXTA)
		{
			_wcstombsz(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
		}
		else
		{
			lstrcpyn(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
		}
#endif

		*pResult = 0;

		::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE | SWP_NOOWNERZORDER);

		return TRUE;
	}

	return FALSE;
}


BOOL CMainFrame::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	// TODO: Add your specialized code here and/or call the base class
	/*LPNMHDR pnmh = (LPNMHDR)lParam;
	if (pnmh->hwndFrom == m_wndToolBar.m_hWnd)
	{
		LPNMTBCUSTOMDRAW lpNMCustomDraw = (LPNMTBCUSTOMDRAW)lParam;
		CRect rect;
		m_wndToolBar.GetClientRect(rect);

		HBRUSH hBrush = CreateSolidBrush(RGB(245, 246, 247));
		//HBRUSH hBrush = CreateSolidBrush(RGB(220, 220, 220));
		FillRect(lpNMCustomDraw->nmcd.hdc, rect, hBrush);

		CRect line_rect;
		line_rect.left = rect.left;
		line_rect.right = rect.right;
		line_rect.top = rect.bottom - 2;
		line_rect.bottom = rect.bottom - 1;
		//line_rect.top = rect.bottom + 1;
		//line_rect.bottom = rect.bottom + 2;

		HBRUSH hBrush1 = CreateSolidBrush(RGB(160, 160, 160));
		FillRect(lpNMCustomDraw->nmcd.hdc, line_rect, hBrush1);

		CRect line_rect2;
		line_rect2.left = rect.left;
		line_rect2.right = rect.right;
		line_rect2.top = rect.bottom - 1;
		line_rect2.bottom = rect.bottom;
		//line_rect2.top = rect.bottom + 2;
		//line_rect2.bottom = rect.bottom + 3;

		HBRUSH hBrush2 = CreateSolidBrush(RGB(105, 105, 105));
		FillRect(lpNMCustomDraw->nmcd.hdc, line_rect2, hBrush2);
	}*/

	return CFrameWnd::OnNotify(wParam, lParam, pResult);
}


BOOL CMainFrame::DestroyWindow()
{
	// TODO: Add your specialized code here and/or call the base class
	SaveLoadToolBar(TRUE);

	return CFrameWnd::DestroyWindow();
}


void CMainFrame::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	// TODO: Add your message handler code here
	if (m_bUseToolbar != TRUE)
		return;

	CRect rect2;
	m_wndView.GetWindowRect(rect2);
	CRect rect3;
	m_wndToolBar.GetWindowRect(rect3);
	
	if (point.y < rect3.bottom && point.y > rect3.top)
		PopupToolbarContext();
	else if (point.y < rect2.bottom && point.y > rect2.top)
		m_wndView.OnRButtonDown(0, point);
}


void CMainFrame::OnNcRButtonUp(UINT nHitTest, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (m_bUseToolbar == TRUE && nHitTest == HTMENU)
		PopupToolbarContext();

	CFrameWnd::OnNcRButtonUp(nHitTest, point);
}


void CMainFrame::OnToolbarShowtoolbar()
{
	// TODO: Add your command handler code here
	if (m_pToolbarContext == NULL)
		return;

	CMenu *sub_menu = m_pToolbarContext->GetSubMenu(0);
	sub_menu->CheckMenuItem(ID_TOOLBAR_SHOWTOOLBAR, (m_wndToolBar.IsVisible() == TRUE ? MF_UNCHECKED : MF_CHECKED) | MF_BYCOMMAND);

	OnShowToolBar();
}


void CMainFrame::OnToolbarCustomizetoolbar()
{
	// TODO: Add your command handler code here
	if (m_bUseToolbar != TRUE)
		return;

	CToolBarCtrl& myTBCtrl = m_wndToolBar.GetToolBarCtrl();
	myTBCtrl.Customize();
}
// ============================================================================


// RGB MEASUREMENT
// ============================================================================
void CMainFrame::OnMeasurementRgb()
{
	get_child()->OnMeasurementRgb();
	/*// TODO: Add your command handler code here
	CWinApp* myapp = AfxGetApp();
	CMenu *main_menu = GetMenu();

	int show_rgb = main_menu->GetMenuState(ID_MEASUREMENT_RGB, MF_BYCOMMAND);
	main_menu->CheckMenuItem(ID_MEASUREMENT_RGB, (show_rgb == MF_CHECKED ? MF_UNCHECKED : MF_CHECKED) | MF_BYCOMMAND);
	show_rgb = main_menu->GetMenuState(ID_MEASUREMENT_RGB, MF_BYCOMMAND);
	myapp->WriteProfileInt(L"Settings", L"SWRGB", show_rgb);

	std::vector<CMeasureObj*> measure_list = app->child_wnd->m_pDrawObjMgr->GetMeasureObjList();

	for (int i = 0; i < measure_list.size(); i++)
		measure_list[i]->Draw(&app->child_wnd->m_BackbufferDC, false);

	app->child_wnd->UpdateViewRegion();*/
}
// ============================================================================


void CMainFrame::OnNcPaint()
{
	// TODO: Add your message handler code here
	// Do not call CFrameWnd::OnNcPaint() for painting messages
	m_wndToolBar.SetSizes(CSize(TOOLBAR_BUTTON_WIDTH, TOOLBAR_BUTTON_HEIGHT), CSize(TOOLBAR_ICON_WIDTH, TOOLBAR_ICON_HEIGHT));

	CFrameWnd::OnNcPaint();
}
