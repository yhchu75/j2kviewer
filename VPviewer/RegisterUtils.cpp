#include <stdafx.h>

BOOL	RegisterFileExtension(const char* executable_path, const char *file_extension)
{
	HKEY hkey;
	
	std::string app = executable_path;
	std::string extension = file_extension;            // file extension
	std::string program_extension = "vpviewer" + extension;
	std::string command = "\"" + app + "\"" + " \"%1\"";
	std::string action = "Open with my program";
	std::string path = "Software\\Classes\\" + extension;
	
	// 1: Create subkey for extension -> HKEY_CURRENT_USER\Software\Classes\.jp2
	//@="vpviewer.jpx"
	if (RegCreateKeyExA(HKEY_CURRENT_USER, path.c_str(), 0, 0, 0, KEY_ALL_ACCESS, 0, &hkey, 0) != ERROR_SUCCESS)
	{
		TRACE("Can not create registry key %s \n", path.c_str());
		return FALSE;
	}
	RegSetValueExA(hkey, "", 0, REG_SZ, (BYTE *)program_extension.c_str(), program_extension.length()); // default vlaue is description of file extension
	RegCloseKey(hkey);


	path = "Software\\Classes\\" + program_extension + "\\shell\\open\\command";
	// 2: Create Subkeys for action ( "Open with my program" )
	// HKEY_CURRENT_USER\Software\Classes\vpviewer.jpx\shell\open\command
	// @="D:\work2\InnovaPlex\VPViewer\demo\VPViewer.exe" "%1"
	if (RegCreateKeyExA(HKEY_CURRENT_USER, path.c_str(), 0, 0, 0, KEY_ALL_ACCESS, 0, &hkey, 0) != ERROR_SUCCESS)
	{
		TRACE("Can not create registry key %s \n", path.c_str());
		return FALSE;
	}
	RegSetValueExA(hkey, "", 0, REG_SZ, (BYTE*)command.c_str(), command.length());
	RegCloseKey(hkey);

	return TRUE;
}

BOOL	RegisterProtocols(const char *executable_path)
{
	BOOL bResult = FALSE;
	DWORD disposition;
	HKEY root_key, icon_key, shell_key, open_key, command_key;

	const char *app_name = strrchr(executable_path, '\\');
	if (app_name == NULL)
		app_name = executable_path;
	else
		app_name++;

	const char *description = "JPIP Interactive Imaging Protocol";
	char *command_string = new char[strlen(executable_path) + 4];
	strcpy(command_string, executable_path);
	strcat(command_string, " %1");
	std::string path = "Software\\Classes\\jpip";


	do {
		if (RegCreateKeyExA(HKEY_CURRENT_USER, path.c_str(), 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL,
			&root_key, &disposition) != ERROR_SUCCESS)
			continue;
		if (RegSetValueExA(root_key, NULL, 0, REG_SZ, (BYTE *)description,
			(int)strlen(description) + 1) != ERROR_SUCCESS)
			continue;
		if (RegSetValueExA(root_key, "URL Protocol", 0, REG_SZ,
			(BYTE *)(""), 1) != ERROR_SUCCESS)
			continue;
		if (RegCreateKeyExA(root_key, "DefaultIcon", 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL,
			&icon_key, &disposition) != ERROR_SUCCESS)
			continue;
		if (RegSetValueExA(icon_key, NULL, 0, REG_SZ, (BYTE *)app_name,
			(int)strlen(app_name) + 1) != ERROR_SUCCESS)
			continue;
		if (RegCreateKeyExA(root_key, "shell", 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL,
			&shell_key, &disposition) != ERROR_SUCCESS)
			continue;
		if (RegCreateKeyExA(shell_key, "open", 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL,
			&open_key, &disposition) != ERROR_SUCCESS)
			continue;
		if (RegCreateKeyExA(open_key, "command", 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL,
			&command_key, &disposition) != ERROR_SUCCESS)
			continue;
		RegSetValueExA(command_key, NULL, 0, REG_SZ, (BYTE *)command_string,
			(int)strlen(command_string) + 1);

		bResult = TRUE;
	} while (0);

	return bResult;
}
