#include "stdafx.h"
#include "VPViewerConf.h"
#include "DrawObjMgr.h"

#include "../json/include/json.h"
#pragma comment(lib, "../json/" JSON_LIB_PATH)

#include <fstream>
#include <iostream>

void	LoadImageInfo(Json::Value& config, ImageExternalInfo *info);
void	LoadROIInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr);
void	LoadMeasureInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr);
void	LoadLabelInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr);
void	LoadGridInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr);

#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

TCHAR* GetThisPath(TCHAR* dest, DWORD destSize)
{
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);
	return dest;
}


TCHAR *g_PENColorString[MAX_PEN_TYPE] = {
	_T("Black"), _T("White"), _T("Red"), _T("Green"), _T("Blue"), _T("Yellow") };

CVPViewerConf::CVPViewerConf()
{
	m_strConfFile[0] = NULL;
	m_appliedScale = 1.0;
	m_pDrawObjMgr = NULL;
}


CVPViewerConf::~CVPViewerConf()
{
}


bool	CVPViewerConf::Open(const TCHAR *szImagePath, CDrawObjMgr *pDrawObjMgr)
{	
	Close();

	m_pDrawObjMgr = pDrawObjMgr;
	//get file name 
	int i, pos;
	for (i = (int)_tcslen(szImagePath) - 1; i > 0; i--){
		if (szImagePath[i] == '.'){
			pos = i;
			break;
		}
	}

	bool bOpenJPIPConf = false;
	if (_tcslen(szImagePath) > 4){
		TCHAR strTemp[MAX_PATH];
		for (i = 0; i < 4; i++)strTemp[i] = szImagePath[i];
		strTemp[i] = 0;

		if (_tcsicmp(strTemp, _T("jpip")) == 0){

			size_t nStrLen = _tcslen(szImagePath);
			nStrLen -= (nStrLen - pos) + 7; // remove extention and protocol prefix

			for (i = 0; i < nStrLen; i++){
				if (szImagePath[i + 7] == '.' || szImagePath[i + 7] == '/' || szImagePath[i + 7] == ':'){
					strTemp[i] = '_';
				}
				else{
					strTemp[i] = szImagePath[i + 7];
				}
			}

			strTemp[i] = NULL;

			GetThisPath(m_strConfFile, MAX_PATH);
			_tcscat(m_strConfFile, _T("\\"));
			_tcscat(m_strConfFile, strTemp);
			_tcscat(m_strConfFile, _T(".conf"));

			bOpenJPIPConf = true;
		}
	}

	if (!bOpenJPIPConf){
		_tcsncpy(m_strConfFile, szImagePath, pos);
		m_strConfFile[pos] = NULL;
		_tcscat(m_strConfFile, _T(".conf"));
	}

	LoadDefault();
	std::ifstream inFile(m_strConfFile);
	if (!inFile.is_open()){
		return false;
	}
	

	//Read config
	Json::Value config;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(inFile, config, false);
	if (parsedSuccess == false) {
		AtlTrace("can not open config file [%s] \n", m_strConfFile);
		return false;
	}
	
	LoadImageInfo(config, &m_ImageExternalInfo);
	LoadROIInfo(config, pDrawObjMgr);
	LoadMeasureInfo(config, pDrawObjMgr);
	LoadLabelInfo(config, pDrawObjMgr);
	LoadGridInfo(config, pDrawObjMgr);

	setAppliedScale(m_ImageExternalInfo.viewingScale);
	return true;
}


void	CVPViewerConf::Close()
{
	if (m_strConfFile[0]){
		SaveConf();
		m_strConfFile[0] = NULL;
	}
		
	if (m_pDrawObjMgr){
		m_pDrawObjMgr->Clear();
	}
}


void	CVPViewerConf::LoadDefault()
{
	ScaleBarInfo info;
	CScaleBarObj* obj = new CScaleBarObj(&info);
	m_pDrawObjMgr->AddObj(obj);

	GridInfo grid;
	CGridObj* gridObj = new CGridObj(&grid);
	m_pDrawObjMgr->AddObj(gridObj);
	
	m_ImageExternalInfo.scanningRes = 1.0f;	//default��
	m_ImageExternalInfo.viewingScale = 1.0f;
	m_ImageExternalInfo.brightness = 0;
	m_ImageExternalInfo.contrast = 0;
	m_ImageExternalInfo.saturation = 0;
}


void	CVPViewerConf::setAppliedScale(float scale)
{
	if (scale == 0)scale = 1.0f;

	m_appliedScale = scale;
	m_pDrawObjMgr->UpdateAppliedScale(scale);
}


bool	CVPViewerConf::SaveConf(const TCHAR *strConfFile)
{
	FILE *fp;
	if (strConfFile == NULL)
		fp = _tfopen(m_strConfFile, _T("w+t"));
	else
		fp = _tfopen(strConfFile, _T("w+t"));

	if (fp == NULL) return false;
	
	fprintf(fp, "{ \"ImageExternalInfo\" : {\n");
	fprintf(fp, "    \"scanningResolution\" : %4.5f,\n", m_ImageExternalInfo.scanningRes);
	fprintf(fp, "    \"appliedScale\" : %4.5f,\n", m_appliedScale);
	fprintf(fp, "    \"well\" : \"%s\",\n", m_ImageExternalInfo.well.c_str());
	fprintf(fp, "    \"depth\" : \"%s\",\n", m_ImageExternalInfo.depth.c_str());
	fprintf(fp, "    \"comment\" : \"%s\",\n", m_ImageExternalInfo.comment.c_str());
	fprintf(fp, "    \"brightness\" : %d,\n", m_ImageExternalInfo.brightness);
	fprintf(fp, "    \"contrast\" : %d,\n", m_ImageExternalInfo.contrast);
	fprintf(fp, "    \"saturation\" : %d,\n", m_ImageExternalInfo.saturation);
	fprintf(fp, "    \"layers\" : [\n");
	for (int i = 0; i < m_ImageExternalInfo.layers.size(); i++)
	{
		if (i != 0) fprintf(fp, ", \n");
		fprintf(fp, "      { \"index\" : %d, \"name\" : \"%s\" }", m_ImageExternalInfo.layers[i].index, m_ImageExternalInfo.layers[i].name.c_str());
	}
	fprintf(fp, "      ]\n");
	fprintf(fp, "    },\n");
	
	//Update GridInfo
	std::vector<CGridObj*>& GridObjList = m_pDrawObjMgr->GetGridObjList();
	fprintf(fp, " \"GridInfo\" : [\n");
	for (unsigned int i = 0; i < GridObjList.size(); i++){
		GridInfo *pInfo = GridObjList[i]->getInfo();

		fprintf(fp, "    {\"color\" : %lu, \"interval\" : %d, \"thick\" : %d}", pInfo->color, pInfo->interval, pInfo->thick);
		if (GridObjList.size() - 1 != i){
			fprintf(fp, ",\n");
		}
		else{
			fprintf(fp, "\n");
		}
	}
	fprintf(fp, "  ],\n");
		
	//Update ROIInfo
	std::vector<CROIObj*>& ROIObjList = m_pDrawObjMgr->GetROIObjList();
	fprintf(fp, " \"ROIInfo\" : [\n");
	for (unsigned int i = 0; i < ROIObjList.size(); i++){
		ROIInfo *pInfo = ROIObjList[i]->getInfo();

		fprintf(fp, "    {\"color\" : %d, \"description\" : \"%s\", \"scale\" : %.6f, \"layer\" : %d, \n",
			pInfo->color, pInfo->description.c_str(), pInfo->scale, pInfo->layer);
		fprintf(fp, "       \"Point\" : [\n");

		for (unsigned int j = 0; j < 4; j++){
			if (3 != j)
				fprintf(fp, "       {\"x\" : %.6f,  \"y\" : %.6f }, \n", pInfo->pts[j].x, pInfo->pts[j].y);
			else
				fprintf(fp, "       {\"x\" : %.6f,  \"y\" : %.6f } \n", pInfo->pts[j].x, pInfo->pts[j].y);
		}
		fprintf(fp, "       ]\n");
				
		if (ROIObjList.size() - 1 != i)
			fprintf(fp, "    },\n");
		else
			fprintf(fp, "    }\n");

	}
	fprintf(fp, "  ],\n");
	
	//Update MeasureInfo
	std::vector<CMeasureObj*>& MeasureObjList = m_pDrawObjMgr->GetMeasureObjList();
	fprintf(fp, " \"MeasureInfo\" : [\n");
	for (unsigned int i = 0; i < MeasureObjList.size(); i++){
		MeasureInfo *pInfo = MeasureObjList[i]->getInfo();

		fprintf(fp, "    {\"color\" : %d, \"description\" : \"%s\", \"type\" : %d, \"scale\" : %.6f, \"layer\" : %d, \"length\" : %.6f, \"red\" : %d, \"green\" : %d, \"blue\" : %d,\n",
			pInfo->color, pInfo->description.c_str(), pInfo->type, pInfo->scale, pInfo->layer, pInfo->length, pInfo->red, pInfo->green, pInfo->blue);
		fprintf(fp, "       \"Point\" : [\n");

		for (unsigned int j = 0; j < pInfo->pts.size(); j++){
			if (pInfo->pts.size() - 1 != j)
				fprintf(fp, "       {\"x\" : %.6f,  \"y\" : %.6f }, \n", pInfo->pts[j].x, pInfo->pts[j].y);
			else
				fprintf(fp, "       {\"x\" : %.6f,  \"y\" : %.6f } \n", pInfo->pts[j].x, pInfo->pts[j].y);
		}
		fprintf(fp, "       ]\n");
		if (MeasureObjList.size() - 1 != i)
			fprintf(fp, "    },\n");
		else
			fprintf(fp, "    }\n");
	}
	fprintf(fp, "  ],\n");

	//Update Label Info
	std::vector<CLabelObj*>& LabelObjList = m_pDrawObjMgr->GetLableList();
	fprintf(fp, " \"LabelInfo\" : [\n");
	for (unsigned int i = 0; i < LabelObjList.size(); i++){
		LabelInfo *pInfo = LabelObjList[i]->getInfo();

		fprintf(fp, "    {\"font\" : \"%s\", \"description\" : \"%s\", \"fontColor\" : %lu, \"bgColor\" : %lu, \"size\" : %d, \"transparent\" : %d, \"layer\" : %d, \n",
			std::string(CT2CA(pInfo->font.operator LPCWSTR())).c_str(), pInfo->description.c_str(), pInfo->fontColor, pInfo->bgColor, pInfo->size, pInfo->transparent, pInfo->layer);
		pInfo->font.ReleaseBuffer();
		fprintf(fp, "       \"Point\" : [\n");

		for (unsigned int j = 0; j < 3; j++){
			fprintf(fp, "       {\"x\" : %.6f,  \"y\" : %.6f }, \n", pInfo->pts[j].x, pInfo->pts[j].y);
		}
		fprintf(fp, "       {\"x\" : %.6f,  \"y\" : %.6f } \n", pInfo->pts[3].x, pInfo->pts[3].y);

		fprintf(fp, "       ]\n");
		if (LabelObjList.size() - 1 != i)
			fprintf(fp, "    },\n");
		else
			fprintf(fp, "    }\n");
	}
	fprintf(fp, "  ]\n");


	fprintf(fp, "}\n");
	

	fclose(fp);
	return true;
}


void	LoadImageInfo(Json::Value& config, ImageExternalInfo *info)
{
	Json::Value ImageExternal = config["ImageExternalInfo"];
	info->scanningRes = ImageExternal["scanningResolution"].asFloat();
	info->viewingScale = ImageExternal["appliedScale"].asFloat();
	info->well = ImageExternal["well"].asString();
	info->depth = ImageExternal["depth"].asString();
	info->comment = ImageExternal["comment"].asString();
	info->brightness = ImageExternal["brightness"].asInt();
	info->contrast = ImageExternal["contrast"].asInt();
	info->saturation = ImageExternal["saturation"].asInt();

	Json::Value LayerList = ImageExternal["layers"];
	info->layers.clear();
	for (unsigned int i = 0; i < LayerList.size(); i++){
		LayerInfo layer;
		layer.index = LayerList[i]["index"].asInt();
		layer.name = LayerList[i]["name"].asString();
		info->layers.push_back(layer);
	}

	/*
	Json::Value PointArray = Measure[i]["Point"];
	for (unsigned int j = 0; j < PointArray.size(); j++){
		POINTF pt_f;
		pt_f.x = PointArray[j]["x"].asFloat();
		pt_f.y = PointArray[j]["y"].asFloat();
		info.pts.push_back(pt_f);
	}
	*/

}

void	LoadROIInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr)
{
	Json::Value ROI = config["ROIInfo"];
	for (unsigned int i = 0; i < ROI.size(); i++){
		ROIInfo info;
				
		info.description = ROI[i]["description"].asString();
		info.color = (ePENCOLOR)ROI[i]["color"].asInt();

		Json::Value PointArray = ROI[i]["Point"];
		for (unsigned int j = 0; j < PointArray.size(); j++){
			POINTF pt_f;
			pt_f.x = PointArray[j]["x"].asFloat();
			pt_f.y = PointArray[j]["y"].asFloat();
			info.pts[j] = pt_f;
		}
					
		info.scale = ROI[i]["scale"].asFloat();
		info.layer = ROI[i]["layer"].asInt();

		CROIObj* obj = new CROIObj(&info);
		pDrawObjMgr->AddObj(obj);
	}
}


void	LoadMeasureInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr)
{
	Json::Value Measure = config["MeasureInfo"];
	for (unsigned int i = 0; i < Measure.size(); i++){
		MeasureInfo info;
		
		info.description = Measure[i]["description"].asString();
		info.color	= (ePENCOLOR)Measure[i]["color"].asInt();
		info.type = (eOBJTYPE)Measure[i]["type"].asInt();

		info.red = Measure[i]["red"].asInt();
		info.green = Measure[i]["green"].asInt();
		info.blue = Measure[i]["blue"].asInt();

		Json::Value PointArray = Measure[i]["Point"];
		for (unsigned int j = 0; j < PointArray.size(); j++){
			POINTF pt_f;
			pt_f.x = PointArray[j]["x"].asFloat();
			pt_f.y = PointArray[j]["y"].asFloat();
			info.pts.push_back(pt_f);
		}

		info.scale = Measure[i]["scale"].asFloat();
		info.layer = Measure[i]["layer"].asInt();
		info.length = Measure[i]["length"].asFloat();
		CMeasureObj* obj = new CMeasureObj(&info);
		pDrawObjMgr->AddObj(obj);
	}
}

void	LoadLabelInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr)
{
	Json::Value Label = config["LabelInfo"];
	for (unsigned int i = 0; i < Label.size(); i++){
		LabelInfo info;

		info.font = Label[i]["font"].asCString();
		info.description = Label[i]["description"].asString();
		info.fontColor = Label[i]["fontColor"].asLargestUInt();
		info.bgColor = Label[i]["bgColor"].asLargestUInt();
		info.size = Label[i]["size"].asInt();
		info.transparent = Label[i]["transparent"].asInt();
		Json::Value PointArray = Label[i]["Point"];
		for (unsigned int j = 0; j < 4; j++){
			POINTF pt_f;
			pt_f.x = PointArray[j]["x"].asFloat();
			pt_f.y = PointArray[j]["y"].asFloat();
			info.pts[j] = pt_f;
		}

		info.layer = Label[i]["layer"].asInt();

		CLabelObj* obj = new CLabelObj(&info);
		pDrawObjMgr->AddObj(obj);
	}
}

void	LoadGridInfo(Json::Value& config, CDrawObjMgr *pDrawObjMgr)
{
	Json::Value Grid = config["GridInfo"];
	GridInfo info;

	info.color = Grid[0]["color"].asLargestUInt();
	info.interval = Grid[0]["interval"].asInt();
	info.ratio = 1;
	info.thick = Grid[0]["thick"].asInt();
			
	if (info.interval == 0){			
		info.color = RGB(0, 255, 0);
		info.interval = 1000;
		info.thick = 2;
	}

	pDrawObjMgr->GetGridObjList().clear();		
	CGridObj* obj = new CGridObj(&info);
	pDrawObjMgr->AddObj(obj);
}