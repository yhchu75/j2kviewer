// RGBMeasureTable.cpp : implementation file
//

#include "stdafx.h"
#include "RGBMeasureTable.h"
#include "afxdialogex.h"


// CRGBMeasureTable dialog

IMPLEMENT_DYNAMIC(CRGBMeasureTable, CDialogEx)

CRGBMeasureTable::CRGBMeasureTable(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRGBMeasureTable::IDD, pParent)
{
	Create(CRGBMeasureTable::IDD, pParent);
}

CRGBMeasureTable::~CRGBMeasureTable()
{
}

void CRGBMeasureTable::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RGB, m_ListControl);
}


BEGIN_MESSAGE_MAP(CRGBMeasureTable, CDialogEx)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_CLICK, IDC_LIST_RGB, &CRGBMeasureTable::OnNMClickListRgb)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RGB, &CRGBMeasureTable::OnNMDblclkListRgb)
	ON_BN_CLICKED(IDC_BUTTON_RGBEXPORT, &CRGBMeasureTable::OnBnClickedButtonRgbexport)
END_MESSAGE_MAP()


// CRGBMeasureTable message handlers


void CRGBMeasureTable::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	// TODO: Add your message handler code here
	int i, j;
	for (i = 0; i<NUM_RTABLE_ROW; i++)
	{
		for (j = 0; j<NUM_RTABLE_COL; j++)
		{
			m_ListControl.SetItemText(i, j, itemText[i][j]);
		}
	}
}


BOOL CRGBMeasureTable::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here
	ListView_SetExtendedListViewStyleEx(m_ListControl.GetSafeHwnd(), LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

	LV_COLUMN lvc;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	for (int i = 0; i<NUM_RTABLE_COL; i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = RGBColumnLabel[i];
		lvc.cx = RGBColumnWidth[i];
		lvc.fmt = RGBColumnFmt[i];
		m_ListControl.InsertColumn(i, &lvc);
	}

	LV_ITEM lvi;

	for (int i = 0; i < NUM_RTABLE_ROW; i++)
	{
		lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_STATE;
		lvi.iItem = i;
		lvi.iSubItem = 0;
		lvi.pszText = _T("");
		lvi.stateMask = LVIS_STATEIMAGEMASK;
		lvi.state = INDEXTOSTATEIMAGEMASK(1);

		m_ListControl.InsertItem(&lvi);
	}

	for (int i = 0; i<NUM_RTABLE_ROW; i++)
	{
		for (int j = 0; j<NUM_RTABLE_COL; j++)
		{
			itemText[i][j][0] = '\0';
		}
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CRGBMeasureTable::OnRefresh()
{
	AfxGetMainWnd()->OnCmdMsg(ID_MEASUREMENT_REFRESHTABLE, 0, NULL, NULL);

	int i, j;
	for (i = 0; i<NUM_RTABLE_ROW; i++)
	{
		for (j = 0; j<NUM_RTABLE_COL; j++)
		{
			m_ListControl.SetItemText(i, j, itemText[i][j]);
		}
	}
	Invalidate();
}

void CRGBMeasureTable::OnNMClickListRgb(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	std::vector<CMeasureObj*>& MeasurementList = m_pDrawObjMgr->GetMeasureObjList();
	if (index >= MeasurementList.size())return;

	int indexCount = 0;
	for (int i = 0; i < MeasurementList.size(); i++)
	{
		if (MeasurementList.at(i)->GetObjType() == eOBJTYPE::MEASURE_AREA){
			if (indexCount == index){
				m_pDrawObjMgr->SetActiveObj(MeasurementList[i]);
				return;
			}
			else
				indexCount++;
		}
	}

}


void CRGBMeasureTable::OnNMDblclkListRgb(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	std::vector<CMeasureObj*>& MeasurementList = m_pDrawObjMgr->GetMeasureObjList();
	if (index >= MeasurementList.size())return;
	
	int indexCount = 0;
	int realIndex = 0;
	for (int i = 0; i < MeasurementList.size(); i++)
	{
		if (MeasurementList.at(i)->GetObjType() == eOBJTYPE::MEASURE_AREA){
			if (indexCount == index){
				m_pDrawObjMgr->SetActiveObj(MeasurementList[i]);
				realIndex = i;
				break;
			}
			else
				indexCount++;
		}
	}

	POINTF center_ptf;
	float max_x = 0;
	float min_x = 1;
	float max_y = 0;
	float min_y = 1;
	for (int i = 0; i < MeasurementList[realIndex]->getInfo()->pts.size(); i++){
		float x = MeasurementList[realIndex]->getInfo()->pts.at(i).x;
		float y = MeasurementList[realIndex]->getInfo()->pts.at(i).y;

		max_x = (x > max_x) ? x : max_x;
		min_x = (x < min_x) ? x : min_x;
		max_y = (y > max_y) ? y : max_y;
		min_y = (y < min_y) ? y : min_y;
	}
	center_ptf.x = (max_x + min_x) / 2;
	center_ptf.y = (max_y + min_y) / 2;

	m_app->set_center_pos(center_ptf.x, center_ptf.y, m_app->rendering_scale);
}


void CRGBMeasureTable::OnBnClickedButtonRgbexport()
{
	// TODO: Add your control notification handler code here
	TCHAR Filter[] = _T("csv File(*.csv) |*.csv|All Files(*.*)|*.*|");
	CFileDialog Dlg(FALSE, _T("csv file(*.csv)"), _T("*.csv"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, Filter, NULL);

	if (Dlg.DoModal() != IDOK) return;

	CString	FileName = Dlg.GetPathName();
	FILE *fp = _tfopen(FileName, _T("wt,ccs=UNICODE"));

	_ftprintf(fp, _T("%s \t   %s \t  %s \t  %s \t  %s \n"), _T("No"), _T("Descrtiption"), _T("Red"), _T("Green"), _T("Blue"));

	for (int i = 0; i < NUM_RTABLE_ROW; i++){
		if (itemText[i][0][0] == '\0') break;
		_ftprintf(fp, _T("%s \t  %s \t  %s \t  %s \t %s \n"), itemText[i][0], itemText[i][1], itemText[i][2], itemText[i][3], itemText[i][4]);
	}

	fclose(fp);
}
