// NewRoi.cpp : implementation file
//

#include "stdafx.h"
#include "NewRoi.h"
#include "EditRoi.h"
#include "VPViewerConf.h"

// CNewRoi dialog

IMPLEMENT_DYNAMIC(CNewRoi, CDialog)

CNewRoi::CNewRoi(/*CKdu_showApp* app,*/ CWnd* pParent /*=NULL*/)
	: CDialog(CNewRoi::IDD, pParent)
{
	m_NextROIIndex = 0;
}

CNewRoi::~CNewRoi()
{
}

void CNewRoi::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DESCRIPTION, m_description);
	DDX_Control(pDX, IDC_COLOR, m_color);
	DDX_Text(pDX, IDC_DESCRIPTION, m_descriptionStr);
	DDX_CBIndex(pDX, IDC_COLOR, m_Colorindex);
}


BEGIN_MESSAGE_MAP(CNewRoi, CDialog)
END_MESSAGE_MAP()


// CNewRoi message handlers


BOOL CNewRoi::OnInitDialog()
{
	CDialog::OnInitDialog();

	TCHAR strTemp[32];
	_stprintf(strTemp, _T("ROI%02d"), m_NextROIIndex);

	SetDlgItemText(IDC_DESCRIPTION, strTemp);
	
	for (int i = 0; i < 6; i++)
		m_color.AddString(g_PENColorString[i]);
	m_color.SetCurSel(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
