#pragma once

#include "kdu_compressed.h"
#include "GeneralTab.h"
#include "ImageInfoTab.h"
#include "ROISTab.h"
#include "AdjustmentsTab.h"

// COverview dialog

class COverview : public CDialog
{
	DECLARE_DYNAMIC(COverview)

public:
	COverview(CKdu_showApp *app, CWnd* pParent = NULL);   // standard constructor
	virtual ~COverview();

// Dialog Data
	enum { IDD = IDD_OVERVIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();

	DECLARE_MESSAGE_MAP()

private: // Data
  CKdu_showApp *app;

public:
	virtual BOOL OnInitDialog();

public:
	CTabCtrl		m_overviewTab;
	CGeneralTab		m_generalTab;
	CImageInfoTab	m_imageInfoTab;
	CROISTab		m_roisTab;
	CAdjustmentsTab	m_adjustmentsTab;
	CButton			m_button;

	BOOL			m_bShowTab;
	int				m_nHeight;
	HACCEL			m_hAccel;

public:
	afx_msg void OnClose();
	afx_msg void OnPaint();
	void check_win_size(kdu_coords image_size);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	bool overview_panning;
	float overview_rate;
	CPoint last_mouse_point;

	// for point counting
	bool		m_bPointCounting;
	bool		m_bDrawingBox;
	kdu_coords	pcbox_start, pcbox_end;
	RECT		pointCount_rect;
	SIZE		border;
	HCURSOR		cross_cursor;
	HCURSOR		normal_cursor;


};



