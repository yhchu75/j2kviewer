#pragma once
#include "resource.h"

// Grid 대화 상자입니다.

class GridValues
{
public:
	GridValues(void);
	~GridValues(void);

	static COLORREF LineColor;
	static int LineWidth;
	static int interval;
};

class CGrid : public CDialogEx
{
	DECLARE_DYNAMIC(CGrid)

public:
	CGrid(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CGrid();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GRID };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	void OnColorClick(void);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnChangeWidthEdit();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedApply();
	afx_msg void OnNMReleasedcapture(NMHDR* pNMHDR, LRESULT* pResult);
private:
	CStatic m_ColorBox;
	CEdit m_thickEdit;
	CSliderCtrl m_sliderInterval;
	CStatic m_intervlaLabel;

	std::vector<int> m_intervals;

	std::vector<int> setIntervals(int interval);

	void setIntervalLabel(int interval);

	CString m_unit;
		
public:
	CDrawObjMgr* mgr;
	CKdu_showApp* app;
	
	afx_msg void OnDeltaposThickSpin(NMHDR *pNMHDR, LRESULT *pResult);
};
