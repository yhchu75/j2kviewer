#pragma once
#include "EditRoi.h"

class CKdu_showApp;
class CDrawObjMgr;
class CROISTab : public CDialog
{
	DECLARE_DYNAMIC(CROISTab)

public:
	CROISTab(CWnd* pParent = NULL);   // standard constructor
	virtual ~CROISTab();

// Dialog Data
	enum { IDD = IDD_ROIS };
	void	setApp(CKdu_showApp	*app, CDrawObjMgr* pDrawObjMgr  ){
		m_app = app; 
		m_pDrawObjMgr = pDrawObjMgr;
	}

	void	UpdateROIList();

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedBtnHide();
	afx_msg void OnLbnSelchangeRoilist();
	afx_msg void OnBnClickedGobutton();
	afx_msg void OnBnClickedNew();
	afx_msg void OnBnClickedEditroi();

	DECLARE_MESSAGE_MAP()

private:
	CKdu_showApp	*m_app;
    CDrawObjMgr	*m_pDrawObjMgr;
		
	HACCEL			m_hAccel;
	CButton		m_btnHide;

	CPoint		m_selRoiPoint;
	CListBox	m_roiListBox;
	CNewRoi		m_newRoiDlg;
	CEditRoi	m_editRoiDlg;
};
