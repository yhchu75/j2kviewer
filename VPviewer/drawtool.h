// drawtool.h - interface for CDrawTool and derivatives
//


#ifndef __DRAWTOOL_H__
#define __DRAWTOOL_H__

#include "drawobj.h"

class CChildView;

enum DrawShape
{
	selection,
	line,
	rect,
	roundRect,
	ellipse,
	area,
	curve,
	grainPerimeter,
	grainCoating
};

class CDrawTool
{
// Constructors
public:
	CDrawTool(DrawShape nDrawShape);

// Overridables
	virtual void OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnRButtonDown(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point);
	//virtual void OnEditProperties(CChildView* pView);
	virtual void OnCancel();

// Attributes
	DrawShape m_drawShape;

	static CDrawTool* FindTool(DrawShape drawShape);
	static CPtrList c_tools;
	static CPoint c_down;
	static UINT c_nDownFlags;
	static CPoint c_last;
	static DrawShape c_drawShape;
};

class CSelectTool : public CDrawTool
{
// Constructors
public:
	CSelectTool();

// Implementation
	virtual void OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point);
	//virtual void OnEditProperties(CChildView* pView);
};

class CRectTool : public CDrawTool
{
// Constructors
public:
	CRectTool(DrawShape drawShape);

// Implementation
	virtual void OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point);
};

class CPolyTool : public CDrawTool
{
// Constructors
public:
	CPolyTool(DrawShape drawShape);

// Implementation
	virtual void OnLButtonDown(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnRButtonDown(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(CChildView* pView, UINT nFlags, const CPoint& point);
	virtual void OnCancel();
private:
	CDrawPoly* m_pDrawObj;
};

////////////////////////////////////////////////////////////////////////////

#endif // __DRAWTOOL_H__
