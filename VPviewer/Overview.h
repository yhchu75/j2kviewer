#pragma once

#include "GeneralTab.h"
#include "ImageInfoTab.h"
#include "ROISTab.h"
#include "AdjustmentsTab.h"

// COverview dialog

class COverview : public CDialog
{
	DECLARE_DYNAMIC(COverview)

public:
	COverview(CKdu_showApp *app, CWnd* pParent = NULL);   // standard constructor
	virtual ~COverview();

// Dialog Data
	enum { IDD = IDD_OVERVIEW };

	const CRect*	GetOverviewFrameRect(){ return &m_OverviewFrameRect; }
	BOOL	SetOverviewData(int width, int height, char *pData);
	void	UpdateViewPos(float pos_x, float pos_y, float scale_x, float scale_y, int image_w, int image_h);

protected:
	DECLARE_MESSAGE_MAP()

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
		
	afx_msg void OnPaint();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMoving(UINT nSide, LPRECT lpRect);
	afx_msg void OnBnClickedCheckOverview();

public:
	CTabCtrl		m_overviewTab;
	CGeneralTab		m_generalTab;
	CImageInfoTab	m_imageInfoTab;
	CROISTab		m_roisTab;
	CAdjustmentsTab	m_adjustmentsTab;
	CButton			m_button;

	BOOL			m_bShowTab;
	HACCEL			m_hAccel;

	CPoint			m_position;

private:
	CKdu_showApp*	m_app;

	CRect			m_OverviewFrameRect;
	CRect			m_OverviewRect;
	int				m_nTabHeight;

	char*			m_pOverviewBuf;
	HDC				m_hOverviewDC;
	HBITMAP			m_hOverviewBitmap;
	CRect			m_rcOrg;
	CRect			m_rcView;

	float			m_fOverviewScale_w, m_fOverviewScale_h;
	
	bool			m_bOverviewPanning;
	CPoint			m_LastMousePoint;
		
	void			DrawBox(CDC* dc, RECT* rect, DWORD color);
	void			UpdateOverviewRegion(CDC* dc);
};



