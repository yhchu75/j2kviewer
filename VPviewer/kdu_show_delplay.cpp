/******************************************************************************/
// File: kdu_show.cpp [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/******************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/******************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/*******************************************************************************
Description:
   Implements the application object of the interactive JPEG2000 viewer,
"kdu_show".  Menu processing and idle-time decompressor processing are all
controlled from here.  The "kdu_show" application demonstrates some of the
support offered by the Kakadu framework for interactive applications,
including persistence and incremental region-based decompression.
*******************************************************************************/

#include "stdafx.h"
#include <math.h>
#include "kdu_show.h"
#include "kd_metadata_editor.h"
#include "MainFrm.h"
#include "kdu_arch.h"
#include "kdu_messaging.h"
#include "kdu_threads.h"
#include "kdu_utils.h"
#include ".\kdu_show.h"
#include "Overview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const char *jpip_channel_types[4] = {"http","http-tcp","none",NULL};

CKdu_showApp theApp; // There can only be one application object.


/* ========================================================================== */
/*                            Internal Functions                              */
/* ========================================================================== */

/******************************************************************************/
/* INLINE                         find_scale                                  */
/******************************************************************************/

static inline float
  find_scale(int expand, int discard_levels)
  /* Returns `expand'*2^{-`discard_levels'}. */
{
  float scale = (float) expand;
  for (; discard_levels > 0; discard_levels--)
    scale *= 0.5F;
  return scale;
}

/******************************************************************************/
/* STATIC                      register_protocols                             */
/******************************************************************************/

static void
  register_protocols(const char *executable_path)
  /* Sets up the registry entries required to register the JPIP protocol
     variants which are currently supported.  URL's of the form
     <protocol name>:... will be passed into a newly launched instance of
     the present application when clicked within the iExplorer application. */
{
  DWORD disposition;
  HKEY root_key, icon_key, shell_key, open_key, command_key;

  const char *app_name = strrchr(executable_path,'\\');
  if (app_name == NULL)
    app_name = executable_path;
  else
    app_name++;

  const char *description = "JPIP Interactive Imaging Protocol";
  char *command_string = new char[strlen(executable_path)+4];
  strcpy(command_string,executable_path);
  strcat(command_string," %1");

  do {
      if (RegCreateKeyEx(HKEY_CLASSES_ROOT,"jpip",0,REG_NONE,
                         REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,
                         &root_key,&disposition) != ERROR_SUCCESS)
        continue;
      if (RegSetValueEx(root_key,NULL,0,REG_SZ,(kdu_byte *) description,
                        (int) strlen(description)+1) != ERROR_SUCCESS)
        continue;
      if (RegSetValueEx(root_key,"URL Protocol",0,REG_SZ,
                        (kdu_byte *)(""),1) != ERROR_SUCCESS)
        continue;
      if (RegCreateKeyEx(root_key,"DefaultIcon",0,REG_NONE,
                         REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,
                         &icon_key,&disposition) != ERROR_SUCCESS)
        continue;
      if (RegSetValueEx(icon_key,NULL,0,REG_SZ,(kdu_byte *) app_name,
                        (int) strlen(app_name)+1) != ERROR_SUCCESS)
        continue;
      if (RegCreateKeyEx(root_key,"shell",0,REG_NONE,
                         REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,
                         &shell_key,&disposition) != ERROR_SUCCESS)
        continue;
      if (RegCreateKeyEx(shell_key,"open",0,REG_NONE,
                         REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,
                         &open_key,&disposition) != ERROR_SUCCESS)
        continue;
      if (RegCreateKeyEx(open_key,"command",0,REG_NONE,
                         REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,
                         &command_key,&disposition) != ERROR_SUCCESS)
        continue;
      RegSetValueEx(command_key,NULL,0,REG_SZ,(kdu_byte *) command_string,
                    (int) strlen(command_string)+1);
    } while (0);

  delete[] command_string;
}

/******************************************************************************/
/* STATIC                          copy_tile                                  */
/******************************************************************************/

static void
  copy_tile(kdu_tile tile_in, kdu_tile tile_out)
  /* Walks through all code-blocks of the tile in raster scan order, copying
     them from the input to the output tile. */
{
  int c, num_components = tile_out.get_num_components();
  for (c=0; c < num_components; c++)
    {
      kdu_tile_comp comp_in;  comp_in  = tile_in.access_component(c);
      kdu_tile_comp comp_out; comp_out = tile_out.access_component(c);
      int r, num_resolutions = comp_out.get_num_resolutions();
      for (r=0; r < num_resolutions; r++)
        {
          kdu_resolution res_in;  res_in  = comp_in.access_resolution(r);
          kdu_resolution res_out; res_out = comp_out.access_resolution(r);
          int b, min_band;
          int num_bands = res_in.get_valid_band_indices(min_band);
          for (b=min_band; num_bands > 0; num_bands--, b++)
            {
              kdu_subband band_in;  band_in = res_in.access_subband(b);
              kdu_subband band_out; band_out = res_out.access_subband(b);
              kdu_dims blocks_in;  band_in.get_valid_blocks(blocks_in);
              kdu_dims blocks_out; band_out.get_valid_blocks(blocks_out);
              if ((blocks_in.size.x != blocks_out.size.x) ||
                  (blocks_in.size.y != blocks_out.size.y))
                { kdu_error e; e << "Transcoding operation cannot proceed: "
                  "Code-block partitions for the input and output "
                  "code-streams do not agree."; }
              kdu_coords idx;
              for (idx.y=0; idx.y < blocks_out.size.y; idx.y++)
                for (idx.x=0; idx.x < blocks_out.size.x; idx.x++)
                  {
                    kdu_block *in  = band_in.open_block(idx+blocks_in.pos);
                    kdu_block *out = band_out.open_block(idx+blocks_out.pos);
                    if (in->K_max_prime != out->K_max_prime)
                      { kdu_error e;
                        e << "Cannot copy blocks belonging to subbands with "
                             "different quantization parameters."; }
                    if ((in->size.x != out->size.x) ||
                        (in->size.y != out->size.y))  
                      { kdu_error e; e << "Cannot copy code-blocks with "
                        "different dimensions."; }
                    out->missing_msbs = in->missing_msbs;
                    if (out->max_passes < (in->num_passes+2))
                      out->set_max_passes(in->num_passes+2,false);
                    out->num_passes = in->num_passes;
                    int num_bytes = 0;
                    for (int z=0; z < in->num_passes; z++)
                      {
                        num_bytes += (out->pass_lengths[z]=in->pass_lengths[z]);
                        out->pass_slopes[z] = in->pass_slopes[z];
                      }
                    if (out->max_bytes < num_bytes)
                      out->set_max_bytes(num_bytes,false);
                    memcpy(out->byte_buffer,in->byte_buffer,(size_t) num_bytes);
                    band_in.close_block(in);
                    band_out.close_block(out);
                  }
            }
        }
    }
}

/* ========================================================================== */
/*                     Error and Warning Message Handlers                     */
/* ========================================================================== */

/******************************************************************************/
/* CLASS                         core_messages_dlg                            */
/******************************************************************************/

class core_messages_dlg : public CDialog
{
  public:
    core_messages_dlg(const char *src, CWnd* pParent = NULL)
      : CDialog(core_messages_dlg::IDD, pParent)
      {
        int num_chars;
        const char *sp;
        for (sp=src, num_chars=0; *sp != '\0'; sp++, num_chars++)
          if ((*sp == '&') || (*sp == '\\'))
            num_chars++; // Need to replicate these special characters
        string = new char[num_chars+1];
        for (sp=src, num_chars=0; *sp != '\0'; sp++)
          {
            string[num_chars++] = *sp;
            if ((*sp == '&') || (*sp == '\\'))
              string[num_chars++] = *sp;
          }
        string[num_chars] = '\0';
      }
    ~core_messages_dlg()
      {
        if (string != NULL)
          delete[] string;
      }
// Dialog Data
	//{{AFX_DATA(core_messages_dlg)
	enum { IDD = IDD_CORE_MESSAGES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(core_messages_dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
private:
  CStatic *get_static()
    {
      return (CStatic *) GetDlgItem(IDC_MESSAGE);
    }
private:
  char *string;
protected:
	// Generated message map functions
	//{{AFX_MSG(core_messages_dlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/******************************************************************************/
/*                      core_messages_dlg::DoDataExchange                     */
/******************************************************************************/

void
  core_messages_dlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(core_messages_dlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(core_messages_dlg, CDialog)
	//{{AFX_MSG_MAP(core_messages_dlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************************************************************************/
/*                      core_messages_dlg::OnInitDialog                       */
/******************************************************************************/

BOOL core_messages_dlg::OnInitDialog() 
{
  CDialog::OnInitDialog();

  while (*string == '\n')
    string++; // skip leading empty lines, if any.
  get_static()->SetWindowText(string);

  // Find the height of the displayed text
  int text_height = 0;
  SIZE text_size;
  CDC *dc = get_static()->GetDC();
  const char *scan = string;
  while (*scan != '\0')
    {
      const char *sp = strchr(scan,'\n');
      if (sp == NULL)
        sp = scan + strlen(scan);
      if (scan != sp)
        text_size = dc->GetTextExtent(scan,(int)(sp-scan));
      text_height += text_size.cy;
      scan = (*sp != '\0')?sp+1:sp;
    }
  get_static()->ReleaseDC(dc);

  // Resize windows to fit the text height

  WINDOWPLACEMENT dialog_placement, static_placement;
  GetWindowPlacement(&dialog_placement);
  get_static()->GetWindowPlacement(&static_placement);
  int dialog_width = dialog_placement.rcNormalPosition.right -
    dialog_placement.rcNormalPosition.left;
  int static_width = static_placement.rcNormalPosition.right -
    static_placement.rcNormalPosition.left;
  int dialog_height = dialog_placement.rcNormalPosition.bottom -
    dialog_placement.rcNormalPosition.top;
  int static_height = static_placement.rcNormalPosition.bottom -
    static_placement.rcNormalPosition.top;

  get_static()->SetWindowPos(NULL,0,0,static_width,text_height,
                             SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW);
  SetWindowPos(NULL,0,0,dialog_width,text_height+8+dialog_height-static_height,
               SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW);  
  return TRUE;
}

/******************************************************************************/
/* CLASS                     kd_message_collector                             */
/******************************************************************************/

class kd_message_collector : public kdu_message {
  /* This object is used to collect message text within the
     `CPropertiesDlg' object, so as to format and print descriptions of
     codestream parameter attributes.  It derives from the non-thread-safe
     `kdu_message' object, since multiple threads cannot access the same
     object simultaneously within `CPropertiesDlg'. */
  public: // Member functions
    kd_message_collector()
      {
        max_chars = 10; num_chars = 0;
        buffer = new char[max_chars+1]; *buffer = '\0';
      }
    ~kd_message_collector() { delete[] buffer; }
    const char *get_text() { return buffer; }
    void put_text(const char *string)
      {
        int new_chars = (int) strlen(string);
        if ((num_chars+new_chars) > max_chars)
          {
            max_chars = (num_chars+new_chars)*2;
            char *buf = new char[max_chars+1];
            strcpy(buf,buffer);
            delete[] buffer;
            buffer = buf;
          }
        num_chars += new_chars;
        strcat(buffer,string);
      }
    void flush(bool end_of_message)
      {
        if (end_of_message)
          { num_chars = 0; *buffer = '\0'; }
      }
  private: // Data
    int num_chars, max_chars;
    char *buffer;
  };

/******************************************************************************/
/* CLASS                   kd_core_message_collector                          */
/******************************************************************************/

class kd_core_message_collector : public kdu_thread_safe_message {
  /* This object is used to implement error and warning message services
     which are registered globally for use by all threads which may
     use `kdu_error' or `kdu_warning'.  Since this may happen from multiple
     threads, we derive this object from the thread-safe messaging service,
     `kdu_thread_safe_message'.  It is important that we explicitly call the
     base object's `flush' function at the end of the derived object's
     `flush' implementation, since this will release the mutex which
     serializes access to the messaging service. */
  public: // Member functions
    kd_core_message_collector(bool for_errors)
      {
        max_chars = 10; num_chars = 0;
        buffer = new char[max_chars+1]; *buffer = '\0';
        raise_end_of_message_exception = for_errors;
      }
    ~kd_core_message_collector() { delete[] buffer; }
    void put_text(const char *string)
      {
        int new_chars = (int) strlen(string);
        if ((num_chars+new_chars) > max_chars)
          {
            max_chars = (num_chars+new_chars)*2;
            char *buf = new char[max_chars+1];
            strcpy(buf,buffer);
            delete[] buffer;
            buffer = buf;
          }
        num_chars += new_chars;
        strcat(buffer,string);
      }
    void flush(bool end_of_message)
      {
        if (end_of_message)
          {
            core_messages_dlg messages(buffer,theApp.frame_wnd);
            num_chars = 0;   *buffer = '\0';
            messages.DoModal();
            kdu_thread_safe_message::flush(true); // May release other threads
            if (raise_end_of_message_exception)
              throw (int) 0;
          }
        else
          kdu_thread_safe_message::flush(false);
      }
  private: // Data
    int num_chars, max_chars;
    char *buffer;
    bool raise_end_of_message_exception;
  };

static kd_core_message_collector warn_collector(false);
static kd_core_message_collector err_collector(true);
static kdu_message_formatter warn_formatter(&warn_collector,50);
static kdu_message_formatter err_formatter(&err_collector,50);


/* ========================================================================== */
/*                                 kd_settings                                */
/* ========================================================================== */

/******************************************************************************/
/*                           kd_settings::kd_settings                         */
/******************************************************************************/

kd_settings::kd_settings()
{
  open_save_dir = NULL; open_idx = save_idx = 1;
  jpip_server = jpip_proxy = jpip_cache = NULL;
  jpip_request = jpip_channel = NULL;
  set_jpip_channel_type(jpip_channel_types[0]);
}

/******************************************************************************/
/*                          kd_settings::~kd_settings                         */
/******************************************************************************/

kd_settings::~kd_settings()
{
  if (open_save_dir != NULL)
    delete[] open_save_dir;
  if (jpip_server != NULL)
    delete[] jpip_server;
  if (jpip_proxy != NULL)
    delete[] jpip_proxy;
  if (jpip_cache != NULL)
    delete[] jpip_cache;
  if (jpip_request != NULL)
    delete[] jpip_request;
  if (jpip_channel != NULL)
    delete[] jpip_channel;
}

/******************************************************************************/
/*                        kd_settings::save_to_registry                       */
/******************************************************************************/

void
  kd_settings::save_to_registry(CWinApp *app)
{
  app->WriteProfileString("files","directory",get_open_save_dir());
  app->WriteProfileInt("files","open-index",open_idx);
  app->WriteProfileInt("files","save-index",save_idx);
  app->WriteProfileString("jpip","cache-dir",get_jpip_cache());
  app->WriteProfileString("jpip","server",get_jpip_server());
  app->WriteProfileString("jpip","request",get_jpip_request());
  app->WriteProfileString("jpip","channel-type",get_jpip_channel_type());
}

/******************************************************************************/
/*                       kd_settings::load_from_registry                      */
/******************************************************************************/

void
  kd_settings::load_from_registry(CWinApp *app)
{
  set_open_save_dir(app->GetProfileString("files","directory").GetBuffer(0));
  open_idx = app->GetProfileInt("files","open-index",1);
  save_idx = app->GetProfileInt("files","save-index",1);
  const char *missing_cache_dir = "***";
  CString existing_cache_dir =
    app->GetProfileString("jpip","cache-dir",missing_cache_dir).GetBuffer(0);
  if (strcmp(existing_cache_dir,"***") == 0)
    {
      char temp_path[MAX_PATH+1];
      int len = GetTempPath(MAX_PATH,temp_path);
      if (len > 0)
        set_jpip_cache(temp_path);
    }
  else
    set_jpip_cache(existing_cache_dir);
  set_jpip_server(app->GetProfileString("jpip","server").GetBuffer(0));
  set_jpip_request(app->GetProfileString("jpip","request").GetBuffer(0));
  CString c_string = app->GetProfileString("jpip","channel-type");
  const char *string = c_string.GetBuffer(0);
  if ((string == NULL) || (*string == '\0'))
    string = jpip_channel_types[0];
  set_jpip_channel_type(string);
}


/* ========================================================================== */
/*                               kd_bitmap_buf                                */
/* ========================================================================== */

/******************************************************************************/
/*                        kd_bitmap_buf::create_bitmap                        */
/******************************************************************************/

void
  kd_bitmap_buf::create_bitmap(kdu_coords size)
{
  if (bitmap != NULL)
    {
      assert(this->size == size);
      return;
    }

  memset(&bitmap_info,0,sizeof(bitmap_info));
  bitmap_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  bitmap_info.bmiHeader.biWidth = size.x;
  bitmap_info.bmiHeader.biHeight = -size.y;
  bitmap_info.bmiHeader.biPlanes = 1;
  bitmap_info.bmiHeader.biBitCount = 32;
  bitmap_info.bmiHeader.biCompression = BI_RGB;
  void *buf_addr;
  bitmap = CreateDIBSection(NULL,&bitmap_info,DIB_RGB_COLORS,&buf_addr,NULL,0);
  if (bitmap == NULL)
    { kdu_error e; e << "Unable to allocate sufficient bitmap surfaces "
      "to service `kdu_region_compositor'."; }
  this->buf = (kdu_uint32 *) buf_addr;
  this->row_gap = size.x;
  this->size = size;
}


/* ========================================================================== */
/*                            kd_bitmap_compositor                            */
/* ========================================================================== */

/******************************************************************************/
/*                    kd_bitmap_compositor::allocate_buffer                   */
/******************************************************************************/

kdu_compositor_buf *
  kd_bitmap_compositor::allocate_buffer(kdu_coords min_size,
                                        kdu_coords &actual_size,
                                        bool read_access_required)
{
  if (min_size.x < 1) min_size.x = 1;
  if (min_size.y < 1) min_size.y = 1;
  actual_size = min_size;
  int row_gap = actual_size.x;

  kd_bitmap_buf *prev=NULL, *elt=NULL;

  // Start by trying to find a compatible buffer on the free list.
  for (elt=free_list, prev=NULL; elt != NULL; prev=elt, elt=elt->next)
    if (elt->size == actual_size)
      break;

  bool need_init = false;
  if (elt != NULL)
    { // Remove from the free list
      if (prev == NULL)
        free_list = elt->next;
      else
        prev->next = elt->next;
      elt->next = NULL;
    }
  else
    {
      // Delete the entire free list: one way to avoid accumulating buffers
      // whose sizes are no longer helpful
      while ((elt=free_list) != NULL)
        { free_list = elt->next;  delete elt; }

      // Allocate a new element
      elt = new kd_bitmap_buf;
      need_init = true;
    }
  elt->next = active_list;
  active_list = elt;
  elt->set_read_accessibility(read_access_required);
  if (need_init)
  {
	  TRACE("do create_bitmap\n");
    elt->create_bitmap(actual_size);
  }
  return elt;
}

/******************************************************************************/
/*                    kd_bitmap_compositor::delete_buffer                     */
/******************************************************************************/

void
  kd_bitmap_compositor::delete_buffer(kdu_compositor_buf *_buffer)
{
  kd_bitmap_buf *buffer = (kd_bitmap_buf *) _buffer;
  kd_bitmap_buf *scan, *prev;
  for (prev=NULL, scan=active_list; scan != NULL; prev=scan, scan=scan->next)
    if (scan == buffer)
      break;
  assert(scan != NULL);
  if (prev == NULL)
    active_list = scan->next;
  else
    prev->next = scan->next;

  scan->next = free_list;
  free_list = scan;
}


/* ========================================================================== */
/*                               CAboutDlg Class                              */
/* ========================================================================== */

class CAboutDlg : public CDialog {
  public:
    CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

  protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
  };

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************************************************************************/
/*                             CAboutDlg::CAboutDlg                           */
/******************************************************************************/

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
  //{{AFX_DATA_INIT(CAboutDlg)
  //}}AFX_DATA_INIT
}

/******************************************************************************/
/*                           CAboutDlg::DoDataExchange                        */
/******************************************************************************/

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAboutDlg)
  //}}AFX_DATA_MAP
}

/* ========================================================================== */
/*                              CJpipOpenDlg Class                            */
/* ========================================================================== */

class CJpipOpenDlg : public CDialog {
  public:
    CJpipOpenDlg(char *server_name, int server_len,
                 char *request, int request_len,
                 char *channel_type, int channel_length,
                 char *proxy_name, int proxy_len,
                 char *cache_dir_name, int cache_dir_len,
                 CWnd *pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CJpipOpenDlg)
	enum { IDD = IDD_JPIP_OPEN };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJpipOpenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
  protected:
	// Generated message map functions
	//{{AFX_MSG(CJpipOpenDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

  private: // Base object overrides
    BOOL OnInitDialog();
    void OnOK();

  private: // Control access
    CEdit *get_server()
      { return (CEdit *) GetDlgItem(IDC_JPIP_SERVER); }
    CEdit *get_request()
      { return (CEdit *) GetDlgItem(IDC_JPIP_REQUEST); }
    CComboBox *get_channel_type()
      { return (CComboBox *) GetDlgItem(IDC_JPIP_CHANNEL_TYPE); }
    CEdit *get_proxy()
      { return (CEdit *) GetDlgItem(IDC_JPIP_PROXY); }
    CEdit *get_cache_dir()
      { return (CEdit *) GetDlgItem(IDC_JPIP_CACHE_DIR); }
  private:
    char *server_name, *request, *channel_type;
    char *proxy_name, *cache_dir_name;
    int server_len, request_len, channel_len, proxy_len, cache_dir_len;
  };

/******************************************************************************/
/*                          CJpipOpenDlg::CJpipOpenDlg                        */
/******************************************************************************/

CJpipOpenDlg::CJpipOpenDlg(char *server_name, int server_len,
                           char *request, int request_len,
                           char *channel_type, int channel_len,
                           char *proxy_name, int proxy_len,
                           char *cache_dir_name, int cache_dir_len,
                           CWnd *pParent /*=NULL*/)
	: CDialog(CJpipOpenDlg::IDD, pParent)
{
  this->server_name = server_name; this->server_len = server_len;
  this->request = request; this->request_len = request_len;
  this->channel_type = channel_type; this->channel_len = channel_len;
  this->proxy_name = proxy_name; this->proxy_len = proxy_len;
  this->cache_dir_name = cache_dir_name; this->cache_dir_len = cache_dir_len;
	//{{AFX_DATA_INIT(CJpipOpenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************************************************************************/
/*                          CJpipOpenDlg::OnInitDialog                        */
/******************************************************************************/

BOOL
  CJpipOpenDlg::OnInitDialog()
{
  get_server()->SetWindowText(server_name);
  get_request()->SetWindowText(request);
  get_proxy()->SetWindowText(proxy_name);
  get_cache_dir()->SetWindowText(cache_dir_name);
  const char **scan;
  for (scan=jpip_channel_types; *scan != NULL; scan++)
    get_channel_type()->AddString(*scan);
  scan = jpip_channel_types;
  if (channel_type[0] == '\0')
    strncpy(channel_type,*scan,channel_len-1);
  get_channel_type()->SelectString(-1,channel_type);
  if (*server_name == '\0')
    GotoDlgCtrl(GetDlgItem(IDC_JPIP_SERVER));
  else
    GotoDlgCtrl(GetDlgItem(IDC_JPIP_REQUEST));
  return 0;
}

/******************************************************************************/
/*                         CJpipOpenDlg::DoDataExchange                       */
/******************************************************************************/

void
  CJpipOpenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJpipOpenDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CJpipOpenDlg, CDialog)
	//{{AFX_MSG_MAP(CJpipOpenDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************************************************************************/
/*                              CJpipOpenDlg::OnOK                            */
/******************************************************************************/

void
  CJpipOpenDlg::OnOK()
{
  int string_len;
  string_len = get_server()->GetLine(0,server_name,server_len-1);
  server_name[string_len] = '\0';
  string_len = get_request()->GetLine(0,request,request_len-1);
  request[string_len] = '\0';
  string_len = get_proxy()->GetLine(0,proxy_name,proxy_len-1);
  proxy_name[string_len] = '\0';
  string_len = get_cache_dir()->GetLine(0,cache_dir_name,cache_dir_len-1);
  cache_dir_name[string_len] = '\0';
  int cb_idx = get_channel_type()->GetCurSel();
  if (cb_idx != CB_ERR)
    {
      string_len = get_channel_type()->GetLBTextLen(cb_idx);
      if (string_len < channel_len)
        get_channel_type()->GetLBText(cb_idx,channel_type);
    }
  if (*server_name == '\0')
    {
      MessageBox("You must enter a server name or IP address!",
                 "Dialog entry error",MB_OK|MB_ICONERROR);
      GotoDlgCtrl(GetDlgItem(IDC_JPIP_SERVER));
      return;
    }
  if (*request == '\0')
    {
      MessageBox("You must enter an object (e.g., file name) to be served!",
                 "Dialog entry error",MB_OK|MB_ICONERROR);
      GotoDlgCtrl(GetDlgItem(IDC_JPIP_REQUEST));
      return;
    }
  if (*channel_type == '\0')
    {
      MessageBox("You must select a channel type\n"
                 "(or \"none\" for stateless communications)!",
                 "Dialog entry error",MB_OK|MB_ICONERROR);
      GotoDlgCtrl(GetDlgItem(IDC_JPIP_CHANNEL_TYPE));
      return;
    }
  EndDialog(IDOK);
}

/* ========================================================================== */
/*                                  kd_static                                 */
/* ========================================================================== */

/******************************************************************************/
/*                            kd_static::kd_static                            */
/******************************************************************************/

kd_static::kd_static()
{
  is_created = false;
  last_label = NULL;
  anchor_on_left = true;
}

/******************************************************************************/
/*                               kd_static::show                              */
/******************************************************************************/

void
  kd_static::show(const char *label, CWnd *view_wnd, kdu_coords pos,
                  kdu_dims view_dims)
{
  pos -= view_dims.pos;
  if (pos.x >= view_dims.size.x)
    pos.x = view_dims.size.x-1;
  if (pos.y >= view_dims.size.y)
    pos.y = view_dims.size.y;
  if (pos.x < 0)
    pos.x = 0;
  if (pos.y < 0)
    pos.y = 0;

  CRect rect;
  if (!is_created)
    {
      // Create with dummy dimensions
      rect.top = 0; rect.left = 0; rect.bottom = 16; rect.right = 64;
      Create(NULL,WS_CHILD | WS_BORDER,rect,view_wnd);
      is_created = true;
    }

  if (label != last_label)
    {
      last_label = label;
      SetWindowText(label);

      // Find dimensions of the displayed text
      CDC *dc = GetDC();
      SIZE text_size = dc->GetTextExtent(label,(int) strlen(label));
      ReleaseDC(dc);

      // Size window to fit text and display
      anchor_on_left = (pos.x <= (view_dims.size.x>>1));
      last_pos = pos;
      last_size.x = text_size.cx+4;
      last_size.y = text_size.cy+4;
      int anchor_x = (anchor_on_left)?pos.x:(pos.x-last_size.x+1);
      SetWindowPos(NULL,anchor_x,pos.y-last_size.y,last_size.x,last_size.y,
                   SWP_SHOWWINDOW);
      Invalidate();
    }
  else if (pos != last_pos)
    {
      last_pos = pos;
      if (anchor_on_left)
        {
          if ((pos.x+last_size.x) > view_dims.size.x)
            anchor_on_left = false;
        }
      else
        {
          if (pos.x < last_size.x)
            anchor_on_left = true;
        }
      int anchor_x = (anchor_on_left)?pos.x:(pos.x-last_size.x+1);
      SetWindowPos(NULL,anchor_x,pos.y-last_size.y,last_size.x,last_size.y,
                   SWP_SHOWWINDOW);
    }
}

/******************************************************************************/
/*                               kd_static::hide                              */
/******************************************************************************/

void
  kd_static::hide()
{
  if (!is_created)
    return;
  last_label = NULL;
  ShowWindow(SW_HIDE);
}


/* ========================================================================== */
/*                       CKdu_showApp Class Implementation                    */
/* ========================================================================== */

BEGIN_MESSAGE_MAP(CKdu_showApp, CWinApp)
	//{{AFX_MSG_MAP(CKdu_showApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_CLOSE, OnFileClose)
	ON_UPDATE_COMMAND_UI(ID_FILE_CLOSE, OnUpdateFileClose)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_VIEW_HFLIP, OnViewHflip)
	ON_UPDATE_COMMAND_UI(ID_VIEW_HFLIP, OnUpdateViewHflip)
	ON_COMMAND(ID_VIEW_VFLIP, OnViewVflip)
	ON_UPDATE_COMMAND_UI(ID_VIEW_VFLIP, OnUpdateViewVflip)
	ON_COMMAND(ID_VIEW_ROTATE, OnViewRotate)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ROTATE, OnUpdateViewRotate)
	ON_COMMAND(ID_VIEW_COUNTER_ROTATE, OnViewCounterRotate)
	ON_UPDATE_COMMAND_UI(ID_VIEW_COUNTER_ROTATE, OnUpdateViewCounterRotate)
	ON_COMMAND(ID_VIEW_ZOOM_OUT, OnViewZoomOut)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_OUT, OnUpdateViewZoomOut)
	ON_COMMAND(ID_VIEW_ZOOM_IN, OnViewZoomIn)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_IN, OnUpdateViewZoomIn)
	ON_COMMAND(ID_VIEW_RESTORE, OnViewRestore)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RESTORE, OnUpdateViewRestore)
	ON_COMMAND(ID_COMPONENT1, OnComponent1)
	ON_UPDATE_COMMAND_UI(ID_COMPONENT1, OnUpdateComponent1)
	ON_COMMAND(ID_MULTI_COMPONENT, OnMultiComponent)
	ON_UPDATE_COMMAND_UI(ID_MULTI_COMPONENT, OnUpdateMultiComponent)
	ON_COMMAND(ID_STATUS_TOGGLE, OnStatusToggle)
	ON_UPDATE_COMMAND_UI(ID_STATUS_TOGGLE, OnUpdateStatusToggle)
	ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
	ON_UPDATE_COMMAND_UI(ID_VIEW_REFRESH, OnUpdateViewRefresh)
	ON_COMMAND(ID_FILE_DISCONNECT, OnFileDisconnect)
	ON_UPDATE_COMMAND_UI(ID_FILE_DISCONNECT, OnUpdateFileDisconnect)
	ON_COMMAND(ID_COMPONENT_NEXT, OnComponentNext)
	ON_UPDATE_COMMAND_UI(ID_COMPONENT_NEXT, OnUpdateComponentNext)
	ON_COMMAND(ID_COMPONENT_PREV, OnComponentPrev)
	ON_UPDATE_COMMAND_UI(ID_COMPONENT_PREV, OnUpdateComponentPrev)
	ON_COMMAND(ID_JPIP_OPEN, OnJpipOpen)
	ON_COMMAND(ID_VIEW_METADATA, OnViewMetadata)
	ON_UPDATE_COMMAND_UI(ID_VIEW_METADATA, OnUpdateViewMetadata)
	ON_COMMAND(ID_IMAGE_NEXT, OnImageNext)
	ON_UPDATE_COMMAND_UI(ID_IMAGE_NEXT, OnUpdateImageNext)
	ON_COMMAND(ID_IMAGE_PREV, OnImagePrev)
	ON_UPDATE_COMMAND_UI(ID_IMAGE_PREV, OnUpdateImagePrev)
	ON_COMMAND(ID_COMPOSITING_LAYER, OnCompositingLayer)
	ON_UPDATE_COMMAND_UI(ID_COMPOSITING_LAYER, OnUpdateCompositingLayer)
	ON_COMMAND(ID_META_ADD, OnMetaAdd)
	ON_UPDATE_COMMAND_UI(ID_META_ADD, OnUpdateMetaAdd)
	//}}AFX_MSG_MAP
  ON_COMMAND(ID_VIEW_OPTIMIZESCALE, OnViewOptimizescale)
  ON_UPDATE_COMMAND_UI(ID_VIEW_OPTIMIZESCALE, OnUpdateViewOptimizescale)
  ON_COMMAND(ID_TRACK_NEXT, OnTrackNext)
  ON_UPDATE_COMMAND_UI(ID_TRACK_NEXT, OnUpdateTrackNext)
  ON_COMMAND(ID_TOOL_OVERVIEWWINDOW, &CKdu_showApp::OnToolOverviewwindow)
  END_MESSAGE_MAP()


/******************************************************************************/
/*                         CKdu_showApp::CKdu_showApp                         */
/******************************************************************************/

CKdu_showApp::CKdu_showApp()
{
  child_wnd = NULL;

  open_file_pathname[0] = temp_file_pathname[0] = '\0';
  open_filename = NULL;
  compositor = NULL;
  num_recommended_threads = kdu_get_num_processors();
  if (num_recommended_threads < 2)
    num_recommended_threads = 0;
  num_threads = num_recommended_threads;
  if (num_threads > 0)
    {
      thread_env.create();
      for (int k=1; k < num_threads; k++)
        if (!thread_env.add_thread())
          num_threads = k;
    }
  in_idle = false;
  processing_suspended = false;

  allow_seeking = true;
  error_level = 0;
  max_display_layers = 1<<16;
  transpose = vflip = hflip = false;
  min_rendering_scale = -1.0F;
  rendering_scale = 1.0F;
  max_components = max_codestreams = max_compositing_layer_idx = -1;
  single_component_idx = single_codestream_idx = single_layer_idx = 0;
  frame_idx = 0;
  frame_start = frame_end = 0.0;
  frame = NULL;
  frame_iteration = 0;
  frame_expander.reset();
  fit_scale_to_window = false;
  configuration_complete = false;

  jpip_progress = NULL;
  status_id = KDS_STATUS_LAYER_RES;

  //PLAY in_playmode = false;
  //playmode_repeat = false;
  //pushed_last_frame = false;
  //use_native_timing = true;
  //custom_fps = rate_multiplier = 1.0F;
  //max_queue_size = 2;
  //max_queue_period = 1.0; // Process up to 1 second ahead
  //playclock_base = 0;
  //playstart_instant = 0.0;
  //frame_time_offset = 0.0;
  //last_actual_time = last_nominal_time = -1.0;

  pixel_scale = 1;
  image_dims.pos = image_dims.size = kdu_coords(0,0);
  buffer_dims = view_dims = image_dims;
  view_centre_x = view_centre_y = 0.0F;
  view_centre_known = false;

  enable_focus_box = false;
  highlight_focus = true;
  focus_anchors_known = false;
  focus_centre_x = focus_centre_y = focus_size_x = focus_size_y = 0.0F;
  focus_pen.CreatePen(PS_DOT,0,0x000000FF);
  focus_box.pos = focus_box.size = kdu_coords(0,0);
  enable_region_posting = false;

  overlays_enabled = overlay_flashing_enabled = false;
  overlay_painting_intensity = overlay_painting_colour = 0;
  overlay_size_threshold = 1;
  metainfo_label = NULL;

  buffer = NULL;
  compatible_dc.CreateCompatibleDC(NULL);

  overview_buffer = NULL; // MJY_091508
  overview_compatible_dc.CreateCompatibleDC(NULL); // MJY_091508
  overviewmap = NULL; //MJY_

  strip_extent = kdu_coords(0,0);
  stripmap = NULL;
  stripmap_buffer = NULL;
  for (int i=0; i < 256; i++)
    {
      foreground_lut[i] = (kdu_byte)(40 + ((i*216) >> 8));
      background_lut[i] = (kdu_byte)((i*216) >> 8);
    }

  refresh_timer_id = 0;
  flash_timer_id = 0;
  last_transferred_bytes = 0;

  metashow = NULL;
  //PLAY playcontrol = NULL;
}

/******************************************************************************/
/*                        CKdu_showApp::~CKdu_showApp                         */
/******************************************************************************/

CKdu_showApp::~CKdu_showApp()
{
  if (jpip_progress != NULL)
    { delete jpip_progress; jpip_progress = NULL; }
  if (compositor != NULL)
    { delete compositor; compositor = NULL; }
  if (thread_env.exists())
    thread_env.destroy();
  if (metashow != NULL)
    delete metashow;
  //PLAYif (playcontrol != NULL)
  //  delete playcontrol;
  file_in.close();
  jpx_in.close();
  mj2_in.close();
  jp2_family_in.close();
  jpip_client.close();
  jpip_client.install_context_translator(NULL);
  if (stripmap != NULL)
    { DeleteObject(stripmap); stripmap = NULL; stripmap_buffer = NULL; }

  if ((temp_file_pathname[0] != '\0') && (open_file_pathname[0] != '\0'))
    {
      MoveFileEx(temp_file_pathname,open_file_pathname,
                 MOVEFILE_REPLACE_EXISTING);
      temp_file_pathname[0] = open_file_pathname[0] = '\0';
    }
}

/******************************************************************************/
/*                         CKdu_showApp::InitInstance                         */
/******************************************************************************/

BOOL CKdu_showApp::InitInstance()
{
  AfxEnableControlContainer();
#if _MFC_VER < 0x0500
#  ifdef _AFXDLL
     Enable3dControls(); // Call this when using MFC in a shared DLL
#  else
     Enable3dControlsStatic(); // Call this when linking to MFC statically
#  endif
#endif // _MFC_VER
  SetRegistryKey(_T("Kakadu"));
  LoadStdProfileSettings(4);
  settings.load_from_registry(this);
  char executable_path[_MAX_PATH+1];
  int path_len = GetModuleFileName(m_hInstance,executable_path,_MAX_PATH);
  if (path_len > 0)
    {
      executable_path[path_len] = '\0';
      register_protocols(executable_path);
    }
  frame_wnd = new CMainFrame;
  m_pMainWnd = frame_wnd;
  child_wnd = frame_wnd->get_child();
  child_wnd->set_app(this);
  frame_wnd->LoadFrame(IDR_MAINFRAME,
                   WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU |
                   WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZE ,
                   NULL,NULL);
  frame_wnd->ShowWindow(SW_SHOW);
  frame_wnd->UpdateWindow();
  frame_wnd->set_app(this);
  frame_wnd->DragAcceptFiles(TRUE);
  client_notifier.set_wnd(frame_wnd->m_hWnd);

  kdu_customize_errors(&err_formatter);
  kdu_customize_warnings(&warn_formatter);

  char *filename = m_lpCmdLine;
  while ((*filename != '\0') && ((*filename == ' ') || (*filename == '\t')))
    filename++;
  if (*filename != '\0')
    {
      if (*filename == '\"')
        {
          char *ch = strrchr(filename+1,'\"');
          if (ch != NULL)
            {
              filename++;
              *ch = '\0';
            }
        }
      open_file(filename);
    }
  return TRUE;
}

/******************************************************************************/
/*                          CKdu_showApp::close_file                          */
/******************************************************************************/

void
  CKdu_showApp::close_file()
{
  if (compositor != NULL)
    { delete compositor; compositor = NULL; }
  if (metashow != NULL)
    metashow->deactivate();
  file_in.close();
  jpx_in.close();
  mj2_in.close();
  jp2_family_in.close();
  jpip_client.close();
  jpip_client.install_context_translator(NULL);

  if ((temp_file_pathname[0] != '\0') && (open_file_pathname[0] != '\0'))
    {
      MoveFileEx(temp_file_pathname,open_file_pathname,
                 MOVEFILE_REPLACE_EXISTING);
      temp_file_pathname[0] = open_file_pathname[0] = '\0';
    }
  open_filename = NULL;

  composition_rules = jpx_composition(NULL);
  processing_suspended = false;
  fit_scale_to_window = false;
  frame_idx = 0;
  frame_start = frame_end = 0.0;
  frame = NULL;
  frame_iteration = 0;
  frame_expander.reset();
  configuration_complete = false;
  transpose = vflip = hflip = false;
  image_dims.pos = image_dims.size = kdu_coords(0,0);
  buffer_dims = view_dims = image_dims;
  view_centre_known = false;
  focus_anchors_known = false;
  enable_focus_box = false;
  overlays_enabled = overlay_flashing_enabled = false;
  overlay_painting_intensity = overlay_painting_colour = 0;
  buffer = NULL;
  if (refresh_timer_id != 0)
    m_pMainWnd->KillTimer(refresh_timer_id);
  if (flash_timer_id != 0)
    m_pMainWnd->KillTimer(flash_timer_id);
  refresh_timer_id = 0;
  flash_timer_id = 0;
  last_transferred_bytes = 0;
  if (child_wnd != NULL)
    {
      child_wnd->set_max_view_size(kdu_coords(20000,20000),pixel_scale);
      if (child_wnd->GetSafeHwnd() != NULL)
        child_wnd->Invalidate();
    }
  m_pMainWnd->SetWindowText("<no data>");
  //PLAYif (playcontrol != NULL)
  //  {
  //    playcontrol->OnClose();
  //    assert(playcontrol == NULL);
  //  }
  //stop_playmode();
}

/******************************************************************************/
/*                          CKdu_showApp::open_file                           */
/******************************************************************************/

void
  CKdu_showApp::open_file(char *filename)
{
  if (filename != NULL)
    close_file();
  assert(compositor == NULL);
  enable_focus_box = false;
  focus_box.size = kdu_coords(0,0);
  client_roi.init();
  processing_suspended = false;
  enable_region_posting = false;
  try {
      if (filename != NULL)
        {
          if ((((toupper(filename[0]) == (int) 'J') &&
                (toupper(filename[1]) == (int) 'P') &&
                (toupper(filename[2]) == (int) 'I') &&
                (toupper(filename[3]) == (int) 'P')) ||
               ((toupper(filename[0]) == (int) 'H') &&
                (toupper(filename[1]) == (int) 'T') &&
                (toupper(filename[2]) == (int) 'T') &&
                (toupper(filename[3]) == (int) 'P'))) &&
              (filename[4] == ':'))
            { // Open as an interactive client-server application
              char *prefix = filename + 5;
              while ((*prefix == '/') || (*prefix == '\\'))
                prefix++;
              char *fstart;
              for (fstart=prefix; *fstart != '\0'; fstart++)
                if ((*fstart == '/') || (*fstart == '\\'))
                  break;
              if (*fstart == '\0')
                { kdu_error e;
                  e << "Illegal JPIP request, \"" << filename
                    << "\".  General form is \"<prot>://<hostname>[:<port>]/"
                       "<resource>[?<query>]\"\nwhere <prot> is \"jpip\" or "
                       "\"http\" and forward slashes may be substituted "
                       "for back slashes if desired.";
                }
              *fstart = '\0';
              jpip_client.connect(prefix,settings.get_jpip_proxy(),
                                  fstart+1,settings.get_jpip_channel_type(),
                                   settings.get_jpip_cache());
              *fstart = '/';
              status_id = KDS_STATUS_CACHE;
              jpip_client.install_notifier(&client_notifier);
              cumulative_transmission_time = 0;
              last_transmission_start = 0;
              jp2_family_in.open(&jpip_client);
              if (metashow != NULL)
                metashow->activate(&jp2_family_in,false);
            }
          else
            { // Open as a local file
              status_id = KDS_STATUS_LAYER_RES;
              jp2_family_in.open(filename,allow_seeking);
			  //overview_jp2_family_in.open(filename, allow_seeking); //MJY_091608
              compositor = new kd_bitmap_compositor(&thread_env);
			  //overview_compositor = new kd_bitmap_compositor(); //MJY_091108
              if (jpx_in.open(&jp2_family_in,true) >= 0)
                { // We have a JP2/JPX-compatible file.
                  compositor->create(&jpx_in);
				  //overview_jpx_in.open(&overview_jp2_family_in, true); // MJY_09
				  //overview_compositor->create(&overview_jpx_in); // MJY_091108
                  if (metashow != NULL)
                    metashow->activate(&jp2_family_in,false);
                }
              else if (mj2_in.open(&jp2_family_in,true) >= 0)
                {
                  compositor->create(&mj2_in);
                  if (metashow != NULL)
                    metashow->activate(&jp2_family_in,false);
                }
              else
                { // Incompatible with JP2/JPX or MJ2. Try opening as raw stream
                  jp2_family_in.close();
                  file_in.open(filename,allow_seeking);
                  compositor->create(&file_in);
                }
              compositor->set_error_level(error_level);
            }
        }
      else if (jpip_client.is_active())
        { // See if we can open the client yet
          assert((compositor == NULL) && jp2_family_in.exists());
          bool bin0_complete = false;
          if (jpip_client.get_databin_length(KDU_META_DATABIN,0,0,
                                             &bin0_complete) > 0)
            { // Open as a JP2/JPX family file
              if (metashow != NULL)
                metashow->update_tree(bin0_complete);
              if (jpx_in.open(&jp2_family_in,false))
                {
                  compositor = new kd_bitmap_compositor(&thread_env);
                  compositor->create(&jpx_in);
                }
              jpip_client.install_context_translator(&jpx_client_translator);
            }
          else if (bin0_complete)
            { // Open as a raw file
              if (metashow != NULL)
                metashow->update_tree(true);
              assert(!jpx_in.exists());
              bool hdr_complete;
              jpip_client.get_databin_length(KDU_MAIN_HEADER_DATABIN,0,0,
                                             &hdr_complete);
              if (hdr_complete)
                {
                  jpip_client.set_read_scope(KDU_MAIN_HEADER_DATABIN,0,0);
                  compositor = new kd_bitmap_compositor(&thread_env);
                  compositor->create(&jpip_client);
                }
            }
          if (compositor == NULL)
            {
              if (!jpip_client.is_alive())
                { kdu_error e; e << "Unable to recover sufficient information "
                  "from remote server (or a local cache) to open the image."; }
              return; // Come back later
            }
          compositor->set_error_level(error_level);
          client_roi.init();
          enable_region_posting = true;
        }
    }
  catch (int)
    {
      close_file();
    }

  if (compositor != NULL)
    {
      max_display_layers = 1<<16;
      transpose = vflip = hflip = false;
      min_rendering_scale = -1.0F;
      rendering_scale = 1.0F;
      single_component_idx = -1;
      single_codestream_idx = 0;
      max_components = -1;
      frame_idx = 0;
      frame_start = frame_end = 0.0;
      num_frames = -1;
      frame = NULL;
      composition_rules = jpx_composition(NULL);

      if (jpx_in.exists())
        {
          max_codestreams = -1;  // Unknown as yet
          max_compositing_layer_idx = -1; // Unknown as yet
          single_layer_idx = -1; // Try starting in composite frame mode
        }
      else if (mj2_in.exists())
        {
          single_layer_idx = 0; // Start in composed single layer (track) mode
        }
      else
        {
          max_codestreams = 1;
          max_compositing_layer_idx = 0;
          num_frames = 0;
          single_layer_idx = 0; // Start in composed single layer mode
        }

      overlays_enabled = true; // Default starting position
      overlay_flashing_enabled = true;
      overlay_painting_intensity = overlay_painting_colour = 0;
      compositor->configure_overlays(overlays_enabled,overlay_size_threshold,
           (overlay_painting_colour<<8)+(overlay_painting_intensity & 0xFF));
	  

   //   invalidate_configuration();      
	  //fit_scale_to_window = true;
	  //need_overview_map = true; // MJY_091908
   //   image_dims = buffer_dims = view_dims = kdu_dims();
   //   buffer = NULL;
	  //initialize_overview_regions();
	  ////OnToolOverviewwindow();

	  invalidate_configuration();
	  fit_scale_to_window = true;
	  need_overview_map = true; // MJY_091908
	  first_open = true; //MJY_101308
      image_dims = buffer_dims = view_dims = kdu_dims();
      buffer = NULL;
	  initialize_regions();

    }

  if (filename != NULL)
    {
      char title[256];
      strcpy(title,"kdu_show: ");
      char *delim = strrchr(filename,'\\');
      if ((delim==NULL) || (delim[1]=='\0') || jpip_client.is_active())
        delim = filename-1;
      strncat(title,delim+1,255-strlen(title));
      m_pMainWnd->SetWindowText(title);
      int path_len =
        GetFullPathName(filename,MAX_PATH,open_file_pathname,&open_filename);
      if (path_len >= MAX_PATH)
        path_len--;
      open_file_pathname[path_len] = '\0';
      if (open_filename > open_file_pathname)
        {
          char sep = open_filename[-1];
          open_filename[-1] = '\0';
          settings.set_open_save_dir(open_file_pathname);
          open_filename[-1] = sep;
        }
    }

}

/******************************************************************************/
/*                        CKdu_showApp::increase_scale                        */
/******************************************************************************/

float
  CKdu_showApp::increase_scale(float from_scale)
{
  float min_scale = from_scale*1.30F;
  float max_scale = from_scale*2.70F;
  if (compositor == NULL)
    return min_scale;
  kdu_dims region_basis;
  if (configuration_complete)
    region_basis = (enable_focus_box)?focus_box:view_dims;
  return compositor->find_optimal_scale(region_basis,from_scale,
                                        min_scale,max_scale);
}

/******************************************************************************/
/*                        CKdu_showApp::decrease_scale                        */
/******************************************************************************/

float
  CKdu_showApp::decrease_scale(float from_scale)
{
  float max_scale = from_scale/1.30F;
  float min_scale = from_scale/2.70F;
  if (min_rendering_scale > 0.0F)
    {
      if (min_scale < min_rendering_scale)
        min_scale = min_rendering_scale;
      if (max_scale < min_rendering_scale)
        max_scale = min_rendering_scale;
    }
  if (compositor == NULL)
    return max_scale;
  kdu_dims region_basis;
  if (configuration_complete)
    region_basis = (enable_focus_box)?focus_box:view_dims;
  float new_scale = compositor->find_optimal_scale(region_basis,from_scale,
                                                   min_scale,max_scale);
  if (new_scale > max_scale)
    min_rendering_scale = new_scale; // This is as small as we can go
  return new_scale;
}

/******************************************************************************/
/*                       CKdu_showApp::change_frame_idx                       */
/******************************************************************************/

void
  CKdu_showApp::change_frame_idx(int new_frame_idx)
  /* Note carefully that, on entry to this function, only the `frame_start'
     time can be relied upon.  The function sets `frame_end' from scratch,
     rather than basing the newly calculated value on a previous one. */
{
  if (new_frame_idx < 0)
    new_frame_idx = 0;
  if ((num_frames >= 0) && (new_frame_idx >= (num_frames-1)))
    new_frame_idx = num_frames-1;
  if ((new_frame_idx == frame_idx) && (frame_end > frame_start))
    return; // Nothing to do

  if (composition_rules.exists() && (frame != NULL))
    {
      int num_instructions, duration, repeat_count, delta;
      bool is_persistent;

      if (new_frame_idx == 0)
        {
          frame = composition_rules.get_next_frame(NULL);
          frame_iteration = 0;
          frame_idx = 0;
          frame_start = 0.0;
          composition_rules.get_frame_info(frame,num_instructions,duration,
                                           repeat_count,is_persistent);
          frame_end = frame_start + 0.001*duration;
        }

      while (frame_idx < new_frame_idx)
        {
          composition_rules.get_frame_info(frame,num_instructions,duration,
                                           repeat_count,is_persistent);
          delta = repeat_count - frame_iteration;
          if (delta > 0)
            {
              if ((frame_idx+delta) > new_frame_idx)
                delta = new_frame_idx - frame_idx;
              frame_iteration += delta;
              frame_idx += delta;
              frame_start += delta * 0.001*duration;
              frame_end = frame_start + 0.001*duration;
            }
          else
            {
              jx_frame *new_frame = composition_rules.get_next_frame(frame);
              frame_end = frame_start + 0.001*duration; // Just in case
              if (new_frame == NULL)
                {
                  num_frames = frame_idx+1;
                  break;
                }
              else
                { frame = new_frame; frame_iteration=0; }
              composition_rules.get_frame_info(frame,num_instructions,duration,
                                               repeat_count,is_persistent);
              frame_start = frame_end;
              frame_end += 0.001*duration;
              frame_idx++;
            }
        }

      while (frame_idx > new_frame_idx)
        {
          composition_rules.get_frame_info(frame,num_instructions,duration,
                                           repeat_count,is_persistent);
          if (frame_iteration > 0)
            {
              delta = frame_idx - new_frame_idx;
              if (delta > frame_iteration)
                delta = frame_iteration;
              frame_iteration -= delta;
              frame_idx -= delta;
              frame_start -= delta * 0.001*duration;
              frame_end = frame_start + 0.001*duration;
            }
          else
            {
              frame = composition_rules.get_prev_frame(frame);
              assert(frame != NULL);
              composition_rules.get_frame_info(frame,num_instructions,duration,
                                               repeat_count,is_persistent);
              frame_iteration = repeat_count;
              frame_idx--;
              frame_start -= 0.001*duration;
              frame_end = frame_start + 0.001*duration;
            }
        }
    }
  else if (mj2_in.exists() && (single_layer_idx >= 0))
    {
      mj2_video_source *track =
        mj2_in.access_video_track((kdu_uint32)(single_layer_idx+1));
      if (track == NULL)
        return;
      frame_idx = new_frame_idx;
      track->seek_to_frame(new_frame_idx);
      frame_start = ((double) track->get_frame_instant()) /
        ((double) track->get_timescale());
      frame_end = frame_start + ((double) track->get_frame_period()) /
        ((double) track->get_timescale());
    }
}

/******************************************************************************/
/*                   CKdu_showApp::invalidate_configuration                   */
/******************************************************************************/

void
  CKdu_showApp::invalidate_configuration()
{
  configuration_complete = false;
  max_components = -1; // Changed config may have different num image components
  buffer = NULL;

  if (compositor != NULL)
    compositor->remove_compositing_layer(-1,false);
}




/******************************************************************************/
/*                      CKdu_showApp::initialize_regions                      */
/******************************************************************************/

void
  CKdu_showApp::initialize_regions()
{


  if (child_wnd != NULL)
    child_wnd->cancel_focus_drag();

  bool first_time_through = fit_scale_to_window;
        // Use this flag later to determine if we need to fire off the play
        // controls.

  // Reset the buffer and view dimensions to zero size.
  buffer = NULL;
  frame_expander.reset();
  while (!configuration_complete)
    { // Install configuration
      try {
 TRACE("inside configuratoin_complete is not true\n");
          if (single_component_idx >= 0)
            { // Check for valid codestream index before opening
              if (jpx_in.exists())
                {
                  int count=max_codestreams;
                  if ((count < 0) && !jpx_in.count_codestreams(count))
                    { // Actual number not yet known, but have at least `count'
                      if (single_codestream_idx >= count)
                        return; // Come back later once more data is in cache
                    }
                  else
                    { // Number of codestreams is now known
                      max_codestreams = count;
                      if (single_codestream_idx >= max_codestreams)
                        single_codestream_idx = max_codestreams-1;
                    }
                }
              else if (mj2_in.exists())
                {
                  kdu_uint32 trk;
                  int frm, fld;
                  if (!mj2_in.find_stream(single_codestream_idx,trk,frm,fld))
                    return; // Come back later once more data is in cache
                  if (trk == 0)
                    {
                      int count;
                      if (mj2_in.count_codestreams(count))
                        max_codestreams = count;
                      single_codestream_idx = count-1;
                    }
                }
              else
                { // Raw codestream
                  single_codestream_idx = 0;
                }
              int idx = compositor->set_single_component(single_codestream_idx,
                                        single_component_idx,
                                        KDU_WANT_CODESTREAM_COMPONENTS);
              if (idx < 0)
                { // Cannot open codestream yet; waiting for cache
                  update_client_window_of_interest();
                  return;
                }
              kdrc_stream *str =
                compositor->get_next_codestream(NULL,true,false);
              kdu_codestream codestream = compositor->access_codestream(str);
              codestream.apply_input_restrictions(0,0,0,0,NULL);
              max_components = codestream.get_num_components();
              if (single_component_idx >= max_components)
                single_component_idx = max_components-1;
            }
          else if (single_layer_idx >= 0)
            { // Check for valid compositing layer index before opening
              if (jpx_in.exists())
                {
                  frame_idx = 0;
                  frame_start = frame_end = 0.0;
                  int count=max_compositing_layer_idx+1;
                  if ((count <= 0) && !jpx_in.count_compositing_layers(count))
                    { // Actual number not yet known, but have at least `count'
                      if (single_layer_idx >= count)
                        return; // Come back later once more data is in cache
                    }
                  else
                    { // Number of compositing layers is now known
                      max_compositing_layer_idx = count-1;
                      if (single_layer_idx > max_compositing_layer_idx)
                        single_layer_idx = max_compositing_layer_idx;
                    }
                }
              else if (mj2_in.exists())
                {
                  int track_type;
                  kdu_uint32 track_idx = (kdu_uint32)(single_layer_idx+1);
                  bool loop_check = false;
                  mj2_video_source *track = NULL;
                  while (track == NULL)
                    {
                      track_type = mj2_in.get_track_type(track_idx);
                      if (track_type == MJ2_TRACK_IS_VIDEO)
                        {
                          track = mj2_in.access_video_track(track_idx);
                          if (track == NULL)
                            return; // Come back later once more data in cache
                          num_frames = track->get_num_frames();
                          if (num_frames == 0)
                            { // Skip over track with no frames
                              track_idx = mj2_in.get_next_track(track_idx);
                              continue;
                            }
                        }
                      else if (track_type == MJ2_TRACK_NON_EXISTENT)
                        { // Go back to the first track
                          if (loop_check)
                            { kdu_error e; e << "Motion JPEG2000 source "
                              "has no video tracks with any frames!"; }
                          loop_check = true;
                          track_idx = mj2_in.get_next_track(0);
                        }
                      else if (track_type == MJ2_TRACK_MAY_EXIST)
                        return; // Come back later once more data is in cache
                      else
                        { // Go to the next track
                          track_idx = mj2_in.get_next_track(track_idx);
                        }
                      single_layer_idx = ((int) track_idx) - 1;
                    }
                  if (frame_idx >= num_frames)
                    frame_idx = num_frames-1;
                  change_frame_idx(frame_idx);
                }
              else
                { // Raw codestream
                  single_layer_idx = 0;
                }
// TRACE("single_layer_idx = %d frame_idx = %d\n", single_layer_idx, frame_idx);
              if (!compositor->add_compositing_layer(single_layer_idx,
                                                     kdu_dims(),kdu_dims(),
                                                     false,false,false,
                                                     frame_idx,0))
                { // Cannot open compositing layer yet; waiting for cache
                  if (jpip_client.is_one_time_request())
                    { // See if request is compatible with opening a layer
                      if (!check_one_time_request_compatibility())         
                        continue; // Mode changed to single component
                    }
                  else
                    update_client_window_of_interest();
                  return;
                }
			  //TRACE ("single_layer_idx = %d\n", single_layer_idx);
            }
          else
            { // Attempt to open frame
              if (num_frames == 0)
                { // Downgrade to single layer mode
                  single_layer_idx = 0;
                  frame_idx = 0;
                  frame_start = frame_end = 0.0;
                  frame = NULL;
                  continue;
                }
              assert(jpx_in.exists());
              if (frame == NULL)
                {
                  if (!composition_rules)
                    composition_rules = jpx_in.access_composition();
                  if (!composition_rules)
                    return; // Cannot open composition rules yet; wait for cache
                  assert((frame_idx == 0) && (frame_iteration == 0));
                  frame = composition_rules.get_next_frame(NULL);
                  if (frame == NULL)
                    { // Downgrade to single layer mode
                      single_layer_idx = 0;
                      frame_idx = 0;
                      frame_start = frame_end = 0.0;
                      num_frames = 0;
                      continue;
                    }
                }

              if (!frame_expander.construct(&jpx_in,frame,frame_iteration,true))
                {
                  if (jpip_client.is_one_time_request())
                    { // See if request is compatible with opening a frame
                      if (!check_one_time_request_compatibility())         
                        continue; // Mode changed to single layer or component
                    }
                  else
                    update_client_window_of_interest();
                  return; // Can't open all frame members yet; waiting for cache
                }
                  
              if (frame_expander.get_num_members() == 0)
                { // Requested frame does not exist
                  if ((num_frames < 0) || (num_frames > frame_idx))
                    num_frames = frame_idx;
                  if (frame_idx == 0)
                    { kdu_error e; e << "Cannot render even the first "
                      "composited frame described in the JPX composition "
                      "box due to unavailability of the required compositing "
                      "layers in the original file.  Viewer will fall back "
                      "to single layer rendering mode."; }
                  change_frame_idx(frame_idx-1);
                  continue; // Loop around to try again
                }

              //PLAYcompositor->set_frame(&frame_expander);
            }
          if (fit_scale_to_window && jpip_client.is_one_time_request() &&
              !check_one_time_request_compatibility())
            { // Check we are opening the image in the intended manner
              compositor->remove_compositing_layer(-1,false);
              continue; // Open again in different mode
            }
          compositor->set_max_quality_layers(max_display_layers);
          compositor->cull_inactive_layers(3); // Really an arbitrary amount of
                                               // culling in this demo app.
          configuration_complete = true;
          min_rendering_scale=-1.0F; // Need to discover from scratch each time
		}
      catch (int) { // Try downgrading to a more primitive viewing mode
          if ((single_component_idx >= 0) ||
              !(jpx_in.exists() || mj2_in.exists()))
            { // Already in single component mode; nothing more primitive exists
              close_file();
              return;
            }
          else if (single_layer_idx >= 0)
            { // Downgrade from single layer mode to single component mode
              single_component_idx = 0;
              single_codestream_idx = 0;
              single_layer_idx = -1;
              if (enable_region_posting)
                update_client_window_of_interest();
            }
          else
            { // Downgrade from frame animation mode to single layer mode
              frame = NULL;
              num_frames = 0;
              frame_idx = 0;
              frame_start = frame_end = 0.0;
              single_layer_idx = 0;
            }
        }
    }


  // Get size at current scale, possibly adjusting the scale if this is the
  // first time through

  float new_rendering_scale = rendering_scale;
  TRACE("before while loop %5.2f \n", rendering_scale);
  kdu_dims new_image_dims = image_dims;
  try {
      bool found_valid_scale=false;
      while (fit_scale_to_window || !found_valid_scale )
        {
          if ((min_rendering_scale > 0.0F) &&
              (new_rendering_scale < min_rendering_scale))
            new_rendering_scale = min_rendering_scale;
		  TRACE("inside while loop new rendering scale %5.2f\n", new_rendering_scale);
          compositor->set_scale(transpose,vflip,hflip,new_rendering_scale);
          found_valid_scale = 
            compositor->get_total_composition_dims(new_image_dims);
          if (!found_valid_scale)
            { // Increase scale systematically before trying again
              int invalid_scale_code = compositor->check_invalid_scale_code();
              if (invalid_scale_code & KDU_COMPOSITOR_SCALE_TOO_SMALL)
                {
                  min_rendering_scale = increase_scale(new_rendering_scale);
                  if (new_rendering_scale > 1000.0F)
                    {
                      if (single_component_idx >= 0)
                        { kdu_error e;
                          e << "Cannot display the image.  Seems to "
                          "require some non-trivial scaling.";
                        }
                      else
                        {
                          { kdu_warning w; w << "Cannot display the image.  "
                            "Seems to require some non-trivial scaling.  "
                            "Downgrading to single component mode.";
                          }
                          single_component_idx = 0;
                          invalidate_configuration();
                          if (compositor->set_single_component(0,0,
                                           KDU_WANT_CODESTREAM_COMPONENTS) < 0)
                            return;
                          configuration_complete = true;
                        }
                    }
                }
              else if ((invalid_scale_code & KDU_COMPOSITOR_CANNOT_FLIP) &&
                       (vflip || hflip))
                {
                  kdu_warning w; w << "This image cannot be decompressed "
                    "with the requested geometry (horizontally or vertically "
                    "flipped), since it employs a packet wavelet transform "
                    "in which horizontally (resp. vertically) high-pass "
                    "subbands are further decomposed in the horizontal "
                    "(resp. vertical) direction.  Only transposed decoding "
                    "will be permitted.";
                  vflip = hflip = false;
                }
              else
                {
                  if (single_component_idx >= 0)
                    { kdu_error e;
                      e << "Cannot display the image.  Unexplicable error "
                        "code encountered in call to "
                        "`kdu_region_compositor::get_total_composition_dims'.";
                    }
                  else
                    {
                      { kdu_warning w; w << "Cannot display the image.  "
                        "Seems to require some incompatible set of geometric "
                        "manipulations for the various composed codestreams.";
                      }
                      single_component_idx = 0;
                      invalidate_configuration();
                      if (compositor->set_single_component(0,0,
                                       KDU_WANT_CODESTREAM_COMPONENTS) < 0)
                        return;
                      configuration_complete = true;
                    }
                }
            }
          else if (fit_scale_to_window)
            {
              kdu_coords max_tgt_size =
                kdu_coords(700/pixel_scale,700/pixel_scale);
              if (jpip_client.is_one_time_request() &&
                  jpip_client.get_window_in_progress(&tmp_roi) &&
                  (tmp_roi.resolution.x > 0) && (tmp_roi.resolution.y > 0))
                { // Roughly fit scale to match the resolution
                  max_tgt_size = tmp_roi.resolution;
                  max_tgt_size.x += (3*max_tgt_size.x)/4;
                  max_tgt_size.y += (3*max_tgt_size.y)/4;
                  if (!tmp_roi.region.is_empty())
                    { // Configure view and focus box based on one-time request
                      focus_centre_x = (((float) tmp_roi.region.pos.x) +
                                        0.5F*((float) tmp_roi.region.size.x)) /
                                       ((float) tmp_roi.resolution.x);
                      focus_centre_y = (((float) tmp_roi.region.pos.y) +
                                        0.5F*((float) tmp_roi.region.size.y)) /
                                       ((float) tmp_roi.resolution.y);
                      focus_size_x = ((float) tmp_roi.region.size.x) /
                                     ((float) tmp_roi.resolution.x);
                      focus_size_y = ((float) tmp_roi.region.size.y) /
                                     ((float) tmp_roi.resolution.y);
                      focus_codestream = focus_layer = -1;
                      if (single_component_idx >= 0)
                        focus_codestream = single_codestream_idx;
                      else if (single_layer_idx >= 0)
                        focus_layer = single_layer_idx;
                      enable_focus_box = true;
                      view_centre_x = focus_centre_x;
                      view_centre_y = focus_centre_y;
                      view_centre_known = true;
                    }
                }
              float max_x = new_rendering_scale *
                ((float) max_tgt_size.x) / ((float) new_image_dims.size.x);
              float max_y = new_rendering_scale *
                ((float) max_tgt_size.y) / ((float) new_image_dims.size.y);
              while (((min_rendering_scale < 0.0) ||
                      (new_rendering_scale > min_rendering_scale)) &&
                     ((new_rendering_scale > max_x) ||
                      (new_rendering_scale > max_y)))
                {
					TRACE("decresing...scale...\n");
                  new_rendering_scale = decrease_scale(new_rendering_scale);
                  found_valid_scale = false;
                }
              fit_scale_to_window = false;
            }
			TRACE("fit_scale_to_window %d found_valid_scale %d\n", fit_scale_to_window, found_valid_scale);
		} // end of while
    }
  catch (int) { // Some error occurred while parsing code-streams
      close_file();
      return;
    }

  // Install the dimensioning parameters we just found, checking to see
  // if the window needs to be resized.
  rendering_scale = new_rendering_scale;
  TRACE("rendering_scale %5.2f\n", new_rendering_scale);

  if ((image_dims == new_image_dims) && (!view_dims.is_empty()) &&
      (!buffer_dims.is_empty()))
    { // No need to resize the window

      compositor->set_buffer_surface(buffer_dims);
	  TRACE("WHEN we set buffer surface.....................\n");
      buffer = compositor->get_composition_bitmap(buffer_dims);
 	  TRACE("buffer_dims %d %d %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y, buffer_dims.size.x, buffer_dims.size.y);
   	  TRACE("image_dims %d %d %d %d\n", image_dims.pos.x, image_dims.pos.y, image_dims.size.x, image_dims.size.y);
      if (adjust_focus_anchors())
        update_focus_box();
    }
  else
    { // Send a message to the child view window identifying the
      // maximum allowable image dimensions.  We expect to hear back (possibly
      // after some windows message processing) concerning
      // the actual view dimensions via the `set_view_size' function.
	TRACE("CHILD INVALIDATE...............\n");
      view_dims = buffer_dims = kdu_dims();
      image_dims = new_image_dims;
      if (adjust_focus_anchors())
        { view_centre_x = focus_centre_x; view_centre_y = focus_centre_y; }

      child_wnd->set_max_view_size(image_dims.size,pixel_scale);
	  child_wnd->Invalidate();

    }

  //TRACE("buffer_dims size %d %d\n", buffer_dims.size.x, buffer_dims.size.y);
  //TRACE("buffer_dims pos %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y);
  //TRACE("image_dims size %d %d\n", image_dims.size.x, image_dims.size.y);
  //TRACE("image_dims pos %d %d\n", image_dims.pos.x, image_dims.pos.y);
  //TRACE("view_dims size %d %d\n", view_dims.size.x, view_dims.size.y);
  //TRACE("view_dims pos %d %d\n", view_dims.pos.x, view_dims.pos.y);

// MJY_092408
  {
	  CRect	rect;
	  child_wnd->GetClientRect(&rect);

	  x_offset = y_offset = 0;
	  if (image_dims.size.x < rect.right)
		  x_offset = rect.right/2 - image_dims.size.x/2;
	  if (image_dims.size.y < rect.bottom)
		  y_offset = rect.bottom/2 - image_dims.size.y/2;
  } // MJY



  update_client_window_of_interest();
  //PLAYif (in_playmode)
  //  { // See if we need to turn off play mode.
  //    if ((single_component_idx >= 0) ||
  //        ((single_layer_idx >= 0) && jpx_in.exists()))
  //      stop_playmode();
  //  }
  display_status();




}

/******************************************************************************/
/*                      CKdu_showApp::suspend_processing                      */
/******************************************************************************/

void
  CKdu_showApp::suspend_processing(bool suspend)
{
  processing_suspended = suspend;
}

/******************************************************************************/
/*                       CKdu_showApp::refresh_display                        */
/******************************************************************************/

void
  CKdu_showApp::refresh_display()
{
	if ((compositor == NULL)) //PLAY || in_playmode)
    return;
  if (configuration_complete && !compositor->refresh())
    { // Can no longer trust buffer surfaces
      kdu_dims new_image_dims;
      bool have_valid_scale = 
        compositor->get_total_composition_dims(new_image_dims);
      if ((!have_valid_scale) || (new_image_dims != image_dims))
        initialize_regions();
      else
        buffer = compositor->get_composition_bitmap(buffer_dims);
    }
  if (metashow != NULL)
    {
      bool bin0_complete = true;
      if (jpip_client.is_active())
        jpip_client.get_databin_length(KDU_META_DATABIN,0,0,&bin0_complete);
      metashow->update_tree(bin0_complete);
    }
  if (refresh_timer_id != 0)
    { // Kill outstanding requests for a delayed refresh.
      m_pMainWnd->KillTimer(refresh_timer_id);
      refresh_timer_id = 0;
    }
}

/******************************************************************************/
/*                       CKdu_showApp::flash_overlays                         */
/******************************************************************************/

void
  CKdu_showApp::flash_overlays()
{
  if (flash_timer_id != 0)
    { // Kill outstanding requests for overlay flashing
      m_pMainWnd->KillTimer(flash_timer_id);
      flash_timer_id = 0;
    }
  if (!(overlays_enabled && overlay_flashing_enabled && (compositor != NULL)))
    return;
  overlay_painting_colour++;
  compositor->configure_overlays(overlays_enabled,overlay_size_threshold,
    (overlay_painting_colour<<8)+(overlay_painting_intensity & 0xFF));
}

/******************************************************************************/
/*               CKdu_showApp::convert_region_to_focus_anchors                */
/******************************************************************************/

void
  CKdu_showApp::convert_region_to_focus_anchors(kdu_dims region, kdu_dims ref)
{
  region &= ref;
  if (!region)
    {
      focus_anchors_known = enable_focus_box = false;
      return;
    }
  focus_centre_x =
    (float)(region.pos.x + region.size.x/2 - ref.pos.x) / ((float) ref.size.x);
  focus_centre_y =
    (float)(region.pos.y + region.size.y/2 - ref.pos.y) / ((float) ref.size.y);
  focus_size_x =
    ((float) region.size.x) / ((float) ref.size.x);
  focus_size_y =
    ((float) region.size.y) / ((float) ref.size.y);
  focus_anchors_known = true;
}

/******************************************************************************/
/*               CKdu_showApp::convert_focus_anchors_to_region                */
/******************************************************************************/

void
  CKdu_showApp::convert_focus_anchors_to_region(kdu_dims &region, kdu_dims ref)
{
  region.size.x = (int)(0.5F + ref.size.x * focus_size_x);
  region.size.y = (int)(0.5F + ref.size.y * focus_size_y);
  if (region.size.x <= 0)
    region.size.x = 1;
  if (region.size.y <= 0)
    region.size.y = 1;
  region.pos.x = (int)(0.5F + ref.pos.x +
    (focus_centre_x-0.5F*focus_size_x) * ref.size.x);
  region.pos.y = (int)(0.5F + ref.pos.y +
    (focus_centre_y-0.5F*focus_size_y) * ref.size.y);
}

/******************************************************************************/
/*                     CKdu_showApp::adjust_focus_anchors                     */
/******************************************************************************/

bool
  CKdu_showApp::adjust_focus_anchors()
{
  if (image_dims.is_empty() || !(focus_anchors_known && configuration_complete))
    return false;
  if (focus_codestream >= 0)
    {
      if (single_component_idx >= 0)
        return false; // No change in image type
      kdu_dims stream_region, stream_dims =
        compositor->find_codestream_region(focus_codestream,0,false);
      if (stream_dims.is_empty())
        { // Cannot map focus across from unused codestream
          focus_anchors_known = enable_focus_box = false;
        }
      else
        {
          convert_focus_anchors_to_region(stream_region,stream_dims);
          convert_region_to_focus_anchors(stream_region,image_dims);
          focus_codestream = -1;
          focus_layer = (single_layer_idx >= 0)?single_layer_idx:-1;
        }
    }
  else if (focus_layer >= 0)
    {
      if (single_layer_idx >= 0)
        return false; // Adjustments required only when focus was generated for
                      // a more primitive image type (map layer to frame)
      kdu_dims layer_region, layer_dims =
        compositor->find_layer_region(focus_layer,0,false);
      if (layer_dims.is_empty())
        { // Cannot map focus across from unused compositing layer
          focus_anchors_known = enable_focus_box = false;
        }
      else
        {
          convert_focus_anchors_to_region(layer_region,layer_dims);
          convert_region_to_focus_anchors(layer_region,image_dims);
          focus_layer = -1;
        }
    }
  else
    return false; // Adjustments required only when focus was generated for
                  // a more primitive image type (frames are the most advanced)
  return true;
}


/******************************************************************************/
/*                    CKdu_showApp::calculate_view_anchors                    */
/******************************************************************************/

void
  CKdu_showApp::calculate_view_anchors()
{
  if ((buffer == NULL) || !image_dims)
    return;
  view_centre_known = true;
  view_centre_x =
    (float)(view_dims.pos.x + view_dims.size.x/2 - image_dims.pos.x) /
      ((float) image_dims.size.x);
  view_centre_y =
    (float)(view_dims.pos.y + view_dims.size.y/2 - image_dims.pos.y) /
      ((float) image_dims.size.y);
  if (enable_focus_box)
    convert_region_to_focus_anchors(focus_box,image_dims);
  focus_codestream = focus_layer = -1;
  if (single_component_idx >= 0)
    focus_codestream = single_codestream_idx;
  else if (single_layer_idx >= 0)
    focus_layer = single_layer_idx;
}

/******************************************************************************/
/*                        CKdu_showApp::set_view_size                         */
/******************************************************************************/

void
  CKdu_showApp::set_view_size(kdu_coords size)
{
  if ((compositor == NULL) || !configuration_complete)
    return;
  if (child_wnd != NULL)
    child_wnd->cancel_focus_drag();

  // Set view region to the largest subset of the image region consistent with
  // the size of the new viewing region.
  kdu_dims new_view_dims = view_dims;
  new_view_dims.size = size;
  if (view_centre_known)
    {
      new_view_dims.pos.x = image_dims.pos.x - (size.x / 2) +
        (int) floor(0.5 + image_dims.size.x*view_centre_x);
      new_view_dims.pos.y = image_dims.pos.y - (size.y / 2) +
        (int) floor(0.5 + image_dims.size.y*view_centre_y);
      view_centre_known = false;
    }
  if (new_view_dims.pos.x < image_dims.pos.x)
    new_view_dims.pos.x = image_dims.pos.x;
  if (new_view_dims.pos.y < image_dims.pos.y)
    new_view_dims.pos.y = image_dims.pos.y;
  kdu_coords view_lim = new_view_dims.pos + new_view_dims.size;
  kdu_coords image_lim = image_dims.pos + image_dims.size;
  if (view_lim.x > image_lim.x)
    new_view_dims.pos.x -= view_lim.x-image_lim.x;
  if (view_lim.y > image_lim.y)
    new_view_dims.pos.y -= view_lim.y-image_lim.y;
  new_view_dims &= image_dims;
  bool need_redraw = new_view_dims.pos != view_dims.pos;
  view_dims = new_view_dims;

  // Get preferred minimum dimensions for the new buffer region.
  size = view_dims.size;
  //PLAYif (!in_playmode)
  //  { // A small boundary minimizes impact of scrolling
  //    size.x += (size.x>>4)+100;
  //    size.y += (size.y>>4)+100;
  //  }

  // Make sure buffered region is no larger than image
  if (size.x > image_dims.size.x)
    size.x = image_dims.size.x;
  if (size.y > image_dims.size.y)
    size.y = image_dims.size.y;
  kdu_dims new_buffer_dims;
  new_buffer_dims.size = size;
  new_buffer_dims.pos = buffer_dims.pos;

  // Make sure the buffer region is contained within the image
  kdu_coords buffer_lim = new_buffer_dims.pos + new_buffer_dims.size;
  if (buffer_lim.x > image_lim.x)
    new_buffer_dims.pos.x -= buffer_lim.x-image_lim.x;
  if (buffer_lim.y > image_lim.y)
    new_buffer_dims.pos.y -= buffer_lim.y-image_lim.y;
  if (new_buffer_dims.pos.x < image_dims.pos.x)
    new_buffer_dims.pos.x = image_dims.pos.x;
  if (new_buffer_dims.pos.y < image_dims.pos.y)
    new_buffer_dims.pos.y = image_dims.pos.y;
  assert(new_buffer_dims == (new_buffer_dims & image_dims));

  // See if the buffered region includes any new locations at all.
  if ((new_buffer_dims.pos != buffer_dims.pos) ||
      (new_buffer_dims != (new_buffer_dims & buffer_dims)) ||
      (view_dims != (view_dims & new_buffer_dims)))
    { // We will need to reshuffle or resize the buffer anyway, so might
      // as well get the best location for the buffer.
      new_buffer_dims.pos.x = view_dims.pos.x;
      new_buffer_dims.pos.y = view_dims.pos.y;
      new_buffer_dims.pos.x -= (new_buffer_dims.size.x-view_dims.size.x)/2;
      new_buffer_dims.pos.y -= (new_buffer_dims.size.y-view_dims.size.y)/2;
      if (new_buffer_dims.pos.x < image_dims.pos.x)
        new_buffer_dims.pos.x = image_dims.pos.x;
      if (new_buffer_dims.pos.y < image_dims.pos.y)
        new_buffer_dims.pos.y = image_dims.pos.y;
      buffer_lim = new_buffer_dims.pos + new_buffer_dims.size;
      if (buffer_lim.x > image_lim.x)
        new_buffer_dims.pos.x -= buffer_lim.x - image_lim.x;
      if (buffer_lim.y > image_lim.y)
        new_buffer_dims.pos.y -= buffer_lim.y - image_lim.y;
      assert(view_dims == (view_dims & new_buffer_dims));
      assert(new_buffer_dims == (image_dims & new_buffer_dims));
      assert(view_dims == (new_buffer_dims & view_dims));
    }

  // Set surface and get buffer
  compositor->set_buffer_surface(new_buffer_dims);
  buffer = compositor->get_composition_bitmap(buffer_dims);

  // Now reflect changes in the view size to the appearance of scroll bars.
/* MJY_092908 temporarily comment out
  SCROLLINFO sc_info; sc_info.cbSize = sizeof(sc_info);
  sc_info.fMask = SIF_DISABLENOSCROLL | SIF_ALL;
  sc_info.nMin = 0;
  sc_info.nMax = image_dims.size.x-1;
  sc_info.nPage = view_dims.size.x;
  sc_info.nPos = view_dims.pos.x - image_dims.pos.x;
  child_wnd->SetScrollInfo(SB_HORZ,&sc_info);
  sc_info.fMask = SIF_DISABLENOSCROLL | SIF_ALL;
  sc_info.nMin = 0;
  sc_info.nMax = image_dims.size.y-1;
  sc_info.nPage = view_dims.size.y;
  sc_info.nPos = view_dims.pos.y - image_dims.pos.y;
  child_wnd->SetScrollInfo(SB_VERT,&sc_info);
  kdu_coords step = view_dims.size;
  step.x = (step.x >> 4) + 1;
  step.y = (step.y >> 4) + 1;
  kdu_coords page = view_dims.size - step;
  child_wnd->set_scroll_metrics(step,page,image_dims.size-view_dims.size);
  */

  // Update related display properties
  if (need_redraw)
    child_wnd->Invalidate();
  child_wnd->UpdateWindow();
  update_focus_box();
  if (jpip_progress != NULL)
    { delete jpip_progress;  jpip_progress = NULL; }
}

/******************************************************************************/
/*                      CKdu_showApp::resize_stripmap                         */
/******************************************************************************/

void
  CKdu_showApp::resize_stripmap(int min_width)
{
  if (min_width <= strip_extent.x)
    return;
  strip_extent.x = min_width + 100;
  strip_extent.y = 100;
  memset(&stripmap_info,0,sizeof(stripmap_info));
  stripmap_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  stripmap_info.bmiHeader.biWidth = strip_extent.x;
  stripmap_info.bmiHeader.biHeight = -strip_extent.y;
  stripmap_info.bmiHeader.biPlanes = 1;
  stripmap_info.bmiHeader.biBitCount = 32;
  stripmap_info.bmiHeader.biCompression = BI_RGB;
  if (stripmap != NULL)
    DeleteObject(stripmap);
  stripmap =
    CreateDIBSection(NULL,&stripmap_info,DIB_RGB_COLORS,
                     (void **) &stripmap_buffer,NULL,0);
}

/******************************************************************************/
/*                       CKdu_showApp::set_hscroll_pos                        */
/******************************************************************************/

void
  CKdu_showApp::set_hscroll_pos(int pos, bool relative_to_last)
{
  if ((compositor == NULL) || (buffer == NULL))
    return;
  if (child_wnd != NULL)
    child_wnd->cancel_focus_drag();

  view_centre_known = false;
  if (relative_to_last)
    pos += view_dims.pos.x;
  else
    pos += image_dims.pos.x;
  if (pos < image_dims.pos.x)
    pos = image_dims.pos.x;
  if ((pos+view_dims.size.x) > (image_dims.pos.x+image_dims.size.x))
    pos = image_dims.pos.x+image_dims.size.x-view_dims.size.x;
  if (pos != view_dims.pos.x)
    {
      RECT update;
      child_wnd->ScrollWindowEx((view_dims.pos.x-pos)*pixel_scale,0,
                                NULL,NULL,NULL,&update,0);
      view_dims.pos.x = pos;
      if (view_dims != (view_dims & buffer_dims))
        { // The view is no longer fully contained in the buffered region.
          buffer_dims.pos.x =
            view_dims.pos.x - (buffer_dims.size.x-view_dims.size.x)/2;
          if (buffer_dims.pos.x < image_dims.pos.x)
            buffer_dims.pos.x = image_dims.pos.x;
          int image_lim = image_dims.pos.x+image_dims.size.x;
          int buf_lim = buffer_dims.pos.x + buffer_dims.size.x;
          if (buf_lim > image_lim)
            buffer_dims.pos.x -= (buf_lim-image_lim);
          compositor->set_buffer_surface(buffer_dims);
          buffer = compositor->get_composition_bitmap(buffer_dims);
        }
      // Repaint the erased area -- note that although the scroll function
      // is supposed to be able to invalidate the relevant regions of the
      // window, rendering this code unnecessary, that functionality appears
      // to be able to fail badly under certain extremely fast scrolling
      // sequences. Best to do the job ourselves.
      update.left = update.left / pixel_scale;
      update.right = (update.right+pixel_scale-1) / pixel_scale;
      update.top = update.top / pixel_scale;
      update.bottom = (update.bottom+pixel_scale-1) / pixel_scale;
      kdu_dims update_dims;
      update_dims.pos.x = update.left;
      update_dims.pos.y = update.top;
      update_dims.size.x = update.right-update.left;
      update_dims.size.y = update.bottom-update.top;

      update_dims.pos += view_dims.pos; // Get into the global coordinate system
      CDC *dc = child_wnd->GetDC();
      paint_region(dc,update_dims);
      child_wnd->ReleaseDC(dc);
    }
  child_wnd->SetScrollPos(SB_HORZ,pos-image_dims.pos.x);
  update_focus_box();
}

/******************************************************************************/
/*                       CKdu_showApp::set_vscroll_pos                        */
/******************************************************************************/

void
  CKdu_showApp::set_vscroll_pos(int pos, bool relative_to_last)
{
  if ((compositor == NULL) || (buffer == NULL))
    return;
  if (child_wnd != NULL)
    child_wnd->cancel_focus_drag();

  view_centre_known = false;
  if (relative_to_last)
    pos += view_dims.pos.y;
  else
    pos += image_dims.pos.y;
  if (pos < image_dims.pos.y)
    pos = image_dims.pos.y;
  if ((pos+view_dims.size.y) > (image_dims.pos.y+image_dims.size.y))
    pos = image_dims.pos.y+image_dims.size.y-view_dims.size.y;
  if (pos != view_dims.pos.y)
    {
      RECT update;
      child_wnd->ScrollWindowEx(0,(view_dims.pos.y-pos)*pixel_scale,
                                NULL,NULL,NULL,&update,0);
      view_dims.pos.y = pos;
      if (view_dims != (view_dims & buffer_dims))
        { // The view is no longer fully contained in the buffered region.
          buffer_dims.pos.y =
            view_dims.pos.y - (buffer_dims.size.y-view_dims.size.y)/2;
          if (buffer_dims.pos.y < image_dims.pos.y)
            buffer_dims.pos.y = image_dims.pos.y;
          int image_lim = image_dims.pos.y+image_dims.size.y;
          int buf_lim = buffer_dims.pos.y + buffer_dims.size.y;
          if (buf_lim > image_lim)
            buffer_dims.pos.y -= (buf_lim-image_lim);
          compositor->set_buffer_surface(buffer_dims);
          buffer = compositor->get_composition_bitmap(buffer_dims);
        }
      // Repaint the erased area -- note that although the scroll function
      // is supposed to be able to invalidate the relevant regions of the
      // window, rendering this code unnecessary, that functionality appears
      // to be able to fail badly under certain extremely fast scrolling
      // sequences.  Best to do the job ourselves.
      update.left = update.left / pixel_scale;
      update.right = (update.right+pixel_scale-1) / pixel_scale;
      update.top = update.top / pixel_scale;
      update.bottom = (update.bottom+pixel_scale-1) / pixel_scale;
      kdu_dims update_dims;
      update_dims.pos.x = update.left;
      update_dims.pos.y = update.top;
      update_dims.size.x = update.right-update.left;
      update_dims.size.y = update.bottom-update.top;
      update_dims.pos += view_dims.pos; // Get into the global coordinate system
      CDC *dc = child_wnd->GetDC();
      paint_region(dc,update_dims);
      child_wnd->ReleaseDC(dc);
    }
  child_wnd->SetScrollPos(SB_VERT,pos-image_dims.pos.y);
  update_focus_box();
}

/******************************************************************************/
/*                       CKdu_showApp::set_scroll_pos                         */
/******************************************************************************/

void
  CKdu_showApp::set_scroll_pos(int pos_x, int pos_y, bool relative_to_last)
{
  if ((compositor == NULL) || (buffer == NULL))
    return;
  view_centre_known = false;
  if (relative_to_last)
    {
      pos_y += view_dims.pos.y;
      pos_x += view_dims.pos.x;
    }
  else
    {
      pos_y += image_dims.pos.y;
      pos_x += image_dims.pos.x;
    }
  if (pos_y < image_dims.pos.y)
    pos_y = image_dims.pos.y;
  if ((pos_y+view_dims.size.y) > (image_dims.pos.y+image_dims.size.y))
    pos_y = image_dims.pos.y+image_dims.size.y-view_dims.size.y;
  if (pos_x < image_dims.pos.x)
    pos_x = image_dims.pos.x;
  if ((pos_x+view_dims.size.x) > (image_dims.pos.x+image_dims.size.x))
    pos_x = image_dims.pos.x+image_dims.size.x-view_dims.size.x;

  if ((pos_y != view_dims.pos.y) || (pos_x != view_dims.pos.x))
    {
      RECT update;
      child_wnd->ScrollWindowEx((view_dims.pos.x-pos_x)*pixel_scale,
                                (view_dims.pos.y-pos_y)*pixel_scale,
                                NULL,NULL,NULL,&update,0);
      view_dims.pos.x = pos_x;
      view_dims.pos.y = pos_y;
      if (view_dims != (view_dims & buffer_dims))
        { // The view is no longer fully contained in the buffered region.
          buffer_dims.pos.x =
            view_dims.pos.x - (buffer_dims.size.x-view_dims.size.x)/2;
          buffer_dims.pos.y =
            view_dims.pos.y - (buffer_dims.size.y-view_dims.size.y)/2;
          if (buffer_dims.pos.x < image_dims.pos.x)
            buffer_dims.pos.x = image_dims.pos.x;
          if (buffer_dims.pos.y < image_dims.pos.y)
            buffer_dims.pos.y = image_dims.pos.y;
          kdu_coords image_lim = image_dims.pos + image_dims.size;
          kdu_coords buf_lim = buffer_dims.pos + buffer_dims.size;
          if (buf_lim.y > image_lim.y)
            buffer_dims.pos.y -= (buf_lim.y - image_lim.y);
          if (buf_lim.x > image_lim.x)
            buffer_dims.pos.x -= (buf_lim.x - image_lim.x);
          compositor->set_buffer_surface(buffer_dims);
          buffer = compositor->get_composition_bitmap(buffer_dims);
        }
      // Repaint the erased area -- note that although the scroll function
      // is supposed to be able to invalidate the relevant regions of the
      // window, rendering this code unnecessary, that functionality appears
      // to be able to fail badly under certain extremely fast scrolling
      // sequences.  Best to do the job ourselves.
      update.left = update.left / pixel_scale;
      update.right = (update.right+pixel_scale-1) / pixel_scale;
      update.top = update.top / pixel_scale;
      update.bottom = (update.bottom+pixel_scale-1) / pixel_scale;
      kdu_dims update_dims;
      update_dims.pos.x = update.left;
      update_dims.pos.y = update.top;
      update_dims.size.x = update.right-update.left;
      update_dims.size.y = update.bottom-update.top;
      update_dims.pos += view_dims.pos; // Get into the global coordinate system
      CDC *dc = child_wnd->GetDC();
      paint_region(dc,update_dims);
      child_wnd->ReleaseDC(dc);
    }
  child_wnd->SetScrollPos(SB_HORZ,pos_x-image_dims.pos.x);
  child_wnd->SetScrollPos(SB_VERT,pos_y-image_dims.pos.y);
  update_focus_box();  
}


/******************************************************************************/
/*                        CKdu_showApp::paint_region4                          */
/******************************************************************************/

void
  CKdu_showApp::paint_region4(CDC *dc, kdu_dims region, bool is_absolute)
{


//kdu_dims new_image_dims;
//  compositor->set_scale(transpose,vflip,hflip,0.1F);
//  compositor->get_total_composition_dims(new_image_dims);
//  TRACE("size.x = %d size.y = %d\n", new_image_dims.size.x, new_image_dims.size.y);
//  compositor->set_buffer_surface(new_image_dims); // Render the whole thing
//    TRACE("size.x = %d size.y = %d\n", new_image_dims.size.x, new_image_dims.size.y);
//
//  buffer = compositor->get_composition_bitmap(new_image_dims);
//    TRACE("size.x = %d size.y = %d\n", new_image_dims.size.x, new_image_dims.size.y);



//  if (!is_absolute)
//    region.pos += view_dims.pos;
//
//  TRACE("view_dims %d %d %d %d\n", view_dims.pos.x, view_dims.pos.y, view_dims.size.x, view_dims.size.y);
//
//  kdu_dims region_on_buffer = region;
//  region_on_buffer &= buffer_dims; // Intersect with buffer region
//
//TRACE("buffer_dims %d %d %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y, buffer_dims.size.x, buffer_dims.size.y);
//
//  region_on_buffer.pos -= buffer_dims.pos; // Make relative to buffer region
//TRACE("region_on_buffer %d %d %d %d\n", region_on_buffer.pos.x, region_on_buffer.pos.y, region_on_buffer.size.x, region_on_buffer.size.y);
//
//  if (region_on_buffer.size != region.size)
//    { /* Need to erase the region first and then modify it to reflect the
//         region we are about to actually paint. */
//      dc->BitBlt(region.pos.x*pixel_scale,region.pos.y*pixel_scale,
//                 region.size.x*pixel_scale,region.size.y*pixel_scale,
//                 NULL,0,0,WHITENESS);
//      region = region_on_buffer;
//      region.pos += buffer_dims.pos; // Convert to absolute region
//    }
//  region.pos -= view_dims.pos; // Make relative to curent view
//  if ((!region_on_buffer) || (buffer == NULL))
//    return;
//  assert(region.size == region_on_buffer.size);
//
//  kdu_coords screen_pos;
//
//  int bitmap_row_gap;
//  kdu_uint32 *bitmap_buffer;
//  HBITMAP bitmap = buffer->access_bitmap(bitmap_buffer,bitmap_row_gap);
//
     // Paint bitmap directly
      //SelectObject(overview_compatible_dc.m_hDC,bitmap);

        dc->BitBlt(region.pos.x,region.pos.y, region.size.x,region.size.y,
                   &overview_compatible_dc,0,0, //region_on_buffer.pos.x,region_on_buffer.pos.y,
                   SRCCOPY);

}

/******************************************************************************/
/*                        CKdu_showApp::paint_region                          */
/******************************************************************************/

void
  CKdu_showApp::paint_region(CDC *dc, kdu_dims region, bool is_absolute)
{
  TRACE("rendering_scale %5.2f\n", rendering_scale);
  TRACE("buffer_dims size %d %d\n", buffer_dims.size.x, buffer_dims.size.y);
  TRACE("buffer_dims pos %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y);
  TRACE("image_dims size %d %d\n", image_dims.size.x, image_dims.size.y);
  TRACE("image_dims pos %d %d\n", image_dims.pos.x, image_dims.pos.y);
  TRACE("view_dims size %d %d\n", view_dims.size.x, view_dims.size.y);
  TRACE("view_dims pos %d %d\n", view_dims.pos.x, view_dims.pos.y);

  if (!is_absolute)
    region.pos += view_dims.pos;
  //TRACE("view_dims %d %d %d %d\n", view_dims.pos.x, view_dims.pos.y, view_dims.size.x, view_dims.size.y);

  kdu_dims region_on_buffer = region;
  region_on_buffer &= buffer_dims; // Intersect with buffer region

//TRACE("buffer_dims %d %d %d %d\n", buffer_dims.pos.x, buffer_dims.pos.y, buffer_dims.size.x, buffer_dims.size.y);

  region_on_buffer.pos -= buffer_dims.pos; // Make relative to buffer region
//TRACE("region_on_buffer %d %d %d %d\n", region_on_buffer.pos.x, region_on_buffer.pos.y, region_on_buffer.size.x, region_on_buffer.size.y);

  if (region_on_buffer.size != region.size)
    { /* Need to erase the region first and then modify it to reflect the
         region we are about to actually paint. */
      dc->BitBlt(region.pos.x*pixel_scale,region.pos.y*pixel_scale,
                 region.size.x*pixel_scale,region.size.y*pixel_scale,
                 NULL,0,0,WHITENESS);
      region = region_on_buffer;
      region.pos += buffer_dims.pos; // Convert to absolute region
    }
  region.pos -= view_dims.pos; // Make relative to curent view
  if ((!region_on_buffer) || (buffer == NULL))
    return;
  assert(region.size == region_on_buffer.size);

  kdu_coords screen_pos;

  int bitmap_row_gap;
  kdu_uint32 *bitmap_buffer;
  HBITMAP bitmap = buffer->access_bitmap(bitmap_buffer,bitmap_row_gap);

  // Paint bitmap

  SelectObject(compatible_dc.m_hDC,bitmap);

  dc->BitBlt(region.pos.x+x_offset,region.pos.y+y_offset, region.size.x,region.size.y,
                 &compatible_dc,region_on_buffer.pos.x,region_on_buffer.pos.y,
                 SRCCOPY);


}

/******************************************************************************/
/*                        CKdu_showApp::display_status                        */
/******************************************************************************/

void
  CKdu_showApp::display_status()
{
  //PLAYif (in_playmode)
  //  return;

  //if (playcontrol != NULL)
  //  {
  //    int track_idx = -1;
  //    if (single_component_idx < 0)
  //      {
  //        if (single_layer_idx < 0)
  //          track_idx = 0;
  //        else if (mj2_in.exists())
  //          track_idx = single_layer_idx+1;
  //      }
  //    playcontrol->update(track_idx,num_frames,frame_idx);
  //  }

  char string[128]; string[0] = '\0';
  char *sp = string;

  kdu_dims basis_region = (enable_focus_box)?focus_box:view_dims;
  float component_scale_x=-1.0F, component_scale_y=-1.0F;
  if ((compositor != NULL) && configuration_complete)
    {
      kdrc_stream *scan =
        compositor->get_next_visible_codestream(NULL,basis_region,false);
      if ((scan != NULL) &&
          (compositor->get_next_visible_codestream(scan,basis_region,
                                                   false) == NULL))
        {
          int cs_idx, layer_idx;
          compositor->get_codestream_info(scan,cs_idx,layer_idx,NULL,NULL,
                                          &component_scale_x,
                                          &component_scale_y);
          component_scale_x *= rendering_scale;
          component_scale_y *= rendering_scale;
          if (transpose)
            {
              float tmp=component_scale_x;
              component_scale_x=component_scale_y;
              component_scale_y=tmp;
            }
        }
    }

  if ((status_id == KDS_STATUS_CACHE) && !jpip_client.is_active())
    status_id = KDS_STATUS_LAYER_RES;

  if ((jpip_progress != NULL) && (status_id != KDS_STATUS_CACHE))
    { delete jpip_progress; jpip_progress = NULL; }
  if ((compositor == NULL) && !jpip_client.is_active())
    string[0] = '\0';
  else if ((status_id == KDS_STATUS_LAYER_RES) ||
           (status_id == KDS_STATUS_DIMS))
    {
      float res_percent = 100.0F*rendering_scale;
      float x_percent = 100.0F*component_scale_x;
      float y_percent = 100.0F*component_scale_y;
      if ((x_percent > (0.99F*res_percent)) &&
          (x_percent < (1.01F*res_percent)) &&
          (y_percent > (0.99F*res_percent)) &&
          (y_percent < (1.01F*res_percent)))
        x_percent = y_percent = -1.0F;
      if (x_percent < 0.0F)
        sprintf(sp,"Res=%1.1f%%",res_percent);
      else if ((x_percent > (0.99F*y_percent)) &&
               (x_percent < (1.01F*y_percent)))
        sprintf(sp,"Res=%1.1f%% (%1.1f%%)",res_percent,x_percent);
      else
        sprintf(sp,"Res=%1.1f%% (x:%1.1f%%,y:%1.1f%%)",
                res_percent,x_percent,y_percent);
      sp += strlen(sp);

      if (single_component_idx >= 0)
        {
          sprintf(sp,", Comp=%d/",single_component_idx+1);
          sp += strlen(sp);
          if (max_components <= 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",max_components); sp += strlen(sp); }
          sprintf(sp,", Stream=%d/",single_codestream_idx+1);
          sp += strlen(sp);
          if (max_codestreams <= 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",max_codestreams); sp += strlen(sp); }
        }
      else if ((single_layer_idx >= 0) && !mj2_in)
        { // Report compositing layer index
          sprintf(sp,", C.Layer=%d/",single_layer_idx+1);
          sp += strlen(sp);
          if (max_compositing_layer_idx < 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",max_compositing_layer_idx+1); sp += strlen(sp); }
        }
      else if ((single_layer_idx >= 0) && mj2_in.exists())
        { // Report track and frame indices, rather than layer index
          sprintf(sp,", Track:Frame=%d:%d",single_layer_idx+1,frame_idx+1);
          sp += strlen(sp);
        }
      else
        {
          sprintf(sp,", Frame=%d/",frame_idx+1);
          sp += strlen(sp);
          if (num_frames <= 0)
            *(sp++) = '?';
          else
            { sprintf(sp,"%d",num_frames); sp += strlen(sp); }
        }

      *(sp++) = '\t';
      if (compositor != NULL)
        {
          if (!compositor->is_processing_complete())
            { strcpy(sp,"WORKING"); sp += strlen(sp); }
          if (status_id == KDS_STATUS_DIMS)
            sprintf(sp,"\tHxW=%dx%d ",image_dims.size.y,image_dims.size.x);
          else        
            {
              int max_layers = compositor->get_max_available_quality_layers();
              if (max_display_layers >= max_layers)
                sprintf(sp,"\tQ.Layers=all ");
              else
                sprintf(sp,"\tQ.Layers=%d/%d ",max_display_layers,max_layers);
            }
        }
    }
  else if (status_id == KDS_STATUS_MEM)
    {
      kdu_long bytes = 0;
      if (compositor != NULL)
        {
          kdrc_stream *str=NULL;
          while ((str=compositor->get_next_codestream(str,false,true)) != NULL)
            {
              kdu_codestream ifc = compositor->access_codestream(str);
              bytes += ifc.get_compressed_data_memory()
                     + ifc.get_compressed_state_memory();
            }
        }
      if (jpip_client.is_active())
        bytes += jpip_client.get_peak_cache_memory();
      sprintf(sp,"Compressed working/cache memory = %5g MB",1.0E-6*bytes);
      sp += strlen(sp);
      if ((compositor != NULL) && !compositor->is_processing_complete())
        { strcpy(sp,"\t\tWORKING "); sp += strlen(sp); }
    }
  else if (status_id == KDS_STATUS_CACHE)
    {
      float received_kbytes = ((float) 1.0E-3) *
        jpip_client.get_received_bytes();
      strcpy(sp,jpip_client.get_status());
      sp += strlen(sp);
      sprintf(sp,"\t\t%8.1f kB",received_kbytes);
      sp += strlen(sp);
      if ((compositor != NULL) && !compositor->is_processing_complete())
        { strcpy(sp,"WORKING "); sp += strlen(sp); }
      if (cumulative_transmission_time || last_transmission_start)
        {
          double time = cumulative_transmission_time;
          if (last_transmission_start)
            time += (double)(clock() - last_transmission_start);
          time *= (1.0 / CLOCKS_PER_SEC);
          if (time > 0.0)
            sprintf(sp,"; %7.1f kB/s ",received_kbytes / time);
        }
      if (jpip_progress == NULL)
        {
          RECT rc;
          frame_wnd->get_status_bar()->GetItemRect(0,&rc);
          int size = rc.right-rc.left;
          rc.left += (3*size) >> 3;
          rc.right -= (3*size) >> 3;
          jpip_progress = new CProgressCtrl();
          jpip_progress->Create(WS_CHILD|WS_VISIBLE,rc,
                                frame_wnd->get_status_bar(),1);
          jpip_progress->SetRange(0,100);
          jpip_progress->SetPos(0);
          jpip_progress->SetStep(1);
        }
    }
  frame_wnd->SetMessageText(string);
  if ((jpip_progress != NULL) && (compositor != NULL) &&
      compositor->is_processing_complete())
    {
      kdu_long samples=0, packet_samples=0, max_packet_samples=0, s, p, m;
      kdrc_stream *stream=NULL;
      while ((compositor != NULL) &&
             ((stream = compositor->get_next_visible_codestream(stream,
                                              basis_region,true)) != NULL))
        {
          compositor->get_codestream_packets(stream,basis_region,s,p,m);
          samples += s;  packet_samples += p;  max_packet_samples += m;
        }
      int pos = 0;
      if (packet_samples > 0)
        pos = (int) floor(0.01+100.0 * (((double) packet_samples) /
                                       ((double) max_packet_samples)));
      jpip_progress->SetPos(pos);
    }
}

/******************************************************************************/
/*            CKdu_showApp::check_one_time_request_compatibility              */
/******************************************************************************/

bool
  CKdu_showApp::check_one_time_request_compatibility()
{
  if ((!jpx_in.exists()) || (single_component_idx >= 0))
    return true;
  if (!jpip_client.get_window_in_progress(&tmp_roi))
    return true; // Window information not available yet

  int c;
  kdu_sampled_range *rg;
  bool compatible = true;
  if ((single_component_idx < 0) && (single_layer_idx < 0) &&
      (frame_idx == 0) && (frame != NULL))
    { // Check compatibility of default animation frame
      int m, num_members = frame_expander.get_num_members();
      for (m=0; m < num_members; m++)
        {
          int instruction_idx, layer_idx;
          bool covers_composition;
          kdu_dims source_dims, target_dims;
          int remapping_ids[2];
          jx_frame *frame =
            frame_expander.get_member(m,instruction_idx,layer_idx,
                                      covers_composition,source_dims,
                                      target_dims);
          composition_rules.get_original_iset(frame,instruction_idx,
                                           remapping_ids[0],remapping_ids[1]);
          for (c=0; (rg=tmp_roi.contexts.access_range(c)) != NULL; c++)
            if ((rg->from <= layer_idx) && (rg->to >= layer_idx) &&
                (((layer_idx-rg->from) % rg->step) == 0))
              {
                if (covers_composition &&
                    (rg->remapping_ids[0] < 0) && (rg->remapping_ids[1] < 0))
                  break; // Found compatible layer
                if ((rg->remapping_ids[0] == remapping_ids[0]) &&
                    (rg->remapping_ids[1] == remapping_ids[1]))
                  break; // Found compatible layer
              }
          if (rg == NULL)
            { // No appropriate context request for layer; see if it needs one
              if (covers_composition && (layer_idx == 0) &&
                  jpx_in.access_compatibility().is_jp2_compatible())
                {
                  for (c=0; (rg=tmp_roi.codestreams.access_range(c))!=NULL; c++)
                    if (rg->from == 0)
                      break;
                }
            }
          if (rg == NULL)
            { // No response for this frame layer; must downgrade
              compatible = false;
              frame_idx = 0;
              frame_start = frame_end = 0.0;
              frame = NULL;
              single_layer_idx = 0; // Try this one
              break;
            }
        }
    }

  if (compatible)
    return true;

  bool have_codestream0 = false;
  for (c=0; (rg=tmp_roi.codestreams.access_range(c)) != NULL; c++)
    {
      if (rg->from == 0)
        have_codestream0 = true;
      if (rg->context_type == KDU_JPIP_CONTEXT_TRANSLATED)
        { // Use the compositing layer translated here
          int range_idx = rg->remapping_ids[0];
          int member_idx = rg->remapping_ids[1];
          rg = tmp_roi.contexts.access_range(range_idx);
          if (rg == NULL)
            continue;
          single_layer_idx = rg->from + rg->step*member_idx;
          if (single_layer_idx > rg->to)
            continue;
          return false; // Found a suitable compositing layer
        }
    }

  if (have_codestream0 && jpx_in.access_compatibility().is_jp2_compatible())
    {
      single_layer_idx = 0;
      return false; // Compositing layer 0 is suitable
    }

  // If we get here, we could not find a compatible layer
  single_layer_idx = -1;
  single_component_idx = 0;
  if ((rg=tmp_roi.codestreams.access_range(0)) != NULL)
    single_codestream_idx = rg->from;
  else
    single_codestream_idx = 0;

  return compatible;
}

/******************************************************************************/
/*              CKdu_showApp::update_client_window_of_interest                */
/******************************************************************************/

void
  CKdu_showApp::update_client_window_of_interest()
{
  if ((compositor == NULL) || (!jpip_client.is_alive()) ||
      jpip_client.is_one_time_request())
    return;

  // Begin by trying to find the location, size and resolution of the
  // view window.  We might have to guess at these parameters here if we
  // do not currently have an open image.
  kdu_dims res_dims, roi_dims;
  if (!image_dims.is_empty())
    {
      res_dims = image_dims;
      res_dims.from_apparent(transpose,vflip,hflip);
    }
  else
    { // Try to guess what the image dimensions would be at the current scale
      if (single_component_idx >= 0)
        { // Use size of the codestream, if we can determine it
          if (!jpx_in.exists())
            {
              kdrc_stream *sref =
                compositor->get_next_codestream(NULL,false,false);
              compositor->access_codestream(sref).get_dims(-1,res_dims);
            }
          else
            {
              jpx_codestream_source stream =
                jpx_in.access_codestream(single_codestream_idx);
              if (stream.exists())
                res_dims.size = stream.access_dimensions().get_size();
            }
        }
      else if (single_layer_idx >= 0)
        { // Use size of compositing layer, if we can evaluate it
          if (!jpx_in.exists())
            {
              kdrc_stream *sref =
                compositor->get_next_codestream(NULL,false,false);
              if (sref != NULL)
                {
                  compositor->access_codestream(sref).get_dims(-1,res_dims);
                  res_dims.pos = kdu_coords(0,0);
                }
            }
          else
            {
              jpx_layer_source layer = jpx_in.access_layer(single_layer_idx);
              if (layer.exists())
                res_dims.size = layer.get_layer_size();
            }
        }
      else
        { // Use size of composition surface, if we can find it
          if (composition_rules.exists())
            composition_rules.get_global_info(res_dims.size);
        }
      res_dims.pos = kdu_coords(0,0);
      res_dims.size.x = (int) ceil(res_dims.size.x * rendering_scale);
      res_dims.size.y = (int) ceil(res_dims.size.y * rendering_scale);
    }

  if (enable_focus_box && !focus_box.is_empty())
    {
      roi_dims = focus_box;
      roi_dims.from_apparent(transpose,vflip,hflip);
    }
  else if (focus_anchors_known && !res_dims.is_empty())
    {
      res_dims.to_apparent(transpose,vflip,hflip);
      convert_focus_anchors_to_region(roi_dims,res_dims);
      roi_dims.from_apparent(transpose,vflip,hflip);
      res_dims.from_apparent(transpose,vflip,hflip);
    }
  else if (!view_dims.is_empty())
    {
      roi_dims = view_dims;
      roi_dims.from_apparent(transpose,vflip,hflip);
    }
  else
    roi_dims = res_dims;
  if (roi_dims.is_empty())
    roi_dims = res_dims;
  roi_dims &= res_dims;

  // Now, we have as much information as we can guess concerning the
  // intended view window.  Let's use it to create a window request.
  tmp_roi.init();
  roi_dims.pos -= res_dims.pos; // Make relative to current resolution
  tmp_roi.resolution = res_dims.size;
  tmp_roi.region = roi_dims;
  tmp_roi.round_direction = 0;

  if (configuration_complete && (max_display_layers < (1<<16)) &&
      (max_display_layers < compositor->get_max_available_quality_layers()))
    tmp_roi.max_layers = max_display_layers;

  if (single_component_idx >= 0)
    {
      tmp_roi.components.add(single_component_idx);
      tmp_roi.codestreams.add(single_codestream_idx);
    }
  else if (single_layer_idx >= 0)
    { // single compositing layer
      if ((single_layer_idx == 0) &&
          ((!jpx_in) || jpx_in.access_compatibility().is_jp2_compatible()))
        tmp_roi.codestreams.add(0); // Maximize chance of success even if
                                    // server cannot translate contexts
      kdu_sampled_range rg;
      rg.from = rg.to = single_layer_idx;
      rg.context_type = KDU_JPIP_CONTEXT_JPXL;
      tmp_roi.contexts.add(rg);
    }
  else if (frame != NULL)
    { // Request whole animation frame
      kdu_dims scaled_roi_dims;
      if (configuration_complete &&
          !(roi_dims.is_empty() || res_dims.is_empty()))
        {
          kdu_coords comp_size; composition_rules.get_global_info(comp_size);
          double scale_x = ((double) comp_size.x) / ((double) res_dims.size.x);
          double scale_y = ((double) comp_size.y) / ((double) res_dims.size.y);
          kdu_coords min = roi_dims.pos;
          kdu_coords lim = min + roi_dims.size;
          min.x = (int) floor(scale_x * min.x);
          lim.x = (int) ceil(scale_x * lim.x);
          min.y = (int) floor(scale_y * min.y);
          lim.y = (int) ceil(scale_y * lim.y);
          scaled_roi_dims.pos = min;
          scaled_roi_dims.size = lim - min;
        }
      kdu_sampled_range rg;
      frame_expander.construct(&jpx_in,frame,frame_iteration,true,
                               scaled_roi_dims);
      int m, num_members = frame_expander.get_num_members();
      for (m=0; m < num_members; m++)
        {
          int instruction_idx, layer_idx;
          bool covers_composition;
          kdu_dims source_dims, target_dims;
          jx_frame *frame =
            frame_expander.get_member(m,instruction_idx,layer_idx,
                                      covers_composition,source_dims,
                                      target_dims);
          rg.from = rg.to = layer_idx;
          rg.context_type = KDU_JPIP_CONTEXT_JPXL;
          if (covers_composition)
            {
              if ((layer_idx == 0) && (num_members == 1) &&
                  jpx_in.access_compatibility().is_jp2_compatible())
                tmp_roi.codestreams.add(0); // Maximize chance that server will
                      // respond correctly, even if it can't translate contexts
              rg.remapping_ids[0] = rg.remapping_ids[1] = -1;
            }
          else
            {
              composition_rules.get_original_iset(frame,instruction_idx,
                                                  rg.remapping_ids[0],
                                                  rg.remapping_ids[1]);
            }
          tmp_roi.contexts.add(rg);
        }
    }

  // Post the window
  if (jpip_client.post_window(&tmp_roi))
    {
      client_roi.copy_from(tmp_roi);
      if (last_transmission_start == 0)
        { // Server was idle; start timing server responses again
          last_transmission_start = clock();
        }
    }
}

/******************************************************************************/
/*                     CKdu_showApp::change_client_focus                      */
/******************************************************************************/

void
  CKdu_showApp::change_client_focus(kdu_coords actual_resolution,
                                    kdu_dims actual_region)
{
  // Find the relative location and dimensions of the new focus box
  focus_centre_x =
    (((float) actual_region.pos.x) + 0.5F*((float) actual_region.size.x)) /
    ((float) actual_resolution.x);
  focus_centre_y =
    (((float) actual_region.pos.y) + 0.5F*((float) actual_region.size.y)) /
    ((float) actual_resolution.y);
  focus_size_x = ((float) actual_region.size.x) / ((float) actual_resolution.x);
  focus_size_y = ((float) actual_region.size.y) / ((float) actual_resolution.y);

  // Correct for geometric transformations
  if (transpose)
    {
      float tmp=focus_size_x; focus_size_x=focus_size_y; focus_size_y=tmp;
      tmp=focus_centre_x; focus_centre_x=focus_centre_y; focus_centre_y=tmp;
    }
  if (image_dims.pos.x < 0)
    focus_centre_x = 1.0F - focus_centre_x;
  if (image_dims.pos.y < 0)
    focus_centre_y = 1.0F - focus_centre_y;

  // Draw the focus box from its relative coordinates.
  focus_anchors_known = true;
  enable_focus_box = true;
  bool posting_state = enable_region_posting;
  enable_region_posting = false;
  update_focus_box();
  enable_region_posting = posting_state;
}

/******************************************************************************/
/*                       CKdu_showApp::set_focus_box                          */
/******************************************************************************/

void
  CKdu_showApp::set_focus_box(kdu_dims new_box)
{
  if ((compositor == NULL) || (buffer == NULL))
    return;
  new_box.pos += view_dims.pos;
  CDC *dc = child_wnd->GetDC();
  kdu_dims previous_box = focus_box;
  bool previous_enable = enable_focus_box;
  if (new_box.area() < (kdu_long) 100)
    enable_focus_box = false;
  else
    { focus_box = new_box; enable_focus_box = true; }
  focus_box = get_new_focus_box();
  enable_focus_box = (focus_box != view_dims);
  if (previous_enable != enable_focus_box)
    paint_region(dc,view_dims);
  else if (enable_focus_box)
    {
      paint_region(dc,previous_box);
      paint_region(dc,focus_box);
    }
  child_wnd->ReleaseDC(dc);
  if (jpip_client.is_alive())
    update_client_window_of_interest();
}

/******************************************************************************/
/*                      CKdu_showApp::get_new_focus_box                       */
/******************************************************************************/

kdu_dims
  CKdu_showApp::get_new_focus_box()
{
  kdu_dims new_focus_box = focus_box;
  if ((!enable_focus_box) || (!focus_box))
    new_focus_box = view_dims;
  if (enable_focus_box && focus_anchors_known)
    { // Generate new focus box dimensions from anchor coordinates.
      convert_focus_anchors_to_region(new_focus_box,image_dims);
      focus_anchors_known = false;
    }

  // Adjust box to fit into view port.
  if (new_focus_box.pos.x < view_dims.pos.x)
    new_focus_box.pos.x = view_dims.pos.x;
  if ((new_focus_box.pos.x+new_focus_box.size.x) >
      (view_dims.pos.x+view_dims.size.x))
    new_focus_box.pos.x = view_dims.pos.x+view_dims.size.x-new_focus_box.size.x;
  if (new_focus_box.pos.y < view_dims.pos.y)
    new_focus_box.pos.y = view_dims.pos.y;
  if ((new_focus_box.pos.y+new_focus_box.size.y) >
      (view_dims.pos.y+view_dims.size.y))
    new_focus_box.pos.y = view_dims.pos.y+view_dims.size.y-new_focus_box.size.y;
  new_focus_box &= view_dims;

  if (!new_focus_box)
    new_focus_box = view_dims;
  return new_focus_box;
}

/******************************************************************************/
/*                       CKdu_showApp::update_focus_box                       */
/******************************************************************************/

void
  CKdu_showApp::update_focus_box()
{
  kdu_dims new_focus_box = get_new_focus_box();
  bool new_focus_enable = (new_focus_box != view_dims);
  if ((new_focus_box != focus_box) || (new_focus_enable != enable_focus_box))
    {
      bool previous_enable = enable_focus_box;
      kdu_dims previous_box = focus_box;
      focus_box = new_focus_box;
      enable_focus_box = new_focus_enable;
      CDC *dc = child_wnd->GetDC();
      if (previous_enable != enable_focus_box)
        paint_region(dc,view_dims);
      else if (enable_focus_box)
        {
          paint_region(dc,previous_box);
          paint_region(dc,focus_box);
        }
      child_wnd->ReleaseDC(dc);
    }
  if (enable_region_posting)
    update_client_window_of_interest();
}

/******************************************************************************/
/*                      CKdu_showApp::outline_focus_box                       */
/******************************************************************************/

void
  CKdu_showApp::outline_focus_box(CDC *dc)
{
  if ((!enable_focus_box) || (focus_box == view_dims))
    return;
  kdu_coords box_min = focus_box.pos - view_dims.pos;
  kdu_coords box_lim = box_min + focus_box.size;
  POINT pt;
  dc->SelectObject(focus_pen);
  dc->SetBkColor(0x00FFFF00);
  pt.x = box_min.x*pixel_scale; pt.y = box_min.y*pixel_scale; dc->MoveTo(pt);
  pt.y = (box_lim.y-1)*pixel_scale; dc->LineTo(pt);
  pt.x = (box_lim.x-1)*pixel_scale; dc->LineTo(pt);
  pt.y = box_min.y*pixel_scale; dc->LineTo(pt);
  pt.x = box_min.x*pixel_scale; dc->LineTo(pt);
}

/******************************************************************************/
/*                            CKdu_showApp::OnIdle                            */
/******************************************************************************/

BOOL CKdu_showApp::OnIdle(LONG lCount) 
  /* Note: this function implements a very simple heuristic for scheduling
     regions for processing.  It is intended primarily to demonstrate
     capabilities of the underlying framework -- in particular the
     `kdu_region_compositor' object and the rest of the Kakadu JPEG2000
     platform on which it is built. */
{
  if (processing_suspended)
    return CWinApp::OnIdle(lCount);
  if (in_idle)
    return FALSE;
  if (compositor == NULL)
    {
      if (jpip_client.is_active())
        open_file(NULL); // Try to complete the client source opening operation.
      display_status();
      return FALSE; // Don't need to be called from recursive loop again.
    }
  in_idle = true;

  if (jpip_client.is_active() && last_transmission_start &&
      jpip_client.is_idle())
    {
      cumulative_transmission_time += (clock() - last_transmission_start);
      last_transmission_start = 0;
    }

  //double time=0.0, frame_seconds_left=-1.0;
  //PLAYif (in_playmode)
  //  { // See if we are ready to display a new processed frame
  //    int track_idx = -1;
  //    if (frame != NULL)
  //      track_idx = 0;
  //    else if (mj2_in.exists() && (single_component_idx < 0))
  //      track_idx = single_layer_idx+1;

  //    time = playstart_instant +
  //      ((double)(clock()-playclock_base))*(1.0/CLOCKS_PER_SEC);

  //    int display_frame_idx;
  //    kdu_long stamp=0;
  //    bool queue_exists =
  //      compositor->inspect_composition_queue(0,&stamp,&display_frame_idx);
  //    double timestamp = ((double) stamp)*0.001; // Time when frame display ends
  //    frame_seconds_left = timestamp - time;
  //    if (queue_exists && (frame_seconds_left <= 0.0) &&
  //        compositor->inspect_composition_queue(1,&stamp,&display_frame_idx))
  //      {
  //        double nominal_period = timestamp - last_nominal_time;
  //        double actual_period = time - last_actual_time;
  //        if ((last_actual_time < 0.0) || (last_nominal_time < 0.0))
  //          nominal_period = actual_period = -1.0;

  //        compositor->pop_composition_buffer();
  //        buffer = compositor->get_composition_bitmap(buffer_dims);
  //        CDC *dc = child_wnd->GetDC();
		//  //TRACE("pain_region call in OnIdle....view_dims...........%d, %d, %d, %d\n", 
		//	//  view_dims.pos.x, view_dims.pos.y, view_dims.size.x, view_dims.size.y);
  //        paint_region(dc,view_dims);
  //        child_wnd->ReleaseDC(dc);



  //        playcontrol->update(track_idx,num_frames,display_frame_idx,-1.0F,
  //                            (float) nominal_period,
  //                            (float) actual_period);
  //        if ((time - timestamp) > 0.5)
  //          { // Adjust playclock_base to avoid playing catch up for lags
  //            // which exceed half a second
  //            double delta = time-timestamp-0.5;
  //            playclock_base += (clock_t)(delta*CLOCKS_PER_SEC);
  //            time -= delta;
  //          }
  //        last_nominal_time = timestamp;
  //        last_actual_time = time;
  //        in_idle = false;
  //        return TRUE; // Give the message queue another go
  //      }
  //    else if (queue_exists && (frame_seconds_left <= 0.0) && pushed_last_frame)
  //      {
  //        stop_playmode();
  //        in_idle = false;
  //        return TRUE;
  //      }
  //    if (queue_exists)
  //      playcontrol->update(track_idx,num_frames,display_frame_idx,
  //                          (float) frame_seconds_left);
  //  }

  kdu_dims new_region;
  bool need_more_processing = (configuration_complete && (buffer != NULL));
  try {
      if (need_more_processing && !compositor->process(16384,new_region))
        {
          need_more_processing = false;
          if (!compositor->get_total_composition_dims(image_dims))
            { // Must have invalid scale
              compositor->flush_composition_queue();
              buffer = NULL;
              initialize_regions(); // This call will find a satisfactory scale
              in_idle = false;
              return FALSE;
            }
          //else if (in_playmode && !pushed_last_frame)
          //  { // See if we should push this frame onto the queue
          //    double adjusted_end_instant, adjusted_start_instant;
          //    if (use_native_timing)
          //      {
          //        adjusted_start_instant = frame_start/rate_multiplier;
          //        adjusted_end_instant = frame_end/rate_multiplier;
          //      }
          //    else
          //      {
          //        adjusted_start_instant = adjusted_end_instant =
          //          ((double) frame_idx) / (custom_fps*rate_multiplier);
          //        adjusted_end_instant += 1.0 / (custom_fps*rate_multiplier);
          //      }
          //    adjusted_start_instant += frame_time_offset;
          //    adjusted_end_instant += frame_time_offset;
          //    if ((!compositor->inspect_composition_queue(max_queue_size-1)) &&
          //        ((adjusted_start_instant-time) <= max_queue_period))
          //      {
          //        compositor->set_surface_initialization_mode(false);
          //             // Avoids wasting time initializing new buffer surfaces
          //        compositor->push_composition_buffer(
          //          (kdu_long)(adjusted_end_instant*1000),frame_idx);
          //        if (!set_frame(frame_idx+1))
          //          {
          //            if (playmode_repeat)
          //              {
          //                set_frame(0);
          //                frame_time_offset = adjusted_end_instant;
          //              }
          //            else
          //              pushed_last_frame = true;
          //          }
          //        need_more_processing = true;
          //      }
          //  }
        }
    }
  catch (int) { // Problem occurred.  Only safe thing is to close the file.
      close_file();
      in_idle = false;
      return FALSE;
    }

  if ((compositor != NULL) && !compositor->inspect_composition_queue(0))
    { // Paint any newly decompressed region right away.
      new_region &= view_dims; // No point in painting invisible regions.
      if (!new_region.is_empty())
        {
          CDC *dc = child_wnd->GetDC();
		  //TRACE("pain_region call in OnIdle..new_region.............%d, %d, %d, %d\n", 
			//  new_region.pos.x, new_region.pos.y, new_region.size.x, new_region.size.y);

          paint_region(dc,new_region);
          child_wnd->ReleaseDC(dc);

        }
    }

  if (need_more_processing)
    {
      in_idle = false;
      return TRUE;
    }

  //PLAY if (in_playmode)
  //  { // Set up a timer to make sure we get woken up in time to paint the
  //    // next frame when it comes due or to update the amount of time left until
  //    // a frame change, for frames with a long duration.
  //    kdu_long stamp;
  //    if (compositor->inspect_composition_queue(0,&stamp))
  //      {
  //        double time = playstart_instant +
  //          ((double)(clock()-playclock_base))*(1.0/CLOCKS_PER_SEC);
  //        stamp -= (kdu_long)(1000.0*time); // Stamp is already in miliseconds
  //        if (stamp > 250)
  //          stamp = 250;
  //        refresh_timer_id =
  //          m_pMainWnd->SetTimer(KDU_SHOW_REFRESH_TIMER_IDENT,
  //                               (kdu_uint32)(1+stamp),NULL);
  //      }
  //    else if (refresh_timer_id != 0)
  //      {
  //        m_pMainWnd->KillTimer(refresh_timer_id);
  //        refresh_timer_id = 0;
  //      }
  //  }
  //else
    display_status(); // All processing done, keep status up to date

  if (jpip_client.is_active())
    {
      // See if focus box needs to be changed
      if (jpip_client.get_window_in_progress(&tmp_roi))
        {
          if ((client_roi.region != tmp_roi.region) ||
              (client_roi.resolution != tmp_roi.resolution))
            change_client_focus(tmp_roi.resolution,tmp_roi.region);
          client_roi.copy_from(tmp_roi);
        }
    
      // See if cache has been updated.
      int new_bytes = (int)
        (jpip_client.get_transferred_bytes(KDU_MAIN_HEADER_DATABIN) +
         jpip_client.get_transferred_bytes(KDU_TILE_HEADER_DATABIN) +
         jpip_client.get_transferred_bytes(KDU_PRECINCT_DATABIN) +
         jpip_client.get_transferred_bytes(KDU_META_DATABIN));
      if (new_bytes != last_transferred_bytes)
        {
          if (!configuration_complete)
            initialize_regions();
          if ((refresh_timer_id == 0)) //PLAY && !in_playmode)
            refresh_timer_id =
              m_pMainWnd->SetTimer(KDU_SHOW_REFRESH_TIMER_IDENT,1000,NULL);
          last_transferred_bytes = new_bytes;
        }
    }

  if (overlays_enabled && overlay_flashing_enabled &&
      (flash_timer_id == 0)) // PLAY && !in_playmode)
    flash_timer_id =
      m_pMainWnd->SetTimer(KDU_SHOW_FLASH_TIMER_IDENT,800,NULL);

  in_idle = false;
  return CWinApp::OnIdle(lCount); // Give low-level idle processing a go.
}

/******************************************************************************/
/*                          CKdu_showApp::OnAppAbout                          */
/******************************************************************************/

void CKdu_showApp::OnAppAbout()
{
  CAboutDlg aboutDlg;
  aboutDlg.DoModal();
}

/******************************************************************************/
/*                          CKdu_showApp::OnFileOpen                          */
/******************************************************************************/

void CKdu_showApp::OnFileOpen() 
{
  char filename[MAX_PATH];
  char initial_dir[MAX_PATH];
  OPENFILENAME ofn;
  memset(&ofn,0,sizeof(ofn)); ofn.lStructSize = sizeof(ofn);
  strcpy(initial_dir,settings.get_open_save_dir());
  if (*initial_dir == '\0')
    GetCurrentDirectory(MAX_PATH,initial_dir);

  ofn.hwndOwner = m_pMainWnd->GetSafeHwnd();
  ofn.lpstrFilter =
    "JP2-family file (*.jp2, *.jpx, *.jpf, *.mj2)\0*.jp2;*.jpx;*.jpf;*.mj2\0"
    "JP2 files (*.jp2)\0*.jp2\0"
    "JPX files (*.jpx, *.jpf)\0*.jpx;*.jpf\0"
    "MJ2 files (*.mj2)\0*.mj2\0"
    "Uwrapped code-streams (*.j2c, *.j2k)\0*.j2c;*.j2k\0"
    "All files (*.*)\0*.*\0\0";
  ofn.nFilterIndex = settings.get_open_idx();
  if ((ofn.nFilterIndex > 5) || (ofn.nFilterIndex < 1))
    ofn.nFilterIndex = 1;
  ofn.lpstrFile = filename; filename[0] = '\0';
  ofn.nMaxFile = MAX_PATH-1;
  ofn.lpstrTitle = "Open JPEG2000 Compressed Image";
  ofn.lpstrInitialDir = initial_dir;
  ofn.Flags = OFN_FILEMUSTEXIST;
  if (!GetOpenFileName(&ofn))
    return;

  strcpy(initial_dir,filename);
  if ((ofn.nFileOffset > 0) &&
      (initial_dir[ofn.nFileOffset-1] == '/') ||
      (initial_dir[ofn.nFileOffset-1] == '\\'))
    ofn.nFileOffset--;
  initial_dir[ofn.nFileOffset] = '\0';
  settings.set_open_save_dir(initial_dir);
  settings.set_open_idx(ofn.nFilterIndex);
  open_file(filename);

}

/******************************************************************************/
/*                          CKdu_showApp::OnFileClose                         */
/******************************************************************************/

void CKdu_showApp::OnFileClose() 
{
  close_file();
}

/******************************************************************************/
/*                       CKdu_showApp::OnUpdateFileClose                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateFileClose(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) || jpip_client.is_active());
}

/******************************************************************************/
/*                           CKdu_showApp::OnJpipOpen                         */
/******************************************************************************/

void CKdu_showApp::OnJpipOpen() 
{
  char server_name[MAX_PATH];
  char proxy_name[MAX_PATH];
  char request[MAX_PATH];
  char channel_type[15];
  char cache_dir_name[MAX_PATH];

  // Initialize fields
  strncpy(server_name,settings.get_jpip_server(),MAX_PATH);
  server_name[MAX_PATH-1] = '\0';
  strncpy(proxy_name,settings.get_jpip_proxy(),MAX_PATH);
  proxy_name[MAX_PATH-1] = '\0';
  strncpy(request,settings.get_jpip_request(),MAX_PATH);
  request[MAX_PATH-1] = '\0';
  strncpy(channel_type,settings.get_jpip_channel_type(),15);
  channel_type[14] = '\0';
  strncpy(cache_dir_name,settings.get_jpip_cache(),MAX_PATH);
  cache_dir_name[MAX_PATH-1] = '\0';

  // Run dialog
  CJpipOpenDlg open_dlg(server_name,MAX_PATH,request,MAX_PATH,
                        channel_type,15,proxy_name,MAX_PATH,
                        cache_dir_name,MAX_PATH);
  if (open_dlg.DoModal() != IDOK)
    return;

  // Save settings
  settings.set_jpip_server(server_name);
  settings.set_jpip_request(request);
  settings.set_jpip_channel_type(channel_type);
  settings.set_jpip_proxy(proxy_name);
  settings.set_jpip_cache(cache_dir_name);
  settings.save_to_registry(this);

  // Create string.
  char url_string[2*MAX_PATH+30]; // Plenty of space.
  strcpy(url_string,"jpip://");
  strcat(url_string,server_name);
  strcat(url_string,"/");
  strcat(url_string,request);
  open_file(url_string);
}

/******************************************************************************/
/*                       CKdu_showApp::OnFileDisconnect                       */
/******************************************************************************/

void CKdu_showApp::OnFileDisconnect() 
{
  if (jpip_client.is_alive())
    jpip_client.disconnect(true,2000);
}

/******************************************************************************/
/*                     CKdu_showApp::OnUpdateFileDisconnect                   */
/******************************************************************************/

void CKdu_showApp::OnUpdateFileDisconnect(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(jpip_client.is_alive());
}

/******************************************************************************/
/*                          CKdu_showApp::OnViewHflip                         */
/******************************************************************************/

void CKdu_showApp::OnViewHflip()
{
  if (buffer == NULL)
    return;
  hflip = !hflip;
  calculate_view_anchors();
  view_centre_x = 1.0F - view_centre_x;
  if (focus_anchors_known)
    focus_centre_x = 1.0F - focus_centre_x;
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                       CKdu_showApp::OnUpdateViewHflip                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewHflip(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(buffer != NULL);
}

/******************************************************************************/
/*                          CKdu_showApp::OnViewVflip                         */
/******************************************************************************/

void CKdu_showApp::OnViewVflip() 
{
  if (buffer == NULL)
    return;
  vflip = !vflip;
  calculate_view_anchors();
  view_centre_y = 1.0F - view_centre_y;
  if (focus_anchors_known)
    focus_centre_y = 1.0F - focus_centre_y;
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                       CKdu_showApp::OnUpdateViewVflip                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewVflip(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(buffer != NULL);
}

/******************************************************************************/
/*                          CKdu_showApp::OnViewRotate                        */
/******************************************************************************/

void CKdu_showApp::OnViewRotate() 
{
  if (buffer == NULL)
    return;
  // Need to effectively add an extra transpose, followed by an extra hflip.
  transpose = !transpose;
  bool tmp = hflip; hflip = vflip; vflip = tmp;
  hflip = !hflip;
  calculate_view_anchors();
  float f_tmp = view_centre_y;
  view_centre_y = view_centre_x;
  view_centre_x = 1.0F-f_tmp;
  if (focus_anchors_known)
    {
      f_tmp = focus_centre_y;
      focus_centre_y = focus_centre_x;
      focus_centre_x = 1.0F-f_tmp;
      f_tmp = focus_size_x; focus_size_x = focus_size_y; focus_size_y = f_tmp;
    }
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                       CKdu_showApp::OnUpdateViewRotate                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewRotate(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(buffer != NULL);
}

/******************************************************************************/
/*                      CKdu_showApp::OnViewCounterRotate                     */
/******************************************************************************/

void CKdu_showApp::OnViewCounterRotate() 
{
  if (buffer == NULL)
    return;
  // Need to effectively add an extra transpose, followed by an extra vflip.
  transpose = !transpose;
  bool tmp = hflip; hflip = vflip; vflip = tmp;
  vflip = !vflip;
  calculate_view_anchors();
  float f_tmp = view_centre_x;
  view_centre_x = view_centre_y;
  view_centre_y = 1.0F-f_tmp;
  if (focus_anchors_known)
    {
      f_tmp = focus_centre_x;
      focus_centre_x = focus_centre_y;
      focus_centre_y = 1.0F-f_tmp;
      f_tmp = focus_size_x; focus_size_x = focus_size_y; focus_size_y = f_tmp;
    }
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                   CKdu_showApp::OnUpdateViewCounterRotate                  */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewCounterRotate(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(buffer != NULL);
}

/******************************************************************************/
/*                         CKdu_showApp::OnViewZoomOut                        */
/******************************************************************************/

void CKdu_showApp::OnViewZoomOut() 
{
  if (buffer == NULL)
    return;
  if (rendering_scale == min_rendering_scale)
    return;
  rendering_scale = decrease_scale(rendering_scale);
  calculate_view_anchors();
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                      CKdu_showApp::OnUpdateViewZoomOut                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewZoomOut(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((buffer != NULL) &&
                 ((min_rendering_scale < 0.0F) ||
                  (rendering_scale > min_rendering_scale)));
}

/******************************************************************************/
/*                         CKdu_showApp::OnViewZoomIn                         */
/******************************************************************************/

void CKdu_showApp::OnViewZoomIn() 
{
  if (buffer == NULL)
    return;
  rendering_scale = increase_scale(rendering_scale);
  calculate_view_anchors();
  if (focus_anchors_known)
    { view_centre_x = focus_centre_x; view_centre_y = focus_centre_y; }
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                      CKdu_showApp::OnUpdateViewZoomIn                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewZoomIn(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(buffer != NULL);
}

/******************************************************************************/
/*                      CKdu_showApp::OnViewOptimizescale                     */
/******************************************************************************/

void CKdu_showApp::OnViewOptimizescale()
{
  if ((compositor == NULL) || !configuration_complete)
    return;
  float min_scale = rendering_scale * 0.5F;
  float max_scale = rendering_scale * 2.0F;
  if ((min_rendering_scale > 0.0F) && (min_scale < min_rendering_scale))
    min_scale = min_rendering_scale;
  kdu_dims region_basis = (enable_focus_box)?focus_box:view_dims;
  float new_scale =
    compositor->find_optimal_scale(region_basis,rendering_scale,
                                   min_scale,max_scale);
  if (new_scale == rendering_scale)
    return;
  calculate_view_anchors();
  if (focus_anchors_known)
    { view_centre_x = focus_centre_x; view_centre_y = focus_centre_y; }
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  rendering_scale = new_scale;
  initialize_regions();
}

/******************************************************************************/
/*                   CKdu_showApp::OnUpdateViewOptimizescale                  *
/
/******************************************************************************/

void CKdu_showApp::OnUpdateViewOptimizescale(CCmdUI *pCmdUI)
{
  pCmdUI->Enable((compositor != NULL) && configuration_complete);
}

/******************************************************************************/
/*                         CKdu_showApp::OnViewRestore                        */
/******************************************************************************/

void CKdu_showApp::OnViewRestore() 
{
  if (buffer == NULL)
    return;
  calculate_view_anchors();
  if (vflip)
    {
      vflip = false;
      view_centre_y = 1.0F-view_centre_y;
      if (focus_anchors_known)
        focus_centre_y = 1.0F-focus_centre_y;
    }
  if (hflip)
    {
      hflip = false;
      view_centre_x = 1.0F-view_centre_x;
      if (focus_anchors_known)
        focus_centre_x = 1.0F-focus_centre_x;
    }
  if (transpose)
    {
      float tmp;
      transpose = false;
      tmp = view_centre_y;
      view_centre_y = view_centre_x;
      view_centre_x = tmp;
      if (focus_anchors_known)
        {
          tmp = focus_centre_y;
          focus_centre_y = focus_centre_x;
          focus_centre_x = tmp;
          tmp = focus_size_y;
          focus_size_y = focus_size_x;
          focus_size_x = tmp;
        }
    }
  view_dims = kdu_dims(); // Force full window init inside `initialize_regions'
  initialize_regions();
}

/******************************************************************************/
/*                      CKdu_showApp::OnUpdateViewRestore                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewRestore(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((buffer != NULL) && (transpose || vflip || hflip));
}


/******************************************************************************/
/*                       CKdu_showApp::OnStatusToggle                         */
/******************************************************************************/

void CKdu_showApp::OnStatusToggle() 
{
  if (status_id == KDS_STATUS_LAYER_RES)
    status_id = KDS_STATUS_DIMS;
  else if (status_id == KDS_STATUS_DIMS)
    status_id = KDS_STATUS_MEM;
  else if (status_id == KDS_STATUS_MEM)
    status_id = KDS_STATUS_CACHE;
  else
    status_id = KDS_STATUS_LAYER_RES;
  display_status();
}

/******************************************************************************/
/*                   CKdu_showApp::OnUpdateStatusToggle                       */
/******************************************************************************/

void CKdu_showApp::OnUpdateStatusToggle(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(compositor != NULL);
}

/******************************************************************************/
/*                        CKdu_showApp::set_codestream                        */
/******************************************************************************/

void
  CKdu_showApp::set_codestream(int codestream_idx)
{
  calculate_view_anchors();
  if ((single_component_idx<0) && focus_anchors_known && !image_dims.is_empty())
    { // Try to map focus region to this codestream
      kdu_dims region; convert_focus_anchors_to_region(region,image_dims);
      kdu_dims stream_dims, stream_region;
      kdu_long area, max_area=0;
      int instance, best_instance=0;
      for (instance=0;
           !(stream_dims =
             compositor->find_codestream_region(codestream_idx,
                                                instance,true)).is_empty();
           instance++)
        {
          stream_region = stream_dims & region;
          area = stream_region.area();
          if (area > max_area)
            { max_area = area; best_instance = instance; }
        }
      if (max_area == 0)
        focus_anchors_known = enable_focus_box = false;
      else
        {
          stream_dims =
            compositor->find_codestream_region(codestream_idx,
                                               best_instance,false);
          stream_region = stream_dims & region;
          convert_region_to_focus_anchors(stream_region,stream_dims);
          focus_layer = -1;
          focus_codestream = codestream_idx;
          view_centre_x = focus_centre_x; view_centre_y = focus_centre_y;
        }
    }
  single_component_idx = 0;
  single_codestream_idx = codestream_idx;
  single_layer_idx = -1;
  frame = NULL;
  invalidate_configuration();
  initialize_regions();
}

/******************************************************************************/
/*                    CKdu_showApp::set_compositing_layer                     */
/******************************************************************************/

void
  CKdu_showApp::set_compositing_layer(int layer_idx)
{
  calculate_view_anchors();
  if ((frame != NULL) && focus_anchors_known && !image_dims.is_empty())
    { // Try to map focus region to this compositing layer
      kdu_dims region; convert_focus_anchors_to_region(region,image_dims);
      kdu_dims layer_dims, layer_region;
      kdu_long area, max_area=0;
      int instance, best_instance=0;
      for (instance=0;
           !(layer_dims =
             compositor->find_layer_region(layer_idx,
                                           instance,true)).is_empty();
           instance++)
        {
          layer_region = layer_dims & region;
          area = layer_region.area();
          if (area > max_area)
            { max_area = area; best_instance = instance; }
        }
      if (max_area == 0)
        focus_anchors_known = enable_focus_box = false;
      else
        {
          layer_dims =
            compositor->find_layer_region(layer_idx,best_instance,false);
          layer_region = layer_dims & region;
          convert_region_to_focus_anchors(layer_region,layer_dims);
          focus_layer = layer_idx;
          focus_codestream = -1;
          view_centre_x = focus_centre_x; view_centre_y = focus_centre_y;
        }
    }
  int translate_codestream =
    (single_component_idx < 0)?-1:single_codestream_idx;
  if (layer_idx != single_layer_idx)
    {
      frame_idx = 0;
      frame_start = frame_end = 0.0;
    }
  else
    translate_codestream = -1;
  single_component_idx = -1;
  single_layer_idx = layer_idx;
  frame = NULL;
  if (translate_codestream && mj2_in.exists())
    {
      kdu_uint32 trk;
      int frm, fld;
      if (mj2_in.find_stream(translate_codestream,trk,frm,fld) &&
          (trk == (kdu_uint32)(single_layer_idx+1)))
        change_frame_idx(frm);
    }
  invalidate_configuration();
  initialize_regions();
}

/******************************************************************************/
/*                    CKdu_showApp::set_compositing_layer                     */
/******************************************************************************/

void
  CKdu_showApp::set_compositing_layer(kdu_coords point)
{
  if (compositor == NULL)
    return;
  point += view_dims.pos;
  int layer_idx, stream_idx;
  if (compositor->find_point(point,layer_idx,stream_idx) && (layer_idx >= 0))
    set_compositing_layer(layer_idx);
}


/******************************************************************************/
/*                        CKdu_showApp::show_metainfo                         */
/******************************************************************************/

bool
  CKdu_showApp::show_metainfo(kdu_coords point)
{
  if (compositor == NULL)
    {
      kill_metainfo();
      return false;
    }
  point += view_dims.pos;
  int codestream_idx; // Not used here.
  jpx_metanode node;
  if (overlays_enabled)
    node = compositor->search_overlays(point,codestream_idx);
  if ((!node) && jpx_in.exists())
    { // Search for global metadata
      int stream_idx, layer_idx;
      jpx_meta_manager manager = jpx_in.access_meta_manager();
      if (compositor->find_point(point,layer_idx,stream_idx))
        {
          manager.load_matches(1,&stream_idx,(layer_idx<0)?0:1,&layer_idx);
          if (layer_idx >= 0)
            node = manager.enumerate_matches(jpx_metanode(),-1,layer_idx,
                                             false,kdu_dims(),0,true);
          if (!node)
            node = manager.enumerate_matches(jpx_metanode(),stream_idx,-1,
                                             false,kdu_dims(),0,true);
        }
      if (!node)
        node = manager.enumerate_matches(jpx_metanode(),-1,-1,true,
                                         kdu_dims(),0,true);
      if (!node)
        node = manager.enumerate_matches(jpx_metanode(),-1,-1,false,
                                         kdu_dims(),0,true);
    }
  if (!node)
    {
      kill_metainfo();
      return false;
    }

  if (node != metainfo_node)
    { // New node
      metainfo_node = node;
      metainfo_label = NULL;
      int n, num_descendants=0;
      bool is_complete = node.count_descendants(num_descendants);
      metainfo_label = node.get_label(); // In case node is global
      for (n=0; (n < num_descendants) && (metainfo_label == NULL); n++)
        {
          jpx_metanode child = node.get_descendant(n);
          if (child.exists())
            metainfo_label = child.get_label();
          else
            is_complete = false;
        }
      if ((metainfo_label == NULL) && !is_complete)
        metainfo_node = jpx_metanode(NULL); // So we search again next time
    }

  if (metainfo_label != NULL)
    {
      suspend_processing(true);
      metainfo_show.show(metainfo_label,child_wnd,point,view_dims);
    }
  else
    {
      metainfo_show.hide();
      suspend_processing(false);
    }
  return true;
}

/******************************************************************************/
/*                        CKdu_showApp::kill_metainfo                         */
/******************************************************************************/

void
  CKdu_showApp::kill_metainfo()
{
  metainfo_label = NULL;
  metainfo_node = jpx_metanode();
  metainfo_show.hide();
  suspend_processing(false);
}

/******************************************************************************/
/*                        CKdu_showApp::edit_metainfo                         */
/******************************************************************************/

bool
  CKdu_showApp::edit_metainfo(kdu_coords point)
{
  if ((compositor == NULL) || !jpx_in)
    {
      kill_metainfo();
      return false;
    }
  point += view_dims.pos;
  int codestream_idx; // Not used here.
  jpx_metanode node;
  if (overlays_enabled)
    node = compositor->search_overlays(point,codestream_idx);
  if (!node)
    { // Search for global metadata
      int stream_idx, layer_idx;
      jpx_meta_manager manager = jpx_in.access_meta_manager();
      if (compositor->find_point(point,layer_idx,stream_idx))
        {
          manager.load_matches(1,&stream_idx,(layer_idx<0)?0:1,&layer_idx);
          if (layer_idx >= 0)
            node = manager.enumerate_matches(jpx_metanode(),-1,layer_idx,
                                             false,kdu_dims(),0,true);
          if (!node)
            node = manager.enumerate_matches(jpx_metanode(),stream_idx,-1,
                                             false,kdu_dims(),0,true);
        }
      if (!node)
        node = manager.enumerate_matches(jpx_metanode(),-1,-1,true,
                                         kdu_dims(),0,true);
     if (!node)
        node = manager.enumerate_matches(jpx_metanode(),-1,-1,false,
                                         kdu_dims(),0,true);
    }
  if (!node)
    {
      kill_metainfo();
      return false;
    }

  kd_metadata_editor editor;
  editor.init(node,!jpip_client.is_alive());
  INT_PTR result = editor.DoModal();
  if ((result == IDCANCEL) || ((result == IDOK) && !editor.any_change()))
    return true;
  if (result == IDOK)
    {
      bool is_elliptical = false;
      int num_codestreams;
      const int *codestream_indices = editor.get_codestreams(num_codestreams);
      int num_layers;
      const int *layer_indices = editor.get_layers(num_layers);
      try {
          jpx_meta_manager manager = jpx_in.access_meta_manager();
          jpx_roi roi; roi.is_encoded = false; roi.coding_priority = 0;
          int num_regions = 0;
          if (editor.get_region(roi.region,roi.is_elliptical))
            num_regions = 1;
          jpx_metanode new_node =
            manager.insert_node(num_codestreams,codestream_indices,
                                num_layers,layer_indices,
                                editor.get_rendered_result(),
                                num_regions,&roi);
          int n, num_descendants; node.count_descendants(num_descendants);
          if (node == new_node)
            { // Delete the first label from the existing node
              for (n=0; n < num_descendants; n++)
                {
                  jpx_metanode child = node.get_descendant(n);
                  if (child.exists() && (child.get_label() != NULL))
                    { child.delete_node(); break; }
                }
              node = jpx_metanode(NULL); // So we don't delete it
            }
          else
            { // Copy all but the first label from the original node
              bool found_first_label = false;
              for (n=0; n < num_descendants; n++)
                {
                  jpx_metanode child = node.get_descendant(n);
                  if (child.exists())
                    {
                      if ((child.get_label() != NULL) && !found_first_label)
                        found_first_label = true;
                      else
                        new_node.add_copy(child,true);
                    }
                }
            }

          const char *label = editor.get_label();
          if (*label != '\0')
            new_node.add_label(label);
        }
      catch(int)
        {} // Catches errors which might be generated if parameters supplied
           // to `jpx_meta_manager::insert_node' were inappropriate.
    }
  else
    assert(result == ID_META_DELETE);
  if (node.exists())
    node.delete_node();

  compositor->update_overlays(true);
  kill_metainfo();
  return true;
}

/******************************************************************************/
/*                       CKdu_showApp::OnMultiComponent                       */
/******************************************************************************/

void CKdu_showApp::OnMultiComponent() 
{
  if ((compositor == NULL) ||
      ((single_component_idx < 0) &&
       ((single_layer_idx < 0) || (num_frames == 0) || !jpx_in)))
    return;
  if ((num_frames == 0) || !jpx_in)
    OnCompositingLayer();
  else
    {
      calculate_view_anchors();
      single_component_idx = -1;
      single_layer_idx = -1;
      frame_idx = 0;
      frame_start = frame_end = 0.0;
      frame = NULL; // Let `initialize_regions' try to find it.
      frame_iteration = 0;
    }
  invalidate_configuration();
  initialize_regions();
}

/******************************************************************************/
/*                   CKdu_showApp::OnUpdateMultiComponent                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateMultiComponent(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) &&
                 ((single_component_idx >= 0) ||
                  ((single_layer_idx >= 0) && (num_frames != 0) &&
                    jpx_in.exists())));
  pCmdUI->SetCheck((compositor != NULL) &&
                   (single_component_idx < 0) &&
                   ((single_layer_idx < 0) || (num_frames == 0) || !jpx_in));
}
	
/******************************************************************************/
/*                    CKdu_showApp::OnCompositingLayer                        */
/******************************************************************************/

void
 CKdu_showApp::OnCompositingLayer() 
{
  if ((compositor == NULL) || (single_layer_idx >= 0))
    return;
  int layer_idx = 0;
  if (frame != NULL)
    { // See if we can find a good layer to go to
      kdu_coords point;
      if (this->enable_focus_box)
        {
          point.x = focus_box.pos.x + (focus_box.size.x>>1);
          point.y = focus_box.pos.y + (focus_box.size.y>>1);
        }
      else if (!view_dims.is_empty())
        {
          point.x = view_dims.pos.x + (view_dims.size.x>>1);
          point.y = view_dims.pos.y + (view_dims.size.y>>1);
        }
      int stream_idx; // Not actually used
      if (!(compositor->find_point(point,layer_idx,stream_idx) &&
            (layer_idx >= 0)))
        layer_idx = 0;
    }
  else if (mj2_in.exists() && (single_component_idx >= 0))
    { // Find layer based on the track to which the single component belongs
      kdu_uint32 trk;
      int frm, fld;
      if (mj2_in.find_stream(single_codestream_idx,trk,frm,fld) && (trk != 0))
        layer_idx = (int)(trk-1);
    }
  set_compositing_layer(layer_idx);
}

/******************************************************************************/
/*                 CKdu_showApp::OnUpdateCompositingLayer                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateCompositingLayer(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) && (single_layer_idx < 0));
  pCmdUI->SetCheck((compositor != NULL) && (single_layer_idx >= 0));
}

/******************************************************************************/
/*                        CKdu_showApp::OnTrackNext                           */
/******************************************************************************/

void CKdu_showApp::OnTrackNext()
{
  if ((compositor == NULL) || !mj2_in)
    return;
  if (single_layer_idx < 0)
    OnCompositingLayer();
  else
    {
      single_layer_idx++;
      if ((max_compositing_layer_idx >= 0) &&
          (single_layer_idx > max_compositing_layer_idx))
        single_layer_idx = 0;
      invalidate_configuration();
      initialize_regions();
    }
}

/******************************************************************************/
/*                     CKdu_showApp::OnUpdateTrackNext                        */
/******************************************************************************/

void CKdu_showApp::OnUpdateTrackNext(CCmdUI *pCmdUI)
{
  pCmdUI->Enable((compositor != NULL) && mj2_in.exists());
}

/******************************************************************************/
/*                        CKdu_showApp::OnComponent1                          */
/******************************************************************************/

void CKdu_showApp::OnComponent1() 
{
  if ((compositor == NULL) || (single_component_idx == 0))
    return;
  if (single_component_idx < 0)
    { // See if we can find a good codestream to go to
      kdu_coords point;
      if (this->enable_focus_box)
        {
          point.x = focus_box.pos.x + (focus_box.size.x>>1);
          point.y = focus_box.pos.y + (focus_box.size.y>>1);
        }
      else if (!view_dims.is_empty())
        {
          point.x = view_dims.pos.x + (view_dims.size.x>>1);
          point.y = view_dims.pos.y + (view_dims.size.y>>1);
        }
      int stream_idx, layer_idx;
      if (!compositor->find_point(point,layer_idx,stream_idx))
        stream_idx = 0;
      set_codestream(stream_idx);
    }
  else
    {
      single_component_idx = 0;
      frame = NULL;
      calculate_view_anchors();
      invalidate_configuration();
      initialize_regions();
    }
}

/******************************************************************************/
/*                     CKdu_showApp::OnUpdateComponent1                       */
/******************************************************************************/

void CKdu_showApp::OnUpdateComponent1(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) && (single_component_idx != 0));
  pCmdUI->SetCheck((compositor != NULL) && (single_component_idx == 0));
}

/******************************************************************************/
/*                       CKdu_showApp::OnComponentNext                        */
/******************************************************************************/

void CKdu_showApp::OnComponentNext()
{
  if ((compositor == NULL) ||
      ((max_components >= 0) && (single_component_idx >= (max_components-1))))
    return;
  if (single_component_idx < 0)
    { // See if we can find a good codestream to go to
      kdu_coords point;
      if (this->enable_focus_box)
        {
          point.x = focus_box.pos.x + (focus_box.size.x>>1);
          point.y = focus_box.pos.y + (focus_box.size.y>>1);
        }
      else if (!view_dims.is_empty())
        {
          point.x = view_dims.pos.x + (view_dims.size.x>>1);
          point.y = view_dims.pos.y + (view_dims.size.y>>1);
        }
      int stream_idx, layer_idx;
      if (!compositor->find_point(point,layer_idx,stream_idx))
        stream_idx = 0;
      set_codestream(stream_idx);
    }
  else
    {
      single_component_idx++;
      frame = NULL;
      calculate_view_anchors();
      invalidate_configuration();
      initialize_regions();
    }
}

/******************************************************************************/
/*                    CKdu_showApp::OnUpdateComponentNext                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateComponentNext(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) &&
                 ((max_components < 0) ||
                  (single_component_idx < (max_components-1))));
}

/******************************************************************************/
/*                       CKdu_showApp::OnComponentPrev                        */
/******************************************************************************/

void CKdu_showApp::OnComponentPrev() 
{
  if ((compositor == NULL) || (single_component_idx == 0))
    return;
  if (single_component_idx < 0)
    { // See if we can find a good codestream to go to
      kdu_coords point;
      if (this->enable_focus_box)
        {
          point.x = focus_box.pos.x + (focus_box.size.x>>1);
          point.y = focus_box.pos.y + (focus_box.size.y>>1);
        }
      else if (!view_dims.is_empty())
        {
          point.x = view_dims.pos.x + (view_dims.size.x>>1);
          point.y = view_dims.pos.y + (view_dims.size.y>>1);
        }
      int stream_idx, layer_idx;
      if (!compositor->find_point(point,layer_idx,stream_idx))
        stream_idx = 0;
      set_codestream(stream_idx);
    }
  else
    {
      single_component_idx--;
      frame = NULL;
      calculate_view_anchors();
      invalidate_configuration();
      initialize_regions();
    }
}

/******************************************************************************/
/*                    CKdu_showApp::OnUpdateComponentPrev                     */
/******************************************************************************/

void CKdu_showApp::OnUpdateComponentPrev(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) && (single_component_idx != 0));
}

/******************************************************************************/
/*                       CKdu_showApp::OnImageNext                            */
/******************************************************************************/

void
  CKdu_showApp::OnImageNext() 
{
  if (compositor == NULL)
    return;
  if (single_component_idx >= 0)
    { // Advance to next code-stream
      if ((max_codestreams > 0) &&
          (single_codestream_idx >= (max_codestreams-1)))
        return;
      single_codestream_idx++;
      single_component_idx = 0;
    }
  else if ((single_layer_idx >= 0) && !mj2_in)
    { // Advance to next compositing layer
      if ((max_compositing_layer_idx >= 0) &&
          (single_layer_idx >= max_compositing_layer_idx))
        return;
      single_layer_idx++;
    }
  else if (single_layer_idx >= 0)
    { // Advance to next frame in track
      if ((num_frames > 0) && (frame_idx == (num_frames-1)))
        return;
      change_frame_idx(frame_idx+1);
    }
  else
    { // Advance to next frame in animation
      if (frame == NULL)
        return; // Still have not been able to open the composition box
      if ((num_frames > 0) && (frame_idx >= (num_frames-1)))
        return;
      change_frame_idx(frame_idx+1);
    }
  calculate_view_anchors();
  invalidate_configuration();
  initialize_regions();
}

/******************************************************************************/
/*                    CKdu_showApp::OnUpdateImageNext                         */
/******************************************************************************/

void
  CKdu_showApp::OnUpdateImageNext(CCmdUI* pCmdUI) 
{
  if (single_component_idx >= 0)
    pCmdUI->Enable((compositor != NULL) &&
                   ((max_codestreams < 0) ||
                    (single_codestream_idx < (max_codestreams-1))));
  else if ((single_layer_idx >= 0) && !mj2_in)
    pCmdUI->Enable((compositor != NULL) &&
                   ((max_compositing_layer_idx < 0) ||
                    (single_layer_idx < max_compositing_layer_idx)));
  else if (single_layer_idx >= 0)
    pCmdUI->Enable((compositor != NULL) &&
                   ((num_frames < 0) || (frame_idx < (num_frames-1))));
  else
    pCmdUI->Enable((compositor != NULL) && (frame != NULL) &&
                   ((num_frames < 0) ||
                    (frame_idx < (num_frames-1))));
}

/******************************************************************************/
/*                       CKdu_showApp::OnImagePrev                            */
/******************************************************************************/

void
  CKdu_showApp::OnImagePrev() 
{
  if (compositor == NULL)
    return;
  if (single_component_idx >= 0)
    { // Go to previous code-stream
      if (single_codestream_idx == 0)
        return;
      single_codestream_idx--;
      single_component_idx = 0;
    }
  else if ((single_layer_idx >= 0) && !mj2_in)
    { // Go to previous compositing layer
      if (single_layer_idx == 0)
        return;
      single_layer_idx--;
    }
  else if (single_layer_idx >= 0)
    { // Go to previous frame in track
      if (frame_idx == 0)
        return;
      change_frame_idx(frame_idx-1);
    }
  else
    { // Go to previous frame
      if ((frame_idx == 0) || (frame == NULL))
        return;
      change_frame_idx(frame_idx-1);
    }
  calculate_view_anchors();
  invalidate_configuration();
  initialize_regions();
}

/******************************************************************************/
/*                    CKdu_showApp::OnUpdateImagePrev                         */
/******************************************************************************/

void
  CKdu_showApp::OnUpdateImagePrev(CCmdUI* pCmdUI) 
{
  if (single_component_idx >= 0)
    pCmdUI->Enable((compositor != NULL) && (single_codestream_idx > 0));
  else if ((single_layer_idx >= 0) && !mj2_in)
    pCmdUI->Enable((compositor != NULL) && (single_layer_idx > 0));
  else if (single_layer_idx >= 0)
    pCmdUI->Enable((compositor != NULL) && (frame_idx > 0));
  else
    pCmdUI->Enable((compositor != NULL) && (frame != NULL) && (frame_idx > 0));
}

/******************************************************************************/
/*                        CKdu_showApp::OnViewRefresh                         */
/******************************************************************************/

void CKdu_showApp::OnViewRefresh() 
{
  refresh_display();
}

/******************************************************************************/
/*                     CKdu_showApp::OnUpdateViewRefresh                      */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewRefresh(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(compositor != NULL);
}


/******************************************************************************/
/*                      CKdu_showApp::OnViewMetadata                          */
/******************************************************************************/

void CKdu_showApp::OnViewMetadata() 
{
  if (metashow != NULL)
    return;
  metashow = new kd_metashow(this);
  if (jp2_family_in.exists())
    {
      bool bin0_complete = true;
      if (jpip_client.is_active())
        jpip_client.get_databin_length(KDU_META_DATABIN,0,0,&bin0_complete);
      metashow->activate(&jp2_family_in,bin0_complete);
    }
}

/******************************************************************************/
/*                   CKdu_showApp::OnUpdateViewMetadata                       */
/******************************************************************************/

void CKdu_showApp::OnUpdateViewMetadata(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(metashow == NULL);
}

/******************************************************************************/
/*                         CKdu_showApp::OnMetaAdd                            */
/******************************************************************************/

void
  CKdu_showApp::OnMetaAdd() 
{
  if ((compositor == NULL) || (!jpx_in) || jpip_client.is_alive())
    return;
  kd_metadata_editor editor;
  int initial_codestream_idx=0;
  kdu_dims initial_region = focus_box;
  if (enable_focus_box &&
      compositor->map_region(initial_codestream_idx,initial_region))
    editor.init(initial_codestream_idx,initial_region);
  else if (single_component_idx >= 0)
    editor.init(single_codestream_idx,-1,false);
  else if (single_layer_idx >= 0)
    editor.init(-1,single_layer_idx,false);
  else
    editor.init(-1,-1,true);

  if (editor.DoModal() != IDOK)
    return;
  bool is_elliptical = false;
  int num_codestreams;
  const int *codestream_indices = editor.get_codestreams(num_codestreams);
  int num_layers;
  const int *layer_indices = editor.get_layers(num_layers);
  try {
      jpx_meta_manager manager = jpx_in.access_meta_manager();
      jpx_metanode node;
      jpx_roi roi; roi.is_encoded = false; roi.coding_priority = 0;
      int num_regions = 0;
      if (editor.get_region(roi.region,roi.is_elliptical))
        num_regions = 1;
      node = manager.insert_node(num_codestreams,codestream_indices,
                                 num_layers,layer_indices,
                                 editor.get_rendered_result(),
                                 num_regions,&roi);
      const char *label = editor.get_label();
      if (*label != '\0')
        node.add_label(label);
    }
  catch(int)
    {} // Catches errors which might be generated if parameters supplied
       // to `jpx_meta_manager::insert_node' were inappropriate.
  compositor->update_overlays(false);
}

/******************************************************************************/
/*                      CKdu_showApp::OnUpdateMetaAdd                         */
/******************************************************************************/

void
  CKdu_showApp::OnUpdateMetaAdd(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((compositor != NULL) && jpx_in.exists() &&
                 !jpip_client.is_alive());
}


// MJY 0908 
void CKdu_showApp::OnToolOverviewwindow()
{
	// TODO: Add your command handler code here
    if (m_pOverview != NULL)
		m_pOverview->SetFocus();
	else
	{
		m_pOverview = new COverview(this);
		initialize_overview_regions();
		m_pOverview->Invalidate();
	}
}

// MJY_091809
void CKdu_showApp::initialize_overview_regions(void)
{
	//kdu_dims overview_image_dims = image_dims;
	//TRACE("overview image %d %d\n", overview_image_dims.size.x, overview_image_dims.size.y);
	//compositor->set_scale(false, false, false, 0.2F);
	//compositor->get_total_composition_dims(overview_image_dims);
	//TRACE("overview image %d %d\n", overview_image_dims.size.x, overview_image_dims.size.y);
	////compositor->set_buffer_surface(overview_image_dims);
	////buffer = compositor->get_composition_bitmap(overview_image_dims);


	// MJY: initialize regions for overview

	// MJY:copy bitmap for overview from prepared buffer
	if (need_overview_map)
	{
	  int bitmap_row_gap;
  kdu_uint32 *bitmap_buffer;
  HBITMAP bitmap = buffer->access_bitmap(bitmap_buffer,bitmap_row_gap);

  	  	  memset(&overview_info,0,sizeof(overview_info));
  overview_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  overview_info.bmiHeader.biWidth = view_dims.size.x;
  overview_info.bmiHeader.biHeight = -view_dims.size.y;
  overview_info.bmiHeader.biPlanes = 1;
  overview_info.bmiHeader.biBitCount = 32;
  overview_info.bmiHeader.biCompression = BI_RGB;

  overviewmap = CreateDIBSection(NULL,&overview_info,DIB_RGB_COLORS,(void**)&overview_buffer,NULL,0);
  if (overviewmap == NULL)
    { kdu_error e; e << "Unable to allocate sufficient bitmap surfaces "
	"to service `kdu_region_compositor'."; }

  	kdu_uint32 *dptr, *ddptr = overview_buffer;
	kdu_uint32 *sptr, *ssptr = bitmap_buffer;
	dptr = ddptr; sptr = ssptr;

	for (int i=0; i<view_dims.size.x*view_dims.size.y; i++) 
		*(dptr++) = *(sptr++);
    SelectObject(overview_compatible_dc.m_hDC, overviewmap);



	need_overview_map = false;
	}


}
