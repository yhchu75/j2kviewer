#pragma once

#include "../LicenseModule/InnovaLicenseModule.h"

#define		INNOVAPLEX_CUSTOM_HEADER_VERSION 0x00010001
// 128 byte encryption data
//total size 1024 * 4 = 4096
struct INNOVAPLEXBoxHeader{
	int		version;
	int		dummy[1023];
};	

bool WriteInnovaplexCustomHeader(void* tgt /*jp2_family_tgt*/, INNOVAPLEXBoxHeader *header);
bool ReadInnovaplexCustomHeader(void* src /*jp2_family_src*/, SystemInfo *pSystemInfo, INNOVAPLEXBoxHeader *header);