#include "stdafx.h"
#include "VPViewerCore.h"
#include "kdu_show.h"


CVPViewerCore::CVPViewerCore()
{
	m_app = NULL;
	m_AnalyzeMode = AnalyzeMode_NONE;
	m_AddSectorRegionType = SectorRegionType_NONE;
}


CVPViewerCore::~CVPViewerCore()
{
}


int		CVPViewerCore::ZoomByIndex(int id)
{
	if (m_app == NULL) return -1;

	m_app->OnChangeRenderScale(id);

	return 0;
}

CDC*	CVPViewerCore::GetDC()
{
	return &m_app->child_wnd->m_BackbufferDC;
}

float	CVPViewerCore::GetScale()
{
	if (m_app == NULL) return -1;

	return m_app->m_config.getAppliedScale();
}


void	CVPViewerCore::SetAnalyzeMode(eAnalyzeMode mode)
{
	m_app->m_Analyzing = (AnalyzeMode_NONE != mode);
	m_app->m_pOverview->m_generalTab.SetAnalyzeMode(m_app->m_Analyzing);

	if (AnalyzeMode_GRID == mode)
		m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::GRID_ANALYZE);
	else if (AnalyzeMode_SECTOR == mode) {
		RECT rc;
		m_app->frame_wnd->GetClientRect(&rc);
		m_app->m_DrawObjMgr.GetGridObjList()[0]->SetSectorInterval(rc.right);
		m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::SECTOR_ANALYZE);
	}
	else
		m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::NONE);
	

	m_AnalyzeMode = mode;
	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
}


void	CVPViewerCore::SetGridRatio(int mode)
{
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetGridRatio((CGridObj::eGridRatioMode)mode);
	m_app->m_DrawObjMgr.GetGridObjList()[0]->UpdateSectorCellStatus();
	m_app->UpdateViewContent();
}


void	CVPViewerCore::GridOnButtonDown(UINT nFlags, CPoint point)
{
	if(m_AnalyzeMode == AnalyzeMode_GRID)
		m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::GRID_ANALYZE);
	else if (m_AnalyzeMode == AnalyzeMode_SECTOR) {
		if (m_AddSectorRegionType == SectorRegionType_RECT) {
			m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::NEW_SECTOR_RECT_REGION);
		}
		else if (m_AddSectorRegionType == SectorRegionType_ELLISPE) {
			m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::NEW_SECTOR_ELLIPSE_REGION);
		}
		else if (m_AddSectorRegionType == SectorRegionType_FREE) {
			m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::NEW_SECTOR_FREE_REGION);
		}
		else {
			m_app->m_DrawObjMgr.SetCurStatus(CDrawObjMgr::SELECT_SECTOR);
		}
	}
					
	m_app->m_DrawObjMgr.OnLButtonDown(nFlags, point);
}

void	CVPViewerCore::GridOnButtonMove(UINT nFlags, CPoint point)
{
	m_app->m_DrawObjMgr.OnMouseMove(nFlags, point);	
}

void	CVPViewerCore::GridOnButtonUp(UINT nFlags, CPoint point)
{
	m_app->m_DrawObjMgr.OnLButtonUp(nFlags, point);
	m_AddSectorRegionType = SectorRegionType_NONE;
}

void	CVPViewerCore::SetCenterPos(POINT p)
{
	POINT position = m_app->m_DrawObjMgr.GetGridObjList()[0]->GetPosition();

	POINT pt;
	pt.x = position.x -1 + p.x-1;
	pt.y = position.y -1 + p.y-1;

	POINTF step = m_app->m_DrawObjMgr.GetGridObjList()[0]->GetStep();
	POINTF pos;
	pos.x = (step.x * pt.x) + step.x * 0.5;
	pos.y = (step.y * pt.y) + step.y * 0.5;
	
	double interval = m_app->m_DrawObjMgr.GetGridObjList()[0]->getInfo()->interval;
	double render_scale = m_app->rendering_scale;
	double applied_scale = m_app->m_DrawObjMgr.GetGridObjList()[0]->getInfo()->ratio;

	CRect r;
	m_app->child_wnd->GetWindowRect(r);
	
	double frame = (r.Height() < r.Width()) ? r.Height() : r.Width();
	
	render_scale = frame * applied_scale / (interval * 1.05);

	m_app->set_center_pos(pos.x, pos.y, render_scale);
	m_app->UpdateViewContent();
}

void	CVPViewerCore::GetGridBox(POINT &pos, POINT &size)
{
	pos = m_app->m_DrawObjMgr.GetGridObjList()[0]->GetPosition();
	size = m_app->m_DrawObjMgr.GetGridObjList()[0]->GetSize();
}

void	CVPViewerCore::SetGridBox(POINT pos, POINT size)
{
	//if (!m_app->m_DrawObjMgr.GetGridObjList()[0]->isDrawing()){
		m_app->m_DrawObjMgr.GetGridObjList()[0]->SetPosition(pos.x, pos.y);
		m_app->m_DrawObjMgr.GetGridObjList()[0]->SetSize(size.x, size.y);
		m_app->UpdateViewContent();
	//}
}

void	CVPViewerCore::SetGridBoxArray(int *counterArray)
{
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetCounterArray(counterArray);
	m_app->UpdateViewContent();
}

void	CVPViewerCore::SetGridCrossHair(bool draw)
{
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetCross(draw);
	m_app->UpdateViewContent();
}

void	CVPViewerCore::SetGridRedCrossHair(bool draw)
{
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetRedCross(draw);
	m_app->UpdateViewContent();
}

void	CVPViewerCore::ShowGrid(bool bShow)
{
#if 0
	if (!m_app->m_DrawObjMgr.IsShowGridObj()){
		m_app->OnMeasurementGrid();
	}
#else
	m_app->m_Analyzing = bShow;
	if (bShow) {
		m_AnalyzeMode = AnalyzeMode_GRID;
		//m_app->OnMeasurementGrid();
	}
	else {
		m_AnalyzeMode = AnalyzeMode_NONE;
	}

	m_app->m_DrawObjMgr.ShowGridObj(bShow);
	m_app->m_pOverview->m_generalTab.SetAnalyzeMode(bShow);
	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
#endif
}


void	CVPViewerCore::ShowSector(bool bShow)
{
#if 0
	m_app->m_DrawObjMgr.ShowGridObj(true);
	m_app->UpdateViewContent();
#else
	m_app->m_Analyzing = bShow;
	if (bShow) {
		m_AnalyzeMode = AnalyzeMode_SECTOR;
	}
	else {
		m_AnalyzeMode = AnalyzeMode_NONE;
	}

	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetGridRatio(CGridObj::GRID_RATIO_16X9);
	m_app->m_DrawObjMgr.ShowGridObj(bShow);
	m_app->m_pOverview->m_generalTab.SetAnalyzeMode(bShow);
	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
#endif
}

void	CVPViewerCore::SetSectorZoomLevel(int ZoomLevel)
{
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetZoomLevel(ZoomLevel);
	m_app->UpdateViewContent();
}

TCHAR*	CVPViewerCore::GetFileName()
{
	return m_app->open_filename;
}

bool	CVPViewerCore::CropGridImage(POINT pt, const TCHAR* filename, int width, int height)
{
	POINTF step = m_app->m_DrawObjMgr.GetGridObjList()[0]->GetStep();
	POINTF ptf1, ptf2;
	ptf1.x = (pt.x - 1) * step.x;
	ptf1.y = (pt.y - 1) * step.y;

	ptf2.x = (pt.x - 1 + width) * step.x;
	ptf2.y = (pt.y - 1 + height) * step.y;

	bool ret = m_app->UpdateCropImage(ptf1, ptf2, m_app->single_layer_idx, filename);

	m_app->RestoreViewRegion();

	return ret;
}

int		CVPViewerCore::GetRotate()
{
	if (m_app == NULL)
		return -1;
	return m_app->m_rotationStatus;
}

int		CVPViewerCore::GetInterval()
{
	if (m_app == NULL)
		return -1;
	return m_app->m_DrawObjMgr.GetGridObjList()[0]->getInfo()->interval;
}

void	CVPViewerCore::SetGridProperty(int rotate, float scale, int interval)
{

	m_app->OnRedisplayRotate((eRotateStatus)( rotate / 90));

	m_app->m_DrawObjMgr.UpdateAppliedScale(scale);
	m_app->m_config.setAppliedScale(scale);

	kdu_dims new_buffer_dims;
	new_buffer_dims.size = m_app->view_dims.size;
	new_buffer_dims.pos = m_app->buffer_dims.pos;
	m_app->UpdateViewDim(new_buffer_dims, true);

	m_app->m_DrawObjMgr.GetGridObjList()[0]->getInfo()->interval = interval;
	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
}


RECT	CVPViewerCore::GetFrameRect()
{
	return m_app->m_DrawObjMgr.GetFrameRect();
}


void	CVPViewerCore::SetSectorRegionMode(eSectorRegionMode mode)
{
	if (IVPViewerCore::SectorMode_RegionDraw == mode) {
		m_app->m_DrawObjMgr.GetGridObjList()[0]->SetAnalysisMode(CGridObj::GRID_ANALYSIS_REGION_DRAW);
	}
	else if (IVPViewerCore::SectorMode_SectorAnalyze == mode) {
		m_app->m_DrawObjMgr.GetGridObjList()[0]->SetAnalysisMode(CGridObj::GRID_ANALYSIS_REGION_ANALYSIS);
	}
	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
}


void	CVPViewerCore::SetSectorRegionDrawMode(eSectorRegionDrawMode mode)
{	
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetAnalysisRegionMode((mode == SectorMode_RegionDrawModeAll));
	m_app->m_DrawObjMgr.GetGridObjList()[0]->UpdateSectorCellStatus();
	m_AddSectorRegionType = SectorRegionType_NONE;

	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
}


void	CVPViewerCore::AddSectorRegion(eSectorRegionType type)
{
	//m_app->m_DrawObjMgr.SetCurStatus(type);
	m_AddSectorRegionType = type;

	AfxGetMainWnd()->SetActiveWindow();
	//m_app->frame_wnd->SetFocus();
	m_app->UpdateViewContent();
	m_app->m_pOverview->Invalidate();
}


int		CVPViewerCore::GetRegionCount()
{
	return m_app->m_DrawObjMgr.GetGridObjList()[0]->GetCellCount();
}


int		CVPViewerCore::MoveSector(int index, int init)
{
	//1. Get 
	if (index >= m_app->m_DrawObjMgr.GetGridObjList()[0]->GetRegionCellInfo().size()) {
		return -1;
	}
		
	POINTF ptf;
	float scale;
	m_app->m_DrawObjMgr.GetGridObjList()[0]->MoveSectorCentorPos(index, &ptf, &scale);
	m_app->set_center_pos(ptf.x, ptf.y, scale);
	m_app->m_DrawObjMgr.GetGridObjList()[0]->UpdateGridCellStatus(true);

	return index;
}


std::vector<GridCellInfo>& CVPViewerCore::GetSectorCellInfo()
{
	return m_app->m_DrawObjMgr.GetGridObjList()[0]->GetSectorCellInfo();
}


int 	CVPViewerCore::SetSectorCellInfo(CPoint point, int fossil_index, DWORD color)
{
	return m_app->m_DrawObjMgr.GetGridObjList()[0]->SetSectorCellInfo(point, fossil_index, color);
}


int 	CVPViewerCore::SetSectorCellInfo(int cell_index, int fossil_index, DWORD color)
{
	m_app->m_DrawObjMgr.GetGridObjList()[0]->SetSectorCellInfo(cell_index, fossil_index, color);
	m_app->UpdateViewContent();
	return 0;
}


void 	CVPViewerCore::ResetAnalyze()
{
	m_app->m_DrawObjMgr.DeleteObj();
	m_app->m_DrawObjMgr.GetGridObjList()[0]->Reset();
	m_app->UpdateViewContent();
}


int 	CVPViewerCore::GetTotalSectorCount()
{
	return m_app->m_DrawObjMgr.GetGridObjList()[0]->GetRegionCellInfo().size();
}


int 	CVPViewerCore::GetTotalCellCount()
{
	return m_app->m_DrawObjMgr.GetGridObjList()[0]->GetSectorCellInfo().size();
}
