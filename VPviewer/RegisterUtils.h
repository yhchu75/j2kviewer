#pragma once


BOOL	RegisterProtocols(const char *executable_path);
BOOL	RegisterFileExtension(const char* executable_path, const char *file_extension);
