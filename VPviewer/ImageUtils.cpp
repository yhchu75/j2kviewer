#include <stdafx.h>
#include <tmmintrin.h>


#include <intrin.h>
namespace Avx2
{
	enum Level
	{
		Ordinary = 1, Extended = 7,
	};

	enum Register
	{
		Eax = 0, Ebx = 1, Ecx = 2, Edx = 3,
	};

	enum Bit
	{
		// Ordinary:
		// Edx:
		SSE = 1 << 25,
		SSE2 = 1 << 26,

		// Ecx:
		SSSE3 = 1 << 9,
		SSE41 = 1 << 19,
		SSE42 = 1 << 20,
		OSXSAVE = 1 << 27,
		AVX = 1 << 28,

		// Extended:
		// Ebx:
		AVX2 = 1 << 5,
		AVX512F = 1 << 16,
		AVX512BW = 1 << 30,

		// Ecx:
		AVX512VBMI = 1 << 1,
	};

	inline bool CheckBit(Level level, Register index, Bit bit)
	{
		unsigned int registers[4] = { 0, 0, 0, 0 };
		__cpuid((int*)registers, level);
		return (registers[index] & bit) == bit;
	}

	inline bool SupportedByCPU()
	{
		return 	CheckBit(Ordinary, Ecx, OSXSAVE) && CheckBit(Extended, Ebx, AVX2);
	}
	/* Support windows 7 sp1
	inline bool SupportedByOS()
	{
	__try
	{
	__m256i value = _mm256_abs_epi8(_mm256_set1_epi8(1));// try to execute of AVX2 instructions;
	return true;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	return false;
	}
	}

	const bool Enable = SupportedByCPU() && SupportedByOS();
	*/

	const bool Enable = SupportedByCPU();
}

void	MixLayerSSE(int data_len, unsigned char* in_src1, unsigned char* in_src2, unsigned char* out_dst, int alpha);
void	MixLayerAVX2(int data_len, unsigned char* in_src1, unsigned char* in_src2, unsigned char* out_dst, int alpha);

void	MixLayer(int data_len, unsigned char* in_src1, unsigned char* in_src2, unsigned char* out_dst, int alpha)
{
	if (Avx2::Enable){
		MixLayerAVX2(data_len, in_src1, in_src2, out_dst, alpha);
	}
	else{
		MixLayerSSE(data_len, in_src1, in_src2, out_dst, alpha);
	}
}

