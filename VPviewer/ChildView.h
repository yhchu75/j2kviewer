/******************************************************************************/
// File: ChildView.h [scope = APPS/SHOW]
// Version: Kakadu, V6.0
// Author: David Taubman
// Last Revised: 12 August, 2007
/*****************************************************************************/
// Copyright 2001, David Taubman, The University of New South Wales (UNSW)
// The copyright owner is Unisearch Ltd, Australia (commercial arm of UNSW)
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Jangsoon Lee
// License number: 00842
// The licensee has been granted a NON-COMMERCIAL license to the contents of
// this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to install and use the Kakadu software and
//    to develop Applications for the Licensee's own use.
// 2. The Licensee has the right to Deploy Applications built using the
//    Kakadu software to Third Parties, so long as such Deployment does not
//    result in any direct or indirect financial return to the Licensee or
//    any other Third Party, which further supplies or otherwise uses such
//    Applications.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party, provided the Third Party possesses a license to use the Kakadu
//    software, and provided such distribution does not result in any direct
//    or indirect financial return to the Licensee.
/*******************************************************************************
Description:
   MFC-based class definitions and message mapping macros for the single child
view window in the interactive JPEG2000 image viewer, "kdu_show".
*******************************************************************************/

#if !defined(AFX_CHILDVIEW_H__9C359608_5C42_42FF_BE3C_E011ABAAF4AA__INCLUDED_)
#define AFX_CHILDVIEW_H__9C359608_5C42_42FF_BE3C_E011ABAAF4AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


using namespace kdu_supp; // Also includes the `kdu_core' namespace

//MJY_020509 : measurement
// Hints for UpdateAllViews/OnUpdate
#define HINT_UPDATE_WINDOW      0
#define HINT_UPDATE_DRAWOBJ     1
#define HINT_UPDATE_SELECTION   2
#define HINT_DELETE_SELECTION   3
#define HINT_UPDATE_OLE_ITEMS   4



class CKdu_showApp;// Forward declaration.
class CDrawObjMgr;
class CToolPlugInsMgr;
class CMeasureTable;
class CRGBMeasureTable;
/////////////////////////////////////////////////////////////////////////////
// CChildView window

class CChildView : public CWnd
{
public:
  CChildView();
  virtual ~CChildView();

	// Generated message map functions
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
protected:
	//{{AFX_MSG(CChildView)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnPaint();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg int	OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point); 
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
// ----------------------------------------------------------------------------
public: // Access methods for use by the frame window
  void set_app(CKdu_showApp *app);
  void set_max_view_size(kdu_coords size, int pixel_scale);
    /* Called by the application object whenever the image dimensions
       change for one reason or another.  This is used to prevent the
       window from growing larger than the dimensions of the image (unless
       the image is too tiny).  During periods when no image is loaded, the
       maximum view size should be set sufficiently large that it does not
       restrict window resizing.  The `size' value refers to the number
       of image pixels which are available for display.  Each of these
       occupies `pixel_scale' by `pixel_scale' display pixels.*/
  void set_scroll_metrics(kdu_coords step, kdu_coords page, kdu_coords end)
    { /* This function is called by the application to set or adjust the
         interpretation of incremental scrolling commands.  In particular, these
         parameters are adjusted whenever the window size changes.
            The `step' coordinates identify the amount to scroll by when an
         arrow key is used, or the scrolling arrows are clicked.
            The `page' coordinates identify the amount to scroll by when the
         page up/down keys are used or the empty scroll bar region is clicked.
            The `end' coordinates identify the maximum scrolling position. */
      scroll_step = step;
      scroll_page = page;
      scroll_end = end;
    }
  void check_and_report_size();
    /* Called when the window size may have changed or may need to be
       adjusted to conform to maximum dimensions established by the
       image -- e.g., called from inside the frame's OnSize message handler,
       or when the image dimensions may have changed. */
  void cancel_focus_drag();
    /* Called if the user cancels an in-progress focus box definition
       operation (mouse click down, drag, mouse click again to finish)
       by pressing the escape key, or any other method which may be
       provided. */

  

private:
	CKdu_showApp *app;
	int pixel_scale; // Display pixels per image pixel
  int last_pixel_scale; // So we can resize display in `check_and_report_size'
  kdu_coords screen_size; // Dimensions of the display
  kdu_coords max_view_size; // Max size for client area (in display pixels)
  kdu_coords last_size;
  bool sizing; // Flag to prevent recursive calls to resizing functions.
  kdu_coords scroll_step, scroll_page, scroll_end;
  kdu_coords last_mouse_point; // Last known mouse location

  bool focusing; // True only when defining focus box region with mouse clicks
  kdu_coords focus_start; // Coordinates of start point in focusing operation
  kdu_coords focus_end; // Coordinates of end point in focusing operation
  RECT focus_rect; // Most recent focus box during focusing.

  bool panning;
  CPoint mouse_pressed_point;
  bool crop;
  bool cropping;
  bool cropped;
  CPoint crop_point;
  CPoint mouse_point;

  BOOL analyze;



public:
  HCURSOR normal_cursor;
  HCURSOR cross_cursor;
  HCURSOR panning_cursor;
  HCURSOR overlay_cursor; // Cursor used when mouse is over a metadata overlay
  HCURSOR beam_cursor;

// Operations
public:
	void DocToClient(CRect& rect);
	void DocToClient(CPoint& point);
	void ClientToDoc(CPoint& point);
	void ClientToDoc(CRect& rect);
	void SelectWithinRect(CRect rect, BOOL bAdd = FALSE);
	void CloneSelection();
	void UpdateActiveItem();
	void PasteNative(COleDataObject& dataObject);
	void PasteEmbedded(COleDataObject& dataObject, CPoint point );
	void OnUpdate(LPARAM lHint, CObject* pHint);
	BOOL IsSelected(const CObject* pDocItem) const;
	void ClientToFullImage(CPoint& point);
	void ClientToFullImage(CRect& rect);
	void FullImageToClient(CPoint& point);
	void FullImageToClient(CRect& rect);

	void UpdateViewRegion(CDC *dc = NULL);
	void UpdateSelectRegion(POINTF	pt1, POINTF	pt2);

// Attributes
public:
	CDrawObjMgr*		m_pDrawObjMgr;
	CToolPlugInsMgr*	m_pToolPlugInsMgr;
	CDC					m_BackbufferDC; // Compatible DC for use with BitBlt function

	bool				m_adjustment;
	COLORADJUSTMENT		m_caHalf;

	// for measurement
	bool		m_bMeasureTable;
	bool		m_bRGBMeasureTable;
	CMeasureTable	*m_pMeasureTable; //MJY:021609
	CRGBMeasureTable *m_pRGBMeasureTable;
	void		updateMeasurementTableData();
	void		updateRGBMeasurementTableData();

	// for Full Screen
	bool	m_bFullScreen;
	CWnd	*m_ParentWnd;

public:
  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
  afx_msg void OnDelete();
  afx_msg void OnScoopLeft();
  afx_msg void OnScoopRight();
  afx_msg void OnScoopDown();
  afx_msg void OnScoopUp();
  afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnEditCopyviewtoclipboard();
  afx_msg void OnEditExportASJPEG();
  afx_msg void OnEditCropImage();
  afx_msg void OnAnalyzeImage();
  afx_msg void OnMeasurementSelect();
  afx_msg void OnMeasurementLine();
  afx_msg void OnMeasurementCurve();
  afx_msg void OnMeasurementArea();
  afx_msg void OnMeasurementLabel();
  afx_msg void OnShowLabel();
  afx_msg void OnUpdateMeasurementSelect(CCmdUI *pCmdUI);
  afx_msg void OnUpdateMeasurementLine(CCmdUI *pCmdUI);
  afx_msg void OnUpdateMeasurementCurve(CCmdUI *pCmdUI);
  afx_msg void OnUpdateMeasurementArea(CCmdUI *pCmdUI);
  afx_msg void OnUpdateMeasurementLabel(CCmdUI *pCmdUI);
  afx_msg void OnUpdateShowLabel(CCmdUI *pCmdUI);
  afx_msg void OnEditClear();
  afx_msg void OnMeasurementTable();
  afx_msg void OnUpdateMeasurementTable(CCmdUI *pCmdUI);
  afx_msg void OnMeasurementShow();
  afx_msg void OnUpdateMeasurementShow(CCmdUI *pCmdUI);
  afx_msg void OnMeasurementScaleBar();
  afx_msg void OnUpdateMeasurementScaleBar(CCmdUI *pCmdUI);
  afx_msg void OnAutoUnit();
  afx_msg void OnUpdateAutoUnit(CCmdUI *pCmdUI);
  afx_msg void OnViewFullscreen();
  afx_msg void OnViewBackfromfull();
  afx_msg void OnUpdateEditCopyviewtoclipboard(CCmdUI *pCmdUI);
  afx_msg void OnUpdateEditCropImage(CCmdUI *pCmdUI);
  afx_msg void OnUpdateAnalyzeImage(CCmdUI *pCmdUI);
  afx_msg void OnUpdateViewFullscreen(CCmdUI *pCmdUI);
  afx_msg void OnMeasurementRefreshtable();
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnShowToolbar();
  afx_msg void OnUpdateToolbar(CCmdUI *pCmdUI);

  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnMeasurementDeleteall();
  afx_msg void OnUpdateMeasurementDeleteall(CCmdUI *pCmdUI);
  afx_msg void OnMeasurementRgbTable();
  afx_msg void OnUpdateMeasurementRgbTable(CCmdUI *pCmdUI);


  int m_nShowRGB = 0;
  void OnMeasurementRgb();

  afx_msg void OnFitScreen();
  afx_msg void OnUpdateFitScreen(CCmdUI *pCmdUI);
  afx_msg void OnExportMeasurementAsSVG();
  afx_msg void OnUpdateExportMeasurementAsSVG(CCmdUI *pCmdUI);
  afx_msg void OnShowRGBMeasurement();
  afx_msg void OnUpdateShowRGBMeasurement(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__9C359608_5C42_42FF_BE3C_E011ABAAF4AA__INCLUDED_)
