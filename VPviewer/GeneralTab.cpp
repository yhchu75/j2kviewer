// GeneralTab.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "ChildView.h"
#include "Overview.h"
#include "GeneralTab.h"


// CGeneralTab dialog

IMPLEMENT_DYNAMIC(CGeneralTab, CDialog)

CGeneralTab::CGeneralTab(CWnd* pParent /*=NULL*/)
	: CDialog(CGeneralTab::IDD, pParent)
{
	//m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	m_bInit = false;
}

CGeneralTab::~CGeneralTab()
{
}

BOOL CGeneralTab::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	m_sliderMix.SetRange(0,100);
	m_sliderMix.SetPos(0);
	m_sliderMix.SetLineSize(1);
	m_sliderMix.SetPageSize(10);
		
	if (app->max_compositing_layer_idx >= 1)
	{
		//add layer 
		TCHAR strLayerName[64];
		ImageExternalInfo *pImageExternalInfo = app->m_config.GetImageExternalInfo();
		for (int i = 0; i <= app->max_compositing_layer_idx; i++){
			if (pImageExternalInfo->layers.size()){
				for (int j = 0; j < pImageExternalInfo->layers.size(); j++){
					if (pImageExternalInfo->layers[i].index == (i + 1)){
						if (pImageExternalInfo->layers[i].name.size() == 0){
							_stprintf(strLayerName, _T("Layer %02d"), i + 1);
						}
						else{
							int k;
							for (k = 0; k < pImageExternalInfo->layers[i].name.size(); k++)
								strLayerName[k] = pImageExternalInfo->layers[i].name[k];
							strLayerName[k] = 0;
						}
						break;
					}
				}
				
			}
			else{
				_stprintf(strLayerName, _T("Layer %02d"), i + 1);
			}
			m_LayerList1.AddString(strLayerName);
			m_LayerList2.AddString(strLayerName);
		}

		m_LayerList1.SetCurSel(0);
		m_LayerList2.SetCurSel(1);

		m_nLastSelectLayer = 1;
	}
	else{
		m_nLastSelectLayer = 1;
		
		m_LayerList2.EnableWindow(FALSE);
		m_sliderMix.EnableWindow(FALSE);
	}

	m_sliderFocus.SetRange(0, app->max_compositing_layer_idx);
	m_sliderFocus.SetPos(0);
	UpdateFocusInfo(1);
	m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	m_bInit = true;
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CGeneralTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MIX_SLIDER, m_sliderMix);
	DDX_Control(pDX, IDC_FOCUS_SLIDER, m_sliderFocus);
	DDX_Control(pDX, IDC_COMBO_LAYER1, m_LayerList1);
	DDX_Control(pDX, IDC_COMBO_LAYER2, m_LayerList2);
}

BEGIN_MESSAGE_MAP(CGeneralTab, CDialog)
	ON_WM_SHOWWINDOW()
	ON_WM_HSCROLL()
	ON_CONTROL_RANGE(BN_CLICKED, IDC_BUTTON_2x, IDC_BUTTON_80x, OnBnClickedButtonZoom)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_ROTATION_0, IDC_ROTATION_270, OnBnClickedButtonRotate)
	ON_BN_CLICKED(IDC_BUTTON_FIT, OnClickedButtonFit)
	ON_CBN_SELCHANGE(IDC_COMBO_LAYER1, OnCbnSelchangeComboLayer1)
	ON_CBN_SELCHANGE(IDC_COMBO_LAYER2, OnCbnSelchangeComboLayer2)
END_MESSAGE_MAP()


// CGeneralTab message handlers
void CGeneralTab::OnBnClickedButtonRotate(UINT id)
{	
	eRotateStatus status = eRotateStatus(id - IDC_ROTATION_0);
	if (app->m_rotationStatus == status) return;
	app->OnRedisplayRotate(status);
	
	for (int i = 0; i < 4; i++){
		GetDlgItem(IDC_ROTATION_0 + i)->EnableWindow((i != status));
	}
}

void CGeneralTab::SetAnalyzeMode(bool mode)
{
	if (mode)
	{
		eRotateStatus status = eRotateStatus(0);
		app->OnRedisplayRotate(status);
		for (int i = 0; i < 4; i++){
			GetDlgItem(IDC_ROTATION_0 + i)->EnableWindow((false));
		}
	}
	else
	{
		eRotateStatus status = eRotateStatus(0);
		app->OnRedisplayRotate(status);
		for (int i = 0; i < 4; i++){
			GetDlgItem(IDC_ROTATION_0 + i)->EnableWindow((i != status));
		}
	}
}


void CGeneralTab::OnBnClickedButtonZoom(UINT id)
{
	GetDlgItem(IDC_BUTTON_80x)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_40x)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_20x)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_10x)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_5x)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_2x)->EnableWindow(TRUE);

	UINT RenderingID = (id - IDC_BUTTON_2x) + 1;
	app->OnChangeRenderScale(RenderingID);
	GetDlgItem(id)->EnableWindow(FALSE);
}


BOOL CGeneralTab::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (m_hAccel != NULL){
		if ((pMsg->message == WM_KEYDOWN)){
			if (VK_LEFT == pMsg->wParam || VK_RIGHT == pMsg->wParam || VK_PRIOR == pMsg->wParam || VK_NEXT == pMsg->wParam)
				return CDialog::PreTranslateMessage(pMsg);
		}
		else{
			if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
				return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void	CGeneralTab::UpdateFocusInfo(int index)
{
	TCHAR FocusInfo[32];
	_stprintf(FocusInfo, _T("%d/%d"), index, app->max_compositing_layer_idx + 1);
	SetDlgItemText(IDC_STATIC_FOCUS, FocusInfo);
}


//define kdu_show
void CGeneralTab::UpdateCurrentScaleStatus()
{
	if (!m_bInit) return;
	
	for (int i = 1; i < CKdu_showApp::MAX_RENDER_SCALE ; i++){
		GetDlgItem(IDC_BUTTON_2x + i -1)->EnableWindow(!(app->rendering_scale == FIX_SCALE_VALUE[i]));
	}
}


void CGeneralTab::OnHScroll(UINT /*nSBCode*/, UINT /*nPos*/, CScrollBar* pScrollBar)
{
	CSliderCtrl *p_sliderCtrl = (CSliderCtrl *) pScrollBar;
	if (app->max_compositing_layer_idx == 0)return;

	if ( p_sliderCtrl == &m_sliderMix ){
		int	pos = p_sliderCtrl->GetPos();
		TRACE("pos : %d, single_layer_idx : %d, app->second_mix_layer_idx : %d mixing : %d\n",
			pos, app->single_layer_idx, app->second_mix_layer_idx, app->m_bMixing);

		int first_layer = m_LayerList1.GetCurSel();
		int second_layer = m_LayerList2.GetCurSel();
		bool bOldMixing = app->m_bMixing;

		if (first_layer == second_layer) return ;
		if (pos == 0 || pos == 100)
		{
			app->m_bMixing = false;
			app->ChangeSingleLayerIndex(!(pos == 0), (pos == 0) ? first_layer : second_layer);
			return;
		}

		app->m_bMixing = true;
		if (app->second_mix_layer_idx == -1){	//load second mix layer
			app->ChangeSingleLayerIndex(1, second_layer, false, false);
		}

		if (pos != app->m_MixingRatio){
			app->m_MixingRatio = pos;
			if (app->m_bMixing != bOldMixing){
				app->refresh_display();
			}
			else{
				app->MixingLayer();
				app->child_wnd->UpdateViewRegion();
			}
		}
	}
	else if (p_sliderCtrl == &m_sliderFocus){
		int	pos = p_sliderCtrl->GetPos();
		UpdateFocusInfo(pos+1);
		app->ChangeSingleLayerIndex(0, pos, true, false);
		/*
		if (pos < MAX_LAYER_CNT){
			OnBnClickedButtonFocus(pos + IDC_CHECK_FOCUS1);
		}
		else{


		}
		*/
	}
}


void CGeneralTab::OnClickedButtonFit()
{
	// TODO: Add your control notification handler code here
	app->FitImageToClient();
}


void CGeneralTab::OnCbnSelchangeComboLayer1()
{
	m_sliderMix.SetPos(0);
	app->m_bMixing = false;
	//if (m_LayerList1.GetCurSel() != m_LayerList2.GetCurSel()){
		app->ChangeSingleLayerIndex(0, m_LayerList1.GetCurSel());
	//}
	app->child_wnd->UpdateViewRegion();
}


void CGeneralTab::OnCbnSelchangeComboLayer2()
{
	m_sliderMix.SetPos(100);
	app->m_bMixing = false;
	//if (m_LayerList1.GetCurSel() != m_LayerList2.GetCurSel()){
		app->ChangeSingleLayerIndex(1, m_LayerList2.GetCurSel());
	//}
	app->child_wnd->UpdateViewRegion();
}
