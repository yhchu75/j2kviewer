#pragma once
#include "IVPViewerCore.h"

class CKdu_showApp;
class CVPViewerCore : public IVPViewerCore
{
public:
	CVPViewerCore();
	~CVPViewerCore();

	void	set_app(CKdu_showApp *app){ m_app = app; }
	CDC*	GetDC();
	
	int		ZoomByIndex(int id);
	eAnalyzeMode	GetAnalyzeMode() { return m_AnalyzeMode; }
	float	GetScale();
		
	void	SetAnalyzeMode(eAnalyzeMode mode);
	void	GridOnButtonDown(UINT nFlags, CPoint point);
	void	GridOnButtonMove(UINT nFlags, CPoint point);
	void	GridOnButtonUp(UINT nFlags, CPoint point);
	void	GetGridBox(POINT &pos, POINT &size);
	void	SetGridBox(POINT pos, POINT size);
	void	SetCenterPos(POINT p);
	void	SetGridBoxArray(int* counterArray);
	void	SetGridCrossHair(bool draw);
	void	SetGridRedCrossHair(bool draw);

	void	ShowGrid(bool bShow);

	TCHAR*	GetFileName();
	bool	CropGridImage(POINT pt, const TCHAR* filename, int width = 1, int height = 1);
	int		GetRotate();
	int		GetInterval();
	void	SetGridProperty(int rotation, float scale, int interval);
	RECT	GetFrameRect();

	void	ShowSector(bool bShow);
	void	SetGridRatio(int mode);
	void	SetSectorZoomLevel(int ZoomLevel);
	void	SetSectorRegionMode(eSectorRegionMode mode);
	void	SetSectorRegionDrawMode(eSectorRegionDrawMode mode);
	void	AddSectorRegion(eSectorRegionType type);

	std::vector<GridCellInfo>& GetSectorCellInfo();
	int		GetRegionCount();
	int		MoveSector(int index, int init);
	int 	SetSectorCellInfo(CPoint point, int fossil_index, DWORD color);
	int 	SetSectorCellInfo(int cell_index, int fossil_index, DWORD color);

	int 	GetTotalSectorCount();
	int 	GetTotalCellCount();

	void 	ResetAnalyze();
private:

	CKdu_showApp		*m_app;
	eAnalyzeMode		m_AnalyzeMode;
	eSectorRegionType	m_AddSectorRegionType;
};

