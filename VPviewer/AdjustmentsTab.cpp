// AdjustmentsTab.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "AdjustmentsTab.h"


// CAdjustmentsTab dialog

IMPLEMENT_DYNAMIC(CAdjustmentsTab, CDialog)

CAdjustmentsTab::CAdjustmentsTab(CWnd* pParent /*=NULL*/)
	: CDialog(CAdjustmentsTab::IDD, pParent)
{

}

CAdjustmentsTab::~CAdjustmentsTab()
{
}

void CAdjustmentsTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_BRIGHTNESS, m_staticBrightness);
	DDX_Control(pDX, IDC_STATIC_CONTRAST, m_staticContrast);
	DDX_Control(pDX, IDC_STATIC_SATURATION, m_staticSaturation);
	DDX_Control(pDX, IDC_SLIDER_BRIGHTNESS, m_sliderBrightness);
	DDX_Control(pDX, IDC_SLIDER_CONTRAST, m_sliderContrast);
	DDX_Control(pDX, IDC_SLIDER_SATURATION, m_sliderSaturation);
}

BEGIN_MESSAGE_MAP(CAdjustmentsTab, CDialog)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_RESET_BRIGHTNESS, &CAdjustmentsTab::OnBnClickedResetBrightness)
	ON_BN_CLICKED(IDC_RESET_CONTRAST, &CAdjustmentsTab::OnBnClickedResetContrast)
	ON_BN_CLICKED(IDC_RESET_SATURATION, &CAdjustmentsTab::OnBnClickedResetSaturation)
END_MESSAGE_MAP()


// CAdjustmentsTab message handlers

BOOL CAdjustmentsTab::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (m_hAccel != NULL)
		if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
			return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CAdjustmentsTab::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_brightness = app->child_wnd->m_caHalf.caBrightness = app->m_config.GetImageExternalInfo()->brightness;
	m_contrast = app->child_wnd->m_caHalf.caContrast = app->m_config.GetImageExternalInfo()->contrast;
	m_saturation = app->child_wnd->m_caHalf.caColorfulness = app->m_config.GetImageExternalInfo()->saturation;

	// TODO:  Add extra initialization here
	CString strSize;
	strSize.Format(_T("%d"), m_brightness);
	m_staticBrightness.SetWindowText(strSize);

	strSize.Format(_T("%d"), m_contrast);
	m_staticContrast.SetWindowText(strSize);

	strSize.Format(_T("%d"), m_saturation);
	m_staticSaturation.SetWindowText(strSize);

	m_sliderBrightness.SetRange(0,200);
	m_sliderContrast.SetRange(0,200);
	m_sliderSaturation.SetRange(0,200);
	
	if (m_brightness == 0 && m_contrast == 0 && m_saturation == 0)
		app->child_wnd->m_adjustment = false;

	else
		app->child_wnd->m_adjustment = true;

	app->child_wnd->UpdateViewRegion();

	m_sliderBrightness.SetPos(100+m_brightness);
	m_sliderContrast.SetPos(100+m_contrast);
	m_sliderSaturation.SetPos(100+m_saturation);

	m_sliderBrightness.SetPageSize(10);
	m_sliderContrast.SetPageSize(10);
	m_sliderSaturation.SetPageSize(10);
	
	m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAdjustmentsTab::OnHScroll(UINT /*nSBCode*/, UINT /*nPos*/, CScrollBar* pScrollBar)
{
	CSliderCtrl *p_sliderCtrl = (CSliderCtrl *) pScrollBar;

	CString strSize;

	int brightness = m_sliderBrightness.GetPos() - 100;
	int contrast = m_sliderContrast.GetPos() -100;
	int saturation = m_sliderSaturation.GetPos() -100;


	if (p_sliderCtrl->m_hWnd == m_sliderBrightness.m_hWnd)
	{
		strSize.Format(_T("%d"), brightness);
		m_staticBrightness.SetWindowText(strSize);
	}
	else if (p_sliderCtrl->m_hWnd == m_sliderContrast.m_hWnd)
	{
		strSize.Format(_T("%d"), contrast);
		m_staticContrast.SetWindowText(strSize);
	}
	if (p_sliderCtrl->m_hWnd == m_sliderSaturation.m_hWnd)
	{
		strSize.Format(_T("%d"), saturation);
		m_staticSaturation.SetWindowText(strSize);
	}

	adjustColor(brightness, contrast, saturation);
}

void CAdjustmentsTab::adjustColor(int brightness, int contrast, int saturation)
{
	CChildView *child_wnd = app->child_wnd;
	child_wnd->m_caHalf.caBrightness = brightness;
	child_wnd->m_caHalf.caContrast = contrast;
	child_wnd->m_caHalf.caColorfulness = saturation;

	if (brightness == 0 && contrast == 0 && saturation == 0)
		//app->SetAdjustments(false, 0,0,0);
		child_wnd->m_adjustment = false;

	else
		//app->SetAdjustments(true, brightness, contrast, saturation);
		child_wnd->m_adjustment = true;

	child_wnd->UpdateViewRegion();

}

void CAdjustmentsTab::OnBnClickedResetBrightness()
{
	m_sliderBrightness.SetPos(100);
	m_staticBrightness.SetWindowText(_T("0"));
	adjustColor(0, m_sliderContrast.GetPos()-100, m_sliderSaturation.GetPos()-100);
}

void CAdjustmentsTab::OnBnClickedResetContrast()
{
	m_sliderContrast.SetPos(100);
	m_staticContrast.SetWindowText(_T("0"));
	adjustColor(m_sliderBrightness.GetPos()-100, 0, m_sliderSaturation.GetPos()-100);
}
void CAdjustmentsTab::OnBnClickedResetSaturation()
{
	m_sliderSaturation.SetPos(100);
	m_staticSaturation.SetWindowText(_T("0"));
	adjustColor(m_sliderBrightness.GetPos()-100, m_sliderContrast.GetPos()-100, 0);
}
