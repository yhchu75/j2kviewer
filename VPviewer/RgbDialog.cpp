// RgbDialog.cpp : implementation file
//

#include "stdafx.h"
#include "RgbDialog.h"
#include "afxdialogex.h"


// CRgbDialog dialog

IMPLEMENT_DYNAMIC(CRgbDialog, CDialogEx)

CRgbDialog::CRgbDialog(CWnd* pParent /*=NULL*/)
: CDialogEx(CRgbDialog::IDD, pParent)
{

}

CRgbDialog::~CRgbDialog()
{
}

void CRgbDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CRgbDialog, CDialogEx)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CRgbDialog message handlers


void CRgbDialog::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;

	CDialogEx::PostNcDestroy();
}

void CRgbDialog::SetData(unsigned int r_data[], unsigned int g_data[], unsigned int b_data[])
{
	m_arrRgbRed = r_data;
	m_arrRgbGreen = g_data;
	m_arrRgbBlue = b_data;
}

unsigned int CRgbDialog::GetMaxBin(unsigned int data1[], unsigned int data2[], unsigned int data3[])
{
	unsigned int max_n = 0;

	for (int i = 0; i < 256; ++i)
	{
		TRACE("data : %d\n", data1[i]);

		if (data1[i] > max_n) max_n = data1[i];
	}

	for (int i = 0; i < 256; ++i)
	{
		TRACE("data : %d\n", data2[i]);

		if (data2[i] > max_n) max_n = data2[i];
	}

	for (int i = 0; i < 256; ++i)
	{
		TRACE("data : %d\n", data3[i]);

		if (data3[i] > max_n) max_n = data3[i];
	}

	return max_n;
}

void CRgbDialog::DrawGraph(CPaintDC *dc, int x, int y, unsigned int data[], unsigned int max_n ,const COLORREF rgb)
{
	unsigned int width = 258;
	unsigned int height = 110;
	unsigned int data_print[256] = { 0, };

	// Draw Background
	CPen pen;
	pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen *oldPen = dc->SelectObject(&pen);
	dc->Rectangle(x, y, x + width, y + height);
	dc->SelectObject(oldPen);

	double ratio = max_n ? (1.0 * (height - 1) / max_n) : 0.0;

	// Adjust Scale
	double adjust = 1.0;
	double min_threshold = 0.24; // 24%
	unsigned int min_threshold_cnt = (unsigned int)(height * width * min_threshold);
	unsigned int cnt = 0;

	while (1)
	{
		cnt = 0;

		for (int i = 0; i < 256; ++i)
			cnt += (unsigned int)((data[i] * adjust) * ratio);

		if (cnt == 0 || cnt > min_threshold_cnt)
			break;

		adjust += 0.1;
	}

	if (TRUE)
	adjust = 0.9;	//the top value of histogram is 90% of the rect

	// Set data
	for (int i = 0; i < 256; ++i)
		data_print[i] = (unsigned int)((data[i] * adjust) * ratio);
	TRACE(L"max_x %d, adjust %f, ratio %f\n", max_n, adjust, ratio);
	// Draw Graph
	CPen penData;
	penData.CreatePen(PS_SOLID, 1, rgb);
	oldPen = dc->SelectObject(&penData);

	for (int i = 0; i < 256; ++i)
	{
		if (data_print[i] == 0) continue;

		dc->MoveTo(x + i+1, y + height - 2);
		dc->LineTo(x + i+1, y + height - (data_print[i] > height ? height : data_print[i]) - 1);
	}

	dc->SelectObject(oldPen);
}


void CRgbDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	int max_n = GetMaxBin(m_arrRgbRed, m_arrRgbGreen, m_arrRgbBlue);
	DrawGraph(&dc, 20, 10, m_arrRgbRed, max_n, RGB(255, 0, 0));
	DrawGraph(&dc, 20, 130, m_arrRgbGreen, max_n, RGB(0, 255, 0));
	DrawGraph(&dc, 20, 250, m_arrRgbBlue, max_n, RGB(0, 0, 255));

	// Do not call CDialogEx::OnPaint() for painting messages
}


BOOL CRgbDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here
	int diag_width = 315;
	int diag_height = 415;

	CRect main_rect;
	AfxGetMainWnd()->GetWindowRect(main_rect);

	int diag_left = main_rect.left + (main_rect.Width() - diag_width) / 2;
	int diag_top = main_rect.top + (main_rect.Height() - diag_height) / 2;

	MoveWindow(CRect(
		diag_left,
		diag_top,
		diag_left + diag_width,
		diag_top + diag_height
		));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
