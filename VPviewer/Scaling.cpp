// Scaling.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "Scaling.h"
#include <numeric>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CScaling dialog
IMPLEMENT_DYNAMIC(CScaling, CDialog)

CScaling::CScaling(CKdu_showApp* app, CWnd* pParent /*=NULL*/)
	: CDialog(CScaling::IDD, pParent)
{
	this->app = app;
    Create(CScaling::IDD, pParent);
    ShowWindow(SW_SHOW);
}

CScaling::~CScaling()
{
}

void CScaling::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SCALE_LIST, m_scaleList);
	DDX_Control(pDX, IDC_CUSTOM, m_scaleCustom);
	DDX_Control(pDX, IDC_LABEL, m_scaleLabel);
	DDX_Control(pDX, IDC_BUTTON_DELETESCALE, m_deleteScale);
}


BEGIN_MESSAGE_MAP(CScaling, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_APPLY, &CScaling::OnBnClickedApply)
	ON_LBN_SELCHANGE(IDC_SCALE_LIST, &CScaling::OnLbnSelchangeScaleList)
	ON_BN_CLICKED(IDOK, &CScaling::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_ADDSCALE, &CScaling::OnBnClickedButtonAddscale)
	ON_BN_CLICKED(IDC_BUTTON_DELETESCALE, &CScaling::OnBnClickedButtonDeletescale)
END_MESSAGE_MAP()


// CScaling message handlers

BOOL CScaling::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_scaleLabel.SetWindowText(L"μm/pixel");

	return TRUE;  // return TRUE unless you set the focus to a control
}


void CScaling::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	if (bShow){
		
		fill_listBox();
		CString initText;
		initText.Format(L"%g", app->m_config.getAppliedScale());
		m_scaleCustom.SetWindowText(initText);
	}
}

void CScaling::read_list()
{
	target.clear();
	CString mysubkey = "SOFTWARE\\";

	mysubkey = mysubkey + app->m_pszRegistryKey + "\\" + app->m_pszAppName + "\\" + "Settings";

	HKEY h_key;
	int ret = RegOpenKeyEx(HKEY_CURRENT_USER, mysubkey, 0, KEY_QUERY_VALUE, &h_key);
	DWORD type, size;
	
	if (RegQueryValueExW(
		h_key, // HKEY
		TEXT("UserScale"),
		NULL,
		&type,
		NULL,
		&size) != ERROR_SUCCESS)
		return;
	if (type == REG_MULTI_SZ)
	{
		std::vector<wchar_t> temp(size / sizeof(wchar_t));

		if (RegQueryValueExW(
			h_key,
			TEXT("UserScale"),
			NULL,
			NULL,
			reinterpret_cast<LPBYTE>(&temp[0]),
			&size) != ERROR_SUCCESS)
			return;

		size_t index = 0;
		size_t len = wcslen(&temp[0]);
		while (len > 0)
		{
			target.push_back(&temp[index]);
			index += len + 1;
			len = wcslen(&temp[index]);
		}
	}

	if (h_key != NULL) RegCloseKey(h_key);

}

void CScaling::fill_listBox()	//get list form reg
{
	read_list();
	int i;
	m_scaleList.ResetContent();
	m_scales.clear();

	for (i = 0; i < 6; i++)
	{
		ScaleList sl;
		sl.check = L" ";
		m_scales.push_back(sl);
	}

	float appliedScale = app->m_config.getAppliedScale();
	if (appliedScale == 1.0f) m_scales[0].check = L"v";
	else if (appliedScale == 0.16f) m_scales[1].check = L"v";
	else if (appliedScale == 0.32f) m_scales[2].check = L"v";
	else if (appliedScale == 0.65f || appliedScale == 0.64f) m_scales[3].check = L"v";
	else if (appliedScale == 1.30f || appliedScale == 1.28f) m_scales[4].check = L"v";
	else if (appliedScale == 2.60f || appliedScale == 2.54f) m_scales[5].check = L"v";

	m_scales[0].unit = L"pixel/pixel";
	for (i = 1; i<6; i++)
		m_scales[i].unit = L"μm/pixel";
	m_scales[0].value = 1.0f;
	m_scales[1].value = 0.16f;
	m_scales[2].value = 0.32f;
	m_scales[3].value = 0.65f;
	m_scales[4].value = 1.30f;
	m_scales[5].value = 2.60f;

	for (i = 0; i < target.size(); i++)
	{
		ScaleList buf;
		buf.check = L" ";
		buf.unit = L"μm/pixel";
		buf.value = _ttof(target.at(i));
		m_scales.push_back(buf);
	}

	for (i = 0; i < m_scales.size(); i++)
	{
		if (m_scales.at(i).value == appliedScale)
			m_scales.at(i).check = L"v";

		CString str;
		str.Format(L"%-3.3s     %-7.7g      %-13.13s", m_scales[i].check, m_scales[i].value, m_scales[i].unit);
		m_scaleList.AddString(str);
	}
}


void CScaling::OnBnClickedApply()
{
	app->m_config.setAppliedScale(GetCurSelScale());
	fill_listBox();

	kdu_dims new_buffer_dims;
	new_buffer_dims.size = app->view_dims.size;
	new_buffer_dims.pos = app->buffer_dims.pos;
	app->UpdateViewDim(new_buffer_dims, true);
}


float CScaling::GetCurSelScale()
{
	CString text;
	m_scaleCustom.GetWindowText(text);
	return (float)_ttof(text);
}


void CScaling::OnLbnSelchangeScaleList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//UpdateData(TRUE);
	int curSel = m_scaleList.GetCurSel();
	CString text;
	float	newAppliedScale;
	m_deleteScale.EnableWindow(true);
	if (curSel < 6)
		m_deleteScale.EnableWindow(false);

	newAppliedScale = m_scales.at(curSel).value;

	text.Format(_T("%g"), newAppliedScale);
	m_scaleCustom.SetWindowText(text);
}


void CScaling::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnBnClickedApply();
	CDialog::OnOK();
}

void CScaling::write_list()
{
	CString mysubkey = "SOFTWARE\\";

	mysubkey = mysubkey + app->m_pszRegistryKey + "\\" + app->m_pszAppName + "\\" + "Settings";

	HKEY h_key;
	int ret = RegOpenKeyEx(HKEY_CURRENT_USER, mysubkey, 0, KEY_SET_VALUE, &h_key);

	CString reg_out;

	for (int i = 6; i < m_scales.size(); i++)
	{
		CString buf;
		buf.Format(L"%g", m_scales.at(i).value);
		reg_out += buf;
		reg_out += '\0';
	}
	reg_out += '\0';
	wchar_t *b = reg_out.GetBuffer();
	int n = reg_out.GetLength();
	BYTE* buf = (BYTE*)(LPCTSTR)reg_out;
	int err = RegSetValueEx(h_key, TEXT("UserScale"), 0, REG_MULTI_SZ, (BYTE*)b, reg_out.GetLength() * sizeof(wchar_t));
	ERROR_SUCCESS;
	if (h_key != NULL) RegCloseKey(h_key);
}

void CScaling::OnBnClickedButtonAddscale()
{
	// TODO: Add your control notification handler code here
	ScaleList buf;
	buf.check = L" ";
	buf.unit = "μm/pixel";
	CString val;
	GetDlgItemText(IDC_CUSTOM, val);
	buf.value = _ttof(val);
	m_scales.push_back(buf);

	write_list();
	fill_listBox();
}


void CScaling::OnBnClickedButtonDeletescale()
{
	// TODO: Add your control notification handler code here
	int sel = m_scaleList.GetCurSel();
	if (sel < 6)
		return;
	m_scales.erase(m_scales.begin() + sel);
	write_list();
	fill_listBox();
}
