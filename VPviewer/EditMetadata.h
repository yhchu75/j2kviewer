#pragma once
#include "resource.h"


// CEditMetadata dialog
#define NUM_COLUMNS 2
#define	NUM_ITEMS	4

static _TCHAR *_gszColumnLabel[NUM_COLUMNS] =
{
	_T("Number"), _T("Name")
};

static int _gnColumnFmt[NUM_COLUMNS] =
{
	LVCFMT_CENTER, LVCFMT_LEFT
};

static int _gnColumnWidth[NUM_COLUMNS] =
{
	50, 150
};

class CVPViewerConf;
class CEditMetadata : public CDialog
{
	DECLARE_DYNAMIC(CEditMetadata)

public:
	CEditMetadata(CKdu_showApp *app, CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditMetadata();
	CString GetItemText(HWND hWnd, int nItem, int nSubItem) const;
	void SetCell(HWND hWnd1, CString value, int nRow, int nCol);
	void InsertItems();

// Dialog Data
	enum { IDD = IDD_EDITMETADATA };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

public:
	CKdu_showApp *app;
	CVPViewerConf *m_config;
	CEdit		m_editScanningRes, m_editWell, m_editDepth, m_editComments;
	CEdit		m_editBrightness, m_editContrast, m_editSaturation, m_editImageLayer;

	CListCtrl	m_listLayer;
	CEdit		m_edit; // for editing list item
	int			nItem, nSubItem;
	TCHAR		itemText[NUM_ITEMS][NUM_COLUMNS][64];
	CString		layerName[4];
	CString		m_brightness, m_contrast, m_saturation, m_imageLayer;


public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	afx_msg void OnOK();

protected:
//	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
public:
	afx_msg void OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedModify();
	afx_msg void OnBnClickedButtonGetcurval();
};
