// EditMetadata.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "EditMetadata.h"


// CEditMetadata dialog

IMPLEMENT_DYNAMIC(CEditMetadata, CDialog)

CEditMetadata::CEditMetadata(CKdu_showApp *app, CWnd* pParent /*=NULL*/)
	: CDialog(CEditMetadata::IDD, pParent)
{
	this->app = app;
	m_config = &app->m_config;
    Create(CEditMetadata::IDD, pParent);
	

    //ShowWindow(SW_SHOW); after fill imageInfo
}

CEditMetadata::~CEditMetadata()
{
}

void CEditMetadata::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SCANNINGRES, m_editScanningRes);
	DDX_Control(pDX, IDC_EDIT_WELL, m_editWell);
	DDX_Control(pDX, IDC_EDIT_DEPTH, m_editDepth);
	DDX_Control(pDX, IDC_EDIT_COMMENTS, m_editComments);

	DDX_Control(pDX, IDC_LIST1, m_listLayer);
	DDX_Control(pDX, IDC_EDIT2, m_edit);

	DDX_Control(pDX, IDC_EDIT_BRIGHTNESS, m_editBrightness);
	DDX_Control(pDX, IDC_EDIT_CONTRAST, m_editContrast);
	DDX_Control(pDX, IDC_EDIT_SATURATION, m_editSaturation);
	DDX_Control(pDX, IDC_EDIT_IMAGELAYER, m_editImageLayer);
}


BEGIN_MESSAGE_MAP(CEditMetadata, CDialog)
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &CEditMetadata::OnNMClickList1)
	ON_BN_CLICKED(IDC_MODIFY, &CEditMetadata::OnBnClickedModify)
	ON_BN_CLICKED(IDC_BUTTON_GETCURVAL, &CEditMetadata::OnBnClickedButtonGetcurval)
END_MESSAGE_MAP()


void CEditMetadata::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	ImageExternalInfo*	info = m_config->GetImageExternalInfo();
	
	// TODO: Add your message handler code here
	/*
	char strTemp[1024];
	sprintf(strTemp, "%4.2f", info->scanningRes);
	m_editScanningRes.SetWindowTextA(strTemp);
	*/
	std::ostringstream stringStream;
	stringStream.setf(std::ios::fixed, std::ios::floatfield);
	stringStream.precision(2);
	
	USES_CONVERSION;
	stringStream << info->scanningRes;
	m_editScanningRes.SetWindowText(A2T(stringStream.str().c_str()));
	
	stringStream.clear();
	stringStream.str("");
	stringStream << info->well;
	m_editWell.SetWindowText(A2T(stringStream.str().c_str()));

	stringStream.clear();
	stringStream.str("");
	stringStream << info->depth;
	m_editDepth.SetWindowText(A2T(stringStream.str().c_str()));

	stringStream.clear();
	stringStream.str("");
	stringStream << info->comment;
	m_editComments.SetWindowText(A2T(stringStream.str().c_str()));

	int i;
	for (i = 0; i<info->layers.size(); i++){
		m_listLayer.SetItemText(i, 1, A2T(info->layers[i].name.c_str()) );
	}

	stringStream.clear();
	stringStream.str("");
	stringStream << info->brightness;
	m_editBrightness.SetWindowText(A2T(stringStream.str().c_str()));
	stringStream.clear();
	stringStream.str("");
	stringStream << info->contrast;
	m_editContrast.SetWindowText(A2T(stringStream.str().c_str()));
	stringStream.clear();
	stringStream.str("");
	stringStream << info->saturation;
	m_editSaturation.SetWindowText(A2T(stringStream.str().c_str()));
	stringStream.clear();
	stringStream.str("");
	m_editImageLayer.SetWindowText(m_imageLayer);

	if (app->jpip_client && app->jpip_client->is_alive())
	{
		GetDlgItem(IDC_MODIFY)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_GETCURVAL)->EnableWindow(FALSE);
	}

}

BOOL CEditMetadata::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	ListView_SetExtendedListViewStyle(::GetDlgItem(m_hWnd,IDC_LIST1),LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES); 
	InsertItems();

	::ShowWindow(::GetDlgItem(m_hWnd,IDC_EDIT2),SW_HIDE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

// This function inserts the default values into the listControl
void CEditMetadata::InsertItems()
{
	HWND hWnd = ::GetDlgItem(m_hWnd, IDC_LIST1);

	// Set the LVCOLUMN structure with the required column information
	LVCOLUMN list;
	list.mask =  LVCF_TEXT |LVCF_WIDTH| LVCF_FMT |LVCF_SUBITEM;
	list.fmt = LVCFMT_LEFT;
	list.cx = 50;
	list.pszText   = _T("No");
	list.iSubItem = 0;
	//Inserts the column
	::SendMessage(hWnd,LVM_INSERTCOLUMN, (WPARAM)0,(WPARAM)&list);
	
	list.cx = 250;
	list.pszText = _T("Name");
	list.iSubItem = 1;
	::SendMessage(hWnd  ,LVM_INSERTCOLUMN, (WPARAM)1,(WPARAM)&list);

	ImageExternalInfo*	info = app->m_config.GetImageExternalInfo();
	for (int i = 0; i < info->layers.size();i++){
		CString buf;
		buf.Format(_T("%d"), i + 1);
		SetCell(hWnd, buf, i, 0);
		SetCell(hWnd, "", i, 1);
	}
}


void CEditMetadata::OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	Invalidate();
	HWND hWnd1 =  ::GetDlgItem (m_hWnd,IDC_LIST1);
	LPNMITEMACTIVATE temp = (LPNMITEMACTIVATE) pNMHDR;
	RECT rect;
	//get the row number
	nItem = temp->iItem;
	//get the column number
	nSubItem = temp->iSubItem;
	if(nSubItem == 0 || nSubItem == -1 || nItem == -1)
		return ;
	//Retrieve the text of the selected subItem from the list
	CString str = GetItemText(hWnd1,nItem ,nSubItem);

	RECT rect1,rect2;
	// this macro is used to retrieve the Rectanle of the selected SubItem
	ListView_GetSubItemRect(hWnd1,temp->iItem,temp->iSubItem,LVIR_BOUNDS,&rect);
	//Get the Rectange of the listControl
	::GetWindowRect(temp->hdr.hwndFrom,&rect1);
	//Get the Rectange of the Dialog
	::GetWindowRect(m_hWnd,&rect2);

	int x=rect1.left-rect2.left;
	int y=rect1.top-rect2.top;
	
	if(nItem != -1)	
	::SetWindowPos(::GetDlgItem(m_hWnd,IDC_EDIT2),HWND_TOP,rect.left+x-4,rect.top+y-29,rect.right-rect.left - 3,rect.bottom-rect.top -1,NULL);
	::ShowWindow(::GetDlgItem(m_hWnd,IDC_EDIT2),SW_SHOW);
	::SetFocus(::GetDlgItem(m_hWnd,IDC_EDIT2));
	//Draw a Rectangle around the SubItem
	::Rectangle(::GetDC(temp->hdr.hwndFrom),rect.left,rect.top-1,rect.right,rect.bottom);
	//Set the listItem text in the EditBox
	::SetWindowText(::GetDlgItem(m_hWnd,IDC_EDIT2),str);
	*pResult = 0;
}

CString CEditMetadata::GetItemText(HWND hWnd, int nItem, int nSubItem) const
{
	LVITEM lvi;
	memset(&lvi, 0, sizeof(LVITEM));
	lvi.iSubItem = nSubItem;
	CString str;
	int nLen = 128;
	int nRes;
	do
	{
		nLen *= 2;
		lvi.cchTextMax = nLen;
		lvi.pszText = str.GetBufferSetLength(nLen);
		nRes  = (int)::SendMessage(hWnd, LVM_GETITEMTEXT, (WPARAM)nItem,
			(LPARAM)&lvi);
	} while (nRes == nLen-1);
	str.ReleaseBuffer();
	return str;
}

void CEditMetadata::OnOK() 
{	
    CWnd* pwndCtrl = GetFocus();
    // get the control ID which is presently having the focus
	int ctrl_ID = pwndCtrl->GetDlgCtrlID();
	CString str;
    switch (ctrl_ID)
	{	//if the control is the EditBox	
        case IDC_EDIT2:
			//get the text from the EditBox
			GetDlgItemText(IDC_EDIT2,str);
			//set the value in the listContorl with the specified Item & SubItem values
			SetCell(::GetDlgItem (m_hWnd,IDC_LIST1),str,nItem,nSubItem);
			::SendDlgItemMessage(m_hWnd,IDC_EDIT2,WM_KILLFOCUS,0,0);
			::ShowWindow(::GetDlgItem(m_hWnd,IDC_EDIT2),SW_HIDE);
            break;     
        default:
            break;
    }
}

// This function set the text in the specified SubItem depending on the Row and Column values
void CEditMetadata::SetCell(HWND hWnd1, CString value, int nRow, int nCol)
{
	TCHAR     szString [256];
	wsprintf(szString,value ,0);

	//Fill the LVITEM structure with the values given as parameters.
	LVITEM lvItem;
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = nRow;
	lvItem.pszText = szString;
	lvItem.iSubItem = nCol;
	if(nCol >0)
		::SendMessage(hWnd1,LVM_SETITEM, (WPARAM)0,(WPARAM)&lvItem);
	else
		ListView_InsertItem(hWnd1,&lvItem);
}


void CEditMetadata::OnBnClickedModify()
{
	ImageExternalInfo* info = m_config->GetImageExternalInfo();
	
	//Update to config settig
	TCHAR strTemp[256];
	std::wstring buf;
	GetDlgItemText(IDC_EDIT_SCANNINGRES, strTemp, 32);
	info->scanningRes = _ttof(strTemp);
	
	GetDlgItemText(IDC_EDIT_WELL, strTemp, 256);
	buf = std::wstring(strTemp);
	info->well.assign(buf.begin(), buf.end());

	GetDlgItemText(IDC_EDIT_DEPTH, strTemp, 256);
	buf = std::wstring(strTemp);
	info->depth.assign(buf.begin(), buf.end());

	GetDlgItemText(IDC_EDIT_COMMENTS, strTemp, 256);
	buf = std::wstring(strTemp);
	info->comment.assign(buf.begin(), buf.end());

	for (int i = 0; i < m_config->GetImageExternalInfo()->layers.size(); i++){
		layerName[i] = m_listLayer.GetItemText(i, 1);
		buf = std::wstring(layerName[i]);
		info->layers.at(i).index = i+1;
		info->layers.at(i).name.assign(buf.begin(), buf.end());
	}
	
	

	//TRACE("edit metadata dialog : |%s| |%s| |%s| |%s| ...\n", layerName[0], layerName[1], layerName[2], layerName[3]);
	m_editBrightness.GetWindowText(m_brightness);
	m_editContrast.GetWindowText(m_contrast);
	m_editSaturation.GetWindowText(m_saturation);
	m_editImageLayer.GetWindowText(m_imageLayer);

	GetDlgItemText(IDC_EDIT_BRIGHTNESS, strTemp, 32);
	info->brightness = _ttof(strTemp);

	GetDlgItemText(IDC_EDIT_CONTRAST, strTemp, 32);
	info->contrast = _ttof(strTemp);
	
	GetDlgItemText(IDC_EDIT_SATURATION, strTemp, 32);
	info->saturation = _ttof(strTemp);

}


void CEditMetadata::OnBnClickedButtonGetcurval()
{
	// TODO: Add your control notification handler code here
	CString str;

	str.Format(_T("%4.2f"), m_config->GetImageExternalInfo()->scanningRes);
	m_editScanningRes.SetWindowText(str);

	m_editWell.SetWindowText( CA2T(m_config->GetImageExternalInfo()->well.c_str()));
	m_editDepth.SetWindowText(CA2T(m_config->GetImageExternalInfo()->depth.c_str()));
	m_editComments.SetWindowText(CA2T(m_config->GetImageExternalInfo()->comment.c_str()));

	for (int i = 0; i < m_config->GetImageExternalInfo()->layers.size(); i++){
		m_listLayer.SetItemText(i, 1, CA2T(m_config->GetImageExternalInfo()->layers[i].name.c_str()));
	}

	CChildView *child_wnd = app->child_wnd;

	str.Format(_T("%d"), child_wnd->m_caHalf.caBrightness);
	m_editBrightness.SetWindowText(str);

	str.Format(_T("%d"), child_wnd->m_caHalf.caContrast);
	m_editContrast.SetWindowText(str);

	str.Format(_T("%d"), child_wnd->m_caHalf.caColorfulness);
	m_editSaturation.SetWindowText(str);
	
	str.Format(_T("%d"), app->single_layer_idx+1);
	m_editImageLayer.SetWindowText(str);
}
