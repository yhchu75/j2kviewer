// Overview.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "Overview.h"


// COverview dialog

IMPLEMENT_DYNAMIC(COverview, CDialog)

COverview::COverview(CKdu_showApp *app, CWnd* pParent)
	: CDialog()
{

	m_app = app;
    Create(COverview::IDD, pParent);
	CWinApp* pApp = AfxGetApp();
	m_position.x = pApp->GetProfileInt(L"Settings", L"OPosX", 0);
	m_position.y = pApp->GetProfileInt(L"Settings", L"OPosY", 80);

	RECT r;
	m_app->m_pMainWnd->GetWindowRect(&r);
	SetWindowPos(NULL, r.left + m_position.x, r.top + m_position.y, 0, 0, SWP_NOSIZE);
  	m_overviewTab.InsertItem(0, _T("General"));
	m_overviewTab.InsertItem(1, _T("Image Info"));
	m_overviewTab.InsertItem(2, _T("ROIs"));
	m_overviewTab.InsertItem(3, _T("Adjustments"));

	m_generalTab.Create(IDD_GENERAL, this);
	m_imageInfoTab.Create(IDD_IMAGEINFO, this);
	m_roisTab.Create(IDD_ROIS, this);
	m_adjustmentsTab.Create(IDD_ADJUSTMENTS, this);
	
	CRect rect;

	GetDlgItem(IDC_STATIC_FRAME)->GetWindowRect(rect);
	ScreenToClient(rect);
	m_generalTab.MoveWindow(rect);
	m_imageInfoTab.MoveWindow(rect);
	m_roisTab.MoveWindow(rect);
	m_adjustmentsTab.MoveWindow(rect);

	m_imageInfoTab.ShowWindow(SW_HIDE);
	m_roisTab.ShowWindow(SW_HIDE);
	m_adjustmentsTab.ShowWindow(SW_HIDE);

	GetDlgItem(IDC_TAB1)->GetWindowRect(rect);
	m_nTabHeight = rect.Height() + 28; //Button Height 

	m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	m_bOverviewPanning = false;

	GetClientRect(&rect);
	m_OverviewFrameRect.left = 1;
	m_OverviewFrameRect.top = 1;
	m_OverviewFrameRect.right = rect.Width()- 1;
	m_OverviewFrameRect.bottom = rect.Height() - m_nTabHeight - 28;

	m_hOverviewBitmap = NULL;
	memset(m_OverviewRect, 0, sizeof(m_OverviewRect));

	HDC hdc = ::GetDC(NULL);
	m_hOverviewDC = CreateCompatibleDC(hdc);
	::ReleaseDC(NULL, hdc);

	m_fOverviewScale_w = m_fOverviewScale_h = 0.0;
	m_bShowTab = true;

	ShowWindow(SW_SHOW);
}

COverview::~COverview()
{
	if (m_hOverviewBitmap){
		DeleteObject(m_hOverviewBitmap);
	}

	if (m_hOverviewDC){
		DeleteDC(m_hOverviewDC);
	}
	CWinApp* pApp = AfxGetApp();
	pApp->WriteProfileInt(L"Settings", L"OPosX", m_position.x);
	pApp->WriteProfileInt(L"Settings", L"OPosY", m_position.y);

}

BOOL COverview::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	m_generalTab.app = m_app;
	m_adjustmentsTab.app = m_app;
	m_imageInfoTab.app = m_app;
	m_roisTab.setApp(m_app, &m_app->m_DrawObjMgr);

	return TRUE;  // return TRUE unless you set the focus to a control
}


void COverview::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_overviewTab);
	DDX_Control(pDX, IDC_BUTTON1, m_button);
}


BEGIN_MESSAGE_MAP(COverview, CDialog)
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON1, &COverview::OnBnClickedButton1)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &COverview::OnTcnSelchangeTab1)
	ON_WM_SHOWWINDOW()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOVING()
	ON_BN_CLICKED(IDC_CHECK_ATTACH, &COverview::OnBnClickedCheckOverview)
END_MESSAGE_MAP()


BOOL	COverview::SetOverviewData(int ov_width, int ov_height, char *pData)
{
	int old_ov_width = m_OverviewRect.right - m_OverviewRect.left;
	int old_ov_height = m_OverviewRect.bottom - m_OverviewRect.top;

	int width = m_OverviewFrameRect.right - ov_width;
	int height = m_OverviewFrameRect.bottom - ov_height;

	m_OverviewRect.left = width / 2;
	m_OverviewRect.top = height / 2;
	m_OverviewRect.right = ov_width + m_OverviewRect.left;
	m_OverviewRect.bottom = ov_height + m_OverviewRect.top;

	//Recreate
	if (ov_width != old_ov_width || ov_height != old_ov_height){
		if (m_hOverviewBitmap){
			DeleteObject(m_hOverviewBitmap);
			m_hOverviewBitmap = NULL;
		}
	}
	
	if ( NULL == m_hOverviewBitmap )
	{
		BITMAPINFOHEADER bih;
		memset(&bih, 0, sizeof(BITMAPINFOHEADER));
		bih.biSize = sizeof(BITMAPINFOHEADER);
		bih.biWidth = ov_width;
		bih.biHeight = -ov_height;
		bih.biPlanes = 1;
		bih.biBitCount = 32;
		bih.biCompression = BI_RGB;

		m_hOverviewBitmap = CreateDIBSection(m_hOverviewDC, (BITMAPINFO *)&bih, DIB_RGB_COLORS, (void**)&m_pOverviewBuf, NULL, 0);
		SelectObject(m_hOverviewDC, m_hOverviewBitmap);
		if (m_hOverviewBitmap == NULL){
			return FALSE;
		}
	}

	memcpy(m_pOverviewBuf, pData, ov_width*ov_height * 4);

	//clear framerectregion
	CDC *dc = GetDC();
	HBRUSH writeBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	RECT viewRC = m_OverviewFrameRect;
	viewRC.left = viewRC.top = 0;
	viewRC.right += 1; 
	viewRC.bottom += 1;
	FillRect(dc->m_hDC, &viewRC, writeBrush);
	ReleaseDC(dc);

	return TRUE;
}


void COverview::UpdateViewPos(float pos_x, float pos_y, float image_scale_x, float image_scale_y, int image_w, int image_h)
{
	int width = (m_OverviewRect.right - m_OverviewRect.left);
	int height = (m_OverviewRect.bottom - m_OverviewRect.top);
	
	m_fOverviewScale_w = (float)width / image_w;
	m_fOverviewScale_h = (float)height / image_h;

	CRect			newView;
	newView.left	= pos_x * width + m_OverviewRect.left;
	newView.top		= pos_y * height + m_OverviewRect.top;
	newView.right = image_scale_x * width + newView.left;
	newView.bottom = image_scale_y * height + newView.top;

	if (m_rcOrg == newView) return;
	m_rcOrg = newView;

	m_rcView = newView;
	LONG tmp;
	if (ROTATE_90 == m_app->m_rotationStatus)
	{
		tmp = m_rcView.left;
		m_rcView.left = m_OverviewRect.Width() + m_rcView.right;
		m_rcView.right = m_OverviewRect.Width() + tmp;
	}
	else if (ROTATE_180 == m_app->m_rotationStatus)
	{
		tmp = m_rcView.top;
		m_rcView.top = m_OverviewRect.Height() + m_rcView.bottom;
		m_rcView.bottom = m_OverviewRect.Height() + tmp;

		tmp = m_rcView.left;
		m_rcView.left = m_OverviewRect.Width() + m_rcView.right;
		m_rcView.right = m_OverviewRect.Width() + tmp;

	}
	else if (ROTATE_270 == m_app->m_rotationStatus)
	{
		tmp = m_rcView.top;
		m_rcView.top = m_OverviewRect.Height() + m_rcView.bottom;
		m_rcView.bottom = m_OverviewRect.Height() + tmp;
	}


	//Check border condition
	if (m_rcView.left < m_rcView.right){
		if (m_rcView.left <= m_OverviewRect.left) m_rcView.left = m_OverviewRect.left + 1;
		if (m_rcView.right >= m_OverviewRect.right) m_rcView.right = m_OverviewRect.right - 1;
	}
	else{
		if (m_rcView.right <= m_OverviewRect.left) m_rcView.right = m_OverviewRect.left + 1;
		if (m_rcView.left >= m_OverviewRect.right) m_rcView.left = m_OverviewRect.right - 1;
	}
	
	if (m_rcView.top < m_rcView.bottom){
		if (m_rcView.top <= m_OverviewRect.top) m_rcView.top = m_OverviewRect.top + 1;
		if (m_rcView.bottom >= m_OverviewRect.bottom) m_rcView.bottom = m_OverviewRect.bottom - 1;
	}
	else{
		if (m_rcView.bottom <= m_OverviewRect.top) m_rcView.bottom = m_OverviewRect.top + 1;
		if (m_rcView.top >= m_OverviewRect.bottom) m_rcView.top = m_OverviewRect.bottom - 1;
	}
	TRACE("rect %d, %d, %d, %d : %d, %d \n", m_rcView.top, m_rcView.bottom, m_rcView.left, m_rcView.right, m_OverviewRect.Height(), m_OverviewRect.Width());
		
	CDC *dc = GetDC();
	UpdateOverviewRegion(dc);
	ReleaseDC(dc);
		
}


void COverview::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	UpdateOverviewRegion(&dc);
}


void COverview::UpdateOverviewRegion(CDC *dc)
{
	if (!m_hOverviewBitmap)return;

	::BitBlt(dc->m_hDC, m_OverviewRect.left, m_OverviewRect.top, m_OverviewRect.Width(), m_OverviewRect.Height(),
		m_hOverviewDC, 0, 0, SRCCOPY);

	//Draw View Rectangle. 
	DrawBox(dc, &m_rcView, RGB(255, 0, 0));

	m_generalTab.UpdateCurrentScaleStatus();
}


void	COverview::DrawBox(CDC* dc, RECT* rect, DWORD color)
{
	CPen  Pen;
	Pen.CreatePen(PS_SOLID, 0, color);
	dc->SelectObject(Pen);
		
	dc->MoveTo(rect->left, rect->top);
	dc->LineTo(rect->left, rect->bottom);
	dc->LineTo(rect->right, rect->bottom);
	dc->LineTo(rect->right, rect->top);
	dc->LineTo(rect->left, rect->top);
}


void COverview::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	
	if(m_bShowTab)
	{
		CRect rect;

		GetWindowRect(rect);
		rect.bottom -= m_nTabHeight;
		MoveWindow(rect);
		m_button.SetWindowText(_T("< < < < < < < < < "));
	}
	else
	{
		CRect rect;

		GetWindowRect(rect);
		rect.bottom += m_nTabHeight;
		MoveWindow(rect);
		m_button.SetWindowText(_T("> > > > > > > > >"));

	}

	m_bShowTab = !m_bShowTab;
}


void COverview::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	m_generalTab.ShowWindow(SW_HIDE);
	m_imageInfoTab.ShowWindow(SW_HIDE);
	m_roisTab.ShowWindow(SW_HIDE);
	m_adjustmentsTab.ShowWindow(SW_HIDE);
	
	switch(m_overviewTab.GetCurSel())
	{
		case 0 :
			m_generalTab.ShowWindow(SW_SHOW);
			break;
		case 1 :
			m_imageInfoTab.ShowWindow(SW_SHOW);
			break;
		case 2 :
			m_roisTab.ShowWindow(SW_SHOW);
			break;
		case 3 :
			m_adjustmentsTab.ShowWindow(SW_SHOW);
			break;
	}

	*pResult = 0;
}


void COverview::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	m_generalTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	m_imageInfoTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	m_roisTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	m_adjustmentsTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

	CheckDlgButton(IDC_CHECK_ATTACH, m_app->frame_wnd->m_attach_overview);
}


void COverview::OnMouseMove(UINT nFlags, CPoint point)
{
	//if (m_app->m_rotationStatus  %4 != 0) return;
	// TODO: Add your message handler code here and/or call default
	if (m_bOverviewPanning)
	{
		//TRACE("overview_panning is true..%5.2f\n", overview_rate);
		int deltaX = (point.x - m_LastMousePoint.x) / m_fOverviewScale_w;
		int deltaY = (point.y - m_LastMousePoint.y) / m_fOverviewScale_h;

		m_app->set_scroll_pos(deltaX, deltaY, true);

		m_LastMousePoint = point;
		//TRACE("point %d %d\n", point.x, point.y);
	}		
	CDialog::OnMouseMove(nFlags, point);
}

void COverview::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_app != NULL && m_OverviewRect.PtInRect(point) && (m_fOverviewScale_w < 1 || m_fOverviewScale_h < 1) && !m_app->m_Analyzing)
	{
		int deltaX = (point.x - m_OverviewRect.left - abs(m_rcView.Width()/2)) / m_fOverviewScale_w;
		int deltaY = (point.y - m_OverviewRect.top - abs(m_rcView.Height()/2)) / m_fOverviewScale_h;

		if (deltaX < 0) deltaX = 0;
		if (deltaY < 0) deltaY = 0;
		
		m_app->set_scroll_pos(deltaX, deltaY, false);

		m_LastMousePoint = point;
		m_bOverviewPanning = true;
		SetCapture();
	}
	CDialog::OnLButtonDown(nFlags, point);
}

BOOL COverview::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (m_hAccel != NULL)
		if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
			return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}

void COverview::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (m_bOverviewPanning){
		ReleaseCapture();
		m_bOverviewPanning = false;
	}
	
	CDialog::OnLButtonUp(nFlags, point);
}

void COverview::OnMoving(UINT nSide, LPRECT lpRect)
{
	CDialog::OnMoving(nSide, lpRect);
	if (!m_app->m_Analyzing){
		CRect mainR;
		CRect overviewR;
		m_app->m_pMainWnd->GetWindowRect(mainR);
		GetWindowRect(overviewR);

		m_position.x = overviewR.left - mainR.left;
		m_position.y = overviewR.top - mainR.top;
	}
}

void COverview::OnBnClickedCheckOverview()
{
	// TODO: Add your control notification handler code here
	if (IsDlgButtonChecked(IDC_CHECK_ATTACH))
		m_app->frame_wnd->m_attach_overview = true;
	else
		m_app->frame_wnd->m_attach_overview = false;
}
