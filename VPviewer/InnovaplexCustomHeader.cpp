#include "stdafx.h"
#include <mmsystem.h>

// Kakadu core includes
#include "kdu_arch.h"
#include "kdu_elementary.h"
#include "kdu_messaging.h"
#include "kdu_params.h"
#include "kdu_compressed.h"
#include "kdu_roi_processing.h"
#include "kdu_sample_processing.h"
// Application includes
#include "jp2.h"
#include "jpx.h"
using namespace kdu_supp;

#include "InnovaplexCustomHeader.h"

static const kdu_uint32 innova_box_type = MAKEFOURCC('O', 'N', 'N', 'I');

bool WriteInnovaplexCustomHeader(void* target, INNOVAPLEXBoxHeader *header)
{
	jp2_family_tgt* tgt = (jp2_family_tgt*)target;
	
	jp2_output_box box_out;
	
	box_out.open(tgt, innova_box_type, false);

	//1. insert encrpypted systemGUID 
	char szGUID[INNOVA_GUID_LENGTH];
	GetInnovaSystemGUID(szGUID);
	box_out.write((kdu_byte*)szGUID, INNOVA_GUID_LENGTH);
	box_out.write((kdu_byte*)header, sizeof(INNOVAPLEXBoxHeader) );

	box_out.close();

	return true;
}


bool ReadInnovaplexCustomHeader(void* source, SystemInfo *pSystemInfo, INNOVAPLEXBoxHeader *header)
{
	jp2_family_src* src = (jp2_family_src*)source;

	jp2_input_box box_in;
	box_in.open(src);
	while (box_in.exists())
	{
		if (box_in.get_box_type() == innova_box_type){

			char szGUID[INNOVA_GUID_LENGTH];
			if (box_in.read((kdu_byte*)szGUID, INNOVA_GUID_LENGTH) != INNOVA_GUID_LENGTH){
				return false;
			}

			if (!GetSystemInfoFromGUID(szGUID, pSystemInfo)){
				return false;
			}

			if (box_in.read((kdu_byte*)header, sizeof(INNOVAPLEXBoxHeader)) != sizeof(INNOVAPLEXBoxHeader)){
				return false;
			}

			box_in.close();
			return true;
		}
		else{	//get next box.. 
			box_in.close();
			box_in.open_next();
		}
	}

	return false;
}

