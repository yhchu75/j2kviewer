// ROISTab.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "ROISTab.h"



// CROISTab dialog


IMPLEMENT_DYNAMIC(CROISTab, CDialog)

CROISTab::CROISTab(CWnd* pParent /*=NULL*/)
	: CDialog(CROISTab::IDD, pParent)
{
	m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	m_pDrawObjMgr = NULL;
}

CROISTab::~CROISTab()
{
	
}


void CROISTab::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ROILIST, m_roiListBox);
	DDX_Control(pDX, IDC_BTN_HIDE, m_btnHide);

}


BEGIN_MESSAGE_MAP(CROISTab, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BTN_HIDE, &CROISTab::OnBnClickedBtnHide)
	ON_LBN_SELCHANGE(IDC_ROILIST, &CROISTab::OnLbnSelchangeRoilist)
	ON_BN_CLICKED(IDC_GOBUTTON, &CROISTab::OnBnClickedGobutton)
	ON_BN_CLICKED(IDC_BUTTON2, &CROISTab::OnBnClickedNew)
	ON_BN_CLICKED(IDC_EDITROI, &CROISTab::OnBnClickedEditroi)
END_MESSAGE_MAP()


// CROISTab message handlers

BOOL CROISTab::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (m_hAccel != NULL)
		if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
			return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}


void CROISTab::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	if (bShow) UpdateROIList();
	if (m_pDrawObjMgr->IsShowROIObj())
		m_btnHide.SetWindowText(_T("Hide All"));
	else
		m_btnHide.SetWindowText(_T("Show All"));
}


void CROISTab::OnBnClickedBtnHide()
{
	// TODO: Add your control notification handler code here
	if (m_pDrawObjMgr->IsShowROIObj())
		m_btnHide.SetWindowText(_T("Show All"));
	else
		m_btnHide.SetWindowText(_T("Hide All"));
		
	m_pDrawObjMgr->ToggleShowROIObj();

}


void CROISTab::UpdateROIList()
{
	m_roiListBox.ResetContent();

	USES_CONVERSION;
	std::vector<CROIObj*>& ROIList = m_pDrawObjMgr->GetROIObjList();
	for (unsigned int i = 0; i < ROIList.size(); i++){
		ROIInfo* info = ROIList[i]->getInfo();
		m_roiListBox.AddString(A2T(info->description.c_str()));
	}
}


void CROISTab::OnBnClickedGobutton()
{
	if (!m_app) return;

	int nSelIndex = m_roiListBox.GetCurSel();
	std::vector<CROIObj *>& ROIList = m_pDrawObjMgr->GetROIObjList();
	if (nSelIndex >= ROIList.size()) return;

	ROIInfo* pInfo = ROIList[nSelIndex]->getInfo();
	POINTF center_ptf;
	center_ptf.x = (pInfo->pts[0].x + pInfo->pts[2].x) /2.0f  ;
	center_ptf.y = (pInfo->pts[0].y + pInfo->pts[2].y) /2.0f;

	m_app->set_center_pos(center_ptf.x, center_ptf.y, pInfo->scale);
}


void CROISTab::OnBnClickedNew()
{
	if (!m_app) return;

	m_newRoiDlg.SetNextROIIndex((int)m_pDrawObjMgr->GetROIObjList().size());

	USES_CONVERSION;
	if (m_newRoiDlg.DoModal() == IDOK){
		m_pDrawObjMgr->AddObj(ROI, (ePENCOLOR)m_newRoiDlg.m_Colorindex, T2A(m_newRoiDlg.m_descriptionStr.GetBuffer()));
		m_btnHide.SetWindowText(_T("Hide All"));
		m_app->child_wnd->SetFocus();
	}
}


void CROISTab::OnBnClickedEditroi()
{
	int i = m_roiListBox.GetCurSel();
	std::vector<CROIObj*> ROIList = m_pDrawObjMgr->GetROIObjList();

	if (i >= ROIList.size()) return;
	ROIInfo *info = ROIList[i]->getInfo();
	m_editRoiDlg.m_descriptionStr = info->description.c_str();
	m_editRoiDlg.m_colorIndex = info->color;

	if (m_editRoiDlg.DoModal() == IDOK)
	{
		USES_CONVERSION;
		info->description = T2A(m_editRoiDlg.m_descriptionStr.GetBuffer());
		info->color = (ePENCOLOR)m_editRoiDlg.m_colorIndex;
		m_app->child_wnd->UpdateViewRegion();
		UpdateROIList();

	}
}


void CROISTab::OnLbnSelchangeRoilist()
{
	unsigned int curSel = m_roiListBox.GetCurSel();
	TRACE("current Selection = %d\n", curSel);

	std::vector<CROIObj*>& ROIList = m_pDrawObjMgr->GetROIObjList();
	if (curSel >= ROIList.size()) return;

	m_pDrawObjMgr->SetActiveObj(ROIList[curSel]);
}


