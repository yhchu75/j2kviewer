#include "StdAfx.h"

HRESULT Imaging_SaveToFile(HBITMAP handle, LPCWSTR filename, LPCSTR format, float fImageQuality)
{
	HRESULT res;

	res = CoInitialize(NULL);
	//if (res == S_FALSE) return res; 만약 CoIntialize 되어 있을 경우는 실페가 나오게 됨..

	IWICImagingFactory *piFactory = NULL;
	IWICBitmapEncoder *piEncoder = NULL;
	IWICBitmapFrameEncode *piBitmapFrame = NULL;
	IPropertyBag2 *pPropertybag = NULL;
	IWICStream *piStream = NULL;

	res = CoCreateInstance(
		CLSID_WICImagingFactory,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IWICImagingFactory,
		(LPVOID*)&piFactory);
	if (FAILED(res)) goto EXIT;
	
	res = piFactory->CreateStream(&piStream);
	if (FAILED(res))goto EXIT;
	
	res = piStream->InitializeFromFilename(filename, GENERIC_WRITE);
	if (FAILED(res))goto EXIT;


	GUID codecGUID;
	if (strcmp("jpeg", format) == 0){
		codecGUID = GUID_ContainerFormatJpeg;
	}
	else if (strcmp("png", format) == 0){
		codecGUID = GUID_ContainerFormatPng;
	}
	else{
		codecGUID = GUID_ContainerFormatBmp;
	}

	res = piFactory->CreateEncoder(codecGUID, NULL, &piEncoder);
	if (FAILED(res))goto EXIT;
	
	res = piEncoder->Initialize(piStream, WICBitmapEncoderNoCache);
	if (FAILED(res))goto EXIT;

		res = piEncoder->CreateNewFrame(&piBitmapFrame, &pPropertybag);
		if (FAILED(res))goto EXIT;


		//Set option if need 
	{
		PROPBAG2 option = { 0 };
		option.pstrName = L"ImageQuality";
		VARIANT varValue;    
		VariantInit(&varValue);
		varValue.vt = VT_R4;
		varValue.fltVal = fImageQuality;
		res = pPropertybag->Write(1, &option, &varValue);
		if (SUCCEEDED(res))
		{
			res = piBitmapFrame->Initialize(pPropertybag);
		}
		else{
			res = piBitmapFrame->Initialize(0);
		}
	}
	if (FAILED(res))goto EXIT;
	
	BITMAP bm;
	GetObject(handle, sizeof(BITMAP), &bm);
	WICPixelFormatGUID formatGUID, OrigineformatGUID;
	switch (bm.bmBitsPixel) {
		case 15: {
					 formatGUID = GUID_WICPixelFormat16bppBGR555;
					break;
		}
		case 16: {
					 formatGUID = GUID_WICPixelFormat16bppBGR565;
					break;
		}
		case 24: {
					 formatGUID = GUID_WICPixelFormat24bppBGR;
					 break;
		}
		default: {
					 formatGUID = GUID_WICPixelFormat32bppBGRA;
					 break;
		}
	}

	res = piBitmapFrame->SetSize(bm.bmWidth, bm.bmHeight);
	if (FAILED(res))goto EXIT;

	OrigineformatGUID = formatGUID;
	res = piBitmapFrame->SetPixelFormat(&formatGUID);
	if (FAILED(res))goto EXIT;
	
	// We're expecting to write out 24bppRGB. Fail if the encoder cannot do it.
	res = IsEqualGUID(formatGUID, OrigineformatGUID) ? S_OK : E_FAIL;
	BOOL bNeedToConvert24bit = FALSE;

	if (IsEqualGUID(OrigineformatGUID, GUID_WICPixelFormat32bppBGRA) &&
		IsEqualGUID(formatGUID, GUID_WICPixelFormat24bppBGR)){
		bNeedToConvert24bit = TRUE;
	}
	else if (FAILED(res))goto EXIT;

	/*

	IWICBitmapSource *pSource = NULL;

	res = WICConvertBitmapSource(GUID_WICPixelFormat32bppBGRA, piBitmapFrame, &pConverter);
	if (SUCCEEDED(hr))
	{
		pSource->Release();     // the converter has a reference to the source
		pSource = NULL;         // so we don't need it anymore.
		pSource = pConverter;   // let's treat the 128bppPABGR converter as the source
	}
	*/


	UINT cbStride = bm.bmWidthBytes; /***WICGetStride***/;
	UINT cbBufferSize = abs(bm.bmHeight) * cbStride;
	BYTE *pbBuffer = new BYTE[cbBufferSize];

	if (pbBuffer != NULL)
	{
		GetBitmapBits(handle, cbBufferSize, pbBuffer);

		//convert 32bit -> 24bit 
		if (bNeedToConvert24bit){
			cbStride = (bm.bmWidth * 24 + 7) / 8/***WICGetStride***/;
			cbBufferSize = abs(bm.bmHeight) * cbStride;

			int pos_src, pos_dst;
			for (int i = 0; i < abs(bm.bmHeight); i++){
				for (int j = 0; j < bm.bmWidth; j++){
					pos_dst = 3*(i*bm.bmWidth + j);
					pos_src = 4*(i*bm.bmWidth + j);
					pbBuffer[pos_dst] = pbBuffer[pos_src];
					pbBuffer[pos_dst+1] = pbBuffer[pos_src+1];
					pbBuffer[pos_dst+2] = pbBuffer[pos_src+2];
				}
			}
		}
		
		res = piBitmapFrame->WritePixels(bm.bmHeight, cbStride, cbBufferSize, pbBuffer);
		delete[] pbBuffer;
	}
	else
	{
		res = E_OUTOFMEMORY;
	}
	if (FAILED(res))goto EXIT;


	res = piBitmapFrame->Commit();
	if (FAILED(res))goto EXIT;

	res = piEncoder->Commit();


EXIT:

	if (piBitmapFrame) piBitmapFrame->Release();
	if (pPropertybag) pPropertybag->Release();
	if (piEncoder)piEncoder->Release();
	if (piStream)piStream->Release();
	if (piFactory)piFactory->Release();
	
	CoUninitialize();

	return res;
}