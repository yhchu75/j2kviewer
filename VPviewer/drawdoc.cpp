// drawdoc.cpp : implementation of the CDrawDoc class
//

#include "stdafx.h"
#include "kdu_show.h"

#include "drawdoc.h"
#include "ChildView.h"
#include "drawobj.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDrawDoc



/////////////////////////////////////////////////////////////////////////////
// CDrawDoc construction/destruction

CDrawDoc::CDrawDoc()
{

	//m_nMapMode = MM_ANISOTROPIC;
	m_nMapMode = MM_TEXT;
}

CDrawDoc::~CDrawDoc()
{
	POSITION pos = m_objects.GetHeadPosition();
	while (pos != NULL)
		delete m_objects.GetNext(pos);
}



/////////////////////////////////////////////////////////////////////////////
// CDrawDoc serialization

void CDrawDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		m_objects.Serialize(ar);
	}
	else
	{
		m_objects.Serialize(ar);
	}
}


/////////////////////////////////////////////////////////////////////////////
// CDrawDoc implementation

void CDrawDoc::Draw(CDC* pDC, CChildView* pView)
{
	POSITION pos = m_objects.GetHeadPosition();
	while (pos != NULL)
	{
		CDrawObj* pObj = m_objects.GetNext(pos);
		pObj->Draw(pDC, pView);
		if (pView->IsSelected(pObj))  
			pObj->DrawTracker(pDC, CDrawObj::selected);
	}
}

void CDrawDoc::Add(CDrawObj* pObj)
{
	//TRACE("CDRAWDOC Add m_object............\n");
	m_objects.AddTail(pObj);
	pObj->m_pDocument = this;
}

void CDrawDoc::Cleanup()
{
	POSITION pos = m_objects.GetHeadPosition();
	while(pos != NULL)
	{
		delete m_objects.GetNext(pos);
	}
	m_objects.RemoveAll();

}

void CDrawDoc::Remove(CDrawObj* pObj)
{
	// Find and remove from document
	POSITION pos = m_objects.Find(pObj);
	if (pos != NULL)
		m_objects.RemoveAt(pos);
	// set document modified flag

// MJY: todo : remove from child view

	//// call remove for each view so that the view can remove from m_selection
	//pos = GetFirstViewPosition();
	//while (pos != NULL)
	//	((CDrawView*)GetNextView(pos))->Remove(pObj);
}

// point is in logical coordinates
CDrawObj* CDrawDoc::ObjectAt(const CPoint& point, CChildView *pView)
{
	CRect rect(point, CSize(1, 1));
	TRACE("CDrawDoc::ObjectAt rect :::::: %d %d %d %d\n", rect.top, rect.left, rect.bottom, rect.right);
	POSITION pos = m_objects.GetTailPosition();
	while (pos != NULL)
	{
		CDrawObj* pObj = m_objects.GetPrev(pos);
		if (pObj->Intersects(rect, pView))
			return pObj;
	}

	return NULL;
}



/////////////////////////////////////////////////////////////////////////////
// CDrawDoc diagnostics

#ifdef _DEBUG
void CDrawDoc::AssertValid() const
{
	CObject::AssertValid();
}

void CDrawDoc::Dump(CDumpContext& dc) const
{
	CObject::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDrawDoc commands
