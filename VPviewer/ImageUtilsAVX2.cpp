#include <stdafx.h>
#include <tmmintrin.h>
#include <immintrin.h>

void	MixLayerAVX2(int data_len, unsigned char* in_src1, unsigned char* in_src2, unsigned char* out_dst, int alpha)
{
	int avx_len = data_len / 32;
	int avx_remaind = data_len % 32;

	__m256i src, alpha1, alpha2, zero;
	__m256i temp1_1, temp1_2, temp2_1, temp2_2;

	alpha = (64 * alpha) / 100;

	alpha1 = _mm256_set1_epi16(alpha);
	alpha2 = _mm256_set1_epi16(64 - alpha);
	zero = _mm256_set1_epi8(0);

	for (int i = 0; i < avx_len ; i++){
		src = _mm256_stream_load_si256((__m256i*)in_src1);
		temp1_1 = _mm256_unpacklo_epi8(src, zero);
		temp1_1 = _mm256_mullo_epi16(temp1_1, alpha1);

		temp1_2 = _mm256_unpackhi_epi8(src, zero);
		temp1_2 = _mm256_mullo_epi16(temp1_2, alpha1);

		src = _mm256_stream_load_si256((__m256i*)in_src2);
		temp2_1 = _mm256_unpacklo_epi8(src, zero);
		temp2_1 = _mm256_mullo_epi16(temp2_1, alpha2);

		temp2_2 = _mm256_unpackhi_epi8(src, zero);
		temp2_2 = _mm256_mullo_epi16(temp2_2, alpha2);

		temp1_1 = _mm256_adds_epu16(temp1_1, temp2_1);
		temp1_2 = _mm256_adds_epu16(temp1_2, temp2_2);

		temp1_1 = _mm256_srli_epi16(temp1_1, 6);
		temp1_2 = _mm256_srli_epi16(temp1_2, 6);

		temp1_1 = _mm256_packus_epi16(temp1_1, temp1_2);

		_mm256_stream_si256((__m256i*)out_dst, temp1_1);

		in_src1 += 32;
		in_src2 += 32;
		out_dst += 32;

	}
}




