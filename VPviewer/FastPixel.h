#pragma once

#include "StdAfx.h"

class CFastPixel
{
public:
	CFastPixel(){}
	~CFastPixel(){}
	
	void		UpdateBufInfo(DWORD* pSrc, int nHeight, int nBytePerLine){
		m_pBits = pSrc; m_nBytePerLine = nBytePerLine; m_nHeight = nHeight;
	}

	COLORREF	GetPixel(int x, int y){
		DWORD dw = ((DWORD*)m_pBits)[x + (y * (m_nBytePerLine / sizeof(DWORD)))];
		return ((dw & 0x000000FF) << 16) + (dw & 0x0000FF00) + ((dw & 0x00FF0000) >> 16);
	}

	LONG		GetHeight(){ return m_nHeight; }

protected:
	LPVOID	m_pBits;
	int		m_nBytePerLine;
	int		m_nHeight;
};
