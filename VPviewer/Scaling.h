#pragma once
#include "resource.h"
#include "afxwin.h"

class ScaleList
{
public:
	CStringW	check;
	float		value;
	CStringW	unit;
};
// CScaling dialog
class CVPViewerConf;

class CScaling : public CDialog
{
	DECLARE_DYNAMIC(CScaling)

public:
	CScaling(CKdu_showApp *app, CWnd* pParent = NULL);   // standard constructor
	virtual ~CScaling();
// Dialog Data
	enum { IDD = IDD_SCALING };

	float GetCurSelScale();

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnBnClickedApply();
	afx_msg void OnLbnSelchangeScaleList();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	DECLARE_MESSAGE_MAP()

private:
	CKdu_showApp	*app;
	CVPViewerConf	*config;
	CEdit			m_scaleCustom;
	CStatic			m_scaleLabel;

	CListBox		m_scaleList;
	//ScaleList		scales[6];	
	void			fill_listBox();
	void			read_list();
	void			write_list();
	
	std::vector<ScaleList> m_scales;
	std::vector<CString> target;
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonAddscale();
	afx_msg void OnBnClickedButtonDeletescale();
	CButton m_deleteScale;
};
