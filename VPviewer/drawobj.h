// drawobj.h - interface for CDrawObj and derivatives
//


#ifndef __DRAWOBJ_H__
#define __DRAWOBJ_H__

#include "VPViewerConf.h"
#include "IVPViewerCore.h"

typedef enum {
	ROTATE_NONE,
	ROTATE_90,
	ROTATE_180,
	ROTATE_270,
}eRotateStatus;


/////////////////////////////////////////////////////////////////////////////
// CDrawObj - base class for all 'drawable objects'
class CFastPixel;

class CDrawObj 
{
public:
	typedef enum{ NONE, NORMAL, HOVER, SELECT} eSTATUS;
	typedef enum{ MOUSE_STATUS_NONE, MOUSE_STATUS_MOVE, MOUSE_STATUS_LBUTTONW_DOWN, MOUSE_STATUS_LBUTTON_UP,
		MOUSE_STATUS_RBUTTONW_DOWN, MOUSE_STATUS_RBUTTON_UP	} eMouseSTATUS;
		
	static void		InitDrawTools();
	static void		setImageSize(SIZE size){ m_ImageSize = size; }
	static void		setCanvasRect(RECT rc){ m_CanvasRect = rc; }
	static void		setRenderingScale(float scale){ m_RenderingScale = scale; }
	static void		setRotateStatus(eRotateStatus status){ m_RotateStatus = status; }
	static void		setAppliedScale(float scale);
	static void		DrawTranspentText(CDC *dc, int pos_x, int pos_y, const wchar_t *str);
	static void		setAutoUnit(bool status);
	static bool		getAutoUnit();
	static void		setFastPixelBackBuf(CFastPixel*	pFastPixelBackBuf){ m_pFastPixelBackBuf = pFastPixelBackBuf;}
	
	// RGB MEASUREMENT
	// ============================================================================
	static void		DrawTranspentText(CDC *dc, int pos_x, int pos_y, const wchar_t *rgb, const wchar_t *str);
	// ============================================================================

	POINT	ConvertPixelPoint(POINTF pt_f);
	POINTF	ConvertRelativePoint(POINT pt);

	POINT	ConvertPixelPointByCanvas(POINTF pt_f){
		POINT pt = { lround(pt_f.x * (m_CanvasRect.right - m_CanvasRect.left)), lround(pt_f.y * (m_CanvasRect.bottom - m_CanvasRect.top)) };
		return pt;
	}

	POINTF	ConvertRelativePointByCanvas(POINT pt){
		POINTF pt_f = { ((float)pt.x / (m_CanvasRect.right - m_CanvasRect.left)), ((float)pt.y / (m_CanvasRect.bottom - m_CanvasRect.top)) };
		return pt_f;
	}

	CDrawObj() :m_status(NORMAL), m_hHitRegion(NULL), m_NodeCnt(0), m_pNodePoints(NULL), m_drawing(false){
		memset(m_arrRgbRed, 0, sizeof(m_arrRgbRed)); 
		memset(m_arrRgbGreen, 0, sizeof(m_arrRgbGreen));
		memset(m_arrRgbBlue, 0, sizeof(m_arrRgbBlue));
	}

	virtual ~CDrawObj(){ 
		if (m_hHitRegion) DeleteObject(m_hHitRegion); 
		if (m_pNodePoints) delete[]  m_pNodePoints; 
	}

	virtual eOBJTYPE	GetObjType() = 0;
	virtual void		Draw(CDC *pDC, bool isSelected) = 0;
	virtual bool		HitTest(POINT pt) = 0;
	virtual	void		UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status = MOUSE_STATUS_NONE) = 0;
	virtual bool		UpdateMousePos(POINT pt) = 0;
	virtual void		EditDescription(CString description) = 0;
	virtual CString		GetDescription() = 0;

	eSTATUS	getStatus(){ return m_status; }
	void	setStatus(eSTATUS	status){ m_status = status; }
		
	void		setDrawing(bool status) { m_drawing = status; }
	bool		isDrawing() { return m_drawing; }

	void		setDrawn(bool status) { m_drawn = status; }
	bool		isDrawn() { return m_drawn; }	

	
	// RGB MEASUREMENT
	// ============================================================================
	unsigned int m_arrRgbRed[256] ;
	unsigned int m_arrRgbGreen[256];
	unsigned int m_arrRgbBlue[256];
	// ============================================================================
		
protected:
	
	eSTATUS			m_status;
	HRGN			m_hHitRegion;
	int				m_nCurSelPt;
	POINTF			m_SelectPtf;
	bool			m_drawing;
	bool			m_drawn;

	virtual	void	InitObj() = 0;
	int				m_NodeCnt;
	POINT*			m_pNodePoints;

	//current support 2 for line and 4 for rect
	void			MakeLineHitRegion(int count, POINT* pPoints);
	void			MakeRegionHitRegion(int count, POINT* pPoints);
	int				FindSelectNode(POINT TargetPoint);
	
	static SIZE		m_ImageSize;
	static RECT		m_CanvasRect;
	static int		m_curLayer;
	static float	m_RenderingScale;	//pixel base 
	static float	m_AppliedScale;		//
	static CString	m_unit;
	static CPen		m_Pen[MAX_PEN_TYPE];
	static CBrush	m_HatchBrush;
	static CFont	m_font;
	static eRotateStatus m_RotateStatus;
	static CString	getCurLineLengthString(int pixel_length);
	static CString	getDrawedLineLengthString(double Line_length);
	static bool		m_AutoUnit;
	static CFastPixel*	m_pFastPixelBackBuf;
		
	CString	getDrawedAreaString(double length);

	POINT			m_GridAnalysis_start;
	POINT			m_GridAnalysis_end;
	POINTF			m_GridAnalysis_startF;
	POINTF			m_GridAnalysis_endF;
	
	// RGB MEASUREMENT
	// ============================================================================
	BOOL m_bShowRGB = TRUE;
	bool m_bMeasureComplete = false;
	CArray<CRect> m_arrMeasureRect;
	CString m_strRGBText = L"";
	int m_nColorR = 0;
	int m_nColorG = 0;
	int m_nColorB = 0;
	int m_nPixelCnt = 0;
	// ============================================================================
};


#define DEFINE_TEMPLATE_DRAW_OBJECT(_CLASS_NAME, _INFO_NAME)	\
	_CLASS_NAME(_INFO_NAME *info){ m_info = *info; InitObj(); }\
	void	setInfo(_INFO_NAME *info){ m_info = *info; }\
	_INFO_NAME*	getInfo(){ return &m_info; }\
	virtual void	Draw(CDC *pDC, bool isSelected); \
	virtual bool	HitTest(POINT pt); \
	virtual void	UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status = MOUSE_STATUS_NONE); \
	virtual bool	UpdateMousePos(POINT pt); \
	virtual void	EditDescription(CString description); \
	CString GetDescription();\
protected:\
	void	InitObj(); \
	_INFO_NAME	m_info;\


class CScaleBarObj : public CDrawObj
{
public:
	CScaleBarObj(POINT pt, ePENCOLOR color);
	eOBJTYPE	GetObjType(){ return SCALEBAR; }

	DEFINE_TEMPLATE_DRAW_OBJECT(CScaleBarObj, ScaleBarInfo);

private:
	CStringW	getCurScaleLengthString(int pixel_length);
	int			m_scale;
	float		m_length;
	float		m_bar_length;
	POINT		m_second_pt;
};


class CROIObj : public CDrawObj
{
public:
	CROIObj(POINT pt, ePENCOLOR color, std::string description);
	eOBJTYPE	GetObjType(){ return ROI; }
	
	DEFINE_TEMPLATE_DRAW_OBJECT(CROIObj, ROIInfo);
};
 	

class CMeasureObj : public CDrawObj
{
public:
	CMeasureObj(eOBJTYPE type, POINT pt, ePENCOLOR color, std::string description);
	eOBJTYPE	GetObjType(){ return m_info.type; }

	DEFINE_TEMPLATE_DRAW_OBJECT(CMeasureObj, MeasureInfo);

public:
	void getLineLengthStr(wchar_t* strTemp){
		StrCpyW(strTemp, getDrawedLineLengthString(m_length).GetBuffer());
	}
	void getAreaStr(wchar_t* strTemp){
		StrCpyW(strTemp, getDrawedAreaString(m_length).GetBuffer());
	}
	void getUnit(wchar_t* strTemp){
		StrCpyW(strTemp, m_unit);		
	}
	CString			getLengthXMLOutputStringWithUnit();
private:
	double		m_length;
};
		
class CLabelObj : public CDrawObj
{
public:
	CLabelObj(POINT pt, COLORREF fontColor, COLORREF bgColor, bool transparent, float fontSize, std::string description);
	eOBJTYPE	GetObjType(){ return LABEL; }
	void		DrawLabelText(CDC *dc, int pos_x, int pos_y, COLORREF fColor, COLORREF bColor, bool isTransparent, const wchar_t *str);
	DEFINE_TEMPLATE_DRAW_OBJECT(CLabelObj, LabelInfo);

};

class CRegionRectObj : public CROIObj
{
public:
	CRegionRectObj(POINT pt, ePENCOLOR color, std::string description) :
		CROIObj(pt, color, description) {}
	eOBJTYPE	GetObjType() { return REGSION_RECT; }

	void	Draw(CDC *pDC, bool isSelected); 
};


class CRegionEllipseObj : public CROIObj
{
public:
	CRegionEllipseObj(POINT pt, ePENCOLOR color, std::string description) :
		CROIObj(pt, color, description) {}
	eOBJTYPE	GetObjType() { return REGSION_ELLIPSE; }

	void	Draw(CDC *pDC, bool isSelected);
};


class CGridObj : public CDrawObj
{
public:
	typedef enum { GRID_RATIO_1X1, GRID_RATIO_16X9, GRID_RATIO_4X3 } eGridRatioMode;
	typedef enum { GRID_ANALYSIS_NONE, GRID_ANALYSIS, GRID_ANALYSIS_REGION_DRAW, GRID_ANALYSIS_REGION_ANALYSIS} eGridAnlyzeMode;
	enum { CELL_NORMAL_POS , CELL_LEFT_POS = 1, CELL_BOTTOM_POS	= 2} ;

	
	CGridObj(POINT pt, ePENCOLOR color);
	~CGridObj();

	eOBJTYPE	GetObjType(){ return GRID; }
	POINT		GetPosition(){ return m_position; }
	void		SetPosition(int x, int y) { m_position.x = x; m_position.y = y; }
	void		SetSize(int width, int height) { m_size.x = width; m_size.y = height; }
	POINT		GetSize(){ return m_size; }
	int			GetInterval(){ return real_interval; }
	void		DrawCross(CDC *pDC, POINT p, int gap, COLORREF color);
	void		SetCounterArray(int* array){ m_counterArray = array; }
	void		SetCross(bool cross){ m_drawCross = cross; }
	void		SetRedCross(bool cross){ m_drawRedCross = cross; }
	POINTF		GetCenter(){ return m_center; }
	POINTF		GetStep() { return m_step; }
	void		SetGridAnalize(POINT pt) { m_GridAnalysis_start = pt; m_GridAnalysis_end = pt; }
		
	void		SetZoomLevel(int zoomLevel);
	void		SetGridRatio(eGridRatioMode mode) { 
		m_GridRatioMode = mode;
	}
		
	void		SetAnalysisMode(eGridAnlyzeMode mode) {m_AnalysisMode = mode;}
	void		SetAnalysisRegionMode(bool bSelectAll) { m_RegionSelectAll = bSelectAll;}

	eGridAnlyzeMode	GetAnalysisMode() { return	m_AnalysisMode; }
	void		AddDrawObj(CDrawObj *pDrawObj);

	void		UpdateCellStatus();
	void		UpdateSectorCellStatus();
	void		UpdateGridCellStatus(BOOL clearCell = FALSE);
	int			GetCellCount() { return m_SectorCellInfo.size(); }

	std::vector<GridCellInfo>& GetRegionCellInfo() {return m_SectorCellInfo;}
	std::vector<GridCellInfo>& GetSectorCellInfo() { return m_GridCellInfo; }

	void		UpdateGridInterval() {
		m_grid_interval = m_info.interval;
		UpdateGridCellStatus();
	}

	void		SetSectorInterval(int interval);
	int			MoveSectorCentorPos(int index, POINTF* ptf, float *scale);
	void		CheckSectorSelect(POINT& pt);

	int			SetSectorCellInfo(CPoint point, int fossil_index, DWORD color);
	int			SetSectorCellInfo(int cell_index, int fossil_index, DWORD color);
		
	void		Reset();
	DEFINE_TEMPLATE_DRAW_OBJECT(CGridObj, GridInfo);
		
private:
	CStringW	getCurGridIntervalString();
	
	
	//BOOL		DrawRegion(CDC *pDC, bool isSelected);
	int			m_scale;
	int			m_sector_interval;
	int			m_grid_interval;
	POINT		m_position;
	POINT		m_size;
	POINTF		m_center;
	POINTF		m_step;
	int			real_interval;
	
	bool		m_drawCross = true;
	bool		m_drawRedCross = true;
	int			*m_counterArray = nullptr;
	
	eGridRatioMode		m_GridRatioMode;
	eGridAnlyzeMode		m_AnalysisMode;

	bool		m_RegionSelectAll;
	std::vector<CDrawObj*>	m_AnalysisRegions;

	int			m_CurrentSelectIndex;
	std::vector<GridCellInfo>	m_SectorCellInfo;
	std::vector<GridCellInfo>	m_GridCellInfo;

	int GetHitCell(POINT& pt);

};


#endif // __DRAWOBJ_H__
