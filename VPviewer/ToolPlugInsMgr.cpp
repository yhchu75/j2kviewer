#include "stdafx.h"
#include "ToolPlugInsMgr.h"


IToolPlugIn*	CreateToolPlugIn(TCHAR *szModuleName)
{
	if (NULL == _tcsstr(szModuleName, _T(".dll"))){
		TRACE(_T("%s is not dll.\n"), szModuleName);
		return NULL;
	}

	HMODULE hLib = NULL;
	hLib = LoadLibrary(szModuleName);
	if (NULL == hLib){
		TRACE(_T("Failed to Load %s. GetLastError [%d]\n"), szModuleName, GetLastError());
		return NULL;
	}
		
	typedef IToolPlugIn* (FAR WINAPI* FP_CreateToolPlugIn)();
	FP_CreateToolPlugIn pCreateToolPlugIn = (FP_CreateToolPlugIn)GetProcAddress(hLib, "CreateToolPlugIn");
	if (pCreateToolPlugIn == NULL){
		TRACE(_T("Failed to GetProcAddress %s. GetLastError [%d]\n"), szModuleName, GetLastError());
		FreeLibrary(hLib);
		return NULL;
	}

	IToolPlugIn* pPlugInsTool = pCreateToolPlugIn();
	if (pPlugInsTool == NULL){
		TRACE(_T("Failed to CreatePlugInsTool %s. GetLastError [%d]\n"), szModuleName, GetLastError());
		FreeLibrary(hLib);
		return NULL;
	}
		
	return pPlugInsTool;
}


CToolPlugInsMgr::CToolPlugInsMgr()
{
}


CToolPlugInsMgr::~CToolPlugInsMgr()
{
	//remove
	for (IToolPlugIn * item : m_ToolsPlugIns) item->DestorySelf();
}

void CToolPlugInsMgr::OnClose()
{
	for (IToolPlugIn * item : m_ToolsPlugIns) item->Show(NULL, FALSE);
}

size_t	CToolPlugInsMgr::Load(const TCHAR *szRootPath, IVPViewerCore *pVPViewerCore)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	TCHAR szFindFileFilter[MAX_PATH] = { 0 };
	_tcscpy_s(szFindFileFilter, szRootPath);
	_tcscat_s(szFindFileFilter, _T("\\*.dll"));

	hFind = FindFirstFile(szFindFileFilter, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE){
		TRACE(_T("Failed to find modules in [%s]. GetLastError [%d]\n"), szFindFileFilter, GetLastError());
		return FALSE;
	}

	while (1){
		HMODULE hModule = NULL;
		_stprintf_s(szFindFileFilter, _T("%s\\%s"), szRootPath, FindFileData.cFileName);

		TRACE(L"file name : %s\n", FindFileData.cFileName);

		IToolPlugIn *pToolPlugIn = CreateToolPlugIn(szFindFileFilter);
		if (pToolPlugIn){
			//AfxMessageBox(FindFileData.cFileName);

			pToolPlugIn->Setup(pVPViewerCore);
			m_ToolsPlugIns.push_back(pToolPlugIn);
			TRACE("Load ToolPlugIn [%s]\n", pToolPlugIn->GetPlugInsToolName());
		}

		if (FindNextFile(hFind, &FindFileData) == 0)break;
	}

	return m_ToolsPlugIns.size();
}


int FindMenuItem(CMenu* Menu, LPCTSTR MenuString)
{
	ASSERT(Menu);
	ASSERT(::IsMenu(Menu->GetSafeHmenu()));

	int count = Menu->GetMenuItemCount();
	for (int i = 0; i < count; i++)
	{
		CString str;
		if (Menu->GetMenuString(i, str, MF_BYPOSITION) &&
			str.Compare(MenuString) == 0)
			return i;
	}

	return -1;
}


void	CToolPlugInsMgr::InsertMenu(CMenu *pMenu, int BaseMENUIndex)
{
	int pos = FindMenuItem(pMenu, _T("&Help"));

	m_ToolMenu.CreatePopupMenu();
	USES_CONVERSION;
	for (int i = 0; i < m_ToolsPlugIns.size(); i++){
		m_ToolMenu.InsertMenu(i, MF_BYPOSITION, BaseMENUIndex + i, A2T(m_ToolsPlugIns[i]->GetPlugInsToolName()));
	}
	pMenu->InsertMenu(pos, MF_POPUP | MF_BYPOSITION, (UINT_PTR)m_ToolMenu.m_hMenu, _T("&PlugIns"));
}

void	CToolPlugInsMgr::EnableMenu(CMenu *pMenu, int BaseMENUIndex, bool enable)
{
	int pos = FindMenuItem(pMenu, _T("&PlugIns"));
	if (pos < 0)
		return;
	if (enable)
	pMenu->EnableMenuItem(pos, MF_BYPOSITION | MF_ENABLED);
	else
	pMenu->EnableMenuItem(pos, MF_BYPOSITION | MF_DISABLED);
	/*CMenu *sub_menu = pMenu->GetSubMenu(pos);
	for (int i = 0; i < m_ToolsPlugIns.size(); i++){
		if (enable)
			sub_menu->EnableMenuItem(BaseMENUIndex + i, MF_BYCOMMAND | MF_ENABLED);
		else
			sub_menu->EnableMenuItem(BaseMENUIndex + i, MF_BYCOMMAND | MF_DISABLED);
	}*/
}

void CToolPlugInsMgr::OnMenuCommand(int index)
{
	if (index >= m_ToolsPlugIns.size() || index < 0) return;
	m_ToolsPlugIns[index]->Show(NULL, TRUE);
}


BOOL CToolPlugInsMgr::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	for (IToolPlugIn* pToolPlugIn : m_ToolsPlugIns){
		if (pToolPlugIn->IsActive()){
			return pToolPlugIn->OnLButtonDown(hWnd, nFlags, point);
		}
	}
	return 0;
}


BOOL CToolPlugInsMgr::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	for (IToolPlugIn* pToolPlugIn : m_ToolsPlugIns){
		if (pToolPlugIn->IsActive()){
			return pToolPlugIn->OnLButtonMove(hWnd, nFlags, point);
		}
	}
	return 0;
}


BOOL CToolPlugInsMgr::OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point)
{
	for (IToolPlugIn* pToolPlugIn : m_ToolsPlugIns) {
		if (pToolPlugIn->IsActive()) {
			return pToolPlugIn->OnLButtonUp(hWnd, nFlags, point);
		}
	}
	return 0;
}


// NUMPAD
// ============================================================================
BOOL CToolPlugInsMgr::OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags)
{
	for (IToolPlugIn* pToolPlugIn : m_ToolsPlugIns){
		if (pToolPlugIn->IsActive()){
			return pToolPlugIn->OnKeyUp(hWnd, nChar, nRepCnt, nFlags);
		}
	}
	return 0;
}
// ============================================================================
