// drawobj.cpp - implementation for drawing objects
//

#include "stdafx.h"
#include "drawobj.h"
#include "IVPViewerCore.h"

// RGB MEASUREMENT
// ============================================================================
#include "FastPixel.h"
// ============================================================================

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static		const	int TEXT_HEIGHT = 12;
static		const	int MAX_REGION_DIFF = 6;
static		const	int MAX_POINT_DIFF = 4;

SIZE		CDrawObj::m_ImageSize = { 0, 0 };
RECT		CDrawObj::m_CanvasRect;
float		CDrawObj::m_RenderingScale = 1.0f;	//pixel base 
float		CDrawObj::m_AppliedScale = 1.0f;		//
CString	CDrawObj::m_unit = L"pixel";
int			CDrawObj::m_curLayer = 0;
CPen		CDrawObj::m_Pen[MAX_PEN_TYPE];
CBrush		CDrawObj::m_HatchBrush;
CFont		CDrawObj::m_font;
eRotateStatus	CDrawObj::m_RotateStatus = ROTATE_NONE;
bool		CDrawObj::m_AutoUnit = false;
CFastPixel*		CDrawObj::m_pFastPixelBackBuf = NULL;


inline void	DrawNodeCircle(CDC *pDC, POINT pt, bool isSelected)
{
	CPen selPen;
	CPen *pOldPen;
	CBrush selBrush;
	CBrush *pOldBrush;
	if (isSelected)
	{
		selPen.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		
		LOGBRUSH logBrush;
		logBrush.lbStyle = BS_SOLID;
		logBrush.lbColor = RGB(255, 255, 255); // ignored
		logBrush.lbHatch = HS_CROSS; // ignored
		selBrush.CreateBrushIndirect(&logBrush);

		pOldPen = (CPen *)pDC->SelectObject(&selPen);
		pOldBrush = (CBrush *)pDC->SelectObject(&selBrush);

		RECT CenterPos;

		CenterPos.left = pt.x - MAX_POINT_DIFF;
		CenterPos.right = pt.x + MAX_POINT_DIFF;
		CenterPos.top = pt.y - MAX_POINT_DIFF;
		CenterPos.bottom = pt.y + MAX_POINT_DIFF;
		//pDC->Ellipse(&CenterPos);
		pDC->Rectangle(&CenterPos);

		if (pOldPen)pDC->SelectObject(pOldPen);
		if (pOldBrush)pDC->SelectObject(pOldBrush);
	}
	else
	{
		RECT CenterPos;

		CenterPos.left = pt.x - MAX_POINT_DIFF;
		CenterPos.right = pt.x + MAX_POINT_DIFF;
		CenterPos.top = pt.y - MAX_POINT_DIFF;
		CenterPos.bottom = pt.y + MAX_POINT_DIFF;
		pDC->Ellipse(&CenterPos);
	}
}


void	CDrawObj::InitDrawTools()
{
	m_Pen[BLOCK].CreatePen(PS_SOLID, 3, RGB(0, 0, 0));
	m_Pen[WHITE].CreatePen(PS_SOLID, 3, RGB(255, 255, 255));
	m_Pen[RED].CreatePen(PS_SOLID, 3, RGB(255, 0, 0));
	m_Pen[GREEN].CreatePen(PS_SOLID, 3, RGB(0, 255, 0));
	m_Pen[BLUE].CreatePen(PS_SOLID, 3, RGB(0, 0, 255));
	m_Pen[YELLOW].CreatePen(PS_SOLID, 3, RGB(192, 192, 0));
	
	LOGBRUSH logBrush;
	logBrush.lbStyle = BS_NULL;
	logBrush.lbColor = RGB(0, 192, 192); // ignored
	logBrush.lbHatch = HS_CROSS; // ignored
	m_HatchBrush.CreateBrushIndirect(&logBrush);

	m_font.CreatePointFont(150, _T("tahoma"));
}


void	CDrawObj::setAppliedScale(float scale)
{ 
	m_AppliedScale = scale;
	m_unit = (m_AppliedScale == 1.0f) ? L"pixel" : L"μm";
}

void	CDrawObj::setAutoUnit(bool status)
{
	m_AutoUnit = status;
}

bool	CDrawObj::getAutoUnit()
{
	return m_AutoUnit;
}


void	CDrawObj::DrawTranspentText(CDC *dc, int pos_x, int pos_y, const wchar_t *str)
{
	dc->SelectObject(m_font);
	dc->SetBkMode(TRANSPARENT);
	dc->SetTextColor(RGB(0, 0, 0));
	dc->TextOut(pos_x - 1, pos_y - 1, str);
	dc->TextOut(pos_x + 1, pos_y - 1, str);
	dc->TextOut(pos_x - 1, pos_y + 1, str);
	dc->TextOut(pos_x + 1, pos_y + 1, str);
	dc->TextOut(pos_x - 1, pos_y, str);
	dc->TextOut(pos_x + 1, pos_y, str);
	dc->TextOut(pos_x, pos_y - 1, str);
	dc->TextOut(pos_x, pos_y + 1, str);
	dc->SetTextColor(RGB(255, 255, 255));
	dc->TextOut(pos_x, pos_y, str);
}


// RGB MEASUREMENT
// ============================================================================
void	CDrawObj::DrawTranspentText(CDC *dc, int pos_x, int pos_y, const wchar_t *rgb, const wchar_t *str)
{
	int top_diff = 25;
	dc->SelectObject(m_font);
	dc->SetBkMode(TRANSPARENT);

	dc->SetTextColor(RGB(0, 0, 0));
	dc->TextOut(pos_x - 1, pos_y - 1 - top_diff, rgb);
	dc->TextOut(pos_x + 1, pos_y - 1 - top_diff, rgb);
	dc->TextOut(pos_x - 1, pos_y + 1 - top_diff, rgb);
	dc->TextOut(pos_x + 1, pos_y + 1 - top_diff, rgb);
	dc->TextOut(pos_x - 1, pos_y - top_diff, rgb);
	dc->TextOut(pos_x + 1, pos_y - top_diff, rgb);
	dc->TextOut(pos_x, pos_y - 1 - top_diff, rgb);
	dc->TextOut(pos_x, pos_y + 1 - top_diff, rgb);
	dc->SetTextColor(RGB(255, 255, 255));
	dc->TextOut(pos_x, pos_y - top_diff, rgb);

	dc->SetTextColor(RGB(0, 0, 0));
	dc->TextOut(pos_x - 1, pos_y - 1, str);
	dc->TextOut(pos_x + 1, pos_y - 1, str);
	dc->TextOut(pos_x - 1, pos_y + 1, str);
	dc->TextOut(pos_x + 1, pos_y + 1, str);
	dc->TextOut(pos_x - 1, pos_y, str);
	dc->TextOut(pos_x + 1, pos_y, str);
	dc->TextOut(pos_x, pos_y - 1, str);
	dc->TextOut(pos_x, pos_y + 1, str);
	dc->SetTextColor(RGB(255, 255, 255));
	dc->TextOut(pos_x, pos_y, str);
}
// ============================================================================


POINT	CDrawObj::ConvertPixelPoint(POINTF pt_f)
{
	POINT pt;
	if (ROTATE_90 == m_RotateStatus){
		pt.y = lround(pt_f.x * m_ImageSize.cy) - m_CanvasRect.top;
		pt.x = -(lround(pt_f.y * m_ImageSize.cx) + m_CanvasRect.left);
	}
	else if (ROTATE_180 == m_RotateStatus){
		pt.x = -(lround(pt_f.x * m_ImageSize.cx) + m_CanvasRect.left);
		pt.y = -(lround(pt_f.y * m_ImageSize.cy) + m_CanvasRect.top);
	}
	else if (ROTATE_270 == m_RotateStatus){
		pt.y = -(lround(pt_f.x * m_ImageSize.cy) + m_CanvasRect.top);
		pt.x = lround(pt_f.y * m_ImageSize.cx) - m_CanvasRect.left;
	}
	else{
		pt.x = lround(pt_f.x * m_ImageSize.cx) - m_CanvasRect.left;
		pt.y = lround(pt_f.y * m_ImageSize.cy) - m_CanvasRect.top;
	}
	return pt;
}


POINTF	CDrawObj::ConvertRelativePoint(POINT pt)
{
	POINTF pt_f;
	if (ROTATE_90 == m_RotateStatus){
		pt_f.x = (float)(pt.y + m_CanvasRect.top) / m_ImageSize.cy;
		pt_f.y = -(float)(pt.x + m_CanvasRect.left) / m_ImageSize.cx;
	}
	else if (ROTATE_180 == m_RotateStatus){
		pt_f.x = -(float)(pt.x + m_CanvasRect.left) / m_ImageSize.cx;
		pt_f.y = -(float)(pt.y + m_CanvasRect.top) / m_ImageSize.cy;
	}
	else if (ROTATE_270 == m_RotateStatus){
		pt_f.x = -(float)(pt.y + m_CanvasRect.top) / m_ImageSize.cy;
		pt_f.y = (float)(pt.x + m_CanvasRect.left) / m_ImageSize.cx;
	}
	else{
		pt_f.x = (float)(pt.x + m_CanvasRect.left) / m_ImageSize.cx;
		pt_f.y = (float)(pt.y + m_CanvasRect.top) / m_ImageSize.cy;
	}
	return pt_f;
}


CString	CDrawObj::getCurLineLengthString(int pixel_length)
{
	CString str, str_out;
	if (m_AppliedScale == 1.0f){
		int length = lround((float)pixel_length / m_RenderingScale);
		
		if (m_AutoUnit){
			if (length > 1000000)
			{
				str.Format(_T("%.1f"), (float)length / 1000000);
				m_unit = _T("MPixel");
			}
			else
				str.Format(L"%d", length);
		}
		else{
			str.Format(L"%d", length);
			m_unit = _T("pixel");
		}
	}
	else{
		float length = (float)pixel_length / m_RenderingScale * m_AppliedScale;

		if (m_AutoUnit)
		{
			if (length > 10000)
			{
				str.Format(_T("%.2f"), length / 10000.0f);
				m_unit = _T("cm");
			}
			else if (length > 1000)
			{
				str.Format(_T("%.2f"), length / 1000.0f);
				m_unit = _T("mm");
			}
			else if (length >= 1){
				str.Format(L"%.1f", (float)pixel_length / m_RenderingScale * m_AppliedScale);
				m_unit = _T("μm");
			}
			else{
				str.Format(L"%.1f", length * 1000.0f);
				m_unit = _T("nm");
			}
		}
		else{
			str.Format(L"%.1f", (float)pixel_length / m_RenderingScale * m_AppliedScale);

			m_unit = _T("μm");
		}
	}
	str_out.Format(L"%s %s", str, m_unit);

	return str_out;
}


CString	CDrawObj::getDrawedLineLengthString(double line_length)
{
	CString str;
	if (m_AppliedScale == 1.0f){
		
		if (m_AutoUnit)
		{

			if (line_length > 1000000)
			{
				str.Format(_T("%.3f"), lround(line_length / 1000000));
				m_unit = _T("MPixel");
			}
			else{
				str.Format(L"%d", lround(line_length));
				m_unit = _T("pixel");
			}
		}
		else{
			str.Format(L"%d", lround(line_length));
			m_unit = _T("pixel");
		}
	}
	else{
		double length = line_length * m_AppliedScale;
		
		if (m_AutoUnit)
		{

			if (length > 10000)
			{
				str.Format(_T("%.2f"), length / 10000.0);
				m_unit = _T("cm");
			}
			else if (length > 1000)
			{
				str.Format(_T("%.2f"), length / 1000.0);
				m_unit = _T("mm");
			}
			else if(length >= 1){
				str.Format(L"%.1f", length);
				m_unit = _T("μm");
			}
			else{
				str.Format(L"%.1f", length*1000.0);
				m_unit = _T("nm");
			}
		}
		else{
			str.Format(L"%.1f", length);
			m_unit = _T("μm");
		}
	}
	return str;
}

CString	CDrawObj::getDrawedAreaString(double length)
{
	CString str;

	double l_length = length;
	
	if (m_AppliedScale == 1.0f){

		if (m_AutoUnit)
		{
			if (l_length > 1000000000000){
				str.Format(L"%.1f", l_length / 1000000000000);
				m_unit = _T("MP²");
			}
			else if (l_length > 1000000){
				str.Format(L"%.1f", l_length / 1000000);
				m_unit = _T("KP²");
			}
			else
			{
				str.Format(L"%.1f", l_length);
				m_unit = _T("pixel²");
			}
		}
		else
		{
			str.Format(L"%.1f", l_length);
			m_unit = _T("pixel²");
		}
	}
	else{
		l_length = l_length / (m_AppliedScale * m_AppliedScale);
		
		if (m_AutoUnit)
		{
			if (l_length > 100000000){
				str.Format(L"%.1f", l_length / 100000000);
				m_unit = _T("cm²");
			}
			else if (l_length > 1000000){
				str.Format(L"%.1f", l_length / 1000000);
				m_unit = _T("mm²");
			}
			else if (l_length >= 1)
			{
				str.Format(L"%.1f", l_length);
				m_unit = _T("μm²");
			}
			else
			{
				str.Format(L"%.1f", l_length * 1000000);
				m_unit = _T("nm²");
			}
		}
		else
		{
			str.Format(L"%.1f", l_length);
			m_unit = _T("μm²");
		}
	}	
	return str;
}

void	CDrawObj::MakeLineHitRegion(int count, POINT* pPoints)
{
	POINT Start, End, delta;
	for (int i = 1; i < count; i++)
	{
		Start = pPoints[0]; End = pPoints[i];

		delta.x = End.x - Start.x;
		delta.y = End.y - Start.y;

		double angle = atan2((double)-delta.y, (double)delta.x);
		double dy = MAX_REGION_DIFF * cos(angle);
		double dx = MAX_REGION_DIFF * sin(angle);

		POINT	p[5] = { { (int)(Start.x + dx), (int)(Start.y + dy) },
		{ (int)(Start.x - dx), (int)(Start.y - dy) },
		{ (int)(End.x - dx), (int)(End.y - dy) },
		{ (int)(End.x + dx), (int)(End.y + dy) },
		{ (int)(Start.x + dx), (int)(Start.y + dy) } };

		if (m_hHitRegion){
			DeleteObject(m_hHitRegion);
			m_hHitRegion = NULL;
		}

		m_hHitRegion = CreatePolygonRgn(p, 5, ALTERNATE);
	}
}


void	CDrawObj::MakeRegionHitRegion(int count, POINT* pPoints)
{
	POINT	p[5] = { { (pPoints[0].x - MAX_POINT_DIFF), (pPoints[0].y - MAX_POINT_DIFF) },
	{ (pPoints[1].x + MAX_POINT_DIFF), (pPoints[1].y - MAX_POINT_DIFF) },
	{ (pPoints[2].x + MAX_POINT_DIFF), (pPoints[2].y + MAX_POINT_DIFF) },
	{ (pPoints[3].x - MAX_POINT_DIFF), (pPoints[3].y + MAX_POINT_DIFF) },
	{ (pPoints[0].x - MAX_POINT_DIFF), (pPoints[0].y - MAX_POINT_DIFF) } };

	if (m_hHitRegion){
		DeleteObject(m_hHitRegion);
		m_hHitRegion = NULL;
	}

	m_hHitRegion = CreatePolygonRgn(p, 5, ALTERNATE);
}


int		CDrawObj::FindSelectNode(POINT pt)
{
	POINT node;
	int distance, mindistance = 0x7FFFFFFF;
	int selectNodeIndex = -1;
	for (int i = 0; i < m_NodeCnt ; ++i){
		node = m_pNodePoints[i];
		if ((abs(node.x - pt.x)<(2 * MAX_POINT_DIFF)) && (abs(node.y - pt.y)<(2 * MAX_POINT_DIFF))){
			distance = (node.x - pt.x)*(node.x - pt.x) + (node.y - pt.y)*(node.y - pt.y);
			if (distance< mindistance){
				mindistance = distance;
				selectNodeIndex = i;
			}
		}
	}
	return selectNodeIndex;
}


CScaleBarObj::CScaleBarObj(POINT pt, ePENCOLOR color)
{
	m_info.color = color;
	m_info.bar_length	= 100;
	m_info.pt = ConvertRelativePointByCanvas(pt);

	m_second_pt.x = pt.x + m_info.bar_length;
	m_second_pt.y = pt.y;

	m_status = SELECT;
	m_nCurSelPt = -1;
	m_SelectPtf = m_info.pt;
	
	InitObj();
}


void	CScaleBarObj::InitObj()
{
	m_scale = 1;
	m_NodeCnt = 2;
	m_pNodePoints = new POINT[m_NodeCnt];
	m_bar_length = m_info.bar_length;
}


CString	CScaleBarObj::getCurScaleLengthString(int pixel_length)
{
	CString str, str_out;
	if (m_scale != 0)
	{
		if (m_AppliedScale != 1.0){
			TRACE("m_length %d\n", m_length);
			if (m_AutoUnit)
			{
				if (m_length > 10000)
				{
					str.Format(_T("%.2f"), m_length / 10000);
					m_unit = _T("cm");
				}
				else if (m_length > 1000)
				{
					str.Format(_T("%.2f"), m_length / 1000);
					m_unit = _T("mm");
				}
				else if( m_length >= 1){
					str.Format(L"%.1f", m_length);
					m_unit = _T("μm");
				}
				else{
					str.Format(L"%.1f", m_length*1000);
					m_unit = _T("nm");
				}
			}
			else{
				str.Format(L"%.1f", m_length);
				m_unit = _T("μm");
			}
		}
		else
			str.Format(L"%d", lround(m_length));
	}
	else
	{
		if (m_AutoUnit)
		{
			str.Format(L"%.1f", m_length * 1000);
			m_unit = _T("nm");
		}
		else
			str.Format(L"%.2f", m_length);
	}
	
	str_out.Format(L"%s %s", str, m_unit);

	return str_out;
}


void	CScaleBarObj::Draw(CDC *pDC, bool isSelected)
{
	if (m_status == NONE) return;

	CPen *oldPen = (CPen*)pDC->SelectObject(&m_Pen[m_info.color]);
	pDC->MoveTo(m_pNodePoints[0]);
	pDC->LineTo(m_pNodePoints[1]);
	pDC->MoveTo(m_pNodePoints[0].x, m_pNodePoints[0].y - 3);
	pDC->LineTo(m_pNodePoints[0].x, m_pNodePoints[0].y + 3);
	pDC->MoveTo(m_pNodePoints[1].x, m_pNodePoints[1].y - 3);
	pDC->LineTo(m_pNodePoints[1].x, m_pNodePoints[1].y + 3);
	
	DrawTranspentText(pDC, (m_pNodePoints[0].x + m_pNodePoints[1].x) / 2 - 10, m_pNodePoints[1].y + 10, getCurScaleLengthString(m_info.bar_length));

	if (isSelected && (m_status!=SELECT) )
	{
		DrawNodeCircle(pDC, m_pNodePoints[0], isSelected);
		DrawNodeCircle(pDC, m_pNodePoints[1], isSelected);
	}

	pDC->SelectObject(oldPen);
}


bool	CScaleBarObj::HitTest(POINT pt)
{
	if (m_status == NONE) return false;
	
	bool bSelect = false;

	m_nCurSelPt = FindSelectNode(pt);
	if (m_nCurSelPt == -1){
		if (PtInRegion(m_hHitRegion, pt.x, pt.y))	bSelect = true;
	}
	else bSelect = true;

	if (bSelect){
		m_SelectPtf = ConvertRelativePointByCanvas(pt);
		m_status = SELECT;
	}

	return bSelect;
}


void	CScaleBarObj::UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status)
{		
	float length;
	if (bChangeViewSize){
		length = (float)m_info.bar_length / m_RenderingScale * m_AppliedScale;
	}else{
		length = m_bar_length / m_RenderingScale * m_AppliedScale;
	}
	
	if (length >= 500.0f){
		m_scale = 500;
	}else if (length >= 200.0f){
		m_scale = 200;
	}else if (length >= 100.0f){
		m_scale = 100;
	}else if (length >= 100.0f){
		m_scale = 100;
	}else if (length >= 50.0f)	{
		m_scale = 50;
	}else if (length >= 20.0f)	{
		m_scale = 20;
	}else if (length >= 10.0f){
		m_scale = 10;
	}else if (length >= 1.0f){
		m_scale = 1;
	}else{
		m_scale = 0;
	}

	if (m_scale)
		m_length = ( (int)(length+0.5)/ m_scale)*m_scale;
	else
		m_length = length;
	
	if (bChangeViewSize ){	//m_info.bar_length 가 변경이 되면 안된다. 내부적으로 가지고 있음
		m_bar_length = m_length / m_AppliedScale*m_RenderingScale;
	}
	else if (mouse_status == MOUSE_STATUS_LBUTTON_UP){
		m_info.bar_length = lround(m_length / m_AppliedScale*m_RenderingScale);
		m_bar_length = m_length / m_AppliedScale*m_RenderingScale;
	}
	else{	//Mouse을 통해 이동할 경우만 bar_length가 변경 된다.
		m_info.bar_length = lround(m_length / m_AppliedScale*m_RenderingScale);
	}
	
	m_pNodePoints[0] = ConvertPixelPointByCanvas(m_info.pt);
	m_pNodePoints[1] = m_pNodePoints[0];
	m_pNodePoints[1].x = m_pNodePoints[0].x + lround(m_bar_length);


	if (m_info.pt.x < 0.02f)
	{
		m_info.pt.x = 0.02f;
		m_pNodePoints[0] = ConvertPixelPointByCanvas(m_info.pt);
		m_info.bar_length = m_pNodePoints[1].x - m_pNodePoints[0].x;
	}else if (m_info.pt.x > 0.98f)
	{
		m_info.pt.x = 0.98f;
		m_pNodePoints[0] = ConvertPixelPointByCanvas(m_info.pt);
		m_info.bar_length = m_pNodePoints[1].x - m_pNodePoints[0].x;
	}
	
	POINTF ptf = ConvertRelativePointByCanvas(m_pNodePoints[1]);
	if (ptf.x > 0.98f)
	{
		ptf.x = 0.98f;
		m_pNodePoints[1] = ConvertPixelPointByCanvas(ptf);
		m_pNodePoints[0].x = m_pNodePoints[1].x - m_info.bar_length;

		POINTF ptf1 = ConvertRelativePointByCanvas(m_pNodePoints[0]);
		if (ptf1.x < 0.02f)
		{
			ptf1.x = 0.02f;
			m_pNodePoints[0] = ConvertPixelPointByCanvas(ptf1);
			m_info.bar_length = m_pNodePoints[1].x - m_pNodePoints[0].x;
		}
	}
	
	if (m_info.bar_length < 1)
		m_info.bar_length = 1;
		
	
	if (bChangeViewSize || mouse_status == MOUSE_STATUS_LBUTTON_UP){
		m_second_pt = m_pNodePoints[1];
	}

	if (bUpdateHitRegion) MakeLineHitRegion(2, m_pNodePoints);
}


bool	CScaleBarObj::UpdateMousePos(POINT pt)
{
	if (SELECT != m_status) return false;
	
	POINTF ptf = ConvertRelativePointByCanvas(pt);
	if (m_nCurSelPt == -1){	//move measure object
		POINTF movePTf;
		movePTf.x = ptf.x - m_SelectPtf.x;
		movePTf.y = ptf.y - m_SelectPtf.y;
			
		m_info.pt.x += movePTf.x;
		m_info.pt.y += movePTf.y;
						
		m_SelectPtf = ptf;
	}
	else{	//change point
		if (m_nCurSelPt == 0){
			m_info.pt.x = ptf.x;
			m_bar_length = m_second_pt.x - pt.x;

			if (m_bar_length < 1)
			{
				POINT buf = m_second_pt;
				buf.x -= 1;
				m_info.pt.x = ConvertRelativePointByCanvas(buf).x;
			}
		}
		else if (m_nCurSelPt == 1){
			POINT pos = ConvertPixelPointByCanvas(m_info.pt);
			m_bar_length = pt.x - pos.x;
		}

		if (m_bar_length < 1){
			m_bar_length = 1;
		}
	}

	return true;
}

void	CScaleBarObj::EditDescription(CString description)
{
	;
}
CString CScaleBarObj::GetDescription()
{
	return "";
}

CROIObj::CROIObj(POINT pt, ePENCOLOR color, std::string description)
{
	m_info.layer = m_curLayer;
	m_info.scale = m_RenderingScale;
	m_info.color = color;
	m_info.description = description;
	m_info.pts[0] = m_info.pts[1] = m_info.pts[2] = m_info.pts[3] = ConvertRelativePoint(pt);

	m_status = SELECT;
	m_nCurSelPt = 2;
	m_SelectPtf = m_info.pts[0];

	InitObj();
}


void	CROIObj::InitObj()
{
	m_NodeCnt = 4;
	m_pNodePoints = new POINT[m_NodeCnt];
}


void	CROIObj::Draw(CDC *pDC, bool isSelected)
{
	if (m_status == NONE || m_curLayer != m_info.layer) return;
	
	CPen *pOldPen = (CPen *)pDC->SelectObject(&m_Pen[m_info.color]);
	CBrush *pOldBrush = (CBrush *)pDC->SelectObject(&m_HatchBrush);

	pDC->Ellipse(m_pNodePoints[0].x, m_pNodePoints[0].y, m_pNodePoints[2].x, m_pNodePoints[2].y);
	for (unsigned int i = 0; i < 4; i++)DrawNodeCircle(pDC, m_pNodePoints[i], (isSelected && (m_status != SELECT)));


	//pDC->Rectangle(m_pNodePoints[0].x, m_pNodePoints[0].y, m_pNodePoints[2].x, m_pNodePoints[2].y);

	if (pOldPen)pDC->SelectObject(pOldPen);
	if (pOldBrush)pDC->SelectObject(pOldBrush);
}


bool	CROIObj::HitTest(POINT pt)
{
	if (m_status == NONE || m_curLayer != m_info.layer) return false;

	bool bSelect = false;

	m_nCurSelPt = FindSelectNode(pt);
	if (m_nCurSelPt == -1){
		if (PtInRegion(m_hHitRegion, pt.x, pt.y))	bSelect = true;
	}
	else bSelect = true;

	if (bSelect){
		m_SelectPtf = ConvertRelativePoint(pt);
		m_status = SELECT;
	}

	return bSelect;
}


void	CROIObj::UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status)
{
	//Convert by Rotate Info
	for (int i = 0; i < 4; i++){
		m_pNodePoints[i] = ConvertPixelPoint(m_info.pts[i]);
	}

	if (bUpdateHitRegion) MakeRegionHitRegion(4, m_pNodePoints);
}


bool	CROIObj::UpdateMousePos(POINT pt)
{
	if (SELECT != m_status) return false;

	POINTF ptf = ConvertRelativePoint(pt);
	if (m_nCurSelPt == -1){	//move measure object
		POINTF movePTf;
		movePTf.x = ptf.x - m_SelectPtf.x;
		movePTf.y = ptf.y - m_SelectPtf.y;

		for (int i = 0; i < 4; i++){
			m_info.pts[i].x += movePTf.x;
			m_info.pts[i].y += movePTf.y;
		}
		m_SelectPtf = ptf;
	}
	else{	//change point
		if (m_info.pts[m_nCurSelPt].x == ptf.x && m_info.pts[m_nCurSelPt].y == ptf.y) return false;
		m_info.pts[m_nCurSelPt] = ptf;

		if (m_nCurSelPt == 0){
			m_info.pts[3].x = ptf.x;
			m_info.pts[1].y = ptf.y;
		}
		else if (m_nCurSelPt == 1){
			m_info.pts[2].x = ptf.x;
			m_info.pts[0].y = ptf.y;
		}
		else if (m_nCurSelPt == 2){
			m_info.pts[1].x = ptf.x;
			m_info.pts[3].y = ptf.y;
		}
		else if (m_nCurSelPt == 3){
			m_info.pts[0].x = ptf.x;
			m_info.pts[2].y = ptf.y;
		}
	}
	return true;
}

void	CROIObj::EditDescription(CString description)
{
	m_info.description = CT2CA(description.operator LPCWSTR());
}
CString CROIObj::GetDescription()
{
	return m_info.description.c_str();
}

CMeasureObj::CMeasureObj(eOBJTYPE type, POINT pt, ePENCOLOR color, std::string description)
{
	m_info.layer		= m_curLayer;
	m_info.scale		= m_RenderingScale;
	m_info.color		= color;
	m_info.description	= description;
	m_info.type = type;

	POINTF ptf = ConvertRelativePoint(pt);
	m_info.pts.push_back(ptf);
	m_info.pts.push_back(ptf);
	
	m_status = SELECT;
	m_nCurSelPt = 1;
	m_SelectPtf = ptf;

	InitObj();
}


void	CMeasureObj::InitObj()
{
	m_length = 0;
	m_NodeCnt = 2;
	m_pNodePoints = new POINT[m_NodeCnt];
}


void	CMeasureObj::Draw(CDC *pDC,bool isSelected)
{
	if (m_status == NONE || m_curLayer != m_info.layer) return;

	CPen *pOldPen = NULL;
	pOldPen = (CPen *)pDC->SelectObject(&m_Pen[m_info.color]);
	CBrush *pOldBrush = (CBrush *)pDC->SelectObject(&m_HatchBrush);
	POINT first_pos, second_pos, pre_pos, pos;

	//Draw Lines
	first_pos = m_pNodePoints[0];
	pDC->MoveTo(first_pos);
	DrawNodeCircle(pDC, first_pos, (isSelected && (m_status != SELECT)));
		
	float pixel_length = 0.f;
	pre_pos = first_pos;

	// RGB MEASUREMENT
	// ============================================================================
	int show_rgb = AfxGetApp()->GetProfileIntW(L"Settings", L"SWRGB", MF_CHECKED);

	//for (unsigned int i = 1; i < m_info.pts.size(); i++){
	for (unsigned int i = 0; i < m_info.pts.size(); i++){
		if (m_info.type == MEASURE_AREA && m_bMeasureComplete == false && m_strRGBText.CompareNoCase(L"") == 0)
		{
			BOOL is_pixel = FALSE;
			POINT pixel_pt = ConvertPixelPoint(m_info.pts[i]);

			for (int j = 0; j < m_arrMeasureRect.GetSize(); j++)
			{
				CRect measure_rect = m_arrMeasureRect.GetAt(j);

				if (pixel_pt.y == measure_rect.top)
				{
					if (pixel_pt.x < measure_rect.left)
						measure_rect.left = pixel_pt.x;
					if (pixel_pt.x > measure_rect.right)
						measure_rect.right = pixel_pt.x;

					//TRACE(L"i : %d, j : %d, top : %d, left : %d, right : %d\n", i, j, measure_rect.top, measure_rect.left, measure_rect.right);

					m_arrMeasureRect[j] = measure_rect;

					is_pixel = TRUE;
				}
			}

			if (is_pixel == FALSE)
			{
				CRect measure_rect;
				measure_rect.left = measure_rect.right = pixel_pt.x;
				measure_rect.top = measure_rect.bottom = pixel_pt.y;
				m_arrMeasureRect.Add(measure_rect);
			}
		}

		if (i < 1) continue;
		// ============================================================================

		pos = m_pNodePoints[i];
		pDC->LineTo(m_pNodePoints[i]);
		if (i != 0) second_pos = pos;

		pixel_length += (float)sqrt((pos.x - pre_pos.x) * (pos.x - pre_pos.x) + (pos.y - pre_pos.y) * (pos.y - pre_pos.y));
		if (i == (m_info.pts.size()-1) && (m_info.type != MEASURE_AREA))
			DrawNodeCircle(pDC, pos, (isSelected && (m_status != SELECT)));
				
		pre_pos = pos;


	}
	if (m_info.type == MEASURE_AREA)
	{
		pDC->LineTo(m_pNodePoints[0]);

		// RGB MEASUREMENT
		// ============================================================================
		if (m_bMeasureComplete == true && m_strRGBText.CompareNoCase(L"") == 0)
		{

			int r = 0, g = 0, b = 0;

			for (int i = 0; i < m_arrMeasureRect.GetSize(); i++)
			{
				CRect measure_rect = m_arrMeasureRect.GetAt(i);

				for (int j = measure_rect.left; j <= measure_rect.right; j++)
				{
					if (measure_rect.top < 0 || j < 0)
						continue;

					if (measure_rect.top > m_pFastPixelBackBuf->GetHeight())
						continue;

					COLORREF pt_color = m_pFastPixelBackBuf->GetPixel(j, measure_rect.top);
					r = GetRValue(pt_color);
					g = GetGValue(pt_color);
					b = GetBValue(pt_color);

					/*if (r == 0 && g == 255 && b == 0)
					{
					//TRACE(L"area line ==> x : %d, y : %d\n", j, measure_rect.top);
					continue;
					}*/

					m_arrRgbRed[r]++;
					m_arrRgbGreen[g]++;
					m_arrRgbBlue[b]++;
					m_nColorR += r;
					m_nColorG += g;
					m_nColorB += b;

					//TRACE(L"i : %d, x : %d, y : %d, R : %d, G : %d, B : %d\n", i, j, measure_rect.top, r, g, b);

					m_nPixelCnt++;
				}
			}

			if (m_nPixelCnt > 0){
				m_strRGBText.Format(L"R : %d, G : %d, B : %d", m_nColorR / m_nPixelCnt, m_nColorG / m_nPixelCnt, m_nColorB / m_nPixelCnt);
				m_info.red = m_nColorR / m_nPixelCnt;
				m_info.green = m_nColorG / m_nPixelCnt;
				m_info.blue = m_nColorB / m_nPixelCnt;
			}
		}
		// ============================================================================
	}

	//Draw Scale information consider 
	pos.x = first_pos.x + TEXT_HEIGHT;
	pos.y = (second_pos.y > first_pos.y) ? first_pos.y - (TEXT_HEIGHT + TEXT_HEIGHT / 2) : first_pos.y + TEXT_HEIGHT / 2;
	if (m_info.type != MEASURE_AREA)
		DrawTranspentText(pDC, pos.x, pos.y, getDrawedLineLengthString(m_length) + m_unit);
	else
	{
		//DrawTranspentText(pDC, pos.x, pos.y, getDrawedAreaString(m_length) + m_unit);

		// RGB MEASUREMENT
		// ============================================================================
		if (show_rgb != MF_CHECKED)
			DrawTranspentText(pDC, pos.x, pos.y, getDrawedAreaString(m_length) + m_unit);
		else
			DrawTranspentText(pDC, pos.x, pos.y, m_bMeasureComplete == false ? L"Measuring RGB..." : m_strRGBText, getDrawedAreaString(m_length) + m_unit);
		//DrawTranspentText(pDC, pos.x, pos.y, isDrawing() == true ? L"Measuring RGB..." : (m_bMeasureComplete == false ? L"Measuring RGB..." : m_strRGBText), getDrawedAreaString(m_length) + m_unit);
		// ============================================================================
	}

	if (pOldPen)pDC->SelectObject(pOldPen);
	if (pOldBrush)pDC->SelectObject(pOldBrush);
}


bool	CMeasureObj::HitTest(POINT pt)
{
	if (m_status == NONE || m_curLayer != m_info.layer) return false;

	bool bSelect = false;

	m_nCurSelPt = FindSelectNode(pt);
	if (m_nCurSelPt == -1){
		if (PtInRegion(m_hHitRegion, pt.x, pt.y))	bSelect = true;
	}
	else bSelect = true;
	
	if (bSelect){
		m_SelectPtf = ConvertRelativePoint(pt);
		m_status = SELECT;
	}
	return bSelect;
}

bool optimize(std::vector<POINTF, std::allocator<POINTF>> *points)
{
	int i = 0;
	POINTF pt1, pt2, pt3;
	double dx1, dy1, dx2, dy2;
	while (i < points->size() - 2)
	{
		pt1 = points->at(i);
		pt2 = points->at(i + 1);
		pt3 = points->at(i + 2);

		dx1 = pt2.x - pt1.x;
		dy1 = pt2.y - pt1.y;
		dx2 = pt3.x - pt2.x;
		dy2 = pt3.y - pt2.y;

		if (dx1 == 0.0 && dy1 == 0.0)
		{
			points->erase(points->begin() + i + 1);
			return false;
		}

		if ((dx1 == 0.0 && dx2 == 0.0) || (dy1 == 0.0 && dy2 == 0.0))
		{
			points->erase(points->begin() + i + 1);
			return false;
			
		}

		if ( abs((dy1 / dx1) - (dy2 / dx2)) < 0.001)
		{
			points->erase(points->begin() + i + 1);
			return false;
		}
		i++;
	}
	return true;
}


void	CMeasureObj::UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status)
{

	//assert(m_info.pts.size() == m_NodeCnt);
	if (mouse_status == MOUSE_STATUS_LBUTTON_UP)
	{
		//optimization #1
		while (!optimize(&m_info.pts))
		{
			;
		}

		// RGB MEASUREMENT
		// ============================================================================
		m_bMeasureComplete = true;
	}
	else if (mouse_status == MOUSE_STATUS_MOVE)
	{
		m_bMeasureComplete = false;
		m_strRGBText = L"";
		m_arrMeasureRect.RemoveAll();
	}
	// ============================================================================

	m_NodeCnt = m_info.pts.size();
	delete[] m_pNodePoints;
	m_pNodePoints = new POINT[m_NodeCnt];
	for (int i = 0; i < m_NodeCnt; i++) m_pNodePoints[i] = ConvertPixelPoint(m_info.pts[i]);
	
	if (bUpdateHitRegion) MakeLineHitRegion(m_NodeCnt, m_pNodePoints);

	//Calculate m_length
	double pixel_length = 0.f;
	POINT first_pos, pre_pos, pos;
	first_pos = m_pNodePoints[0];
	if (m_info.type != MEASURE_AREA)
		pre_pos = first_pos;
	else
		pre_pos = m_pNodePoints[m_NodeCnt-1];

	for (unsigned int i = 1; i < m_info.pts.size(); i++){
		pos = m_pNodePoints[i];
		if (m_info.type != MEASURE_AREA)
			pixel_length += (double)sqrt((pos.x - pre_pos.x) * (pos.x - pre_pos.x) + (pos.y - pre_pos.y) * (pos.y - pre_pos.y));
		else
			pixel_length += (pre_pos.x + pos.x)*(pre_pos.y - pos.y);
		pre_pos = pos;
	}
	if (mouse_status == MOUSE_STATUS_LBUTTON_UP){
		if (m_info.type != MEASURE_AREA){
			m_info.length = pixel_length / m_RenderingScale;
		}
		else{
			if (m_info.length == 0){	//area length 값이 초기 업데이트 되지 않을 경우
				pixel_length = pixel_length / 2.0;
				pixel_length = abs(pixel_length);
				m_info.length = pixel_length / (m_RenderingScale*m_RenderingScale);
			}
		}
		m_length = m_info.length;
		TRACE("1. m_info.length %lf, m_length %lf\n", m_info.length, m_length);
	}

	if (m_drawing || m_length == 0){		
		if (mouse_status == MOUSE_STATUS_LBUTTONW_DOWN || mouse_status == MOUSE_STATUS_MOVE)
		{
			if (m_info.type != MEASURE_AREA)
				m_length = pixel_length / m_RenderingScale;
			else{
				if (m_info.length == 0){
					pixel_length = pixel_length / 2.0;
					pixel_length = abs(pixel_length);
					m_length = pixel_length / (m_RenderingScale*m_RenderingScale);
				}
			}
		}
		else
			m_length = m_info.length;
		TRACE("2. m_info.length %lf, m_length %lf\n", m_info.length, m_length);
	}
}


bool	CMeasureObj::UpdateMousePos(POINT pt)
{
	if (SELECT != m_status) return false;

	POINTF ptf = ConvertRelativePoint(pt);
	if (m_nCurSelPt == -1){	//move measure object
		POINTF movePTf;
		movePTf.x = ptf.x - m_SelectPtf.x;
		movePTf.y = ptf.y - m_SelectPtf.y;
		for (int i = 0; i < m_info.pts.size(); i++){
			m_info.pts[i].x += movePTf.x;
			m_info.pts[i].y += movePTf.y;
		}
		m_SelectPtf = ptf;
	}
	else{	//change point
		if (m_nCurSelPt >= m_info.pts.size()) return false;
		if (m_info.pts[m_nCurSelPt].x == ptf.x && m_info.pts[m_nCurSelPt].y == ptf.y) return false;
		if ((m_info.type == MEASURE_CURVE || m_info.type == MEASURE_AREA) && !m_drawn)
		{
			m_info.pts.push_back(ptf);
			m_NodeCnt++;
		}
		else if (m_info.type == MEASURE_CURVE || m_info.type == MEASURE_AREA)
		{
			POINTF movePTf;
			movePTf.x = ptf.x - m_SelectPtf.x;
			movePTf.y = ptf.y - m_SelectPtf.y;
			for (int i = 0; i < m_info.pts.size(); i++){
				m_info.pts[i].x += movePTf.x;
				m_info.pts[i].y += movePTf.y;
			}
			m_SelectPtf = ptf;
		}
		else
		{
			m_info.pts[m_nCurSelPt] = ptf;
		}
	}
	return true;
}

void	CMeasureObj::EditDescription(CString description)
{	
	m_info.description = CT2CA(description.operator LPCWSTR());
}
CString CMeasureObj::GetDescription()
{
	return m_info.description.c_str();
}


CString		CMeasureObj::getLengthXMLOutputStringWithUnit()
{
	CString converted_unit;
	CString length_str;
	if (m_info.type != MEASURE_AREA)
		length_str =  getDrawedLineLengthString(m_length);
	else 
		length_str = getDrawedAreaString(m_length);
	
	for (int i = 0; i < m_unit.GetLength(); i++){
		if (m_unit[i] == L'μ')
			converted_unit += _T("&#x00B5;");
		else if (m_unit[i] == L'²')
			converted_unit += _T("&#x00B2;");
		else
			converted_unit += m_unit[i];
	}
	
	return length_str + converted_unit;
}


CLabelObj::CLabelObj(POINT pt, COLORREF fontColor, COLORREF bgColor, bool transparent, float fontSize, std::string description)
{
	m_info.layer = m_curLayer;
	m_info.fontColor = fontColor;
	m_info.bgColor = bgColor;
	m_info.transparent = transparent;
	m_info.description = description;
	m_info.size = fontSize;

	POINT pt2;
	pt2.x = pt.x + m_info.size / 10;
	pt2.y = pt.y + m_info.size / 10;
	POINTF ptf = ConvertRelativePoint(pt);
	POINTF ptf2 = ConvertRelativePoint(pt2);


	m_info.pts[0] = m_info.pts[1] = m_info.pts[2] = m_info.pts[3] = ptf;
	m_info.pts[2] = ptf2;

	m_info.pts[1].x = ptf2.x;	
	m_info.pts[3].y = ptf2.y;

	m_status = SELECT;
	m_nCurSelPt = 1;
	m_SelectPtf = ptf;

	InitObj();
}


void	CLabelObj::InitObj()
{
	m_NodeCnt = 4;
	m_pNodePoints = new POINT[m_NodeCnt];
}


void	CLabelObj::DrawLabelText(CDC *dc, int pos_x, int pos_y,COLORREF fColor, COLORREF bColor, bool isTransparent, const wchar_t *str)
{
	dc->SelectObject(m_font);
	if (isTransparent)
		dc->SetBkMode(TRANSPARENT);
	else
		dc->SetBkMode(OPAQUE);
	dc->SetTextColor(fColor);
	dc->SetBkColor(bColor);
	dc->TextOut(pos_x, pos_y, str);
}

void	CLabelObj::Draw(CDC *pDC, bool isSelected)
{
	if (m_status == NONE || m_curLayer != m_info.layer) return;

	CFont font;
	font.CreatePointFont(m_info.size, m_info.font);

	CFont* oldFont = pDC->SelectObject(&font);
	COLORREF oldColor = pDC->SetTextColor(m_info.fontColor);
	int oldBK = pDC->SetBkMode(m_info.transparent);
	COLORREF oldBkColor = pDC->SetBkColor(m_info.bgColor);

	//for (unsigned int i = 0; i < 4; i++)DrawNodeCircle(pDC, m_pNodePoints[i], true);

	USES_CONVERSION;
	pDC->TextOut(m_pNodePoints[0].x, m_pNodePoints[0].y, A2T(m_info.description.c_str()));

	pDC->SelectObject(oldFont);
	pDC->SetTextColor(oldColor);
	pDC->SetBkMode(oldBK);
	pDC->SetBkColor(oldBkColor);

}


bool	CLabelObj::HitTest(POINT pt)
{
	if (m_status == NONE || m_curLayer != m_info.layer) return false;

	bool bSelect = false;

	m_nCurSelPt = FindSelectNode(pt);
	if (m_nCurSelPt == -1){
		if (PtInRegion(m_hHitRegion, pt.x, pt.y))	bSelect = true;
	}
	else bSelect = true;

	if (bSelect){
		m_SelectPtf = ConvertRelativePoint(pt);
		m_status = SELECT;
	}
	return bSelect;
}

void	CLabelObj::UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status)
{

	//Convert by Rotate Info
	for (int i = 0; i < 4; i++){
		m_pNodePoints[i] = ConvertPixelPoint(m_info.pts[i]);
	}

	m_pNodePoints[0] = m_pNodePoints[1] = m_pNodePoints[3] = ConvertPixelPoint(m_info.pts[0]);
	
	m_pNodePoints[2].x = m_pNodePoints[0].x + m_info.size / 2;
	m_pNodePoints[2].y = m_pNodePoints[0].y + m_info.size / 8;

	m_pNodePoints[1].x = m_pNodePoints[2].x;
	m_pNodePoints[3].y = m_pNodePoints[2].y;

	if (bUpdateHitRegion) MakeRegionHitRegion(4, m_pNodePoints);
}


bool	CLabelObj::UpdateMousePos(POINT pt)
{
	if (SELECT != m_status) return false;

	POINTF ptf = ConvertRelativePoint(pt);
	if (m_nCurSelPt == -1){	//move measure object
		POINTF movePTf;
		movePTf.x = ptf.x - m_SelectPtf.x;
		movePTf.y = ptf.y - m_SelectPtf.y;

		for (int i = 0; i < 4; i++){
			m_info.pts[i].x += movePTf.x;
			m_info.pts[i].y += movePTf.y;
		}
		m_SelectPtf = ptf;
	}
	return true;
}

void	CLabelObj::EditDescription(CString description)
{
	m_info.description = CT2CA(description.operator LPCWSTR());
}

CString CLabelObj::GetDescription()
{
	return m_info.description.c_str();
}


void	CRegionRectObj::Draw(CDC *pDC, bool isSelected)
{
	AtlTrace("CRegionRectObj::Draw Status [%d], isSelected [%d] , [%d:%d, %d:%d]\n",
		m_status, isSelected, m_pNodePoints[0].x, m_pNodePoints[0].y, m_pNodePoints[2].x, m_pNodePoints[2].y);
	if (m_status == NONE) return;

	CPen *pOldPen = (CPen *)pDC->SelectObject(&m_Pen[m_info.color]);

	pDC->MoveTo(m_pNodePoints[0]);
	pDC->LineTo(m_pNodePoints[1]);

	pDC->MoveTo(m_pNodePoints[1]);
	pDC->LineTo(m_pNodePoints[2]);

	pDC->MoveTo(m_pNodePoints[2]);
	pDC->LineTo(m_pNodePoints[3]);

	pDC->MoveTo(m_pNodePoints[3]);
	pDC->LineTo(m_pNodePoints[0]);

	if (pOldPen)pDC->SelectObject(pOldPen);
}


void	CRegionEllipseObj::Draw(CDC *pDC, bool isSelected)
{
	AtlTrace("CRegionEllipseObj::Draw Status [%d], isSelected [%d] , [%d:%d, %d:%d]\n",
		m_status, isSelected, m_pNodePoints[0].x, m_pNodePoints[0].y, m_pNodePoints[2].x, m_pNodePoints[2].y);
	if (m_status == NONE) return;
	
	CPen *pOldPen = (CPen *)pDC->SelectObject(&m_Pen[m_info.color]);
	CBrush *pOldBrush = (CBrush *)pDC->SelectObject(&m_HatchBrush);

	pDC->Ellipse(m_pNodePoints[0].x, m_pNodePoints[0].y, m_pNodePoints[2].x, m_pNodePoints[2].y);
	for (unsigned int i = 0; i < 4; i++)DrawNodeCircle(pDC, m_pNodePoints[i], (isSelected && (m_status != SELECT)));
	
	if (pOldPen)pDC->SelectObject(pOldPen);
	if (pOldBrush)pDC->SelectObject(pOldBrush);
}


CGridObj::CGridObj(POINT pt, ePENCOLOR color)
{
	m_status = NONE;
	m_nCurSelPt = -1;
	m_SelectPtf = ConvertRelativePointByCanvas(pt);

	InitObj();
}


CGridObj::~CGridObj()
{
	CDrawObj::~CDrawObj();
	for (int i = 0; i < m_AnalysisRegions.size(); i++) {
		delete m_AnalysisRegions[i];
	}
}


void	CGridObj::InitObj()
{
	m_AnalysisMode = GRID_ANALYSIS_NONE;
	m_scale = 1;
	m_NodeCnt = 3;
	//m_pNodePoints = POINT p[m_NodeCnt];
	m_grid_interval = m_info.interval;
	m_info.ratio = m_AppliedScale;
	m_position.x = 0;
	m_position.y = 0;
	m_size.x = 0;
	m_size.y = 0;
	m_GridRatioMode = GRID_RATIO_1X1;
	m_sector_interval = m_sector_interval = 0;
}


CString	CGridObj::getCurGridIntervalString()
{
	CString str, str_out;
	if (m_scale != 0){
		str.Format(L"%d", lround(m_grid_interval));
	}else{
		str.Format(L"%.2f", m_grid_interval);
	}
	str_out.Format(L"%s %s", str, m_unit);

	return str_out;
}


void	CGridObj::UpdateCellStatus()
{
	if (GRID_ANALYSIS_REGION_DRAW == m_AnalysisMode) {
		UpdateSectorCellStatus();
	}
	else {
		UpdateGridCellStatus();
	}
}


void	CGridObj::UpdateSectorCellStatus()
{
	//if ((GRID_ANALYSIS_REGION_DRAW == m_AnalysisMode || GRID_ANALYSIS_REGION_ANALYSIS == m_AnalysisMode)) return;
		
	m_SectorCellInfo.clear();

	//m_info.ratio = m_AppliedScale;
	float interval_x, interval_y;

	interval_x = (float)m_sector_interval * m_RenderingScale / m_AppliedScale;
	if (m_GridRatioMode == GRID_RATIO_4X3)
		interval_y = (interval_x * 3) / 4; 
	else if (m_GridRatioMode == GRID_RATIO_16X9)
		interval_y = (interval_x * 9) / 16;
	else
		interval_y = interval_x;

	if (m_RotateStatus == ROTATE_NONE || m_RotateStatus == ROTATE_180) {
		m_step.x = interval_x / m_ImageSize.cx;
		m_step.y = interval_y / m_ImageSize.cy;
	}
	else {
		m_step.x = interval_y / m_ImageSize.cy;
		m_step.y = interval_x / m_ImageSize.cx;
	}
	
	if (m_RegionSelectAll) {
		for (float pos_y = 0; pos_y < 1; pos_y += m_step.y) {
			for (float pos_x = 0; pos_x < 1; pos_x += m_step.x) {
				GridCellInfo info = { CELL_STATUS_SELECT, {pos_x , pos_y }, CELL_NORMAL_POS };
				if(pos_x == 0) info.pos_type |= CELL_LEFT_POS;
				if(pos_y += m_step.y >= 1) info.pos_type |= CELL_BOTTOM_POS;
				m_SectorCellInfo.push_back(info);
			}
		}
	}
	else {
		for (int i = 0; i < m_AnalysisRegions.size(); i++) {
			if (m_AnalysisRegions[i]->GetObjType() == REGSION_RECT) {
				CRegionRectObj* pObj = (CRegionRectObj*)m_AnalysisRegions[i];
				POINTF* pts = pObj->getInfo()->pts;
				for (float pos_y = pts[0].y; pos_y < pts[2].y - m_step.y; pos_y += m_step.y) {
					for (float pos_x = pts[0].x; pos_x < pts[1].x - m_step.x; pos_x += m_step.x) {
						GridCellInfo info = { CELL_STATUS_SELECT,{ pos_x , pos_y }, CELL_NORMAL_POS };
						if (pos_x == pts[0].x) info.pos_type |= CELL_LEFT_POS;
						if (pos_y += m_step.y >= pts[2].y) info.pos_type |= CELL_BOTTOM_POS;
						m_SectorCellInfo.push_back(info);
					}
				}
			}
			else if (m_AnalysisRegions[i]->GetObjType() == REGSION_ELLIPSE) {
				CRegionEllipseObj* pObj = (CRegionEllipseObj*)m_AnalysisRegions[i];
				POINTF* pts = pObj->getInfo()->pts;

				bool new_line = false;
				bool bottom_line = false;
				for (float pos_y = pts[0].y; pos_y < pts[2].y - m_step.y; pos_y += m_step.y) {
					new_line = true;
					if (pos_y += m_step.y >= pts[2].y) bottom_line = true;
					for (float pos_x = pts[0].x; pos_x < pts[1].x - m_step.x; pos_x += m_step.x) {
						float r_x = (pts[1].x - pts[0].x) / 2.0f;
						float r_y = (pts[2].y - pts[0].y) / 2.0f;

						POINTF pos[4] = { { pos_x , pos_y }, { pos_x + m_step.x, pos_y },{ pos_x , pos_y + m_step.y },{ pos_x + m_step.x, pos_y + m_step.y } };
						bool bInsertPt = true;
						for (int c = 0; c < 4; c++) {
							if (!((pow((pos[c].x - pts[0].x - r_x), 2) / pow(r_x, 2)) + (pow((pos[c].y - pts[0].y - r_y), 2) / pow(r_y, 2)) < 1)) {
								bInsertPt = false;
								break;
							}
						}
						
						if(bInsertPt){
							GridCellInfo info = { CELL_STATUS_SELECT,{ pos_x , pos_y }, CELL_NORMAL_POS };
							if (new_line) info.pos_type |= CELL_LEFT_POS;
							if (bottom_line) info.pos_type |= CELL_BOTTOM_POS;
							m_SectorCellInfo.push_back(info);
						}
					}
				}
			}
			else if (m_AnalysisRegions[i]->GetObjType() == REGSION_FREE) {
				/*
								CRegionFreeObj* pObj = (CRegionFreeObj*)m_AnalysisRegions[i];

								float minX, maxX, minY, maxY;
								minX = maxX = m_info.pts[0].x;
								minY = maxY = m_info.pts[0].y;
								for (int n = 1; n < m_info.pts.size(); n++) {
									minX = min(m_info.pts[n].x, minX);
									maxX = max(m_info.pts[n].x, maxX);
									minY = min(m_info.pts[n].y, minY);
									maxY = max(m_info.pts[n].y, maxY);
								}

								BOOL isInside = FALSE;
								int i = 0;
								int j = m_info.pts.size() - 1;
								for (float pos_y = minY; pos_y < maxY; pos_y += m_step.y) {
									for (float pos_x = minX; pos_x < maxX; pos_x += m_step.x) {

										for (; i < m_info.pts.size(); j = i++) {
											if ((m_info.pts[i].y > pos_y) != (m_info.pts[j].y > pos_y) &&
												pos_x < (m_info.pts[j].x - m_info.pts[i].x) * (pos_y - m_info.pts[i].y) / (m_info.pts[j].y - m_info.pts[i].y) + m_info.pts[i].x) {
												GridCellInfo info = { { pos_x , pos_y }, CELL_STATUS_NONE };
												m_SectorGridCellInfo.push_back(info);
											}
										}
									}
								}
							}
				*/
			}
		}
	}
}


void	CGridObj::UpdateGridCellStatus(BOOL clearCell)
{
	float interval = (float)m_grid_interval * m_RenderingScale / m_AppliedScale;
	POINTF ptf;

	if (m_RotateStatus == ROTATE_NONE || m_RotateStatus == ROTATE_180) {
		m_step.x = interval / m_ImageSize.cx;
		m_step.y = interval / m_ImageSize.cy;
	}
	else {
		m_step.x = interval / m_ImageSize.cy;
		m_step.y = interval / m_ImageSize.cx;
	}

	if (GRID_ANALYSIS_NONE == m_AnalysisMode) {
		m_GridCellInfo.clear();
		for (float pos_y = 0 ; pos_y < 1 ; pos_y += m_step.y) {
			for (float pos_x = 0 ; pos_x < 1 ; pos_x += m_step.x) {
				GridCellInfo info = { CELL_STATUS_NONE,{ pos_x , pos_y }, CELL_NORMAL_POS };
				if (pos_x == 0) info.pos_type |= CELL_LEFT_POS;
				if (pos_y += m_step.y >= 1) info.pos_type |= CELL_BOTTOM_POS;
				m_GridCellInfo.push_back(info);
			}
		}
	}else if(GRID_ANALYSIS_REGION_ANALYSIS == m_AnalysisMode){
		bool bNewCellInsert = false;
		if (m_GridCellInfo.size() == 0 || clearCell) {
			bNewCellInsert = true;
			m_GridCellInfo.clear();
		}

		float interval_x, interval_y;
		interval_x = (float)m_sector_interval * m_RenderingScale / m_AppliedScale;
		if (m_GridRatioMode == GRID_RATIO_4X3)
			interval_y = (interval_x * 3) / 4;
		else if (m_GridRatioMode == GRID_RATIO_16X9)
			interval_y = (interval_x * 9) / 16;
		
		if (m_RotateStatus == ROTATE_NONE || m_RotateStatus == ROTATE_180) {
			interval_x /=  m_ImageSize.cx;
			interval_y /=  m_ImageSize.cy;
		}
		else {
			interval_y /= m_ImageSize.cy;
			interval_x /= m_ImageSize.cx;
		}

		//marging @@ need to know grid count in sector
		m_step.y -= m_step.y / 80;
		m_step.x -= m_step.x / 80;

		int index = 0;
		for (float pos_y = m_SectorCellInfo[m_CurrentSelectIndex].pt.y; pos_y <= m_SectorCellInfo[m_CurrentSelectIndex].pt.y + interval_y - m_step.y; pos_y += m_step.y) {
			for (float pos_x = m_SectorCellInfo[m_CurrentSelectIndex].pt.x; pos_x <= m_SectorCellInfo[m_CurrentSelectIndex].pt.x + interval_x - m_step.x; pos_x += m_step.x) {
				GridCellInfo info = { CELL_STATUS_NONE,{ pos_x , pos_y }, CELL_NORMAL_POS };
				if (pos_x == 0) info.pos_type |= CELL_LEFT_POS;
				if (pos_y += m_step.y >= 1) info.pos_type |= CELL_BOTTOM_POS;
				if(bNewCellInsert) m_GridCellInfo.push_back(info);
				else {
					m_GridCellInfo[index].pos_type = info.pos_type;
					m_GridCellInfo[index].pt = info.pt;
				}
			}
		}
	}
}


inline void	DrawNodeRect(CDC *pDC, POINT pt, DWORD color)
{
	CPen selPen;
	CPen *pOldPen;
	CBrush selBrush;
	CBrush *pOldBrush;
	selPen.CreatePen(PS_SOLID, 1, color);

	LOGBRUSH logBrush;
	logBrush.lbStyle = BS_SOLID;
	logBrush.lbColor = color;//RGB(255, 255, 255); // ignored
	logBrush.lbHatch = HS_CROSS; // ignored
	selBrush.CreateBrushIndirect(&logBrush);

	pOldPen = (CPen *)pDC->SelectObject(&selPen);
	pOldBrush = (CBrush *)pDC->SelectObject(&selBrush);

	RECT CenterPos;

	CenterPos.left = pt.x - 2*MAX_POINT_DIFF;
	CenterPos.right = pt.x + 2 * MAX_POINT_DIFF;
	CenterPos.top = pt.y - 2 * MAX_POINT_DIFF;
	CenterPos.bottom = pt.y + 2 * MAX_POINT_DIFF;
	//pDC->Ellipse(&CenterPos);
	pDC->Rectangle(&CenterPos);

	if (pOldPen)pDC->SelectObject(pOldPen);
	if (pOldBrush)pDC->SelectObject(pOldBrush);
}



void	CGridObj::Draw(CDC *pDC, bool isSelected)
{
	AtlTrace("CGridObj::Draw Status [%d], isSelected [%d] AnalysisMode [%d] Region [%d]\n",
		m_status, isSelected, m_AnalysisMode, m_AnalysisRegions.size());
	if (m_status == NONE) return;

	CPen newPen;
	newPen.CreatePen(PS_SOLID, m_info.thick, m_info.color);
	CPen *oldPen = (CPen*)pDC->SelectObject(&newPen);
	
	//Draw
	if (GRID_ANALYSIS_REGION_DRAW == m_AnalysisMode)  {
		for (int i = 0; i < m_AnalysisRegions.size(); i++) {
			m_AnalysisRegions[i]->Draw(pDC, false);
		}
		float interval_x, interval_y;
		interval_x = (float)m_sector_interval * m_RenderingScale / m_AppliedScale;
		if (m_GridRatioMode == GRID_RATIO_16X9)
			interval_y = (interval_x * 9) / 16;
		else
			interval_y = (interval_x * 3) / 4;
				
		POINTF pf1, pf2;
		POINT p1, p2;
		
		for (int i = 0; i < m_SectorCellInfo.size(); i++) {
			if (m_SectorCellInfo[i].status == CELL_STATUS_SELECT) {
				pf1.x = m_SectorCellInfo[i].pt.x;
				pf1.y = m_SectorCellInfo[i].pt.y;

				p1 = ConvertPixelPoint(pf1);
				pDC->MoveTo(p1);
				if (CELL_LEFT_POS&m_SectorCellInfo[i].pos_type) {
					pf2.x = pf1.x;
					pf2.y = pf1.y + m_step.y;
					p2 = ConvertPixelPoint(pf2);
					pDC->LineTo(p2);
					pDC->MoveTo(p1);
				}

				pf2.x = pf1.x + m_step.x;
				pf2.y = pf1.y;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);

				pf2.y += m_step.y;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);

				if (CELL_BOTTOM_POS&m_SectorCellInfo[i].pos_type) {
					pf2.x -= m_step.x;
					p2 = ConvertPixelPoint(pf2);
					pDC->LineTo(p2);
				}
			}
		}

		CPen nonSelectPen;
		nonSelectPen.CreatePen(PS_SOLID, m_info.thick, RGB(40, 40, 40));
		oldPen = (CPen*)pDC->SelectObject(&nonSelectPen);
		for (int i = 0; i < m_SectorCellInfo.size(); i++) {
			if (m_SectorCellInfo[i].status == CELL_STATUS_NONSELECT) {
				pf1.x = m_SectorCellInfo[i].pt.x;
				pf1.y = m_SectorCellInfo[i].pt.y;

				p1 = ConvertPixelPoint(pf1);
				pDC->MoveTo(p1);
				
				pf2.x = pf1.x + m_step.x;
				pf2.y = pf1.y;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);
								
				pf2.y += m_step.y;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);
				
				pf2.x -= m_step.x;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);

				pf2.y -= m_step.y;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);

			}
		}

		pDC->SelectObject(oldPen);
	}
	else if (GRID_ANALYSIS == m_AnalysisMode)
	{
		CPen newPen1;
		float  interval = (float)m_grid_interval * m_RenderingScale / m_AppliedScale;
		newPen1.CreatePen(PS_SOLID, m_info.thick+2, COLORREF(RGB(255, 0, 0)));
		CPen *oldPen = (CPen*)pDC->SelectObject(&newPen1);
		
		if (m_drawing){
			POINTF p1;
			POINTF p2;

			p1 = ConvertRelativePoint(m_GridAnalysis_start);
			p2 = ConvertRelativePoint(m_GridAnalysis_end);

			TRACE(_T("gs %d %d\n"), m_GridAnalysis_start.x, m_GridAnalysis_start.y);
			TRACE(_T("ge %d %d\n"), m_GridAnalysis_end.x, m_GridAnalysis_end.y);

			TRACE(_T("p1 %f %f\n"), p1.x, p1.y);
			TRACE(_T("p2 %f %f\n"), p2.x, p2.y);

			m_GridAnalysis_startF.x = ((p1.x < p2.x) ? p1.x : p2.x);
			m_GridAnalysis_startF.y = ((p1.y < p2.y) ? p1.y : p2.y);
			m_GridAnalysis_endF.x = ((p1.x > p2.x) ? p1.x : p2.x);
			m_GridAnalysis_endF.y = ((p1.y > p2.y) ? p1.y : p2.y);

			//size and position
			m_position.x = floor(m_GridAnalysis_startF.x / m_step.x) + 1;
			m_position.y = floor(m_GridAnalysis_startF.y / m_step.y) + 1;
			m_size.x = floor(m_GridAnalysis_endF.x / m_step.x) - floor(m_GridAnalysis_startF.x / m_step.x) + 1;
			m_size.y = floor(m_GridAnalysis_endF.y / m_step.y) - floor(m_GridAnalysis_startF.y / m_step.y) + 1;
		}
		else
		{
			m_GridAnalysis_startF.x = (float)(m_position.x - 0.5) * m_step.x;
			m_GridAnalysis_startF.y = (float)(m_position.y - 0.5) * m_step.y;

			m_GridAnalysis_endF.x = (float)(m_position.x + m_size.x - 1.5) * m_step.x;
			m_GridAnalysis_endF.y = (float)(m_position.y + m_size.y - 1.5) * m_step.y;
		}


		double gap_x = interval / m_ImageSize.cx;
		double gap_y = interval / m_ImageSize.cy;

		int section_s_x = floor(m_GridAnalysis_startF.x / gap_x);
		int section_s_y = floor(m_GridAnalysis_startF.y / gap_y);

		int section_e_x = ceil(m_GridAnalysis_endF.x / gap_x);
		int section_e_y = ceil(m_GridAnalysis_endF.y / gap_y);

		m_GridAnalysis_startF.x = (float)section_s_x * gap_x;
		m_GridAnalysis_startF.y = (float)section_s_y * gap_y;

		m_GridAnalysis_endF.x = (float)section_e_x * gap_x;
		m_GridAnalysis_endF.y = (float)section_e_y * gap_y;

		//@@Draw Rectangle ...
		POINT pt1, pt2, pt3, pt4;

		pt1 = ConvertPixelPoint(m_GridAnalysis_startF);
		pt2 = ConvertPixelPoint(m_GridAnalysis_endF);

		pt3.x = pt1.x;
		pt3.y = pt2.y;

		pt4.x = pt2.x;
		pt4.y = pt1.y;

		pDC->MoveTo(pt1);
		pDC->LineTo(pt3);
		
		pDC->MoveTo(pt3);
		pDC->LineTo(pt2);

		pDC->MoveTo(pt2);
		pDC->LineTo(pt4);

		pDC->MoveTo(pt4);
		pDC->LineTo(pt1);

		pDC->SelectObject(oldPen);
		if (m_counterArray != nullptr && m_drawCross)
		{
			int gap = (pt2.x - pt1.x) / m_size.x;

			for (int y = 0; y < m_size.y; y++)
			{
				for (int x = 0; x < m_size.x; x++)
				{
					if (m_counterArray[y*m_size.x + x] == 1)
					{
						POINT p;
						p.x = pt1.x + gap * x;
						p.y = pt1.y + gap * y;

						DrawCross(pDC, p, gap, RGB(255, 255, 0));
					}
					if (m_counterArray[y*m_size.x + x] == -1)
					{
						POINT p;
						p.x = pt1.x + gap * x;
						p.y = pt1.y + gap * y;

						POINT p2;
						p2.x = p.x + gap / 2;
						p2.y = p.y + gap / 2;
						m_center = ConvertRelativePoint(p2);
						if (m_drawRedCross)
							DrawCross(pDC, p, gap, RGB(255, 0, 0));
					}
				}
			}
		}
	}
	else if (GRID_ANALYSIS_NONE == m_AnalysisMode || GRID_ANALYSIS_REGION_ANALYSIS == m_AnalysisMode) {
		float interval = (float)m_grid_interval * m_RenderingScale / m_AppliedScale;
		
		POINTF pf1, pf2;
		POINT p1, p2;

		for (int i = 0; i < m_GridCellInfo.size(); i++) {
			pf1.x = m_GridCellInfo[i].pt.x;
			pf1.y = m_GridCellInfo[i].pt.y;
			p1 = ConvertPixelPoint(pf1);
			pDC->MoveTo(p1);
			if (CELL_LEFT_POS&m_GridCellInfo[i].pos_type) {
				pf2.x = pf1.x;
				pf2.y = pf1.y + m_step.y;
				p2 = ConvertPixelPoint(pf2);
				pDC->LineTo(p2);
				pDC->MoveTo(p1);
			}
						
			pf2.x = pf1.x + m_step.x;
			pf2.y = pf1.y;
			p2 = ConvertPixelPoint(pf2);
			pDC->LineTo(p2);

			pf2.y += m_step.y;
			p2 = ConvertPixelPoint(pf2);
			pDC->LineTo(p2);

			if (GRID_ANALYSIS_REGION_ANALYSIS == m_AnalysisMode) {
				DrawNodeRect(pDC, p2, m_GridCellInfo[i].color);
			}

			if (CELL_BOTTOM_POS&m_GridCellInfo[i].pos_type) {
				pf2.x -= m_step.x;
				p2= ConvertPixelPoint(pf2);
				pDC->LineTo(p2);
			}
		}

		pDC->SelectObject(oldPen);
	}
	else
	{
		//size and position
		m_position.x = 1;
		m_position.y = 1;
		m_size.x = 1;
		m_size.y = 1;
	}
}


void	CGridObj::DrawCross(CDC *pDC, POINT pt, int gap, COLORREF color)
{
	CPen newPen1;

	newPen1.CreatePen(PS_SOLID, m_info.thick , color);
	CPen *oldPen = (CPen*)pDC->SelectObject(&newPen1);
	POINT p = pt;

	p.x += gap / 2;
	p.y += gap / 2;

	POINT p1, p2, p3, p4, p5, p6, p7, p8;

	p1.x = p.x;
	p2.x = p.x;
	p3.x = p.x;
	p4.x = p.x;

	p1.y = p.y - gap / 8 * 3;
	p2.y = p.y;
	p3.y = p.y;
	p4.y = p.y + gap / 8 * 3;

	p5.y = p.y;
	p6.y = p.y;
	p7.y = p.y;
	p8.y = p.y;

	p5.x = p.x - gap / 8 * 3;
	p6.x = p.x;
	p7.x = p.x;
	p8.x = p.x + gap / 8 * 3;

	pDC->MoveTo(p1);
	pDC->LineTo(p2);

	pDC->MoveTo(p3);
	pDC->LineTo(p4);

	pDC->MoveTo(p5);
	pDC->LineTo(p6);

	pDC->MoveTo(p7);
	pDC->LineTo(p8);
	
	pDC->SelectObject(oldPen);

}

bool	CGridObj::HitTest(POINT pt)
{
	if (m_status == NONE) return false;

	bool bSelect = false;

	return bSelect;
}


void	CGridObj::UpdateViewRegion(bool bUpdateHitRegion, bool bChangeViewSize, eMouseSTATUS mouse_status)
{
	if(GRID_ANALYSIS_REGION_DRAW == m_AnalysisMode){
		for (int i = 0; i < m_AnalysisRegions.size(); i++) {
			m_AnalysisRegions[i]->UpdateViewRegion(bUpdateHitRegion, bChangeViewSize, mouse_status);
		}
	}
	else if(GRID_ANALYSIS == m_AnalysisMode || GRID_ANALYSIS_REGION_ANALYSIS == m_AnalysisMode){

		if (bChangeViewSize) {
			m_GridAnalysis_start = ConvertPixelPoint(m_GridAnalysis_startF);
			m_GridAnalysis_end = ConvertPixelPoint(m_GridAnalysis_endF);
		}
	}
}


bool	CGridObj::UpdateMousePos(POINT pt)
{
	if (GRID_ANALYSIS != m_status) return false;
	m_GridAnalysis_end = pt;
	return true;
}

void	CGridObj::EditDescription(CString description)
{
	;
}

CString CGridObj::GetDescription()
{
	return "";
}


void	CGridObj::SetZoomLevel(int zoomLevel)
{
	if(zoomLevel == 0) m_AppliedScale = 0.25f;
	else if (zoomLevel == 1) m_AppliedScale = 0.5f;
	else m_AppliedScale = 1.f;

	UpdateSectorCellStatus();
}


void	CGridObj::AddDrawObj(CDrawObj *pDrawObj)
{
	m_AnalysisRegions.push_back(pDrawObj);
	UpdateSectorCellStatus();
}


int		CGridObj::MoveSectorCentorPos(int index, POINTF* ptf, float *scale)
{
	float interval_x, interval_y;
	interval_x = (float)m_sector_interval * m_RenderingScale / m_AppliedScale;
	if (m_GridRatioMode == GRID_RATIO_4X3)
		interval_y = (interval_x * 3) / 4;
	else if (m_GridRatioMode == GRID_RATIO_16X9)
		interval_y = (interval_x * 9) / 16;

	if (m_RotateStatus == ROTATE_NONE || m_RotateStatus == ROTATE_180) {
		interval_x /= m_ImageSize.cx;
		interval_y /= m_ImageSize.cy;
	}
	else {
		interval_y /= m_ImageSize.cy;
		interval_x /= m_ImageSize.cx;
	}

	ptf->x = m_SectorCellInfo[index].pt.x + interval_x / 2 ;
	ptf->y = m_SectorCellInfo[index].pt.y + interval_y / 2;

	*scale = m_AppliedScale;
	m_CurrentSelectIndex = index;

return 0;
}


void	CGridObj::SetSectorInterval(int interval)
{
	m_sector_interval = interval; 
	m_grid_interval = m_sector_interval/20;	//추후 설정 
}


void	CGridObj::CheckSectorSelect(POINT& pt)
{
	if (GRID_ANALYSIS_REGION_DRAW != m_AnalysisMode) return;
	POINTF	ptf = ConvertRelativePoint(pt);
	BOOL bUpdateGrid = FALSE;
	for (int i = 0; i < m_SectorCellInfo.size(); i++) {
		if (m_SectorCellInfo[i].pt.x < ptf.x && (m_SectorCellInfo[i].pt.x + m_step.x) > ptf.x &&
			m_SectorCellInfo[i].pt.y < ptf.y && (m_SectorCellInfo[i].pt.y + m_step.y) > ptf.y) {
			if (m_SectorCellInfo[i].status == CELL_STATUS_SELECT) m_SectorCellInfo[i].status = CELL_STATUS_NONSELECT;
			else if (m_SectorCellInfo[i].status == CELL_STATUS_NONSELECT) m_SectorCellInfo[i].status = CELL_STATUS_SELECT;
			bUpdateGrid = TRUE;
			break;
		}
	}
}


int		CGridObj::SetSectorCellInfo(CPoint point, int fossil_index, DWORD color)
{
	int index = GetHitCell(point);
	if (-1 == index) return -1;

	m_GridCellInfo[index].color = color;
	m_GridCellInfo[index].info = fossil_index;

	return index;
}


int		CGridObj::SetSectorCellInfo(int cell_index, int fossil_index, DWORD color)
{
	if (cell_index == -1) {
		for (int i = 0; i < m_GridCellInfo.size(); i++) {
			if(m_GridCellInfo[i].color == 0){
				m_GridCellInfo[i].color = color;
				m_GridCellInfo[i].info = fossil_index;
			}
		}
	}
	else if (cell_index < m_GridCellInfo.size()) {
		m_GridCellInfo[cell_index].color = color;
		m_GridCellInfo[cell_index].info = fossil_index;

		return cell_index;
	}

	return -1;
}


int		CGridObj::GetHitCell(POINT& pt)
{
	if (GRID_ANALYSIS_REGION_ANALYSIS != m_AnalysisMode) return -1;
	POINTF	ptf = ConvertRelativePoint(pt);

	POINTF cell_pf;
	const float HIT_MARGIN_X = m_step.x / 5.0f;
	const float HIT_MARGIN_Y = m_step.y/5.0f;
	for (int i = 0; i < m_GridCellInfo.size(); i++) {
		cell_pf.x = m_GridCellInfo[i].pt.x;
		cell_pf.y = m_GridCellInfo[i].pt.y;

		cell_pf.x += m_step.x;
		cell_pf.y += m_step.y;

		if ((cell_pf.x - HIT_MARGIN_X)< ptf.x && (cell_pf.x + HIT_MARGIN_X) > ptf.x &&
			(cell_pf.y - HIT_MARGIN_Y) < ptf.y && (cell_pf.y + HIT_MARGIN_Y) > ptf.y) {
			return i;
		}
	}

	return -1;
}


void	CGridObj::Reset()
{
	m_RegionSelectAll = true;
	m_GridRatioMode = GRID_RATIO_1X1;
	m_AnalysisMode = GRID_ANALYSIS_NONE;

	for (int i = 0; i < m_AnalysisRegions.size(); i++) {
		delete m_AnalysisRegions[i];
	}
	m_AnalysisRegions.clear();
	
	m_CurrentSelectIndex = 0;
	m_SectorCellInfo.clear();
	m_GridCellInfo.clear();

}