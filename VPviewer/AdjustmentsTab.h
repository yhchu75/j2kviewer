#pragma once


// CAdjustmentsTab dialog

class CAdjustmentsTab : public CDialog
{
	DECLARE_DYNAMIC(CAdjustmentsTab)

public:
	CAdjustmentsTab(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAdjustmentsTab();

// Dialog Data
	enum { IDD = IDD_ADJUSTMENTS };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedResetBrightness();
	afx_msg void OnBnClickedResetContrast();
	afx_msg void OnBnClickedResetSaturation();

	DECLARE_MESSAGE_MAP()

public:
	HACCEL		m_hAccel;
	CStatic		m_staticBrightness, m_staticContrast, m_staticSaturation;
	CButton		m_resetBrightness, m_resetContrast, m_resetSaturation;
	CSliderCtrl	m_sliderBrightness, m_sliderContrast, m_sliderSaturation;

public:
	CKdu_showApp	*app;
	int			m_brightness;
	int			m_contrast;
	int			m_saturation;


	void adjustColor(int brightness, int contrast, int saturation);

};
