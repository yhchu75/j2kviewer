// drawdoc.cpp : implementation of the CDrawDoc class
//

#include "stdafx.h"
#include "DrawobjMgr.h"
#include "EditLabel.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CDrawObjMgr::CDrawObjMgr() :
m_bShowMeasureObj(false),
m_bShowROIObj(false),
m_bShowScaleBarObj(false),
m_bShowLabelObj(false),
m_bShowGridObj(false),
m_bAutoUnit(false),
m_curStatus(NONE),
m_pNotifyReceiver(NULL)
{

}


CDrawObjMgr::~CDrawObjMgr()
{
	Clear();
}


void	CDrawObjMgr::DrawObjs(CDC *pDC)
{
	if (m_bShowGridObj) for (unsigned int i = 0; i < m_GridList.size(); i++)	m_GridList[i]->Draw(pDC, false);
	if (m_bShowROIObj)	for (unsigned int i = 0; i < m_ROIList.size(); i++)	m_ROIList[i]->Draw(pDC,false);
	if (m_bShowMeasureObj)for (unsigned int i = 0; i < m_MeasureList.size(); i++)	m_MeasureList[i]->Draw(pDC,false);
	if (m_bShowLabelObj) for (unsigned int i = 0; i < m_LabelList.size(); i++)	m_LabelList[i]->Draw(pDC, false);
	if (m_bShowScaleBarObj)	for (unsigned int i = 0; i < m_ScaleBarList.size(); i++)	m_ScaleBarList[i]->Draw(pDC,false);

	if (m_pActiveObj)
	{
		if ((m_pActiveObj->GetObjType() == ROI) && m_bShowROIObj)
			m_pActiveObj->Draw(pDC, true);
		else if ((m_pActiveObj->GetObjType() > LABEL) && m_bShowMeasureObj)
			m_pActiveObj->Draw(pDC, true);
		else if((m_pActiveObj->GetObjType() >= REGSION_RECT) && m_bShowGridObj)
			m_pActiveObj->Draw(pDC, true);
	}

	// RGB MEASUREMENT
	// ============================================================================
	else
	{
		if (m_bShowMeasureObj) {
			for (unsigned int i = 0; i < m_MeasureList.size(); i++)
				m_MeasureList[i]->UpdateViewRegion(false, false, CDrawObj::MOUSE_STATUS_LBUTTON_UP);
		}
	}
	// ============================================================================
}

bool	CDrawObjMgr::DeleteObj()
{
	if (m_pActiveObj == NULL)
		return false;

	if (m_pActiveObj->GetObjType() == ROI)
	{
		for (unsigned int i = 0; i < m_ROIList.size(); i++)
		{
			if (m_ROIList.at(i) == (CROIObj*)m_pActiveObj)
			{
				m_ROIList.erase(m_ROIList.begin() + i);
				m_pActiveObj = NULL;
				NotifyUpdateViewContent();

				return true;
			}
		}
	}

	if (m_pActiveObj->GetObjType() == LABEL)
	{
		for (unsigned int i = 0; i < m_LabelList.size(); i++)
		{
			if (m_LabelList.at(i) == (CLabelObj*)m_pActiveObj)
			{
				m_LabelList.erase(m_LabelList.begin() + i);
				m_pActiveObj = NULL;
				NotifyUpdateViewContent();
				return true;
			}
		}
	}

	if (m_pActiveObj->GetObjType() >= MEASURE_LINE)
	{
		for (unsigned int i = 0; i < m_MeasureList.size(); i++)
		{
			if (m_MeasureList.at(i) == (CMeasureObj*)m_pActiveObj)
			{
				m_MeasureList.erase(m_MeasureList.begin() + i);
				m_pActiveObj = NULL;
				NotifyUpdateViewContent();
				return true;
			}
		}
	}
	m_pActiveObj = NULL;

	return false;
}


bool	CDrawObjMgr::DeleteAll()
{
	m_pActiveObj = NULL;
	for (unsigned int i = 0; i < m_MeasureList.size(); i++)
	{
		delete m_MeasureList[i];
	}

	for (unsigned int i = 0; i < m_LabelList.size(); i++)
	{
		delete m_LabelList[i];
	}
	
	m_MeasureList.clear();
	m_LabelList.clear();

	NotifyUpdateViewContent();
	return false;
}


bool	CDrawObjMgr::AddObj(CDrawObj* obj)
{
	if (obj->GetObjType() == SCALEBAR)	m_ScaleBarList.push_back( (CScaleBarObj*) obj);
	else if (obj->GetObjType() == ROI)	m_ROIList.push_back((CROIObj*)obj);
	else if (obj->GetObjType() == LABEL) m_LabelList.push_back((CLabelObj*)obj);
	else if (obj->GetObjType() == GRID) m_GridList.push_back((CGridObj*)obj);
	else if (obj->GetObjType() >= MEASURE_LINE) m_MeasureList.push_back((CMeasureObj*)obj);	
	else return false;
	
	return true;
}


void	CDrawObjMgr::Clear()
{
	//Delate object.. 
	for (unsigned int i = 0; i < m_ScaleBarList.size(); i++)	delete m_ScaleBarList[i];
	for (unsigned int i = 0; i < m_ROIList.size(); i++)	delete m_ROIList[i];
	for (unsigned int i = 0; i < m_LabelList.size(); i++)	delete m_LabelList[i];
	for (unsigned int i = 0; i < m_MeasureList.size(); i++)	delete  m_MeasureList[i];
	for (unsigned int i = 0; i < m_GridList.size(); i++)	delete  m_GridList[i];
		
	m_ScaleBarList.clear();
	m_ROIList.clear();
	m_LabelList.clear();
	m_MeasureList.clear();
	m_GridList.clear();
}


void	CDrawObjMgr::UpdateViewImageNCanvase(SIZE image_size, RECT rcCanvas, eRotateStatus eRotate, float rendering_scale, RECT rcFrame)
{
	//check vailed image and canvase size
	if (rcCanvas.right == 0 || rcCanvas.bottom == 0 || image_size.cx == 0 || image_size.cy == 0) return;
	
	CDrawObj::setImageSize(image_size);
	CDrawObj::setCanvasRect(rcCanvas);
	CDrawObj::setRenderingScale(rendering_scale);
	CDrawObj::setRotateStatus(eRotate);
	m_FrameRect = rcFrame;

	//Update Hittest Region
	for (unsigned int i = 0; i < m_ScaleBarList.size(); i++)	m_ScaleBarList[i]->UpdateViewRegion(true, true);
	for (unsigned int i = 0; i < m_ROIList.size(); i++)	m_ROIList[i]->UpdateViewRegion(true, true);
	for (unsigned int i = 0; i < m_LabelList.size(); i++)	m_LabelList[i]->UpdateViewRegion(true, true);
	for (unsigned int i = 0; i < m_MeasureList.size(); i++)	m_MeasureList[i]->UpdateViewRegion(true, true);
	for (unsigned int i = 0; i < m_GridList.size(); i++)	m_GridList[i]->UpdateViewRegion(true, true);
}


void	CDrawObjMgr::UpdateAppliedScale(float applied_scale)
{
	CDrawObj::setAppliedScale(applied_scale);
	NotifyUpdateViewContent();
}


void	CDrawObjMgr::SetActiveObj(CDrawObj *pActiveObj)
{
	m_pActiveObj = pActiveObj;
	NotifyUpdateViewContent();
}


void	CDrawObjMgr::AddObj(eOBJTYPE type, ePENCOLOR color, std::string description)
{
	m_curStatus = (eSTATUS)type;
	m_descripton = description;
	m_selectPenColor = color;

	if (SCALEBAR == type)	m_bShowScaleBarObj = true;
	if (ROI == type) m_bShowROIObj = true;
	if (LABEL == type) m_bShowLabelObj = true;
	if (MEASURE_LINE <= type) m_bShowMeasureObj = true;
	if (GRID == type) m_bShowGridObj = true;
}


inline bool	CDrawObjMgr::ConvertCanvasPoint(POINT &in, POINT &out)
{	
	if ((m_FrameRect.left > in.x) || (m_FrameRect.top > in.y)) return false;
	if ((m_FrameRect.right < in.x) || (m_FrameRect.bottom < in.y)) return false;

	out.x = in.x - m_FrameRect.left;
	out.y = in.y - m_FrameRect.top;
	
	return true;
}

void	CDrawObjMgr::SetLabelProperty()
{
	if (m_pActiveObj)
	{
		CEditLabel dlg;
		dlg.mgr = this;

		if (dlg.DoModal() == IDOK){
			;
		}
		m_pActiveObj->setStatus(m_pActiveObj->NORMAL);
	}
	NotifyUpdateViewContent();
}

bool	CDrawObjMgr::OnRButtonDown(UINT nFlags, CPoint point)
{
	//Process Convert Point to Canvase Point
	bool	bProcess = false;
	POINT canvasPt;
	if (!ConvertCanvasPoint(point, canvasPt)) return false;

	m_curStatus = NONE;
	m_pActiveObj = NULL;

	if (m_bShowScaleBarObj){
		for (unsigned int i = 0; i < m_ScaleBarList.size(); i++){
			if (m_ScaleBarList[i]->HitTest(canvasPt)){
				m_pActiveObj = m_ScaleBarList[i];
				m_pActiveObj->setDrawing(true);
				bProcess = true;
			}
		}
	}

	if (m_bShowROIObj){
		for (unsigned int i = 0; i < m_ROIList.size(); i++){
			if (m_ROIList[i]->HitTest(canvasPt)){
				m_pActiveObj = m_ROIList[i];
				m_pActiveObj->setDrawing(true);
				bProcess = true;
			}
		}
	}

	if (m_bShowMeasureObj){
		for (unsigned int i = 0; i < m_MeasureList.size(); i++){
			if (m_MeasureList[i]->HitTest(canvasPt)){
				m_pActiveObj = m_MeasureList[i];
				m_pActiveObj->setDrawing(true);
				bProcess = true;
			}
		}
	}
	if (m_bShowLabelObj){
		for (unsigned int i = 0; i < m_LabelList.size(); i++){
			if (m_LabelList[i]->HitTest(canvasPt)){
				m_pActiveObj = m_LabelList[i];
				m_pActiveObj->setDrawing(true);
				bProcess = true;
			}
		}
	}
	
	NotifyUpdateViewContent();
	return bProcess;
}

bool	CDrawObjMgr::OnLButtonDown(UINT nFlags, CPoint point)
{ 
	//Process Convert Point to Canvase Point
	bool	bProcess = false;
	POINT canvasPt;
	if (!ConvertCanvasPoint(point, canvasPt)) return false;

	// RGB MEASUREMENT
	// ============================================================================
	BOOL rgb_opened = FALSE;

	if (m_pRgbDialog != NULL)
		rgb_opened = ::IsWindowVisible(m_pRgbDialog->GetSafeHwnd());

	if (rgb_opened == TRUE)
	{
		m_pActiveObj = NULL;
		return false;
	}
	// ============================================================================
	if (GRID_ANALYZE == m_curStatus){
		//m_GridList[0]->setStatus(CGridObj::GRID_ANALYSIS);
		m_GridList[0]->SetGridAnalize(canvasPt);
		m_pActiveObj = m_GridList[0];
		m_pActiveObj->setDrawing(true);
		bProcess = true;
	}
	else if (SELECT_SECTOR == m_curStatus) {
		if (m_GridList[0]->GetAnalysisMode() == CGridObj::GRID_ANALYSIS_REGION_DRAW) {
			m_GridList[0]->CheckSectorSelect(canvasPt);
			bProcess = true;
		}
	}
	else if (NEW_SECTOR_RECT_REGION == m_curStatus ||  NEW_SECTOR_ELLIPSE_REGION == m_curStatus || NEW_SECTOR_FREE_REGION == m_curStatus) {
		if (m_GridList[0]->GetAnalysisMode() == CGridObj::GRID_ANALYSIS_REGION_DRAW) {
			//POINT pt;
			//pt.x = canvasPt.x;
			//pt.y = canvasPt.y;
			//m_GridList[0]->SetGridAnalize(pt);
			//m_pActiveObj = m_GridList[0];
			if(NEW_SECTOR_RECT_REGION == m_curStatus)
				m_pActiveObj = new CRegionRectObj(canvasPt, m_selectPenColor, m_descripton);
			else if(NEW_SECTOR_ELLIPSE_REGION == m_curStatus)
				m_pActiveObj = new CRegionEllipseObj(canvasPt, m_selectPenColor, m_descripton);
			else if(NEW_SECTOR_FREE_REGION == m_curStatus)
				m_pActiveObj = new CRegionEllipseObj(canvasPt, m_selectPenColor, m_descripton);

			m_pActiveObj->setDrawing(true);
			bProcess = true;
		}
	}
	else if (NEW_SCALE == m_curStatus){
		m_pActiveObj = new CScaleBarObj(canvasPt, m_selectPenColor);
		bProcess = true;
	}
	else if (NEW_ROI == m_curStatus){
		m_pActiveObj = new CROIObj(canvasPt, m_selectPenColor, m_descripton);
		AtlTrace("NEW ROI Point [%d:%d] , Color [%d] , Description %s \n ", canvasPt.x, canvasPt.y, m_selectPenColor, m_descripton.c_str());
		bProcess = true;
	}
	else if (NEW_LABEL == m_curStatus){
		char strTemp[32];
		int index = m_LabelList.size();
		sprintf(strTemp, "Label %02d", index);
		AddObj(LABEL, BLOCK, strTemp);

		m_pActiveObj = new CLabelObj(canvasPt, BLOCK, BLOCK, true, 200, m_descripton);
		AtlTrace("NEW Label Point [%d:%d] , Color1 [%d] , Color2[%d], Description %s \n ", canvasPt.x, canvasPt.y, BLOCK, m_selectPenColor, m_descripton.c_str());
		bProcess = true;
	}
	else if (NEW_MEASURE_LINE == m_curStatus || NEW_MEASURE_CURVE == m_curStatus || NEW_MEASURE_AREA == m_curStatus){
		
		char strTemp[32];
		int index = m_MeasureList.size();
		
		if (m_curStatus == NEW_MEASURE_LINE){
			sprintf(strTemp, "Measure Line %02d", index);
			AddObj(MEASURE_LINE, BLUE, strTemp);
		}
		if (m_curStatus == NEW_MEASURE_CURVE){
			sprintf(strTemp, "Measure Curve %02d", index);
			AddObj(MEASURE_CURVE, RED, strTemp);
		}
		if (m_curStatus == NEW_MEASURE_AREA){
			sprintf(strTemp, "Measure Area %02d", index);
			AddObj(MEASURE_AREA, GREEN, strTemp);
		}

		m_pActiveObj = new CMeasureObj((eOBJTYPE)m_curStatus, canvasPt, m_selectPenColor, m_descripton);
		m_pActiveObj->setDrawn(false);
		AtlTrace("NEW Measure Type %d Point [%d:%d] , Color [%d] , Description %s \n ", m_curStatus, canvasPt.x, canvasPt.y, m_selectPenColor, m_descripton.c_str());
		bProcess = true;
	}
	else if (NONE == m_curStatus){	//find select object
		m_pActiveObj = NULL;

		if (m_pActiveObj == NULL && m_bShowScaleBarObj){
			for (unsigned int i = 0; i < m_ScaleBarList.size(); i++){
				if (m_ScaleBarList[i]->HitTest(canvasPt)){
					m_pActiveObj = m_ScaleBarList[i];
					m_pActiveObj->setDrawing(true);
					bProcess = true;
				}
			}
		}
		
		if (m_pActiveObj == NULL && m_bShowROIObj){
			for (unsigned int i = 0; i < m_ROIList.size(); i++){
				if (m_ROIList[i]->HitTest(canvasPt)){
					m_pActiveObj = m_ROIList[i];
					m_pActiveObj->setDrawing(true);
					bProcess = true;
				}
			}
		}

		if (m_pActiveObj == NULL && m_bShowMeasureObj){
			for (unsigned int i = 0; i < m_MeasureList.size(); i++){
				if (m_MeasureList[i]->HitTest(canvasPt)){
					m_pActiveObj = m_MeasureList[i];
					m_pActiveObj->setDrawing(true);
					bProcess = true;
				}
			}
		}
		if (m_pActiveObj == NULL && m_bShowLabelObj){
			for (unsigned int i = 0; i < m_LabelList.size(); i++){
				if (m_LabelList[i]->HitTest(canvasPt)){
					m_pActiveObj = m_LabelList[i];
					m_pActiveObj->setDrawing(true);
					bProcess = true;
				}
			}
		}

		if (m_pActiveObj) m_curStatus = SELECT;
	}

	NotifyUpdateViewContent();

	return bProcess;
}


bool	CDrawObjMgr::OnMouseMove(UINT nFlags, CPoint point)
{
	bool bProcess = false;
	POINT canvasPt;
	if (!ConvertCanvasPoint(point, canvasPt)){
		return false;
	}
	if (m_pActiveObj == NULL) return false;

	// RGB MEASUREMENT
	// ============================================================================
	BOOL rgb_opened = FALSE;

	if (m_pRgbDialog != NULL)
		rgb_opened = ::IsWindowVisible(m_pRgbDialog->GetSafeHwnd());

	if (rgb_opened == TRUE)
	{
		m_pActiveObj = NULL;
		return false;
	}
	// ============================================================================

	if (m_pActiveObj->UpdateMousePos(canvasPt)){
		m_pActiveObj->setDrawing(true);
		m_pActiveObj->UpdateViewRegion(false, false, CDrawObj::MOUSE_STATUS_MOVE);
		NotifyUpdateViewContent();
		bProcess = true;
	}

	return bProcess;
}


bool	CDrawObjMgr::OnLButtonUp(UINT nFlags, CPoint point)
{
	bool bProcess = false;
	POINT canvasPt;
	ConvertCanvasPoint(point, canvasPt);

	// RGB MEASUREMENT
	// ============================================================================
	BOOL rgb_opened = FALSE;

	if (m_pRgbDialog != NULL)
		rgb_opened = ::IsWindowVisible(m_pRgbDialog->GetSafeHwnd());

	if (rgb_opened == TRUE)
	{
		m_pActiveObj = NULL;
		return false;
	}
	// ============================================================================

	if (GRID_ANALYZE == m_curStatus){
		if (m_pActiveObj != NULL)
			m_pActiveObj->setDrawing(false);
		return bProcess;
	}

	else if (NEW_SECTOR_RECT_REGION == m_curStatus || NEW_SECTOR_ELLIPSE_REGION == m_curStatus || NEW_SECTOR_FREE_REGION == m_curStatus) {
		if(m_pActiveObj){
			m_pActiveObj->setDrawing(false);
			m_pActiveObj->setStatus(CDrawObj::NORMAL);
			m_pActiveObj->UpdateViewRegion(true, false, CDrawObj::MOUSE_STATUS_LBUTTON_UP);
			m_GridList[0]->AddDrawObj(m_pActiveObj);

			m_curStatus = NONE;
			NotifyUpdateViewContent();
		}
		//m_GridList[0]->SetGridAnalize(pt);
		return bProcess;
	}

	if (m_pActiveObj){
		m_pActiveObj->setDrawing(false);
		m_pActiveObj->setStatus(CDrawObj::NORMAL);
		m_pActiveObj->UpdateViewRegion(true, false, CDrawObj::MOUSE_STATUS_LBUTTON_UP);

		// RGB MEASUREMENT
		// ============================================================================
		if (m_bShowMeasureObj) {
			for (unsigned int i = 0; i < m_MeasureList.size(); i++)
				m_MeasureList[i]->UpdateViewRegion(false, false, CDrawObj::MOUSE_STATUS_LBUTTON_UP);
		}
		// ============================================================================

		NotifyUpdateViewContent();
	}
	else{
		m_curStatus = NONE;
		return bProcess;
	}

	if (NEW_SCALE == m_curStatus){
		m_ScaleBarList.push_back((CScaleBarObj*)m_pActiveObj);
		bProcess = true;
	}
	else if (NEW_ROI == m_curStatus){
		m_ROIList.push_back((CROIObj*)m_pActiveObj);
		AtlTrace("ADD ROI Point [%d:%d] size[%d] \n", canvasPt.x, canvasPt.y, m_ROIList.size());
		if (m_pNotifyReceiver) m_pNotifyReceiver->UpdateROIObj();
		bProcess = true;
	}
	else if (NEW_LABEL == m_curStatus){
		m_LabelList.push_back((CLabelObj*)m_pActiveObj);
		AtlTrace("ADD Label Point [%d:%d] size[%d] \n", canvasPt.x, canvasPt.y, m_LabelList.size());
		bProcess = true;
	}
	else if (NEW_MEASURE_LINE == m_curStatus || NEW_MEASURE_CURVE == m_curStatus || NEW_MEASURE_AREA == m_curStatus){
		m_pActiveObj->setDrawn(true);
		m_MeasureList.push_back((CMeasureObj*)m_pActiveObj);
		AtlTrace("ADD Measure Object Type [%d]  Point [%d:%d] size[%d] \n", m_curStatus, canvasPt.x, canvasPt.y, m_MeasureList.size());
		if (m_pNotifyReceiver) m_pNotifyReceiver->UpdateMeasureObj();
		bProcess = true;
	}
	else
		m_curStatus = NONE;
	
	NotifyUpdateViewContent();

	if (m_curStatus == NEW_ROI)
		m_curStatus = NONE;

	return bProcess;
}


void	CDrawObjMgr::ShowGridObj(bool bShowGridObj)
{
	if (bShowGridObj) {
		m_GridList[0]->UpdateCellStatus();
	}
	m_bShowGridObj = bShowGridObj;
}

// RGB MEASUREMENT
// ============================================================================
void CDrawObjMgr::RGBHistogram(CDrawObj *draw_obj)
{
	if (m_pRgbDialog == NULL)
	{
		m_pRgbDialog = new CRgbDialog(AfxGetMainWnd());
		m_pRgbDialog->Create(IDD_RGB_HISTOGRAM, AfxGetMainWnd());
	}

	m_pRgbDialog->EnableWindow(TRUE);
	m_pRgbDialog->SetData(draw_obj->m_arrRgbRed, draw_obj->m_arrRgbGreen, draw_obj->m_arrRgbBlue);
	m_pRgbDialog->ShowWindow(SW_SHOW);
	m_pRgbDialog->Invalidate();
	m_pActiveObj = NULL;
}
// ============================================================================
