#pragma once
#include "../ToolPlugIns/ToolPlusgIn.h"
#include <vector>

class IVPViewerCore;
class CToolPlugInsMgr
{
public:
	CToolPlugInsMgr();
	~CToolPlugInsMgr();

	size_t	Load(const TCHAR *szRootPath, IVPViewerCore *pVPViewerCore);
	void	InsertMenu(CMenu *pMenu, int BaseMENUIndex);
	void	EnableMenu(CMenu *pMenu, int BaseMENUIndex, bool enable);
	void	OnMenuCommand(int index);

	void	OnClose();
	
	BOOL	OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	BOOL	OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);
	BOOL	OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point);

	// NUMPAD
	// ============================================================================
	BOOL	OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags);
	// ============================================================================

private:
	std::vector <IToolPlugIn*> m_ToolsPlugIns;
	CMenu	m_ToolMenu;
};

