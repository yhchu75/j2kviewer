// Overview.cpp : implementation file
//

#include "stdafx.h"
#include "kdu_show.h"
#include "Overview.h"


// COverview dialog

IMPLEMENT_DYNAMIC(COverview, CDialog)

COverview::COverview(CKdu_showApp *app, CWnd* pParent)
	: CDialog()
{

	this->app = app;
    Create(COverview::IDD, pParent);
  	m_overviewTab.InsertItem(0, _T("General"));
	m_overviewTab.InsertItem(1, _T("Image Info"));
	m_overviewTab.InsertItem(2, _T("ROIs"));
	m_overviewTab.InsertItem(3, _T("Adjustments"));

	m_generalTab.Create(IDD_GENERAL, this);
	m_imageInfoTab.Create(IDD_IMAGEINFO, this);
	m_roisTab.Create(IDD_ROIS, this);
	m_adjustmentsTab.Create(IDD_ADJUSTMENTS, this);

	CRect rect;

	GetDlgItem(IDC_STATIC_FRAME)->GetWindowRect(rect);
	ScreenToClient(rect);
	m_generalTab.MoveWindow(rect);
	m_imageInfoTab.MoveWindow(rect);
	m_roisTab.MoveWindow(rect);
	m_adjustmentsTab.MoveWindow(rect);

	m_imageInfoTab.ShowWindow(SW_HIDE);
	m_roisTab.ShowWindow(SW_HIDE);
	m_adjustmentsTab.ShowWindow(SW_HIDE);

	GetDlgItem(IDC_TAB1)->GetWindowRect(rect);
	m_nHeight = rect.Height()+20;

	TRACE("m_nHeight = %d\n", m_nHeight);

	m_hAccel = ::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	cross_cursor = ::LoadCursor(NULL, IDC_CROSS);
	normal_cursor = ::LoadCursor(NULL, IDC_ARROW);

	overview_panning = false;
	m_bPointCounting = false;
	m_bDrawingBox = false;

    ShowWindow(SW_SHOW);

}

COverview::~COverview()
{
}

BOOL COverview::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	
}
void COverview::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_overviewTab);
	DDX_Control(pDX, IDC_BUTTON1, m_button);
}


BEGIN_MESSAGE_MAP(COverview, CDialog)
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON1, &COverview::OnBnClickedButton1)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &COverview::OnTcnSelchangeTab1)
	ON_WM_SHOWWINDOW()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


// COverview message handlers

void COverview::PostNcDestroy() 
{
  CDialog::PostNcDestroy();
  if (app != NULL)
    {
      assert(app->m_pOverview == this);
      app->m_pOverview = NULL;
      app = NULL;
    }
  delete this;
}

void COverview::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnClose();
	DestroyWindow();
}

void COverview::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CDialog::OnPaint() for painting messages
	TRACE("OVERVIEW OnPaint().......\n");
	kdu_dims	region;
	CRect	rect;
	GetClientRect(&rect);
	region.pos.x = rect.left;
	region.pos.y = rect.top;
	region.size.x=rect.right;
	region.size.y = rect.bottom;

	if (app != NULL)
	
		
//		app->initialize_regions4(
		app->paint_overview(&dc,region);
}

void COverview::check_win_size(kdu_coords image_size)
{
//	assert(overview_win_size>image_size);

		kdu_coords tmp;
		CRect rect;
	  GetClientRect(&rect);
      tmp.x = rect.right-rect.left;
      tmp.y = rect.bottom-rect.top;
	  //MJY

      app->set_view_size(tmp);

}



void COverview::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	
	if(m_bShowTab)
	{
		CRect rect;

		GetWindowRect(rect);
		rect.bottom -= m_nHeight;
		MoveWindow(rect);
		m_button.SetWindowTextA(_T("< < < < < < < < < "));
	}
	else
	{
		CRect rect;

		GetWindowRect(rect);
		rect.bottom += m_nHeight;
		MoveWindow(rect);
		m_button.SetWindowTextA(_T("> > > > > > > > >"));

	}

	m_bShowTab = !m_bShowTab;
}


void COverview::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here

	m_generalTab.ShowWindow(SW_HIDE);
	m_imageInfoTab.ShowWindow(SW_HIDE);
	m_roisTab.ShowWindow(SW_HIDE);
	m_adjustmentsTab.ShowWindow(SW_HIDE);


	switch(m_overviewTab.GetCurSel())
	{
		case 0 :
			m_generalTab.ShowWindow(SW_SHOW);
			break;
		case 1 :
			m_imageInfoTab.ShowWindow(SW_SHOW);
			break;
		case 2 :
			m_roisTab.ShowWindow(SW_SHOW);
			break;
		case 3 :
			m_adjustmentsTab.ShowWindow(SW_SHOW);
			break;
	}

	*pResult = 0;
}

void COverview::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Add your message handler code here

	m_generalTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	m_imageInfoTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	m_roisTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	m_adjustmentsTab.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

}

/******************************************************************************/
/*                           COverview::OnKeyDown                            */
/******************************************************************************/
//
//void COverview::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
//{
//	TRACE("Overview: KEY_DOWN\n");
//  if (app != NULL)
//    { // Look out for keys which are not bound to the menu -- currently the only
//       //keys of this form are those with obvious scrolling functions and the
//       //escape key.
//
//      if (nChar == VK_NEXT)
//		  AfxGetMainWnd()->SendMessage(WM_USER_LEFT_ARROW, 0, 0); //set_hscroll_pos(-scroll_step.x,true);
//    //  else if (nChar == VK_RIGHT)
//    //    app->set_hscroll_pos(scroll_step.x,true);
//    //  else if (nChar == VK_UP)
//    //    app->set_vscroll_pos(-scroll_step.y,true);
//    //  else if (nChar == VK_DOWN)
//    //    app->set_vscroll_pos(scroll_step.y,true);
//    //  else if (nChar == VK_PRIOR)
//    //    app->set_vscroll_pos(-scroll_page.y,true);
//    //  else if (nChar == VK_NEXT)
//    //    app->set_vscroll_pos(scroll_page.y,true);
//    //  if (nChar == VK_CONTROL)
//    //    {
//    //      if (panning)
//    //        {
//    //          SetCursor(normal_cursor);
//    //          panning = false;
//    //        }
//    //      kdu_coords tmp = last_mouse_point;
//    //      tmp.x /= pixel_scale;   tmp.y /= pixel_scale;
//    //      showing_metainfo = app->show_metainfo(tmp);
//    //      if (showing_metainfo)
//    //        SetCursor(overlay_cursor);
//    //    }
//    }
//  //CWnd::OnKeyDown(nChar, nRepCnt, nFlags);
//}
//

void COverview::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (overview_panning)
	{
		TRACE("overview_panning is true..%5.2f\n", overview_rate);
		app->set_scroll_pos((int)((point.x - last_mouse_point.x) / overview_rate),
                            (int)((point.y - last_mouse_point.y) / overview_rate), true);

		last_mouse_point = point;
		TRACE("point %d %d\n", point.x, point.y);
	}

	//  MJY : for draw a box for point counting : temp
	else if (m_bPointCounting && m_bDrawingBox)
    {
      pcbox_end.x = point.x;
      pcbox_end.y = point.y;
      RECT new_rect;
      if (pcbox_start.x < pcbox_end.x)
        { new_rect.left = pcbox_start.x; new_rect.right = pcbox_end.x; }
      else
        { new_rect.left = pcbox_end.x; new_rect.right = pcbox_start.x; }
      if (pcbox_start.y < pcbox_end.y)
        { new_rect.top = pcbox_start.y; new_rect.bottom = pcbox_end.y; }
      else
        { new_rect.top = pcbox_end.y; new_rect.bottom = pcbox_start.y; }
      new_rect.right++; new_rect.bottom++;
      SIZE border; border.cx = border.cy = 2;
      CDC *dc = GetDC();
      dc->DrawDragRect(&new_rect,border,&pointCount_rect,border);
      ReleaseDC(dc);
      pointCount_rect = new_rect;
    }
	
	//CDialog::OnMouseMove(nFlags, point);

}

void COverview::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	TRACE("LBUTTON DOWN %d %d\n", point.x, point.y);
	if (app == NULL) return;


	// MJY : draw a box for point counting : temp
	if (m_bPointCounting)
    { // Start point counting operation step 1 - draw a box
		//Invalidate(); //??
      pcbox_start.x = point.x;
      pcbox_start.y = point.y;
      pcbox_end = pcbox_start;
      pointCount_rect.left = point.x;
      pointCount_rect.right = point.x+1;
      pointCount_rect.top = point.y;
      pointCount_rect.bottom = point.y+1;
      SIZE border; border.cx = border.cy = 2;
      CDC *dc = GetDC();
      dc->DrawDragRect(&pointCount_rect,border,NULL,border);
      ReleaseDC(dc);
      app->suspend_processing(true);
      SetCapture();
      SetCursor(cross_cursor);
	  m_bDrawingBox = true;
    }
	else if (app->point_in_overview_image(point) && app->smaller_redbox()) 
	{
		CPoint redbox_center;

		redbox_center = app->get_redbox_center();
		overview_rate = app->get_overview_rate();

		app->set_scroll_pos((int)((point.x - redbox_center.x) / overview_rate),
							(int)((point.y - redbox_center.y) / overview_rate), true);

		last_mouse_point = point;
		overview_panning = true;

		TRACE("point is in the overivew image......\n");
	}


	CDialog::OnLButtonDown(nFlags, point);
}

BOOL COverview::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	if (m_hAccel != NULL)
		if (::TranslateAccelerator(m_hWnd, m_hAccel, pMsg))
			return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}

void COverview::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	// MJY : draw a box for point counting : temp : it was OnLButtonDown
   if (m_bPointCounting)
    { // End focusing operation
      SIZE border; border.cx = border.cy = 2;
      SIZE zero_border; zero_border.cx = zero_border.cy = 0;
      CDC *dc = GetDC();
      dc->DrawDragRect(&pointCount_rect,zero_border,&pointCount_rect,border);
      m_bDrawingBox = false;

      kdu_dims new_box;
      new_box.pos = pcbox_start; new_box.size = pcbox_end - pcbox_start;
      if (new_box.size.x < 0)
        { new_box.pos.x = pcbox_end.x; new_box.size.x = -new_box.size.x; }
      if (new_box.size.y < 0)
        { new_box.pos.y = pcbox_end.y; new_box.size.y = -new_box.size.y; }
	  dc->MoveTo(new_box.pos.x, new_box.pos.y);
	  dc->LineTo(new_box.pos.x+new_box.size.x, new_box.pos.y);
	  dc->LineTo(new_box.pos.x+new_box.size.x, new_box.pos.y+new_box.size.y);
	  dc->LineTo(new_box.pos.x, new_box.pos.y+new_box.size.y);
	  dc->LineTo(new_box.pos.x, new_box.pos.y);

      ReleaseDC(dc);

      //new_box.pos.x = new_box.pos.x / pixel_scale;
      //new_box.pos.y = new_box.pos.y / pixel_scale;
      //new_box.size.x = (new_box.size.x+pixel_scale-1)/pixel_scale;
      //new_box.size.y = (new_box.size.y+pixel_scale-1)/pixel_scale;
      //app->set_focus_box(new_box);
      //app->suspend_processing(false);
      ReleaseCapture();
      SetCursor(normal_cursor);
    }

	else if (overview_panning)
		overview_panning = false;

	CDialog::OnLButtonUp(nFlags, point);
}
