#include "stdafx.h"
#include "TimeLimitCheckor.h"
#include "Sntp.h"

#define _WIN32_DCOM 
#include <iostream>
using namespace std;
#include <comdef.h>
#include <Wbemidl.h>

# pragma comment(lib, "wbemuuid.lib")

BOOL GetBootupTime(SYSTEMTIME *st, BOOL bUseMFC)
{
	HRESULT hres;

	// Step 1: --------------------------------------------------
	// Initialize COM. ------------------------------------------
	
	if (bUseMFC){
		hres = CoInitializeEx(0, COINIT_APARTMENTTHREADED);
		if (FAILED(hres))
		{
			cout << "Failed to initialize COM library. Error code = 0x" << hex << hres << endl;
			return FALSE;                  // Program has failed.
		}
	}
	else{
		hres = CoInitializeEx(0, COINIT_MULTITHREADED);
		if (FAILED(hres))
		{
			cout << "Failed to initialize COM library. Error code = 0x" << hex << hres << endl;
			return FALSE;                  // Program has failed.
		}
		
		// Step 2: --------------------------------------------------
		// Set general COM security levels --------------------------
		hres = CoInitializeSecurity(
			NULL,
			-1,                          // COM authentication
			NULL,                        // Authentication services
			NULL,                        // Reserved
			RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication
			RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation
			NULL,                        // Authentication info
			EOAC_NONE,                   // Additional capabilities
			NULL                         // Reserved
			);


		if (FAILED(hres))
		{
			cout << "Failed to initialize security. Error code = 0x" << hex << hres << endl;
			CoUninitialize();
			return FALSE;                    // Program has failed.
		}

	}
	
	
	// Step 3: ---------------------------------------------------
	// Obtain the initial locator to WMI -------------------------

	IWbemLocator *pLoc = NULL;

	hres = CoCreateInstance(
		CLSID_WbemLocator,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator, (LPVOID *)&pLoc);

	if (FAILED(hres))
	{
		cout << "Failed to create IWbemLocator object." << " Err code = 0x" << hex << hres << endl;
		CoUninitialize();
		return FALSE;                 // Program has failed.
	}

	// Step 4: -----------------------------------------------------
	// Connect to WMI through the IWbemLocator::ConnectServer method

	IWbemServices *pSvc = NULL;

	// Connect to the root\cimv2 namespace with
	// the current user and obtain pointer pSvc
	// to make IWbemServices calls.
	hres = pLoc->ConnectServer(
		_bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
		NULL,                    // User name. NULL = current user
		NULL,                    // User password. NULL = current
		0,                       // Locale. NULL indicates current
		NULL,                    // Security flags.
		0,                       // Authority (for example, Kerberos)
		0,                       // Context object 
		&pSvc                    // pointer to IWbemServices proxy
		);

	if (FAILED(hres))
	{
		cout << "Could not connect. Error code = 0x" << hex << hres << endl;
		pLoc->Release();
		CoUninitialize();
		return FALSE;                // Program has failed.
	}

	cout << "Connected to ROOT\\CIMV2 WMI namespace" << endl;


	// Step 5: --------------------------------------------------
	// Set security levels on the proxy -------------------------

	hres = CoSetProxyBlanket(
		pSvc,                        // Indicates the proxy to set
		RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
		RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
		NULL,                        // Server principal name 
		RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
		RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
		NULL,                        // client identity
		EOAC_NONE                    // proxy capabilities 
		);

	if (FAILED(hres))
	{
		cout << "Could not set proxy blanket. Error code = 0x" << hex << hres << endl;
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return FALSE;               // Program has failed.
	}

	// Step 6: --------------------------------------------------
	// Use the IWbemServices pointer to make requests of WMI ----

	// For example, get the name of the operating system
	IEnumWbemClassObject* pEnumerator = NULL;
	hres = pSvc->ExecQuery(
		bstr_t("WQL"),
		bstr_t("SELECT * FROM Win32_OperatingSystem"),
		WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
		NULL,
		&pEnumerator);

	if (FAILED(hres))
	{
		cout << "Query for operating system name failed." << " Error code = 0x" << hex << hres << endl;
		pSvc->Release();
		pLoc->Release();
		CoUninitialize();
		return FALSE;               // Program has failed.
	}

	// Step 7: -------------------------------------------------
	// Get the data from the query in step 6 -------------------

	IWbemClassObject *pclsObj = NULL;
	ULONG uReturn = 0;

	while (pEnumerator)
	{
		HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1,
			&pclsObj, &uReturn);

		if (0 == uReturn)
		{
			break;
		}

		VARIANT vtProp;

		// Get the value of the Name property
		hr = pclsObj->Get(L"LastBootUpTime", 0, &vtProp, 0, 0);
		wcout << " Bootup Time : " << vtProp.bstrVal << endl;
		VariantClear(&vtProp);


		wchar_t * LookupName = vtProp.bstrVal;

		//Convert BSTR string data to SYSTEM time 
		swscanf_s(vtProp.bstrVal, L"%4d%2d%2d%2d%2d%2d", &st->wYear, &st->wMonth, &st->wDay, &st->wHour, &st->wMinute, &st->wSecond);
		pclsObj->Release();
	}
	
	// Cleanup
	// ========

	pSvc->Release();
	pLoc->Release();
	pEnumerator->Release();
	CoUninitialize();

	return TRUE;   // Program successfully completed.

}



CTimeLimitCheckor::CTimeLimitCheckor()
{
}


CTimeLimitCheckor::~CTimeLimitCheckor()
{
}


BOOL CTimeLimitCheckor::CheckLimitTime(FILETIME *pLimitTime, BOOL bUseMFC)
{

	BOOL bInTime = TRUE;;
	//Check with NTP time
	if (ChecktimeWithNTP(pLimitTime, &bInTime)){
		return bInTime;
	}
		
	//Check with bootup time
	if (ChecktimeWithBootupTime(pLimitTime, &bInTime, bUseMFC)){
		return bInTime;
	}
	
	//check windows systemTime
	SYSTEMTIME st;
	FILETIME CurTime;
	GetLocalTime(&st);
	SystemTimeToFileTime(&st, &CurTime);

	int result = CompareFileTime(pLimitTime, &CurTime);
	return (result >= 0);
}


BOOL	CTimeLimitCheckor::ChecktimeWithNTP(FILETIME *pLimitTime, BOOL* bInTime)
{
	//1. convert 
	WSADATA wsaData;
	BYTE wsMajorVersion = 1;
	BYTE wsMinorVersion = 1;
	WORD wVersionRequested = MAKEWORD(wsMinorVersion, wsMajorVersion);
	if (WSAStartup(wVersionRequested, &wsaData) != 0)
	{
		_tprintf(_T("Failed to load winsock stack\n"));
		return FALSE;
	}
	if (LOBYTE(wsaData.wVersion) != wsMajorVersion || HIBYTE(wsaData.wVersion) != wsMinorVersion)
	{
		_tprintf(_T("Winsock stack does not support version which this program requires\n"));
		return FALSE;
	}

	//Do the actual NTP Query
	CSNTPClient sntp;
	NtpServerResponse response;
	TCHAR *szTimeServer = _T("time.windows.com");

	if (sntp.GetServerTime(szTimeServer, response)){
		SYSTEMTIME st1 = response.m_ReceiveTime;
		FILETIME CurTime;
		SystemTimeToTzSpecificLocalTime(NULL, &st1, &st1);	//Localtime
		SystemTimeToFileTime(&st1, &CurTime);
		int result = CompareFileTime(pLimitTime, &CurTime);
		*bInTime = (result >= 0);

		return TRUE;
	}

	return FALSE;
}


BOOL	CTimeLimitCheckor::ChecktimeWithBootupTime(FILETIME *pLimitTime, BOOL* bInTime, BOOL bUseMFC )
{
	SYSTEMTIME st = { 0 };
	FILETIME CurTime;
	
	if (GetBootupTime(&st, bUseMFC)){
		//SystemTimeToTzSpecificLocalTime(NULL, &st, &st);	//Localtime
		SystemTimeToFileTime(&st, &CurTime);
		ULONGLONG milli_second = GetTickCount64();

		ULARGE_INTEGER uiCurTime;
		uiCurTime.HighPart = CurTime.dwHighDateTime;
		uiCurTime.LowPart = CurTime.dwLowDateTime;

		uiCurTime.QuadPart += milli_second * 10000;	//(0.1 us unit)

		CurTime.dwHighDateTime = uiCurTime.HighPart;
		CurTime.dwLowDateTime = uiCurTime.LowPart;

		//for debug 
		FileTimeToSystemTime(&CurTime, &st);

		int result = CompareFileTime(pLimitTime, &CurTime);
		*bInTime = (result >= 0);

		return TRUE;
	}

	return FALSE;

}



