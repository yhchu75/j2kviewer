// InnovaSystemGUIDGen.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "InnovaLicenseModule.h"
#include "TimeLimitCheckor.h"

#ifdef _DEBUG
#pragma comment(lib, "../LicenseModule/InnovaLicenseGenLib_d")
#else
#pragma comment(lib, "../LicenseModule/InnovaLicenseGenLib")
#endif

static TCHAR* GetThisPath(TCHAR* dest, DWORD destSize)
{
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);
	return dest;
}


BOOL	CheckLicenseFile()
{
	char				g_SystemMACAddr[6];
	InnovaLicenseInfoV2	g_LicenseInfo;

	TCHAR szLicenseFilePath[MAX_PATH];
	GetThisPath(szLicenseFilePath, MAX_PATH);

	_tcscat(szLicenseFilePath, _T("\\Innova_license.lic"));
	
	FILE *fp = _tfopen(szLicenseFilePath, _T("r+t"));
	if (NULL == fp){
		//MessageBox(NULL, _T("Can not find Innova License File(Innova_license.lic)"), _T("License ERROR"), MB_OK | MB_ICONERROR);
		return FALSE;
	}

	char license_buffer[INNOVA_LICENSE_LENGTH];
	fread(license_buffer, 1, INNOVA_LICENSE_LENGTH, fp);

	int nLicenseCnt = 0;
	InnovaLicenseInfoV2 LicenseInfo[MAX_SUPPORT_LICENSE_CNT];
	CTimeLimitCheckor TimeCheckor;

	FILETIME limitTime;
	int bCheckLicense = -1;
	if (VailedInnovaLicense(license_buffer, &nLicenseCnt, LicenseInfo, g_SystemMACAddr)){
		for (int i = 0; i < nLicenseCnt; i++){
			if (LicenseInfo[i].licenseType == INNOVA_LICENSE_VPVIEWER) {
				if (LicenseInfo[i].Options & INNOVA_CHECK_TIMELIMIT){
					if (TimeCheckor.CheckLimitTime(&LicenseInfo[i].TimeLimit)){
						bCheckLicense = 0;
					}
					else{
						bCheckLicense = -2;
						limitTime = LicenseInfo[i].TimeLimit;

					}
				}
				else{
					bCheckLicense = 0;
				}
				g_LicenseInfo = LicenseInfo[i];
				break;
			}
		}
	}
	fclose(fp);

	if (bCheckLicense < 0){
		if (bCheckLicense == -2){
			TCHAR strTemp[256];
			SYSTEMTIME st;
			FileTimeToSystemTime(&limitTime, &st);
			_stprintf(strTemp, _T("Invalied License  : Timeout license [%04d-%02d-%02d]"), st.wYear, st.wMonth, st.wDay);
			MessageBox(NULL, strTemp, _T("License ERROR"), MB_OK | MB_ICONERROR);
		}
		else{
			//MessageBox(NULL, _T("Invalied License File, Please ask Innovaplex "), _T("License ERROR"), MB_OK | MB_ICONERROR);
		}
	}

	return bCheckLicense >= 0;
}

