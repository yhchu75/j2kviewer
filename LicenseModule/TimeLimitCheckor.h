#pragma once

#include <Windows.h>

class CTimeLimitCheckor
{
public:
	CTimeLimitCheckor();
	~CTimeLimitCheckor();

	BOOL	CheckLimitTime(FILETIME *pLimitTime, BOOL bUseMFC = TRUE);

	void	SetTimeServer(const TCHAR *szTimeServer);

private:
	BOOL	ChecktimeWithNTP(FILETIME *pLimitTime, BOOL* bInTime);
	BOOL	ChecktimeWithBootupTime(FILETIME *pLimitTime, BOOL* bInTime, BOOL bUseMFC = TRUE);

};


