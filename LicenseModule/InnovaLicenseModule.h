#pragma once
#include <Windows.h>

#define MAX_STR_LEN				256
#define MAX_SUPPORT_LICENSE_CNT	16

struct SystemInfo
{
	BYTE		version[2];
	SYSTEMTIME	st;
	char		processor_serial[MAX_STR_LEN];
	BYTE		macAddr[MAX_STR_LEN];
};

const int INNOVA_GUID_LENGTH = 128;
const int INNOVA_LICENSE_LENGTH = 460;
const int INNOVA_LICENSE_TYPE_STIRNG = 32;



typedef enum { 
	INNOVA_LICENSE_JP2_ENCODER = 1, 
	INNOVA_LICENSE_JP2_MERGE, 
	INNOVA_LICENSE_VPVIEWER, 
	INNOVA_LICENSE_FOSSIL_DETECTOR,  
	INNOVA_LICENSE_FOSSIL_TRAINER,
	INNOVA_LICENSETYPE_COUNT,  
	INNOVA_UNDEFINE_LICENSE = 256
}InnovaPlexeLicenseType;

enum { 
	INNOVA_FUNCTIONS_SENDLOG = 0x000000001, 
	INNOVA_CHECK_TIMELIMIT	= 0x000000002,
	INNOVA_CHECK_SIZELIMIT = 0x000000004,
};


//Used OptionParam Param8[0] 
enum {
	INNOVA_SIZELIMIT_1G,
	INNOVA_SIZELIMIT_2G,
	INNOVA_SIZELIMIT_4G,
	INNOVA_SIZELIMIT_8G,
	INNOVA_SIZELIMIT_16G,
	INNOVA_SIZELIMIT_32G,
	INNOVA_SIZELIMIT_UNLIMITED,
};


const LONGLONG REAL_INNOVA_SIZELIMIT[INNOVA_SIZELIMIT_UNLIMITED] = {
	1 * 1024 * 1024 ,
	2 * 1024 * 1024 ,
	4 * 1024 * 1024 ,
	8 * 1024 * 1024 ,
	16 * 1024 * 1024,
	32 * 1024 * 1024
};


typedef struct{
	InnovaPlexeLicenseType licenseType;
	DWORD		Options;
	FILETIME	TimeLimit;
}InnovaLicenseInfo;


typedef struct{
	InnovaPlexeLicenseType licenseType;
	DWORD		Options;
	FILETIME	TimeLimit;
	union {
		UCHAR		Param8[16];
		USHORT		Param16[8];
		DWORD		Param32[4];
	}OptionParam;
}InnovaLicenseInfoV2;


typedef enum{
	INNOVA_SUCCESS = 0,
}INNOVA_LICENSE_ERRORCODE;

BOOL	GetInnovaSystemGUID(char szGUID[INNOVA_GUID_LENGTH]);
BOOL	GetSystemInfoLocal( SystemInfo *pSystemInfo );
BOOL	GetSystemInfoFromGUID(char szGUID[INNOVA_GUID_LENGTH], SystemInfo *pSystemInfo);

BOOL	GetLicenseFileInfo(char szLicense[INNOVA_LICENSE_LENGTH],
						SystemInfo *pSystemInfo,
						int* nLicenseCnt,
						InnovaLicenseInfoV2 *pLicenseInfo);

#ifdef _EXPORT_INNOVA_LINCENSE_DLL
extern "C" 
#endif
BOOL	VailedInnovaLicense(char szLicense[INNOVA_LICENSE_LENGTH],
							int* nLicenseCnt, 
							InnovaLicenseInfoV2 *pLicenseInfo,
							char	SystemMACAddr[6]);

BOOL	GenerateInnovaLicense(char szGUID[INNOVA_GUID_LENGTH], 
							  int totalLicneseSupport,
							  InnovaLicenseInfoV2 *pLicenseInfo,
							  char szLicense[INNOVA_LICENSE_LENGTH]);

#ifdef _EXPORT_INNOVA_LINCENSE_DLL
extern "C"
#endif
BOOL	CheckLimitTime(FILETIME *pLimitTime);

INNOVA_LICENSE_ERRORCODE GetInnovaLastErrorCode();