/*
	Innovaplex 
*/

#pragma once

class IVPViewerCore;
class IToolPlugIn
{
public:
	virtual const char* GetPlugInsToolName() = 0;
	virtual void Setup(IVPViewerCore *pVPViewerCore) = 0;
	virtual HWND Show(HWND hWnd, BOOL bShow) = 0;
	virtual BOOL IsActive() = 0;

	virtual void DestorySelf() = 0;
	
	
	//Mouse & Keybaord event 
	//Return Value : TRUE process this message, FALSE not process this message
	//virtual DWORD OnMessage() = 0;
	virtual BOOL OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point) = 0;
	virtual BOOL OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point) = 0;
	virtual BOOL OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point) = 0;

	// NUMPAD
	// ============================================================================
	virtual BOOL OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags) = 0;
	// ============================================================================

};

extern "C" {
	IToolPlugIn * WINAPI  CreatePlugInsTool();
};
