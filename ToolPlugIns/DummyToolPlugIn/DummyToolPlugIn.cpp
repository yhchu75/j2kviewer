// DummyToolPlugIn.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "DummyToolPlugIn.h"
#include "../../VPviewer/IVPViewerCore.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CDummyToolPlugInApp

BEGIN_MESSAGE_MAP(CDummyToolPlugInApp, CWinApp)
END_MESSAGE_MAP()


// CDummyToolPlugInApp construction

CDummyToolPlugInApp::CDummyToolPlugInApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CDummyToolPlugInApp object

CDummyToolPlugInApp theApp;

extern "C" IToolPlugIn * WINAPI  CreateToolPlugIn()
{
	return new CDummyToolPlugInDlg();
}


// CDummyToolPlugInApp initialization

BOOL CDummyToolPlugInApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}

IMPLEMENT_DYNAMIC(CDummyToolPlugInDlg, CDialog)

void CDummyToolPlugInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDummyToolPlugInDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ZOOM, &CDummyToolPlugInDlg::OnBnClickedButtonZoom)
	ON_BN_CLICKED(IDCANCEL, &CDummyToolPlugInDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


HWND  CDummyToolPlugInDlg::Show(HWND hWnd, BOOL bShow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());


	if (GetSafeHwnd() == NULL){
		Create(CDummyToolPlugInDlg::IDD, CWnd::FromHandle(hWnd));
	}
	
	ShowWindow(bShow);
	m_bActive = bShow;

	return GetSafeHwnd();
}


void	CDummyToolPlugInDlg::OnBnClickedButtonZoom()
{
	if (m_pVPViewerCore == NULL) return;
	m_pVPViewerCore->ZoomByIndex(0);
}


BOOL	CDummyToolPlugInDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	AfxMessageBox(_T("Click LButton "));
	return TRUE;
}


void CDummyToolPlugInDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	m_bActive = FALSE;
	ShowWindow(FALSE);
}
