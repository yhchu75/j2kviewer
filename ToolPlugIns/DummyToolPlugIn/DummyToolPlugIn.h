// DummyToolPlugIn.h : main header file for the DummyToolPlugIn DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "../ToolPlusgIn.h"

// CDummyToolPlugInApp
// See DummyToolPlugIn.cpp for the implementation of this class
//


class CDummyToolPlugInApp : public CWinApp
{
public:
	CDummyToolPlugInApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};


class CDummyToolPlugInDlg : public CDialog, public IToolPlugIn
{
	DECLARE_DYNAMIC(CDummyToolPlugInDlg)

public: 
	const char* GetPlugInsToolName(){ return "DummyTestToolPlugIn"; }
	
	CDummyToolPlugInDlg() :m_pVPViewerCore(NULL), m_bActive(FALSE){}
	~CDummyToolPlugInDlg(){}

	enum { IDD = IDD_DIALOG_DUMMY };

//interface
	void	Setup(IVPViewerCore *pVPViewerCore){ m_pVPViewerCore = pVPViewerCore; }
	HWND	Show(HWND hWnd, BOOL bShow);
	void	DestorySelf(){ delete this; }
	BOOL	IsActive(){ return m_bActive; }

	int		OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);

protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg void OnBnClickedButtonZoom();
	afx_msg void OnBnClickedCancel();

private:

	IVPViewerCore *	m_pVPViewerCore;
	BOOL			m_bActive;
};
