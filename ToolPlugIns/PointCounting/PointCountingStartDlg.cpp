// PointCounting.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <string>
#include "PointCountingStartDlg.h"
#include "PointCounting.h"

//#include "gXMLUtils.h"
#include "tinyxml2.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CPointCountingStartDlg, CDialog)

void CPointCountingStartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_GRID, m_gridStatus);
}


BEGIN_MESSAGE_MAP(CPointCountingStartDlg, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_START_NEXT, &CPointCountingStartDlg::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDCANCEL, &CPointCountingStartDlg::OnBnClickedCancel)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_BUTTON3, &CPointCountingStartDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


void	CPointCountingStartDlg::OnBnClickedButtonNext()
{
	m_pPointCountingToolPlugIn->NextPage();
}


BOOL	CPointCountingStartDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	//m_pVPViewerCore->GridOnButtonDown(nFlags, point);
	return TRUE;
}

BOOL	CPointCountingStartDlg::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	//m_pVPViewerCore->GridOnButtonMove(nFlags, point);
	return TRUE;
}


void CPointCountingStartDlg::OnBnClickedCancel()
{
	m_pPointCountingToolPlugIn->EndupProcess();
}


void CPointCountingStartDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	__super::OnActivate(nState, pWndOther, bMinimized);
	
	if (m_pVPViewerCore->GetAnalyzeMode() == IVPViewerCore::AnalyzeMode_GRID){
		SetDlgItemText(IDC_STATIC_GRID, L"ON");
		GetDlgItem(IDC_BUTTON_START_NEXT)->EnableWindow(TRUE);
	}
	else{
		SetDlgItemText(IDC_STATIC_GRID, L"OFF");
		GetDlgItem(IDC_BUTTON_START_NEXT)->EnableWindow(FALSE);
	}
	CString str;
	if (m_pVPViewerCore->GetScale() < 1.001 && m_pVPViewerCore->GetScale() > 0.999)
		str.Format(L"%.2f pixel/pixel", m_pVPViewerCore->GetScale());
	else
		str.Format(L"%.2f um/pixel", m_pVPViewerCore->GetScale());

	SetDlgItemText(IDC_STATIC_SCALE, str);
	// TODO: Add your message handler code here
}

void CPointCountingStartDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	m_pVPViewerCore->SetAnalyzeMode(bShow? IVPViewerCore::AnalyzeMode_GRID: IVPViewerCore::AnalyzeMode_NONE);
	CPointCountingBaseDlg::OnShowWindow(bShow, nStatus);	
}

bool CPointCountingStartDlg::LoadTrace(const TCHAR* szFile)
{
	tinyxml2::XMLDocument m_xmlDocument;
	tinyxml2::XMLNode *m_pNode;
	tinyxml2::XMLNode *m_pNode2;
	tinyxml2::XMLElement *m_pElement;
	tinyxml2::XMLAttribute *m_pAttribute;

	char buf[MAX_PATH];

	WideCharToMultiByte(CP_ACP, 0 , szFile, MAX_PATH, buf, MAX_PATH, NULL, NULL);
	int error = m_xmlDocument.LoadFile(buf);
	if (error != 0)
		return false;
	m_pNode = m_xmlDocument.FirstChildElement("Trace");

	int width, height;
	int rotation;
	double scale;
	int interval;
	std::string type;
	int *counter_array;
	int counted;

	m_pElement = m_pNode->FirstChildElement("Position");
	m_pPointCountingToolPlugIn->GetPosition()->y = m_pElement->IntAttribute("row", 1);
	m_pPointCountingToolPlugIn->GetPosition()->x = m_pElement->IntAttribute("column", 1);

	m_pElement = m_pNode->FirstChildElement("Size");
	m_pPointCountingToolPlugIn->GetSize()->x = width = m_pElement->IntAttribute("width", 1);
	m_pPointCountingToolPlugIn->GetSize()->y = height = m_pElement->IntAttribute("height", 1);

	m_pElement = m_pNode->FirstChildElement("Current");
	m_pPointCountingToolPlugIn->GetCurrentPosition()->y = m_pElement->IntAttribute("row", 1);
	m_pPointCountingToolPlugIn->GetCurrentPosition()->x = m_pElement->IntAttribute("column", 1);

	m_pElement = m_pNode->FirstChildElement("Rotation");
	m_pElement->QueryIntText(&rotation);

	m_pElement = m_pNode->FirstChildElement("Scale");
	m_pElement->QueryDoubleText(&scale);

	m_pElement = m_pNode->FirstChildElement("Interval");
	m_pElement->QueryIntText(&interval);

	m_pElement = m_pNode->FirstChildElement("Type");
	type = m_pElement->GetText();

	m_pElement = m_pNode->FirstChildElement("Counted");
	m_pElement->QueryIntText(&counted);
	m_pPointCountingToolPlugIn->SetCounting(counted);

	m_pVPViewerCore->SetGridProperty(0, scale, interval);

	counter_array = new int[width*height];
	m_pPointCountingToolPlugIn->SetCounterArray(counter_array);

	m_pElement = m_pNode->FirstChildElement("GRID")->FirstChildElement("item");
	for (m_pElement; m_pElement; m_pElement = m_pElement->NextSiblingElement())
	{
		int x = m_pElement->IntAttribute("column");
		int y = m_pElement->IntAttribute("row");
		if (x != 0 && y != 0){
			int val;
			m_pElement->QueryIntText(&val);
			m_pPointCountingToolPlugIn->GetCounterArray()[(y - 1)*width + (x - 1)] = val;
		}
	}

	return true;	
}

void CPointCountingStartDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
	CFileDialog load_dlg(TRUE, NULL, _T(""), OFN_NOCHANGEDIR, _T("All files(*.*)|*.*| XML Files(*.xml) | *.xml||"));
	load_dlg.m_ofn.nFilterIndex = 2;
	if (IDOK == load_dlg.DoModal()){
		if(LoadTrace(load_dlg.GetPathName()))
			m_pPointCountingToolPlugIn->JumpPage();
	}
}
