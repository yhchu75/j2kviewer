// CPointCountingCountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounting.h"
#include "PointCountingCountDlg.h"
#include "afxdialogex.h"

// CPointCountingCountDlg dialog

IMPLEMENT_DYNAMIC(CPointCountingCountDlg, CDialogEx)

CPointCountingCountDlg::CPointCountingCountDlg(CWnd* pParent /*=NULL*/)
: m_RadioBtn(0)
{
	current_position.x = 1;
	current_position.y = 1;
	privious_position.x = 1;
	privious_position.y = 1;
	counted = 0;
	counter_array = nullptr;
	current_val = 0;
	isLoaded = false;
}

CPointCountingCountDlg::~CPointCountingCountDlg()
{
}

void CPointCountingCountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL_CURRENT, m_currentCell);
	DDX_Control(pDX, IDC_LABEL_TOTAL, m_totalCells);
	DDX_Control(pDX, IDC_LABEL_COUNTED, m_countedCell);
	DDX_Control(pDX, IDC_LABEL_TOTAL_COUNT, m_totalCells2);
	DDX_Control(pDX, IDC_EDIT_ROW, m_editRow);
	DDX_Control(pDX, IDC_EDIT_COL, m_editCol);
	DDX_Radio(pDX, IDC_RADIO1, m_RadioBtn);
}


BEGIN_MESSAGE_MAP(CPointCountingCountDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CPointCountingCountDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_ENDANALYSIS, &CPointCountingCountDlg::OnBnClickedButtonEndanalysis)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CPointCountingCountDlg::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_BUTTON_SKIP, &CPointCountingCountDlg::OnBnClickedButtonSkip)
	ON_BN_CLICKED(IDC_BUTTON_MOVE, &CPointCountingCountDlg::OnBnClickedButtonMove)
	ON_BN_CLICKED(IDC_BUTTON_BACK, &CPointCountingCountDlg::OnBnClickedButtonBack)
	ON_BN_CLICKED(IDC_RADIO1, &CPointCountingCountDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CPointCountingCountDlg::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_BUTTON1, &CPointCountingCountDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CPointCountingCountDlg message handlers

BOOL	CPointCountingCountDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	return TRUE;
}

BOOL	CPointCountingCountDlg::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	return TRUE;
}

void CPointCountingCountDlg::OnBnClickedCancel()
{
	m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_NONE);
	m_pPointCountingToolPlugIn->EndupProcess();
}


void CPointCountingCountDlg::OnBnClickedButtonEndanalysis()
{
	m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_NONE);

	CString m_strPath;
	CString m_strFolder;
	CStdioFile file;
	// CFile file;
	CString strNow;

	SYSTEMTIME time;
	::ZeroMemory(reinterpret_cast<void*>(&time), sizeof(time));

	::GetLocalTime(&time);

	strNow.Format(_T("%4d-%02d-%02d %02d%02d%02d"), time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

	m_strPath.Format(L"PointCounting_%s_%s.xml", m_pVPViewerCore->GetFileName(), strNow);
	
	CFileDialog dlg(FALSE, _T("*.xml"), m_strPath, OFN_OVERWRITEPROMPT, _T("XML files(*.xml)|*.xml|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".xml")
		{
			m_strPath += ".xml";
		}

		SaveTrace(m_strPath);
	}
	m_strFolder = m_strPath.Left(m_strPath.GetLength() - 4);
	CreateDirectory(m_strFolder, NULL);

	for (int y = 0; y < size.y; y++)
	{
		for (int x = 0; x < size.x; x++)
		{
			CString ImagePath;
			ImagePath.Format(_T("%s\\%03d_%03d.bmp"), m_strFolder, y+1, x+1);
			
			POINT pt1;

			pt1.x = position.x + x;
			pt1.y = position.y + y;
			
			m_pVPViewerCore->CropGridImage(pt1, ImagePath);
		}
	}

	CString ImagePath;
	ImagePath.Format(_T("%s\\overview.bmp"), m_strFolder);
	m_pVPViewerCore->CropGridImage(position, ImagePath, size.x, size.y);

	m_pPointCountingToolPlugIn->EndupProcess(FALSE);	
}

void CPointCountingCountDlg::redraw()
{
	CString buf;
	buf.Format(L"%d x %d (row X col)", current_position.y, current_position.x);
	m_currentCell.SetWindowTextW(buf);
	buf.Format(L"%d x %d", size.y, size.x);
	m_totalCells.SetWindowTextW(buf);
	buf.Format(L"%d", counted);
	m_countedCell.SetWindowTextW(buf);
	buf.Format(L"%d", size.x * size.y);
	m_totalCells2.SetWindowTextW(buf);
}

void CPointCountingCountDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	m_pVPViewerCore->SetAnalyzeMode(bShow? IVPViewerCore::AnalyzeMode_GRID : IVPViewerCore::AnalyzeMode_NONE);
	CPointCountingBaseDlg::OnShowWindow(bShow, nStatus);
	// TODO: Add your message handler code here
	if (bShow)
	{
		if (!isLoaded){
			m_pVPViewerCore->GetGridBox(position, size);
			counter_array = new int[size.x * size.y];
			counter_array_buffer = new int[size.x * size.y];
			memset(counter_array, 0, sizeof(int)*size.x*size.y);
			memcpy(counter_array_buffer, counter_array, sizeof(int)*size.x*size.y);
			counter_array[0] = -1;
			current_val = 0;
			m_pVPViewerCore->SetGridBoxArray(counter_array);			
		}
		else
		{
			position = *m_pPointCountingToolPlugIn->GetPosition();
			size = *m_pPointCountingToolPlugIn->GetSize();
			current_position = *m_pPointCountingToolPlugIn->GetCurrentPosition();
						
			counter_array = new int[size.x * size.y];
			counter_array_buffer = new int[size.x * size.y];
			memcpy(counter_array, m_pPointCountingToolPlugIn->GetCounterArray(), sizeof(int)*size.x*size.y);
			memcpy(counter_array_buffer, counter_array, sizeof(int)*size.x*size.y);
			counted = m_pPointCountingToolPlugIn->GetCounting();

			current_val = counter_array[(current_position.y - 1)*size.y + current_position.x - 1];

			m_pVPViewerCore->SetGridBoxArray(counter_array);
			m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_GRID);
			m_pVPViewerCore->SetGridBox(position, size);

		}
		isLoaded = false;
		redraw();
		jump(current_position);
	}
	else
	{ 
		if (counter_array != nullptr){
			delete[] counter_array;
			delete[] counter_array_buffer;
			counter_array = nullptr;
		}
		TRACE("counter array deleted!\n");

		current_position.x = 1;
		current_position.y = 1;
		privious_position.x = 1;
		privious_position.y = 1;
		counted = 0;
		current_val = 0;
	}

}

void CPointCountingCountDlg::jump(POINT p)
{
	//change current val
	counter_array[((current_position.y - 1) * size.x) + current_position.x - 1] = current_val;
	memcpy(counter_array_buffer, counter_array, sizeof(int)*size.x*size.y);
	current_val = counter_array[((p.y - 1) * size.x) + p.x - 1];
	//change position
	counter_array[((p.y - 1) * size.x) + p.x - 1] = -1;
	current_position.x = p.x;
	current_position.y = p.y;
	SetDlgItemInt(IDC_EDIT_ROW, p.y);
	SetDlgItemInt(IDC_EDIT_COL, p.x);
	m_pVPViewerCore->SetCenterPos(p);
	redraw();
}

void CPointCountingCountDlg::next(bool skip)
{
	POINT p;
	p.x = current_position.x;
	p.y = current_position.y;

	if (!skip)
	{
		if (current_val != 1)
		{
			current_val = 1;
			counted++;
		}
	}
	
	if (p.x < size.x)
		p.x += 1;
	else if (p.y < size.y){
		p.x = 1;
		p.y += 1;
	}
	else{
		OnBnClickedButtonEndanalysis();
		return;
	}	

	m_pVPViewerCore->SetGridBoxArray(counter_array);
	jump(p);
}

void CPointCountingCountDlg::OnBnClickedButtonNext()
{
	// TODO: Add your control notification handler code here
	next(false);
}


void CPointCountingCountDlg::OnBnClickedButtonSkip()
{
	// TODO: Add your control notification handler code here
	next(true);
}


void CPointCountingCountDlg::OnBnClickedButtonMove()
{
	// TODO: Add your control notification handler code here
	int row = GetDlgItemInt(IDC_EDIT_ROW);
	int col = GetDlgItemInt(IDC_EDIT_COL);

	if (row > size.y)
		row = size.y;
	if (col > size.x)
		col = size.x;

	if (row < 1)
		row = 1;
	if (col < 1)
		col = 1;


	POINT p;
	p.x = col;
	p.y = row;

	jump(p);
}


void CPointCountingCountDlg::OnBnClickedButtonBack()
{
	// TODO: Add your control notification handler code here
	POINT p;
	p.x = current_position.x;
	p.y = current_position.y;


	if (p.x > 1)
		p.x -= 1;
	else if (p.y > 1){
		p.x = size.x;
		p.y -= 1;
	}
	else{
		p.x = size.x;
		p.y = size.y;
	}

	m_pVPViewerCore->SetGridBoxArray(counter_array);
	jump(p);
}


void CPointCountingCountDlg::SetRadioStatus()
{
	switch (m_RadioBtn)
	{
	case 0:
		m_pVPViewerCore->SetGridCrossHair(true);
		break;

	case 1:
		m_pVPViewerCore->SetGridCrossHair(false);
		break;
	}
}


void CPointCountingCountDlg::OnBnClickedRadio1()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	SetRadioStatus();
}


void CPointCountingCountDlg::OnBnClickedRadio2()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	SetRadioStatus();
}


void CPointCountingCountDlg::OnBnClickedButton1()
{
	CString m_strPath;
	CStdioFile file;
	// CFile file;
	

	m_strPath.Format(L"%s_grid_%d_%d_to_%d_%d.xml", m_pVPViewerCore->GetFileName(), position.x, position.y, position.x + size.x , position.y + size.y);
	CFileDialog dlg(FALSE, _T("*.xml"), m_strPath, OFN_OVERWRITEPROMPT, _T("XML files(*.xml)|*.xml|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".xml")
		{
			m_strPath += ".xml";
		}

		SaveTrace(m_strPath);
	}

	

	// TODO: Add your control notification handler code here
}

bool CPointCountingCountDlg::SaveTrace(const TCHAR *szFile)
{
	FILE *fp = _tfopen(szFile, _T("w+t"));
	if (NULL == fp)
		return false;

	InsertHeader(fp);
	InsertGRID(fp);
	InsertTale(fp);
	
	fclose(fp);

	return true;
}

void CPointCountingCountDlg::InsertHeader(FILE* fp)
{
	_ftprintf(fp, _T("<?xml version=\"1.0\"?> \n"));
	_ftprintf(fp, _T("<Trace>\n"));
	_ftprintf(fp, _T("<Version> %d </Version>\n"), 1);
	_ftprintf(fp, _T("<Position row=\"%d\" column=\"%d\"/>\n"), position.y, position.x);
	_ftprintf(fp, _T("<Size width=\"%d\" height=\"%d\"/>\n"), size.x, size.y);
	_ftprintf(fp, _T("<Current row=\"%d\" column=\"%d\"/>\n"), current_position.y, current_position.x);
	_ftprintf(fp, _T("<Rotation> %d </Rotation>\n"), m_pVPViewerCore->GetRotate()*90);
	_ftprintf(fp, _T("<Scale> %f</Scale>\n"), m_pVPViewerCore->GetScale());
	_ftprintf(fp, _T("<Interval> %d </Interval>\n"), m_pVPViewerCore->GetInterval());
	_ftprintf(fp, _T("<Counted> %d </Counted>\n"), counted);
}

void CPointCountingCountDlg::InsertTale(FILE* fp)
{
	_ftprintf(fp, _T("</Trace>\n"));
}

void CPointCountingCountDlg::InsertGRID(FILE* fp)
{
	_ftprintf(fp, _T("<Type> %s </Type>\n"), _T("integer"));
	_ftprintf(fp, _T("<GRID>\n"));
	for (int y = 0; y < size.y; y++)
	{
		for (int x = 0; x < size.x; x++)
		{
			_ftprintf(fp, _T("<item row=\"%d\" column=\"%d\">  %d </item>\n"), y+1, x+1, counter_array_buffer[y*size.x + x] );
		}
	}
	_ftprintf(fp, _T("</GRID>\n"));
}