//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PointCounting.rc
//
#define IDD_DIALOG_DUMMY                1000
#define IDC_BUTTON_ZOOM                 1000
#define IDD_DIALOG_START                1000
#define IDC_BUTTON1                     1001
#define IDD_DIALOG_COUNT                1001
#define IDC_BUTTON2                     1002
#define IDC_EDIT1                       1002
#define IDC_BUTTON_BACK                 1002
#define IDC_BUTTON3                     1003
#define IDC_EDIT2                       1003
#define IDC_RADIO1                      1003
#define IDD_DIALOG_DRAW                 1003
#define IDC_EDIT3                       1004
#define IDC_BUTTON_START_NEXT           1004
#define IDC_EDIT4                       1005
#define IDC_BUTTON7                     1005
#define IDC_BUTTON_ENDANALYSIS          1005
#define IDC_RADIO2                      1006
#define IDC_BUTTON_DRAW_NEXT            1006
#define IDC_BUTTON4                     1007
#define IDC_BUTTON_NEXT                 1007
#define IDC_BUTTON5                     1008
#define IDC_STATIC_GRID                 1008
#define IDC_BUTTON_SKIP                 1008
#define IDC_BUTTON6                     1009
#define IDC_STATIC_SCALE                1009
#define IDC_BUTTON_MOVE                 1009
#define IDC_EDIT5                       1010
#define IDC_EDIT_COL                    1010
#define IDC_LABEL_CURRENT               1011
#define IDC_LABEL_TOTAL                 1012
#define IDC_LABEL_COUNTED               1013
#define IDC_LABEL_TOTAL_COUNT           1014
#define IDC_EDIT_ROW                    1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1003
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
