// PointCounting.h : main header file for the PointCounting DLL
//

#pragma once
#include "../../VPviewer/IVPViewerCore.h"
#include "PointCounting.h"

class CPointCountingStartDlg : public CPointCountingBaseDlg
{
	DECLARE_DYNAMIC(CPointCountingStartDlg)

public: 
	
	CPointCountingStartDlg(){}
	~CPointCountingStartDlg(){}

	enum { IDD = IDD_DIALOG_START };

	int		OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	int		OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);
	void	ShowGrid(){
		int interval = m_pVPViewerCore->GetInterval();
		int rotate = m_pVPViewerCore->GetRotate();
		double scale = m_pVPViewerCore->GetScale();
		m_pVPViewerCore->SetGridProperty(0, scale, interval);	
		m_pVPViewerCore->ShowGrid(true);
	}

	bool	LoadTrace(const TCHAR *szFile);

protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

private:
	CStatic			m_gridStatus;
public:
	afx_msg void OnBnClickedButton3();
};
