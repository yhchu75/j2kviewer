// PointCounting.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "PointCounting.h"
#include "../../VPviewer/IVPViewerCore.h"

#include "PointCountingStartDlg.h"
#include "PointCountingDrawDlg.h"
#include "PointCountingCountDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CPointCountingApp

BEGIN_MESSAGE_MAP(CPointCountingApp, CWinApp)
END_MESSAGE_MAP()


// CPointCountingApp construction

CPointCountingApp::CPointCountingApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPointCountingApp object

CPointCountingApp theApp;

extern "C" IToolPlugIn * WINAPI  CreateToolPlugIn()
{
	return new CPointCountingToolPlugIn();
}


// CPointCountingApp initialization

BOOL CPointCountingApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}


CPointCountingToolPlugIn::CPointCountingToolPlugIn()
{
	m_bInstance = FALSE;
	m_pCurDlg = NULL;
	m_StartDlg = NULL;
	m_DrawDlg = NULL;
	m_CountDlg = NULL;

	m_pCounter_array = NULL;
	m_pPosition = NULL;
	m_pSize = NULL;
	m_pCurrent = NULL;
}


CPointCountingToolPlugIn::~CPointCountingToolPlugIn()
{
	if (m_StartDlg) delete m_StartDlg;
	if (m_DrawDlg) delete m_DrawDlg;
	if (m_CountDlg) delete m_CountDlg;

	if (m_pPosition != NULL)
	{
		delete[] m_pPosition;
		m_pPosition = NULL;
	}
	if (m_pSize != NULL)
	{
		delete[] m_pSize;
		m_pSize = NULL;
	}
	if (m_pCurrent != NULL)
	{
		delete[] m_pCurrent;
		m_pCurrent = NULL;
	}
}


void  CPointCountingToolPlugIn::Setup(IVPViewerCore *pVPViewerCore)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_StartDlg = new CPointCountingStartDlg();
	m_DrawDlg = new CPointCountingDrawDlg();
	m_CountDlg = new CPointCountingCountDlg();

	m_StartDlg->Setup(this, pVPViewerCore);
	m_DrawDlg->Setup(this, pVPViewerCore);
	m_CountDlg->Setup(this, pVPViewerCore);

	m_pPosition = new POINT();
	m_pSize = new POINT();
	m_pCurrent = new POINT();
}


HWND  CPointCountingToolPlugIn::Show(HWND hWnd, BOOL bShow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (m_pCurDlg == NULL){
		if (m_bInstance == FALSE){
			m_StartDlg->Create(CPointCountingStartDlg::IDD, CWnd::FromHandle(hWnd));
			m_DrawDlg->Create(CPointCountingDrawDlg::IDD, CWnd::FromHandle(hWnd));
			m_CountDlg->Create(CPointCountingCountDlg::IDD, CWnd::FromHandle(hWnd));
			m_bInstance = TRUE;
		}
		m_pCurDlg = m_StartDlg;
	}
	m_pCurDlg->ShowWindow(bShow);
	if (!bShow)
		m_pCurDlg = m_StartDlg;
	else
		m_StartDlg->ShowGrid();
	return m_pCurDlg->GetSafeHwnd();
}


void	CPointCountingToolPlugIn::NextPage()
{	
	if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}
	if (m_pCurDlg == m_StartDlg){
		m_pCurDlg = m_DrawDlg;
	}
	else if (m_pCurDlg == m_DrawDlg){
		m_pCurDlg = m_CountDlg;
	}
	else{
		EndupProcess();
		return;
	}
	m_pCurDlg->ShowWindow(TRUE);
}


void	CPointCountingToolPlugIn::EndupProcess(BOOL bCancel)
{
	if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}

	//TODO : Need to Initialize current setting .... 
	m_pCurDlg = NULL;	
}


void	CPointCountingToolPlugIn::JumpPage()
{
	if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}
	if (m_pCurDlg != m_StartDlg) {
		return;
	}
	m_pCurDlg = m_CountDlg;
	m_CountDlg->isLoaded = true;

	m_pCurDlg->ShowWindow(TRUE);
}