#pragma once

#include "../../VPviewer/IVPViewerCore.h"
#include "afxwin.h"
// CPointCountingCountDlg dialog

class CPointCountingCountDlg : public CPointCountingBaseDlg
{
	DECLARE_DYNAMIC(CPointCountingCountDlg)

public:
	CPointCountingCountDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPointCountingCountDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_COUNT };

	int		OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	int		OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);

	void	redraw();
	void	back();
	void	next(bool skip);
	void	jump(POINT p);

	bool	SaveTrace(const TCHAR *szFile);
	void	InsertHeader(FILE *fp);
	void	InsertTale(FILE *fp);
	void	InsertGRID(FILE *fp);

	bool	isLoaded;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonEndanalysis();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

private:
	POINT	position;
	POINT	size;
	POINT	current_position;
	POINT	privious_position;
	int*	counter_array;		//initial : 0 , counted : 1, counting : -1
	int*	counter_array_buffer;	//copy from counter_array
	int		counted;
	int		current_val;
public:
	
	CStatic m_currentCell;
	CStatic m_totalCells;
	CStatic m_countedCell;
	CStatic m_totalCells2;
	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnBnClickedButtonSkip();
	CEdit m_editRow;
	CEdit m_editCol;
	afx_msg void OnBnClickedButtonMove();
	afx_msg void OnBnClickedButtonBack();
	void SetRadioStatus();
	int m_RadioBtn;
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedButton1();
};
