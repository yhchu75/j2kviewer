#pragma once

#include "../../VPviewer/IVPViewerCore.h"
#include "afxwin.h"
//#include "afxwin.h"
// PointCountingDrawDlg dialog

class CPointCountingDrawDlg : public CPointCountingBaseDlg
{
	DECLARE_DYNAMIC(CPointCountingDrawDlg)

public:
	CPointCountingDrawDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPointCountingDrawDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_DRAW };

	int		OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	int		OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnClickedButtonDrawNext();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

public:
	CEdit m_GridRows;
	CEdit m_GridColumns;
	CEdit m_GridX;
	CEdit m_GridY;
	int *counter_array;
	CButton m_buttonNext;
	afx_msg void OnEnChangeEdit();
};
