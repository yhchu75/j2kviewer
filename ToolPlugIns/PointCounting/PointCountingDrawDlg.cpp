// CPointCountingDrawDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounting.h"
#include "PointCountingDrawDlg.h"
#include "afxdialogex.h"


// CPointCountingDrawDlg dialog

IMPLEMENT_DYNAMIC(CPointCountingDrawDlg, CDialogEx)

CPointCountingDrawDlg::CPointCountingDrawDlg(CWnd* pParent /*=NULL*/)
{

}

CPointCountingDrawDlg::~CPointCountingDrawDlg()
{
}

void CPointCountingDrawDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_GridRows);
	DDX_Control(pDX, IDC_EDIT2, m_GridColumns);
	DDX_Control(pDX, IDC_EDIT3, m_GridX);
	DDX_Control(pDX, IDC_EDIT4, m_GridY);
	DDX_Control(pDX, IDC_BUTTON_DRAW_NEXT, m_buttonNext);
}


BEGIN_MESSAGE_MAP(CPointCountingDrawDlg, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_DRAW_NEXT, &CPointCountingDrawDlg::OnClickedButtonDrawNext)
	ON_BN_CLICKED(IDCANCEL, &CPointCountingDrawDlg::OnBnClickedCancel)
	ON_EN_CHANGE(IDC_EDIT1, &CPointCountingDrawDlg::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_EDIT2, &CPointCountingDrawDlg::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_EDIT3, &CPointCountingDrawDlg::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_EDIT4, &CPointCountingDrawDlg::OnEnChangeEdit)
END_MESSAGE_MAP()


// CPointCountingDrawDlg message handlers

BOOL	CPointCountingDrawDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));

	m_pVPViewerCore->SetGridBoxArray(nullptr);
	m_pVPViewerCore->GridOnButtonDown(nFlags, point);
	m_buttonNext.EnableWindow(true);
	return TRUE;
}

BOOL	CPointCountingDrawDlg::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	POINT size;
	POINT position;
	m_pVPViewerCore->GridOnButtonMove(nFlags, point);
	if (m_buttonNext.IsWindowEnabled()){
		m_pVPViewerCore->GetGridBox(position, size);
		CString buf;
		buf.Format(L"%d", size.x);
		m_GridColumns.SetWindowTextW(buf);
		buf.Format(L"%d", size.y);
		m_GridRows.SetWindowTextW(buf);
		buf.Format(L"%d", position.x);
		m_GridX.SetWindowTextW(buf);
		buf.Format(L"%d", position.y);
		m_GridY.SetWindowTextW(buf);
	}
	//Invalidate();
	return TRUE;
}


void CPointCountingDrawDlg::OnClickedButtonDrawNext()
{
	m_pPointCountingToolPlugIn->NextPage();
}


void CPointCountingDrawDlg::OnBnClickedCancel()
{
	m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_NONE);
	m_pPointCountingToolPlugIn->EndupProcess();
}


void CPointCountingDrawDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	m_pVPViewerCore->SetAnalyzeMode(bShow? IVPViewerCore::AnalyzeMode_SECTOR :IVPViewerCore::AnalyzeMode_NONE);
	CPointCountingBaseDlg::OnShowWindow(bShow, nStatus);
	if (!bShow){
		CString buf;
		buf.Format(L"");
		m_GridColumns.SetWindowTextW(buf);
		m_GridRows.SetWindowTextW(buf);
		m_GridX.SetWindowTextW(buf);
		m_GridY.SetWindowTextW(buf);
		m_buttonNext.EnableWindow(false);
	}
}


void CPointCountingDrawDlg::OnEnChangeEdit()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CPointCountingBaseDlg::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString buf;

	int posx, posy, sizex, sizey;

	m_GridColumns.GetWindowTextW(buf);
	sizex = _ttoi(buf);
	m_GridRows.GetWindowTextW(buf);
	sizey = _ttoi(buf);
	m_GridX.GetWindowTextW(buf);
	posx = _ttoi(buf);
	m_GridY.GetWindowTextW(buf);
	posy = _ttoi(buf);

	POINT pos = { posx, posy };
	POINT size = { sizex, sizey };
	if (posx != 0 && posy != 0 && sizex != 0 && sizey != 0)
		m_pVPViewerCore->SetGridBox(pos, size);

}
