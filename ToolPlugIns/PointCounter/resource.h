//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PointCounter.rc
//
#define IDD_DIALOG1                     102
#define IDD_DIALOG_SAVE                 102
#define IDC_STATUS_SCALE_VALUE_LABEL    1001
#define IDC_MENU_LOAD_TRACE_BUTTON      1002
#define IDC_STATUS_CROSSHAIR_ON_RADIO   1003
#define IDD_DIALOG_MAIN                 1004
#define IDC_RADIO2                      1004
#define IDC_STATUS_CROSSHAIR_OFF_RADIO  1004
#define IDC_MENU_UNDO_BUTTON            1005
#define IDC_STATUS_TOTAL_VALUE_LABEL    1006
#define IDC_MENU_SAVE_RESULT_BUTTON     1007
#define IDC_MENU_RESTART_BUTTON         1008
#define IDC_MENU_SKIP_BUTTON            1009
#define IDC_START_ROW_EDIT              1010
#define IDC_STATUS_POSITION_VALUE_LABEL 1011
#define IDC_STATUS_SCORED_VALUE_LABEL   1012
#define IDC_START_TOTAL                 1013
#define IDC_START_TOTAL_VALUE           1013
#define IDC_CROSSHAIR_BUTTON            1014
#define IDC_PROGRESS1                   1015
#define IDC_GROUP_STATUS                1016
#define IDC_STATUS_LABEL                1017
#define IDC_STATUS_SCALE_LABEL          1018
#define IDC_STATUS_CROSSHAIR_LABEL      1019
#define IDC_STATUS_TOTAL_LABEL          1020
#define IDC_STATUS_POSITION_LABEL       1021
#define IDC_STATUS_SCORED_LABEL         1022
#define IDC_OUTPUT_LABEL                1023
#define IDC_OUTPUT_EDIT                 1024
#define IDC_MENU_LABEL                  1025
#define IDC_MENU_LOAD_PROFILE_BUTTON    1026
#define IDC_MENU_SAVE_TRACE_BUTTON      1027
#define IDC_MENU_BACK_STEP_BUTTON       1028
#define IDC_MENU_MOVE_TO_BUTTON         1029
#define IDC_MENU_ROW_EDIT               1030
#define IDC_MENU_COL_EDIT               1031
#define IDC_MENU_MUL_LABEL              1032
#define IDC_MENU_ROW_LABEL              1033
#define IDC_MENU_COL_LABEL              1034
#define IDC_MENU_SAME_AS_LAST_BUTTON    1035
#define IDC_START_GROUP                 1036
#define IDC_START_POINT_COUNTER_LABEL   1037
#define IDC_START_POINT_COUNTER_GUIDE_LABEL 1038
#define IDC_START_GRID_CELL_LABEL       1039
#define IDC_START_AREA_POSITION_LABEL   1040
#define IDC_START_COLUMN_EDIT           1041
#define IDC_START_LOCATION_X_EDIT       1042
#define IDC_START_LOCATION_Y_EDIT       1043
#define IDC_START_START_BUTTON          1044
#define IDC_START_ROW_LABEL             1045
#define IDC_START_COLUMN_LABEL          1046
#define IDC_START_LOCATION_X_LABEL      1047
#define IDC_START_LOCATION_Y_LABEL      1048
#define IDC_MAIN_PAGE_LABEL             1049
#define IDC_MAIN_GROUP                  1050
#define IDC_STATUS_CROSSHAIR_LABEL_2    1051

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
