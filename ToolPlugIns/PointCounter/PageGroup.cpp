// PageGroup.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounter.h"
#include "PageGroup.h"
#include "PointCounterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CPageGroup

IMPLEMENT_DYNAMIC(CPageGroup, CStatic)

CPageGroup::CPageGroup()
{

}

CPageGroup::~CPageGroup()
{
}


BEGIN_MESSAGE_MAP(CPageGroup, CStatic)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_START_START_BUTTON, &CPageGroup::OnBnClickedStartStartButton)
END_MESSAGE_MAP()



// CPageGroup message handlers


void CPageGroup::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CStatic::OnPaint() for painting messages

	CDC* window_dc = GetWindowDC();

	//work out the coordinates of the window rectangle,
	CRect window_rect;
	GetWindowRect(&window_rect);
	window_rect.OffsetRect(-window_rect.left, -window_rect.top);

	//Draw a single line around the outside
	CBrush line_brush(::GetSysColor(COLOR_3DFACE));
	//CBrush line_brush(RGB(200, 200, 200));
	window_dc->FrameRect(&window_rect, &line_brush);
	ReleaseDC(window_dc);
}


HBRUSH CPageGroup::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CStatic::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	if (strcmp(pWnd->GetRuntimeClass()->m_lpszClassName, "CItemButton") == 0)
	{
		CItemButton *item_button = (CItemButton *)pWnd;
		CRect window_rect, button_position;
		GetWindowRect(window_rect);
		item_button->GetWindowRect(button_position);
		button_position.left = button_position.left - window_rect.left;
		button_position.right = button_position.right - window_rect.left;
		button_position.top = button_position.top - window_rect.top;
		button_position.bottom = button_position.bottom - window_rect.top;

		//TRACE("ctl color l:%d, t:%d, r:%d, b:%d\n", button_position.left, button_position.top, button_position.right, button_position.bottom);

		pDC->SetBkMode(TRANSPARENT);

		CClientDC client_dc(this);
		CBrush bg_color;
		bg_color.CreateSolidBrush(item_button->BackgroundColor());
		CBrush* bg_brush = client_dc.SelectObject(&bg_color);
		client_dc.Rectangle(button_position.left, button_position.top, button_position.right, button_position.bottom);
		client_dc.SelectObject(bg_brush);

		return (HBRUSH)GetStockObject(NULL_BRUSH);
		//pDC->SetBkColor(item_button->BackgroundColor());
		//return hbr;
	}

	switch (pWnd->GetDlgCtrlID())
	{
	case IDC_START_POINT_COUNTER_LABEL:
	case IDC_START_POINT_COUNTER_GUIDE_LABEL:
	case IDC_MAIN_PAGE_LABEL:
		pDC->SetTextColor(RGB(0, 0, 0));
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}

void CPageGroup::OnBnClickedStartStartButton()
{
	// TODO: Add your control notification handler code here
	CPointCounterDlg *main_dialog = (CPointCounterDlg *)GetParent();
	main_dialog->MainPage();
	//main_dialog->CloseGrid();
	main_dialog->CountPosition();
}

unsigned int CPageGroup::AddControl(CString label_text, CFont *control_font, CRect control_size)
{
	CPointCounterDlg *main_dialog = (CPointCounterDlg *)GetParent();

	unsigned int height_buffer = 0;

	CStatic title_label;
	title_label.Create(L"CCCCCC", WS_CHILD, control_size, this);

	if (control_size.bottom > 0)
		height_buffer = control_size.bottom;
	else
	{
		CString string_buffer;
		CSize string_size;
		title_label.GetWindowTextW(string_buffer);
		string_size = TextSize(control_font, string_buffer);
		height_buffer = control_size.top + string_size.cy;
	}

	title_label.SetFont(control_font);
	title_label.MoveWindow(CRect(control_size.left, control_size.top, control_size.right, height_buffer));
	//GetDlgItem(control_id)->SetParent(this);

	return height_buffer - control_size.top;
}

unsigned int CPageGroup::AddControl(unsigned int control_id, CFont *control_font, CRect control_size)
{
	CPointCounterDlg *main_dialog = (CPointCounterDlg *)GetParent();

	if (main_dialog->GetDlgItem(control_id))
		main_dialog->GetDlgItem(control_id)->SetParent(this);

	if (GetDlgItem(control_id)->GetSafeHwnd())
	{
		unsigned int height_buffer = 0;

		if (control_size.bottom > 0)
			height_buffer = control_size.bottom;
		else
		{
			CString string_buffer;
			CSize string_size;
			GetDlgItem(control_id)->GetWindowTextW(string_buffer);
			string_size = TextSize(control_font, string_buffer);
			height_buffer = control_size.top + string_size.cy;
		}

		GetDlgItem(control_id)->SetFont(control_font);
		GetDlgItem(control_id)->MoveWindow(CRect(control_size.left, control_size.top, control_size.right, height_buffer));
		//GetDlgItem(control_id)->SetParent(this);

		return height_buffer - control_size.top;
	}

	return 0;
}

CSize CPageGroup::TextSize(CFont *text_font, CString text_string)
{
	CClientDC text_dc(this);
	CSize text_size;
	int token_position = 0;
	CString token_string = text_string.Tokenize(_T("\r\n"), token_position);

	text_dc.SelectObject(text_font);
	text_size.cx = text_size.cy = 0;

	while (!token_string.IsEmpty())
	{
		CSize token_size = text_dc.GetTextExtent(token_string, token_string.GetLength());
		text_size.cx = text_size.cx < token_size.cx ? token_size.cx : text_size.cx;
		text_size.cy += token_size.cy;
		token_string = text_string.Tokenize(_T("\r\n"), token_position);
	}

	return text_size;
}

BOOL CPageGroup::AddButton(int item_count, POINTITEM point_item, CRect button_position
	//, int item_count, int button_order
	)
{
	//CFont *button_font = new CFont;
	//button_font->CreatePointFont(70, L"Arial");
	CItemButton *item_button = new CItemButton;
	item_button->SetData(item_count, point_item);
	item_button->Create(point_item.name, WS_CHILD | WS_VISIBLE | WS_BORDER | WS_EX_TRANSPARENT | SS_NOTIFY | SS_CENTER | SS_CENTERIMAGE | SS_ENDELLIPSIS, button_position, this, NULL);
	item_button->SetWindowPos(&CWnd::wndTop, button_position.left, button_position.top, button_position.Width(), button_position.Height(), NULL);
	//item_button->SetFont(button_font);
	item_button->ShowWindow(SW_SHOW);
	//button_font->Detach();
	//delete button_font;
	m_arrItemButton.Add(item_button);
	return TRUE;
}
