// PointCounting.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "PointCounter.h"
#include "PointCounterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CPointCountingApp

BEGIN_MESSAGE_MAP(CPointCounterApp, CWinApp)
END_MESSAGE_MAP()


// CPointCountingApp construction

CPointCounterApp::CPointCounterApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPointCountingApp object

CPointCounterApp theApp;

extern "C" IToolPlugIn * WINAPI  CreateToolPlugIn()
{
	return new CPointCounterToolPlugIn();
}


// CPointCountingApp initialization

BOOL CPointCounterApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}


CPointCounterToolPlugIn::CPointCounterToolPlugIn()
{
	m_bInstance = FALSE;
	m_pCurDlg = NULL;
	m_pMainDlg = NULL;

	m_pCounter_array = NULL;
	m_pPosition = NULL;
	m_pSize = NULL;
	m_pCurrent = NULL;
}


CPointCounterToolPlugIn::~CPointCounterToolPlugIn()
{
	if (m_pMainDlg) delete m_pMainDlg;

	if (m_pPosition != NULL)
	{
		delete[] m_pPosition;
		m_pPosition = NULL;
	}
	if (m_pSize != NULL)
	{
		delete[] m_pSize;
		m_pSize = NULL;
	}
	if (m_pCurrent != NULL)
	{
		delete[] m_pCurrent;
		m_pCurrent = NULL;
	}
}


void  CPointCounterToolPlugIn::Setup(IVPViewerCore *pVPViewerCore)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pMainDlg = new CPointCounterDlg();
	m_pMainDlg->Setup(this, pVPViewerCore);

	m_pPosition = new POINT();
	m_pSize = new POINT();
	m_pCurrent = new POINT();
}


HWND  CPointCounterToolPlugIn::Show(HWND hWnd, BOOL bShow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if (m_pCurDlg == NULL)
	{
		if (m_bInstance == FALSE)
		{
			m_pMainDlg->Create(CPointCounterDlg::IDD, CWnd::FromHandle(hWnd));
			m_bInstance = TRUE;
		}
		m_pCurDlg = m_pMainDlg;
	}

	if (bShow)
	{
		m_pMainDlg->ShowGrid();
		m_pCurDlg->StartPage();
	}

	m_pCurDlg->ShowWindow(bShow);

	return m_pCurDlg->GetSafeHwnd();
}


void	CPointCounterToolPlugIn::NextPage()
{	
	/*if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}
	if (m_pCurDlg == m_StartDlg){
		m_pCurDlg = m_DrawDlg;
	}
	else if (m_pCurDlg == m_DrawDlg){
		m_pCurDlg = m_CountDlg;
	}
	else{
		EndupProcess();
		return;
	}
	m_pCurDlg->ShowWindow(TRUE);*/
}


void	CPointCounterToolPlugIn::EndupProcess(BOOL bCancel)
{
	//TODO : Need to Initialize current setting .... 
	if (m_pCurDlg)
		m_pCurDlg->ShowWindow(SW_HIDE);

	m_pCurDlg = NULL;
}
