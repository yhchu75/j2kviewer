#pragma once
#include "afxcmn.h"

#include "PointCounterDlg.h"

// CProgressDialog dialog

class CProgressDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CProgressDialog)

public:
	CProgressDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG_SAVE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CProgressCtrl m_ctlProgressBar;
	virtual void PostNcDestroy();

	CPointCounterDlg *m_PCDlg;
	void PrcType(int prc_type);
	void ProgressPos();
	void ProgressPos(int progress_pos);
	int m_nPrcType = 0;
	int m_nPrcCount = 0;
	void InitProgress();
	void StartProgress();
};
