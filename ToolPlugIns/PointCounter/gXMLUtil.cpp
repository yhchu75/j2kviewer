#include "stdafx.h"
#include "gXMLUtil.h"
#include "gXMLUtils.h"
#include <atlstr.h> 

#ifndef TRACE
#include <atlbase.h>
#define TRACE AtlTrace
#endif



#define CHECK_XML_RETURN_AND_ASSIGN(VAL)	\
	if(!m_pXML){	\
		TRACE(_T("XML not assigned before\n"));\
		return VAL;\
	}\
	IXMLDOMNode* pXML = (IXMLDOMNode*)m_pXML;\



CXMLUtil::CXMLUtil(PXML pXML)
{
	CoInitialize(NULL);
	m_pXMLDoc	= NULL;
	m_pXML		= pXML;
}


CXMLUtil::CXMLUtil()
{
	CoInitialize(NULL);
	m_pXMLDoc	= NULL;
	m_pXML		= NULL;
}


CXMLUtil::~CXMLUtil()
{
	if(m_pXMLDoc){
		IXMLDOMDocument2*	pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;
		pXMLDoc->Release();
	}
	//ReleaseXML();
	CoUninitialize();
}


BOOL	CXMLUtil::Load(const TCHAR *szFileName, TCHAR *szRootTag)
{
	IXMLDOMDocument2*	pXMLDoc;// = new IXMLDOMDocument2;
	if(!m_pXMLDoc){
		HRESULT hr;
		hr = CoCreateInstance(__uuidof(DOMDocument) , NULL, CLSCTX_INPROC_SERVER, __uuidof(IXMLDOMDocument2), (void**)&pXMLDoc); 
		if(S_OK != hr){
			TRACE(_T("Can not Create XMLDOMDocument Install MSXML3.0"));
			return FALSE;
		}
		m_pXMLDoc = pXMLDoc;
	}else{
		pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;
		ReleaseXML();
	}

	pXMLDoc->resolveExternals = VARIANT_TRUE;
	pXMLDoc->async = VARIANT_FALSE;
	pXMLDoc->validateOnParse = VARIANT_TRUE;

	VARIANT_BOOL varRet = VARIANT_FALSE;

	varRet = pXMLDoc->load(szFileName);
	if (varRet == VARIANT_FALSE){
		return FALSE;
	}

	IXMLDOMNodeListPtr	pRootXMLList;
	bool bRet = true;

	try{
		pRootXMLList	= pXMLDoc->documentElement->getElementsByTagName(szRootTag);
		IXMLDOMNode* pNode;
		pRootXMLList->get_item(0, &pNode);

		m_pXML = (PXML)pNode;
		
	}catch(...)
	{
		bRet = false;
	}

	_tcscpy_s(m_szXMLPath, szFileName);

	return TRUE;
}


BOOL	CXMLUtil::LoadXML(char *szXML, int nLength, TCHAR *szRootTag)
{
	if(!m_pXMLDoc){
		HRESULT hr;
		IXMLDOMDocument2*	pXMLDoc;
		hr = CoCreateInstance(__uuidof(DOMDocument) , NULL, CLSCTX_INPROC_SERVER, __uuidof(IXMLDOMDocument2), (void**)&pXMLDoc); 
		if(S_OK != hr){
			TRACE(_T("Can not Create XMLDOMDocument Install MSXML3.0"));
			return FALSE;
		}
		m_pXMLDoc = pXMLDoc;
	}else{
		ReleaseXML();
	}

	VARIANT_BOOL vb;
	USES_CONVERSION;
	IXMLDOMDocument2Ptr	XMLDocPtr;
	BOOL bRet = TRUE;

	XMLDocPtr = (IXMLDOMDocument2*)m_pXMLDoc;
	szXML[nLength] = '\0';
	BSTR bstr = SysAllocString( A2W( (char *)szXML ) );

	vb = XMLDocPtr->loadXML( bstr);

	if( VARIANT_TRUE == vb ){
		IXMLDOMNodeListPtr	pRootXMLList;
		
		try{
			pRootXMLList	= XMLDocPtr->documentElement->getElementsByTagName(szRootTag);
			IXMLDOMNode* pNode;
			pRootXMLList->get_item(0, &pNode);

			m_pXML = (PXML)pNode;
			
		}catch(...)
		{
			bRet = FALSE;
		}
	}else {
		MSXML2::IXMLDOMParseErrorPtr pErr = XMLDocPtr->parseError;

		CString strLine, sResult;
		strLine.Format(_T(" ( line %u, column %u )"), pErr->Getline(), pErr->Getlinepos());
		// Return validation results in message to the user.
		if (pErr->errorCode != S_OK)
		{
			sResult = CString("Validation failed on ") +
			 CString ("\n=====================") +
			 CString("\nReason: ") + CString( (char*)(pErr->Getreason())) +
			 CString("\nSource: ") + CString( (char*)(pErr->GetsrcText())) +
			 strLine + CString("\n");

			ATLTRACE(sResult);
		}
		bRet = FALSE;
	}

	SysFreeString( bstr );
	return bRet;
}


void	CXMLUtil::ReleaseXML()
{
	if(m_pXML){
		IXMLDOMNode* pNode;
		pNode = (IXMLDOMNode* )m_pXML;
		pNode->Release();
	}
}


BOOL	CXMLUtil::Save(const TCHAR *szFileName)
{
	if(!m_pXMLDoc){
		TRACE(_T("IXMLDOMDocument2 not loaded before\n"));
		return FALSE;
	}

	HRESULT hr;
	IXMLDOMDocument2*	pXMLDoc = (IXMLDOMDocument2*)m_pXMLDoc;

	if (NULL == szFileName) {
		hr = pXMLDoc->save(m_szXMLPath);
	} else {
		hr = pXMLDoc->save(szFileName);
	}

	if(S_OK != hr){
		return FALSE;
	}
	return TRUE;
}


BOOL	CXMLUtil::SetXML(PXML pXmlElement)
{
	if(NULL == pXmlElement) return FALSE;
	ReleaseXML();

	m_pXML = pXmlElement;
	return TRUE;
}

	
TCHAR*	CXMLUtil::GetNodeName()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);

	IXMLDOMElementPtr XMLElement;
	XMLElement = (IXMLDOMNodePtr)pXML;

	return XMLElement->GetnodeName();
}


TCHAR*	CXMLUtil::GetNodeValue()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);

	IXMLDOMElementPtr XMLElement;
	XMLElement = (IXMLDOMNodePtr)pXML;
	
	//return XMLElement->GetnodeValue();
	return XMLElement->Gettext();
}


PXML	CXMLUtil::GetChildXML(TCHAR* szItem)
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);
	
	IXMLDOMElementPtr XMLElement;
	XMLElement = (IXMLDOMNodePtr)pXML;

	try{
		IXMLDOMNodeListPtr	XMLNodeList;
		XMLNodeList		= XMLElement->getElementsByTagName(szItem);
		IXMLDOMNode* pXMLNode;
		XMLNodeList->get_item(0, &pXMLNode);
		return (PXML)pXMLNode;
	}catch(...){
		TRACE(_T("XML Fail Get Child XML %s\n"), szItem);
	}

	return NULL;
}

PXML	CXMLUtil::GetFirstChildXML()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);
	
	IXMLDOMElementPtr XMLElement;
	//XMLElement = (IXMLDOMElementPtr)pXML;
	
	XMLElement = pXML->firstChild;
	if(XMLElement){
		IXMLDOMElement* pXMLElement;
		XMLElement->QueryInterface(__uuidof(IXMLDOMElement), (void**)&pXMLElement);
		return pXMLElement;
	}

	return NULL;
}

PXML	CXMLUtil::GetNextChild(PXML pPrevChild)
{
	if(!pPrevChild){
		TRACE(_T("PrevChild not assigned\n"));
		return NULL;
	}

	IXMLDOMNode* pXML = (IXMLDOMElement*)pPrevChild;

	IXMLDOMElementPtr XMLElement;
	XMLElement = pXML->nextSibling;
	if(XMLElement){
		IXMLDOMElement* pXMLElement;
		XMLElement->QueryInterface(__uuidof(IXMLDOMElement), (void**)&pXMLElement);
		return pXMLElement;
	}

	return NULL;
}


TCHAR*	CXMLUtil::GetXMLData()
{
	CHECK_XML_RETURN_AND_ASSIGN(NULL);
	return pXML->xml;
}



DWORD	CXMLUtil::GetAttrValueHex(TCHAR* attrName, DWORD DefulatValue)
{
	CHECK_XML_RETURN_AND_ASSIGN(DefulatValue);
	return ::GetAttrValueHex(pXML, attrName, DefulatValue);
}

long	CXMLUtil::GetAttrValueNum(TCHAR* attrName, long DefulatValue)
{
	CHECK_XML_RETURN_AND_ASSIGN(DefulatValue);
	return ::GetAttrValueNum(pXML, attrName, DefulatValue);
}

double	CXMLUtil::GetAttrValueFloat(TCHAR* attrName, double DefulatValue)
{
	CHECK_XML_RETURN_AND_ASSIGN(DefulatValue);
	return ::GetAttrValueFloat(pXML, attrName, DefulatValue);
}

BOOL	CXMLUtil::GetAttrValueString(TCHAR* attrName, TCHAR* buffer, DWORD count)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	return ::GetAttrValueString(pXML, attrName, buffer, count);
}

BOOL	CXMLUtil::GetAttrValueBinData(TCHAR* attrName, int count, TCHAR* data)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::GetAttrValueBinData(pXML, attrName, count, data);
	return TRUE;
}

BOOL	CXMLUtil::SetAttrValueHex(TCHAR* attrName,DWORD v)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueHex(pXML, attrName, v);
	return TRUE;
}

BOOL	CXMLUtil::SetAttrValueNum(TCHAR* attrName,long v)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueNum(pXML, attrName, v);
	return TRUE;
}

BOOL	CXMLUtil::SetAttrValueFloat(TCHAR* attrName,double v)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueFloat(pXML, attrName, v);
	return TRUE;
}

BOOL	CXMLUtil::SetAttrValueString(TCHAR* attrName,TCHAR* buffer)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueString(pXML, attrName, buffer);
	return TRUE;
}

BOOL	CXMLUtil::SetAttrValueBinData(TCHAR* attrName,int count, TCHAR* data)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	::SetAttrValueBinData(pXML, attrName, count, data);
	return TRUE;
}


PXML	CXMLUtil::CreateElement(TCHAR* szTagName)
{
	IXMLDOMDocument2Ptr	XMLDocPtr;
	IXMLDOMElementPtr ChildXML;

	if(m_pXMLDoc){
		XMLDocPtr = (IXMLDOMDocument2 *)m_pXMLDoc;
	}else{
		CHECK_XML_RETURN_AND_ASSIGN(0);
		IXMLDOMElementPtr XMLElement;
		XMLElement = (IXMLDOMNodePtr)pXML;
		XMLDocPtr = XMLElement->GetownerDocument();
	}

	if(!XMLDocPtr) return NULL;

	ChildXML = XMLDocPtr->createElement(szTagName);

	IXMLDOMElement* pNewElement;
	ChildXML->QueryInterface(__uuidof(IXMLDOMElement), (void**)&pNewElement);
	return pNewElement;
}


BOOL	CXMLUtil::AppendChild(PXML pChildXML)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	pXML->appendChild((IXMLDOMNode*)pChildXML);

	return TRUE;
}


BOOL	CXMLUtil::RemoveChild(PXML pChildXML)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	pXML->removeChild((IXMLDOMNode*)pChildXML);

	return TRUE;
}


BOOL	CXMLUtil::ReplaceChild(PXML pNewChildXML, PXML pOldChildXML)
{
	CHECK_XML_RETURN_AND_ASSIGN(0);
	pXML->replaceChild((IXMLDOMNode*)pNewChildXML, (IXMLDOMNode*)pOldChildXML);

	return TRUE;
}
