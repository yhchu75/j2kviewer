#pragma once


// CTraceEdit

class CTraceEdit : public CEdit
{
	DECLARE_DYNAMIC(CTraceEdit)

public:
	CTraceEdit();
	virtual ~CTraceEdit();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
};


