#pragma once

typedef struct PointItem
{
	unsigned int category = 0;
	CString id = L"";
	CString name = L"";
	int level = 0;
	CSize size;
	CPoint point;
	unsigned int order = 0;
	int depth = -1;
	CString path = L"";
	CString exp = L"";
	CString navi = L"";
	int count = 1;
	int ratio = 0;
	int total = 1;
} POINTITEM;
