#pragma once


// CItemGroup

class CItemGroup : public CButton
{
	DECLARE_DYNAMIC(CItemGroup)

public:
	CItemGroup();
	virtual ~CItemGroup();

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
};


