#pragma once

#include "_PointItem.h"

// CItemButton

class CItemButton : public CStatic
{
	DECLARE_DYNAMIC(CItemButton)

public:
	CItemButton();
	virtual ~CItemButton();

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();

	POINTITEM m_tPointItem;
	CArray<COLORREF> m_arrButtonColor;
public:
	void SetData(int, POINTITEM);
	POINTITEM GetData() {
		return m_tPointItem;
	};
	COLORREF BackgroundColor();
	afx_msg void OnStnClicked();
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
private:
	CString m_strButtonCaption;
};


