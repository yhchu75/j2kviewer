// PointCounting.h : main header file for the PointCounting DLL
//

#pragma once

#ifndef __AFXWIN_H__
#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "afxwin.h"
#include "resource.h"		// main symbols
#include "../ToolPlusgIn.h"

typedef enum { THREAD, START, MAIN, CONTENT } PAGE_STEP;

// CPointCountingApp
// See PointCounting.cpp for the implementation of this class
//

class CPointCounterApp : public CWinApp
{
public:
	CPointCounterApp();

	// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};


class CPointCounterToolPlugIn;
class CPointCounterBaseDlg : public CDialog
{
public:
	CPointCounterBaseDlg() :m_bActivaed(FALSE){}

	virtual void Setup(CPointCounterToolPlugIn *pPointCounterToolPlugIn, IVPViewerCore *pVPViewerCore)
	{
		m_pPointCounterToolPlugIn = pPointCounterToolPlugIn;
		m_pVPViewerCore = pVPViewerCore;
	}
	void CPointCounterBaseDlg::OnShowWindow(BOOL bShow, UINT nStatus)
	{
		CDialog::OnShowWindow(bShow, nStatus);
		m_bActivaed = bShow;
	}
	void CPointCounterBaseDlg::OnClose()
	{
		CDialog::OnClose();
		m_bActivaed = FALSE;
	}
	virtual BOOL	IsActive(){ return m_bActivaed; }
	virtual BOOL	OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point){ return FALSE; }
	virtual BOOL	OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point){ return FALSE; }
	virtual BOOL	OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags){ return FALSE; }

	virtual int StartPage() = 0;

protected:
	CPointCounterToolPlugIn *m_pPointCounterToolPlugIn;
	IVPViewerCore *	m_pVPViewerCore;
	BOOL			m_bActivaed;
};

class CPointCounterDlg;
class CPointCounterToolPlugIn : public IToolPlugIn
{
public:
	const char* GetPlugInsToolName(){ return "Point Counter"; }

	CPointCounterToolPlugIn();
	~CPointCounterToolPlugIn();

	void	Setup(IVPViewerCore *pVPViewerCore);
	BOOL	IsActive(){
		if (m_pCurDlg) return m_pCurDlg->IsActive();
		else return FALSE;
		//return TRUE;
	}

	BOOL	OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point){
		if (m_bInstance == TRUE)
		{
			if (m_pCurDlg->IsWindowVisible() == TRUE)
				return m_pCurDlg->OnLButtonDown(hWnd, nFlags, point);
		}

		return FALSE;
	}
	BOOL	OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point){
		if (m_bInstance == TRUE)
		{
			if (m_pCurDlg->IsWindowVisible() == TRUE)
				return m_pCurDlg->OnLButtonMove(hWnd, nFlags, point);
		}

		return FALSE;
	}
	BOOL	OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags){
		if (m_bInstance == TRUE)
		{
			if (m_pCurDlg->IsWindowVisible() == TRUE)
				return m_pCurDlg->OnKeyUp(hWnd, nChar, nRepCnt, nFlags);
		}

		return FALSE;
	}

	HWND	Show(HWND hWnd, BOOL bShow);
	void	DestorySelf(){ delete this; }

	void	NextPage();
	void	EndupProcess(BOOL bCancel = FALSE);

	int*	GetCounterArray(){ return m_pCounter_array; }
	void	SetCounterArray(int *ca){ m_pCounter_array = ca; }
	POINT*	GetSize(){ return m_pSize; }
	POINT*	GetPosition(){ return m_pPosition; }
	POINT*	GetCurrentPosition(){ return m_pCurrent; }

private:
	BOOL	m_bInstance;
	CPointCounterBaseDlg* m_pCurDlg;
	CPointCounterDlg* m_pMainDlg;
	IVPViewerCore* m_pViewerCore;

	int*	m_pCounter_array;
	POINT*	m_pSize;
	POINT*	m_pPosition;
	POINT*	m_pCurrent;
};
