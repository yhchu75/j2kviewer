#pragma once

#include "../../VPviewer/IVPViewerCore.h"
#include "PointCounter.h"
#include "afxwin.h"
#include "TraceEdit.h"
#include "PageGroup.h"
#include "gXMLUtils.h"
//#include "RaipidXmlHelper.h"
#include "NavigationLabel.h"
#include "_PointItem.h"
#include "_TraceOutput.h"

//#include "ProgressDialog.h"

// CPointCountingDlg dialog
#define DIALOG_WIDTH 880
#define DIALOG_HEIGHT 840
#define CLIENT_PADDING 20
#define PAGE_PADDING 10
#define LEFT_WIDTH 200
#define RIGHT_WIDTH 150
#define NAVIGATION_HEIGHT 22

class CPointCounterDlg : public CPointCounterBaseDlg
{
	DECLARE_DYNAMIC(CPointCounterDlg)

public:
	CPointCounterDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPointCounterDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_MAIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	CString m_strAppPath = L"";
	//CString m_strProfilePath = L"";

	CFont *m_pGroupFont = nullptr;
	CFont *m_pNormalFont = nullptr;
	CFont *m_pNaviFont = nullptr;
	CFont *m_pMenuFont = nullptr;
	CFont *m_pTitleFont = nullptr;

	int m_nPointCount = 0;
	PAGE_STEP m_ePageStep = PAGE_STEP::START;
	int m_nHeightBuffer = 0;
	CStatic *m_pContentTitle = nullptr;
	CPageGroup *m_pContentPage = nullptr;
	int m_nNaviationLeft = 0;
	//CArray<POINTITEM> m_arrNavigationItem;
	CArray<CNavigationLabel*> m_arrNavigationLabel;
	CArray<POINTITEM> m_arrLastData;
	CArray<POINTITEM> m_arrResultData;
	//CArray<POINTITEM> m_arrProfileData;
	CArray<POINTITEM> m_arrExcelData;
	CArray<TRACEOUTPUT> m_arrOutputData;
private:
	POINT	position;
	POINT	size;
	POINT	current_position;
	POINT	privious_position;
	int*	counter_array;		//initial : 0 , counted : 1, counting : -1
	int*	counter_array_buffer;	//copy from counter_array
	int		counted;
	int		current_val;

	void redraw();
	void jump(POINT p);
	void next(bool skip, bool end = true);
	void resize();
	void tophwnd();
	HWND top_hwnd;
private:
	int StatusPanel();
	int OutputPanel();
	int MenuPanel();

	unsigned int AddControl(unsigned int, CFont*, CRect);
	CSize TextSize(CFont*, CString);

	void InitPage();
	void UndoNavigation();
	void ResetNavigation();
	POINTITEM AddNavigation(CString);
	POINTITEM AddNavigation(POINTITEM, BOOL display_label = TRUE);
	int MainXml(CPageGroup*);
	int PageXml(int, CPageGroup*, POINTITEM);
	BOOL DeletePage();
	BOOL EnableButtons(PAGE_STEP);

	//LPCWSTR XmlGetAttr(CRaipidXmlHelper, CRaipidXmlHelper::HANDLE_NODE, CString);
	unsigned int CountItem(CString);
	//POINTITEM GetItem(CString);

	BOOL IsProfile(POINTITEM source_item);
	void ProfileData();
	//void ProfileData(POINTITEM source_item);
	int SaveResult();

	void EnableMenu();
	void DisableMenu();

	int GetCountChar(CString Ps_Str, TCHAR Pc_Char);
	CString* Split(CString Ps_Str, TCHAR Pc_Separator, int& Ri_Length);

	CWinThread *xml_thread = NULL;

	//int InitXml();
public:
	CStatic m_ctlStatusScaleLabel;
	CStatic m_ctlStatusTotalLabel;
	CStatic m_ctlStatusPositionLabel;
	CStatic m_ctlStatusScoredLabel;
	CTraceEdit m_ctlOutputTraceEdit;
	CPageGroup m_ctlStartPageGroup;
	CPageGroup m_ctlMainPageGroup;
	CEdit m_ctlCellRowEdit;
	CEdit m_ctlCellColumnEdit;
	CEdit m_ctlLocationXEdit;
	CEdit m_ctlLocationYEdit;
	CButton m_ctlStartPageButton;
	CButton m_ctlLoadProfileButton;
	CButton m_ctlLoadTraceButton;
	CButton m_ctlSaveTraceButton;
	CButton m_ctlSaveResultButton;
	CButton m_ctlRestartCountButton;
	CButton m_ctlSkipCountButton;
	CButton m_ctlUndoCountButton;
	CButton m_ctlBackStepButton;
	CButton m_ctlMoveToButton;
	CButton m_ctlSameLastButton;
	CEdit m_ctlPosRowEdit;
	CEdit m_ctlPosColumnEdit;

	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	int OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	int OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);

	// NUMPAD
	// ============================================================================
	int OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags);
	// ============================================================================

	void CountPosition();
	void CloseGrid();
	void ShowGrid();
	virtual int StartPage();
	int MainPage();
	int GoPage(POINTITEM);
	void ReverseNavigation(POINTITEM);
	void GenerateExcelData();
	CString AddItem(POINTITEM);
	void AddOutput(POINTITEM);
	void AddOutput(POINTITEM, CString);
	afx_msg void OnBnClickedMenuSkipButton();
	afx_msg void OnBnClickedMenuUndoButton();
	afx_msg void OnBnClickedMenuMoveToButton();
	afx_msg void OnBnClickedMenuBackStepButton();
	afx_msg void OnBnClickedMenuRestartButton();
	afx_msg void OnBnClickedMenuLoadProfileButton();
	afx_msg void OnBnClickedMenuLoadTraceButton();
	afx_msg void OnBnClickedMenuSaveTraceButton();
	afx_msg void OnBnClickedStatusCrosshairOnRadio();
	afx_msg void OnBnClickedStatusCrosshairOffRadio();

	bool	LoadTrace(const TCHAR *szFile);
	bool	SaveTrace(const TCHAR *szFile);
	void	InsertHeader(FILE *fp);
	void	InsertTale(FILE *fp);
	void	InsertGRID(FILE *fp);

	void InsertPath(FILE* fp);
	bool IsSkip(CPoint target_pos);
	//void LoadData(const TCHAR* szFile);
	void LoadData(IXMLDOMDocument2Ptr pXMLDoc);

	afx_msg void OnBnClickedMenuSameAsLastButton();
	afx_msg void OnBnClickedMenuSaveResultButton();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnClose();

	static UINT ThreadFirst(LPVOID _mothod);

	void ProfileData(POINTITEM source_item);
	POINTITEM GetItem(CString);
	CString m_strProfilePath = L"";
	TCHAR *szProfilePath = L"";
	IXMLDOMDocument2Ptr pXMLDoc;
	CArray<POINTITEM> m_arrProfileData;
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnEnChangeEdit();

	void OnBnClickedButtonEndanalysis();
	CStatic m_ctlStartTotalValue;
	afx_msg void OnBnClickedCrosshairButton();
	CButton m_ctlCrosshairButton;

	int SaveResult(CString save_path);

	CString m_strPath;
	CString m_strExcelPath;
	void SaveImage(CString);
	int m_nPrcMax = 0;
	void GetMax(int);
	void Endup();
};
