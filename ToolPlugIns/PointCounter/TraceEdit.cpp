// TraceEdit.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounter.h"
#include "TraceEdit.h"


// CTraceEdit

IMPLEMENT_DYNAMIC(CTraceEdit, CEdit)

CTraceEdit::CTraceEdit()
{

}

CTraceEdit::~CTraceEdit()
{
}


BEGIN_MESSAGE_MAP(CTraceEdit, CEdit)
	ON_WM_ERASEBKGND()
	ON_WM_ACTIVATE()
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()



// CTraceEdit message handlers




BOOL CTraceEdit::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	CRect client_rect;
	GetClientRect(&client_rect);
	pDC->FillSolidRect(&client_rect, RGB(255, 255, 255));

	return TRUE;
	//return CEdit::OnEraseBkgnd(pDC);
}


void CTraceEdit::OnKillFocus(CWnd* pNewWnd)
{
	//CEdit::OnKillFocus(pNewWnd);

	// TODO: Add your message handler code here
	HideCaret();
}


void CTraceEdit::OnSetFocus(CWnd* pOldWnd)
{
	//CEdit::OnSetFocus(pOldWnd);

	// TODO: Add your message handler code here
	SendMessage(WM_KILLFOCUS, NULL);
}
