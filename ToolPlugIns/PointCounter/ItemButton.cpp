// ItemButton.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounter.h"
#include "ItemButton.h"
#include "PointCounterDlg.h"
#include "PageGroup.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CItemButton

IMPLEMENT_DYNAMIC(CItemButton, CStatic)

CItemButton::CItemButton()
{

}

CItemButton::~CItemButton()
{
}


BEGIN_MESSAGE_MAP(CItemButton, CStatic)
	ON_CONTROL_REFLECT(STN_CLICKED, &CItemButton::OnStnClicked)
	ON_WM_PAINT()
	ON_WM_CREATE()
END_MESSAGE_MAP()



// CItemButton message handlers




void CItemButton::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;

	CStatic::PostNcDestroy();
}


void CItemButton::OnStnClicked()
{
	// TODO: Add your control notification handler code here
	CPageGroup *current_page = (CPageGroup *)GetParent();
	CPointCounterDlg *main_dialog = (CPointCounterDlg *)current_page->GetParent();
	main_dialog->GoPage(m_tPointItem);
}


void CItemButton::SetData(int item_count, POINTITEM point_item)
{
	if (item_count > 0)
	{
		if (item_count >= 16)
		{
			m_arrButtonColor.Add(RGB(255, 69, 69));
			m_arrButtonColor.Add(RGB(255, 121, 121));
			m_arrButtonColor.Add(RGB(255, 222, 222));
			m_arrButtonColor.Add(RGB(71, 71, 255));
			m_arrButtonColor.Add(RGB(120, 120, 255));
			m_arrButtonColor.Add(RGB(173, 173, 255));
			m_arrButtonColor.Add(RGB(237, 72, 17));
			m_arrButtonColor.Add(RGB(245, 190, 21));
			m_arrButtonColor.Add(RGB(209, 255, 22));
			m_arrButtonColor.Add(RGB(11, 143, 143));
			m_arrButtonColor.Add(RGB(20, 255, 208));
			m_arrButtonColor.Add(RGB(174, 255, 214));
			m_arrButtonColor.Add(RGB(255, 124, 124));
			m_arrButtonColor.Add(RGB(255, 172, 255));
			m_arrButtonColor.Add(RGB(173, 173, 255));
			m_arrButtonColor.Add(RGB(21, 247, 78));
			m_arrButtonColor.Add(RGB(123, 255, 123));
			m_arrButtonColor.Add(RGB(175, 255, 175));
			m_arrButtonColor.Add(RGB(255, 255, 23));
			m_arrButtonColor.Add(RGB(255, 255, 124));
			m_arrButtonColor.Add(RGB(255, 255, 175));
			m_arrButtonColor.Add(RGB(201, 18, 201));
			m_arrButtonColor.Add(RGB(255, 72, 255));
			m_arrButtonColor.Add(RGB(255, 122, 255));
			m_arrButtonColor.Add(RGB(11, 142, 142));
			m_arrButtonColor.Add(RGB(70, 163, 255));
			m_arrButtonColor.Add(RGB(173, 173, 255));
			m_arrButtonColor.Add(RGB(92, 92, 92));
			m_arrButtonColor.Add(RGB(194, 194, 194));
			m_arrButtonColor.Add(RGB(245, 245, 245));
		}
		else if (item_count >= 8)
		{
			m_arrButtonColor.Add(RGB(255, 18, 18));
			m_arrButtonColor.Add(RGB(255, 121, 121));
			m_arrButtonColor.Add(RGB(76, 243, 20));
			m_arrButtonColor.Add(RGB(120, 255, 120));
			m_arrButtonColor.Add(RGB(24, 70, 255));
			m_arrButtonColor.Add(RGB(72, 72, 255));
			m_arrButtonColor.Add(RGB(13, 148, 148));
			m_arrButtonColor.Add(RGB(74, 164, 255));
			m_arrButtonColor.Add(RGB(150, 150, 13));
			m_arrButtonColor.Add(RGB(243, 243, 20));
			m_arrButtonColor.Add(RGB(18, 199, 199));
			m_arrButtonColor.Add(RGB(22, 255, 209));
			m_arrButtonColor.Add(RGB(100, 100, 100));
			m_arrButtonColor.Add(RGB(138, 138, 138));
		}
		else
		{
			m_arrButtonColor.Add(RGB(72, 118, 255));
			m_arrButtonColor.Add(RGB(72, 164, 255));
			m_arrButtonColor.Add(RGB(71, 209, 255));
			m_arrButtonColor.Add(RGB(124, 255, 255));
			m_arrButtonColor.Add(RGB(173, 255, 255));
			m_arrButtonColor.Add(RGB(224, 255, 255));
			m_arrButtonColor.Add(RGB(224, 226, 231));
		}
	}
	else
	{
		m_arrButtonColor.Add(RGB(255, 74, 74));
		m_arrButtonColor.Add(RGB(255, 153, 74));
		m_arrButtonColor.Add(RGB(255, 255, 101));
		m_arrButtonColor.Add(RGB(255, 139, 196));
		m_arrButtonColor.Add(RGB(178, 178, 255));
		m_arrButtonColor.Add(RGB(255, 113, 35));
		m_arrButtonColor.Add(RGB(176, 255, 155));
	}

	m_tPointItem = point_item;
	//m_arrButtonColor.GetAt(color_order);
}


COLORREF CItemButton::BackgroundColor()
{
	unsigned int color_order = m_tPointItem.order % m_arrButtonColor.GetSize();
	return m_arrButtonColor.GetAt(color_order);
}


void CItemButton::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CStatic::OnPaint() for painting messages

	this->SetWindowTextW(m_strButtonCaption);
}


int CItemButton::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CStatic::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	this->GetWindowTextW(m_strButtonCaption);

	return 0;
}
