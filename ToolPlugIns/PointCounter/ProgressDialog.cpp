// ProgressDialog.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounter.h"
#include "ProgressDialog.h"
#include "afxdialogex.h"


// CProgressDialog dialog

IMPLEMENT_DYNAMIC(CProgressDialog, CDialogEx)

CProgressDialog::CProgressDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CProgressDialog::IDD, pParent)
{

}

CProgressDialog::~CProgressDialog()
{
}

void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, m_ctlProgressBar);
}


BEGIN_MESSAGE_MAP(CProgressDialog, CDialogEx)
END_MESSAGE_MAP()


// CProgressDialog message handlers


void CProgressDialog::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;

	CDialogEx::PostNcDestroy();
}

void CProgressDialog::PrcType(int prc_type)
{
	m_nPrcType = prc_type;
}

void CProgressDialog::ProgressPos()
{
	m_nPrcCount++;
	double prc_per = m_nPrcCount * 100 / m_PCDlg->m_nPrcMax;
	m_ctlProgressBar.SetPos(ceil(prc_per));
	UpdateData(FALSE);
}

void CProgressDialog::ProgressPos(int progress_pos)
{
	m_ctlProgressBar.SetPos(progress_pos);
	UpdateData(FALSE);
}

void CProgressDialog::InitProgress()
{
	m_ctlProgressBar.SetRange(0, 100);
	m_ctlProgressBar.SetPos(0);
	m_nPrcCount = 0;
}

void CProgressDialog::StartProgress()
{
	m_PCDlg->GetMax(m_nPrcType);

	switch (m_nPrcType)
	{
	case 1:
	case 3:
	case 5:
	case 7:
		m_PCDlg->SaveTrace(m_PCDlg->m_strPath);
		break;
	}

	switch (m_nPrcType)
	{
	case 2:
	case 3:
	case 6:
	case 7:
		m_PCDlg->SaveResult(m_PCDlg->m_strExcelPath);
		break;
	}

	switch (m_nPrcType)
	{
	case 4:
	case 5:
	case 6:
	case 7:
		m_PCDlg->SaveImage(m_PCDlg->m_strPath);
		break;
	}

	switch (m_nPrcType)
	{
	case 7:
		AfxMessageBox(L"Counting results saved.");
		break;
	}

	m_PCDlg->EndModalState();
	this->ShowWindow(SW_HIDE);

	switch (m_nPrcType)
	{
	case 7:
		m_PCDlg->Endup();
		break;
	}
}
