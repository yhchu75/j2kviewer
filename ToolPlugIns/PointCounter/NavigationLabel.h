#pragma once

#include "_PointItem.h"

// CNavigationLabel

class CNavigationLabel : public CStatic
{
	DECLARE_DYNAMIC(CNavigationLabel)

public:
	CNavigationLabel();
	virtual ~CNavigationLabel();

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
protected:
	POINTITEM m_tPointItem;
public:
	afx_msg void OnStnClicked();

	void SetData(POINTITEM);
	POINTITEM GetData();
};


