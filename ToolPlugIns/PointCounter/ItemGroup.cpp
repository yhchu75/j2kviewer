// ItemGroup.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounter.h"
#include "ItemGroup.h"


// CItemGroup

IMPLEMENT_DYNAMIC(CItemGroup, CButton)

CItemGroup::CItemGroup()
{

}

CItemGroup::~CItemGroup()
{
}


BEGIN_MESSAGE_MAP(CItemGroup, CButton)
END_MESSAGE_MAP()



// CItemGroup message handlers




void CItemGroup::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;

	CButton::PostNcDestroy();
}
