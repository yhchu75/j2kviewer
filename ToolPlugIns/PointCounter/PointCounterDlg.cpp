// PointCountingDlg.cpp : implementation file
//

#include "stdafx.h"

#include <algorithm>

#include "PointCounterDlg.h"
#include "ItemGroup.h"
//#include "gXMLUtils.h"

//#include "tinyxml2.h"

//#include "xlslib.h"
//#include "BasicExcel.hpp"
#include "ProgressDialog.h"
#include "libxl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//using namespace xlslib_core;
//using namespace YExcel;
//using namespace libxl;

// CPointCountingDlg dialog

CProgressDialog *m_ProgressDlg = NULL;

IMPLEMENT_DYNAMIC(CPointCounterDlg, CDialog)

CPointCounterDlg::CPointCounterDlg(CWnd* pParent /*=NULL*/)
{
	current_position.x = 1;
	current_position.y = 1;
	privious_position.x = 1;
	privious_position.y = 1;
	counted = 0;
	counter_array = nullptr;
	current_val = 0;
	//isLoaded = false;
}

CPointCounterDlg::~CPointCounterDlg()
{
	delete m_pGroupFont;
	delete m_pNormalFont;
	delete m_pNaviFont;
	delete m_pMenuFont;
	delete m_pTitleFont;

	DeletePage();
}

void CPointCounterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_START_GROUP, m_ctlStartPageGroup);
	DDX_Control(pDX, IDC_MAIN_GROUP, m_ctlMainPageGroup);
	DDX_Control(pDX, IDC_START_START_BUTTON, m_ctlStartPageButton);
	DDX_Control(pDX, IDC_START_ROW_EDIT, m_ctlCellRowEdit);
	DDX_Control(pDX, IDC_START_COLUMN_EDIT, m_ctlCellColumnEdit);
	DDX_Control(pDX, IDC_START_LOCATION_X_EDIT, m_ctlLocationXEdit);
	DDX_Control(pDX, IDC_START_LOCATION_Y_EDIT, m_ctlLocationYEdit);
	DDX_Control(pDX, IDC_MENU_ROW_EDIT, m_ctlPosRowEdit);
	DDX_Control(pDX, IDC_MENU_COL_EDIT, m_ctlPosColumnEdit);
	DDX_Control(pDX, IDC_MENU_LOAD_PROFILE_BUTTON, m_ctlLoadProfileButton);
	DDX_Control(pDX, IDC_MENU_LOAD_TRACE_BUTTON, m_ctlLoadTraceButton);
	DDX_Control(pDX, IDC_MENU_SAVE_TRACE_BUTTON, m_ctlSaveTraceButton);
	DDX_Control(pDX, IDC_MENU_SAVE_RESULT_BUTTON, m_ctlSaveResultButton);
	DDX_Control(pDX, IDC_MENU_RESTART_BUTTON, m_ctlRestartCountButton);
	DDX_Control(pDX, IDC_MENU_SKIP_BUTTON, m_ctlSkipCountButton);
	DDX_Control(pDX, IDC_MENU_UNDO_BUTTON, m_ctlUndoCountButton);
	DDX_Control(pDX, IDC_MENU_BACK_STEP_BUTTON, m_ctlBackStepButton);
	DDX_Control(pDX, IDC_MENU_MOVE_TO_BUTTON, m_ctlMoveToButton);
	DDX_Control(pDX, IDC_MENU_SAME_AS_LAST_BUTTON, m_ctlSameLastButton);
	DDX_Control(pDX, IDC_STATUS_SCALE_VALUE_LABEL, m_ctlStatusScaleLabel);
	DDX_Control(pDX, IDC_STATUS_TOTAL_VALUE_LABEL, m_ctlStatusTotalLabel);
	DDX_Control(pDX, IDC_STATUS_POSITION_VALUE_LABEL, m_ctlStatusPositionLabel);
	DDX_Control(pDX, IDC_STATUS_SCORED_VALUE_LABEL, m_ctlStatusScoredLabel);
	DDX_Control(pDX, IDC_OUTPUT_EDIT, m_ctlOutputTraceEdit);
	DDX_Control(pDX, IDC_START_TOTAL_VALUE, m_ctlStartTotalValue);
	DDX_Control(pDX, IDC_CROSSHAIR_BUTTON, m_ctlCrosshairButton);
}


BEGIN_MESSAGE_MAP(CPointCounterDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_MENU_SKIP_BUTTON, &CPointCounterDlg::OnBnClickedMenuSkipButton)
	ON_BN_CLICKED(IDC_MENU_UNDO_BUTTON, &CPointCounterDlg::OnBnClickedMenuUndoButton)
	ON_BN_CLICKED(IDC_MENU_MOVE_TO_BUTTON, &CPointCounterDlg::OnBnClickedMenuMoveToButton)
	ON_BN_CLICKED(IDC_MENU_BACK_STEP_BUTTON, &CPointCounterDlg::OnBnClickedMenuBackStepButton)
	ON_BN_CLICKED(IDC_MENU_RESTART_BUTTON, &CPointCounterDlg::OnBnClickedMenuRestartButton)
	ON_BN_CLICKED(IDC_MENU_LOAD_PROFILE_BUTTON, &CPointCounterDlg::OnBnClickedMenuLoadProfileButton)
	ON_BN_CLICKED(IDC_MENU_LOAD_TRACE_BUTTON, &CPointCounterDlg::OnBnClickedMenuLoadTraceButton)
	ON_BN_CLICKED(IDC_MENU_SAVE_TRACE_BUTTON, &CPointCounterDlg::OnBnClickedMenuSaveTraceButton)
	ON_BN_CLICKED(IDC_STATUS_CROSSHAIR_ON_RADIO, &CPointCounterDlg::OnBnClickedStatusCrosshairOnRadio)
	ON_BN_CLICKED(IDC_STATUS_CROSSHAIR_OFF_RADIO, &CPointCounterDlg::OnBnClickedStatusCrosshairOffRadio)
	ON_BN_CLICKED(IDC_MENU_SAME_AS_LAST_BUTTON, &CPointCounterDlg::OnBnClickedMenuSameAsLastButton)
	ON_BN_CLICKED(IDC_MENU_SAVE_RESULT_BUTTON, &CPointCounterDlg::OnBnClickedMenuSaveResultButton)
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_EN_CHANGE(IDC_START_ROW_EDIT, &CPointCounterDlg::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_START_COLUMN_EDIT, &CPointCounterDlg::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_START_LOCATION_X_EDIT, &CPointCounterDlg::OnEnChangeEdit)
	ON_EN_CHANGE(IDC_START_LOCATION_Y_EDIT, &CPointCounterDlg::OnEnChangeEdit)
	ON_BN_CLICKED(IDC_CROSSHAIR_BUTTON, &CPointCounterDlg::OnBnClickedCrosshairButton)
END_MESSAGE_MAP()


// CPointCountingDlg message handlers
HBRUSH CPointCounterDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CPointCounterBaseDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	switch (pWnd->GetDlgCtrlID())
	{
	case IDC_STATUS_LABEL:
	case IDC_STATUS_SCALE_LABEL:
	case IDC_STATUS_CROSSHAIR_LABEL:
	case IDC_STATUS_CROSSHAIR_LABEL_2:
	case IDC_STATUS_TOTAL_LABEL:
	case IDC_STATUS_POSITION_LABEL:
	case IDC_STATUS_SCORED_LABEL:
		pDC->SetTextColor(RGB(0, 0, 0));
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	case IDC_OUTPUT_EDIT:
		pDC->SetBkColor(RGB(255, 255, 255));
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}

BOOL CPointCounterDlg::OnInitDialog()
{
	CPointCounterBaseDlg::OnInitDialog();

	// TODO:  Add extra initialization here
	SetIcon(NULL, FALSE);

	CRect dialog_rect;
	GetWindowRect(dialog_rect);

	MoveWindow(CRect(dialog_rect.left, dialog_rect.top, dialog_rect.left + DIALOG_WIDTH, dialog_rect.top + DIALOG_HEIGHT));

	m_pGroupFont = new CFont;
	m_pNormalFont = new CFont;
	m_pNaviFont = new CFont;
	m_pMenuFont = new CFont;
	m_pTitleFont = new CFont;

	m_pGroupFont->CreatePointFont(160, L"Arial");
	m_pNormalFont->CreatePointFont(85, L"Arial");
	m_pNaviFont->CreatePointFont(90, L"Arial");
	m_pMenuFont->CreatePointFont(100, L"Arial");
	m_pTitleFont->CreatePointFont(200, L"Arial");

	StatusPanel();
	OutputPanel();
	MenuPanel();

	m_ctlOutputTraceEdit.HideCaret();
	m_ctlOutputTraceEdit.SetReadOnly();

	//InitXml();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPointCounterDlg::CountPosition()
{
	m_pVPViewerCore->GetGridBox(position, size);
	counter_array = new int[size.x * size.y];
	counter_array_buffer = new int[size.x * size.y];
	memset(counter_array, 0, sizeof(int)*size.x*size.y);
	memcpy(counter_array_buffer, counter_array, sizeof(int)*size.x*size.y);
	counter_array[0] = -1;
	current_val = 0;
	m_pVPViewerCore->SetGridBoxArray(counter_array);
	redraw();
	jump(current_position);
}

void CPointCounterDlg::redraw()
{
	CString buf;
	buf.Format(L"%d (%d x %d)", size.y * size.x, size.y, size.x);
	m_ctlStatusTotalLabel.SetWindowTextW(buf);
	buf.Format(L"%d x %d (row x col)", current_position.y, current_position.x);
	m_ctlStatusPositionLabel.SetWindowTextW(buf);
	buf.Format(L"%d / %d", counted, size.x * size.y);
	m_ctlStatusScoredLabel.SetWindowTextW(buf);

	if (m_pVPViewerCore->GetScale() < 1.001 && m_pVPViewerCore->GetScale() > 0.999)
		buf.Format(L"%.2f pixel/pixel", m_pVPViewerCore->GetScale());
	else
		buf.Format(L"%.2f um/pixel", m_pVPViewerCore->GetScale());

	m_ctlStatusScaleLabel.SetWindowTextW(buf);
}

void CPointCounterDlg::tophwnd()
{
	//HWND ret_hwnd = NULL;

	if (top_hwnd == NULL)
	{
		ULONG idProc;
		DWORD TempCurrProcId = GetCurrentProcessId();
		top_hwnd = ::FindWindow(NULL, NULL);
		while (top_hwnd != NULL)
		{
			if (::GetParent(top_hwnd) == NULL)
			{
				GetWindowThreadProcessId(top_hwnd, &idProc);
				if (TempCurrProcId == idProc){ break; }
			}
			top_hwnd = ::GetWindow(top_hwnd, GW_HWNDNEXT);
		}
	}
}

void CPointCounterDlg::resize()
{
	//AfxGetMainWnd()->OnSize(0, int cx, int cy);
	//::PostMessageW(AfxGetMainWnd()->GetSafeHwnd(), WM_SIZE, 0, 0);
	//HWND target_hwnd = tophwnd();

	if (top_hwnd)
	{
		CRect window_rect;
		::GetWindowRect(top_hwnd, window_rect);
		::MoveWindow(top_hwnd, window_rect.left, window_rect.top, window_rect.Width() - 1, window_rect.Height() - 1, TRUE);
		::MoveWindow(top_hwnd, window_rect.left, window_rect.top, window_rect.Width(), window_rect.Height(), TRUE);
		//::PostMessageW(target_hwnd, WM_SIZE, 0, 0);
	}
}

void CPointCounterDlg::jump(POINT p)
{
	//change current val
	counter_array[((current_position.y - 1) * size.x) + current_position.x - 1] = current_val;
	memcpy(counter_array_buffer, counter_array, sizeof(int)*size.x*size.y);
	TRACE(L"\n\njump : %d\n\n", ((p.y - 1) * size.x) + p.x - 1);
	current_val = counter_array[((p.y - 1) * size.x) + p.x - 1];
	//change position
	counter_array[((p.y - 1) * size.x) + p.x - 1] = -1;
	current_position.x = p.x;
	current_position.y = p.y;
	SetDlgItemInt(IDC_MENU_ROW_EDIT, p.y);
	SetDlgItemInt(IDC_MENU_COL_EDIT, p.x);
	TRACE(L"\n\njump : center pos start\n\n");
	m_pVPViewerCore->SetCenterPos(p);
	TRACE(L"\n\njump : center pos stop\n\n");
	redraw();
}

void CPointCounterDlg::next(bool skip, bool end)
{
	//::GetWindowRect();
	//::SendMessage(AfxGetMainWnd()->GetSafeHwnd(), WM_SIZE, 1, NULL);
	resize();

	POINT p;
	p.x = current_position.x;
	p.y = current_position.y;

	if (!skip)
	{
		if (current_val != 1)
		{
			current_val = 1;
			counted++;
		}
	}

	TRACE(L"size in next : %d, %d\n", size.x, size.y);

	if (p.x < size.x)
		p.x += 1;
	else if (p.y < size.y){
		p.x = 1;
		p.y += 1;
	}
	else{
		if (end){
			jump(p);
			m_pVPViewerCore->SetGridBoxArray(counter_array);
			OnBnClickedButtonEndanalysis();
			return;
		}
	}

	m_pVPViewerCore->SetGridBoxArray(counter_array);
	jump(p);
}

CString strClip(CString str, int nType)
{
	//파일 Full Path를 복사
	TCHAR szTmp[4096];
	StrCpy(szTmp, str);
	CString strTmp;
	CString strResult = _T("");
	switch (nType)
	{
	case 0:
		//파일의 경로만 복사.
		PathRemoveFileSpec(szTmp);
		strResult = szTmp;
		break;
	case 1:
		// 1: 파일 이름만 복사
		strResult = PathFindFileName(szTmp);
		//strResult = szTmp;
		break;
	case 2:
		// 2: 파일 확장자 복사
		strResult = PathFindExtension(szTmp);
		break;
	case 3:
		// 3: 확장자를 뺀 파일명 복사
		strTmp = PathFindFileName(szTmp);
		ZeroMemory(szTmp, 4096);
		StrCpy(szTmp, strTmp);
		PathRemoveExtension(szTmp);
		strResult = szTmp;
		break;
	case 4:
		// 4: 2번케이스의 파일 확장자에서 .을 뺌.
		strResult = PathFindExtension(szTmp);
		strResult = strResult.Right(strResult.GetLength() - 1);
		break;
	}

	return strResult;
}

void CPointCounterDlg::OnBnClickedButtonEndanalysis()
{
	m_pVPViewerCore->SetGridAnalyzeMode(false);

	//CString m_strPath;
	//CString m_strExcelPath;
	CString m_strFolder;
	CStdioFile file;
	// CFile file;
	CString strNow;

	SYSTEMTIME time;
	::ZeroMemory(reinterpret_cast<void*>(&time), sizeof(time));

	::GetLocalTime(&time);

	strNow.Format(_T("%4d-%02d-%02d %02d%02d%02d"), time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

	m_strPath.Format(L"PointCounting_%s_%s.xml", m_pVPViewerCore->GetFileName(), strNow);
	//m_strExcelPath.Format(L"PointCounting_%s_%s.xls", m_pVPViewerCore->GetFileName(), strNow);

	CFileDialog dlg(FALSE, _T("*.xml"), m_strPath, OFN_OVERWRITEPROMPT, _T("XML files(*.xml)|*.xml|"), NULL);

	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".xml")
		{
			m_strPath += ".xml";
		}

		m_strExcelPath = strClip(m_strPath, 0) + L"\\" + strClip(m_strPath, 3);
		m_strExcelPath += L".xls";

		/*SaveTrace(m_strPath);
		SaveResult(m_strExcelPath);
		SaveImage(m_strPath);*/

		if (m_ProgressDlg == NULL)
		{
			m_ProgressDlg = new CProgressDialog(this);
			m_ProgressDlg->Create(IDD_DIALOG_SAVE, this);
			m_ProgressDlg->m_PCDlg = this;
		}

		m_ProgressDlg->PrcType(7);
		m_ProgressDlg->InitProgress();
		m_ProgressDlg->ShowWindow(SW_SHOW);
		this->BeginModalState();
		m_ProgressDlg->StartProgress();
	}

	//m_pPointCounterToolPlugIn->EndupProcess(FALSE);
}

void CPointCounterDlg::SaveImage(CString save_path)
{
	CString m_strFolder = save_path.Left(save_path.GetLength() - 4);
	CreateDirectory(m_strFolder, NULL);

	for (int y = 0; y < size.y; y++)
	{
		for (int x = 0; x < size.x; x++)
		{
			CString ImagePath;
			ImagePath.Format(_T("%s\\%03d_%03d.bmp"), m_strFolder, y + 1, x + 1);

			POINT pt1;

			pt1.x = position.x + x;
			pt1.y = position.y + y;

			m_pVPViewerCore->CropGridImage(pt1, ImagePath);

			m_ProgressDlg->ProgressPos();
		}
	}

	CString ImagePath;
	ImagePath.Format(_T("%s\\overview.bmp"), m_strFolder);
	m_pVPViewerCore->CropGridImage(position, ImagePath, size.x, size.y);
}

void CPointCounterDlg::CloseGrid()
{
	//m_pVPViewerCore->CloseGrid();
}

void CPointCounterDlg::ShowGrid()
{
	m_pVPViewerCore->ShowGrid();
}

int CPointCounterDlg::StatusPanel()
{
	m_nHeightBuffer = 10;

	// 'status' label
	m_nHeightBuffer += AddControl(IDC_STATUS_LABEL, m_pGroupFont, CRect(CLIENT_PADDING, CLIENT_PADDING, CLIENT_PADDING + LEFT_WIDTH, 0));
	int height_buffer = m_nHeightBuffer;

	// 'scale' label
	m_nHeightBuffer += 30;
	AddControl(IDC_STATUS_SCALE_LABEL, m_pNormalFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + 85, 0));
	m_nHeightBuffer += AddControl(IDC_STATUS_SCALE_VALUE_LABEL, m_pNormalFont, CRect(CLIENT_PADDING + 95, m_nHeightBuffer, CLIENT_PADDING + 95 + 100, 0));
	m_ctlStatusScaleLabel.SetWindowTextW(L"-----");

	// 'crosshair' label
	m_nHeightBuffer += 10;
	AddControl(IDC_STATUS_CROSSHAIR_LABEL, m_pNormalFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + 85, 0));
	AddControl(IDC_CROSSHAIR_BUTTON, m_pNormalFont, CRect(CLIENT_PADDING + 95, m_nHeightBuffer, CLIENT_PADDING + 95 + 80, 0));
	m_nHeightBuffer += 20;
	AddControl(IDC_STATUS_CROSSHAIR_LABEL_2, m_pNormalFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + 85, 0));
	AddControl(IDC_STATUS_CROSSHAIR_ON_RADIO, m_pNormalFont, CRect(CLIENT_PADDING + 95, m_nHeightBuffer, CLIENT_PADDING + 95 + 40, 0));
	m_nHeightBuffer += AddControl(IDC_STATUS_CROSSHAIR_OFF_RADIO, m_pNormalFont, CRect(CLIENT_PADDING + 135, m_nHeightBuffer, CLIENT_PADDING + 135 + 40, 0));
	CButton *crosshair_on = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_ON_RADIO);
	crosshair_on->SetCheck(1);
	CButton *crosshair_off = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_OFF_RADIO);
	crosshair_off->SetCheck(0);

	// 'total' label
	m_nHeightBuffer += 10;
	AddControl(IDC_STATUS_TOTAL_LABEL, m_pNormalFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + 85, 0));
	m_nHeightBuffer += AddControl(IDC_STATUS_TOTAL_VALUE_LABEL, m_pNormalFont, CRect(CLIENT_PADDING + 95, m_nHeightBuffer, CLIENT_PADDING + 95 + 100, 0));
	m_ctlStatusTotalLabel.SetWindowTextW(L"-----");

	// 'position' label
	m_nHeightBuffer += 10;
	AddControl(IDC_STATUS_POSITION_LABEL, m_pNormalFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + 85, 0));
	m_nHeightBuffer += AddControl(IDC_STATUS_POSITION_VALUE_LABEL, m_pNormalFont, CRect(CLIENT_PADDING + 95, m_nHeightBuffer, CLIENT_PADDING + 95 + 100, 0));
	m_ctlStatusPositionLabel.SetWindowTextW(L"-----");

	// 'scored' label
	m_nHeightBuffer += 10;
	AddControl(IDC_STATUS_SCORED_LABEL, m_pNormalFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + 85, 0));
	m_nHeightBuffer += AddControl(IDC_STATUS_SCORED_VALUE_LABEL, m_pNormalFont, CRect(CLIENT_PADDING + 95, m_nHeightBuffer, CLIENT_PADDING + 95 + 100, 0));
	m_ctlStatusScoredLabel.SetWindowTextW(L"-----");

	// 'status' group
	m_nHeightBuffer += 10;
	GetDlgItem(IDC_GROUP_STATUS)->MoveWindow(CRect(CLIENT_PADDING, height_buffer + 10, CLIENT_PADDING + LEFT_WIDTH, m_nHeightBuffer));

	return 0;
}

int CPointCounterDlg::OutputPanel()
{
	CRect client_rect;
	GetClientRect(client_rect);

	// 'output' label
	m_nHeightBuffer += 30;
	m_nHeightBuffer += AddControl(IDC_OUTPUT_LABEL, m_pGroupFont, CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + LEFT_WIDTH, 0));

	// 'output' edit
	m_nHeightBuffer += 10;
	GetDlgItem(IDC_OUTPUT_EDIT)->ModifyStyleEx(0, WS_EX_STATICEDGE, 0);
	GetDlgItem(IDC_OUTPUT_EDIT)->SetFont(m_pNormalFont);
	GetDlgItem(IDC_OUTPUT_EDIT)->MoveWindow(CRect(CLIENT_PADDING, m_nHeightBuffer, CLIENT_PADDING + LEFT_WIDTH, client_rect.Height() - CLIENT_PADDING));

	return 0;
}

int CPointCounterDlg::MenuPanel()
{
	m_nHeightBuffer = 0;

	int button_height = 30;
	int edit_height = 20;
	CRect client_rect;
	GetClientRect(client_rect);

	// 'menu' label
	m_nHeightBuffer += CLIENT_PADDING;
	m_nHeightBuffer += AddControl(IDC_MENU_LABEL, m_pGroupFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, 0));

	// 'load profile' button
	m_nHeightBuffer += 20;
	m_nHeightBuffer += AddControl(IDC_MENU_LOAD_PROFILE_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'load trace' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_LOAD_TRACE_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'save trace' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_SAVE_TRACE_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'save result' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_SAVE_RESULT_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'restart' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_RESTART_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'skip' button
	m_nHeightBuffer += 10;
	AddControl(IDC_MENU_SKIP_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 - 5, m_nHeightBuffer + button_height));

	// 'undo' button
	m_nHeightBuffer += AddControl(IDC_MENU_UNDO_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 + 10, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'back step' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_BACK_STEP_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'move to' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_MOVE_TO_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	// 'row' edit
	m_nHeightBuffer += 10;
	AddControl(IDC_MENU_ROW_EDIT, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH + 15, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 - 30, m_nHeightBuffer + edit_height));

	// 'mul' label
	AddControl(IDC_MENU_MUL_LABEL, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 - 15, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 + 15, m_nHeightBuffer + edit_height));

	// 'col' edit
	m_nHeightBuffer += AddControl(IDC_MENU_COL_EDIT, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 + 30, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING - 15, m_nHeightBuffer + edit_height));

	// 'row' label
	m_nHeightBuffer += 10;
	AddControl(IDC_MENU_ROW_LABEL, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH + 15, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 - 30, 0));

	// 'col' label
	m_nHeightBuffer += AddControl(IDC_MENU_COL_LABEL, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH / 2 + 30, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING - 15, 0));

	// 'move to' button
	m_nHeightBuffer += 10;
	m_nHeightBuffer += AddControl(IDC_MENU_SAME_AS_LAST_BUTTON, m_pMenuFont, CRect(client_rect.Width() - CLIENT_PADDING - RIGHT_WIDTH, m_nHeightBuffer, client_rect.Width() - CLIENT_PADDING, m_nHeightBuffer + button_height));

	return 0;
}

void CPointCounterDlg::InitPage()
{
	if (PathFileExists(m_strProfilePath) == FALSE)
		CopyFile(m_strAppPath + "\\tools\\default.xml", m_strProfilePath, FALSE);
}

int CPointCounterDlg::StartPage()
{
	GetDlgItem(IDC_OUTPUT_EDIT)->SetWindowTextW(L"");

	current_position.x = current_position.y = 1;

	m_arrExcelData.RemoveAll();
	m_arrLastData.RemoveAll();
	m_arrNavigationLabel.RemoveAll();
	m_arrResultData.RemoveAll();
	m_arrOutputData.RemoveAll();

	InitPage();

	counted = 0;
	m_nHeightBuffer = 20;

	m_ctlStatusTotalLabel.SetWindowTextW(L"-----");
	m_ctlStatusPositionLabel.SetWindowTextW(L"-----");
	m_ctlStatusScoredLabel.SetWindowTextW(L"-----");

	int edit_height = 20;
	int button_height = 40;
	CRect client_rect, box_rect;
	GetClientRect(client_rect);

	m_ePageStep = PAGE_STEP::THREAD;
	DeletePage();
	EnableButtons(m_ePageStep);

	GetDlgItem(IDC_MAIN_GROUP)->ShowWindow(SW_HIDE);

	CPageGroup *start_group = (CPageGroup *)GetDlgItem(IDC_START_GROUP);
	int main_width = client_rect.Width() - CLIENT_PADDING * 4 - (LEFT_WIDTH + RIGHT_WIDTH);

	//box_rect.left = 0;
	//box_rect.right = main_width;

	start_group->SetWindowTextW(L"");
	start_group->MoveWindow(CRect(CLIENT_PADDING * 2 + LEFT_WIDTH, CLIENT_PADDING, client_rect.Width() - CLIENT_PADDING * 2 - RIGHT_WIDTH, client_rect.Height() - CLIENT_PADDING));
	start_group->ShowWindow(SW_SHOW);

	m_nHeightBuffer += start_group->AddControl(IDC_START_POINT_COUNTER_LABEL, m_pTitleFont, CRect(0, m_nHeightBuffer, main_width, 0));

	m_nHeightBuffer += 20;
	m_nHeightBuffer += start_group->AddControl(IDC_START_POINT_COUNTER_GUIDE_LABEL, m_pNormalFont, CRect(0, m_nHeightBuffer, main_width, 0));

	box_rect.top = m_nHeightBuffer + 10;

	m_nHeightBuffer += 30;
	start_group->AddControl(IDC_START_GRID_CELL_LABEL, m_pNormalFont, CRect(main_width / 2 - 150, m_nHeightBuffer, main_width / 2 - 30, 0));
	m_nHeightBuffer += start_group->AddControl(IDC_START_AREA_POSITION_LABEL, m_pNormalFont, CRect(main_width / 2 + 30, m_nHeightBuffer, main_width / 2 + 150, 0));

	m_nHeightBuffer += 20;
	start_group->AddControl(IDC_START_ROW_LABEL, m_pNormalFont, CRect(main_width / 2 - 180, m_nHeightBuffer, main_width / 2 - 85, m_nHeightBuffer + edit_height));
	start_group->AddControl(IDC_START_ROW_EDIT, m_pNormalFont, CRect(main_width / 2 - 80, m_nHeightBuffer, main_width / 2 - 30, m_nHeightBuffer + edit_height));
	start_group->AddControl(IDC_START_LOCATION_X_LABEL, m_pNormalFont, CRect(main_width / 2 - 30, m_nHeightBuffer, main_width / 2 + 65, m_nHeightBuffer + edit_height));
	m_nHeightBuffer += start_group->AddControl(IDC_START_LOCATION_X_EDIT, m_pNormalFont, CRect(main_width / 2 + 70, m_nHeightBuffer, main_width / 2 + 120, m_nHeightBuffer + edit_height));

	box_rect.left = main_width / 2 - 180 + 15;

	m_nHeightBuffer += 10;
	start_group->AddControl(IDC_START_COLUMN_LABEL, m_pNormalFont, CRect(main_width / 2 - 180, m_nHeightBuffer, main_width / 2 - 85, m_nHeightBuffer + edit_height));
	start_group->AddControl(IDC_START_COLUMN_EDIT, m_pNormalFont, CRect(main_width / 2 - 80, m_nHeightBuffer, main_width / 2 - 30, m_nHeightBuffer + edit_height));
	start_group->AddControl(IDC_START_LOCATION_Y_LABEL, m_pNormalFont, CRect(main_width / 2 - 30, m_nHeightBuffer, main_width / 2 + 65, m_nHeightBuffer + edit_height));
	m_nHeightBuffer += start_group->AddControl(IDC_START_LOCATION_Y_EDIT, m_pNormalFont, CRect(main_width / 2 + 70, m_nHeightBuffer, main_width / 2 + 120, m_nHeightBuffer + edit_height));

	box_rect.right = main_width / 2 + 120 + 45;

	m_nHeightBuffer += 10;
	m_nHeightBuffer += start_group->AddControl(IDC_START_TOTAL_VALUE, m_pNormalFont, CRect(main_width / 2 - 80 - 33, m_nHeightBuffer, main_width / 2 + 65, m_nHeightBuffer + edit_height));

	m_nHeightBuffer += 10;
	m_nHeightBuffer += start_group->AddControl(IDC_START_START_BUTTON, m_pNormalFont, CRect(main_width / 2 - 80, m_nHeightBuffer, main_width / 2 + 80, m_nHeightBuffer + button_height));

	box_rect.bottom = m_nHeightBuffer + 15;

	start_group->GetDlgItem(IDC_START_TOTAL_VALUE)->SetWindowTextW(L"");
	start_group->GetDlgItem(IDC_START_ROW_EDIT)->SetWindowTextW(L"");
	start_group->GetDlgItem(IDC_START_COLUMN_EDIT)->SetWindowTextW(L"");
	start_group->GetDlgItem(IDC_START_LOCATION_X_EDIT)->SetWindowTextW(L"");
	start_group->GetDlgItem(IDC_START_LOCATION_Y_EDIT)->SetWindowTextW(L"");
	start_group->GetDlgItem(IDC_START_LOCATION_X_EDIT)->SetWindowTextW(L"");
	GetDlgItem(IDC_MENU_ROW_EDIT)->SetWindowTextW(L"");
	GetDlgItem(IDC_MENU_COL_EDIT)->SetWindowTextW(L"");

	CItemGroup *content_group = new CItemGroup;
	content_group->Create(L"", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, box_rect, start_group, NULL);
	content_group->ShowWindow(SW_SHOW);

	Invalidate();

	if (xml_thread == NULL)
	{
		EnableButtons(PAGE_STEP::START);
		Invalidate();
		return 0;
	}

	::WaitForSingleObject(xml_thread->m_hThread, INFINITE);

	xml_thread = NULL;
	EnableButtons(PAGE_STEP::START);
	Invalidate();

	/*POINT pos;
	pos.x = 10;
	pos.y = 10;
	POINT size;
	size.x = 10;
	size.y = 10;*/

	//m_pVPViewerCore->SetGridBoxArray(nullptr);
	//m_pVPViewerCore->GridOnButtonDown(1, pos);
	//m_pVPViewerCore->SetGridBox(pos, size);

	/*if (::WaitForSingleObject(xml_thread->m_hThread, INFINITE) == TRUE)
	{
		AfxMessageBox(L"start...");
		m_pVPViewerCore->SetGridBoxArray(nullptr);
		m_pVPViewerCore->GridOnButtonDown(1, pos);
		m_pVPViewerCore->SetGridBox(pos, size);


		xml_thread = NULL;
		EnableButtons(PAGE_STEP::START);
		Invalidate();
	}*/

	return 0;
}

int CPointCounterDlg::MainPage()
{
	CRect main_rect;
	::GetWindowRect(AfxGetMainWnd()->GetSafeHwnd(), main_rect);
	::SendMessage(AfxGetMainWnd()->m_hWnd, WM_SIZE, SIZE_RESTORED, MAKELPARAM(main_rect.Width(), main_rect.Height()));

	InitPage();

	m_nHeightBuffer = 20;

	CRect client_rect, box_rect;
	GetClientRect(client_rect);

	m_ePageStep = PAGE_STEP::MAIN;
	DeletePage();
	ResetNavigation();
	EnableButtons(PAGE_STEP::MAIN);

	GetDlgItem(IDC_START_GROUP)->ShowWindow(SW_HIDE);

	CPageGroup *main_group = (CPageGroup *)GetDlgItem(IDC_MAIN_GROUP);
	int main_width = client_rect.Width() - CLIENT_PADDING * 4 - (LEFT_WIDTH + RIGHT_WIDTH);

	box_rect.left = 0;
	box_rect.right = main_width;

	main_group->SetWindowTextW(L"");
	main_group->MoveWindow(CRect(CLIENT_PADDING * 2 + LEFT_WIDTH, CLIENT_PADDING + NAVIGATION_HEIGHT, client_rect.Width() - CLIENT_PADDING * 2 - RIGHT_WIDTH, client_rect.Height() - CLIENT_PADDING));
	main_group->ShowWindow(SW_SHOW);

	m_nHeightBuffer += main_group->AddControl(IDC_MAIN_PAGE_LABEL, m_pTitleFont, CRect(0, m_nHeightBuffer, main_width, 0));
	m_nHeightBuffer += 20;

	box_rect.top = m_nHeightBuffer - 5;

	MainXml(main_group);

	box_rect.bottom = m_nHeightBuffer + 15;

	TRACE(L"main box : %d, %d, %d, %d\n", box_rect.left, box_rect.top, box_rect.right, box_rect.bottom);

	CItemGroup *content_group = new CItemGroup;
	content_group->Create(L"", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, box_rect, main_group, NULL);
	content_group->ShowWindow(SW_SHOW);

	Invalidate();

	return 0;
}

int CPointCounterDlg::MainXml(CPageGroup *main_group)
{
	AddNavigation(L"Main");

	unsigned int category_idx = 0;
	int button_height = 80;
	CRect client_rect;
	GetClientRect(client_rect);
	int page_width = client_rect.Width() - CLIENT_PADDING * 4 - (LEFT_WIDTH + RIGHT_WIDTH) - PAGE_PADDING * 2;
	int button_margin = 5;
	unsigned int column_count = 3;
	unsigned int column_order = 0;
	unsigned int button_order = 0;


	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("my-nodes"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyNode"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				POINTITEM point_item;

				pAttrPtr = pNode2->Getattributes()->getNamedItem("level");
				if (NULL != pAttrPtr)
					point_item.level = _ttoi(pAttrPtr->Gettext());

				if (point_item.level != 1)
					continue;

				pAttrPtr = pNode2->Getattributes()->getNamedItem("id");
				if (NULL != pAttrPtr)
					point_item.id = (LPCTSTR)pAttrPtr->Gettext();

				point_item = GetItem(point_item.id);
				point_item.category = category_idx;
				point_item.order = button_order;
				point_item.depth = 0;
				point_item.point.x = current_position.x;
				point_item.point.y = current_position.y;
				point_item.path = point_item.id + L".";
				point_item.exp = point_item.name;

				column_order = button_order % column_count;

				if (button_order > 0 && column_order == 0)
					m_nHeightBuffer += 20 + button_height;

				main_group->AddButton(
					-1,
					point_item,
					CRect(
					PAGE_PADDING + page_width / column_count * column_order + (column_order > 0 ? button_margin : 0),
					m_nHeightBuffer + 20,
					PAGE_PADDING + page_width / column_count * (column_order + 1),
					m_nHeightBuffer + 20 + button_height
					)
					);

				button_order++;
				category_idx++;
			}
		}
	}

	m_nHeightBuffer += 20 + button_height;

	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;

	xml_util.AllocXmlFromFile(m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-nodes") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyNode") == 0)
				{
					POINTITEM point_item;

					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"level") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);
							point_item.level = _ttoi(attr_val);
							break;
						}

						node_attr = xml_util.NextAttr(node_attr);
					}

					if (point_item.level == 1)
					{
						node_attr2 = xml_util.FirstAttr(cur_node2);

						while (node_attr2 != NULL)
						{
							attr_name2 = xml_util.GetName(node_attr2);

							if (wcscmp(attr_name2, L"id") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.id = attr_val2;
								break;
							}

							node_attr2 = xml_util.NextAttr(node_attr2);
						}

						point_item = GetItem(point_item.id);
						point_item.category = category_idx;
						point_item.order = button_order;
						point_item.depth = 0;
						point_item.point.x = current_position.x;
						point_item.point.y = current_position.y;
						point_item.path = point_item.id + L".";
						point_item.exp = point_item.name;

						column_order = button_order % column_count;

						if (button_order > 0 && column_order == 0)
							m_nHeightBuffer += 20 + button_height;

						main_group->AddButton(
							-1,
							point_item,
							CRect(
								PAGE_PADDING + page_width / column_count * column_order + (column_order > 0 ? button_margin : 0),
								m_nHeightBuffer + 20,
								PAGE_PADDING + page_width / column_count * (column_order + 1),
								m_nHeightBuffer + 20 + button_height
							)
						);

						button_order++;
						category_idx++;
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	m_nHeightBuffer += 20 + button_height;

	xml_util.DeAlloc();
	*/

	return 0;
}

int CPointCounterDlg::PageXml(int item_count, CPageGroup *content_page, POINTITEM source_item)
{
	AddNavigation(source_item);

	m_nHeightBuffer = 0;

	int button_height = 40;
	CRect client_rect;
	GetClientRect(client_rect);
	int page_width = client_rect.Width() - CLIENT_PADDING * 4 - (LEFT_WIDTH + RIGHT_WIDTH) - PAGE_PADDING * 2;
	int button_margin = 5;
	unsigned int column_count = 1;
	unsigned int column_order = 0;
	unsigned int button_order = 0;

	if (item_count >= 16) column_count = 3;
	else if (item_count >= 8) column_count = 2;
	else column_count = 1;


	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("my-edges"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyEdge"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				POINTITEM point_item;

				pAttrPtr = pNode2->Getattributes()->getNamedItem("source");
				if (NULL != pAttrPtr)
					point_item.id = (LPCTSTR)pAttrPtr->Gettext();

				if (wcscmp(point_item.id, source_item.id) == 0)
				{
					pAttrPtr = pNode2->Getattributes()->getNamedItem("target");
					if (NULL != pAttrPtr)
						point_item.id = (LPCTSTR)pAttrPtr->Gettext();

					point_item = GetItem(point_item.id);
					point_item.category = source_item.category;
					point_item.order = button_order;
					point_item.depth = source_item.depth + 1;
					point_item.point.x = current_position.x;
					point_item.point.y = current_position.y;

					column_order = button_order % column_count;

					if (button_order > 0 && column_order == 0)
						m_nHeightBuffer += 20 + button_height;

					content_page->AddButton(
						item_count,
						point_item,
						CRect(
						PAGE_PADDING + page_width / column_count * column_order + (column_order > 0 ? button_margin : 0),
						m_nHeightBuffer + 20,
						PAGE_PADDING + page_width / column_count * (column_order + 1),
						m_nHeightBuffer + 20 + button_height
						)
						);

					button_order++;
				}
			}
		}
	}

	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;

	xml_util.AllocXmlFromFile(m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-edges") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyEdge") == 0)
				{
					POINTITEM point_item;

					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"source") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);
							point_item.id = attr_val;
							break;
						}

						node_attr = xml_util.NextAttr(node_attr);
					}

					if (wcscmp(point_item.id, source_item.id) == 0)
					{
						node_attr2 = xml_util.FirstAttr(cur_node2);

						while (node_attr2 != NULL)
						{
							attr_name2 = xml_util.GetName(node_attr2);

							if (wcscmp(attr_name2, L"target") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.id = attr_val2;
								break;
							}

							node_attr2 = xml_util.NextAttr(node_attr2);
						}

						point_item = GetItem(point_item.id);
						point_item.category = source_item.category;
						point_item.order = button_order;
						point_item.depth = source_item.depth + 1;
						point_item.point.x = current_position.x;
						point_item.point.y = current_position.y;

						column_order = button_order % column_count;

						if (button_order > 0 && column_order == 0)
							m_nHeightBuffer += 20 + button_height;

						content_page->AddButton(
							item_count,
							point_item,
							CRect(
								PAGE_PADDING + page_width / column_count * column_order + (column_order > 0 ? button_margin : 0),
								m_nHeightBuffer + 20,
								PAGE_PADDING + page_width / column_count * (column_order + 1),
								m_nHeightBuffer + 20 + button_height
							)
						);

						button_order++;
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();
	*/

	m_nHeightBuffer += 20 + button_height;

	return 0;
}

unsigned int CPointCounterDlg::CountItem(CString source_id)
{
	unsigned int item_count = 0;


	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("my-edges"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyEdge"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				POINTITEM point_item;

				pAttrPtr = pNode2->Getattributes()->getNamedItem("source");
				if (NULL != pAttrPtr)
				{
					CString item_id = (LPCTSTR)pAttrPtr->Gettext();

					if (wcscmp(item_id, source_id) == 0)
						item_count++;
				}
			}
		}
	}


	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_val = NULL;

	xml_util.AllocXmlFromFile(m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-edges") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyEdge") == 0)
				{
					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"source") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);

							if (wcscmp(attr_val, source_id) == 0)
								item_count++;
						}

						node_attr = xml_util.NextAttr(node_attr);
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();
	*/

	return item_count;
}

POINTITEM CPointCounterDlg::GetItem(CString item_id)
{
	POINTITEM point_item;



	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("my-nodes"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyNode"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				pAttrPtr = pNode2->Getattributes()->getNamedItem("id");
				if (NULL != pAttrPtr)
				{
					CString _id = (LPCTSTR)pAttrPtr->Gettext();

					if (wcscmp(_id, item_id) == 0)
						point_item.id = item_id;
				}

				if (wcscmp(point_item.id, L"") != 0)
				{
					pAttrPtr = pNode2->Getattributes()->getNamedItem("name");
					if (NULL != pAttrPtr)
						point_item.name = (LPCTSTR)pAttrPtr->Gettext();

					pAttrPtr = pNode2->Getattributes()->getNamedItem("level");
					if (NULL != pAttrPtr)
						point_item.level = _ttoi(pAttrPtr->Gettext());

					pAttrPtr = pNode2->Getattributes()->getNamedItem("x");
					if (NULL != pAttrPtr)
						point_item.size.cx = _ttoi(pAttrPtr->Gettext());

					pAttrPtr = pNode2->Getattributes()->getNamedItem("y");
					if (NULL != pAttrPtr)
						point_item.size.cy = _ttoi(pAttrPtr->Gettext());

					break;
				}
			}
		}
	}


	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr3 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_name3 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;
	LPCWSTR attr_val3 = NULL;

	xml_util.AllocXmlFromFile(m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-nodes") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyNode") == 0)
				{
					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"id") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);

							if (wcscmp(attr_val, item_id) == 0)
							{
								point_item.id = item_id;
								break;
							}
						}

						node_attr = xml_util.NextAttr(node_attr);
					}

					if (wcscmp(point_item.id, L"") != 0)
					{
						node_attr2 = xml_util.FirstAttr(cur_node2);

						while (node_attr2 != NULL)
						{
							attr_name2 = xml_util.GetName(node_attr2);

							if (wcscmp(attr_name2, L"name") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.name = attr_val2;
							}
							else if (wcscmp(attr_name2, L"level") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.level = _ttoi(attr_val2);
							}
							else if (wcscmp(attr_name2, L"x") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.size.cx = _ttoi(attr_val2);
							}
							else if (wcscmp(attr_name2, L"y") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.size.cy = _ttoi(attr_val2);
							}

							node_attr2 = xml_util.NextAttr(node_attr2);
						}

						break;
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();
	*/

	return point_item;
}

/*
LPCWSTR CPointCounterDlg::XmlGetAttr(CRaipidXmlHelper xml_util, CRaipidXmlHelper::HANDLE_NODE cur_node, CString target_attr)
{
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_val = NULL;

	node_attr = xml_util.FirstAttr(cur_node);

	while (node_attr != NULL)
	{
		attr_name = xml_util.GetName(node_attr);

		if (wcscmp(attr_name, target_attr) == 0)
		{
			attr_val = xml_util.GetValue(node_attr);
			break;
		}

		node_attr = xml_util.NextAttr(node_attr);
	}

	return attr_val;
}
*/

BOOL CPointCounterDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	if (m_ePageStep > PAGE_STEP::START)
		return TRUE;

	m_pVPViewerCore->SetGridBoxArray(nullptr);
	TRACE(L"grid box : %d, %d, %d\n", nFlags, point.x, point.y);
	m_pVPViewerCore->GridOnButtonDown(nFlags, point);
	m_ctlStartPageButton.EnableWindow(TRUE);
	return TRUE;
}

BOOL CPointCounterDlg::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	if (m_ePageStep > PAGE_STEP::START)
		return TRUE;

	POINT size;
	POINT position;
	m_pVPViewerCore->GridOnButtonMove(nFlags, point);
	if (m_ctlStartPageButton.IsWindowEnabled())
	{
		m_pVPViewerCore->GetGridBox(position, size);
		CString buf;
		buf.Format(L"%d", size.x);
		m_ctlCellColumnEdit.SetWindowTextW(buf);
		buf.Format(L"%d", size.y);
		m_ctlCellRowEdit.SetWindowTextW(buf);
		buf.Format(L"%d", position.x);
		m_ctlLocationXEdit.SetWindowTextW(buf);
		buf.Format(L"%d", position.y);
		m_ctlLocationYEdit.SetWindowTextW(buf);
	}
	return TRUE;
}

BOOL CPointCounterDlg::EnableButtons(PAGE_STEP page_step)
{
	switch (page_step)
	{
	case PAGE_STEP::THREAD:
		m_ctlLoadProfileButton.EnableWindow(FALSE);
		m_ctlLoadTraceButton.EnableWindow(FALSE);
		m_ctlSaveTraceButton.EnableWindow(FALSE);
		m_ctlSaveResultButton.EnableWindow(FALSE);
		m_ctlRestartCountButton.EnableWindow(FALSE);
		m_ctlSkipCountButton.EnableWindow(FALSE);
		m_ctlBackStepButton.EnableWindow(FALSE);
		m_ctlUndoCountButton.EnableWindow(FALSE);
		m_ctlMoveToButton.EnableWindow(FALSE);
		m_ctlSameLastButton.EnableWindow(FALSE);
		m_ctlPosRowEdit.EnableWindow(FALSE);
		m_ctlPosColumnEdit.EnableWindow(FALSE);
		break;
	case PAGE_STEP::START:
		m_ctlLoadProfileButton.EnableWindow(TRUE);
		m_ctlLoadTraceButton.EnableWindow(TRUE);
		m_ctlSaveTraceButton.EnableWindow(FALSE);
		m_ctlSaveResultButton.EnableWindow(FALSE);
		m_ctlRestartCountButton.EnableWindow(FALSE);
		m_ctlSkipCountButton.EnableWindow(FALSE);
		m_ctlBackStepButton.EnableWindow(FALSE);
		m_ctlMoveToButton.EnableWindow(FALSE);
		m_ctlSameLastButton.EnableWindow(FALSE);
		m_ctlPosRowEdit.EnableWindow(FALSE);
		m_ctlPosColumnEdit.EnableWindow(FALSE);
		break;
	default:
		m_ctlLoadProfileButton.EnableWindow(FALSE);
		m_ctlLoadTraceButton.EnableWindow(FALSE);
		m_ctlSaveTraceButton.EnableWindow(TRUE);
		m_ctlSaveResultButton.EnableWindow(TRUE);
		m_ctlRestartCountButton.EnableWindow(TRUE);
		m_ctlSkipCountButton.EnableWindow(TRUE);
		m_ctlBackStepButton.EnableWindow(TRUE);
		m_ctlMoveToButton.EnableWindow(TRUE);
		m_ctlSameLastButton.EnableWindow(TRUE);
		m_ctlPosRowEdit.EnableWindow(TRUE);
		m_ctlPosColumnEdit.EnableWindow(TRUE);
	}

	m_ctlBackStepButton.EnableWindow(m_ePageStep == PAGE_STEP::MAIN ? TRUE : FALSE);
	m_ctlUndoCountButton.EnableWindow(m_ePageStep > PAGE_STEP::MAIN ? TRUE : FALSE);
	m_ctlStartPageButton.EnableWindow(FALSE);

	return TRUE;
}

BOOL CPointCounterDlg::DeletePage()
{
	if (m_pContentPage != NULL)
	{
		delete m_pContentTitle;
		delete m_pContentPage;
		m_pContentTitle = NULL;
		m_pContentPage = NULL;
	}

	return TRUE;
}

int CPointCounterDlg::GoPage(POINTITEM point_item)
{
	if (point_item.depth < 0)
	{
		MainPage();
		return -1;
	}

	unsigned int item_count = CountItem(point_item.id);

	if (item_count > 0)
	{
		m_ePageStep = PAGE_STEP::CONTENT;
		DeletePage();
		EnableButtons(PAGE_STEP::CONTENT);

		GetDlgItem(IDC_START_GROUP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MAIN_GROUP)->ShowWindow(SW_HIDE);

		CRect client_rect, box_rect;
		GetClientRect(client_rect);

		int main_width = client_rect.Width() - CLIENT_PADDING * 4 - (LEFT_WIDTH + RIGHT_WIDTH);

		box_rect.left = 0;
		box_rect.right = main_width;
		box_rect.top = 0;

		//CSize string_size = TextSize(m_pTitleFont, L"CCCCCCCC");

		m_pContentPage = new CPageGroup;
		m_pContentPage->Create(L"", WS_CHILD, CRect(CLIENT_PADDING * 2 + LEFT_WIDTH, CLIENT_PADDING + NAVIGATION_HEIGHT + 67, client_rect.Width() - CLIENT_PADDING * 2 - RIGHT_WIDTH, client_rect.Height() - CLIENT_PADDING), this);
		m_pContentPage->ShowWindow(SW_SHOW);

		//m_nHeightBuffer += m_pContentPage->AddControl(L"CCCC PAGE", m_pTitleFont, CRect(0, m_nHeightBuffer, main_width, 0));
		//m_nHeightBuffer += 20;

		PageXml(item_count, m_pContentPage, point_item);

		box_rect.bottom = m_nHeightBuffer + 15;

		CItemGroup *content_group = new CItemGroup;
		content_group->Create(L"", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, box_rect, m_pContentPage, NULL);
		content_group->ShowWindow(SW_SHOW);

		/*if (m_pContentTitle->GetSafeHwnd())
		{
			m_pContentTitle->MoveWindow(CRect(
				CLIENT_PADDING * 2 + SIDE_WIDTH,
				CLIENT_PADDING + NAVIGATION_HEIGHT + 48,
				client_rect.Width() - CLIENT_PADDING * 2 - SIDE_WIDTH,
				CLIENT_PADDING + NAVIGATION_HEIGHT + 97
				));
			title_label->ShowWindow(SW_SHOW);
		}*/

		m_pContentTitle = new CStatic;
		m_pContentTitle->Create(point_item.name, WS_CHILD | SS_CENTER, CRect(
			CLIENT_PADDING * 2 + LEFT_WIDTH,
			CLIENT_PADDING + NAVIGATION_HEIGHT + 18,
			client_rect.Width() - CLIENT_PADDING * 2 - RIGHT_WIDTH,
			CLIENT_PADDING + NAVIGATION_HEIGHT + 67
			), this);
		m_pContentTitle->SetFont(m_pTitleFont);
		m_pContentTitle->ShowWindow(SW_SHOW);

		Invalidate();
	}
	else
	{
		AddOutput(point_item);
		next(false);
		MainPage();
	}

	return 0;
}

void CPointCounterDlg::ResetNavigation()
{
	m_nNaviationLeft = LEFT_WIDTH + CLIENT_PADDING * 2;

	for (INT_PTR i = 0; i < m_arrNavigationLabel.GetSize(); i++)
	{
		CStatic *navigation_label = (CStatic *)m_arrNavigationLabel.GetAt(i);
		navigation_label->ShowWindow(SW_HIDE);
		delete navigation_label;
	}

	m_arrNavigationLabel.RemoveAll();
	//m_arrNavigationItem.RemoveAll();
}


void CPointCounterDlg::ReverseNavigation(POINTITEM point_item)
{
	POINTITEM cur_item;
	int remove_step = 0;

	/*for (INT_PTR i = m_arrNavigationItem.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM navigation_item = m_arrNavigationItem.GetAt(i);

		if (remove_item == TRUE && wcscmp(navigation_item.id, L"") != 0)
			break;

		if (wcscmp(navigation_item.id, point_item.id) == 0)
		{
			cur_item = navigation_item;
			remove_item = TRUE;
			//m_arrNavigationItem.RemoveAt(i);
			//break;
		}

		m_arrNavigationItem.RemoveAt(i);
	}

	remove_item = FALSE;*/

	/*if (point_item.depth == -1)
	{
		MainPage();
		return;
	}*/

	for (INT_PTR i = m_arrNavigationLabel.GetSize() - 1; i >= 0; i--)
	{
		CNavigationLabel *navigation_label = m_arrNavigationLabel.GetAt(i);
		POINTITEM navigation_item = navigation_label->GetData();

		//if (navigation_item.depth > -1)
		if (navigation_item.depth >= point_item.depth)
		{
			if (wcscmp(navigation_item.id, point_item.id) == 0)
			{
				cur_item = navigation_item;
				remove_step = 1;
			}
		}
		else
		{
			if (remove_step >= 1)
				remove_step = 2;
		}

		CRect label_rect;
		navigation_label->GetWindowRect(label_rect);
		ScreenToClient(label_rect);
		m_nNaviationLeft = label_rect.left;
		delete navigation_label;
		m_arrNavigationLabel.RemoveAt(i);

		if (remove_step >= 2)
			break;



		/*CNavigationLabel *navigation_label = m_arrNavigationLabel.GetAt(i);
		POINTITEM navigation_item = navigation_label->GetData();

		//AfxMessageBox(L"1 : " + navigation_item.id);

		if (remove_item == TRUE && navigation_item.depth > -1)
		{
			cur_item = navigation_item;
			break;
		}

		if (wcscmp(navigation_item.id, point_item.id) == 0)
		{
			remove_item = TRUE;
		}

		//AfxMessageBox(L"2 : " + navigation_item.id);

		CRect label_rect;
		navigation_label->GetWindowRect(label_rect);
		ScreenToClient(label_rect);
		m_nNaviationLeft = label_rect.left;
		delete navigation_label;
		m_arrNavigationLabel.RemoveAt(i);*/
	}

	//Invalidate();

	if (point_item.depth == -1)
		MainPage();
	else
		GoPage(cur_item);
}

POINTITEM CPointCounterDlg::AddNavigation(CString navigation_caption)
{
	CString string_buffer;
	CSize text_size;
	POINTITEM navigation_item;
	navigation_item.name = navigation_caption;
	navigation_item.depth = -1;
	navigation_item.point.x = current_position.x;
	navigation_item.point.y = current_position.y;

	CNavigationLabel *navigation_label = new CNavigationLabel;
	text_size = TextSize(m_pNaviFont, navigation_item.name);
	navigation_label->Create(navigation_item.name, WS_CHILD | WS_VISIBLE | SS_NOTIFY, CRect(m_nNaviationLeft, CLIENT_PADDING, m_nNaviationLeft + text_size.cx, CLIENT_PADDING + NAVIGATION_HEIGHT), this);
	m_nNaviationLeft += text_size.cx;
	navigation_label->SetFont(m_pNaviFont);
	navigation_label->SetData(navigation_item);
	navigation_label->ShowWindow(SW_SHOW);
	m_arrNavigationLabel.Add(navigation_label);
	//m_arrNavigationItem.Add(navigation_item);

	return navigation_item;
}

POINTITEM CPointCounterDlg::AddNavigation(POINTITEM point_item, BOOL display_label)
{
	CString string_buffer;
	CSize text_size;
	//int item_depth = 0;

	if (m_arrNavigationLabel.GetSize() > 0)
	{
		/*for (int i = 0; i < m_arrNavigationLabel.GetSize(); i++)
		{
			CNavigationLabel *navigation_label = m_arrNavigationLabel.GetAt(i);
			POINTITEM navigation_item = navigation_label->GetData();

			if (navigation_item.depth < 0)
				continue;
			else
				item_depth = navigation_item.depth + 1;
		}*/

		POINTITEM arrow_item;
		arrow_item.name = L" >> ";
		arrow_item.depth = -2;
		arrow_item.point.x = current_position.x;
		arrow_item.point.y = current_position.y;

		CNavigationLabel *arrow_label = new CNavigationLabel;
		text_size = TextSize(m_pNaviFont, arrow_item.name);
		arrow_label->Create(arrow_item.name, WS_CHILD | WS_VISIBLE, CRect(m_nNaviationLeft, CLIENT_PADDING, m_nNaviationLeft + text_size.cx, CLIENT_PADDING + NAVIGATION_HEIGHT), this);
		m_nNaviationLeft += text_size.cx;
		arrow_label->SetFont(m_pNaviFont);
		arrow_label->SetData(arrow_item);
		arrow_label->ShowWindow(display_label ? SW_SHOW : SW_HIDE);
		m_arrNavigationLabel.Add(arrow_label);
		//m_arrNavigationItem.Add(arrow_item);
	}

	TRACE(L"[add navi] %d, %d, %d, %d : %s\n", point_item.point.x, point_item.point.y, point_item.depth, point_item.id, point_item.name);

	CNavigationLabel *navigation_label = new CNavigationLabel;
	text_size = TextSize(m_pNaviFont, point_item.name);
	navigation_label->Create(point_item.name, WS_CHILD | WS_VISIBLE | SS_NOTIFY, CRect(m_nNaviationLeft, CLIENT_PADDING, m_nNaviationLeft + text_size.cx, CLIENT_PADDING + NAVIGATION_HEIGHT), this);
	m_nNaviationLeft += text_size.cx;
	navigation_label->SetFont(m_pNaviFont);
	navigation_label->SetData(point_item);
	navigation_label->ShowWindow(display_label ? SW_SHOW : SW_HIDE);
	m_arrNavigationLabel.Add(navigation_label);
	//m_arrNavigationItem.Add(point_item);

	return point_item;
}

void CPointCounterDlg::UndoNavigation()
{
	if (m_ePageStep <= PAGE_STEP::MAIN)
		return;

	POINTITEM cur_item;
	int remove_step = 0;

	/*for (INT_PTR i = m_arrNavigationItem.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM navigation_item = m_arrNavigationItem.GetAt(i);

		if (remove_item == TRUE && wcscmp(navigation_item.id, L"") != 0)
		{
			cur_item = navigation_item;
			break;
		}

		if (wcscmp(navigation_item.id, L"") != 0)
			remove_item = TRUE;

		m_arrNavigationItem.RemoveAt(i);
	}

	CString aa;
	aa.Format(L"cur_item : %s", cur_item.id);
	AfxMessageBox(aa);

	remove_item = FALSE;*/

	for (INT_PTR i = m_arrNavigationLabel.GetSize() - 1; i >= 0; i--)
	{
		CNavigationLabel *navigation_label = m_arrNavigationLabel.GetAt(i);
		POINTITEM navigation_item = navigation_label->GetData();

		if (navigation_item.depth > -1)
		{
			if (remove_step >= 1)
			{
				cur_item = navigation_item;
				remove_step = 2;
			}

			if (remove_step < 1)
				remove_step = 1;
		}
		else
		{
			if (remove_step >= 2)
				remove_step = 3;
		}

		CRect label_rect;
		navigation_label->GetWindowRect(label_rect);
		ScreenToClient(label_rect);
		m_nNaviationLeft = label_rect.left;
		delete navigation_label;
		m_arrNavigationLabel.RemoveAt(i);

		if (remove_step >= 3)
			break;
	}

	GoPage(cur_item);
}

unsigned int CPointCounterDlg::AddControl(unsigned int control_id, CFont *control_font, CRect control_size)
{
	if (GetDlgItem(control_id)->GetSafeHwnd())
	{
		unsigned int height_buffer = 0;

		if (control_size.bottom > 0)
			height_buffer = control_size.bottom;
		else
		{
			CString string_buffer;
			CSize string_size;
			GetDlgItem(control_id)->GetWindowTextW(string_buffer);
			string_size = TextSize(control_font, string_buffer);
			height_buffer = control_size.top + string_size.cy;
		}

		GetDlgItem(control_id)->SetFont(control_font);
		GetDlgItem(control_id)->MoveWindow(CRect(control_size.left, control_size.top, control_size.right, height_buffer));

		return height_buffer - control_size.top;
	}

	return 0;
}

CSize CPointCounterDlg::TextSize(CFont *text_font, CString text_string)
{
	CClientDC text_dc(this);
	CSize text_size;
	int token_position = 0;
	CString token_string = text_string.Tokenize(_T("\r\n"), token_position);

	text_dc.SelectObject(text_font);
	text_size.cx = text_size.cy = 0;

	while (!token_string.IsEmpty())
	{
		CSize token_size = text_dc.GetTextExtent(token_string, token_string.GetLength());
		text_size.cx = text_size.cx < token_size.cx ? token_size.cx : text_size.cx;
		text_size.cy += token_size.cy;
		token_string = text_string.Tokenize(_T("\r\n"), token_position);
	}

	return text_size;
}

void CPointCounterDlg::GenerateExcelData()
{
	int tot_count = 0;

	m_arrExcelData.RemoveAll();

	for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		if (point_item.depth == 0)
		{
			tot_count++;
		}
	}

	for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);
		BOOL exist_data = FALSE;

		for (INT_PTR j = m_arrExcelData.GetSize() - 1; j >= 0; j--)
		{
			POINTITEM excel_data = m_arrExcelData.GetAt(j);

			if (excel_data.path.Compare(point_item.path) == 0)
			{
				POINTITEM copy_item;
				copy_item.category = excel_data.category;
				copy_item.id = excel_data.id;
				copy_item.name = excel_data.name;
				copy_item.path = excel_data.path;
				copy_item.exp = excel_data.exp;
				copy_item.count = excel_data.count + 1;
				//copy_item.ratio = category_id.Compare(point_item.id) == 0 ? ratio_count : point_item.ratio;
				//copy_item.total = total_count;
				copy_item.total = tot_count;

				exist_data = TRUE;
				break;
			}
		}



		if (point_item.path)
		{
			tot_count++;
		}
	}
}

CString CPointCounterDlg::AddItem(POINTITEM last_item)
{
	CString category_id = L"";
	CString category_name = L"";
	CString exp_text = L"";
	CString navigation_text = L"";
	CString id_path = L"";
	int id_count = 0;
	int tot_count = 0;
	int minus_count = 0;

	m_nPointCount++;

	m_arrLastData.RemoveAll();

	/*for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		if (point_item.point.x == last_item.point.x && point_item.point.y == last_item.point.y)
		{
			TRACE(L"[remove item x and y] %d, %d : %s\n", point_item.point.x, point_item.point.y, point_item.name);
			m_arrResultData.RemoveAt(i);
		}
	}*/

	CString last_id = L"";

	for (INT_PTR i = 0; i < m_arrNavigationLabel.GetSize(); i++)
	{
		CNavigationLabel *navigation_label = m_arrNavigationLabel.GetAt(i);
		POINTITEM point_item = navigation_label->GetData();

		TRACE(L"[label arr] %d, %d, %d, %d : %s\n", point_item.point.x, point_item.point.y, point_item.depth, point_item.id, point_item.name);

		if (point_item.depth < 0)
			continue;

		if (category_id.CompareNoCase(L"") == 0)
		//if (point_item.depth == 0)
		{
			category_id = point_item.id;
			category_name = point_item.name;
		}

		TRACE(L"[label res] %s, %s\n", category_id, category_name);

		id_path += point_item.id + L".";

		last_id = point_item.id;
	}

	if (last_id.CompareNoCase(last_item.id) != 0)
		id_path += last_item.id + L".";

	/*for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		if (id_path.Compare(point_item.path) == 0)
		{
			id_count = point_item.count;
			break;
		}
	}

	id_count++;*/

	for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		TRACE(L"check data : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
			point_item.point.x,
			point_item.point.y,
			point_item.category,
			point_item.depth,
			point_item.id,
			point_item.name,
			point_item.path,
			point_item.exp,
			point_item.count,
			point_item.ratio,
			point_item.total
			);

		if (point_item.point.x == current_position.x && point_item.point.y == current_position.y)
		{
			TRACE(L"[remove item x and y] %d, %d : %s\n", point_item.point.x, point_item.point.y, point_item.name);
			m_arrResultData.RemoveAt(i);
		}
		else
		{
			if (point_item.depth == 0)
			{
				if (id_path.Compare(point_item.path) == 0)
					id_count++;

				tot_count++;
			}
		}
	}

	id_count++;
	tot_count++;

	for (INT_PTR i = 0; i < m_arrNavigationLabel.GetSize(); i++)
	{
		CNavigationLabel *navigation_label = m_arrNavigationLabel.GetAt(i);
		POINTITEM point_item = navigation_label->GetData();

		if (point_item.depth < 0)
			continue;

		if (wcscmp(navigation_text, L"") != 0)
			navigation_text += L" | ";

		if (wcscmp(exp_text, L"") != 0)
			exp_text += L";";

		navigation_text += point_item.name;
		exp_text += point_item.name;

		point_item.path = id_path;
		point_item.count = id_count;
		point_item.point.x = current_position.x;
		point_item.point.y = current_position.y;

		m_arrLastData.Add(point_item);
		m_arrResultData.Add(point_item);
	}

	if (wcscmp(navigation_text, L"") != 0)
		navigation_text += " | ";

	if (wcscmp(exp_text, L"") != 0)
		exp_text += L";";

	navigation_text += last_item.name;
	exp_text += last_item.name;

	last_item.path = id_path;
	last_item.count = id_count;

	// excel data
	int data_at = -1;
	int total_count = id_count;
	int ratio_count = id_count;

	TRACE(L"add item insert excel id : %s\n", category_id);

	POINTITEM excel_item;
	excel_item.category = last_item.category;
	excel_item.point.x = last_item.point.x;
	excel_item.point.y = last_item.point.y;
	excel_item.size.cx = last_item.size.cx;
	excel_item.size.cy = last_item.size.cy;
	excel_item.id = category_id;
	excel_item.name = category_name;
	excel_item.path = id_path;
	excel_item.exp = exp_text;
	excel_item.count = id_count;

	for (INT_PTR i = m_arrExcelData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrExcelData.GetAt(i);

		if (id_path.Compare(point_item.path) == 0)
		{
			data_at = i;
			m_arrExcelData.RemoveAt(i);
		}
		else
		{
			if (category_id.Compare(point_item.id) == 0)
				ratio_count += point_item.count;

			//total_count += point_item.count;
		}
	}

	excel_item.ratio = ratio_count;
	//excel_item.total = total_count;
	excel_item.total = tot_count;

	if (data_at >= 0)
		m_arrExcelData.InsertAt(data_at, excel_item);
	else
		m_arrExcelData.Add(excel_item);

	if (TRUE)
	{
		for (INT_PTR i = m_arrExcelData.GetSize() - 1; i >= 0; i--)
		{
			POINTITEM point_item = m_arrExcelData.GetAt(i);
			POINTITEM copy_item;
			copy_item.category = point_item.category;
			copy_item.point.x = point_item.point.x;
			copy_item.point.y = point_item.point.y;
			copy_item.size.cx = point_item.size.cx;
			copy_item.size.cy = point_item.size.cy;
			copy_item.id = point_item.id;
			copy_item.name = point_item.name;
			copy_item.path = point_item.path;
			copy_item.exp = point_item.exp;
			copy_item.depth = point_item.depth;
			//copy_item.count = point_item.count;
			//copy_item.ratio = category_id.Compare(point_item.id) == 0 ? ratio_count : point_item.ratio;

			int item_count = 0, ratio_count = 0;

			for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
			{
				POINTITEM item_buff = m_arrResultData.GetAt(i);

				TRACE(L"compare data -- result : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
					item_buff.point.x,
					item_buff.point.y,
					item_buff.category,
					item_buff.depth,
					item_buff.id,
					item_buff.name,
					item_buff.path,
					item_buff.exp,
					item_buff.count,
					item_buff.ratio,
					item_buff.total
					);

				TRACE(L"compare data -- excel : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
					copy_item.point.x,
					copy_item.point.y,
					copy_item.category,
					copy_item.depth,
					copy_item.id,
					copy_item.name,
					copy_item.path,
					copy_item.exp,
					copy_item.count,
					copy_item.ratio,
					copy_item.total
					);

				if (item_buff.depth != 0)
					continue;

				if (item_buff.path.Compare(copy_item.path) == 0)
					item_count++;

				if (item_buff.id.Compare(copy_item.id) == 0)
					ratio_count++;
			}

			copy_item.count = item_count;
			copy_item.ratio = ratio_count;
			//copy_item.total = total_count;
			copy_item.total = tot_count;

			TRACE(L"compare data -- complete : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
				copy_item.point.x,
				copy_item.point.y,
				copy_item.category,
				copy_item.depth,
				copy_item.id,
				copy_item.name,
				copy_item.path,
				copy_item.exp,
				copy_item.count,
				copy_item.ratio,
				copy_item.total
				);

			m_arrExcelData.RemoveAt(i);

			if (item_count > 0)
				m_arrExcelData.InsertAt(i, copy_item);
		}
	}

	// res data
	if (wcscmp(navigation_text, L"") != 0)
		last_item.navi = navigation_text;

	m_arrLastData.Add(last_item);
	m_arrResultData.Add(last_item);

	for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);
		TRACE(L"[complete data] res data : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
			point_item.point.x,
			point_item.point.y,
			point_item.category,
			point_item.depth,
			point_item.id,
			point_item.name,
			point_item.path,
			point_item.exp,
			point_item.count,
			point_item.ratio,
			point_item.total
			);
	}

	for (INT_PTR i = 0; i < m_arrExcelData.GetSize(); i++)
	{
		POINTITEM point_item = m_arrExcelData.GetAt(i);
		TRACE(L"[complete data] excel data : %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
			point_item.category,
			point_item.depth,
			point_item.id,
			point_item.name,
			point_item.path,
			point_item.exp,
			point_item.count,
			point_item.ratio,
			point_item.total
			);
	}

	return last_item.navi;
}

void CPointCounterDlg::AddOutput(POINTITEM last_item)
{
	CString count_text;
	CString output_text;

	GetDlgItem(IDC_OUTPUT_EDIT)->GetWindowTextW(output_text);
	output_text += L"\r\n";

	//count_text.Format(L"%d: ", m_nPointCount);
	count_text.Format(L"%d x %d: ", last_item.point.y, last_item.point.x);
	output_text += count_text;

	TRACE(L"output prev : %d \n\n", last_item.navi);

	CString navi_text = AddItem(last_item);
	TRACE(L"output result : %d \n\n", last_item.navi);

	TRACEOUTPUT output_data;
	output_data.point.x = last_item.point.x;
	output_data.point.y = last_item.point.y;
	output_data.navi = navi_text;

	m_arrOutputData.Add(output_data);

	GetDlgItem(IDC_OUTPUT_EDIT)->SetWindowTextW(output_text + navi_text);

	CEdit *output_edit = (CEdit *)GetDlgItem(IDC_OUTPUT_EDIT);
	output_edit->LineScroll(output_edit->GetLineCount());
}

void CPointCounterDlg::AddOutput(POINTITEM last_item, CString navi_text)
{
	CString count_text;
	CString output_text;

	GetDlgItem(IDC_OUTPUT_EDIT)->GetWindowTextW(output_text);
	output_text += L"\r\n";

	//count_text.Format(L"%d: ", m_nPointCount);
	count_text.Format(L"%d x %d: ", last_item.point.y, last_item.point.x);
	output_text += count_text;

	TRACEOUTPUT output_data;
	output_data.point.x = last_item.point.x;
	output_data.point.y = last_item.point.y;
	output_data.navi = navi_text;

	m_arrOutputData.Add(output_data);

	TRACE(L"output prev : %d \n\n", last_item.navi);

	//CString navigation_text = AddItem(last_item);
	AddItem(last_item);
	TRACE(L"output result : %d \n\n", last_item.navi);

	GetDlgItem(IDC_OUTPUT_EDIT)->SetWindowTextW(output_text + navi_text);

	CEdit *output_edit = (CEdit *)GetDlgItem(IDC_OUTPUT_EDIT);
	output_edit->LineScroll(output_edit->GetLineCount());
}


void CPointCounterDlg::OnBnClickedMenuSkipButton()
{
	// TODO: Add your control notification handler code here
	next(true);
}


void CPointCounterDlg::OnBnClickedMenuUndoButton()
{
	// TODO: Add your control notification handler code here
	UndoNavigation();
}


void CPointCounterDlg::OnBnClickedMenuMoveToButton()
{
	// TODO: Add your control notification handler code here
	int row = GetDlgItemInt(IDC_MENU_ROW_EDIT);
	int col = GetDlgItemInt(IDC_MENU_COL_EDIT);

	if (row > size.y)
		row = size.y;
	if (col > size.x)
		col = size.x;

	if (row < 1)
		row = 1;
	if (col < 1)
		col = 1;

	POINT p;
	p.x = col;
	p.y = row;

	jump(p);
}


void CPointCounterDlg::OnBnClickedMenuBackStepButton()
{
	// TODO: Add your control notification handler code here
	POINT p;
	p.x = current_position.x;
	p.y = current_position.y;

	if (p.x > 1)
		p.x -= 1;
	else if (p.y > 1){
		p.x = size.x;
		p.y -= 1;
	}
	else{
		p.x = size.x;
		p.y = size.y;
	}

	/*for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		if (point_item.point.x == p.x && point_item.point.y == p.y)
			m_arrResultData.RemoveAt(i);
	}

	for (INT_PTR i = m_arrExcelData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrExcelData.GetAt(i);

		TRACE(L"res data : %d, %s, %s, %s, %s, %d, %d, %d\n",
			point_item.category,
			point_item.id,
			point_item.name,
			point_item.path,
			point_item.exp,
			point_item.count,
			point_item.ratio,
			point_item.total
			);

		if (point_item.point.x == p.x && point_item.point.y == p.y)
			m_arrExcelData.RemoveAt(i);
	}*/

	int tot_count = 0;

	for (INT_PTR i = m_arrResultData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		if (point_item.point.x == p.x && point_item.point.y == p.y)
		{
			TRACE(L"[backstep remove item x and y] %d, %d : %s\n", point_item.point.x, point_item.point.y, point_item.name);
			m_arrResultData.RemoveAt(i);
		}
		else
		{
			if (point_item.depth == 0)
				tot_count++;
		}
	}

	for (INT_PTR i = m_arrExcelData.GetSize() - 1; i >= 0; i--)
	{
		POINTITEM point_item = m_arrExcelData.GetAt(i);
		POINTITEM copy_item;
		copy_item.category = point_item.category;
		copy_item.id = point_item.id;
		copy_item.name = point_item.name;
		copy_item.path = point_item.path;
		copy_item.exp = point_item.exp;
		//copy_item.count = point_item.count;
		//copy_item.ratio = category_id.Compare(point_item.id) == 0 ? ratio_count : point_item.ratio;

		int item_count = 0, ratio_count = 0;

		for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
		{
			POINTITEM item_buff = m_arrResultData.GetAt(i);

			if (item_buff.depth != 0)
				continue;

			if (item_buff.path.Compare(copy_item.path) == 0)
				item_count++;

			if (item_buff.id.Compare(copy_item.id) == 0)
				ratio_count++;
		}

		copy_item.count = item_count;
		copy_item.ratio = ratio_count;
		//copy_item.total = total_count;
		copy_item.total = tot_count;

		m_arrExcelData.RemoveAt(i);

		if (item_count > 0)
			m_arrExcelData.InsertAt(i, copy_item);
	}

	for (INT_PTR i = m_arrOutputData.GetSize() - 1; i >= 0; i--)
	{
		TRACEOUTPUT output_data = m_arrOutputData.GetAt(i);

		if (output_data.point.x == p.x && output_data.point.y == p.y)
			m_arrOutputData.RemoveAt(i);
	}

	GetDlgItem(IDC_OUTPUT_EDIT)->SetWindowTextW(L"");
	CString trace_output = L"";

	for (INT_PTR i = 0; i < m_arrOutputData.GetSize(); i++)
	{
		TRACEOUTPUT output_data = m_arrOutputData.GetAt(i);
		CString output_line;

		trace_output += L"\r\n";
		output_line.Format(L"%d x %d: %s", output_data.point.y, output_data.point.x, output_data.navi);

		trace_output += output_line;
	}

	GetDlgItem(IDC_OUTPUT_EDIT)->SetWindowTextW(trace_output);

	CEdit *output_edit = (CEdit *)GetDlgItem(IDC_OUTPUT_EDIT);
	output_edit->LineScroll(output_edit->GetLineCount());

	m_pVPViewerCore->SetGridBoxArray(counter_array);
	jump(p);
}


void CPointCounterDlg::OnBnClickedMenuRestartButton()
{
	// TODO: Add your control notification handler code here
	if (AfxMessageBox(L"Are you sure you want to restart?", MB_YESNO) == IDYES)
	{
		m_pVPViewerCore->SetGridAnalyzeMode(false);

		if (counter_array != nullptr){
			delete[] counter_array;
			delete[] counter_array_buffer;
			counter_array = nullptr;
		}
		TRACE("counter array deleted!\n");

		current_position.x = 1;
		current_position.y = 1;
		privious_position.x = 1;
		privious_position.y = 1;
		counted = 0;
		current_val = 0;

		//CountPosition();
		StartPage();
	}
}


void CPointCounterDlg::OnBnClickedMenuLoadProfileButton()
{
	// TODO: Add your control notification handler code here
	CFileDialog load_dlg(TRUE, NULL, NULL, OFN_NOCHANGEDIR, _T("XML Files(*.xml) | *.xml||"), NULL);
	//CFileDialog file_dialog(TRUE, NULL, _T(""), OFN_NOCHANGEDIR, _T("All files(*.*)|*.*| XML Files(*.xml) | *.xml||"));

	if (load_dlg.DoModal() == IDOK)
	{
		//m_strProfilePath = file_dialog.GetPathName();
		CopyFile(load_dlg.GetPathName(), m_strProfilePath, FALSE);
		szProfilePath = (TCHAR*)(LPCTSTR)m_strProfilePath;
	}
}


void CPointCounterDlg::OnBnClickedMenuLoadTraceButton()
{
	// TODO: Add your control notification handler code here
	CFileDialog load_dlg(TRUE, NULL, _T(""), OFN_NOCHANGEDIR, _T("XML Files(*.xml) | *.xml||"), NULL);
	load_dlg.m_ofn.nFilterIndex = 2;

	if (load_dlg.DoModal() == IDOK)
	{
		//AfxMessageBox(L"load trace.");
		LoadTrace(load_dlg.GetPathName());
	}
}

typedef union{
	struct{
		unsigned char c1, c2, c3;
	};
	struct{
		unsigned int e1 : 6, e2 : 6, e3 : 6, e4 : 6;
	};
} BF;

static int DecodeMimeBase64[256] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 00-0F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 10-1F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,  /* 20-2F */
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,  /* 30-3F */
	-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,  /* 40-4F */
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,  /* 50-5F */
	-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  /* 60-6F */
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,  /* 70-7F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 80-8F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 90-9F */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* A0-AF */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* B0-BF */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* C0-CF */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* D0-DF */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* E0-EF */
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1   /* F0-FF */
};

void base64d(char *src, char *result, int *length){
	int i, j = 0, src_length, blank = 0;
	BF temp;

	src_length = strlen(src);

	for (i = 0; i < src_length; i = i + 4, j = j + 3){
		temp.e4 = DecodeMimeBase64[src[i]];
		temp.e3 = DecodeMimeBase64[src[i + 1]];
		if (src[i + 2] == '='){
			temp.e2 = 0x00;
			blank++;
		}
		else temp.e2 = DecodeMimeBase64[src[i + 2]];
		if (src[i + 3] == '='){
			temp.e1 = 0x00;
			blank++;
		}
		else temp.e1 = DecodeMimeBase64[src[i + 3]];

		result[j] = temp.c3;
		result[j + 1] = temp.c2;
		result[j + 2] = temp.c1;
	}
	*length = j - blank;
}

bool CPointCounterDlg::LoadTrace(const TCHAR* szFile)
{
	IXMLDOMDocument2Ptr pXMLDoc2;

	HRESULT hr;
	hr = pXMLDoc2.CreateInstance(__uuidof(MSXML2::DOMDocument));
	if (S_OK != hr)
	{
		AfxMessageBox(_T("Can not Create XMLDOMDocument Install MSXML6.0"));
		return 0;
	}

	pXMLDoc2->resolveExternals = VARIANT_TRUE;
	pXMLDoc2->async = VARIANT_FALSE;
	pXMLDoc2->validateOnParse = VARIANT_TRUE;

	_variant_t varXml(szFile);
	_variant_t varOut((bool)TRUE);

	varOut = pXMLDoc2->load(szFile);
	if ((bool)varOut == FALSE){
		TCHAR strTemp[100];
		_stprintf_s(strTemp, _countof(strTemp), _T("Can not Read XML File %s\n"), szFile);
		AfxMessageBox(strTemp);
		return S_FALSE;
	}

	POINT size, cur_pos;
	int* counter_array;
	int rotation;
	double scale;
	int interval;


	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	bool bRet = true;

	pRootXMLList = pXMLDoc2->documentElement->getElementsByTagName(_T("Position"));
	pRootXML = pRootXMLList->item[0];

	m_pPointCounterToolPlugIn->GetPosition()->x = GetAttrValueNum(pRootXML, _T("x"), 1);
	m_pPointCounterToolPlugIn->GetPosition()->y = GetAttrValueNum(pRootXML, _T("y"), 1);

	pRootXMLList = pXMLDoc2->documentElement->getElementsByTagName(_T("Size"));
	pRootXML = pRootXMLList->item[0];

	size.x = m_pPointCounterToolPlugIn->GetSize()->x = GetAttrValueNum(pRootXML, _T("width"), 1);
	size.y = m_pPointCounterToolPlugIn->GetSize()->y = GetAttrValueNum(pRootXML, _T("height"), 1);

	pRootXMLList = pXMLDoc2->documentElement->getElementsByTagName(_T("Current"));
	pRootXML = pRootXMLList->item[0];

	cur_pos.x = m_pPointCounterToolPlugIn->GetCurrentPosition()->x = GetAttrValueNum(pRootXML, _T("x"), 1);
	cur_pos.y = m_pPointCounterToolPlugIn->GetCurrentPosition()->y = GetAttrValueNum(pRootXML, _T("y"), 1);
	
	pRootXMLList = pXMLDoc2->documentElement->getElementsByTagName(_T("Status"));
	pRootXML = pRootXMLList->item[0];

	scale = GetAttrValueFloat(pRootXML, _T("Scale"), m_pVPViewerCore->GetScale());
	interval = GetAttrValueNum(pRootXML, _T("Interval"), m_pVPViewerCore->GetInterval());
	rotation = GetAttrValueNum(pRootXML, _T("Rotation"), m_pVPViewerCore->GetRotate());
	counted = GetAttrValueNum(pRootXML, _T("Counted"), 0);

	m_pVPViewerCore->SetGridProperty(0, scale, interval);

	counter_array = new int[size.x * size.y];
	m_pPointCounterToolPlugIn->SetCounterArray(counter_array);

	pRootXMLList = pXMLDoc2->documentElement->getElementsByTagName(_T("GRID"));
	pRootXML = pRootXMLList->item[0];

	int src_size = sizeof(int) * size.x * size.y;
	int dst_size = (4 * (src_size / 3)) + (src_size % 3 ? 4 : 0) + 1;

	TCHAR *buf = new TCHAR[dst_size];
	char *buf2 = new char[dst_size];
	GetAttrValueString(pRootXML, _T("data"), buf, dst_size + 1);

	WideCharToMultiByte(CP_ACP, 0, buf, dst_size, buf2, dst_size, NULL, NULL);

	base64d(buf2, (char*)m_pPointCounterToolPlugIn->GetCounterArray(), &dst_size);

	CString temp_str;
	CPoint grid_size, grid_pos, e_pos;
	RECT e_rect;

	e_rect = m_pVPViewerCore->GetFrameRect();

	grid_pos.x = m_pPointCounterToolPlugIn->GetPosition()->x;
	grid_pos.y = m_pPointCounterToolPlugIn->GetPosition()->y;
	grid_size.x = m_pPointCounterToolPlugIn->GetSize()->x;
	grid_size.y = m_pPointCounterToolPlugIn->GetSize()->y;
	e_pos.x = e_rect.left;
	e_pos.y = e_rect.top;

	temp_str.Format(L"%d", m_pPointCounterToolPlugIn->GetSize()->x);
	m_ctlCellColumnEdit.SetWindowTextW(temp_str);
	temp_str.Format(L"%d", m_pPointCounterToolPlugIn->GetSize()->y);
	m_ctlCellRowEdit.SetWindowTextW(temp_str);
	temp_str.Format(L"%d", m_pPointCounterToolPlugIn->GetPosition()->x);
	m_ctlLocationXEdit.SetWindowTextW(temp_str);
	temp_str.Format(L"%d", m_pPointCounterToolPlugIn->GetPosition()->y);
	m_ctlLocationYEdit.SetWindowTextW(temp_str);

	m_pVPViewerCore->SetGridBoxArray(nullptr);
	m_pVPViewerCore->GridOnButtonDown(1, e_pos);
	m_pVPViewerCore->GridOnButtonUp(1, e_pos);
	m_pVPViewerCore->SetGridBox(grid_pos, grid_size);

	//current_position.x = current_position.y = m_pPointCounterToolPlugIn->GetCurrentPosition()->x = m_pPointCounterToolPlugIn->GetCurrentPosition()->y = 1;

	counted = 0;

	CountPosition();
	//next(false, false);
	//MainPage();

	resize();

	//LoadData(szFile);
	LoadData(pXMLDoc2);

	//next(false, false);
	MainPage();

	/*TRACE(L"\n\ncur pos : %d , %d\n", cur_pos.y, cur_pos.x);

	for (int i = 1; i <= cur_pos.y; i++)
	{
		for (int j = 1; j <= cur_pos.x; j++)
		{
			if (i == cur_pos.y && j == cur_pos.x)
			{
				TRACE(L"main page : %d , %d\n", cur_pos.y, cur_pos.x);

				MainPage();
				return true;
			}
			else
			{
				TRACE(L"next : %d , %d\n", j, i);

				next(IsSkip(CPoint(j, i)));
			}
		}
	}

	TRACE(L"\n");*/
}

bool CPointCounterDlg::IsSkip(CPoint target_pos)
{
	for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);

		if (point_item.point.x == target_pos.x && point_item.point.y == target_pos.y)
		{
			return false;
		}
	}

	return true;
}

void CPointCounterDlg::LoadData(IXMLDOMDocument2Ptr pXMLDoc)
{
	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("Item"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		POINTITEM point_item;

		pRootXML = pRootXMLList->item[i];

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("category");
		if (NULL != pAttrPtr)
			point_item.category = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("id");
		if (NULL != pAttrPtr)
			point_item.id = (LPCTSTR)pAttrPtr->Gettext();

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("level");
		if (NULL != pAttrPtr)
			point_item.level = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("w");
		if (NULL != pAttrPtr)
			point_item.size.cx = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("h");
		if (NULL != pAttrPtr)
			point_item.size.cy = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("x");
		if (NULL != pAttrPtr)
			point_item.point.x = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("y");
		if (NULL != pAttrPtr)
			point_item.point.y = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("order");
		if (NULL != pAttrPtr)
			point_item.order = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("depth");
		if (NULL != pAttrPtr)
			point_item.depth = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("path");
		if (NULL != pAttrPtr)
			point_item.path = (LPCTSTR)pAttrPtr->Gettext();

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("count");
		if (NULL != pAttrPtr)
			point_item.count = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("ratio");
		if (NULL != pAttrPtr)
			point_item.ratio = _ttoi(pAttrPtr->Gettext());

		pAttrPtr = pRootXML->Getattributes()->getNamedItem("total");
		if (NULL != pAttrPtr)
			point_item.total = _ttoi(pAttrPtr->Gettext());

		pChildListXML = pRootXML->GetchildNodes();

		if (pChildListXML) {

			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {

				CString nodeName = pNode2->GetnodeName();

				if (!nodeName.Compare(L"name"))
					point_item.name = (LPCTSTR)pNode2->Gettext();

				if (!nodeName.Compare(L"exp"))
					point_item.exp = (LPCTSTR)pNode2->Gettext();
			}
		}

		if (last_x > 0 && last_y > 0 && (last_x != point_item.point.x || last_y != point_item.point.y))
		{
			jump(CPoint(last_item.point.x, last_item.point.y));

			TRACE(L"load data 1 : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
				last_item.point.x,
				last_item.point.y,
				last_item.category,
				last_item.depth,
				last_item.id,
				last_item.name,
				last_item.path,
				last_item.exp,
				last_item.count,
				last_item.ratio,
				last_item.total
				);

			//TRACE(L"last_category : %d\n", last_category);
			//last_item.navi = navi_text;
			AddOutput(last_item, navi_text);
			navi_text = L"";
			ResetNavigation();
			next(false, false);
		}
		else
		{
			jump(CPoint(point_item.point.x, point_item.point.y));

			TRACE(L"load data 2 : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
				point_item.point.x,
				point_item.point.y,
				point_item.category,
				point_item.depth,
				point_item.id,
				point_item.name,
				point_item.path,
				point_item.exp,
				point_item.count,
				point_item.ratio,
				point_item.total
				);

			//m_arrLastData.Add(point_item);
		}

		AddNavigation(point_item, FALSE);

		if (navi_text.CompareNoCase(L"") != 0)
			navi_text += L" | " + point_item.name;
		else
			navi_text += point_item.name;

		last_item = point_item;
		last_x = point_item.point.x;
		last_y = point_item.point.y;

		//TRACE(L"point cate : %d\n", point_item.category);
	}

	if (last_x > 0 && last_y > 0)
	{
		/*TRACE(L"load data 2 : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
		last_item.point.x,
		last_item.point.y,
		last_item.category,
		last_item.depth,
		last_item.id,
		last_item.name,
		last_item.path,
		last_item.exp,
		last_item.count,
		last_item.ratio,
		last_item.total
		);*/
		jump(CPoint(last_item.point.x, last_item.point.y));
		AddOutput(last_item, navi_text);
		next(false, false);
	}
}

/*
void CPointCounterDlg::LoadData(const TCHAR* szFile)
{
	m_nPointCount = 0;

	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	LPCWSTR node_val2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;

	xml_util.AllocXmlFromFile(szFile);

	TRACE(L"load data xml 1\n");

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	TRACE(L"load data xml 2\n");

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"Item") == 0)
		{
			POINTITEM point_item;

			node_attr = xml_util.FirstAttr(cur_node);

			while (node_attr != NULL)
			{
				attr_name = xml_util.GetName(node_attr);

				if (wcscmp(attr_name, L"category") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.category = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"id") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.id = attr_val;
				}
				if (wcscmp(attr_name, L"level") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.level = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"w") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.size.cx = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"h") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.size.cy = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"x") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.point.x = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"y") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.point.y = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"order") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.order = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"depth") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.depth = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"path") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.path = attr_val;
				}
				if (wcscmp(attr_name, L"count") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.count = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"ratio") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.ratio = _ttoi(attr_val);
				}
				if (wcscmp(attr_name, L"total") == 0)
				{
					attr_val = xml_util.GetValue(node_attr);
					point_item.total = _ttoi(attr_val);
				}

				node_attr = xml_util.NextAttr(node_attr);
			}

			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"name") == 0)
				{
					node_val2 = xml_util.GetValue(cur_node2);
					point_item.name = node_val2;
				}
				if (wcscmp(node_name2, L"exp") == 0)
				{
					node_val2 = xml_util.GetValue(cur_node2);
					point_item.exp = node_val2;
				}
				//if (wcscmp(node_name2, L"navi") == 0)
				//{
				//	node_val2 = xml_util.GetValue(cur_node2);
				//	point_item.navi = node_val2;
				//}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}

			//TRACE(L"last compare : %d, %d\n", last_category, last_item.category);

			if (last_x > 0 && last_y > 0 && (last_x != point_item.point.x || last_y != point_item.point.y))
			{
				jump(CPoint(last_item.point.x, last_item.point.y));

				TRACE(L"load data 1 : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
					last_item.point.x,
					last_item.point.y,
					last_item.category,
					last_item.depth,
					last_item.id,
					last_item.name,
					last_item.path,
					last_item.exp,
					last_item.count,
					last_item.ratio,
					last_item.total
					);

				//TRACE(L"last_category : %d\n", last_category);
				//last_item.navi = navi_text;
				AddOutput(last_item, navi_text);
				navi_text = L"";
				ResetNavigation();
				next(false, false);
			}
			else
			{
				jump(CPoint(point_item.point.x, point_item.point.y));

				TRACE(L"load data 2 : %d, %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
					point_item.point.x,
					point_item.point.y,
					point_item.category,
					point_item.depth,
					point_item.id,
					point_item.name,
					point_item.path,
					point_item.exp,
					point_item.count,
					point_item.ratio,
					point_item.total
					);

				//m_arrLastData.Add(point_item);
			}

			AddNavigation(point_item, FALSE);

			if (navi_text.CompareNoCase(L"") != 0)
				navi_text += L" | " + point_item.name;
			else
				navi_text += point_item.name;

			last_item = point_item;
			last_x = point_item.point.x;
			last_y = point_item.point.y;

			//TRACE(L"point cate : %d\n", point_item.category);
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();

	if (last_x > 0 && last_y > 0)
	{
		jump(CPoint(last_item.point.x, last_item.point.y));
		AddOutput(last_item, navi_text);
		next(false, false);
	}
}
*/

void CPointCounterDlg::Endup()
{
	m_pPointCounterToolPlugIn->EndupProcess(FALSE);
}

void CPointCounterDlg::GetMax(int prc_type)
{
	switch (prc_type)
	{
	case 1:
		m_nPrcMax = m_arrResultData.GetSize();
		break;
	case 2:
		m_nPrcMax = m_arrResultData.GetSize();
		break;
	case 3:
	case 5:
	case 7:
		m_nPrcMax = m_arrResultData.GetSize();
		m_nPrcMax += m_arrExcelData.GetSize();
		m_nPrcMax += m_arrProfileData.GetSize();
		m_nPrcMax += m_arrResultData.GetSize();
		m_nPrcMax += size.x * size.y;
		break;
	}
}

bool CPointCounterDlg::SaveTrace(const TCHAR *szFile)
{
	FILE *fp = _tfopen(szFile, _T("w+t"));
	if (NULL == fp)
		return false;

	InsertHeader(fp);
	InsertGRID(fp);
	InsertPath(fp);
	InsertTale(fp);

	fclose(fp);

	return true;
}

void CPointCounterDlg::InsertPath(FILE* fp)
{
	CString item_path = L"";

	for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
	{
		POINTITEM res_item = m_arrResultData.GetAt(i);
		_ftprintf(
			fp
			, _T("<Item category='%d' id='%s' level='%d' w='%d' h='%d' x='%d' y='%d' order='%d' depth='%d' path='%s' count='%d' ratio='%d' total='%d'><name>%s</name><exp>%s</exp><navi>%s</navi></Item>")
			, res_item.category
			, res_item.id
			, res_item.level
			, res_item.size.cx
			, res_item.size.cy
			, res_item.point.x
			, res_item.point.y
			, res_item.order
			, res_item.depth
			, res_item.path
			, res_item.count
			, res_item.ratio
			, res_item.total
			, res_item.name
			, res_item.exp
			, res_item.navi
			);

		m_ProgressDlg->ProgressPos();
	}

	/*
	for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
	{
		res_item = m_arrResultData.GetAt(i);

		if (res_item.depth <= 0)
		{
			if (item_path.CompareNoCase(L"") != 0)
				_ftprintf(fp, _T("<Path x=\"%d\" y=\"%d\" val=\"%s\"/>"), res_item.point.x, res_item.point.y, item_path);

			item_path = L"";
		}
		else
		{
			item_path += L"|" + res_item.id;
		}
	}

	if (item_path.CompareNoCase(L"") != 0)
		_ftprintf(fp, _T("<Path x=\"%d\" y=\"%d\" val=\"%s\"/>"), res_item.point.x, res_item.point.y, item_path);
	*/
}

void CPointCounterDlg::InsertHeader(FILE* fp)
{
	_ftprintf(fp, _T("<?xml version=\"1.0\"?> \n"));
	_ftprintf(fp, _T("<Trace>\n"));
	_ftprintf(fp, _T("<Version> %d </Version>\n"), 1);
	_ftprintf(fp, _T("<Position x=\"%d\" y=\"%d\"/>"), position.x, position.y);
	_ftprintf(fp, _T("<Size width=\"%d\" height=\"%d\"/>"), size.x, size.y);
	_ftprintf(fp, _T("<Current x=\"%d\" y=\"%d\"/>"), current_position.x, current_position.y);

	_ftprintf(fp, _T("<Status Rotation = \"%d\" "), m_pVPViewerCore->GetRotate());
	_ftprintf(fp, _T("Scale = \"%f\" "), m_pVPViewerCore->GetScale());
	_ftprintf(fp, _T("Interval = \"%d\" "), m_pVPViewerCore->GetInterval());
	_ftprintf(fp, _T("Counted = \"%d\""), counted);	
	_ftprintf(fp, _T("/>\n"));

}

void CPointCounterDlg::InsertTale(FILE* fp)
{
	_ftprintf(fp, _T("</Trace>"));
}

static const char MimeBase64[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/'
};
/*
typedef union{
	struct{
		unsigned char c1, c2, c3;
	};
	struct{
		unsigned int e1 : 6, e2 : 6, e3 : 6, e4 : 6;
	};
} BF;*/

void base64e(char *src, char *result, int length){
	int i, j = 0;
	BF temp;

	for (i = 0; i < length; i = i + 3, j = j + 4){
		temp.c3 = src[i];
		if ((i + 1) > length) temp.c2 = 0x00;
		else temp.c2 = src[i + 1];
		if ((i + 2) > length) temp.c1 = 0x00;
		else temp.c1 = src[i + 2];

		result[j] = MimeBase64[temp.e4];
		result[j + 1] = MimeBase64[temp.e3];
		result[j + 2] = MimeBase64[temp.e2];
		result[j + 3] = MimeBase64[temp.e1];

		if ((i + 2) > length) result[j + 2] = '=';
		if ((i + 3) > length) result[j + 3] = '=';
	}
}

void CPointCounterDlg::InsertGRID(FILE* fp)
{
	_ftprintf(fp, _T("<Type> %s </Type>\n"), _T("integer"));
	_ftprintf(fp, _T("<GRID>\n"));
	for (int y = 0; y < size.y; y++)
	{
		for (int x = 0; x < size.x; x++)
		{
			_ftprintf(fp, _T("<item row=\"%d\" column=\"%d\">  %d </item>\n"), y + 1, x + 1, counter_array_buffer[y*size.x + x]);
		}
	}
	_ftprintf(fp, _T("</GRID>\n"));
}

/*
void CPointCounterDlg::InsertGRID(FILE* fp)
{
	size_t src_size = sizeof(int) * size.x * size.y;
	size_t dst_size = (4 * (src_size / 3)) + (src_size % 3 ? 4 : 0) + 1;

	char *coded_buffer = new char[dst_size];

	base64e((char*)counter_array_buffer, coded_buffer, src_size);

	_ftprintf(fp, _T("<GRID data = \""));
	fwrite(coded_buffer, sizeof(char), dst_size - 1, fp);
	_ftprintf(fp, _T(" \"/>\n"));
}
*/

void CPointCounterDlg::OnBnClickedMenuSaveTraceButton()
{
	// TODO: Add your control notification handler code here
	//CString m_strPath;
	CStdioFile file;
	// CFile file;


	m_strPath.Format(L"%s_grid_%d_%d_to_%d_%d.xml", m_pVPViewerCore->GetFileName(), position.x, position.y, position.x + size.x, position.y + size.y);
	CFileDialog dlg(FALSE, _T("*.xml"), m_strPath, OFN_OVERWRITEPROMPT, _T("XML files(*.xml)|*.xml|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".xml")
		{
			m_strPath += ".xml";
		}

		//SaveTrace(m_strPath);

		if (m_ProgressDlg == NULL)
		{
			m_ProgressDlg = new CProgressDialog(this);
			m_ProgressDlg->Create(IDD_DIALOG_SAVE, this);
			m_ProgressDlg->m_PCDlg = this;
		}

		m_ProgressDlg->PrcType(1);
		m_ProgressDlg->InitProgress();
		m_ProgressDlg->ShowWindow(SW_SHOW);
		this->BeginModalState();
		m_ProgressDlg->StartProgress();
	}
}


void CPointCounterDlg::OnBnClickedStatusCrosshairOnRadio()
{
	// TODO: Add your control notification handler code here
	CButton *crosshair_on = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_ON_RADIO);
	crosshair_on->SetCheck(1);
	CButton *crosshair_off = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_OFF_RADIO);
	crosshair_off->SetCheck(0);
	m_pVPViewerCore->SetGridBoxArray(counter_array);
	m_pVPViewerCore->SetGridRedCrossHair(true);
}


void CPointCounterDlg::OnBnClickedStatusCrosshairOffRadio()
{
	// TODO: Add your control notification handler code here
	CButton *crosshair_on = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_ON_RADIO);
	crosshair_on->SetCheck(0);
	CButton *crosshair_off = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_OFF_RADIO);
	crosshair_off->SetCheck(1);
	m_pVPViewerCore->SetGridBoxArray(counter_array_buffer);
	m_pVPViewerCore->SetGridRedCrossHair(false);
}


void CPointCounterDlg::OnBnClickedMenuSameAsLastButton()
{
	// TODO: Add your control notification handler code here
	if (m_arrLastData.GetSize() <= 0)
	{
		AfxMessageBox(L"no buffer.");
		return;
	}

	CString count_text;
	CString navi_text;
	CString output_text;

	GetDlgItem(IDC_OUTPUT_EDIT)->GetWindowTextW(output_text);
	output_text += L"\r\n";

	m_nPointCount++;

	count_text.Format(L"%d x %d: ", current_position.y, current_position.x);
	output_text += count_text;

	for (INT_PTR i = 0; i < m_arrLastData.GetSize(); i++)
	{
		POINTITEM point_item = m_arrLastData.GetAt(i);
		point_item.point.x = current_position.x;
		point_item.point.y = current_position.y;

		//m_arrResultData.Add(point_item);

		//AddOutput(point_item);

		TRACE(L"\n\n[same as last] last data : %d, %d, %s\n\n", point_item.point.x, point_item.point.y, point_item.navi);

		//m_arrResultData.Add(point_item);

		if (wcscmp(navi_text, L"") != 0)
			navi_text += " | ";

		navi_text += point_item.name;

		if (i == m_arrLastData.GetSize() - 1)
			AddOutput(point_item);
		else
			AddNavigation(point_item, FALSE);
	}

	GetDlgItem(IDC_OUTPUT_EDIT)->SetWindowTextW(output_text + navi_text);

	/*for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
	{
		POINTITEM point_item = m_arrResultData.GetAt(i);
		TRACE(L"\n\n[same as last] res data : %d, %d, %s\n\n", point_item.point.x, point_item.point.y, point_item.navi);
	}*/

	next(false);
	MainPage();
}


void CPointCounterDlg::OnBnClickedMenuSaveResultButton()
{
	// TODO: Add your control notification handler code here
	SaveResult();
}

BOOL CPointCounterDlg::IsProfile(POINTITEM source_item)
{
	BOOL is_profile = TRUE;



	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("my-edges"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyEdge"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				pAttrPtr = pNode2->Getattributes()->getNamedItem("source");
				if (NULL != pAttrPtr)
				{
					CString _id = (LPCTSTR)pAttrPtr->Gettext();

					if (wcscmp(_id, source_item.id) == 0)
						is_profile = FALSE;
				}
			}
		}
	}



	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;

	xml_util.AllocXmlFromFile(m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-edges") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyEdge") == 0)
				{
					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"source") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);

							if (wcscmp(attr_val, source_item.id) == 0)
								is_profile = FALSE;

							break;
						}

						node_attr = xml_util.NextAttr(node_attr);
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();
	*/

	return is_profile;
}

void CPointCounterDlg::ProfileData(POINTITEM source_item)
{
	BOOL is_profile = TRUE;



	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = pXMLDoc->documentElement->getElementsByTagName(_T("my-edges"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyEdge"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				POINTITEM point_item;

				pAttrPtr = pNode2->Getattributes()->getNamedItem("source");
				if (NULL != pAttrPtr)
				{
					point_item.id = (LPCTSTR)pAttrPtr->Gettext();

					if (wcscmp(point_item.id, source_item.id) == 0)
					{
						pAttrPtr = pNode2->Getattributes()->getNamedItem("target");
						if (NULL != pAttrPtr)
						{
							point_item.id = (LPCTSTR)pAttrPtr->Gettext();

							POINTITEM buf_item = GetItem(point_item.id);

							point_item.category = source_item.category;
							point_item.name = source_item.name;
							point_item.path = source_item.path + point_item.id + L".";
							point_item.exp = source_item.exp + L";" + buf_item.name;

							is_profile = FALSE;

							ProfileData(point_item);
						}
					}
				}
			}
		}
	}

	if (is_profile == TRUE)
		m_arrProfileData.Add(source_item);


	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;

	xml_util.AllocXmlFromFile(m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-edges") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyEdge") == 0)
				{
					POINTITEM point_item;

					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"source") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);
							point_item.id = attr_val;
							break;
						}

						node_attr = xml_util.NextAttr(node_attr);
					}

					if (wcscmp(point_item.id, source_item.id) == 0)
					{
						node_attr2 = xml_util.FirstAttr(cur_node2);

						while (node_attr2 != NULL)
						{
							attr_name2 = xml_util.GetName(node_attr2);

							if (wcscmp(attr_name2, L"target") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.id = attr_val2;
								break;
							}

							node_attr2 = xml_util.NextAttr(node_attr2);
						}

						POINTITEM buf_item = GetItem(point_item.id);

						point_item.category = source_item.category;
						point_item.name = source_item.name;
						point_item.path = source_item.path + point_item.id + L".";
						point_item.exp = source_item.exp + L";" + buf_item.name;

						is_profile = FALSE;

						ProfileData(point_item);
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();

	if (is_profile == TRUE)
		m_arrProfileData.Add(source_item);
	*/
}

/*
int CPointCounterDlg::InitXml()
{
	int ret_code = 0;

	tinyxml2::XMLDocument xml_doc;

	char doc_path[2048];
	WideCharToMultiByte(CP_ACP, 0, m_strProfilePath, 2048, doc_path, 2048, NULL, NULL);

	TRACE("doc_path : %s\n", doc_path);

	if (xml_doc.LoadFile(doc_path) != tinyxml2::XML_SUCCESS)
	{
		MessageBoxA(NULL, "XML_ERROR1", "Xml Error", MB_OK);
		return -1;
	}

	tinyxml2::XMLElement *xml_root = xml_doc.RootElement();
	if (xml_root == nullptr) ret_code = 1;
	//if (xml_root == nullptr) ret_code = tinyxml2::XML_ERROR_FILE_READ_ERROR;

	//tinyxml2::XMLElement *xml_el = xml_root->FirstChildElement();
	tinyxml2::XMLElement *xml_el = xml_root->FirstChildElement();
	if (xml_el == nullptr) ret_code = 2;
	//if (xml_el == nullptr) ret_code = tinyxml2::XML_ERROR_FILE_READ_ERROR;

	while (xml_el != nullptr)
	{
		if (strcmp(xml_el->Name(), "MyEdge") == 0)
		{
			//m_strCloseApp = main_win->ConvEnc(xml_el->GetText());

			const char *source_val = xml_el->Attribute("source");

			TRACE("source_val : %s\n", xml_el->Attribute("source"));

			if (strcmp(source_val, "0") == 0)
			{
				POINTITEM point_item;
				point_item.id = source_val;


			}
		}

		xml_el = xml_el->NextSiblingElement();
	}

	TRACE("ret_code : %d\n", ret_code);

	return ret_code;
}
*/

//void CPointCounterDlg::ProfileData()
UINT CPointCounterDlg::ThreadFirst(LPVOID _mothod)
{
	CPointCounterDlg *_this = (CPointCounterDlg*)_mothod;

	int category_idx = 0;



	IXMLDOMNodeListPtr	pRootXMLList, pChildListXML;
	IXMLDOMElementPtr	pRootXML, pChildXML;
	IXMLDOMAttributePtr pAttrPtr;

	int last_x = 0, last_y = 0;
	POINTITEM last_item;
	CString navi_text = L"";

	pRootXMLList = _this->pXMLDoc->documentElement->getElementsByTagName(_T("my-edges"));
	//pRootXML = pRootXMLList->item[0];

	for (int i = 0; i < pRootXMLList->Getlength(); i++)
	{
		pRootXML = pRootXMLList->item[i];

		pChildListXML = pRootXML->getElementsByTagName(_T("MyEdge"));

		if (pChildListXML) {
			IXMLDOMElementPtr pNode2 = NULL;

			while ((pNode2 = pChildListXML->nextNode())) {
				POINTITEM point_item;

				pAttrPtr = pNode2->Getattributes()->getNamedItem("source");
				if (NULL != pAttrPtr)
				{
					point_item.id = (LPCTSTR)pAttrPtr->Gettext();

					if (wcscmp(point_item.id, L"0") == 0)
					{
						pAttrPtr = pNode2->Getattributes()->getNamedItem("target");
						if (NULL != pAttrPtr)
						{
							point_item.id = (LPCTSTR)pAttrPtr->Gettext();

							POINTITEM buf_item = _this->GetItem(point_item.id);

							point_item.category = category_idx;
							point_item.name = buf_item.name;
							point_item.path = point_item.id + L".";
							point_item.exp = buf_item.name;

							//AfxMessageBox(buf_item.name);

							_this->ProfileData(point_item);

							category_idx++;
						}
					}
				}
			}
		}
	}


	/*
	CRaipidXmlHelper xml_util;
	CRaipidXmlHelper::HANDLE_NODE cur_node = NULL;
	CRaipidXmlHelper::HANDLE_NODE cur_node2 = NULL;
	LPCWSTR node_name = NULL;
	LPCWSTR node_name2 = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr = NULL;
	CRaipidXmlHelper::HANDLE_ATTR node_attr2 = NULL;
	LPCWSTR attr_name = NULL;
	LPCWSTR attr_name2 = NULL;
	LPCWSTR attr_val = NULL;
	LPCWSTR attr_val2 = NULL;

	xml_util.AllocXmlFromFile(_this->m_strProfilePath);

	cur_node = xml_util.GetRoot();
	cur_node = xml_util.FirstNode(cur_node);

	while (cur_node != NULL)
	{
		node_name = xml_util.GetName(cur_node);

		if (wcscmp(node_name, L"my-edges") == 0)
		{
			cur_node2 = xml_util.FirstNode(cur_node);

			while (cur_node2 != NULL)
			{
				node_name2 = xml_util.GetName(cur_node2);

				if (wcscmp(node_name2, L"MyEdge") == 0)
				{
					POINTITEM point_item;

					node_attr = xml_util.FirstAttr(cur_node2);

					while (node_attr != NULL)
					{
						attr_name = xml_util.GetName(node_attr);

						if (wcscmp(attr_name, L"source") == 0)
						{
							attr_val = xml_util.GetValue(node_attr);
							point_item.id = attr_val;
							break;
						}

						node_attr = xml_util.NextAttr(node_attr);
					}

					if (wcscmp(point_item.id, L"0") == 0)
					{
						node_attr2 = xml_util.FirstAttr(cur_node2);

						while (node_attr2 != NULL)
						{
							attr_name2 = xml_util.GetName(node_attr2);

							if (wcscmp(attr_name2, L"target") == 0)
							{
								attr_val2 = xml_util.GetValue(node_attr2);
								point_item.id = attr_val2;

								POINTITEM buf_item = _this->GetItem(point_item.id);

								point_item.category = category_idx;
								point_item.name = buf_item.name;
								point_item.path = point_item.id + L".";
								point_item.exp = buf_item.name;

								_this->ProfileData(point_item);

								break;
							}

							node_attr2 = xml_util.NextAttr(node_attr2);
						}

						category_idx++;
					}
				}

				cur_node2 = xml_util.NextSibling(cur_node2);
			}
		}

		cur_node = xml_util.NextSibling(cur_node);
	}

	xml_util.DeAlloc();
	*/

	return 0;
}


int CPointCounterDlg::SaveResult()
{
	if (m_arrResultData.GetSize() <= 0)
	{
		AfxMessageBox(L"No Data.");
		return -100;
	}

	/*CFileDialog save_dlg(FALSE, _T(".xls"), NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Excel Files(*.xls) | *.xls||"), NULL);

	if (save_dlg.DoModal() == IDOK)
	{
		CString save_path = save_dlg.GetPathName();
		SaveResult(save_path);
	}*/

	OnBnClickedButtonEndanalysis();
}


int CPointCounterDlg::SaveResult(CString save_path)
{
	TRACE(L"save result!!!\n");

	libxl::Book* book = xlCreateBook(); // xlCreateXMLBook() for xlsx

	if (book)
	{
		for (INT_PTR i = 0; i < m_arrProfileData.GetSize(); i++)
		{
			POINTITEM point_item = m_arrProfileData.GetAt(i);
			TRACE(L"save result -- profile data : %d, %s, %s, %s, %s, %d, %d, %d\n",
				point_item.category,
				point_item.id,
				point_item.name,
				point_item.path,
				point_item.exp,
				point_item.count,
				point_item.ratio,
				point_item.total
				);
		}

		double inch_fix = 1.2;
		unsigned int cur_row = 0;
		bool order_ascending = true;
		int total_count = 0;

		std::sort(m_arrExcelData.GetData(), m_arrExcelData.GetData() + m_arrExcelData.GetSize(),
			[order_ascending]
			(const POINTITEM& left, const POINTITEM& right)
			{ return order_ascending ? (left.category < right.category) : (left.category > right.category); }
			);

		for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
		{
			POINTITEM point_item = m_arrResultData.GetAt(i);
			TRACE(L"save result -- res data : %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
				point_item.point.x,
				point_item.point.y,
				point_item.category,
				point_item.id,
				point_item.name,
				point_item.path,
				point_item.exp,
				point_item.count,
				point_item.ratio,
				point_item.total
				);
		}

		for (INT_PTR i = 0; i < m_arrExcelData.GetSize(); i++)
		{
			POINTITEM point_item = m_arrExcelData.GetAt(i);
			TRACE(L"save result -- excel data : %d, %d, %d, %s, %s, %s, %s, %d, %d, %d\n",
				point_item.point.x,
				point_item.point.y,
				point_item.category,
				point_item.id,
				point_item.name,
				point_item.path,
				point_item.exp,
				point_item.count,
				point_item.ratio,
				point_item.total
				);
		}

		book->setKey(L"Jong Min Shin", L"windows-2729230003cce6046cb46762a2k1h4lb");

		libxl::Font* bold_font = book->addFont();
		bold_font->setBold(true);

		libxl::Format* subject_format = book->addFormat();
		subject_format->setBorder(libxl::BORDERSTYLE_NONE);
		subject_format->setFillPattern(libxl::FILLPATTERN_SOLID);
		subject_format->setPatternForegroundColor(book->colorPack(255, 153, 0));
		subject_format->setFont(bold_font);

		libxl::Format* subject_format2 = book->addFormat();
		subject_format2->setBorder(libxl::BORDERSTYLE_NONE);
		subject_format2->setFillPattern(libxl::FILLPATTERN_SOLID);
		subject_format2->setPatternForegroundColor(book->colorPack(255, 255, 204));
		subject_format2->setFont(bold_font);

		libxl::Format* content1_format = book->addFormat();
		content1_format->setBorder(libxl::BORDERSTYLE_NONE);
		content1_format->setFillPattern(libxl::FILLPATTERN_SOLID);
		content1_format->setPatternForegroundColor(book->colorPack(204, 255, 204));

		libxl::Format* content1_format_bold = book->addFormat();
		content1_format_bold->setBorder(libxl::BORDERSTYLE_NONE);
		content1_format_bold->setFillPattern(libxl::FILLPATTERN_SOLID);
		content1_format_bold->setPatternForegroundColor(book->colorPack(204, 255, 204));
		content1_format_bold->setFont(bold_font);

		libxl::Format* content2_format = book->addFormat();
		content2_format->setBorder(libxl::BORDERSTYLE_NONE);
		content2_format->setFillPattern(libxl::FILLPATTERN_SOLID);
		content2_format->setPatternForegroundColor(book->colorPack(204, 255, 255));

		libxl::Format* content2_format_bold = book->addFormat();
		content2_format_bold->setBorder(libxl::BORDERSTYLE_NONE);
		content2_format_bold->setFillPattern(libxl::FILLPATTERN_SOLID);
		content2_format_bold->setPatternForegroundColor(book->colorPack(204, 255, 255));
		content2_format_bold->setFont(bold_font);

		libxl::Format* res_format = book->addFormat();
		res_format->setBorder(libxl::BORDERSTYLE_NONE);
		res_format->setFillPattern(libxl::FILLPATTERN_SOLID);
		res_format->setPatternForegroundColor(book->colorPack(255, 255, 0));
		res_format->setFont(bold_font);

		libxl::Sheet* sheet1 = book->addSheet(L"Simple Statistics");

		if (sheet1)
		{
			sheet1->writeStr(cur_row, 0, L"Category", subject_format);
			sheet1->writeStr(cur_row, 1, L"Name", subject_format);
			sheet1->writeStr(cur_row, 2, L"Count", subject_format);
			sheet1->writeStr(cur_row, 3, L"Sub-Ratio", subject_format);
			sheet1->writeStr(cur_row, 4, L"Total (%)", subject_format);

			sheet1->setCol(0, 0, 12.4 * inch_fix);
			sheet1->setCol(1, 1, 26.8 * inch_fix);
			sheet1->setCol(2, 2, 7 * inch_fix);
			sheet1->setCol(3, 3, 7 * inch_fix);
			sheet1->setCol(4, 4, 7 * inch_fix);

			cur_row++;

			int item_category = -1;
			int item_count = 0;
			double total_per = 0;

			for (INT_PTR i = 0; i < m_arrExcelData.GetSize(); i++)
			{
				POINTITEM excel_item = m_arrExcelData.GetAt(i);

				if (item_category >= 0 && item_category != excel_item.category)
				{
					sheet1->writeStr(cur_row, 0, L"Sub Total", res_format);
					sheet1->writeStr(cur_row, 1, L"", res_format);
					sheet1->writeNum(cur_row, 2, item_count, res_format);
					sheet1->writeNum(cur_row, 3, 100, res_format);
					sheet1->writeNum(cur_row, 4, item_count * 100 / excel_item.total, res_format);
					cur_row += 2;

					item_count = 0;
				}

				sheet1->writeStr(cur_row, 0, excel_item.name, content1_format_bold);
				sheet1->writeStr(cur_row, 1, excel_item.exp, content1_format);
				sheet1->writeNum(cur_row, 2, excel_item.count, content1_format);
				sheet1->writeNum(cur_row, 3, excel_item.count * 100 / excel_item.ratio, content1_format);
				sheet1->writeNum(cur_row, 4, excel_item.count * 100 / excel_item.total, content1_format);
				cur_row++;

				item_category = excel_item.category;
				item_count += excel_item.count;
				total_count = excel_item.total;

				m_ProgressDlg->ProgressPos();
			}

			if (total_count > 0)
			{
				sheet1->writeStr(cur_row, 0, L"Sub Total", res_format);
				sheet1->writeStr(cur_row, 1, L"", res_format);
				sheet1->writeNum(cur_row, 2, item_count, res_format);
				sheet1->writeNum(cur_row, 3, 100, res_format);
				sheet1->writeNum(cur_row, 4, item_count * 100 / total_count, res_format);
				cur_row += 2;
			}

			sheet1->writeStr(cur_row, 0, L"Total", res_format);
			sheet1->writeStr(cur_row, 1, L"", res_format);
			sheet1->writeNum(cur_row, 2, total_count, res_format);
			sheet1->writeStr(cur_row, 3, L"", res_format);
			sheet1->writeNum(cur_row, 4, 100, res_format);
		}

		libxl::Sheet* sheet2 = book->addSheet(L"Full Statistics");
		libxl::Sheet* sheet3 = book->addSheet(L"Profile Count");

		if (sheet2 && sheet3)
		{
			unsigned int cur_row1 = 0;
			unsigned int cur_row2 = 0;

			sheet2->writeStr(cur_row1, 0, L"Category", subject_format);
			sheet2->writeStr(cur_row1, 1, L"Name", subject_format);
			sheet2->writeStr(cur_row1, 2, L"Count", subject_format);
			sheet2->writeStr(cur_row1, 3, L"Sub-Ratio", subject_format);
			sheet2->writeStr(cur_row1, 4, L"Total (%)", subject_format);

			sheet2->setCol(0, 0, 7 * inch_fix);
			sheet2->setCol(1, 1, 36.8 * inch_fix);
			sheet2->setCol(2, 2, 7 * inch_fix);
			sheet2->setCol(3, 3, 7 * inch_fix);
			sheet2->setCol(4, 4, 7 * inch_fix);

			sheet3->writeStr(cur_row2, 0, L"Level1", subject_format);
			sheet3->writeStr(cur_row2, 1, L"Level2", subject_format);
			sheet3->writeStr(cur_row2, 2, L"Level3", subject_format);
			sheet3->writeStr(cur_row2, 3, L"Count", subject_format);

			sheet3->setCol(0, 0, 7 * inch_fix);
			sheet3->setCol(1, 1, 7 * inch_fix);
			sheet3->setCol(2, 2, 23.5 * inch_fix);
			sheet3->setCol(3, 3, 7 * inch_fix);

			cur_row1++;

			int coutent_count = 1;
			int item_category = -1;
			int item_count = 0;
			int ratio_count = 0;
			double total_per = 0;

			for (INT_PTR i = 0; i < m_arrProfileData.GetSize(); i++)
			{
				POINTITEM profile_item = m_arrProfileData.GetAt(i);

				if (item_category >= 0 && item_category != profile_item.category)
				{
					sheet2->writeStr(cur_row1, 0, L"Sub Total", res_format);
					sheet2->writeStr(cur_row1, 1, L"", res_format);
					sheet2->writeNum(cur_row1, 2, ratio_count, res_format);
					sheet2->writeNum(cur_row1, 3, ratio_count == 0 ? 0 : 100, res_format);
					sheet2->writeNum(cur_row1, 4, (ratio_count == 0 || total_count == 0) ? 0 : ratio_count * 100 / total_count, res_format);
					cur_row1 += 2;

					ratio_count = 0;

					coutent_count++;
				}

				item_count = 0;

				for (INT_PTR j = 0; j < m_arrExcelData.GetSize(); j++)
				{
					POINTITEM excel_item = m_arrExcelData.GetAt(j);
					TRACE(L"[res excel path compare] %s : %s\n", profile_item.path, excel_item.path);

					if (profile_item.path.Compare(excel_item.path) == 0)
					{
						item_count = excel_item.count;
						//ratio_count += excel_item.ratio;
						ratio_count = excel_item.ratio;
						break;
					}
				}

				sheet2->writeStr(cur_row1, 0, profile_item.name, content1_format_bold);
				sheet2->writeStr(cur_row1, 1, profile_item.exp, content1_format);
				sheet2->writeNum(cur_row1, 2, item_count, content1_format);
				sheet2->writeNum(cur_row1, 3, (item_count == 0 || ratio_count == 0) ? 0 : item_count * 100 / ratio_count, content1_format);
				sheet2->writeNum(cur_row1, 4, (item_count == 0 || total_count == 0) ? 0 : item_count * 100 / total_count, content1_format);
				cur_row1++;

				CString strTok;
				int sepCount = GetCountChar(profile_item.exp, ';');   // " , " 의 갯수를 세어온다.
				CString* temp1 = new CString[sepCount + 1]; // 구해온 갯수만큼 동적 배열을 할당(CString배열)

				int cnt = 0;
				while (AfxExtractSubString(strTok, profile_item.exp, cnt, ';'))  // 문자를 잘라준다. (AfxExtractSubString = strTok)
					temp1[cnt++] = strTok;                                       // 해당 배열에 순차적으로 넣어준다.

				//for (int i = 0; i<sepCount + 1; i++)
					//cout << (LPCSTR)temp1[i] << endl;                  // 화면에 출력한다.

				sheet3->writeStr(cur_row2, 0, temp1[0], coutent_count % 2 == 0 ? content1_format_bold : content2_format_bold);
				sheet3->writeStr(cur_row2, 1, temp1[1], coutent_count % 2 == 0 ? content1_format : content2_format);
				sheet3->writeStr(cur_row2, 2, sepCount > 1 ? temp1[2] : L"", coutent_count % 2 == 0 ? content1_format : content2_format);
				sheet3->writeNum(cur_row2, 3, item_count, coutent_count % 2 == 0 ? content1_format : content2_format);
				cur_row2++;

				delete[] temp1;

				//delete [] path_exp;

				item_category = profile_item.category;

				m_ProgressDlg->ProgressPos();
			}

			sheet2->writeStr(cur_row1, 0, L"Sub Total", res_format);
			sheet2->writeStr(cur_row1, 1, L"", res_format);
			sheet2->writeNum(cur_row1, 2, ratio_count, res_format);
			sheet2->writeNum(cur_row1, 3, 100, res_format);
			sheet2->writeNum(cur_row1, 4, (ratio_count == 0 || total_count == 0) ? 0 : ratio_count * 100 / total_count, res_format);
			//cur_row += 2;
			cur_row1 += 2;

			sheet2->writeStr(cur_row1, 0, L"Total", res_format);
			sheet2->writeStr(cur_row1, 1, L"", res_format);
			sheet2->writeNum(cur_row1, 2, total_count, res_format);
			sheet2->writeStr(cur_row1, 3, L"", res_format);
			sheet2->writeNum(cur_row1, 4, 100, res_format);

			sheet3->writeStr(cur_row2, 0, L"Total", res_format);
			sheet3->writeStr(cur_row2, 1, L"", res_format);
			sheet3->writeStr(cur_row2, 2, L"", res_format);
			sheet3->writeNum(cur_row2, 3, total_count, res_format);
		}

		libxl::Sheet* sheet4 = book->addSheet(L"Trace");

		if (sheet4)
		{
			unsigned int trace_no = 1;
			unsigned int cur_row = 0;
			CString count_str = L"";
			CString level1_name = L"";
			CString level2_name = L"";
			CString level3_name = L"";

			sheet4->writeStr(cur_row, 0, L"Count", subject_format2);
			sheet4->writeStr(cur_row, 1, L"Level1", subject_format2);
			sheet4->writeStr(cur_row, 2, L"Level2", subject_format2);
			sheet4->writeStr(cur_row, 3, L"Level3", subject_format2);

			sheet4->setCol(0, 0, 7 * inch_fix);
			sheet4->setCol(1, 1, 11.5 * inch_fix);
			sheet4->setCol(2, 2, 15.1 * inch_fix);
			sheet4->setCol(3, 3, 12 * inch_fix);

			cur_row++;

			for (INT_PTR i = 0; i < m_arrResultData.GetSize(); i++)
			{
				POINTITEM res_item = m_arrResultData.GetAt(i);

				if (res_item.depth <= 0)
				{
					if (level1_name.CompareNoCase(L"") != 0)
					{
						sheet4->writeStr(cur_row, 0, count_str);
						sheet4->writeStr(cur_row, 1, level1_name);
						sheet4->writeStr(cur_row, 2, level2_name);
						sheet4->writeStr(cur_row, 3, level3_name);
						cur_row++;
						trace_no++;

						level1_name = L"";
						level2_name = L"";
						level3_name = L"";
					}

					count_str.Format(L"%ld x %ld", res_item.point.y, res_item.point.x);
					level1_name = res_item.name;
				}
				else if (res_item.depth == 1)
				{
					level2_name = res_item.name;
				}
				else if (res_item.depth == 2)
				{
					level3_name = res_item.name;
				}

				m_ProgressDlg->ProgressPos();
			}

			if (count_str.CompareNoCase(L"") != 0)
			{
				sheet4->writeStr(cur_row, 0, count_str);
				sheet4->writeStr(cur_row, 1, level1_name);
				sheet4->writeStr(cur_row, 2, level2_name);
				sheet4->writeStr(cur_row, 3, level3_name);
			}
		}

		book->save(save_path);
		book->release();
	}

	return 0;
}


void CPointCounterDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	// TODO: Add your message handler code here
	if (bShow)
	{
		m_pVPViewerCore->SetAnalyzeMode(true);
		CPointCounterBaseDlg::OnShowWindow(bShow, nStatus);
		StartPage();
	}
	else
		m_pVPViewerCore->SetAnalyzeMode(false);
	/*else
	{
		if (AfxMessageBox(L"Exit Point Counter?", MB_YESNO) == IDYES)
		{
			CPointCounterBaseDlg::OnShowWindow(bShow, nStatus);
			DisableMenu();
		}
	}*/
}


void CPointCounterDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	if (AfxMessageBox(L"Exit Point Counter?", MB_YESNO) == IDYES)
	{
		m_pVPViewerCore->SetGridAnalyzeMode(false);
		m_pVPViewerCore->SetAnalyzeMode(false);
		m_pPointCounterToolPlugIn->EndupProcess();

		CPointCounterBaseDlg::OnClose();
	}
}


void CPointCounterDlg::EnableMenu()
{
	/*for (INT_PTR i = 0; i < control_ids.GetSize(); i++)
	{
		if (AfxGetMainWnd()->GetDlgItem(control_ids.GetAt(i))->GetSafeHwnd())
			AfxGetMainWnd()->GetDlgItem(control_ids.GetAt(i))->EnableWindow(TRUE);
	}*/
}


void CPointCounterDlg::DisableMenu()
{
	CArray<int> control_ids;
	control_ids.Add(ID_FILE_OPEN);

	for (INT_PTR i = 0; i < control_ids.GetSize(); i++)
	{
		TRACE(L"check menu : %d\n", control_ids.GetAt(i));
		//m_pVPViewerCore->DisableToolbar(control_ids.GetAt(i));

		/*if (m_pVPViewerCore->GetMainWnd()->GetDlgItem(control_ids.GetAt(i))->GetSafeHwnd())
		{
			TRACE(L"disable menu : %d\n", control_ids.GetAt(i));
			m_pVPViewerCore->GetMainWnd()->GetDlgItem(control_ids.GetAt(i))->EnableWindow(FALSE);
		}*/
	}
}

void CPointCounterDlg::OnEnChangeEdit()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CPointCountingBaseDlg::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString buf;

	int posx, posy, sizex, sizey;

	m_ctlCellColumnEdit.GetWindowTextW(buf);
	sizex = _ttoi(buf);
	m_ctlCellRowEdit.GetWindowTextW(buf);
	sizey = _ttoi(buf);
	m_ctlLocationXEdit.GetWindowTextW(buf);
	posx = _ttoi(buf);
	m_ctlLocationYEdit.GetWindowTextW(buf);
	posy = _ttoi(buf);

	CString total_value;
	total_value.Format(L"Total :  %d", sizey * sizex);
	m_ctlStartTotalValue.SetWindowTextW(total_value);

	POINT pos = { posx, posy };
	POINT size = { sizex, sizey };
	if (posx != 0 && posy != 0 && sizex != 0 && sizey != 0)
		m_pVPViewerCore->SetGridBox(pos, size);

}

int CPointCounterDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CPointCounterBaseDlg::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	tophwnd();

	TCHAR app_path[2048];
	GetModuleFileName(NULL, app_path, 2048);
	PathRemoveFileSpec(app_path);
	m_strAppPath = app_path;

	m_strProfilePath = m_strAppPath + L"\\default.xml";
	szProfilePath = (TCHAR*)(LPCTSTR)m_strProfilePath;

	InitPage();

	//AfxMessageBox(m_strProfilePath);

	HRESULT hr;
	hr = pXMLDoc.CreateInstance(__uuidof(MSXML2::DOMDocument));
	if (S_OK != hr)
	{
		AfxMessageBox(_T("Can not Create XMLDOMDocument Install MSXML6.0"));
		return 0;
	}

	pXMLDoc->resolveExternals = VARIANT_TRUE;
	pXMLDoc->async = VARIANT_FALSE;
	pXMLDoc->validateOnParse = VARIANT_TRUE;

	_variant_t varXml(szProfilePath);
	_variant_t varOut((bool)TRUE);

	varOut = pXMLDoc->load(szProfilePath);
	if ((bool)varOut == FALSE){
		TCHAR strTemp[100];
		_stprintf_s(strTemp, _countof(strTemp), _T("Can not Read XML File %s\n"), szProfilePath);
		AfxMessageBox(strTemp);
		return 0;
	}

	xml_thread = AfxBeginThread(ThreadFirst, this);

	if (xml_thread == NULL)
	{
		AfxMessageBox(L"Error");
	}

	//CloseHandle(xml_thread);

	return 0;
}

int CPointCounterDlg::GetCountChar(CString Ps_Str, TCHAR Pc_Char)
{
	int Li_Count = 0;
	for (int i = 0; i < Ps_Str.GetLength(); i++)
	{
		if (Ps_Str[i] == Pc_Char) Li_Count++;
	}
	return Li_Count;
}

CString* CPointCounterDlg::Split(CString Ps_Str, TCHAR Pc_Separator, int& Ri_Length)
{
	Ri_Length = GetCountChar(Ps_Str, Pc_Separator) + 1;
	if (Ps_Str[0] == Pc_Separator) Ri_Length--;
	if (Ps_Str[Ps_Str.GetLength() - 1] == Pc_Separator) Ri_Length--;
	CString* Rsa_Str = new CString[Ri_Length];

	int Li_Count = 0;
	CString Ls_Token;

	for (int i = 0; i < Ri_Length; i++)
	{
		AfxExtractSubString(Ls_Token, Ps_Str, Li_Count, Pc_Separator);
		Rsa_Str[Li_Count++] = Ls_Token;
	}

	return Rsa_Str;
}


void CPointCounterDlg::OnBnClickedCrosshairButton()
{
	// TODO: Add your control notification handler code here
	
	CString str = L"";
	m_ctlCrosshairButton.GetWindowText(str);

	CButton *crosshair_on = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_ON_RADIO);
	CButton *crosshair_off = (CButton *)GetDlgItem(IDC_STATUS_CROSSHAIR_OFF_RADIO);

	if (str.Compare(L"Turn OFF") == 0)
	{
		m_pVPViewerCore->SetGridCrossHair(false);
		crosshair_on->EnableWindow(FALSE);
		crosshair_off->EnableWindow(FALSE);

		m_ctlCrosshairButton.SetWindowText(_T("Turn ON"));
	}
	else
	{
		m_pVPViewerCore->SetGridCrossHair(true);

		crosshair_on->EnableWindow(TRUE);
		crosshair_off->EnableWindow(TRUE);
		m_ctlCrosshairButton.SetWindowText(_T("Turn OFF"));
	}

}


// NUMPAD
// ============================================================================
#define IsLeftPressed() ( 0x8000 ==(GetKeyState(VK_LEFT) & 0x8000   ))
#define IsRightPressed()  ( 0x8000 ==(GetKeyState(VK_RIGHT) & 0x8000 ))

BOOL CPointCounterDlg::OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	if (m_ePageStep < PAGE_STEP::MAIN)
		return FALSE;

	int sel_button = -1;

	if (IsLeftPressed() == TRUE)
	{
		if (0x8000 == (GetKeyState(VK_NUMPAD0) & 0x8000))
			sel_button = 19;
		else if (0x8000 == (GetKeyState(VK_NUMPAD1) & 0x8000))
			sel_button = 10;
		else if (0x8000 == (GetKeyState(VK_NUMPAD2) & 0x8000))
			sel_button = 11;
		else if (0x8000 == (GetKeyState(VK_NUMPAD3) & 0x8000))
			sel_button = 12;
		else if (0x8000 == (GetKeyState(VK_NUMPAD4) & 0x8000))
			sel_button = 13;
		else if (0x8000 == (GetKeyState(VK_NUMPAD5) & 0x8000))
			sel_button = 14;
		else if (0x8000 == (GetKeyState(VK_NUMPAD6) & 0x8000))
			sel_button = 15;
		else if (0x8000 == (GetKeyState(VK_NUMPAD7) & 0x8000))
			sel_button = 16;
		else if (0x8000 == (GetKeyState(VK_NUMPAD8) & 0x8000))
			sel_button = 17;
		else if (0x8000 == (GetKeyState(VK_NUMPAD9) & 0x8000))
			sel_button = 18;
	}
	else if (IsRightPressed() == TRUE)
	{
		if (0x8000 == (GetKeyState(VK_NUMPAD0) & 0x8000))
			sel_button = 29;
		else if (0x8000 == (GetKeyState(VK_NUMPAD1) & 0x8000))
			sel_button = 20;
		else if (0x8000 == (GetKeyState(VK_NUMPAD2) & 0x8000))
			sel_button = 21;
		else if (0x8000 == (GetKeyState(VK_NUMPAD3) & 0x8000))
			sel_button = 22;
		else if (0x8000 == (GetKeyState(VK_NUMPAD4) & 0x8000))
			sel_button = 23;
		else if (0x8000 == (GetKeyState(VK_NUMPAD5) & 0x8000))
			sel_button = 24;
		else if (0x8000 == (GetKeyState(VK_NUMPAD6) & 0x8000))
			sel_button = 25;
		else if (0x8000 == (GetKeyState(VK_NUMPAD7) & 0x8000))
			sel_button = 26;
		else if (0x8000 == (GetKeyState(VK_NUMPAD8) & 0x8000))
			sel_button = 27;
		else if (0x8000 == (GetKeyState(VK_NUMPAD9) & 0x8000))
			sel_button = 28;
	}
	else
	{
		if (0x8000 == (GetKeyState(VK_NUMPAD0) & 0x8000))
			sel_button = 9;
		else if (0x8000 == (GetKeyState(VK_NUMPAD1) & 0x8000))
			sel_button = 0;
		else if (0x8000 == (GetKeyState(VK_NUMPAD2) & 0x8000))
			sel_button = 1;
		else if (0x8000 == (GetKeyState(VK_NUMPAD3) & 0x8000))
			sel_button = 2;
		else if (0x8000 == (GetKeyState(VK_NUMPAD4) & 0x8000))
			sel_button = 3;
		else if (0x8000 == (GetKeyState(VK_NUMPAD5) & 0x8000))
			sel_button = 4;
		else if (0x8000 == (GetKeyState(VK_NUMPAD6) & 0x8000))
			sel_button = 5;
		else if (0x8000 == (GetKeyState(VK_NUMPAD7) & 0x8000))
			sel_button = 6;
		else if (0x8000 == (GetKeyState(VK_NUMPAD8) & 0x8000))
			sel_button = 7;
		else if (0x8000 == (GetKeyState(VK_NUMPAD9) & 0x8000))
			sel_button = 8;
	}

	if (sel_button >= 0)

	{
		CPageGroup *page_group;

		if (m_ePageStep == PAGE_STEP::MAIN)
			page_group = (CPageGroup *)GetDlgItem(IDC_MAIN_GROUP);
		else
			page_group = m_pContentPage;

		for (int i = 0; i < page_group->m_arrItemButton.GetSize(); i++)
		{
			POINTITEM point_item = page_group->m_arrItemButton[i]->GetData();

			if (point_item.order == sel_button)
			{
				page_group->m_arrItemButton[i]->OnStnClicked();
				return TRUE;
			}
		}
	}

	return TRUE;
}
// ============================================================================
