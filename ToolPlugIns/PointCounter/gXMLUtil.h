#ifndef __UMF_XML_UTIL_H__
#define __UMF_XML_UTIL_H__

#ifndef __PXML__

typedef void*	PXML;
#define __PXML__

#endif

class CXMLUtil
{
public:
	CXMLUtil(PXML pXML);
	CXMLUtil();

	~CXMLUtil();
	
	BOOL	Load(const TCHAR *sz, TCHAR *szRootTag = _T("APPSetting"));
	BOOL	Save(const TCHAR *sz = NULL);

	BOOL	LoadXML(char *szXML, int size, TCHAR *szRootTag = _T("APPSetting"));

	BOOL	SetXML(PXML pXmlElement);
	PXML	GetXML() {return (PXML)m_pXML;}

	TCHAR*	GetNodeName();
	TCHAR*	GetNodeValue();

	PXML	GetChildXML(TCHAR* szItem);
	PXML	GetFirstChildXML();
	PXML	GetNextChild(PXML pPrevChild);
	
	DWORD	GetAttrValueHex(TCHAR* attrName, DWORD DefulatValue = 0);
	long	GetAttrValueNum(TCHAR* attrName, long DefulatValue = 0);
	double	GetAttrValueFloat(TCHAR* attrName, double DefulatValue = 0.0);
	//count : is not buffer length 
	BOOL	GetAttrValueString(TCHAR* attrName, TCHAR* buffer, DWORD count);
	BOOL	GetAttrValueBinData(TCHAR* attrName, int count, TCHAR* data);
	
	BOOL	SetAttrValueHex(TCHAR* attrName,DWORD v);
	BOOL	SetAttrValueNum(TCHAR* attrName,long v);
	BOOL	SetAttrValueFloat(TCHAR* attrName,double v);
	BOOL	SetAttrValueString(TCHAR* attrName,TCHAR* buffer);
	BOOL	SetAttrValueBinData(TCHAR* attrName,int count, TCHAR* data);

	PXML	CreateElement(TCHAR* szTagName);
	BOOL	AppendChild(PXML pChildXML);
	BOOL	RemoveChild(PXML pChildXML);
	BOOL	ReplaceChild(PXML pNewChildXML, PXML pOldChildXML);

	//Show XML content.
	TCHAR*	GetXMLData();

private:

	PXML	m_pXMLDoc;
	TCHAR	m_szXMLPath[MAX_PATH];
	PXML	m_pXML;

	void	ReleaseXML();
};


#endif