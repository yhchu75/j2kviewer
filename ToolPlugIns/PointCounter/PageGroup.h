#pragma once

#include "_PointItem.h"
#include "ItemButton.h"

// CPageGroup

class CPageGroup : public CStatic
{
	DECLARE_DYNAMIC(CPageGroup)

public:
	CPageGroup();
	virtual ~CPageGroup();

protected:
	DECLARE_MESSAGE_MAP()
protected:
	//CArray<CItemButton*> m_arrItemButton;

	CSize TextSize(CFont*, CString);
public:
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedStartStartButton();

	unsigned int AddControl(unsigned int, CFont*, CRect);
	unsigned int AddControl(CString, CFont*, CRect);
	BOOL AddButton(int, POINTITEM, CRect);

	CArray<CItemButton*> m_arrItemButton;
};
