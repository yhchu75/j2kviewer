// NavigationLabel.cpp : implementation file
//

#include "stdafx.h"
#include "PointCounter.h"
#include "NavigationLabel.h"

#include "PointCounterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CNavigationLabel

IMPLEMENT_DYNAMIC(CNavigationLabel, CStatic)

CNavigationLabel::CNavigationLabel()
{

}

CNavigationLabel::~CNavigationLabel()
{
}


BEGIN_MESSAGE_MAP(CNavigationLabel, CStatic)
	ON_CONTROL_REFLECT(STN_CLICKED, &CNavigationLabel::OnStnClicked)
END_MESSAGE_MAP()



// CNavigationLabel message handlers




void CNavigationLabel::PostNcDestroy()
{
	// TODO: Add your specialized code here and/or call the base class
	delete this;

	CStatic::PostNcDestroy();
}


void CNavigationLabel::OnStnClicked()
{
	// TODO: Add your control notification handler code here
	/*if (wcscmp(m_tPointItem.id, L"") == 0)
		return;*/
	if (m_tPointItem.depth < -1)
		return;

	CPointCounterDlg *main_dialog = (CPointCounterDlg *)GetParent();
	main_dialog->ReverseNavigation(m_tPointItem);
}


void CNavigationLabel::SetData(POINTITEM point_item)
{
	m_tPointItem = point_item;
}

POINTITEM CNavigationLabel::GetData()
{
	return m_tPointItem;
}
