#pragma once

#include "../../VPviewer/IVPViewerCore.h"
#include "afxwin.h"
//#include "afxwin.h"
// PointCountingDrawDlg dialog

class CPointCountingDrawDlg : public CPointCountingBaseDlg
{
	DECLARE_DYNAMIC(CPointCountingDrawDlg)
public:
	CPointCountingDrawDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPointCountingDrawDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_SECTOR_REGION};

	int		OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	int		OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);
	int		OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point);
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnClickedButtonDrawNext();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();

	afx_msg void OnClickedButtonDrawMethod(UINT nID);
	afx_msg void OnClickedButtonRatio(UINT nID);
	afx_msg void OnClickedButtonZoomLevel(UINT nID);
	afx_msg void OnBnClickedButtonRegionAdd();

	CButton m_buttonNext;
	int 	m_SelectAnalysisRegionMode;
};
