// PointCounting.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "PointCountingV2.h"
#include "../../VPviewer/IVPViewerCore.h"

#include "PointCountingStartDlg.h"
#include "PointCountingDrawDlg.h"
#include "PointCountingSelectDlg.h"
#include "PointCountingCountDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CPointCountingApp

BEGIN_MESSAGE_MAP(CPointCountingApp, CWinApp)
END_MESSAGE_MAP()


// CPointCountingApp construction

CPointCountingApp::CPointCountingApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPointCountingApp object

CPointCountingApp theApp;

extern "C" IToolPlugIn * WINAPI  CreateToolPlugIn()
{
	return new CPointCountingToolPlugIn();
}


// CPointCountingApp initialization

BOOL CPointCountingApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}


CPointCountingToolPlugIn::CPointCountingToolPlugIn()
{
	m_bInstance = FALSE;
	m_pCurDlg = NULL;
	for (int i = 0; i < DIALOG_ID_MAX; i++) m_pDlgBase[i] = NULL;
	
	m_pCounter_array = NULL;
	m_pPosition = NULL;
	m_pSize = NULL;
	m_pCurrent = NULL;
}


CPointCountingToolPlugIn::~CPointCountingToolPlugIn()
{
	for (int i = 0; i < DIALOG_ID_MAX; i++) {
		if (m_pDlgBase[i]) delete m_pDlgBase[i];
	}

	if (m_pPosition != NULL)
	{
		delete[] m_pPosition;
		m_pPosition = NULL;
	}
	if (m_pSize != NULL)
	{
		delete[] m_pSize;
		m_pSize = NULL;
	}
	if (m_pCurrent != NULL)
	{
		delete[] m_pCurrent;
		m_pCurrent = NULL;
	}
}


void  CPointCountingToolPlugIn::Setup(IVPViewerCore *pVPViewerCore)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDlgBase[DIALOG_ID_START] = new CPointCountingStartDlg();
	m_pDlgBase[DIALOG_ID_SELECT_REGION] = new CPointCountingDrawDlg();
	m_pDlgBase[DIALOG_ID_COUNTING] = new CPointCountingCountDlg();

	for (int i = 0; i < CPointCountingToolPlugIn::DIALOG_ID_MAX; i++) {
		m_pDlgBase[i]->Setup(this, pVPViewerCore, i);
	}

	m_pPosition = new POINT();
	m_pSize = new POINT();
	m_pCurrent = new POINT();
}


HWND  CPointCountingToolPlugIn::Show(HWND hWnd, BOOL bShow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (m_pCurDlg == NULL){
		if (m_bInstance == FALSE){
			for (int i = 0; i < CPointCountingToolPlugIn::DIALOG_ID_MAX; i++) {
				m_pDlgBase[i]->Create(CPointCountingStartDlg::IDD + i, CWnd::FromHandle(hWnd));
			}
			m_bInstance = TRUE;
		}
		m_pCurDlg = m_pDlgBase[0];
	}
	m_pCurDlg->ShowWindow(bShow);
	if (!bShow)
		m_pCurDlg = m_pDlgBase[0];
	else
		((CPointCountingStartDlg *)m_pDlgBase[0])->ShowSectorSelect();

	return m_pCurDlg->GetSafeHwnd();
}


void	CPointCountingToolPlugIn::NextPage()
{	
	if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}
	int NextID = m_pCurDlg->GetID() + 1;

	if (NextID == DIALOG_ID_MAX) {
		EndupProcess();
		return;
	}

	m_pCurDlg = m_pDlgBase[NextID];
	m_pCurDlg->ShowWindow(TRUE);
}


void	CPointCountingToolPlugIn::EndupProcess(BOOL bCancel)
{
	if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}

	//TODO : Need to Initialize current setting .... 
	m_pCurDlg = NULL;	
}


void	CPointCountingToolPlugIn::JumpPage()
{
	if (m_pCurDlg) {
		m_pCurDlg->ShowWindow(FALSE);
	}
	if (m_pCurDlg->GetID() != DIALOG_ID_START) {
		return;
	}
	m_pCurDlg = m_pDlgBase[DIALOG_ID_COUNTING];
	((CPointCountingCountDlg *)m_pCurDlg)->isLoaded = true;

	m_pCurDlg->ShowWindow(TRUE);
}