// CPointCountingCountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PointCountingV2.h"
#include "PointCountingCountDlg.h"
#include "afxdialogex.h"

// CPointCountingCountDlg dialog

IMPLEMENT_DYNAMIC(CPointCountingCountDlg, CDialogEx)

CPointCountingCountDlg::CPointCountingCountDlg(CWnd* pParent /*=NULL*/)
{
	isLoaded = false;
}

CPointCountingCountDlg::~CPointCountingCountDlg()
{
}

void CPointCountingCountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LABEL_TOTAL_SECTOR, m_totalSectors);
	DDX_Control(pDX, IDC_LABEL_COUNTED_SECTOR, m_countedSector);
	DDX_Control(pDX, IDC_LABEL_TOTAL_SECTOR2, m_totalSectors2);

	DDX_Control(pDX, IDC_LABEL_TOTAL_COUNT, m_totalCells);
	DDX_Control(pDX, IDC_LABEL_COUNTED, m_countedCell);
	
	DDX_Control(pDX, IDC_LIST_FOSSILTYPE, m_FossilListCtrl);
}


BEGIN_MESSAGE_MAP(CPointCountingCountDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CPointCountingCountDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_ENDANALYSIS, &CPointCountingCountDlg::OnBnClickedButtonEndanalysis)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CPointCountingCountDlg::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_BUTTON_BACK, &CPointCountingCountDlg::OnBnClickedButtonBack)
	ON_BN_CLICKED(IDC_BUTTON_SAVETRACE, &CPointCountingCountDlg::OnBnClickedButtonSaveTrace)
	ON_BN_CLICKED(IDC_BUTTON_REMAIN, &CPointCountingCountDlg::OnBnClickedButtonRemain)
END_MESSAGE_MAP()


// CPointCountingCountDlg message handlers
TCHAR* GetThisPath(TCHAR* dest, DWORD destSize)
{
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);
	return dest;
}


void	CPointCountingCountDlg::LoadFossilTypeList()
{
	TCHAR szPath[512];
	GetThisPath(szPath, 512);

	CString FossilTypeFile = szPath;
	FossilTypeFile += _T("\\FossilType.txt");

	char buf[512];
	char name[256], temp[16];
	int color[3], j;
	FILE *fp = _tfopen(FossilTypeFile,_T("r+t"));
	while (fgets(buf, sizeof(buf), fp)) {
		int i = 0;
		//read name 
		for (int j = 0; i < strlen(buf); i++) {
			if (buf[i] == ',') {
				name[j] = NULL;
				i++;
				break;
			}
			name[j++] = buf[i];
		}
		
		//read r,g, b
		for(int k = 0 ; k < 3 ; k++){
			for (int j = 0; i < strlen(buf); i++) {
				if (buf[i] == ',' || (strlen(buf) -1 == i)) {
					temp[j] = NULL;
					color[k] = atoi(temp);
					i++;
					break;
				}
				temp[j++] = buf[i];
			}
		}
		FossilItem item;
		item.color = RGB(color[0], color[1], color[2]);
		strcpy(item.name, name);
		m_FossilItemList.push_back(item);
	}
	fclose(fp);
}



BOOL CPointCountingCountDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//Create Camera List 
	LoadFossilTypeList();

	m_FossilListCtrl.SetColumnHeader(_T("Check, 20; Color, 60; Name, 200"));
	m_FossilListCtrl.SetGridLines(TRUE); // SHow grid lines	
	m_FossilListCtrl.SetCheckboxeStyle(RC_CHKBOX_SINGLE);
	USES_CONVERSION;

	for (int i = 0; i < m_FossilItemList.size(); i++)
	{
		m_FossilListCtrl.InsertItem(i, _T(""));
		m_FossilListCtrl.SetItemBkColor(i, 1, m_FossilItemList[i].color);
		m_FossilListCtrl.SetItemTextColor(i, 2, m_FossilItemList[i].color);
		m_FossilListCtrl.SetItemText(i, 2, A2T(m_FossilItemList[i].name));
	}

	return TRUE;
}


BOOL	CPointCountingCountDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	int nItem = m_FossilListCtrl.GetNextItem(-1, LVNI_SELECTED);
	if (nItem < 0 && nItem >= m_FossilItemList.size()) return FALSE;
	if (m_pVPViewerCore->SetSectorCellInfo(point, nItem, m_FossilItemList[nItem].color) > 0) {

		TCHAR buff[32];
		m_countedCell.SetWindowText(_itot(++m_CountingCell, buff, 10));
	}

	return TRUE;
}


BOOL	CPointCountingCountDlg::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	return TRUE;
}


void CPointCountingCountDlg::OnBnClickedCancel()
{
	m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_NONE);
	m_pPointCountingToolPlugIn->EndupProcess();
	isLoaded = false;	
	m_pVPViewerCore->ResetAnalyze();
}


void CPointCountingCountDlg::OnBnClickedButtonEndanalysis()
{
	m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_NONE);

	CString m_strPath;
	CString m_strFolder;
	CStdioFile file;
	// CFile file;
	CString strNow;

	SYSTEMTIME time;
	::ZeroMemory(reinterpret_cast<void*>(&time), sizeof(time));

	::GetLocalTime(&time);

	strNow.Format(_T("%4d-%02d-%02d %02d%02d%02d"), time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);

	m_strPath.Format(L"PointCounting_%s_%s.xml", m_pVPViewerCore->GetFileName(), strNow);
	
	CFileDialog dlg(FALSE, _T("*.xml"), m_strPath, OFN_OVERWRITEPROMPT, _T("XML files(*.xml)|*.xml|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".xml")
		{
			m_strPath += ".xml";
		}

		SaveTrace(m_strPath);
	}
	m_strFolder = m_strPath.Left(m_strPath.GetLength() - 4);
	CreateDirectory(m_strFolder, NULL);
/*
	for (int y = 0; y < size.y; y++)
	{
		for (int x = 0; x < size.x; x++)
		{
			CString ImagePath;
			ImagePath.Format(_T("%s\\%03d_%03d.bmp"), m_strFolder, y+1, x+1);
			
			POINT pt1;

			pt1.x = position.x + x;
			pt1.y = position.y + y;
			
			m_pVPViewerCore->CropGridImage(pt1, ImagePath);
		}
	}

	CString ImagePath;
	ImagePath.Format(_T("%s\\overview.bmp"), m_strFolder);
	m_pVPViewerCore->CropGridImage(position, ImagePath, size.x, size.y);
	*/
	m_pPointCountingToolPlugIn->EndupProcess(FALSE);	

}


void CPointCountingCountDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	m_pVPViewerCore->SetAnalyzeMode(bShow? IVPViewerCore::AnalyzeMode_SECTOR: IVPViewerCore::AnalyzeMode_NONE);
	CPointCountingBaseDlg::OnShowWindow(bShow, nStatus);
	// TODO: Add your message handler code here
	if (bShow)
	{
		//first 
		if (!isLoaded){
			TCHAR buff[32];

			int totalSector = m_pVPViewerCore->GetTotalSectorCount();
			m_totalSectors.SetWindowText(_itot(totalSector, buff, 10));
			m_totalSectors2.SetWindowText(_itot(totalSector, buff, 10));
			
			m_CurrentSector = 0;
			m_countedSector.SetWindowText(_itot(m_CurrentSector, buff, 10 ));
			m_pVPViewerCore->MoveSector(m_CurrentSector, 1);
			

			m_pVPViewerCore->SetSectorCellInfo(-1, 0, m_FossilItemList[0].color);
			m_pVPViewerCore->SetSectorRegionMode(IVPViewerCore::SectorMode_SectorAnalyze);
			m_pVPViewerCore->ShowGrid(true);

			int totalCell = m_pVPViewerCore->GetTotalCellCount();
			m_CountingCell = 0;
			m_totalCells.SetWindowText(_itot(totalCell, buff, 10));
			m_countedCell.SetWindowText(_T("0"));

			isLoaded = true;	//first load 
		}
		else
		{
			/*
			position = *m_pPointCountingToolPlugIn->GetPosition();
			size = *m_pPointCountingToolPlugIn->GetSize();
			current_position = *m_pPointCountingToolPlugIn->GetCurrentPosition();
						
			counter_array = new int[size.x * size.y];
			counter_array_buffer = new int[size.x * size.y];
			memcpy(counter_array, m_pPointCountingToolPlugIn->GetCounterArray(), sizeof(int)*size.x*size.y);
			memcpy(counter_array_buffer, counter_array, sizeof(int)*size.x*size.y);
			counted = m_pPointCountingToolPlugIn->GetCounting();

			current_val = counter_array[(current_position.y - 1)*size.y + current_position.x - 1];

			m_pVPViewerCore->SetGridBoxArray(counter_array);
			m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_SECTOR);
			m_pVPViewerCore->SetGridBox(position, size);
			*/

		}
		//isLoaded = false;
	}
	else
	{ 

	}
}


void CPointCountingCountDlg::OnBnClickedButtonNext()
{
	if (m_CurrentSector == m_pVPViewerCore->GetTotalSectorCount()) return;

	m_pVPViewerCore->MoveSector(++m_CurrentSector, 0);
	TCHAR buff[32];
	m_countedSector.SetWindowText(_itot(m_CurrentSector, buff, 10));
	m_CountingCell = 0;
	m_countedCell.SetWindowText(_T("0"));

}


void CPointCountingCountDlg::OnBnClickedButtonBack()
{
	if(m_CurrentSector > 0)
	m_pVPViewerCore->MoveSector(--m_CurrentSector, 0);
	TCHAR buff[32];
	m_countedSector.SetWindowText(_itot(m_CurrentSector, buff, 10));
	m_CountingCell = 0;
	m_countedCell.SetWindowText(_T("0"));
}


void CPointCountingCountDlg::OnBnClickedButtonSaveTrace()
{
	CString m_strPath;
	CStdioFile file;
	// CFile file;
	/*
	m_strPath.Format(L"%s_grid_%d_%d_to_%d_%d.xml", m_pVPViewerCore->GetFileName(), position.x, position.y, position.x + size.x , position.y + size.y);
	CFileDialog dlg(FALSE, _T("*.xml"), m_strPath, OFN_OVERWRITEPROMPT, _T("XML files(*.xml)|*.xml|"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		m_strPath = dlg.GetPathName();
		if (m_strPath.Right(4) != ".xml")
		{
			m_strPath += ".xml";
		}

		SaveTrace(m_strPath);
	}
	*/
}


bool CPointCountingCountDlg::SaveTrace(const TCHAR *szFile)
{
	FILE *fp = _tfopen(szFile, _T("w+t"));
	if (NULL == fp)
		return false;

	InsertHeader(fp);
	InsertGRID(fp);
	InsertTale(fp);
	
	fclose(fp);

	return true;
}

void CPointCountingCountDlg::InsertHeader(FILE* fp)
{

}

void CPointCountingCountDlg::InsertTale(FILE* fp)
{

}

void CPointCountingCountDlg::InsertGRID(FILE* fp)
{

}

void CPointCountingCountDlg::OnBnClickedButtonRemain()
{
	int nItem = m_FossilListCtrl.GetNextItem(-1, LVNI_SELECTED);
	if (nItem < 0 && nItem >= m_FossilItemList.size()) return ;
	m_pVPViewerCore->SetSectorCellInfo(-1, nItem, m_FossilItemList[nItem].color);

	TCHAR buff[32];
	int totalCell = m_pVPViewerCore->GetTotalCellCount();
	m_countedCell.SetWindowText(_itot(totalCell, buff, 10));
	
}
