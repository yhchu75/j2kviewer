//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// PointCounting.rc에서 사용되고 있습니다.
//
#define IDD_DIALOG_DUMMY                1000
#define IDC_BUTTON_ZOOM                 1000
#define IDD_DIALOG_START                1000
#define IDC_BUTTON1                     1001
#define IDD_DIALOG_SECTOR_REGION        1001
#define IDC_BUTTON_SAVETRACE            1001
#define IDC_EDIT1                       1002
#define IDC_BUTTON_BACK                 1002
#define IDD_DIALOG_COUNT                1002
#define IDC_BUTTON_REMAIN               1003
#define IDC_BUTTON3                     1003
#define IDC_EDIT2                       1003
#define IDC_RADIO1                      1003
#define IDC_BUTTON_LOAD                 1003
#define IDC_RADIO_RATIO_16X9            1003
#define IDC_RADIO_RATIO_4X3             1004
#define IDC_EDIT3                       1004
#define IDC_EDIT4                       1005
#define IDC_BUTTON7                     1005
#define IDC_BUTTON_ENDANALYSIS          1005
#define IDC_RADIO_ZOOM_25               1005
#define IDC_RADIO_SEL_REGION_RECT       1006
#define IDC_RADIO2                      1006
#define IDC_RADIO_ZOOM_5                1006
#define IDC_RADIO_ZOOM_1                1007
#define IDC_BUTTON4                     1007
#define IDC_BUTTON_NEXT                 1007
#define IDC_RADIO_SEL_REGION_CIRCLE     1007
#define IDC_BUTTON5                     1008
#define IDC_STATIC_GRID                 1008
#define IDC_BUTTON_SKIP                 1008
#define IDC_RADIO_SEL_REGION_FREE       1008
#define IDC_BUTTON6                     1009
#define IDC_STATIC_SCALE                1009
#define IDC_BUTTON_MOVE                 1009
#define IDC_EDIT5                       1010
#define IDC_EDIT_COL                    1010
#define IDC_LABEL_CURRENT               1011
#define IDC_LABEL_TOTAL                 1012
#define IDC_LABEL_COUNTED               1013
#define IDC_LABEL_TOTAL_COUNT           1014
#define IDC_EDIT_ROW                    1015
#define IDC_LABEL_CURRENT2              1016
#define IDC_RADIO_SEL_ALL               1017
#define IDC_LABEL_TOTAL2                1017
#define IDC_LABEL_TOTAL_SECTOR          1017
#define IDC_RADIO_SEL_REGION            1018
#define IDC_BUTTON_SKIP2                1019
#define IDC_COMBO_DRAW_TYPE             1019
#define IDC_RADIO_SELECT_DEL            1019
#define IDC_BUTTON_MOVE2                1020
#define IDC_BUTTON_REGION_ADD           1020
#define IDC_EDIT_ROW2                   1021
#define IDC_RADIO3                      1021
#define IDC_EDIT_COL2                   1022
#define IDC_LABEL_COUNTED2              1023
#define IDC_BUTTON_DRAW_NEXT            1023
#define IDC_LABEL_COUNTED_SECTOR        1023
#define IDC_LABEL_TOTAL_COUNT2          1024
#define IDC_BUTTON_START_NEXT           1024
#define IDC_LABEL_TOTAL_SECTOR2         1024
#define IDC_LIST_FOSSILTYPE             1028
#define IDC_LIST_FOSSILTYPE2            1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1006
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
