#pragma once

#include "../../VPviewer/IVPViewerCore.h"
#include "afxwin.h"
#include "ReportCtrl.h"
#include <vector>

// CPointCountingCountDlg dialog
typedef struct {
	DWORD color;
	char name[256];
}FossilItem;

class CPointCountingCountDlg : public CPointCountingBaseDlg
{
	DECLARE_DYNAMIC(CPointCountingCountDlg)

public:
	CPointCountingCountDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPointCountingCountDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_COUNT };

	int		OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point);
	int		OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point);

	
	bool	SaveTrace(const TCHAR *szFile);
	void	InsertHeader(FILE *fp);
	void	InsertTale(FILE *fp);
	void	InsertGRID(FILE *fp);

	bool	isLoaded;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonEndanalysis();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();

private:
	
	std::vector<FossilItem> m_FossilItemList;
	CReportCtrl	m_FossilListCtrl;

	void	LoadFossilTypeList();
public:
	
	CStatic m_totalSectors;
	CStatic m_totalSectors2;
	CStatic m_countedSector;
	CStatic m_totalCells;
	CStatic m_countedCell;
	int		m_CurrentSector;
	int		m_CountingCell;

	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnBnClickedButtonBack();
	afx_msg void OnBnClickedButtonSaveTrace();
	afx_msg void OnBnClickedButtonRemain();
};
