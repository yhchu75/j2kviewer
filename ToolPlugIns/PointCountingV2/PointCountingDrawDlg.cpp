// CPointCountingDrawDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PointCountingV2.h"
#include "PointCountingDrawDlg.h"
#include "afxdialogex.h"


// CPointCountingDrawDlg dialog

IMPLEMENT_DYNAMIC(CPointCountingDrawDlg, CDialogEx)

CPointCountingDrawDlg::CPointCountingDrawDlg(CWnd* pParent /*=NULL*/)
{
}

CPointCountingDrawDlg::~CPointCountingDrawDlg()
{
}

void CPointCountingDrawDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_DRAW_NEXT, m_buttonNext);
}


BEGIN_MESSAGE_MAP(CPointCountingDrawDlg, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_DRAW_NEXT, &CPointCountingDrawDlg::OnClickedButtonDrawNext)
	ON_BN_CLICKED(IDCANCEL, &CPointCountingDrawDlg::OnBnClickedCancel)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_SEL_ALL, IDC_RADIO_SEL_REGION, &CPointCountingDrawDlg::OnClickedButtonDrawMethod)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_RATIO_16X9, IDC_RADIO_RATIO_4X3, &CPointCountingDrawDlg::OnClickedButtonRatio)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_ZOOM_25, IDC_RADIO_ZOOM_1, &CPointCountingDrawDlg::OnClickedButtonZoomLevel)

	ON_BN_CLICKED(IDC_BUTTON_REGION_ADD, &CPointCountingDrawDlg::OnBnClickedButtonRegionAdd)
END_MESSAGE_MAP()


// CPointCountingDrawDlg message handlers

BOOL	CPointCountingDrawDlg::OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point)
{
	//AfxMessageBox(_T("Click LButton "));
	//m_buttonNext.EnableWindow(true);
	m_pVPViewerCore->GridOnButtonDown(nFlags, point);


	return TRUE;
}


BOOL	CPointCountingDrawDlg::OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point)
{
	m_pVPViewerCore->GridOnButtonMove(nFlags, point);
	return TRUE;
}


int		CPointCountingDrawDlg::OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point)
{
	m_pVPViewerCore->GridOnButtonUp(nFlags, point);
	if (((CButton*)GetDlgItem(IDC_RADIO_SEL_ALL))->GetCheck() != TRUE) {
		if (m_pVPViewerCore->GetRegionCount() > 1) {
			m_buttonNext.EnableWindow(true);
		}
	}

	return TRUE;
}


BOOL CPointCountingDrawDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CComboBox *pCComboBox = (CComboBox *)GetDlgItem(IDC_COMBO_DRAW_TYPE);
	pCComboBox->AddString(_T("0.Rectangle"));
	pCComboBox->AddString(_T("1.Ellipse"));
	pCComboBox->AddString(_T("2.Free Draw"));
	pCComboBox->SetCurSel(0);
	
	((CButton*)GetDlgItem(IDC_RADIO_SEL_ALL))->SetCheck(true);
	((CButton*)GetDlgItem(IDC_RADIO_RATIO_16X9))->SetCheck(true);
	((CButton*)GetDlgItem(IDC_RADIO_ZOOM_1))->SetCheck(true);

	GetDlgItem(IDC_BUTTON_REGION_ADD)->EnableWindow(false);
	
	return TRUE;  
}


void CPointCountingDrawDlg::OnClickedButtonDrawNext()
{
	m_pPointCountingToolPlugIn->NextPage();
}


void CPointCountingDrawDlg::OnBnClickedCancel()
{
	m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_NONE);
	m_pPointCountingToolPlugIn->EndupProcess();
	m_pVPViewerCore->ResetAnalyze();
}


void CPointCountingDrawDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	m_pVPViewerCore->SetAnalyzeMode(bShow ? IVPViewerCore::AnalyzeMode_SECTOR : IVPViewerCore::AnalyzeMode_NONE);
	CPointCountingBaseDlg::OnShowWindow(bShow, nStatus);
	if (!bShow){
		m_buttonNext.EnableWindow(false);
		m_pVPViewerCore->ShowSector(false);
	}
	else {
		m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_SECTOR);
		m_pVPViewerCore->SetSectorRegionMode(IVPViewerCore::SectorMode_RegionDraw);
		m_pVPViewerCore->ShowSector(true);
	}
}


void CPointCountingDrawDlg::OnClickedButtonDrawMethod(UINT nID)
{
	if (IDC_RADIO_SEL_ALL == nID) {
		m_pVPViewerCore->SetSectorRegionDrawMode(IVPViewerCore::SectorMode_RegionDrawModeAll);
		GetDlgItem(IDC_BUTTON_REGION_ADD)->EnableWindow(false);
	}
	else {	//IDC_RADIO_SEL_REGION
		m_pVPViewerCore->SetSectorRegionDrawMode(IVPViewerCore::SectorMode_RegionDrawModeSelect);
		GetDlgItem(IDC_BUTTON_REGION_ADD)->EnableWindow(true);
	}
}


void CPointCountingDrawDlg::OnClickedButtonRatio(UINT nID)
{
	if (IDC_RADIO_RATIO_16X9 == nID) {
		m_pVPViewerCore->SetGridRatio(1);
	}else {	//IDC_RADIO_RATIO_4X3
		m_pVPViewerCore->SetGridRatio(2);
	}
}


void CPointCountingDrawDlg::OnClickedButtonZoomLevel(UINT nID)
{
	m_pVPViewerCore->SetSectorZoomLevel(nID - IDC_RADIO_ZOOM_25);
}

void CPointCountingDrawDlg::OnBnClickedButtonRegionAdd()
{
	int AddMethod = ((CComboBox *)GetDlgItem(IDC_COMBO_DRAW_TYPE))->GetCurSel();
	m_pVPViewerCore->AddSectorRegion((IVPViewerCore::eSectorRegionType)(AddMethod + 1));
}

