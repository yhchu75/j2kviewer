// PointCounting.h : main header file for the PointCounting DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "afxwin.h"
#include "resource.h"		// main symbols
#include "../ToolPlusgIn.h"

// CPointCountingApp
// See PointCounting.cpp for the implementation of this class
//

class CPointCountingApp : public CWinApp
{
public:
	CPointCountingApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};


class CPointCountingToolPlugIn;
class CPointCountingBaseDlg : public CDialog
{
public :
	CPointCountingBaseDlg() :m_bActivaed(FALSE){}

	virtual void	Setup(CPointCountingToolPlugIn *pPointCountingToolPlugIn, IVPViewerCore *pVPViewerCore, int Id){
		m_pPointCountingToolPlugIn = pPointCountingToolPlugIn;
		m_pVPViewerCore	= pVPViewerCore;
		m_DialogID = Id;
	}

	void CPointCountingBaseDlg::OnShowWindow(BOOL bShow, UINT nStatus){
		CDialog::OnShowWindow(bShow, nStatus);
		m_bActivaed = bShow;
	}

	virtual BOOL	IsActive(){ return m_bActivaed; }
	virtual BOOL	OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point){ return FALSE; }
	virtual BOOL	OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point){ return FALSE; }
	virtual BOOL	OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point) { return FALSE; }
	virtual BOOL	OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags) { return FALSE; }
	int				GetID() { return m_DialogID; }

protected:

	CPointCountingToolPlugIn *m_pPointCountingToolPlugIn;
	IVPViewerCore *	m_pVPViewerCore;
	BOOL			m_bActivaed;
	int				m_DialogID;
};

/*
typedef enum { 
	ePointDrawType_NONE,
	ePointDrawType_RECT,
	ePointDrawType_CIRCLE,
	ePointDrawType_FREE
}ePointDrawType;


typedef struct  {
	ePointDrawType	type; // point count select type
	RECT rect; //used rectangel and circle



}PointCountingInfo;
*/

class CPointCountingToolPlugIn : public IToolPlugIn
{
public:
	enum{DIALOG_ID_START, DIALOG_ID_SELECT_REGION, DIALOG_ID_COUNTING, DIALOG_ID_MAX};
	const char* GetPlugInsToolName(){ return "Point Counting V2"; }

	CPointCountingToolPlugIn();
	~CPointCountingToolPlugIn();

	void	Setup(IVPViewerCore *pVPViewerCore);
	BOOL	IsActive(){
		if (m_pCurDlg) return m_pCurDlg->IsActive();
		else return FALSE;
	}

	BOOL	OnLButtonDown(HWND hWnd, UINT nFlags, CPoint point){ 
		if (m_pCurDlg) return m_pCurDlg->OnLButtonDown(hWnd, nFlags, point); 
		else return FALSE; 
	}
	BOOL	OnLButtonMove(HWND hWnd, UINT nFlags, CPoint point){
		if (m_pCurDlg) return m_pCurDlg->OnLButtonMove(hWnd, nFlags, point);
		else return FALSE;
	}
	BOOL	OnLButtonUp(HWND hWnd, UINT nFlags, CPoint point) {
		if (m_pCurDlg) return m_pCurDlg->OnLButtonUp(hWnd, nFlags, point);
		else return FALSE;
	}


	BOOL	OnKeyUp(HWND hWnd, UINT nChar, UINT nRepCnt, UINT nFlags) {
		if (m_pCurDlg) return m_pCurDlg->OnKeyUp(hWnd, nChar, nRepCnt, nFlags);
		else return FALSE;
	}


	HWND	Show(HWND hWnd, BOOL bShow);
	void	DestorySelf(){ delete this; }
	
	void	NextPage();
	void	EndupProcess(BOOL bCancel = FALSE);

	void	JumpPage();

	int*	GetCounterArray(){ return m_pCounter_array; }
	void	SetCounterArray(int *ca){ m_pCounter_array = ca; }
	POINT*	GetSize(){ return m_pSize; }
	POINT*	GetPosition(){ return m_pPosition; }
	POINT*	GetCurrentPosition(){ return m_pCurrent; }
	int		GetCounting(){ return m_Counting; }
	void	SetCounting(int input){ m_Counting = input; }

private:
	BOOL	m_bInstance;
	CPointCountingBaseDlg* m_pCurDlg;
	CPointCountingBaseDlg* m_pDlgBase[DIALOG_ID_MAX];
	
	int*	m_pCounter_array;
	POINT*	m_pSize;
	POINT*	m_pPosition;
	POINT*	m_pCurrent;
	int		m_Counting;
};

