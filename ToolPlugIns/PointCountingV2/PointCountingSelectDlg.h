#pragma once

#include "../../VPviewer/IVPViewerCore.h"
#include "afxwin.h"
// CPointCountingSelectDlg 대화 상자입니다.

class CPointCountingSelectDlg : public CPointCountingBaseDlg
{
	DECLARE_DYNAMIC(CPointCountingSelectDlg)

public:
	CPointCountingSelectDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPointCountingSelectDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_SECTOR_SELECT };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	afx_msg void OnClickedButtonDrawNext();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();

	afx_msg void OnClickedButtonChangeSelect(UINT nID);

	CButton m_buttonNext;
};
