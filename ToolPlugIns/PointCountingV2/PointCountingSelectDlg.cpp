// PointCountingSelectDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "PointCountingV2.h"
#include "PointCountingSelectDlg.h"
#include "afxdialogex.h"


// CPointCountingSelectDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPointCountingSelectDlg, CDialogEx)

CPointCountingSelectDlg::CPointCountingSelectDlg(CWnd* pParent /*=NULL*/)
{

}

CPointCountingSelectDlg::~CPointCountingSelectDlg()
{
}

void CPointCountingSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_DRAW_NEXT, m_buttonNext);
}


BEGIN_MESSAGE_MAP(CPointCountingSelectDlg, CDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_DRAW_NEXT, &CPointCountingSelectDlg::OnClickedButtonDrawNext)
	ON_BN_CLICKED(IDCANCEL, &CPointCountingSelectDlg::OnBnClickedCancel)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_RADIO_SELECT_ALL_SECTOR, IDC_RADIO_CANCEL_SECTOR, &CPointCountingSelectDlg::OnClickedButtonChangeSelect)
END_MESSAGE_MAP()


BOOL CPointCountingSelectDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	((CButton*)GetDlgItem(IDC_RADIO_SELECT_ALL_SECTOR))->SetCheck(true);
	return TRUE;
}


void CPointCountingSelectDlg::OnClickedButtonDrawNext()
{
	m_pPointCountingToolPlugIn->NextPage();
}


void CPointCountingSelectDlg::OnBnClickedCancel()
{
//	m_pVPViewerCore->SetGridAnalyzeMode(false);
	m_pPointCountingToolPlugIn->EndupProcess();
}


void CPointCountingSelectDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	m_pVPViewerCore->SetAnalyzeMode(bShow? IVPViewerCore::AnalyzeMode_GRID : IVPViewerCore::AnalyzeMode_NONE);
	CPointCountingBaseDlg::OnShowWindow(bShow, nStatus);
	if (!bShow) {
		m_buttonNext.EnableWindow(false);
	}
	else {
		m_pVPViewerCore->SetAnalyzeMode(IVPViewerCore::AnalyzeMode_SECTOR);
		m_pVPViewerCore->SetSectorRegionMode(IVPViewerCore::SectorMode_SectorSelect);
		m_pVPViewerCore->ShowSector(true);
	}
}


void CPointCountingSelectDlg::OnClickedButtonChangeSelect(UINT nID)
{
	m_pVPViewerCore->SetSectorSelectMode(nID - IDC_RADIO_SELECT_ALL_SECTOR);
}