/*****************************************************************************/
// File: kdu_utils.h [scope = CORESYS/COMMON]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Provides some handy in-line functions.
******************************************************************************/

#ifndef KDU_UTILS_H
#define KDU_UTILS_H

#include <assert.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include "kdu_elementary.h"

#if (defined _MSC_VER)
#  pragma intrinsic (_BitScanReverse)
#  pragma intrinsic (_BitScanForward)
#  ifdef KDU_POINTERS64
#    pragma intrinsic (_BitScanReverse64)
#    pragma intrinsic (_BitScanForward64)
#  endif
#endif

namespace kdu_core { 

/* ========================================================================= */
/*                            Convenient Inlines                             */
/* ========================================================================= */

/*****************************************************************************/
/* INLINE                           kdu_read                                 */
/*****************************************************************************/

static inline int
  kdu_read(kdu_byte * &bp, kdu_byte *end, int nbytes)
{ /* [SYNOPSIS]
       Reads an integer quantity having an `nbytes' bigendian
       representation from the array identified by `bp'.  During the process,
       `bp' is advanced `nbytes' positions.  If this pushes it up to or past
       the `end' pointer, the function throws an exception of type
       `kdu_byte *'.
       [//]
       The byte order is assumed to be big-endian.  If the local machine
       architecture is little-endian, the input bytes are reversed.
     [RETURNS]
       The value of the integer recovered from the first `nbytes' bytes of
       the buffer.
     [ARG: bp]
       Pointer to the first byte in the buffer from which the integer is
       to be recovered.
     [ARG: end]
       Points immediately beyond the last valid entry in the buffer.
     [ARG: nbytes]
       Number of bytes from the buffer which are to be converted into a
       big-endian integer.  Must be one of 1, 2, 3 or 4.
  */
  int val;

  assert(nbytes <= 4);
  if ((end-bp) < nbytes)
    throw bp;
  val = *(bp++);
  if (nbytes > 1)
    val = (val<<8) + *(bp++);
  if (nbytes > 2)
    val = (val<<8) + *(bp++);
  if (nbytes > 3)
    val = (val<<8) + *(bp++);
  return val;
}

/*****************************************************************************/
/* INLINE                       kdu_read_float                               */
/*****************************************************************************/

static inline float
  kdu_read_float(kdu_byte * &bp, kdu_byte *end)
{ /* [SYNOPSIS]
       Reads a 4-byte single-precision floating point quantity from the
       array identified by `bp'.  During the process, `bp' is advanced
       4 bytes.  If this pushes it up to or past the `end' pointer, the
       function throws an exception of type `kdu_byte *'.
       [//]
       The byte order is assumed to be big-endian.  If the local machine
       architecture is little-endian, the input bytes are reversed.
     [RETURNS]
       The value of the floating point quantity recovered from the first
       4 bytes of the buffer.
     [ARG: bp]
       Pointer to the first byte in the buffer from which the integer is
       to be recovered.
     [ARG: end]
       Points immediately beyond the last valid entry in the buffer.
  */
  if ((end-bp) < 4)
    throw bp;
  float val;
  kdu_byte *val_p = (kdu_byte *) &val;
  int n, machine_uses_big_endian = 1;
  ((kdu_byte *) &machine_uses_big_endian)[0] = 0;
  if (machine_uses_big_endian)
    for (n=0; n < 4; n++)
      val_p[n] = *(bp++);
  else
    for (n=3; n >= 0; n--)
      val_p[n] = *(bp++);
  return val;
}

/*****************************************************************************/
/* INLINE                       kdu_read_double                              */
/*****************************************************************************/

static inline double
  kdu_read_double(kdu_byte * &bp, kdu_byte *end)
{ /* [SYNOPSIS]
       Same as `kdu_read_float', but reads an 8-byte double-precision
       quantity from the `bp' array.
  */
  if ((end-bp) < 8)
    throw bp;
  double val;
  kdu_byte *val_p = (kdu_byte *) &val;
  int n, machine_uses_big_endian = 1;
  ((kdu_byte *) &machine_uses_big_endian)[0] = 0;
  if (machine_uses_big_endian)
    for (n=0; n < 8; n++)
      val_p[n] = *(bp++);
  else
    for (n=7; n >= 0; n--)
      val_p[n] = *(bp++);
  return val;
}

/*****************************************************************************/
/* INLINE                          kdu_round                                 */
/*****************************************************************************/

static inline double kdu_round(double x)
{ /* [SYNOPSIS] Returns `x' rounded to the nearest whole integer. */
  return floor(0.5+x);
}

/*****************************************************************************/
/* INLINE                          ceil_ratio                                */
/*****************************************************************************/

static inline int
  ceil_ratio(int num, int den)
{ /* [SYNOPSIS]
       Returns the ceiling function of the ratio `num' / `den', where
       the denominator is required to be strictly positive.
     [RETURNS] Non-negative ratio.
     [ARG: num] Non-negative numerator.
     [ARG: den] Non-negative denomenator.
  */
  assert(den > 0);
  if (num <= 0)
    return -((-num)/den);
  else
    return 1+((num-1)/den);
}

/*****************************************************************************/
/* INLINE                          floor_ratio                               */
/*****************************************************************************/

static inline int
  floor_ratio(int num, int den)
{ /* [SYNOPSIS]
       Returns the floor function of the ratio `num' / `den', where
       the denominator is required to be strictly positive.
     [RETURNS] Non-negative ratio.
     [ARG: num] Non-negative numerator.
     [ARG: den] Non-negative denomenator.
  */
  assert(den > 0);
  if (num < 0)
    return -(1+((-num-1)/den));
  else
    return num/den;
}

/*****************************************************************************/
/* INLINE                       long_ceil_ratio                              */
/*****************************************************************************/

static inline int
  long_ceil_ratio(kdu_long num, kdu_long den)
{ /* [SYNOPSIS]
       Returns the ceiling function of the ratio `num' / `den', where
       the denominator is required to be strictly positive.  The result
       must fit within a signed 32-bit integer, even if the numerator or
       denominator do not.
     [RETURNS] Non-negative ratio.
     [ARG: num] Non-negative numerator.
     [ARG: den] Non-negative denomenator.
  */
  assert(den > 0);
  if (num <= 0)
    { 
      num = -((-num)/den);
      assert((num >= (kdu_long) KDU_INT32_MIN));
    }
  else
    { 
      num = 1+((num-1)/den);
      assert((num <= (kdu_long) KDU_INT32_MAX));
    }
  return (int) num;
}

/*****************************************************************************/
/* INLINE                       long_floor_ratio                             */
/*****************************************************************************/

static inline int
  long_floor_ratio(kdu_long num, kdu_long den)
{ /* [SYNOPSIS]
       Returns the floor function of the ratio `num' / `den', where
       the denominator is required to be strictly positive.  The result
       must fit within a signed 32-bit integer, even if the numerator or
       denominator do not.
     [RETURNS] Non-negative ratio.
     [ARG: num] Non-negative numerator.
     [ARG: den] Non-negative denomenator.
  */
  assert(den > 0);
  if (num < 0)
    { 
      num = -(1+((-num-1)/den));
      assert((num >= (kdu_long) KDU_INT32_MIN));
    }
  else
    { 
      num = num/den;
      assert((num <= (kdu_long) KDU_INT32_MAX));
    }
  return (int) num;
}

/*****************************************************************************/
/* INLINE                     kdu_hex_hex_decode                             */
/*****************************************************************************/

static inline const char *
  kdu_hex_hex_decode(char src[], const char *src_lim=NULL)
{ /* [SYNOPSIS]
       Performs in-place hex-hex decoding of URI's, overwriting the supplied
       `src' string with its hex-hex decoded equivalent.  This function is
       used by `jp2_data_references' as well as various client-server
       components.  The decoding proceeds until a null-terminator is
       encountered, or `src_lim' is reached, at which point the decoded
       string is null-terminated and the function returns a pointer to `src'.
       If `src_lim' does indeed point to a location within the `src' buffer,
       it is possible that the null-terminator will be inserted at that
       location so that *`src_lim'='\0' upon return -- this happens if no
       hex-hex coded characters are encountered.
     [RETURNS]
       A pointer to the original `src' buffer.
  */
  char *dp, *result=src;
  for (dp=src; (*src != '\0') && (src != src_lim); dp++, src++)
    { 
      int hex1, hex2;
      if ((*src == '%') &&
          (isdigit(hex1=toupper(src[1])) ||
           ((hex1 >= (int) 'A') && (hex1 <= (int) 'F'))) &&
          (isdigit(hex2=toupper(src[2])) ||
           ((hex2 >= (int) 'A') && (hex2 <= (int) 'F'))))
        { 
          int decoded = 0;
          if ((hex1 >= (int) 'A') && (hex1 <= (int) 'F'))
            decoded += (hex1 - (int) 'A') + 10;
          else
            decoded += (hex1 - (int) '0');
          decoded <<= 4;
          if ((hex2 >= (int) 'A') && (hex2 <= (int) 'F'))
            decoded += (hex2 - (int) 'A') + 10;
          else
            decoded += (hex2 - (int) '0');
          *dp = (char) decoded;
          src += 2;
        }
      else
        *dp = *src;
    }
  *dp = '\0';
  return result;
}

/*****************************************************************************/
/* INLINE                     kdu_hex_hex_encode                             */
/*****************************************************************************/

static inline int
  kdu_hex_hex_encode(const char *src, char dst[],
                     const char *src_lim=NULL, const char *special_chars=NULL)
{ /* [SYNOPSIS]
       This function is, in some sense, the dual of `kdu_hex_hex_decode'.
       It is used by `jp2_data_references' as well as various client-server
       components.  Since hex-hex encoding generally increases the length of
       a string, it cannot be performed in place.  The function can be invoked
       with a NULL `dst' argument to determine the number of characters in the
       hex-hex encoded result, allowing you to allocate a buffer large enough
       to supply in a second call to the function.
     [RETURNS]
       Number of characters which are (or would be) written to the
       `dst' buffer, not including the terminating null character.
     [ARG: dst]
       If NULL, the function returns only the number of bytes that
       it would write to a non-NULL `dst' buffer, not counting the null
       terminator.
     [ARG: src_lim]
       The `src' string will be read up to but not including any address
       passed by this argument.  This allows you to hex-hex encode an
       initial prefix of the string, if desired.
     [ARG: special_chars]
       By default, the function hex-hex encodes all characters outside the
       range 0x21 to 0x7E (note that 0x20 is the ASCII space character),
       in addition to the non-URI-legal characters defined in RFC2396.
       In some cases, ambiguity may remain unless additional reserved
       characters are hex-hex encoded.  In particular, for JPIP
       communications, the "?" and "&" characters have special meaning
       in URL's and within POST'ed queries, so ambiguity could occur if
       these characters are found within a target filename or a JP2 box
       type code.  Special characters like this may be supplied by a
       non-NULL `special_chars' argument.
  */
  const char *excluded_chars = "<>\"#%{}|\\^[]`";
  int num_octets = 0;
  for (; (src != src_lim) && (*src != '\0'); src++, num_octets++)
    { 
      char ch = *src;
      if ((((kdu_uint32) ch) < 0x21) || (((kdu_uint32) ch) > 0x7E) ||
          (strchr(excluded_chars,ch) != NULL) ||
          ((special_chars != NULL) && (strchr(special_chars,ch) != NULL)))
        { // Need to hex-hex encode
          if (dst != NULL)
            { 
              *(dst++) = '%';
              for (int d=0; d < 2; d++, ch <<= 4)
                { 
                  int digit = (((int) ch) >> 4) & 0x0F;
                  if (digit < 10)
                    *(dst++) = (char)(digit + '0');
                  else
                    *(dst++) = (char)(digit-10 + 'A');
                }
            }
          num_octets += 2; // 2 characters extra
        }
      else if (dst != NULL)
        *(dst++) = ch;
    }
  if (dst != NULL)
    *dst = '\0';
  return num_octets;
}

/*****************************************************************************/
/* INLINE                   kdu_count_leading_zeros                          */
/*****************************************************************************/

static inline kdu_uint32 kdu_count_leading_zeros(kdu_uint32 val)
{ /* [BIND: no-bind]
     [SYNOPSIS]
     Counts the number of most significant bits in `val' that are zero,
     but note that the return value is not defined if `val' is 0.
  */
# if (defined _MSC_VER)
    unsigned long result=0;
    _BitScanReverse(&result,val);
    return (kdu_uint32)(31-result);
# elif (defined __GNUC__)
  return __builtin_clz(val);
# else
  // Set all bits below the leading 1.
  val |= (val>>1);  val |= (val>>2);  val |= (val>>4);
  val |= (val>>8);  val |= (val>>16);

  // Count the number of 1s we now have by repeated partioning and addition
  val = (val & 0x55555555) + ((val >> 1) & 0x55555555);
  val = (val & 0x33333333) | ((val >> 2) & 0x33333333);
  val = (val & 0x0f0f0f0f) | ((val >> 4) & 0x0f0f0f0f);
  val += (val >> 8);
  val += (val >> 16);
  return 32 - (val & 0x1f);
# endif
}

/*****************************************************************************/
/* INLINE                  kdu_count_leading_zeros64                         */
/*****************************************************************************/

static inline kdu_uint32 kdu_count_leading_zeros64(kdu_uint64 val)
  { /* [BIND: no-bind]
     [SYNOPSIS]
     Same as `kdu_count_leading_zeros', but a 64-bit value; this will be
     most efficient on 64-bit platforms.
     */
#ifdef KDU_POINTERS64 // 64-bit OS is the one we focus on
# if (defined _MSC_VER)
    unsigned long result=0;
    _BitScanReverse64(&result,val);
    return (kdu_uint32)(63-result);
# elif (defined __GNUC__)
    return (kdu_uint32)__builtin_clzll(val);
# else
    // Set all bits below the leading 1.
    val |= (val>>1);  val |= (val>>2);  val |= (val>>4);
    val |= (val>>8);  val |= (val>>16);

    // Count the number of 1s we now have by repeated partioning and addition
    val = (val & 0x55555555) + ((val >> 1) & 0x55555555);
    val = (val & 0x33333333) | ((val >> 2) & 0x33333333);
    val = (val & 0x0f0f0f0f) | ((val >> 4) & 0x0f0f0f0f);
    val += (val >> 8);
    val += (val >> 16);
    return 32 - (val & 0x1f);
# endif
#else // only 32-bit platform; cannot evaluate both halves and then combine
      // since behaviour would be undefined if either half were 0.
    kdu_uint32 word = (kdu_uint32)(val>>32);
    if (word)
      return kdu_count_leading_zeros(word);
    else
      { 
        word = (kdu_uint32)val;
        return 32 + kdu_count_leading_zeros(word);
      }
#endif
  }

/*****************************************************************************/
/* INLINE                   kdu_count_trailing_zeros                         */
/*****************************************************************************/

static inline kdu_uint32 kdu_count_trailing_zeros(kdu_uint32 val)
{ /* [BIND: no-bind]
     [SYNOPSIS]
     Counts the number of least significant bits in `val' that are zero,
     but note that the return value is not defined if `val' is 0.
  */
# if (defined _MSC_VER)
  unsigned long result = 0;
  _BitScanForward(&result,val);
  return (kdu_uint32)result;
# elif (defined __GNUC__)
  return __builtin_ctz(val);
#else
  val &= -val; // Resets all bits except the least significant 1
  val--; // So the result we are looking for is now the number of 1s in `val'

  // Count the number of 1s by repeated partitioning and addition
  val = (val & 0x55555555) + ((val >> 1) & 0x55555555);
  val = (val & 0x33333333) | ((val >> 2) & 0x33333333);
  val = (val & 0x0f0f0f0f) | ((val >> 4) & 0x0f0f0f0f);
  val += (val >> 8);
  val += (val >> 16);
  return (val & 0x1f);
#endif
}

/*****************************************************************************/
/* INLINE                  kdu_count_trailing_zeros64                        */
/*****************************************************************************/

static inline kdu_uint32 kdu_count_trailing_zeros64(kdu_uint64 val)
{ /* [BIND: no-bind]
     [SYNOPSIS]
     Same as `kdu_count_trailing_zeros', but for a 64-bit value; this will
     be most efficient on 64-bit platforms.
  */
#ifdef KDU_POINTERS64 // 64-bit OS is the one we focus on
# if (defined _MSC_VER)
  unsigned long result = 0;
  _BitScanForward64(&result,val);
  return (kdu_uint32)result;
# elif (defined __GNUC__)
  return (kdu_uint32)__builtin_ctzll(val);
# else
  val &= -val; // Resets all bits except the least significant 1
  val--; // So the result we are looking for is now the number of 1s in `val'

  // Count the number of 1s by repeated partitioning and addition
  val = (val & 0x5555555555555555) + ((val >> 1) & 0x5555555555555555);
  val = (val & 0x3333333333333333) | ((val >> 2) & 0x3333333333333333);
  val = (val & 0x0f0f0f0f0f0f0f0f) | ((val >> 4) & 0x0f0f0f0f0f0f0f0f);
  val += (val >> 8);
  val += (val >> 16);
  val += (val >> 32);
  return (kdu_uint32)(val & 0x3f);
# endif
#else // only 32-bit platform; cannot evaluate both halves and then combine
      // since behaviour would be undefined if either half were 0.
  kdu_uint32 word = (kdu_uint32)val;
  if (word)
    return kdu_count_trailing_zeros(word);
  else
    { 
      word = (kdu_uint32)(val>>32);
      return 32 + kdu_count_trailing_zeros(word);
    }
#endif
}

/*****************************************************************************/
/* INLINE                        kdu_memsafe_add                             */
/*****************************************************************************/

static inline size_t kdu_memsafe_add(size_t x, size_t y)
{ /* [BIND: no-bind]
     [SYNOPSIS]
     Convenience function that can be used by application developers in
     preparing to allocate memory via new, malloc or similar.  The function
     returns the sum of `x' and `y', unless it would overflow, in which case
     `KDU_SIZE_MAX' is returned, which is sufficient to ensure that a heap
     memory allocation based on the return value fails, without taking any
     extra precautions.
     [//]
     While overflow testing alone should be sufficient for memory allocation,
     in cases where signed integer arithmetic is used to generate array indices
     or pointer/address offsets, there is a risk that signed integer overflow
     might occur, resulting in negative indices that were not intended.  To
     avoid this risk, assuming that the index generation arithmetic is
     otherwise logically sound with regard to allocated array bounds, it is
     recommended that `kdu_idx_t' be used as the type for signed index/offset
     computation.  When this approach is followed, the risk of unintended
     overflow is mostly avoided by avoiding allocations with a size that
     exceeds `KDU_IDX_MAX'.  In practice, this is not a limitation.
     Accordingly, the implementation of this function actually returns
     the failure-inducing value of `KDU_SIZE_MAX' whenever the sum of `x'
     and `y' would exceed `KDU_IDX_MAX'.
     [//]
     The companion function `kdu_memsafe_mul' is even more useful, since
     numeric overflow is more common during multiplication, and it is common
     in image processing applications to dimension memory buffers based on the
     product of a width and a height.
  */
  size_t result = x + y;
  return ((x | y | result) > KDU_IDX_MAX)?KDU_SIZE_MAX:result;
}

/*****************************************************************************/
/* INLINE                        kdu_memsafe_mul                             */
/*****************************************************************************/

static inline size_t kdu_memsafe_mul(size_t x, size_t y)
{ /* [BIND: no-bind]
     [SYNOPSIS]
     Convenience function that can be used by application developers in
     preparing to allocate memory via new, malloc or similar.  The function
     returns the product of `x' and `y', unless it would overflow, in which
     case `KDU_SIZE_MAX' is returned, which is sufficient to ensure that a
     heap memory allocation based on the return value fails, without taking any
     extra precautions.
     [//]
     If one of the multiplicands is a constant, it is preferable to use `y'
     for that value.
     [//]
     As with `kdu_memsafe_add', we actually return `KDU_SIZE_MAX' even if
     the result of the product would exceed `KDU_IDX_MAX', which is half the
     size of `KDU_SIZE_MAX', since this allows `kdu_idx_t'-based signed index
     computation to be used without having to be overly careful about
     memory overwrites that might occur due to numerical overflow.
     [//]
     See also `kdu_memsafe_add' and `kdu_memsafe_mulo'.
  */
  size_t result = x*y;
  if ((x | y) <= KDU_SAFE_ROOT_SIZE_MAX)
    return (result > KDU_IDX_MAX)?KDU_SIZE_MAX:result;
  else if ((y==0) || (x <= (KDU_IDX_MAX/y)))
    return result;
  else
    return KDU_SIZE_MAX;
}

/*****************************************************************************/
/* INLINE                       kdu_memsafe_mulo                             */
/*****************************************************************************/

static inline size_t kdu_memsafe_mulo(size_t x, size_t y, size_t off)
{ /* [BIND: no-bind]
     [SYNOPSIS]
     Combines `kdu_memsafe_mul' with `kdu_memsafe_add' to return the offset
     product `x' * `y' + `off', unless it would incur numeric overflow, or
     produce a value larger than `KDU_IDX_MAX', in which case `KDU_SIZE_MAX' is
     returned, which is sufficient to ensure that a heap memory allocation
     based on the return value fails, without taking any extra precautions.
  */
  return kdu_memsafe_add(kdu_memsafe_mul(x,y),off);
}

/*****************************************************************************/
/* INLINE                   kdu_round_to_int32_valid                         */
/*****************************************************************************/

static inline kdu_int32 kdu_round_to_int32_valid(float fval)
{ /* [SYNOPSIS]
       Convenient function to round a floating point value to a 32-bit
       integer, in cases where the caller already has reasons to be sure
       that the conversion is valid, meaning that `fval' lies within the
       range `KDU_INT32_MIN' to `KDU_INT32_MAX'.
  */
  return (kdu_int32) floorf(fval+0.5f);
}

/*****************************************************************************/
/* INLINE                   kdu_round_to_int32_valid                         */
/*****************************************************************************/

static inline kdu_int32 kdu_round_to_int32_valid(double fval)
{ /* [SYNOPSIS]
     Convenient function to round a double-precision value to a 32-bit
     integer, in cases where the caller already has reasons to be sure
     that the conversion is valid, meaning that `fval' lies within the
     range `KDU_INT32_MIN' to `KDU_INT32_MAX'.
   */
  return (kdu_int32) floor(fval);
}

/*****************************************************************************/
/* INLINE                   kdu_round_to_int32_clip                          */
/*****************************************************************************/

static inline kdu_int32 kdu_round_to_int32_clip(float fval)
{ /* [SYNOPSIS]
       Same as `kdu_round_to_int32_valid', except that `fval' might possibly
       lie outside the valid conversion range of `KDU_INT32_MIN' to
       `KDU_INT32_MAX'; if so, the value is clipped to this range. */
  fval += 0.5f; // rounding offset
  if (fval > KDU_INT32_MAX)
    return KDU_INT32_MAX;
  else if (fval < KDU_INT32_MIN)
    return KDU_INT32_MIN;
  else
    return (kdu_int32) floorf(fval); // already added the rounding offset
}

/*****************************************************************************/
/* INLINE                   kdu_round_to_int32_clip                          */
/*****************************************************************************/

static inline kdu_int32 kdu_round_to_int32_clip(double fval)
{ /* [SYNOPSIS]
       Same as the first form of `kdu_round_to_int32_clip', except that
       `fval' has double precision. */
  fval += 0.5; // rounding offset
  if (fval > KDU_INT32_MAX)
    return KDU_INT32_MAX;
  else if (fval < KDU_INT32_MIN)
    return KDU_INT32_MIN;
  else
    return (kdu_int32) floor(fval); // already added the rounding offset
}

/*****************************************************************************/
/* INLINE                   kdu_round_to_int32_dnc                           */
/*****************************************************************************/

static inline kdu_int32 kdu_round_to_int32_dnc(float fval)
{ /* [SYNOPSIS]
       Same as `kdu_round_to_int32_clip', except that if `fval' does lie
       outside the range `KDU_INT32_MIN' to `KDU_INT32_MAX', its actual value
       is of no interest -- e.g., a multiplication factor where the other
       multiplicand is certain to be 0.  The difference here is that values
       outside the representable range convert to 0, rather than the nearest
       representable value.  This function could be implemented without any
       checks at all, except in environments (rare) that might be configured
       to throw an exception for out-of-range values.
       [//]
       FYI: the suffix in the function name stands for "do not care", because
       we do not care what the result of an out-of-range conversion is.
  */
  fval += 0.5f; // rounding offset
  if ((fval > KDU_INT32_MAX) || (fval < KDU_INT32_MIN))
    return 0;
  else
    return (kdu_int32) floorf(fval); // already added the rounding offset
}


} // namespace kdu_core

#endif // KDU_UTILS_H
