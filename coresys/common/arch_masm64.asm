COMMENT $
/*****************************************************************************/
// File: arch_masm64.asm [scope = CORESYS/COMMON]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Provides CPUID testing code for 64-bit X86 platforms, coded for the 64-bit
version of the Microsoft Macro Assembler (MASM).  This is the only way to
perform CPUID support testing in Microsoft's .NET 2005 compiler.
******************************************************************************/
$

;=============================================================================
; MACROS
;=============================================================================


.code

;=============================================================================
; EXTERNALLY CALLABLE FUNCTIONS FOR REGULAR BLOCK DECODING
;=============================================================================

;*****************************************************************************
; PROC: x64_get_mmx_level
;*****************************************************************************
x64_get_mmx_level PROC USES rbx
      ; Result (integer) returned via EAX/RAX
      ; Registers used: RAX, RCX, RDX

  xor ecx, ecx
  mov eax, 1
  cpuid
  test edx, 00800000h
  JZ no_mmx_exists
    mov eax, edx
    and eax, 06000000h
    cmp eax, 06000000h
    JNZ mmx_exists
      test ecx, 1
      JZ sse2_exists
        test ecx, 200h
        JZ sse3_exists
          mov ebx, ecx
		      and ebx, 00980000h
          cmp ebx, 00980000h ; Are SSE4.1, SSE4.2 and POPCNT flags all set?
          JNZ ssse3_exists
ifndef KDU_NO_AVX
            and ecx, 18000000h  ; Mask off OSXSAVE and AVX feature flags
            cmp ecx, 18000000h  ; Check that both flags are present
            JNE sse41_exists
              xor ecx, ecx    ; Specify 0 for XFEATURE_ENABLED_MASK
        			xgetbv          ; Result returned in EDX:EAX
		  	      and eax, 06h
              cmp eax, 06h    ; Check OS saves both XMM and YMM state
		        	JE avx_exists   ; AVX is fully supported
endif
		      JMP sse41_exists
avx2_exists:
  mov eax, 7
  JMP done
avx_exists:
ifndef KDU_NO_AVX2
    xor ecx,ecx
    xor ebx,ebx
    mov eax, 7    ; Call cpuid with EAX=7 (tests above used EAX=1)
    cpuid
    test ebx, 020h  ; Check bit-5 of EBX (AVX2 bit)
    JNZ avx2_exists
endif
  mov eax, 6
  JMP done
sse41_exists:
  mov eax, 5
  JMP done
ssse3_exists:
  mov eax, 4
  JMP done
sse3_exists:
  mov eax, 3
  JMP done
sse2_exists:
  mov eax, 2
  JMP done
mmx_exists:
  mov eax, 1
  JMP done
no_mmx_exists:
  mov eax, 0
done:
  ret
;-----------------------------------------------------------------------------
x64_get_mmx_level ENDP

;*****************************************************************************
; PROC: x64_get_cmov_exists
;*****************************************************************************
x64_get_cmov_exists PROC USES rbx
      ; Result (boolean) returned via EAX/RAX
      ; Registers used: RAX, RCX, RDX
  xor ecx, ecx
  mov eax, 1
  cpuid
  test edx, 00008000h
  mov eax, 0
  JZ @F
    mov eax, 1 ; CMOV exists
@@:
  ret
;-----------------------------------------------------------------------------
x64_get_cmov_exists ENDP

;*****************************************************************************
; PROC: x64_get_bmi2_level
;*****************************************************************************
x64_get_bmi2_level PROC USES rbx
      ; Result (int) returned via EAX/RAX
      ; Registers used: RAX, RBX, RCX, RDX
  xor ecx, ecx
  xor ebx, ebx
  mov eax, 7
  cpuid
  and ebx, 00000108h
  cmp ebx, 00000108h
  JNE bmi2_level_0 ; No BMI2 support
  xor ecx, ecx
  xor ebx, ebx
  mov eax, 80000001h
  cpuid
  test ecx, 32
  JZ bmi2_level_0 ; No LZCNT support
  xor eax, eax
  cpuid
  cmp ebx, 756e6547h
  JNZ bmi2_level_1 ; Not Genuine Intel -- assume slow PDEP/PEXT
  cmp edx, 49656e69h
  JNZ bmi2_level_1 ; Not Genuine Intel -- assume slow PDEP/PEXT
  cmp ecx, 6c65746eh
  JNZ bmi2_level_1 ; Not Genuine Intel -- assume slow PDEP/PEXT
  mov eax, 2 ; return 2
  JMP bmi2_done
bmi2_level_1:
  mov eax, 1 ; return 1
  JMP bmi2_done
bmi2_level_0:
  mov eax, 0 ; return 0
bmi2_done:
  ret
;-----------------------------------------------------------------------------
x64_get_bmi2_level ENDP


END
