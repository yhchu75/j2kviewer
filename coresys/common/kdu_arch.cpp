/*****************************************************************************/
// File: kdu_arch.cpp [scope = CORESYS/COMMON]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
  Defines and evaluates the architecture-specific variables defined in
"kdu_arch.h".
******************************************************************************/

#include "kdu_arch.h"
using namespace kdu_core;

/* ========================================================================= */
/*                           SIMD Support Testing                            */
/* ========================================================================= */

#if ((defined KDU_PENTIUM_MSVC) || (defined KDU_PENTIUM_GCC) || \
     (defined KDU_X86_INTRINSICS))
#  define __KDU_X86__
#endif

#if (defined KDU_NEON_INTRINSICS)
#  ifndef KDU_NO_NEON
#    define __KDU_NEON__
#  endif
#endif


//--------------------------- KDU_NO_CPUID_TEST -------------------------------
#if ((defined KDU_NO_CPUID_TEST) && (defined __KDU_X86__))
        // Define KDU_NO_CPUID_TEST if the CPUID instruction has been disabled
        // on your platform.
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_altivec_exists = false;
  bool kdu_core::kdu_get_sparcvis_exists() { return false; }
  bool kdu_core::kdu_get_altivec_exists() { return false; }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
# ifdef KDU_POINTERS64 /* Assume 64-bit x86 CPUs have CMOV and at least SSE2 */
    bool kdu_core::kdu_pentium_cmov_exists = true;
    bool kdu_core::kdu_get_pentium_cmov_exists() { return true; }
    int kdu_core::kdu_mmx_level = 2;
    int kdu_core::kdu_get_mmx_level() { return 2; }
# else // !defined KDU_POINTERS64
    int kdu_core::kdu_mmx_level = 1; // Assume CPU has at least MMX support
    bool kdu_core::kdu_pentium_cmov_exists = false;
    bool kdu_core::kdu_get_pentium_cmov_exists() { return false; }
    int kdu_core::kdu_get_mmx_level() { return 1; }
# endif // !defined KDU_POINTERS64
    int kdu_core::kdu_x86_bmi2_level = 0; // Assume no BMI support at all
    int kdu_core::kdu_get_bmi2_level() { return 0; }

//---------------------- Microsoft System, 64-bit X86 -------------------------
#elif (defined __KDU_X86__) && (defined _WIN64) && (defined _MSC_VER)
  extern "C" int x64_get_mmx_level();     // These functions are implemented
  extern "C" bool x64_get_cmov_exists();  // in "arch_masm64.asm".
  extern "C" int x64_get_bmi2_level();

  int kdu_core::kdu_mmx_level = x64_get_mmx_level();
  int kdu_core::kdu_get_mmx_level() { return x64_get_mmx_level(); }
  bool kdu_core::kdu_pentium_cmov_exists = x64_get_cmov_exists();
  bool kdu_core::kdu_get_pentium_cmov_exists() { return x64_get_cmov_exists();}
  int kdu_core::kdu_x86_bmi2_level = x64_get_bmi2_level();
  int kdu_core::kdu_get_x86_bmi2_level() { return x64_get_bmi2_level(); }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_altivec_exists = false;
  bool kdu_core::kdu_get_sparcvis_exists() { return false; }
  bool kdu_core::kdu_get_altivec_exists() { return false; }

//---------------------- Microsoft System, 32-bit X86 -------------------------
#elif (defined __KDU_X86__) && (defined _WIN32) && (defined _MSC_VER)
  static int get_mmx_level();
  static bool get_cmov_exists();
  static int get_bmi2_level();

  int kdu_core::kdu_mmx_level = get_mmx_level();
  int kdu_core::kdu_get_mmx_level() { return get_mmx_level(); }
  bool kdu_core::kdu_pentium_cmov_exists = get_cmov_exists();
  bool kdu_core::kdu_get_pentium_cmov_exists() { return get_cmov_exists(); }
  int kdu_core::kdu_x86_bmi2_level = get_bmi2_level();
  int kdu_core::kdu_get_x86_bmi2_level() { return get_bmi2_level(); }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_altivec_exists = false;
  bool kdu_core::kdu_get_sparcvis_exists() { return false; }
  bool kdu_core::kdu_get_altivec_exists() { return false; }

  static bool get_cmov_exists()
    { 
      int edx_val=0;
      __try { 
          __asm { 
              MOV eax,1
              CPUID
              MOV edx_val,EDX
            }
        }
      __except (EXCEPTION_EXECUTE_HANDLER) { return false; }
      return ((edx_val & 0x00008000) != 0);
    }

  static int get_bmi2_level()
    { 
      int ebx_val=0, ecx_val=0, edx_val=0;
      __try { 
        __asm { 
          XOR ebx,ebx
          XOR ecx,ecx
          MOV eax,7
          CPUID
          MOV ebx_val,ebx
          XOR ecx,ecx
          MOV eax,0x80000001
          CPUID
          MOV ecx_val,ecx
        }
      }
      __except (EXCEPTION_EXECUTE_HANDLER) { return 0; }
      bool bmi2 = ((ebx_val & 0x00000108) == 0x00000108); // BMI1 and BMI2
      bool lzcnt = ((ecx_val & (1<<5)) != 0); // LZCNT bit with EAX=80000001H
      if (!(bmi2 && lzcnt))
        return 0;
      __try { 
        __asm { 
          XOR eax,eax
          CPUID
          MOV ebx_val,ebx
          MOV ecx_val,ecx
          MOV edx_val,edx
        }
      }
      __except (EXCEPTION_EXECUTE_HANDLER) { return 1; }
      if ((ebx_val == 0x756e6547) && (edx_val == 0x49656e69) &&
          (ecx_val == 0x6c65746e))
        return 2; // Genuine Intel processors have fast PDEP/PEXT
      return 1; // Safest to assume that PDEP/PEXT are slow
    }

  static int get_mmx_level()
    { 
      int edx_val=0, ecx_val=0;
      __try { 
          __asm { 
              XOR edx,edx
              XOR ecx,ecx
              MOV eax,1
              CPUID
              MOV edx_val,edx
              MOV ecx_val,ecx
            }
        }
      __except (EXCEPTION_EXECUTE_HANDLER) { return 0; }
      int result = 0;
      if (edx_val & 0x00800000)
        { 
          result = 1; // Basic MMX features exist
#ifndef KDU_NO_SSE
          if ((edx_val & 0x06000000) == 0x06000000)
            { 
              result = 2; // SSE and SSE2 features exist
              __try { // Just to be quite certainm, try an SSE2 instruction
                  __asm xorpd xmm0,xmm0
                }
              __except (EXCEPTION_EXECUTE_HANDLER) { result = 1; }
            }
#endif // !KDU_NO_SSE
        }
      if ((result == 2) && (ecx_val & 1))
        { 
          result = 3; // SSE3 support exists
          if (ecx_val & 0x00000200)
            { 
              result = 4; // SSSE3 support exists
              if ((ecx_val & 0x00980000) == 0x00980000)
                result = 5; // SSE4.1, SSE4.2 and POPCNT support exists
            }
        }
#  ifndef KDU_NO_AVX
      if ((result == 5) && ((ecx_val & 0x18000000) == 0x18000000))
        { // OSXSAVE and AVX feature flags exist; try XGETBV instruction
          int eax_val=0;
          __try { 
              __asm { 
                  XOR ecx,ecx
                  XGETBV
                  MOV eax_val,eax
                }
            }
          __except (EXCEPTION_EXECUTE_HANDLER) { return 5; }
          if ((eax_val & 6) == 6)
            result = 6; // Operating System preserves YMM registers
        }
#  endif // !KDU_NO_AVX
#  ifndef KDU_NO_AVX2
      if ((result == 6) && (ecx_val & (1<<12)))
        { // FMA exists; let's see about AVX2
          int ebx_val=0;
          __try { 
            __asm { 
              XOR ebx,ebx
              XOR ecx,ecx
              MOV eax,7
              CPUID
              MOV ebx_val,ebx
            }
          }
          __except (EXCEPTION_EXECUTE_HANDLER) { return 6; }
          if (ebx_val & (1<<5)) // Bit-5 of EBX for CPUID(7) is the AVX2 bit
            result = 7;
        }
#  endif // !KDU_NO_AVX2
      return result;
    }
//------------------- GCC/CLANG Build, X86 64-bit Platform --------------------
#elif (defined __KDU_X86__) && (defined KDU_POINTERS64)
  static int get_mmx_level();
  static bool get_cmov_exists();
  static int get_bmi2_level();

  int kdu_core::kdu_mmx_level = get_mmx_level();
  int kdu_core::kdu_get_mmx_level() { return get_mmx_level(); }
  bool kdu_core::kdu_pentium_cmov_exists = get_cmov_exists();
  bool kdu_core::kdu_get_pentium_cmov_exists() { return get_cmov_exists(); }
  int kdu_core::kdu_x86_bmi2_level = get_bmi2_level();
  int kdu_core::kdu_get_x86_bmi2_level() { return get_bmi2_level(); }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_altivec_exists = false;
  bool kdu_core::kdu_get_sparcvis_exists() { return false; }
  bool kdu_core::kdu_get_altivec_exists() { return false; }

  static bool get_cmov_exists()
    { 
      int edx_val=0;
      __asm__ volatile ("movq %%rbx,%%rsi\n\t" // Save PIC register RBX in RSI
                        "xorl %%ecx,%%ecx\n\t"
                        "movq $1,%%rax\n\t"
                        "cpuid\n\t"
                        "movl %%edx,%0\n\t"
                        "movq %%rsi,%%rbx" // Restore the PIC register RBX
                        : "=m" (edx_val) : /* no input */
                        : "%rax","%rsi","%rcx","%rdx");
      return ((edx_val & 0x00008000) != 0);
    }

  static int get_bmi2_level()
    { 
      int ebx_val=0, edx_val=0, ecx_val=0;
      __asm__ volatile ("movq %%rbx,%%rsi\n\t" // Save PIC register RBX in RSI
                        "xorl %%ecx,%%ecx\n\t"
                        "movq $7,%%rax\n\t"
                        "cpuid\n\t"
                        "movl %%ebx,%0\n\t"
                        "xorl %%ecx,%%ecx\n\t"
                        "movq $0x80000001,%%rax\n\t"
                        "cpuid\n\t"
                        "movl %%ecx,%1\n\t"
                        "movq %%rsi,%%rbx" // Restore the PIC register RBX
                        : "=m" (ebx_val), "=m" (ecx_val) : /* no input */
                        : "%rax","%rsi","%rcx","%rdx");
      bool bmi2 = ((ebx_val & 0x00000108) == 0x00000108); // BMI1 and BMI2
      bool lzcnt = (ecx_val & (1<<5)); // LZCNT bit found with EAX=80000001H
      if (!(bmi2 && lzcnt))
        return 0;
      __asm__ volatile ("movq %%rbx,%%rsi\n\t" // Save PIC register RBX in RSI
                        "xorl %%eax,%%eax\n\t"
                        "cpuid\n\t"
                        "movl %%ebx,%0\n\t"
                        "movl %%ecx,%1\n\t"
                        "movl %%edx,%2\n\t"
                        "movq %%rsi,%%rbx" // Restore the PIC register RBX
                        : "=m" (ebx_val), "=m" (ecx_val), "=m" (edx_val)
                        : /* no input */
                        : "%rax","%rsi","%rcx","%rdx");
      if ((ebx_val == 0x756e6547) && (edx_val == 0x49656e69) &&
          (ecx_val == 0x6c65746e))
        return 2; // Genuine Intel processors have fast PDEP/PEXT
      return 1; // Safest to assume that PDEP/PEXT are slow
    }

  static int get_mmx_level()
    { 
      int edx_val=0, ecx_val=0;
      __asm__ volatile ("movq %%rbx,%%rsi\n\t" // Save PIC register RBX in RSI
                        "movq $1,%%rax\n\t"
                        "cpuid\n\t"
                        "movl %%edx,%0\n\t"
                        "movl %%ecx,%1\n\t"
                        "movq %%rsi,%%rbx" // Restore the PIC register RBX
                        : "=m" (edx_val), "=m" (ecx_val) : /* no input */
                        : "%rax","%rsi","%rcx","%rdx");
      int result = 0;
      if (edx_val & 0x00800000)
        { 
          result = 1; // Basic MMX features exist
          if ((edx_val & 0x06000000) == 0x06000000)
            result = 2;
        }
      if ((result == 2) && (ecx_val & 1))
        { 
          result = 3; // SSE3 support exists
          if (ecx_val & 0x00000200)
            { 
              result = 4; // SSSE3 support exists
              if ((ecx_val & 0x00980000) == 0x00980000)
                result = 5; // SSE4.1, SSE4.2 and POPCNT support exists
            }
        }
      if ((result == 5) && ((ecx_val & 0x18000000) == 0x18000000))
        { // OSXSAVE and AVX feature flags exist; try XGETBV instruction
          int eax_val=0;
          __asm__ volatile ("xorq %%rcx,%%rcx\n\t" // XFEATURE_ENABLED_MASK=0
                            ".byte 0x0F,0x01,0xD0\n\t" // XGETBV instruction
                            "movl %%eax,%0\n\t"
                            : "=m" (eax_val) : /* no input */
                            : "%rax","%rcx","%rdx");
          if ((eax_val & 6) == 6)
            result = 6; // Operating System preserves YMM registers
        }
#ifndef KDU_NO_AVX2
      if ((result == 6) && (ecx_val & (1<<12)))
        { // FMA exists; let's see about AVX2
          int ebx_val=0;
          __asm__ volatile ("movq %%rbx,%%rsi\n\t" // Save the PIC register RBX
                            "xorl %%ebx,%%ebx\n\t"
                            "xorl %%ecx,%%ecx\n\t"
                            "movq $7,%%rax\n\t"
                            "cpuid\n\t"
                            "movl %%ebx,%0\n\t"
                            "movq %%rsi,%%rbx" // Restore the PIC register RBX
                            : "=m" (ebx_val) : /* no input */
                            : "%rax","%rcx","%rdx","%rsi");
          if (ebx_val & (1<<5)) // Bit-5 of EBX for CPUID(7) is the AVX2 bit
            result = 7;
        }
#endif // !KDU_NO_AVX2
      return result;
    }

//------------------- GCC/CLANG build, 32-bit X86 -----------------------------
#elif (defined __KDU_X86__)
  static bool check_cpuid_exists();
  static int get_mmx_level();
  static bool get_cmov_exists();
  static int get_bmi2_level();

  int kdu_core::kdu_mmx_level = get_mmx_level();
  int kdu_core::kdu_get_mmx_level() { return get_mmx_level(); }
  bool kdu_core::kdu_pentium_cmov_exists = get_cmov_exists();
  bool kdu_core::kdu_get_pentium_cmov_exists() { return get_cmov_exists(); }
  int kdu_core::kdu_x86_bmi2_level = get_bmi2_level();
  int kdu_core::kdu_get_x86_bmi2_level() { return get_bmi2_level(); }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_altivec_exists = false;
  bool kdu_core::kdu_get_sparcvis_exists() { return false; }
  bool kdu_core::kdu_get_altivec_exists() { return false; }

  static bool check_cpuid_exists()
    { 
      kdu_uint16 orig_flags16, mod_flags16;
      __asm__ volatile ("pushfw\n\t"
                        "popw %%ax\n\t" // Get flags into AX
                        "movw %%ax,%%cx\n\t" // Save copy of flags in CX
                        "movw %%cx,%0\n\t"
                        "xorw $0xF000,%%ax\n\t" // Flip bits 12-15
                        "pushw %%ax\n\t" // Put modified flags back on stack
                        "popfw\n\t" // Replace flags
                        "pushfw\n\t" // Push flags back on the stack
                        "popw %%ax\n\t" // Get flags back into AX
                        "movw %%ax,%1\n\t"
                        "pushw %%cx\n\t" // Put original flags back on stack
                        "popfw"
                        : "=m" (orig_flags16), "=m" (mod_flags16)
                        : /* no input */
                        : "%ax","%cx" );
      if (orig_flags16 == mod_flags16)
        return false; // Failed to toggle bits 12-15 of flags register.
                      // Must be 8086/88, 80186 or 80286.

      kdu_uint32 orig_flags32, mod_flags32;
      __asm__ volatile ("pushfl\n\t" // Push eflags onto stack
                        "popl %%eax\n\t" // Get eflags into EAX
                        "movl %%eax,%%ecx\n\t" // Save copy of eflags in ECX
                        "movl %%ecx,%0\n\t"
                        "xorl $0x40000,%%eax\n\t" // Flip AC bit
                        "pushl %%eax\n\t" // Put modified eflags back on stack
                        "popfl\n\t" // Replace eflags
                        "pushfl\n\t" // Push eflags back on the stack
                        "popl %%eax\n\t" // Get eflags back into EAX
                        "movl %%eax,%1\n\t"
                        "pushl %%ecx\n\t" // Put original eflags back on stack
                        "popfl" // Put back original eflags
                        : "=m" (orig_flags32), "=m" (mod_flags32)
                        : /* no input */
                        : "%eax","%ecx" );
      if (orig_flags32 == mod_flags32)
        return false; // Failed to toggle AC bit of eflags register
                      // Must be 80386.

      __asm__ volatile ("pushfl\n\t" // Push eflags onto stack
                        "popl %%eax\n\t" // Get eflags into EAX
                        "movl %%eax,%%ecx\n\t" // Save copy of eflags in ECX
                        "movl %%ecx,%0\n\t"
                        "xorl $0x200000,%%eax\n\t" // Flip ID bit
                        "pushl %%eax\n\t" // Put modified eflags back on stack
                        "popfl\n\t" // Replace eflags
                        "pushfl\n\t" // Push eflags back on the stack
                        "popl %%eax\n\t" // Get eflags back into EAX
                        "movl %%eax,%1\n\t"
                        "pushl %%ecx\n\t" // Put original eflags back on stack
                        "popfl" // Put back original eflags
                        : "=m" (orig_flags32), "=m" (mod_flags32)
                        : /* no input */
                        : "%eax","%ecx" );
      if (orig_flags32 == mod_flags32)
        return false; // Unable to toggle ID bit of eflags register
                      // Must be 80486
      kdu_uint32 max_eax;
      __asm__ volatile ("movl %%ebx,%%esi\n\t" // Save PIC register EBX in ESI
                        "xorl %%eax,%%eax\n\t" // Zero EAX
                        "cpuid\n\t"
                        "movl %%eax,%0\n\t"
                        "movl %%esi,%%ebx" // Restore the PIC register EBX
                        : "=m" (max_eax) : /* no input */
                        : "%eax","%ecx","%edx","%esi");
      if (max_eax < 1)
        return false; // We won't be able to invoke CPUID with EAX=1 to test
                      // for MMX/SSE/SSE2 support.
      return true;
    }

  static bool get_cmov_exists()
    { 
      if (!check_cpuid_exists()) return false;
      int edx_val=0;
      __asm__ volatile ("movl %%ebx,%%esi\n\t" // Save PIC register EBX in ESI
                        "xorl %%ecx,%%ecx\n\t"
                        "movl $1,%%eax\n\t"
                        "cpuid\n\t"
                        "movl %%edx,%0\n\t"
                        "movl %%esi,%%ebx" // Restore the PIC register EBX
                        : "=m" (edx_val) : /* no input */
                        : "%eax","%ecx","%edx","%esi");
      return ((edx_val & 0x00008000) != 0);
    }

  static int get_bmi2_level()
    { 
    if (!check_cpuid_exists()) return false;
      int ebx_val=0, ecx_val=0, edx_val=0;
      __asm__ volatile ("movl %%ebx,%%esi\n\t" // Save PIC register RBX in RSI
                        "xorl %%ecx,%%ecx\n\t"
                        "movl $7,%%eax\n\t"
                        "cpuid\n\t"
                        "movl %%ebx,%0\n\t"
                        "xorl %%ecx,%%ecx\n\t"
                        "movl $0x80000001,%%eax\n\t"
                        "cpuid\n\t"
                        "movl %%ecx,%1\n\t"
                        "movl %%esi,%%ebx" // Restore the PIC register RBX
                        : "=m" (ebx_val), "=m" (ecx_val) : /* no input */
                        : "%eax","%esi","%ecx","%edx");
      bool bmi2 = ((ebx_val & 0x00000108) == 0x00000108); // BMI1 and BMI2
      bool lzcnt = (ecx_val & (1<<5)); // LZCNT bit found with EAX=80000001H
      if (!(bmi2 && lzcnt))
        return 0;
      __asm__ volatile ("movl %%ebx,%%esi\n\t" // Save PIC register EBX in ESI
                        "xorl %%eax,%%eax\n\t"
                        "cpuid\n\t"
                        "movl %%ebx,%0\n\t"
                        "movl %%ecx,%1\n\t"
                        "movl %%edx,%2\n\t"
                        "movl %%esi,%%ebx" // Restore the PIC register EBX
                        : "=m" (ebx_val), "=m" (ecx_val), "=m" (edx_val)
                        : /* no input */
                        : "%rax","%rsi","%rcx","%rdx");
      if ((ebx_val == 0x756e6547) && (edx_val == 0x49656e69) &&
          (ecx_val == 0x6c65746e))
        return 2; // Genuine Intel processors have fast PDEP/PEXT
      return 1; // Safest to assume that PDEP/PEXT are slow
    }

  static int get_mmx_level()
    { 
      if (!check_cpuid_exists()) return 0;
      int edx_val=0, ecx_val=0;
      __asm__ volatile ("movl %%ebx,%%esi\n\t" // Save PIC register EBX in ESI
                        "xorl %%ecx,%%ecx\n\t"
                        "movl $1,%%eax\n\t"
                        "cpuid\n\t"
                        "movl %%edx,%0\n\t"
                        "movl %%ecx,%1\n\t"
                        "movl %%esi,%%ebx" // Restore the PIC register EBX
                        : "=m" (edx_val), "=m" (ecx_val) : /* no input */
                        : "%eax","%ecx","%edx","%esi");
      int result = 0;
      if (edx_val & 0x00800000)
        { 
          result = 1; // Basic MMX features exist
          if ((edx_val & 0x06000000) == 0x06000000)
            result = 2;
        }
      if ((result == 2) && (ecx_val & 1))
        { 
          result = 3; // SSE3 support exists
          if (ecx_val & 0x00000200)
            { 
              result = 4; // SSSE3 support exists
              if ((ecx_val & 0x00980000) == 0x00980000)
                result = 5; // SSE4.1, SSE4.2 and POPCNT support exists
            }
        }
      if ((result == 5) && ((ecx_val & 0x18000000) == 0x18000000))
        { // OSXSAVE and AVX feature flags exist; try XGETBV instruction
          int eax_val=0;
          __asm__ volatile ("xorl %%ecx,%%ecx\n\t" // XFEATURE_ENABLED_MASK=0
                            ".byte 0x0F,0x01,0xD0\n\t" // XGETBV instruction
                            "movl %%eax,%0\n\t"
                            : "=m" (eax_val) : /* no input */
                            : "%eax","%ecx","%edx");
          if ((eax_val & 6) == 6)
            result = 6; // Operating System preserves YMM registers
        }
#ifndef KDU_NO_AVX2
      if ((result == 6) && (ecx_val & (1<<12)))
        { // FMA exists; let's see about AVX2
          int ebx_val=0;
          __asm__ volatile ("movl %%ebx,%%esi\n\t" // Save PIC register EBX
                            "xorl %%ebx,%%ebx\n\t"
                            "xorl %%ecx,%%ecx\n\t"
                            "movl $7,%%eax\n\t"
                            "cpuid\n\t"
                            "movl %%ebx,%0\n\t"
                            "movl %%esi,%%ebx" // Restore the PIC register EBX
                            : "=m" (ebx_val) : /* no input */
                            : "%eax","%ecx","%edx","%esi");
          if (ebx_val & (1<<5)) // Bit-5 of EBX for CPUID(7) is the AVX2 bit
            result = 7;
        }
#endif // !KDU_NO_AVX2
      return result;
    }

//------------------------ IOS/MAC build for ARM ------------------------------
#elif (defined __KDU_NEON__) && (defined __APPLE__)
bool kdu_core::kdu_pentium_cmov_exists = false;
bool kdu_core::kdu_get_pentium_cmov_exists() { return false; }
int kdu_core::kdu_mmx_level = 0;
int kdu_core::kdu_get_mmx_level() { return 0; }
int kdu_core::kdu_x86_bmi2_level = 0;
int kdu_core::kdu_get_x86_bmi2_level() { return 0; }
bool kdu_core::kdu_sparcvis_exists = false;
bool kdu_core::kdu_get_sparcvis_exists() { return false; }
bool kdu_core::kdu_altivec_exists = false;
bool kdu_core::kdu_get_altivec_exists() { return false; }
// All Apple's ARM devices should support NEON
int kdu_core::kdu_neon_level=1;
int kdu_core::kdu_get_neon_level() { return 1; }

//------------------------ Android build for ARM ------------------------------
#elif (defined __KDU_NEON__) && (defined __ANDROID__)
bool kdu_core::kdu_pentium_cmov_exists = false;
bool kdu_core::kdu_get_pentium_cmov_exists() { false; }
int kdu_core::kdu_mmx_level = 0;
int kdu_core::kdu_get_mmx_level() { return 0; }
int kdu_core::kdu_x86_bmi2_level = 0;
int kdu_core::kdu_get_x86_bmi2_level() { return 0; }
bool kdu_core::kdu_sparcvis_exists = false;
bool kdu_core::kdu_get_sparcvis_exists() { return false; }
bool kdu_core::kdu_altivec_exists = false;
bool kdu_core::kdu_get_altivec_exists() { return false; }

#include <cpu-features.h>

int kdu_core::kdu_neon_level = kdu_get_neon_level();
int kdu_core::kdu_get_neon_level()
  { 
#ifdef ANDROID_CPU_ARM_FEATURE_NEON
    if ((android_getCpuFamily() == ANDROID_CPU_FAMILY_ARM) &&
        (android_getCpuFeatures() & ANDROID_CPU_ARM_FEATURE_NEON))
      return 1;
#endif
      return 0;
  }

//------------------------ Linux build for ARM ------------------------------
#elif (defined __KDU_NEON__)
bool kdu_core::kdu_pentium_cmov_exists = false;
bool kdu_core::kdu_get_pentium_cmov_exists() { return false; }
int kdu_core::kdu_mmx_level = 0;
int kdu_core::kdu_get_mmx_level() { return 0; }
int kdu_core::kdu_x86_bmi2_level = 0;
int kdu_core::kdu_get_x86_bmi2_level() { return 0; }
bool kdu_core::kdu_sparcvis_exists = false;
bool kdu_core::kdu_get_sparcvis_exists() { return false; }
bool kdu_core::kdu_altivec_exists = false;
bool kdu_core::kdu_get_altivec_exists() { return false; }

#include <sys/auxv.h>
#include <asm/hwcap.h>

int kdu_core::kdu_neon_level = kdu_get_neon_level();
int kdu_core::kdu_get_neon_level()
{
  return (getauxval(AT_HWCAP) & HWCAP_NEON);
}

//--------------------------- KDU_SPARCVIS_GCC --------------------------------
#elif defined KDU_SPARCVIS_GCC
  static bool get_sparcvis_exists();
  bool kdu_core::kdu_pentium_cmov_exists = false;
  bool kdu_core::kdu_get_pentium_cmov_exists() { return false; }
  int kdu_core::kdu_mmx_level = 0;
  int kdu_core::kdu_get_mmx_level() { return 0; }
  int kdu_core::kdu_x86_bmi2_level = 0;
  int kdu_core::kdu_get_x86_bmi2_level() { return 0; }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = get_sparcvis_exists();
  bool kdu_core::kdu_get_sparcvis_exists() { return get_sparcvis_exists(); }
  bool kdu_core::kdu_altivec_exists = false;
  bool kdu_core::kdu_get_altivec_exists() { return false; }

  static bool get_sparcvis_exists()
    { 
      typedef double kdvis_d64; // Use for 64-bit VIS registers
      union kdvis_4x16 { 
          kdvis_d64 d64;
          kdu_int16 v[4];
        };
      try { 
          kdvis_4x16 val; val.v[0] = 1;
          register kdvis_d64 reg = val.d64;
          __asm__ volatile ("fpadd16 %1,%2,%0" :"=e"(reg):"e"(reg),"e"(reg));
          val.d64 = reg;
          return (val.v[0] == 2);
        }
      catch (...)
        { // Instruction not supported
          return false;
        }
    }

//---------------------------- KDU_ALTIVEC_GCC --------------------------------
#elif defined KDU_ALTIVEC_GCC
# include <sys/types.h>
# include <sys/sysctl.h>
  static bool get_altivec_exists();
  bool kdu_core::kdu_pentium_cmov_exists = false;
  bool kdu_core::kdu_get_pentium_cmov_exists() { return false; }
  int kdu_core::kdu_mmx_level = 0;
  int kdu_core::kdu_get_mmx_level() { return 0; }
  int kdu_core::kdu_x86_bmi2_level = 0;
  int kdu_core::kdu_get_x86_bmi2_level() { return 0; }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_get_sparcvis_exists() { return false; }
  bool kdu_core::kdu_altivec_exists = get_altivec_exists();
  bool kdu_core::kdu_get_altivec_exists() { return get_altivec_exists(); }

#  if (defined CTL_HW) && (defined HS_VECTORUNIT)
    /* Altivec support tests simplest on Darwin platform. */
    static bool get_altivec_exists()
      { 
        try { 
            int vectorunit = 0;
            int mib[2];
            size_t len;
            mib[0] = CTL_HW;
            mib[1] = HW_VECTORUNIT;
            len = sizeof(vectorunit);
            sysctl(mib, 2, &vectorunit, &len, NULL, 0);
            return (vectorunit != 0);
          }
        catch(...)
          { // Just in case something went wrong in the test
            return false;
          }
      }
#else
    /* Use an illegal instruction trap on PPC-based Linux systems. */
#   include <signal.h>
#   include <setjmp.h>

    static jmp_buf jb;

    static void tmp_handler(int val)
      { 
        longjmp(jb,1); // Send non-zero value to `setjmp'
      }

    static bool get_altivec_exists()
      { 
        void (*old_handler)(int);
        old_handler = signal(SIGILL,tmp_handler);
        bool caught_exception = false;
        if (setjmp(jb))
          caught_exception = true;
        else
          { __asm__ __volatile__ (".long 0x10000484"); /* vor 0,0,0 */ }
        signal(SIGILL,old_handler); // Restore SIGILL handler
        return !caught_exception;
      }
#endif

//-------------------------- Unknown Architecture -----------------------------
#else
  bool kdu_core::kdu_pentium_cmov_exists = false;
  bool kdu_core::kdu_get_pentium_cmov_exists() { return false; }
  int kdu_core::kdu_mmx_level = 0;
  int kdu_core::kdu_get_mmx_level() { return 0; }
  int kdu_core::kdu_x86_bmi2_level = 0;
  int kdu_core::kdu_get_x86_bmi2_level() { return 0; }
  int kdu_core::kdu_neon_level = 0;
  int kdu_core::kdu_get_neon_level() { return 0; }
  bool kdu_core::kdu_sparcvis_exists = false;
  bool kdu_core::kdu_altivec_exists = false;
#endif // Architecture tests


/* ========================================================================= */
/*                          Multi-Processor Support                          */
/* ========================================================================= */

#ifdef __APPLE__
# include <sys/param.h>
# include <sys/sysctl.h>
#endif // __APPLE__

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif // HAVE_UNISTD_H

/*****************************************************************************/
/* EXTERN                    kdu_get_num_processors                          */
/*****************************************************************************/

int
  kdu_core::kdu_get_num_processors()
{
#if (defined _WIN64)
  ULONG_PTR proc_mask=0, sys_mask=0;
  if (!GetProcessAffinityMask(GetCurrentProcess(),&proc_mask,&sys_mask))
    return 1;
  int b, result=0;
  for (b=0; b < 64; b++, proc_mask>>=1)
    result += (int)(proc_mask & 1);
  return (result > 1)?result:1;
#elif (defined _WIN32) || (defined WIN32)
  DWORD proc_mask=0, sys_mask=0;
  if (!GetProcessAffinityMask(GetCurrentProcess(),&proc_mask,&sys_mask))
    return 1;
  int b, result=0;
  for (b=0; b < 32; b++, proc_mask>>=1)
    result += (int)(proc_mask & 1);
  return (result > 1)?result:1;
#elif defined _SC_NPROCESSORS_ONLN
  return (int) sysconf(_SC_NPROCESSORS_ONLN);
#elif defined _SC_NPROCESSORS_CONF
  return (int) sysconf(_SC_NPROCESSORS_CONF);
#elif (defined __APPLE__)
  int result = 0;
  size_t result_size = sizeof(result);
  if (sysctlbyname("hw.ncpu",&result,&result_size,NULL,0) != 0)
    result = 0;
  return result;
#else
  return 0;
#endif
}
