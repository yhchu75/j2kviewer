/*****************************************************************************/
// File: kdu_block_coding.h [scope = CORESYS/COMMON]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Uniform interface to embedded block coding (encoding and decoding) services.
******************************************************************************/

#ifndef KDU_BLOCK_CODING_H
#define KDU_BLOCK_CODING_H

#include <assert.h>
#include "kdu_compressed.h"

// Defined here:
namespace kdu_core { 
  class kdu_block_encoder_base;
  class kdu_block_decoder_base;
  class kdu_block_encoder;
  class kdu_block_decoder;
}

namespace kdu_core { 

/* ========================================================================= */
/*                      Class and Structure Definitions                      */
/* ========================================================================= */

/*****************************************************************************/
/*                          kdu_block_encoder_base                           */
/*****************************************************************************/

typedef void
  (*kd_block_encode_func)(kdu_block_encoder_base *, kdu_block *, bool, double,
                          kdu_uint16, kdu_uint16, bool);
typedef void
  (*kd_block_cellular_encode_func)(kdu_block_encoder_base *, kdu_block *, bool,
                                   double, float *, int, int,
                                   kdu_uint16, kdu_uint16, bool);
typedef bool
  (*kd_block_encode16_func)(kdu_block_encoder_base *, kdu_block *,
                            kdu_uint16 *, bool, double, kdu_uint16,
                            kdu_uint16);
typedef bool
  (*kd_block_encode32_func)(kdu_block_encoder_base *, kdu_block *,
                            kdu_uint32 *, bool, double, kdu_uint16,
                            kdu_uint16);

class kdu_block_encoder_base { 
  /* [BIND: reference]
     [SYNOPSIS]
       This base class for `kdu_block_encoder' serves to manage function
       pointers for block encoding, which can be replaced.  The class offers
       a public mechanism to dynamically override an encoding function while
       preserving the original one as a fallback, which can be useful when
       an optimized implementation cannot handle certain corner cases.
  */
  protected:
    kdu_block_encoder_base() // Cannot instantiate the base class directly
      { 
        enc_funcs[0] = enc_funcs[1] = NULL;
        ht_enc_funcs[0] = ht_enc_funcs[1] = NULL;
        enc16_func = NULL;  ht_enc16_func = NULL;
        enc32_func = NULL;  ht_enc32_func = NULL;
        cellular_enc_func = ht_cellular_enc_func = NULL;
      }
  public: // Installing encoding functions.  It is expected that the
          // `kdu_block_encoder' constructor invokes all four `add_...'
          // functions below at least once, but it may add cellular encoding
          // functions that just call the non-cellular equivalent.
          // Note that each `add...' call adds a new "preferred" function,
          // leaving the previously added one to become the fallback function.
    void add_encode_func(kd_block_encode_func func)
      { enc_funcs[1] = enc_funcs[0];  enc_funcs[0] = func;  }
    void add_ht_encode_func(kd_block_encode_func func)
      { ht_enc_funcs[1] = ht_enc_funcs[0];  ht_enc_funcs[0] = func;  }
    void add_cellular_encode_func(kd_block_cellular_encode_func func)
      { cellular_enc_func = func;  }
    void add_ht_cellular_encode_func(kd_block_cellular_encode_func func)
      { ht_cellular_enc_func = func;  }
    void add_encode16_func(kd_block_encode16_func func)
      { enc16_func = func; }
    void add_ht_encode16_func(kd_block_encode16_func func)
      { ht_enc16_func = func; }
    void add_encode32_func(kd_block_encode32_func func)
      { enc32_func = func; }
    void add_ht_encode32_func(kd_block_encode32_func func)
      { ht_enc32_func = func; }
  public: // Retrieving encoding functions.  The `fallback...' functions might
          // return NULL function pointers, but the others should not.
    kd_block_encode_func encode_func(int modes)
      { return (modes&Cmodes_HT)?ht_enc_funcs[0]:enc_funcs[0]; }
    kd_block_encode_func fallback_encode_func(int modes)
      { return (modes&Cmodes_HT)?ht_enc_funcs[1]:enc_funcs[1]; }
    kd_block_encode16_func encode16_func(int modes)
      { return (modes&Cmodes_HT)?ht_enc16_func:enc16_func; }
    kd_block_encode32_func encode32_func(int modes)
      { return (modes&Cmodes_HT)?ht_enc32_func:enc32_func; }
    kd_block_cellular_encode_func cellular_encode_func(int modes)
      { return (modes&Cmodes_HT)?ht_cellular_enc_func:cellular_enc_func; }
  protected: // Function pointers -- may be expanded later to include functions
             // that both quantize and encode in one go.
    kd_block_encode_func enc_funcs[2];
    kd_block_encode_func ht_enc_funcs[2];
    kd_block_encode16_func enc16_func;
    kd_block_encode16_func ht_enc16_func;
    kd_block_encode32_func enc32_func;
    kd_block_encode32_func ht_enc32_func;
    kd_block_cellular_encode_func cellular_enc_func;
    kd_block_cellular_encode_func ht_cellular_enc_func;
  };

/*****************************************************************************/
/*                            kdu_block_encoder                              */
/*****************************************************************************/

class kdu_block_encoder : public kdu_block_encoder_base { 
  /* [BIND: reference]
     [SYNOPSIS]
       Objects of this class need be created only for transcoding or
       other low level operations.  Normally, the relevant block coder
       objects are created by the `kdu_encoder' object which also handles
       quantization and ranging operations.
  */
  public: // Member functions
    KDU_EXPORT kdu_block_encoder();
    KDU_EXPORT void
      speedpack_config(kdu_coords nominal_block_size, int K_max_prime);
      /* [SYNOPSIS]
           This function does nothing unless the current version of the
           software includes the Kakadu speed-pack tools, that typically
           accelerate compression and decompression tasks by a factor of
           around 1.5x, under certain conditions.  The relevant conditions
           are evaluated internally; they may depend upon the supplied
           nominal (or just maximum) code-block dimensions, the maximum
           value of the `K_max_prime' parameter that will occur in the
           `kdu_block' argument passed to the `encode' function.  In practice,
           at the present time, the speed-pack tools are restricted to
           code-blocks whose width and height are at most 64, on 64-bit x86
           operating systems.
           [//]
           If the conditions are met, this function installs accerated
           block coding implementations by invoking functions such as
           `add_encode_func', `add_encode16_func' and `add_encode32_func'.
           Ultra-accelerated implementations of the HT block coding algorithm
           might also be installed.
           [//]
           It is important to pass appropriate values for the
           `nominal_block_size' and `K_max_prime' arguments; otherwise, the
           installed accelerators might turn out to be unsuitable, which
           will prevent any acceleration occurring.  In particular, calls
           to `encode' may internally fall back to the default implementation,
           while calls to `encode16' or `encode32' may return false, even if
           a suitable accelerated implementation had been available to install.
           None of this will break an application, but it may cause available
           accelerators to go unused.  To prevent this, you might instantiate
           a separate instance of `kdu_block_encoder' for each subband,
           invoking this function with the nominal code-block dimensions and
           `K_max_prime' value advertised for that subband, as retrieved by
           `kdu_subband::get_dims' and `kdu_subband::get_K_max_prime'.
      */
    void encode(kdu_block *block, bool reversible=false,
                double msb_wmse=0.0F, kdu_uint16 min_slope_threshold=0,
                kdu_uint16 max_slope_threshold=0xFFFF,
                bool use_existing_slopes=false)
      { encode_func(block->modes)(this,block,reversible,msb_wmse,
                                  min_slope_threshold,max_slope_threshold,
                                  use_existing_slopes); }
      /* [SYNOPSIS]
           Encodes a single block of samples.
           [//]
           On entry, `block->num_passes' indicates the number of coding
           passes which are to be processed and `block->missing_msbs' the
           number of most significant bit-planes which are known to be zero.
           The function should process all coding passes, constrained only
           by the available implementation precision, except under the
           following conditions:
           [>>] If `block->ht_skip_planes' is non-zero, this number of
                additional bit-planes (beyond `block->missing_msbs') should
                be skipped before the first Cleanup pass that is generated,
                but this can only happen for HT code-blocks.
           [>>] If `block->flush_stats' provides non-zero members that help
                identify a limited set of coding passes that need to be
                generated, the block encoder may behave accordingly - usually
                only for HT code-blocks.
           [>>] If the `min_slope_threshold' argument is non-negative, one or
                more finest coding passes may be dropped if they seem highly
                likely to be discarded based on the indicated slope threshold.
           [//]
           The samples must be in the `block->samples' buffer, organized
           in raster scan order.  The sample values themselves are expected
           to have a sign-magnitude representation, with the most significant
           magnitude bit-plane appearing in the second most significant bit
           and the sign (1 for -ve) appearing in the most significant bit
           position.  Samples have a 32-bit, tightly packed representation.
           [//]
           On exit, the `block->byte_buffer' and `block->pass_lengths'
           arrays should be filled out, although note that the `num_passes'
           value may have been reduced, if the function was able to determine
           that some passes should be discarded later on during rate
           allocation.
           [//]
           If `use_existing_slopes' is false and `msb_wmse' > 0.0, the function
           fills in the `block->pass_slopes' array with distortion-length
           slopes for each pass, after a convex hull analysis that leaves
           passes not on the convex hull (i.e., not valid truncation points)
           with slopes of 0.  If the HT block encoder is being used (i.e.,
           if `block->modes' includes the `Cmodes_HT' flag), a value is
           also written to `block->ht_skip_slope'.
           [//]
           If `use_existing_slopes' is false and `msb_wmse' < 0.0, the
           `block->pass_slopes' array is entirely ignored and the
           `min_slope_threshold' and `max_slope_threshold' arguments have
           no meaning.
           [//]
           If `use_existing_slopes' is true, the `msb_wmse' argument is
           ignored, but the `block->pass_slopes' array is considered to
           hold valid distortion-length slopes for the coding passes that
           have yet to be generated.  This is valuable when the HT
           block encoer is being used (i.e., if `block->modes' includes the
           `Cmodes_HT' flag), since then only those coding passes that
           contribute to the convex hull points (passes with non-zero slopes)
           need be generated.
           [//]
           The `min_slope_threshold' and `max_slope_threshold' arguments
           may be used, at the discretion of the internal implementation,
           to determine the set of coding passes that are actually generated.
           These arguments are meaningful only if `msb_wmse' > 0, or
           `use_existing_slopes' is true, since then distortion-length
           slopes are either estimated or already available, allowing the
           min and max slopes of interest to be used to determine the
           coding passes that are of interest.
         [ARG: reversible]
           Irrelevant unless distortion-length slopes are to be estimated
           (i.e., `msb_wmse' is non-zero).  Whether the subband sample indices
           represent reversibly transformed image data or irreversibly
           transformed and quantized image data has a subtle impact on the
           generation of rate-distortion information.
         [ARG: msb_wmse]
           If `use_existing_slopes' is false and this argument is greater
           than 0, the block processor is expected to generate
           distortion-length slope information and perform a convex
           hull analysis, writing the results to the `block->pass_slopes'
           array.  Otherwise, the `block->pass_slopes' array's contents will
           not be touched.
           [//]
           If the HT block encoder is being used (i.e., if `block->modes'
           includes the `Cmodes_HT' flag), the same conditions
           (`msb_wmse' > 0 and `use_existing_slopes' false) are required
           for the `block->ht_skip_slope' member to be written.
         [ARG: min_slope_threshold]
           A non-zero value indicates an expected lower bound on the
           distortion-length slope threshold which is likely to be selected by
           the PCRD-opt rate control algorithm (the logarithmic representation
           is identical to that associated with the `block->pass_slopes'
           array).  This enables some coding passes which are highly unlikely
           to be included in the final compressed representation to be skipped,
           thereby saving processing time.  The capability is available only
           if `msb_wmse' > 0 or `use_existing_slopes' is true, meaning that
           distortion-length slope values are either to be estimated or
           already available.
         [ARG: max_slope_threshold]
           This argument is primarily of interest to the HT block coder
           (`Cmodes_HT'), which has the option to start encoding from a finer
           bit-plane than the first significant one, thereby saving
           computational effort.  One basis for doing this can be a non-trivial
           `max_slope_threshold'.  A value of 0xFFFF conveys no useful
           information, but smaller values may be helpful in allowing the
           coder to avoid generating coarse coding passes that will not be
           of interest during the post compression rate control and codestream
           flushing processes.
           [//]
           A value of 0 for this argument is particularly significant, since
           it informs the encoder that nothing will be discarded and only
           the highest available quality is of any interest.  In this case,
           the HT block encoder can perform just one CLEANUP pass, anchored
           at the finest available bit-plane.
           [//]
           Like `min_slope_threshold', this argument is only meaningful
           if `msb_wmse' > 0 or `use_existing_slopes' is true, meaning that
           distortion-length slope values are either to be estimated or
           already available.
      */
  void cellular_encode(kdu_block *block, bool reversible,
                       double msb_wmse, float cell_weights[],
                       int first_cell_cols, int first_cell_rows,
                       kdu_uint16 min_slope_threshold=0,
                       kdu_uint16 max_slope_threshold=0xFFFF,
                       bool use_existing_slopes=false)
    { cellular_encode_func(block->modes)(this,block,reversible,msb_wmse,
                                         cell_weights,first_cell_cols,
                                         first_cell_rows,min_slope_threshold,
                                         max_slope_threshold,
                                         use_existing_slopes); }
      /* [SYNOPSIS]
           Same as `encode', except that this function assesses the distortion
           impact of each coding pass by applying a set of additional
           distortion weights, as found in the `cell_weights' array.  The
           code-block is partitioned into cells whose nominal size is 4x4,
           except that the cells at the left, right, top and/or bottom edges
           may be smaller.  The `first_cell_cols' and `first_cell_rows'
           arguments give the widths of the left-most cells on each line and
           the heights of the cells at the top of the code-block, respectively,
           from which the dimensions of all other cells are readily computed.
           The total number of cells across and down the code-block can be
           determined by dividing its dimensions by 4 and rounding upwards;
           also, at most one of the left or right edges may have reduced width
           cells and at most one of the top or bottom edges may have reduced
           height cells.
           [//]
           Note that there might not be a separate `cellular_encode' capability
           for some block coding modes (e.g., `Cmodes_HT'), in which case the
           fallback is to effectively invoke `encode'.
      */
    bool encode16(kdu_block *block, kdu_uint16 *data, bool reversible=false,
                  double msb_wmse=0.0F, kdu_uint16 min_slope_threshold=0,
                  kdu_uint16 max_slope_threshold=0xFFFF)
      { 
        kd_block_encode16_func func = encode16_func(block->modes);
        return ((func != NULL) &&
                func(this,block,data,reversible,msb_wmse,
                     min_slope_threshold,max_slope_threshold));
      }
      /* [SYNOPSIS]
           This function provides an alternative to the `encode' function,
           that does not need to be completely generic.  The function returns
           false if the internal implementation does not exist, or does not
           support the code-block dimensions or other attributes identified
           within the `block' structure, in which case the caller should
           invoke the regular `encode' function instead, after transferring
           the source samples to the `block->sample_buffer'.
           [//]
           This function receives the block's subband samples directly
           through the `data' array, rather than via `block->sample_buffer',
           as 16-bit unsigned integers that are effectively the 16 MSBs of
           the values that would be passed to `encode' as 32-bit samples.
           The `data' buffer is tightly packed, but it is likely that the
           implementation, if any, will succeed (returning true) only if
           the code-block has convenient power-of-2 dimensions.  In any
           case, the important message is that the caller needs to be
           prepared to go through the regular path of using `encode'
           if this call is rejected.
           [//]
           Unlike `encode', the `block->missing_msbs' and `block->num_passes'
           members do not have valid values when this function is invoked.  If
           the function does return true, it must work out the number of mising
           MSBs itself from the supplied `data', setting `block->missing_msbs'
           to the relevant value, and based on this it should also set the
           `block->num_passes' member to the number of coding passes that
           are generated.
           [//]
           Unlike `encode', this function has no `use_existing_slopes'
           argument, which is used for transcoding.
           [//]
           The implementation can feel free to overwrite the `block' buffer,
           noting that only `block->size.y'*`block->size.x' samples can
           safely be accessed.
      */
    bool encode32(kdu_block *block, kdu_uint32 *data, bool reversible=false,
                  double msb_wmse=0.0F, kdu_uint16 min_slope_threshold=0,
                  kdu_uint16 max_slope_threshold=0xFFFF)
      { 
        kd_block_encode32_func func = encode32_func(block->modes);
        return ((func != NULL) &&
                func(this,block,data,reversible,msb_wmse,
                     min_slope_threshold,max_slope_threshold));
      }
      /* [SYNOPSIS]
           Same as `encode16' but for 32-bit samples.  This function may
           be provided by implementations that need to organize the sample
           values differently to the tightly packed raster organization
           supplied by the main `encode' function, so it is preferable to
           avoid the caller first transcribing the samples to the
           `block->samples' buffer only to transcribe them again internally.
      */
  };

/*****************************************************************************/
/*                          kdu_block_decoder_base                           */
/*****************************************************************************/

typedef void
  (*kd_block_decode_func)(kdu_block_decoder_base *, kdu_block *);

class kdu_block_decoder_base { 
  /* [BIND: reference]
     [SYNOPSIS]
       This base class for `kdu_block_decoder' serves to manage function
       pointers for block decoding, which can be replaced.  The class offers
       a public mechanism to dynamically override a decoding function while
       preserving the original one as a fallback, which can be useful when
       an optimized implementation cannot handle certain corner cases.
  */
  protected:
    kdu_block_decoder_base() // Cannot instantiate the base class directly
      { 
        for (int f=0; f < 2; f++)
          { decode_funcs[f] = ht_decode_funcs[f] = NULL; }
      }
  public: // Installing decoding functions.  It is expected that the
          // `kdu_block_decoder' constructor invokes both `add_...'
          // functions below at least once.  Note that each `add...' call
          // adds a new "preferred" function, leaving the previously added
          // one to become the fallback function.
    void add_decode_func(kd_block_decode_func func)
      { decode_funcs[1] = decode_funcs[0];
        decode_funcs[0] = func;  }
    void add_ht_decode_func(kd_block_decode_func func)
      { ht_decode_funcs[1] = ht_decode_funcs[0]; ht_decode_funcs[0] = func;  }
  public: // Retrieving decoding functions.  The `fallback...' functions might
          // return NULL function pointers, but the others should not.
    kd_block_decode_func decode_func(int modes)
      { return (modes&Cmodes_HT)?ht_decode_funcs[0]:decode_funcs[0]; }
    kd_block_decode_func fallback_decode_func(int modes)
      { return (modes&Cmodes_HT)?ht_decode_funcs[1]:decode_funcs[1]; }
  protected: // Function pointers -- may be expanded later to include functions
             // that both decode and dequantize in one go.
    kd_block_decode_func decode_funcs[2];
    kd_block_decode_func ht_decode_funcs[2];
  };

/*****************************************************************************/
/*                            kdu_block_decoder                              */
/*****************************************************************************/

class kdu_block_decoder : public kdu_block_decoder_base { 
  /* [BIND: reference]
     [SYNOPSIS]
       Objects of this class need be created only for transcoding or
       other low level operations.  Normally, the relevant block decoder
       objects are created by the `kdu_decoder' object which also handles
       quantization and ranging operations.
  */
  public: // Member functions
    KDU_EXPORT kdu_block_decoder();
    KDU_EXPORT void
      speedpack_config(kdu_coords nominal_block_size, int K_max_prime);
      /* [SYNOPSIS]
           This function does nothing unless the current version of the
           software includes the Kakadu speed-pack tools, that typically
           accelerate compression and decompression tasks by a factor of
           around 1.5x, under certain conditions.  The relevant conditions
           are evaluated internally; they may depend upon the supplied
           nominal (or just maximum) code-block dimensions, the maximum
           value of the `K_max_prime' parameter that will occur in the
           `kdu_block' argument passed to the `decode' function.  In practice,
           at the present time, the speed-pack tools are restricted to
           code-blocks whose width and height are at most 64, on 64-bit x86
           operating systems.
           [//]
           If the conditions are met, this function installs accerated
           block coding implementations by invoking functions such as
           `add_decode_func'.  Ultra-accelerated implementations of the HT
           block decoding algorithm might also be installed.
           [//]
           It is important to pass appropriate values for the
           `nominal_block_size' and `K_max_prime' arguments; otherwise, the
           installed accelerators might turn out to be unsuitable, which
           will prevent any acceleration occurring.  In particular, calls
           to `dcode' may internally fall back to the default implementation,
           even if a suitable accelerated implementation had been available
           to install.  None of this will break an application, but it may
           cause available accelerators to go unused.  To prevent this, you
           might instantiate a separate instance of `kdu_block_decoder' for
           each subband, invoking this function with the nominal code-block
           dimensions and `K_max_prime' value advertised for that subband, as
           retrieved by `kdu_subband::get_dims' and
           `kdu_subband::get_K_max_prime'.
      */
    void decode(kdu_block *block)
      { decode_func(block->modes)(this,block); }
      /* [SYNOPSIS]
           Decodes a single block of samples.  The dimensions of the block
           are in `block->size' -- none of the geometric transformation
           flags in `block->size' have any meaning here.
           [//]
           On entry, `block->num_passes' indicates the number of coding
           passes which are to be decoded and the `block->missing_msbs' and
           `block->ht_skip_msbs' members together identify the number of
           most significant bit-planes which are to be skipped -- this number
           of skipped bit-planes is the sum of `block->missing_msbs' and
           `block->ht_skip_msbs', but the `block->ht_skip_msbs' member will
           be 0 unless the code-block uses the HT block coding algorithm; for
           HT code-blocks, the `block->missing_msbs' value is usually zero,
           since an HT decoder cannot know a priori how many most significant
           magnitude bit-planes are entirely zero.
           [//]
           The function processes all coding passes, unless unable to do so
           for reasons of available implementation precision limitations, or
           an error is detected by error resilience mechanisms and corrected
           by discarding passes believed to be erroneous.  If all coding
           passes are not decoded, the decoder should modify the value of
           `block->num_passes' before returning.
           [//]
           On exit, the decoded samples are in the `block->sample_buffer'
           array, organized in scan-line order.  The sample values themselves
           have a sign-magnitude representation, with the most significant
           magnitude bit-plane appearing in bit position 30 and the sign
           (1 for -ve) in bit position 31.  At each sample location, if p is
           the index of the least significant decoded magnitude bit and the
           sample is non-zero, the function sets bit p-1.  This has the effect
           of both signalling the location of the least significant decoded
           magnitude bit-plane and also implementing the default mid-point
           rounding rule for dequantization.
           [//]
           The value of `block->K_max_prime' identifies the maximum number of
           magnitude bit-planes (including missing MSB's) which could have
           been coded, if both `block->missing_msbs' and `block->ht_skip_msbs'
           were 0.  Knowledge of this value allows the function to
           determine whether or not all coding passes have been decoded,
           without truncation -- this in turn, affects the behaviour of
           the error resilience mechanisms.
        */
  };

} // namespace kdu_core

#endif // KDU_BLOCK_CODING_H
