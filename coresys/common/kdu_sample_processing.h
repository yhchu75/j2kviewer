/*****************************************************************************/
// File: kdu_sample_processing.h [scope = CORESYS/COMMON]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Uniform interface to sample data processing services: DWT analysis; DWT
synthesis; subband sample encoding and decoding; colour and multi-component
transformation.  Here, we consider the encoder and decoder objects to be
sample data processing objects, since they accept or produce unquantized
subband samples.  They build on top of the block coding services defined in
"kdu_block_coding.h", adding quantization, ROI adjustments, appearance
transformations, and buffering to interface with the DWT.
******************************************************************************/

#ifndef KDU_SAMPLE_PROCESSING_H
#define KDU_SAMPLE_PROCESSING_H

#include <assert.h>
#include "kdu_messaging.h"
#include "kdu_compressed.h"
#include "kdu_arch.h"

// Defined here:
namespace kdu_core { 
  union kdu_sample32;
  union kdu_sample16;
  class kdu_sample_allocator;
  class kdu_line_buf;
  class kdu_push_ifc_base;
  class kdu_push_ifc;
  class kdu_pull_ifc_base;
  class kdu_pull_ifc;
  class kdu_analysis;
  class kdu_analysis_fusion;
  class kdu_synthesis;
  class kdu_encoder;
  class kdu_decoder;
  class kdu_cplex_analysis;
  class kdu_cplex_ifc;
  class kdu_cplex_shared_ifc;
  class kdu_cplex_share_cfrag;
  class kdu_cplex_share_tile;
  class kdu_cplex_share;
  class kdu_cplex_bkgnd_level;
  class kdu_cplex_bkgnd_comp;
  class kdu_cplex_bkgnd;
  class kd_multi_analysis_base;
  class kd_multi_synthesis_base;
  class kdu_multi_analysis;
  class kdu_multi_synthesis;
}

// Defined elsewhere:
namespace kdu_core { 
  class kdu_roi_node;
  class kdu_roi_image;
}
namespace kd_core_local { 
  struct kd_tile;
}

// NB: The macros that determine buffer alignment have been moved to
// "kdu_arch.h" which facilitates portable inclusion of those constants
// into architecture-specific optimizations where it may not be appropriate
// to import in-line function definitions that are found in the other
// headers on which we depend here.

namespace kdu_core { 

/* ========================================================================= */
/*                     Class and Structure Definitions                       */
/* ========================================================================= */

/*****************************************************************************/
/*                              kdu_sample32                                 */
/*****************************************************************************/

union kdu_sample32 { 
    float fval;
      /* [SYNOPSIS]
           Normalized floating point representation of an image or subband
           sample subject to irreversible compression.  See the description
           of `kdu_line_buf' for more on this.
      */
    kdu_int32 ival;
      /* [SYNOPSIS]
           Absolute 32-bit integer representation of an image or subband
           sample subject to reversible compression.  See the description
           of `kdu_line_buf' for more on this.
      */
  };

/*****************************************************************************/
/*                              kdu_sample16                                 */
/*****************************************************************************/

#define KDU_FIX_POINT ((int) 13) // Num frac bits in a 16-bit fixed-point value

union kdu_sample16 { 
  kdu_int16 ival;
    /* [SYNOPSIS]
         Normalized (fixed point) or absolute (integer) 16-bit representation
         of an image or subband sample subject to irreversible (normalized)
         or reversible (integer) compression.  See the description of
         `kdu_line_buf' for more on this.
    */
  };

/*****************************************************************************/
/*                          kdu_sample_allocator                             */
/*****************************************************************************/

#define KDU_SAMPLE_ALLOCATOR_MIN_FRAG_BITS ((int) 18) /* 256 kB */
#define KDU_SAMPLE_ALLOCATOR_MAX_FRAG_BITS ((int) 28) /* 256 MB */
#define KDU_SAMPLE_ALLOCATOR_DEF_FRAG_BITS ((int) 26) /* 64 MB */

class kdu_sample_allocator { 
  /* [BIND: reference]
     [SYNOPSIS]
     This object serves to prevent excessive memory fragmentation and
     provide useful memory alignment services.  Beyond that, from Kakadu
     version 7.10, this object forms the basis for robust management of
     all memory allocation associated with sample data processing, via its
     interaction with the customizable integrated memory regulation framework
     represented by `kdu_membroker'.  As explained in the introductory
     documentation for `kdu_membroker', brokered memory management is able
     to take control of the vast majority of memory allocation tasks in
     Kakadu, including memory associated with codestream state, compressed
     data, temporary caches, file-level metadata, and also the memory
     required for actual data processing.
     [//]
     Although the principle intent is to allocate storage for image
     samples (including transformed image samples), the object is also
     used to allocate more general blocks of memory, taking advantage
     of the alignment support which it offers.  In practice, it is used to
     allocate all buffers for subband and image samples throughout the
     Kakadu core system and beyond, and it is also used to allocate data
     processing related structures, such as multi-threaded processing jobs
     with their associated state information.  Beyond this, the
     `kdu_sample_allocator' manages the permits required to allocate
     data processing objects, for DWT analysis/synthesis, for block
     coding/decoding, for multi-component transforms and even for high level
     API's defined outside the core system.  Such memory allocation permits
     are granted along with permission to allocate sample data memory, from
     the permits that are granted by any `kdu_membroker' to which the
     allocator is attached.
     [//]
     Pre-allocation requests are made by all clients which wish to use
     `kdu_sample_allocator' services, after which the memory is allocated
     and assigned in accordance with the original pre-allocation requests.
     Memory chunks allocated by this object to a client are not individually
     returned or recycled, since this incurs overhead and fragmentation.
     Instead, the sample allocator maintains its own information about the
     actual allocated memory blocks, which can be recycled and/or released
     once no longer needed.  The idea is that a single `kdu_sample_allocator'
     manages all memory associated with a data processing configuration that
     will be torn down and destroyed as a whole once the processing is
     complete.
     [//]
     The object may be re-used (after a call to its `restart' member) once
     all memory served out is no longer in use.  When re-used, the object
     makes every attempt to avoid destroying and re-allocating its memory
     block(s).  This avoids memory fragmentation and allocation overhead when
     processing images with multiple tiles or when used for video
     applications.
     [//]
     The object allocates memory in such a way that allocated buffers may
     be over-read by at least `KDU_OVERREAD_BYTES' bytes -- the value of
     this constant is typically 128, but a larger value might be required
     for SIMD architectures with extremely wide data paths.  Applications
     can legally read up to `KDU_OVERREAD_BYTES' bytes beyond the storage
     that has nominally been allocated and also up to `KDU_OVERREAD_BYTES'
     bytes before the storage that has nominally been allocated.  However,
     they may not generally write beyond the allocated storage without
     interfering with other buffers allocated by the same sample allocator.
   */
  public: // Member functions
    kdu_sample_allocator()
      { 
        pre_creation_phase = true;  prealloc_overflow = false;
        existing_bytes = permit_bytes = 0;
        frag_bits = KDU_SAMPLE_ALLOCATOR_DEF_FRAG_BITS;
        frag_size = ((size_t)1)<<frag_bits; existing_frags=0;
        max_frags=1; pre_frag = fragments = &local_frag;
        limit_bytes = remaining_bytes = KDU_INT64_MAX;
        broker = NULL; local_frag.init(0);
        num_failed_release = 0;
      }
      /* [SYNOPSIS] Creates an empty object. */
    KDU_EXPORT ~kdu_sample_allocator();
      /* [SYNOPSIS]
           Destroys all storage served from this object in one go.
      */
    KDU_EXPORT bool
      configure(kdu_membroker *membroker,
                int frag_bits=KDU_SAMPLE_ALLOCATOR_DEF_FRAG_BITS);
      /* [SYNOPSIS]
           This is an optional call which is normally issued prior to the
           first all to `pre_alloc', `pre_alloc_block', `finalize' or
           `seek_permit'.  The function can be called at other times,
           subject to the following considerations:
           [>>] If `frag_bits' differs from the value that is currently
                installed, no sample data memory may be in use, or
                pre-allocated.  This means that if `finalize' has been called,
                `restart' must have been subsequently called; moreover, no
                call to `pre_alloc' or `pre_alloc_block' should have occurred
                since the last call to `restart' (or since construction).
           [>>] If a non-NULL `membroker' argument differs from any previously
                specified memory broker, no sample data memory may be in use,
                meaning again that if `finalize' has been called, `restart'
                must have been subsequently called.  Additionally, when a
                non-NULL `membroker' differs from any previously specified
                broker, all allocation permits obtained via `seek_permit' must
                have been released via `release_permit'.
           [//]
           If the above conditions are not met, an error will be generated
           through `kdu_error'.
         [RETURNS]
           False only if `membroker' is non-NULL and the sample allocator's
           memory allocation cannot be brought under the control of
           `membroker' due to the existence of outstanding allocation permits
           granted via calls to `seek_permit' that the `membroker' is
           unwilling to sanction.  In all other cases, the function returns
           true.  Even if the function does return false, any required
           changes associated with the `frag_bits' argument are applied first.
         [ARG: membroker]
           Supplying a non-NULL `membroker' argument puts the allocator into
           its brokered mode of operation, where all memory allocations and
           permitted memory allocation are subject to constraints imposed by
           the `membroker'.  The constraints are applied during calls to
           `seek_permit' and `finalize', but not during pre-allocation or
           post-finalize allocation calls (`pre_alloc', `pre_alloc_block',
           `alloc16', `alloc32' and `alloc_block').  If a constraint cannot
           be satisfied immediately, `membroker->request' is invoked to augment
           the allocation; if this fails, `membroker->note_allocation_failure'
           is invoked, giving the memory broker an opportunity to notify the
           application in its own way (including throwing an exception) and
           then an informative error is generated through `kdu_error'.
           [//]
           At the time when `configure' is called, it is possible that some
           memory permits have already been granted via calls to `seek_permit'
           that are not matched by calls to `release_permit'.  Where this
           happens, and `membroker' is non-NULL, retrospective permission is
           sought from `membroker' for these existing permits, by calling
           `membroker->request'.  If the call fails to return sufficient
           allocation permission to cover the permits that have already been
           granted by this object, all memory granted by `membroker' is
           returned and the function proceeds as if `membroker' had been NULL,
           except that the function returns false.
           [//]
           You can pass NULL for `membroker' to remove the object from
           control of an existing broker without installing a new one.
         [ARG: frag_bits]
           Prior to Kakadu version 7.10, `kdu_sample_allocator' allocated all
           of its memory in a single contiguous block.  In some cases, however,
           this can result in an allocation request that is too large for the
           system to accommmodate.  It might also result in poor utilization of
           already active memory pages that have been deallocated elsewhere.
           To avoid the problem, memory can now be allocated in fragments,
           whose size is nominally F = 2^`frag_bits' bytes.  In practice,
           pre-allocated memory is assigned to a current fragment until the
           fragment size reaches or exceeds F, at which point a new fragment
           is started.
           [//]
           This means that allocated blocks of memory may exceed
           F by as much as the size of the largest individual pre-allocation
           request.  It follows that `frag_bits' does not impose any
           constraint on the size of an individual pre-allocation (and hence
           allocation) request, but larger values for `frag_bits'
           encourage consolidation, while smaller values facilitate better
           packing within pages already granted by the operating system.
           [//]
           `frag_bits' values are internally restricted to the range
           `KDU_SAMPLE_ALLOCATOR_MIN_FRAG_BITS' to
           `KDU_SAMPLE_ALLOCATOR_MAX_FRAG_BITS'; moreover if this function
           is never called, the value `KDU_SAMPLE_ALLOCATOR_DEF_FRAG_BITS'
           is used.
      */
    kdu_membroker *get_membroker() { return this->broker; }
      /* [SYNOPSIS]
           Convenience method for extracting any memory broker that was
           supplied to `configure'.  This can be used to pass memory brokers
           indirectly through data procesing machinery via the sample allocator
           that is generally needed everywhere.  The function is not normally
           used at the application level.
      */
    KDU_EXPORT void restart();
      /* [SYNOPSIS]
           Invalidates all memory resources served up using `alloc16',
           `alloc32' or `alloc_block' and starts the pre-allocation phase from
           scratch.  Note, however, that this function does not affect
           memory allocation permits sought by calls to `seek_permit' -- these
           are associated with permitted allocation of external objects.
           [//]
           After a call to `restart', internal resources are sized from
           scratch during subsequent calls to `pre_alloc' or `pre_alloc_block',
           and memory is re-allocated, if necessary, during the next call to
           `finalize', based on these pre-allocation requests alone.
      */
    KDU_EXPORT void release();
      /* [SYNOPSIS]
           This function is not an alternative to `restart' -- in fact,
           `restart' should be called first.  This function deallocates all
           allocated sample data memory blocks, deletes any internal array
           of fragments and restores the object to the state in which it was
           constructed, except that the information last supplied to
           `configure' is retained and allocation permission that we have
           granted via `seek_permit' is also retained.  If a memory broker
           has been passed to `configure', all memory allocation permits
           are released back to the broker apart from any memory that we
           have grnted via `seek_permit'.
           [//]
           It is an error to call this function if `finalize' has been
           invoked and has not been followed by `restart', since this means
           that sample data memory is in use and cannot be released.
      */
    void pre_align(size_t alignment_multiple)
      { 
        assert(pre_creation_phase);
        if (alignment_multiple > pre_frag->bytes_aligned)
          { 
            while (pre_frag->alignment_reserved < alignment_multiple)
              { pre_frag->extra_bytes_reserved += pre_frag->alignment_reserved;
                pre_frag->alignment_reserved += pre_frag->alignment_reserved; }
            pre_frag->bytes_reserved += alignment_multiple;
            if (pre_frag->bytes_reserved < alignment_multiple)
              prealloc_overflow = true;
            pre_frag->bytes_reserved &= ~(alignment_multiple-1);
            pre_frag->bytes_aligned = alignment_multiple;
          }
      }
      /* [SYNOPSIS]
           This function allows you to enforce alignment constraints for
           the block of memory associated with the next call to
           `pre_alloc' or `pre_alloc_block'.  These functions have their
           own default alignment constraints which may be looser than you
           would like.  The `alignment_multiple' supplied here must be an
           exact power of 2 -- this is your responsibility and it might
           not be validated.  Values for the `alignment_multiple' that
           might be of interest are 32 (for AVX operands), 64 (typical
           L1/L2 cache line boundary).
      */
    size_t pre_alloc(bool use_shorts, int before, int after,
                     int num_requests=1)
      { 
        pre_align(KDU_PREALIGN_BYTES);
        size_t offset = pre_frag->bytes_reserved;
        if (offset >= frag_size)
          { advance_pre_frag(); offset = 0; } // Correctly aligns next fragment
        int num_bytes;
        if (use_shorts)
          { 
            num_bytes =
              (((KDU_ALIGN_SAMPLES16-1+before) & ~(KDU_ALIGN_SAMPLES16-1)) +
               ((KDU_ALIGN_SAMPLES16-1+after) & ~(KDU_ALIGN_SAMPLES16-1)));
            num_bytes <<= 1;
            pre_frag->bytes_aligned = 2;
          }
        else
          { 
            num_bytes =
              (((KDU_ALIGN_SAMPLES32-1+before) & ~(KDU_ALIGN_SAMPLES32-1)) +
               ((KDU_ALIGN_SAMPLES32-1+after) & ~(KDU_ALIGN_SAMPLES32-1)));
            num_bytes <<= 2;
            pre_frag->bytes_aligned = 4;
          }
        if (((before | after | num_requests | num_bytes) & 0x80000000) ||
            ((num_requests > 1) && (num_bytes > (INT_MAX / num_requests))))
          prealloc_overflow = true;
        num_bytes *= num_requests;
        pre_frag->bytes_reserved += (size_t)num_bytes;
        if (pre_frag->bytes_reserved < offset)
          prealloc_overflow = true;
        return offset + pre_frag->offset;
      }
      /* [SYNOPSIS]
           Reserves enough storage for `num_requests' later calls to `alloc16'
           (if `use_shorts' is true) or `alloc32' (if `use_shorts' is false).
           Space is reserved such that each of these `num_requests' allocations
           can return an appropriately aligned pointer to an array which offers
           entries at locations n in the range -`before' <= n < `after', where
           each entry is of type `kdu_sample16' (if `use_shorts'=true) or
           `kdu_sample32' (if `use_shorts'=false).
           [//]
           Reservation of storage proceeds with the following alignment
           considerations in mind:
           [>>] Entry 0 must be aligned on a multiple of `KDU_ALIGN_SAMPLES16'
               16-bit samples (if `use_shorts'=true) or `KDU_ALIGN_SAMPLES32'
               32-bit samples (if `use_shorts'=false).  For example, octet
               alignment is achieved if both these alignment constants are
               equal to 8 (they will never be smaller than 8), and this is
               sufficient for aligned vector processing on SIMD architectures
               up to and including AVX.  For SIMD processing with AVX2
               instructions, the `KDU_ALIGN_SAMPLES16' macro must be at least
               16, while for vector processing using the Intel MIC instruction
               set, both `KDU_ALIGN_SAMPLES16' and `KDU_ALIGN_SAMPLES32' must
               be at least 16.
           [>>] The space reserved `before' and `after' entry 0 is rounded up
                to an integer multiple of `KDU_ALIGN_SAMPLES16' 16-bit
                samples or `KDU_ALIGN_SAMPLES32' 32-bit samples, as
                appropriate.
           [>>] The entire allocated block of memory also satisfies any
                alignment constraints associated with an immediately
                preceding call to `pre_align', and there is an implicit call
                to pre-align that guarantees alignment on at least a
                `KDU_PREALIGN_BYTES'-byte boundary.
           [//]
           Remember that you can legally read from (but not write to)
           locations that lie up to `KDU_OVERREAD_BYTES' bytes before or
           after the nominal allocated memory block.
         [RETURNS]
           The returned value is an allocation offset that must be remembered
           by the caller and passed to the `alloc16' or `alloc32' function as
           appropriate.  This allows calls to `alloc16', `alloc32' and
           `alloc_block' to safely proceed in a multi-threaded environment
           without the need for thread synchronization.
      */
    size_t pre_alloc_block(size_t num_bytes)
      { 
        pre_align(KDU_PREALIGN_BYTES);
        size_t offset = pre_frag->bytes_reserved;
        if (offset > frag_size)
          { advance_pre_frag(); offset = 0; } // Correctly aligns next fragment
        pre_frag->bytes_reserved += num_bytes;
        if (pre_frag->bytes_reserved < offset)
          prealloc_overflow = true;
        pre_frag->bytes_aligned = 1;
        return offset + pre_frag->offset;
      }
      /* [SYNOPSIS]
           This function is provided to allow for allocation of general
           storage, a little bit like malloc.  The pre-allocated block of
           memory is aligned at least on a `KDU_PREALIGN_BYTES'-byte boundary,
           which should be sufficient at least to offer `KDU_ALIGN_SAMPLES16'
           sample alignment for 16-bit samples and `KDU_ALIGN_SAMPLES32'
           sample alignment for 32-bit samples.  You can also enforce
           stricter alignment by calling `pre_align' prior to this function.
           [//]
           Remember that you can legally read from (but not write to)
           locations that lie up to `KDU_OVERREAD_BYTES' bytes before or
           after the nominal allocated memory block.
         [RETURNS]
           The returned value is an allocation offset that must be
           remembered by the caller and passed to the `alloc_block' function
           as appropriate.  This allows calls to `alloc16', `alloc32' and
           `alloc_block' to safely proceed in a multi-threaded environment
           without the need for thread synchronization.
      */
    void finalize(kdu_codestream codestream)
      { 
        kdu_core_sample_alignment_checker(KDU_OVERREAD_BYTES,
                                          KDU_PREALIGN_BYTES,
                                          KDU_ALIGN_SAMPLES16,
                                          KDU_ALIGN_SAMPLES32,
                                          false,true);
        do_finalize(codestream); // Implementation found in "codestream.cpp" to
                       // internalize memory allocation within DLLs/shared-libs
      }
      /* [SYNOPSIS]
           Call this function after all pre-allocation (calls to
           `pre_alloc' and/or `pre_alloc_block') has been completed.  The
           function performs the actual allocation of heap memory, if
           necessary.  Once this fucntion returns, you may use `alloc16',
           `alloc32' and `alloc_block', as appropriate, to retrieve the
           actual addresses of pre-allocated memory.
           [//]
           Note that calls to `seek_permit' and `release_permit' may be
           performed at any time, irrespective of whether `finalize' has been
           called or not.
         [ARG: codestream]
           This argument was introduced in KDU-7.3.1 in order to allow memory
           allocation errors to be reported via `kdu_codestream::mem_failure'.
           From KDU-7.10, however, the argument is no longer important -- it
           can be an empty interface.  Memory allocation failures are now
           handled in a more comprehensive way, taking into account any
           `kdu_membroker' reference that may have been supplied in a call to
           `configure'.
      */
    kdu_sample16 *alloc16(int before, int after, size_t alloc_off,
                          int inst=0) const
      { 
        assert(!pre_creation_phase);
        size_t frag_off = alloc_off >> frag_bits;  alloc_off &= (frag_size-1);
        assert(!(alloc_off & (KDU_PREALIGN_BYTES-1)));
        assert(frag_off < (size_t)max_frags);
        kd_allocator_frag *frag = fragments + frag_off;
        before = ((before+KDU_ALIGN_SAMPLES16-1) & ~(KDU_ALIGN_SAMPLES16-1));
        size_t num_samples = (size_t)
          (before + ((after+KDU_ALIGN_SAMPLES16-1) & (KDU_ALIGN_SAMPLES16-1)));
        assert((alloc_off + ((num_samples*(size_t)(inst+1))<<1)) <=
               frag->bytes_reserved);
        kdu_sample16 *result = (kdu_sample16 *)(frag->buffer+alloc_off);
        return result + num_samples*(size_t)inst + (size_t)before;
      }
      /* [SYNOPSIS]
           This function completes the allocation of an array that was
           pre-allocated using the `pre_alloc' function.  If the
           `num_requests' value passed to `pre_alloc' was greater than 1,
           the `inst' argument should be used to identify which instance
           (in the range 0 to `num_requests'-1) is being allocated here.
           The `alloc_off' argument must be identical to the value returned
           by the call to `pre_alloc' and the `before' and `after' arguments
           must be identical to those passed to `pre_alloc'.
           [//]
           Before calling this function, you must have invoked `finalize'
           (after performing all the relevant `pre_alloc' calls).
           [//]
           Assuming you have correctly passed in the `alloc_off' value
           received from `pre_alloc', the returned pointer is guaranteed to
           have the required alignment properties.
           [//]
           This function can safely be invoked from multiple threads without
           taking out any mutual exclusion locks because it does not actually
           modify the state of the internal object.  This is possible because
           the allocation offset was generated during the call to `pre_alloc'
           and is passed in here.  This feature/requirement is new to
           Kakadu version 7.0
      */
    kdu_sample32 *alloc32(int before, int after, size_t alloc_off,
                          int inst=0) const
      { 
        assert(!pre_creation_phase);
        size_t frag_off = alloc_off >> frag_bits;  alloc_off &= (frag_size-1);
        assert(!(alloc_off & (KDU_PREALIGN_BYTES-1)));
        assert(frag_off < (size_t)max_frags);
        kd_allocator_frag *frag = fragments + frag_off;
        before = ((before+KDU_ALIGN_SAMPLES32-1) & ~(KDU_ALIGN_SAMPLES32-1));
        size_t num_samples = (size_t)
          (before + ((after+KDU_ALIGN_SAMPLES32-1) & (KDU_ALIGN_SAMPLES32-1)));
        assert((alloc_off + ((num_samples*(size_t)(inst+1))<<2)) <=
               frag->bytes_reserved);
        kdu_sample32 *result = (kdu_sample32 *)(frag->buffer+alloc_off);
        return result + num_samples*(size_t)inst + (size_t)before;
      }
      /* [SYNOPSIS]
           Same as `alloc16', except that it allocates storage for arrays of
           32-bit quantities.  The `alloc_off' value supplied here must have
           been obtained from a call to `pre_alloc' in which the `use_shorts'
           argument was false.
      */
    void *alloc_block(size_t alloc_off, size_t num_bytes) const
      { 
        assert(!pre_creation_phase);
        size_t frag_off = alloc_off >> frag_bits;  alloc_off &= (frag_size-1);
        assert(frag_off < (size_t)max_frags);
        kd_allocator_frag *frag = fragments + frag_off;
        assert((alloc_off+num_bytes) <= frag->bytes_reserved);
        return frag->buffer+alloc_off;
      }
      /* [SYNOPSIS]
           Similar to `alloc16' and `alloc32', except that it allocates
           storage for memory blocks that were pre-allocated using
           `pre_alloc_block'.  The `alloc_off' value must match that
           returned by the `pre_alloc_block' function.
      */
    size_t get_size() const
      { 
        return existing_bytes;
      }
      /* [SYNOPSIS]
           For memory consumption statistics.  Returns the total amount of
           heap memory allocated by this object during the last call to
           `finalize'.  The heap memory usually either grows or stays the same
           between successive calls to `finalize', but may possibly shrink if
           multiple memory fragments are involved.
           [//]
           Note that the returned value is unaffected by memory permits
           granted via `seek_permit'.
      */
    size_t safe_add(size_t x, size_t y)
      { 
        size_t result = x+y;
        if (result < y) handle_overflow();
        return result;
      }
      /* [SYNOPSIS]
           Returns `x' + `y', except if the computation causes overflow, in
           which case the function does not return, but instead generates a
           suitable error through `kdu_error'.  This function is provided to
           assist you in safely preparing memory allocation amounts for passing
           to `pre_alloc_block' or `seek_permit'.
      */
    size_t safe_mul(size_t x, size_t y)
      { 
        if (((x | y) > KDU_SAFE_ROOT_SIZE_MAX) && (y != 0) &&
            (x > (KDU_SIZE_MAX / y)))
          handle_overflow();
        return x*y;
      }
      /* [SYNOPSIS]
           Same as `safe_add', but returns `x' * `y', if successful.
      */
    size_t safe_mulo(size_t x, size_t y, size_t off)
      { 
        return safe_add(safe_mul(x,y),off);
      }
     /* [SYNOPSIS]
          Combines `safe_mul' with `safe_add', returning `x' * `y' + off',
          if successful.
     */
    bool seek_permit(size_t num_bytes, bool return_on_failure=false)
      { 
        kdu_uint64 nbytes = (kdu_uint64)num_bytes;
        remaining_bytes -= (kdu_int64)nbytes;
        if (((nbytes <= ((kdu_uint64)KDU_INT64_MAX)) &&
             (remaining_bytes >= 0)) ||
            handle_failed_permit(num_bytes,return_on_failure))
          { permit_bytes += num_bytes; return true; }
        else
          return false;
      }
      /* [SYNOPSIS]
           This function is used to seek permission to allocate a block of
           memory, but does not do any allocation itself.  If the call
           succeeds, the caller is expected to make the allocation and later
           call `release_permit' once the memory is deallocated.  The
           purpose of this is to bring allocation of data processing constructs
           under the control of a memory broker that may have been supplied
           to `configure'.  A side benefit is that memory leaks are readily
           detected.
         [ARG: return_on_failure]
           Normally the function is called with `return_on_failure'
           false (default) so that the caller can assume that any return is
           a successful one.  The only reason for returning on failure is
           in cases where the proposed allocation is not strictly necessary.
      */
    void release_permit(size_t num_bytes)
      { 
        remaining_bytes += (kdu_int64) num_bytes;
        if ((remaining_bytes > limit_bytes) || (permit_bytes < num_bytes))
          handle_failed_release();
        permit_bytes -= num_bytes;
      }
      /* [SYNOPSIS]
           Each successful call to `seek_permit' must ultimately be matched
           by a corresponding call to `release_permit'.
      */
  private: // Helper functions
    KDU_EXPORT void advance_pre_frag();
      /* Advances `pre_frag' to the next fragment in the `fragments' array,
         allocating a larger array if necessary, and configures the new
         `pre_frag' record to have the same alignment recorded in the original
         `pre_frag->bytes_aligned' member. */
    KDU_EXPORT void do_finalize(kdu_codestream codestream);
      /* Does all the work of finalize except for the call to
         `kdu_core_sample_alignment_checker', which must be in-line. */
    KDU_EXPORT void handle_overflow() const;
      /* Called by `safe_add', `safe_mul' or `safe_mulo', if the computation
         resulted in numerical overflow.  This function does not return. */
    KDU_EXPORT bool
      handle_failed_permit(kdu_uint64 nbytes, bool return_on_fail);
      /* Called by `seek_permit' if the current `remaining_bytes' is inadequate
         to meet the request.  On entry, `nbytes' has already been subtracted
         from `remaining_bytes'; the function adds it back if not successful.
         If `nbytes' is -ve, the failure cannot be corrected; otherwise, if
         there is a memory `broker', the function seeks to increase the
         allocation limit.  If `return_on_fail' is true, failure to resolve
         the problem causes the function to return false.  Otherwise, all
         failures are noted by calls to `broker->note_allocation_failure' if
         there is a memory broker (might throw an exception) and generation
         of an informative error through `kdu_error'.  If successful, the
         function returns true. */
    KDU_EXPORT void handle_failed_release();
      /* Called if `release_permit' is trying to release more memory than was
         allocated.  This function generates a warning when first called. */
  private: // Private definitions
    struct kd_allocator_frag { 
      public: // Member functions
        void init(size_t off)
          { alignment_reserved = ((KDU_MAX_L2_CACHE_LINE > KDU_OVERREAD_BYTES)?
                                  KDU_MAX_L2_CACHE_LINE:KDU_OVERREAD_BYTES);
            this->offset = off; bytes_reserved = 0;
            bytes_aligned = alignment_reserved;
            extra_bytes_reserved = alignment_reserved-1+2*KDU_OVERREAD_BYTES;
            buffer_size = buffer_alignment = buffer_extra_bytes = 0;
            buffer = NULL; buffer_handle = NULL;
          }
      public: // Data
        size_t offset; // multiple of the power-of-2 `frag_size'
        size_t bytes_reserved;
        size_t bytes_aligned; // Known divisor of `bytes_reserved'
        size_t alignment_reserved;
        size_t extra_bytes_reserved; // alignment-1 + 2*`KDU_OVERREAD_BYTES'
        size_t buffer_size; // If `buffer' already exists
        size_t buffer_alignment; // If `buffer' already exists
        size_t buffer_extra_bytes; // If `buffer' already exists
        kdu_byte *buffer; // Non-NULL if the fragment has been finalized
        kdu_byte *buffer_handle; // Actual address of allocated memory block
      };
  private: // Data
    bool pre_creation_phase; // True if in the pre-creation phase.
    bool prealloc_overflow; // True if had numeric overflow in pre-alloc phase
    size_t existing_bytes; // Sum of frag (buffer_size+buffer_extra_bytes)
    size_t permit_bytes; // Sum of all unreleased successful `seek_permit' vals
    size_t frag_size; // Equals 2^frag_bits
    int frag_bits; // This is the `log2_frag_size' value
    int existing_frags; // Number of fragments with an allocated `buffer'
    int max_frags; // Size of the `frags' array
    kd_allocator_frag *pre_frag; // Active during pre-alloc; NULL if finalized
    kd_allocator_frag *fragments; // Fragment array -- local or heap-based
    kdu_int64 limit_bytes; // From `membroker'
    kdu_int64 remaining_bytes; // limit - permit_bytes - existing_frag_bytes
    kdu_membroker *broker; // May be NULL
    kd_allocator_frag local_frag; // Used if there is only one fragment
    kdu_int64 num_failed_release; // Counts `handle_failed_release' calls
  };

/*****************************************************************************/
/*                              kdu_line_buf                                 */
/*****************************************************************************/

#define KDU_LINE_BUF_ABSOLUTE     ((kdu_byte) 0x01)
#define KDU_LINE_BUF_SHORTS       ((kdu_byte) 0x02)
#define KDU_LINE_BUF_EXCHANGEABLE ((kdu_byte) 0x04)

class kdu_line_buf { 
  /* [BIND: copy]
     [SYNOPSIS]
     Instances of this structure manage the buffering of a single line of
     sample values, whether image samples or subband samples.  For the
     reversible path, samples must be absolute 32- or 16-bit integers.  For
     irreversible processing, samples have a normalized representation, where
     the nominal range of the data is typically -0.5 to +0.5 (actual nominal
     ranges for the data supplied to a `kdu_encoder' or `kdu_analysis' object
     or retrieved from a `kdu_decoder' or `kdu_synthesis' object may be
     explicitly set in the relevant constructor).  These normalized quantities
     may have either a true 32-bit floating point representation or a 16-bit
     fixed-point representation.  In the latter case, the least significant
     `KDU_FIX_POINT' bits of the integer are interpreted as binary fraction
     bits, so that 2^{KDU_FIX_POINT} represents a normalized value of 1.0.
     [//]
     The object maintains sufficient space to allow access to a given number
     of samples before the first nominal sample (the one at index 0) and a
     given number of samples beyond the last nominal sample (the one at
     index `width'-1).  These extension lengths are explained in the
     comments appearing with `pre_create'.
     [//]
     From Kakadu version 7, this object supports "exchangeable" line buffers,
     which provide a means for passing data across interfaces without copying
     the sample values themselves.  When passed across a `kdu_push_ifc::push'
     or `kdu_pull_ifc::pull' interface, an exchangeable line buffer might
     be copied to/from a resource managed by the target object or else the
     underlying memory buffer might be exchanged with a compatible buffer
     managed by the target object.  Compatible buffers must the same `width',
     the same data type, and the same nominal `extend_width' and
     `extend_right' parameters.  To set things up for more consistent
     exchange of buffers, Kakadu version 7's sample data processing objects
     are moving towards the use of 0 for `extend_left' and `extend_right'.
     This is accomplished by doing boundary extension operations on-the-fly
     wherever possible, rather than through memory transactions.
     [//]
     Also, from Kakadu version 7, the final allocation of line buffers
     (after pre-allocation) can be performed from any thread of execution
     without needing to take out any mutual exclusion locks.  This is part
     of the effort to reduce thread dependencies in Kakadu, so that multiple
     CPU cores can be used more effectively.
  */
  // --------------------------------------------------------------------------
  public: // Life cycle functions
    kdu_line_buf() { destroy(); }
      /* NB: It is important that this object has no meaningful destructor,
         and that the constructor only zeros out its contents.  This is
         because multi-transform data structures need to allocate arrays of
         line-buffers using a private allocator, and placement array operator
         new[]/delete[] overrides are mostly unusable in C++. */
    void destroy()
      { 
      /* [SYNOPSIS]
           Restores the object to its uninitialized state ready for a new
           `pre_create' call.  Does not actually destroy any storage, since
           the `kdu_sample_allocator' object from which storage is served
           does not permit individual deallocation of storage blocks.
      */
        width=0; flags=0; pre_created=0; alloc_off = 0;
        allocator=NULL; buf16=NULL; buf32=NULL;
      }
    void pre_create(kdu_sample_allocator *allocator,
                    int width, bool absolute, bool use_shorts,
                    int extend_left, int extend_right)
      { 
      /* [SYNOPSIS]
           Declares the characteristics of the internal storage which will
           later be created by `create'.  If `use_shorts' is true, the sample
           values will have 16 bits each and normalized values will use a
           fixed point representation with KDU_FIX_POINT fraction bits.
           Otherwise, the sample values have 32 bits each and normalized
           values use a true floating point representation.
           [//]
           This function essentially calls `allocator->pre_alloc', requesting
           enough storage for a line with `width' samples, providing for legal
           accesses up to `extend_left' samples before the beginning of the
           line and `extend_right' samples beyond the end of the line.
           [//]
           Note: from Kakadu version 7, the `extend_left' and `extend_right'
           arguments no longer have defaults, because we wish to encourage
           the use of 0 extensions whenever this is not too difficult.
           [//]
           The returned line buffer is guaranteed to be aligned on an L-byte
           boundary, the `extend_left' and `extend_right' values are rounded
           up to the nearest multiple of L bytes, and the length of the
           right-extended buffer is also rounded up to a multiple of L bytes,
           where L is 2*`KDU_ALIGN_SAMPLES16' for 16-bit samples
           (`use_shorts'=true) and 4*`KDU_ALIGN_SAMPLES32' for 32-bit samples
           (`use_shorts'=false).  Finally, it is possible to read at least
           `KDU_OVERREAD_BYTES' bytes beyond the end or before the start of
           the extended region, although writes to these extra bytes will
           generally overwrite data belonging to other buffers allocated
           by the `allocator' object.
         [ARG: allocator]
           Pointer to the object which will later be used to complete the
           allocation of storage for the line.  The pointer is saved
           internally until such time as the `create' function is called, so
           you must be careful not to delete this object.  You must also be
           careful to call its `kdu_sample_allocator::finalize' function
           before calling `create'.
         [ARG: width]
           Nominal width of (number of samples in) the line.  Note that space
           reserved for access to `extend_left' samples to the left and
           `extend_right' samples to the right.  Moreover, additional
           samples may often be accessed to the left and right of the nominal
           line boundaries due to the alignment policy discussed above.
         [ARG: absolute]
           If true, the sample values in the line buffer are to be used in
           JPEG2000's reversible processing path, which works with absolute
           integers.  otherwise, the line is prepared for use with the
           irreversible processing path, which works with normalized
           (floating or fixed point) quantities.
         [ARG: use_shorts]
           If true, space is allocated for 16-bit sample values
           (array entries will be of type `kdu_sample16').  Otherwise, the
           line buffer will hold samples of type `kdu_sample32'.
         [ARG: extend_left]
           This quantity should be small, since it will be represented
           internally using 8-bit numbers, after rounding up to an
           appropriately aligned value.  It would be unusual to select
           values larger than 16 or perhaps 32.  If you intend to use this
           buffer in an exchange operation (see `set_exchangeable'), it is
           most likely best to use 0 for the `extend_left' and `extend_right'
           to encourage compatibility with the buffer with which you want
           to perform an exchange.
         [ARG: extend_right]
           This quantity should be small, since it will be represented
           internally using 8-bit numbers, after rounding up to an
           appropriately aligned value.  It would be unusual to select
           values larger than 16 or perhaps 32.
      */
        assert((!pre_created) && (this->allocator == NULL));
        if (use_shorts)
          extend_right =
            (extend_right+KDU_ALIGN_SAMPLES16-1) & ~(KDU_ALIGN_SAMPLES16-1);
        else
          extend_right =
            (extend_right+KDU_ALIGN_SAMPLES32-1) & ~(KDU_ALIGN_SAMPLES32-1);
        assert((extend_left <= 255) && (extend_right <= 255));
        this->width=width;
        flags = (use_shorts)?KDU_LINE_BUF_SHORTS:0;
        flags |= (absolute)?KDU_LINE_BUF_ABSOLUTE:0;
        this->allocator = allocator;
        buf_before = (kdu_byte) extend_left; // Rounded up automatically
        buf_after = (kdu_byte) extend_right;
        alloc_off=allocator->pre_alloc(use_shorts,buf_before,width+buf_after);
        pre_created = 1;
      }
    void pre_create(kdu_sample_allocator *allocator, int width, kdu_byte flags,
                    int extend_left, int extend_right)
      /* [SYNOPSIS]
           This form of the function provides more control over the
           characteristics of the line buffer.  In place of the
           `absolute' and `use_shorts' arguments accepted by the first version
           of the function, this one takes a `flags' argument that accepts
           the following flags:
           [>>] `KDU_LINE_BUF_ABSOLUTE' -- setting this flag is equivalent to
                passing true for the `absolute' argument to the first version
                of the `pre_create' function.
           [>>] `KDU_LINE_BUF_SHORTS' -- setting this flag is equivalent to
                passing true for the `use_shorts' argument to the first version
                of the `pre_create' function.
           [>>] `KDU_LINE_BUF_EXCHANGEABLE' -- setting this flag obviates the
                need to call `set_exchangeable' after `pre_create'.
      */
      { 
        assert((!pre_created) && (this->allocator == NULL));
        this->flags = flags;
        int align_mask = KDU_ALIGN_SAMPLES16-1;
        if (flags & KDU_LINE_BUF_SHORTS)
          align_mask = KDU_ALIGN_SAMPLES32-1;
        extend_right += (-extend_right) & align_mask;
        assert((extend_left <= 255) && (extend_right <= 255));
        this->width=width;
        this->allocator = allocator;
        buf_before = (kdu_byte) extend_left; // Rounded up automatically
        buf_after = (kdu_byte) extend_right;
        int len = width + buf_after;
        alloc_off =
          allocator->pre_alloc((flags & KDU_LINE_BUF_SHORTS)?true:false,
                               buf_before,len);
        pre_created = 1;
      }
    void create()
      { 
      /* [SYNOPSIS]
           Finalizes creation of storage which was initiated by `pre_create'.
           Does nothing at all if the `pre_create' function was not called,
           or the object was previously created and has not been destroyed.
           Otherwise, you may not call this function until the
           `kdu_sample_allocator' object supplied to `pre_create' has had
           its `finalize' member function called.
           [//]
           Note: from Kakadu version 7, calls to this function need not
           be guarded by mutual exclusion objects, even when invoked from
           multiple threads concurrently.
      */
        if (!pre_created)
          return;
        pre_created = 0;
        size_t off = alloc_off;
        orig_allocator = allocator;
        int len = width + buf_after;
        if (flags & KDU_LINE_BUF_SHORTS)
          buf16 = allocator->alloc16(buf_before,len,off);
        else
          buf32 = allocator->alloc32(buf_before,len,off);
      }
    int check_status()
      { 
      /* [SYNOPSIS]
           Returns 1 if `create' has been called, -1 if only `pre_create' has
           been called, 0 if neither has been called since the object was
           constructed or since the last call to `destroy'.
      */
        if (pre_created) return -1;
        else if (buf != NULL) return 1;
        else return 0;
      }
    void set_exchangeable() { flags |= KDU_LINE_BUF_EXCHANGEABLE; }
      /* [SYNOPSIS]
           This function simply marks the line buffer as "exchangeable".
           Otherwise, attempts to pass this object to another line buffer's
           `exchange' function will return false, doing nothing.  Typically,
           the caller uses this function to mark buffers that are passed to
           a `kdu_push_ifc::push' or `kdu_pull_ifc::pull' call as resources
           that it is happy to have exchanged with a compatible resource
           that is managed by the target object.  Of course, if an exchange
           does happen, it will invalidate the buffer pointers you may have
           obtained in previous calls to `get_buf16' or `get_buf32', which
           is why you must explicitly invoke this function to express your
           preparedness for an exchange of the underlying memory resources.
           [//]
           You may call this function any time after `pre_create'.
      */
    bool exchange(kdu_line_buf &src)
      { 
        if ((!(src.flags & KDU_LINE_BUF_EXCHANGEABLE)) ||
            ((src.flags ^ flags) & ~KDU_LINE_BUF_EXCHANGEABLE) ||
            (src.width != width) || (src.buf_before != buf_before) ||
            (src.buf_after != buf_after) || src.pre_created ||
            (src.buf == NULL) || (buf == NULL) ||
            (src.orig_allocator != orig_allocator))
          return false;
        void *tmp = buf; buf = src.buf; src.buf = tmp;
        return true;
      }
      /* [SYNOPSIS]
           This function does nothing, returning false, unless
           `set_exchangeable' has been involed on `src' and the `src'
           buffer is compatible with the current one.  There is no need to
           invoke `set_exchangeable' on the current object; and its own
           exchangeability is not changed by this function.  If an exchange
           does happen, the underlying memory buffers associated with
           the current and `src' objects are exchanged, and the function
           returns true -- that is all.
           [//]
           The current and `src' objects are considered compatible if they
           were pre-created with identical attributes -- i.e., identical
           sample data attributes, identical `width' and identical
           `extend_left' and `extend_right' attributes.
           [//]
           The current and `src' objects should also have been allocated using
           the same `kdu_sample_allocator' object, but this condition might
           or might not be checked, depending upon the internal structure of
           the object -- i.e., be careful not to invoke this function in
           contexts where different allocators might have been used.
      */
    bool raw_exchange(kdu_sample16 *&raw_buf, int raw_width)
      { 
        if ((!(flags & KDU_LINE_BUF_EXCHANGEABLE)) || (this->buf_before!=0) ||
            (raw_width != (this->width+this->buf_after)) ||
            pre_created || (buf16==NULL) || !(flags & KDU_LINE_BUF_SHORTS))
          return false;
        kdu_sample16 *tmp = buf16; buf16 = raw_buf; raw_buf = tmp;
        return true;
      }
      /* [SYNOPSIS]
           Similar to `exchange', but this function requires the current
           object to be marked as "exchangeable" (see `set_exchangeable') and
           performs the exchange (if compatible) with a raw buffer identified
           by `raw_buf'.  This function should be used with great care,
           because there is insufficient auxiliary information for the
           function to make absolutely certain that the exchange is not
           dangerous.  The supplied `raw_buf' array is expected to be
           aligned on a multiple of 2*`KDU_ALIGN_SAMPLES16' bytes and span
           `raw_width' samples, rounded up to a whole multiple of
           `KDU_ALIGN_SAMPLES16' 16-bit samples.  The exchange succeeds if
           the current has `left_extend'=0, `width'+`right_extend'=`raw_width',
           a short integer/fixed-point representation, is marked as
           exchangeable.
           The `raw_buf' resource should have been allocated using the same
           `kdu_sample_allocator' as the current object.
      */
    bool raw_exchange(kdu_sample32 *&raw_buf, int raw_width)
      { 
        if ((!(flags & KDU_LINE_BUF_EXCHANGEABLE)) || (this->buf_before!=0) ||
            (raw_width != (this->width+this->buf_after)) ||
            pre_created || (buf32==NULL) || (flags & KDU_LINE_BUF_SHORTS))
          return false;
        kdu_sample32 *tmp = buf32; buf32 = raw_buf; raw_buf = tmp;
        return true;
      }
      /* [SYNOPSIS]
           See 16-bit version for an explanation.  Note that the `raw_buf'
           array is expected to be aligned on a multiple of
           4*`KDU_ALIGN_SAMPLES32' bytes and have storage that spans
           `raw_width' samples, rounded up to a whole multiple of
           `KDU_ALIGN_SAMPLES32' 32-bit samples.  That buffer should have
           been allocated using the same `kdu_sample_allocator' as the
           current object.
      */
  // --------------------------------------------------------------------------
  public: // Access functions
    kdu_sample32 *get_buf32()
      { 
      /* [SYNOPSIS]
           Returns NULL if the sample values are of type `kdu_sample16'
           instead of `kdu_sample32', or the buffer has not yet been
           created.  Otherwise, returns an array which supports accesses
           at least with indices in the range 0 to `width'-1, but typically
           beyond these bounds -- see `pre_create' for an explanation of
           extended access bounds.
           [//]
           Note that the returned pointer is always aligned on a
           4*`KDU_ALIGN_SAMPLES32'-byte boundary.
      */
        return (flags & KDU_LINE_BUF_SHORTS)?NULL:buf32;
      }
    kdu_sample16 *get_buf16()
      { 
      /* [SYNOPSIS]
           Returns NULL if the sample values are of type `kdu_sample32'
           instead of `kdu_sample16', or the buffer has not yet been
           created.  Otherwise, returns an array which supports accesses
           at least with indices in the range 0 to `width'-1, but typically
           beyond these bounds -- see `pre_create' for an explanation of
           extended access bounds.
           [//]
           Note that the returned pointer is always aligned on a
           2*`KDU_ALIGN_SAMPLES16'-byte boundary.
      */
        return (flags & KDU_LINE_BUF_SHORTS)?buf16:NULL;
      }
    void *get_buf()
      { 
      /* [SYNOPSIS]
           Returns the same pointer as `get_buf16' or `get_buf32', whichever
           (if any) would have returned non-NULL.  This function is provided
           only to avoid redundant conditional statements during certain
           generic operations that might work in the same way on all
           buffer types.
      */
        return buf;
      }
    bool get_floats(float *buffer, int first_idx, int num_samples)
      { 
      /* [SYNOPSIS]
           Returns false if the object does not hold a 32-bit normalized
           (floating point) sample representation.  Otherwise, returns true
           and copies the floating point samples into the supplied `buffer'.
           The first copied sample is `first_idx' positions from the start
           of the line.  There may be little or no checking that the
           sample range represented by `first_idx' and `num_samples' is
           legal, so be careful.
           [//]
           For native C/C++ interfacing, it is more efficient to explicitly
           access the internal buffer using `get_buf32'.
      */
        if (flags & (KDU_LINE_BUF_ABSOLUTE | KDU_LINE_BUF_ABSOLUTE))
          return false;
        for (int i=0; i < num_samples; i++)
          buffer[i] = buf32[first_idx+i].fval;
        return true;
      }
    bool set_floats(float *buffer, int first_idx, int num_samples)
      { 
      /* [SYNOPSIS]
           Returns false if the object does not hold a 32-bit normalized
           (floating point) sample representation.  Otherwise, returns true
           and copies the floating point samples from the supplied `buffer'.
           The first sample in `buffer' is stored `first_idx' positions from
           the start of the line.  There may be little or no checking that
           the sample range represented by `first_idx' and `num_samples' is
           legal, so be careful.
           [//]
           For native C/C++ interfacing, it is more efficient to explicitly
           access the internal buffer using `get_buf32'.
      */
        if (flags & (KDU_LINE_BUF_ABSOLUTE | KDU_LINE_BUF_ABSOLUTE))
          return false;
        for (int i=0; i < num_samples; i++)
          buf32[i+first_idx].fval = buffer[i];
        return true;
      }
    bool get_ints(kdu_int32 *buffer, int first_idx, int num_samples)
      { 
      /* [SYNOPSIS]
           Returns false if the object does not hold a 32-bit absolute
           (integer) sample representation.  Otherwise, returns true
           and copies the integer samples into the supplied `buffer'.
           The first copied sample is `first_idx' positions from the start
           of the line.  There may be little or no checking that the
           sample range represented by `first_idx' and `num_samples' is
           legal, so be careful.
           [//]
           For native C/C++ interfacing, it is more efficient to explicitly
           access the internal buffer using `get_buf32'.
      */
        if ((flags & KDU_LINE_BUF_SHORTS) || !(flags & KDU_LINE_BUF_ABSOLUTE))
          return false;
        for (int i=0; i < num_samples; i++)
          buffer[i] = buf32[first_idx+i].ival;
        return true;
      }
    bool set_ints(kdu_int32 *buffer, int first_idx, int num_samples)
      { 
      /* [SYNOPSIS]
           Returns false if the object does not hold a 32-bit absolute
           (integer) sample representation.  Otherwise, returns true
           and copies the integer samples from the supplied `buffer'.
           The first sample in `buffer' is stored `first_idx' positions from
           the start of the line.  There may be little or no checking that
           the sample range represented by `first_idx' and `num_samples' is
           legal, so be careful.
           [//]
           For native C/C++ interfacing, it is more efficient to explicitly
           access the internal buffer using `get_buf32'.
      */
        if ((flags & KDU_LINE_BUF_SHORTS) || !(flags & KDU_LINE_BUF_ABSOLUTE))
          return false;
        for (int i=0; i < num_samples; i++)
          buf32[i+first_idx].ival = buffer[i];
        return true;
      }
    bool get_ints(kdu_int16 *buffer, int first_idx, int num_samples)
      { 
      /* [SYNOPSIS]
           Returns false if the object does not hold a 16-bit sample
           representation (either fixed point or absolute integers).
           Otherwise, returns true and copies the 16-bit samples into the
           supplied `buffer'.  The first copied sample is `first_idx'
           positions from the start of the line.  There may be little or no
           checking that the sample range represented by `first_idx' and
           `num_samples' is legal, so be careful.
           [//]
           For native C/C++ interfacing, it is more efficient to explicitly
           access the internal buffer using `get_buf16'.
      */
        if (!(flags & KDU_LINE_BUF_SHORTS))
          return false;
        for (int i=0; i < num_samples; i++)
          buffer[i] = buf16[first_idx+i].ival;
        return true;
      }
    bool set_ints(kdu_int16 *buffer, int first_idx, int num_samples)
      { 
      /* [SYNOPSIS]
           Returns false if the object does not hold a 16-bit sample
           representation (either fixed point or absolute integers).
           Otherwise, returns true and copies the integer samples from the
           supplied `buffer'.  The first sample in `buffer' is stored
           `first_idx' positions from the start of the line.  There may be
           little or no checking that the sample range represented by
           `first_idx' and `num_samples' is legal, so be careful.
           [//]
           For native C/C++ interfacing, it is more efficient to explicitly
           access the internal buffer using `get_buf16'.
      */
        if (!(flags & KDU_LINE_BUF_SHORTS))
          return false;
        for (int i=0; i < num_samples; i++)
          buf16[i+first_idx].ival = buffer[i];
        return true;
      }
    int get_width()
      { 
      /* [SYNOPSIS]
           Returns 0 if the object has not been created.  Otherwise, returns
           the nominal width of the line.  Remember that the arrays returned
           by `get_buf16' and `get_buf32' are typically larger than the actual
           line width, as explained in the comments appearing with the
           `pre_create' function.
      */
        return width;
      }
    bool is_absolute() { return ((flags & KDU_LINE_BUF_ABSOLUTE) != 0); }
      /* [SYNOPSIS]
           Returns true if the sample values managed by this object represent
           absolute integers, for use with JPEG2000's reversible processing
           path.
      */
  // --------------------------------------------------------------------------
  private: // Data -- should occupy 12 bytes on a 32-bit machine.
    int width; // Number of samples in buffer.
    kdu_byte buf_before, buf_after;
    kdu_byte flags; // See `pre_create'
    kdu_byte pre_created; // True after `pre_create' if `create' still pending
    union { 
      size_t alloc_off; // Saves `allocator->pre_alloc' return until `create'
      kdu_sample_allocator *orig_allocator; // Valid after `create' is called
    };
    union { 
      kdu_sample32 *buf32; // Valid if !KDU_LINE_BUF_SHORTS and !`pre_created'
      kdu_sample16 *buf16; // Valid if KDU_LINE_BUF_SHORTS && !`pre_created'
      void *buf; // Facilitates untyped buffer exchange
      kdu_sample_allocator *allocator; // Valid if `pre_created'
    };
  };

/*****************************************************************************/
/*                            kdu_push_ifc_base                              */
/*****************************************************************************/

class kdu_push_ifc_base { 
  protected:
    friend class kdu_push_ifc;
    virtual ~kdu_push_ifc_base() { return; }
    virtual void start(kdu_thread_env *env) = 0;
    virtual bool push(kdu_line_buf &line, kdu_thread_env *env) = 0;
    virtual bool finish(kdu_thread_env *env) = 0;
  };

/*****************************************************************************/
/*                               kdu_push_ifc                                */
/*****************************************************************************/

#define KDU_LINE_WILL_BE_EXTENDED ((int) 0x00000001)
#define KDU_LINE_EXTRA_WIDTH_MASK ((int) 0x00FFFF00)
#define KDU_LINE_EXTRA_WIDTH_POS  ((int) 8)
#define KDU_LINE_EXTRA_WIDTH_MAX  ((int) 65535)

class kdu_push_ifc { 
  /* [BIND: reference]
     [SYNOPSIS]
     All classes which support `push' calls derive from this class so
     that the caller may remain ignorant of the specific type of object to
     which samples are being delivered.  The purpose of derivation is usually
     just to introduce the correct constructor.  The objects are actually just
     interfaces to an appropriate object created by the relevant derived
     class's constructor.  The interface directs `push' calls to the internal
     object in a manner which should incur no cost.
     [//]
     The interface objects may be copied at will; the internal object will
     not be destroyed when an interface goes out of scope.  Consequently,
     the interface objects do not have meaningful destructors.  Instead,
     to destroy the internal object, the `destroy' member function must be
     called explicitly.
     [//]
     In addition to `push', every class derived from `kdu_push_ifc' must
     implement both the `start' and the `finish' function.
  */
  public: // Member functions
    kdu_push_ifc() { state = NULL; }
      /* [SYNOPSIS]
           Creates an empty interface.  You must assign the interface to
           one of the derived objects such as `kdu_encoder' or `kdu_analysis'
           before you may use the `push' function.
      */
    KDU_EXPORT void destroy();
      /* [SYNOPSIS]
           Automatically destroys all objects which were created by the
           relevant derived object's constructor, including lower level
           DWT analysis stages and encoding objects.  Upon return, the
           interface will be empty, meaning that `exists' returns
           false.
      */
    void start(kdu_thread_env *env)
      { /* [SYNOPSIS]
             This function may be called at any point after construction
             of a `kdu_analysis' or `kdu_encoder' object, once you have
             invoked the `kdu_sample_allocator::finalize' function on the
             `kdu_sample_allocator' object used during construction.  In
             particular, this means that you will not be creating any
             further objects to share the storage offered by the sample
             allocator.
             [//]
             For multi-threaded applications (i.e., when `env' is non-NULL),
             calling this function explicitly allows the codestream machinery's
             background processing job to start allocating suitable containers
             to receive code-block bit-streams once they are produced.  Doing
             this ahead of time can avoid contention later on if you have
             a lot of processing threads simultaneously trying to do this.
             [//]
             However, it is not actually necessary to call this function
             explicitly.
             [//]
             The `kdu_multi_analysis' object automatically invokes `start'
             on all encoder/analysis objects that it creates.
         */
        state->start(env);
      }
    bool exists() { return (state==NULL)?false:true; }
      /* [SYNOPSIS]
           Returns false until the object is assigned to one constructed
           by one of the derived classes, `kdu_analysis' or `kdu_encoder'.
           Also returns false after `destroy' returns.
      */
    bool operator!() { return (state==NULL)?true:false; }
      /* [SYNOPSIS]
           Opposite of `exists', returning true if the object has not been
           assigned to one of the more derived objects, `kdu_analysis' or
           `kdu_encoder', or after a call to `destroy'.
      */
    kdu_push_ifc &operator=(kdu_analysis rhs);
      /* [SYNOPSIS]
           It is safe to directly assign a `kdu_analysis' object to a
           `kdu_push_ifc' object, since both are essentially just references
           to an anonymous internal object.
      */
    kdu_push_ifc &operator=(kdu_encoder rhs);
      /* [SYNOPSIS]
           It is safe to directly assign a `kdu_encoder' object to a
           `kdu_push_ifc' object, since both are essentially just references
           to an anonymous internal object.
      */
    bool push(kdu_line_buf &line, kdu_thread_env *env=NULL)
      { /* [SYNOPSIS]
             Delivers all samples from the `line' buffer across the interface.
             [//]
             From KDU 7, the `line' buffer can be marked as "exchangeable'
             using `kdu_line_buf::set_exchangeable', in which case the
             internal implementation of this function might exchange the
             supplied `line' object's internal memory buffer with one of
             its own, rather than copying sample values one by one.  Of
             course, this is generally more efficient, but you should be
             careful not to pass a `line' object marked as exchangeable if
             you actually need access to the data, or if you are keeping a
             copy of the internal memory pointer obtained via
             `line.get_buf16' or `line.get_buf32', or if `line' was allocated
             using a different `kdu_sample_allocator' object to the one
             used to construct the target object.
           [RETURNS]
             Returns false if the call did not complete, meaning that the
             function should be called again with the same `line' buffer.
             In practice, this should only happen when using multiple
             simultaneous processing fragments (i.e., with the relevant
             `kdu_node::is_fragmented' function returns true), and that
             should only happen during multi-threaded processing.  In
             particular, when there are multiple simultaneous processing
             fragments, it is possible for deadlock to occur during the
             terminal stages of spatial analysis processing, when pushing a
             final line to this function can result in multiple downward
             `push' calls into descendant subbands before the function returns.
             The deadlock risk arises because multiple fragment processing
             threads can enter a working wait state in which they wait upon
             different points in each others' call stack, while trying to
             progress the state of the `kdu_analysis_fusion' object that
             collects fragments to pass further down the hierarchy.  The
             situation is avoided by insisting that whenever a fusion
             interface may exist below a given `push' call, that call never
             pushes multiple lines to any of its descendant interfaces before
             returning to the top of the push hierarchy, where multi-threaded
             dependencies are inspected.  This, in turn, is achieved by
             returning false so that the call gets re-issued from the top of
             the hierarchy.  Again, this can only happen with multiple
             processing fragments and only when the last line of the relevant
             `kdu_node' is being pushed in.
           [ARG: env]
             If the object was constructed for multi-threaded processing
             (see the constructors for `kdu_analysis' and `kdu_encoder'),
             you MUST pass a non-NULL `env' argument in here, identifying
             the thread which is performing the `push' call.  Otherwise,
             the `env' argument should be ignored.
        */
        return state->push(line,env);
      }
    bool finish(kdu_thread_env *env)
      { /* [SYNOPSIS]
             This function should be called once all `push' calls have been
             delivered.  Prior to Kakadu version 8, there was no `finish'
             function, and if none of the new features are used then it is
             OK to not invoke `finish' at all.  However, calls to `finish'
             are required if the encoding of buffered samples is being
             intentionally deferred, perhaps by a few lines or even until all
             samples have been pushed in.  Deferred encoding is employed when
             collecting subband statistics for use in estimating complexity
             constraints -- see `kdu_cplex_analysis'.  It is good
             practice, therefore, to always ensure that this function is
             called after all `push' calls have been delivered and before the
             object is cleaned up.
             [//]
             Calls to `finish' and `push' may arrive on different threads,
             but no call to `finish' should occur until after all calls to
             `push' have returned, supplying all of the relevant sample values.
             If the `push' process is terminated prematurely for any reason,
             `finish' must not be called, and implementations can rely upon
             this being the case.
             [//]
             Implementations do, however, need to be prepared for the
             possibility that a `finish' call arrives during or after a
             call to `kdu_thread_queue::request_termination' that attempts
             to terminate the issuing of block encoding, or perhaps other
             work early.
           [RETURNS]
             The function returns true if all relevant processing has indeed
             been finished, so there is no need to call the function again.
             Beyond that point, each call to the function is guaranteed to
             just return true without doing anything.  Generally, calls to
             `finish' are passed down the spatial transformation hierarchy,
             all to the way to the subband encoding objects at the leaves of
             the tree, repeatedly until all calls return true.  This allows
             encoders to interleave any final processing work they need to
             perform with that of other subband encoders, if desired.
        */
        return state->finish(env);
      }
  protected: // Data
    friend class kdu_analysis_fusion;
    kdu_push_ifc_base *state;
  };

/*****************************************************************************/
/*                            kdu_pull_ifc_base                              */
/*****************************************************************************/

class kdu_pull_ifc_base { 
  protected:
    friend class kdu_pull_ifc;
    virtual ~kdu_pull_ifc_base() { return; }
    virtual bool start(kdu_thread_env *env) = 0;
    virtual void pull(kdu_line_buf &line, kdu_thread_env *env) = 0;
  };

/*****************************************************************************/
/*                               kdu_pull_ifc                                */
/*****************************************************************************/

class kdu_pull_ifc { 
  /* [BIND: reference]
     [SYNOPSIS]
     All classes which support `pull' calls derive from this class so
     that the caller may remain ignorant of the specific type of object from
     which samples are to be recovered.  The purpose of derivation is usually
     just to introduce the correct constructor.  The objects are actually just
     interfaces to an appropriate object created by the relevant derived
     class's constructor.  The interface directs `pull' calls to the internal
     object in a manner which should incur no cost.
     [//]
     The interface objects may be copied at will; the internal object will
     not be destroyed when an interface goes out of scope.  Consequently,
     the interface objects do not have meaningful destructors.  Instead,
     to destroy the internal object, the `destroy' member function must be
     called explicitly.
  */
  public: // Member functions
    kdu_pull_ifc() { state = NULL; }
      /* [SYNOPSIS]
           Creates an empty interface.  You must assign the interface to
           one of the derived objects such as `kdu_decoder' or `kdu_synthesis'
           before you may use the `pull' function.
      */
    KDU_EXPORT void destroy();
      /* [SYNOPSIS]
           Automatically destroys all objects which were created by this
           relevant derived object's constructor, including lower level
           DWT synthesis stages and decoding objects.  Upon return, the
           interface will be empty, meaning that `exists' returns false.
      */
    bool start(kdu_thread_env *env)
      { /* [SYNOPSIS]
             This function may be called at any point after construction
             of a `kdu_synthesis' or `kdu_decoder' object, once you have
             invoked the `kdu_sample_allocator::finalize' function on the
             `kdu_sample_allocator' object used during construction.  In
             particular, this means that you will not be creating any
             further objects to share the storage offered by the sample
             allocator.
             [//]
             For multi-threaded applications (i.e., when `env' is non-NULL),
             this function allows code-block processing to be started
             immediately, which can help maximize throughput.
             [//]
             NOTE CAREFULLY: You should call this function REPEATEDLY
             (possibly inerleaved with other calls) UNTIL IT RETURNS TRUE,
             PRIOR TO the first call to `pull'.  If you choose to not call
             the function at all, that is OK, but in that case the function
             will automatically be called from within the first call to
             `pull'.  This function is not itself thread-safe, so you
             must be sure that another thread does not invoke `pull' while
             you are invoking this function.
             [//]
             It is not completely necessary to call this function, unless
             another data processing object is set up to defer the scheduling
             of jobs until dependencies associated with `kdu_decoder' objects
             are satisfied; in that case, if you fail to call this function,
             there will never be a call to `pull', assuming that this happens
             only when dependencies are satisifed.  Otherwise, in the event
             that you do not call this function, it will be invoked
             automatically when `pull' is first called.
             [//]
             The `kdu_multi_synthesis' object automatically invokes `start'
             on all decoder/synthesis objects that it creates, arranging
             for all calls to `start' to occur before any synchronous or
             asynchronous call to `pull'.
             [//]
             For applications which are not multi-threaded (i.e., when `env'
             is NULL) there is no particular benefit to calling this
             function, but you can if you like.
         */
        return state->start(env);
      }
    bool exists() { return (state==NULL)?false:true; }
      /* [SYNOPSIS]
           Returns false until the object is assigned to one constructed
           by one of the derived classes, `kdu_synthesis' or `kdu_decoder'.
           Also returns false after `destroy' returns.
      */
    bool operator!() { return (state==NULL)?true:false; }
      /* [SYNOPSIS]
           Opposite of `exists', returning true if the object has not been
           assigned to one of the more derived objects, `kdu_synthesis' or
           `kdu_decoder', or after a call to `destroy'.
      */
    kdu_pull_ifc &operator=(kdu_synthesis rhs);
      /* [SYNOPSIS]
           It is safe to directly assign a `kdu_synthesis' object to a
           `kdu_pull_ifc' object, since both are essentially just references
           to an anonymous internal object.
      */
    kdu_pull_ifc &operator=(kdu_decoder rhs);
      /* [SYNOPSIS]
           It is safe to directly assign a `kdu_decoder' object to a
           `kdu_pull_ifc' object, since both are essentially just references
           to an anonymous internal object.
      */
    void pull(kdu_line_buf &line, kdu_thread_env *env=NULL)
      { /* [SYNOPSIS]
             Fills out the supplied `line' buffer's sample values before
             returning.
             [//]
             From KDU 7, the `line' buffer can be marked as "exchangeable'
             using `kdu_line_buf::set_exchangeable', in which case the
             internal implementation of this function might exchange the
             supplied `line' object's internal memory buffer with one of
             its own, rather than copying sample values one by one.  Of
             course, this is generally more efficient, but you should be
             careful not to pass a `line' object marked as exchangeable if
             you are keeping a separate copy of the buffer returned by
             `line.get_buf16' or `line.get_buf32', or if `line' was allocated
             using a different `kdu_sample_allocator' object to the one
             used to construct the target object.
           [ARG: env]
             If the object was constructed for multi-threaded processing
             (see the constructors for `kdu_synthesis' and `kdu_decoder'),
             you MUST pass a non-NULL `env' argument in here, identifying
             the thread which is performing the `pull' call.  Otherwise,
             the `env' argument should be ignored.
        */
        state->pull(line,env);
      }
  protected: // Data
    kdu_pull_ifc_base *state;
  };

/*****************************************************************************/
/*                          kdu_push_pull_params                             */
/*****************************************************************************/

class kdu_push_pull_params { 
  /* [BIND: reference]
     [SYNOPSIS]
       This class allows for the collection and distribution of parameters
       that may affect the behaviour of objects derived from the
       `kdu_push_ifc' and `kdu_pull_ifc' interfaces, from which compression
       and decompression machinery is constructed.
       [//]
       Instances of `kdu_push_pull_params' serve to keep track of objects
       that fuse the results from multiple simultaneous processing fragments
       at some level in the spatial transformation hierarchy for a
       tile-component, along with information that can be initialize such
       objects.  In particular, `kdu_analysis_fusion' objects may need
       to be created to handle such a fusion task, in which case they need
       to be shared across fragments, and `kdu_push_pull_params' provides
       the means for such sharing.
       [//]
       Instances of `kdu_push_pull_params' are also used to distribute
       information about the double buffering dimensions in the spatial
       analysis machinery and about the number of components and simultaneous
       processing fragments that are being used, so that lower level elements
       such as `kdu_encoder' and `kdu_analysis_fusion' can make informed
       choices about how to allocate their own resources.
       [//]
       Instances of `kdu_push_pull_params' are also used to make
       `kdu_cplex_share' objects available to corresponding tiles within
       consecutive video frames, so that the statistics gathered in one frame
       for optimizing encoding constraints can be used in the following frame
       also, to the extent that this is appropriate.
       [//]
       Similarly, instances of `kdu_push_pull_params' are used to make
       `kdu_cplex_bkgnd' objects available to tile processing engines and
       their constituent elements, so that generic background statistics can
       be collected and shared with compression tasks.  Unlike
       `kdu_cplex_share', which shares complexity statistics between video
       frames, `kdu_cplex_bkgnd' shares complexity statistics between images
       or between separate videos, usually by collecting and storing statistics
       in an external repository, which can then be used to provide
       complexity constrained encoding procedures with valuable hints as to
       how best to use a defined amount of encoding resources.  Both
       `kdu_cplex_share' and `kdu_cplex_bkgnd' mechanisms can be used together,
       allowing background statistics to be used to facilitate the most
       efficient complexity constrained compression of the very first frame
       in the video.
       [//]
       Instances of `kdu_push_pull_params' are also used to exchange
       parameters with the `kdu_encoder' and `kdu_decoder' objects that allow
       customization of the way in which subband data is decomposed into block
       coding jobs in multi-threaded environments.
       [//]
       In the future, other kinds of information and interfaces may need to
       be shared between processing pipelines, and this will be done by
       extending the features of `kdu_push_pull_params'.
  */
  public:
    kdu_push_pull_params()
      { 
        spatial_xform_dbuf_rows = 0; num_component_frags = 1;
        cplex_share_ref = NULL;  cplex_bkgnd_ref = NULL;
        log2_min_block_samples = 12; // Default value of 4096 samples/job
        log2_ideal_block_samples = 14; // Default value of 16384 samples/job
        min_block_jobs_across = 0; // Auto-determined
        max_hires_block_stripes = 0; // Auto-determined
        extra_lowres_block_stripes = 1;
      };
    kdu_push_pull_params(const kdu_push_pull_params &src)
      { kdu_push_pull_params(); copy_params(src); }
      /* [SYNOPSIS]
           Equivalent to construction, followed by `copy_params'.
      */
    kdu_push_pull_params &operator=(const kdu_push_pull_params &src)
      { copy_params(src); return *this; }
      /* [SYNOPSIS]
           Ensures that assignment is equivalent to invoking `copy_params', so
           that nothing is accidentally copied that should not be.
      */
    int get_spatial_xform_dbuf_rows() const
      { return spatial_xform_dbuf_rows; }
      /* [SYNOPSIS]
           Returns the actual (or assumed) double buffering height associated
           with multi-threaded implementation of the spatial transformation
           (usually DWT) processing of each tile-component.  This parameter
           has the same meaning as the `buffer_rows' argument to
           `kdu_multi_analysus::create' and `kdu_multi_synthesis::create'.  In
           fact, if those functions are invoked with the
           `KDU_MULTI_XFORM_DBUF' flag and a non-NULL `extra_params' argument,
           this function is used to obtain an alternate value for the
           `buffer_rows' parameter, and the larger of the two is used to
           dimension the internal buffers used to decouple spatial
           transformation processing from other multi-component transformation
           processing steps.
           [//]
           Perhaps more importantly, `kdu_multi_analysis'
           and `kdu_multi_synthesis' record the number of buffer rows they are
           using via `set_spatial_xform_dbuf_rows' within the
           `kdu_push_pull_params' object that is passed into `kdu_analysis'
           and similar interfaces.  This can be useful in determining a
           reasonable amount of buffering to introduce into objects like
           `kdu_analysis_fusion', which fuse simultaneous processing fragments
           together.
           [//]
           As explained for the `buffer_rows' argument to
           `kdu_multi_analysis::create' and `kdu_multi_synthesis::create',
           the value returned here is actually only around half the total
           number of lines of buffering that are used to decouple
           simultaneous processing of separate components, or fragments of
           components (hence the name "double-buffering" for which "dbuf"
           stands).
      */
    void set_spatial_xform_dbuf_rows(int buffer_rows)
      { spatial_xform_dbuf_rows = (buffer_rows < 0)?0:buffer_rows; }
      /* [SYNOPSIS]
           See `get_spatial_xform_dbuf_rows' for an explanation.
      */
    int get_num_component_frags() const
      { return (num_component_frags > 0)?num_component_frags:1; }
      /* [SYNOPSIS]
           Returns the actual (or assumed) number of component-fragments that
           are being processed within a tile.  This value can be used by
           the subband encoding machinery to determine a reasonable number
           of jobs to instantiate for concurrent processing of code-blocks.
      */
    void set_num_component_frags(int cf)
      { num_component_frags = cf; }
      /* [SYNOPSIS]
            See `get_num_component_frags' for an explanation.  This function
            is normally invoked from within `kdu_multi_analysis' or
            `kdu_multi_synthesis'.
      */
    kdu_cplex_share *get_cplex_share() const
      { return cplex_share_ref; }
      /* [SYNOPSIS]
           Used within `kdu_multi_analysis' and `kdu_fusion_analysis' to
           retrieve any reference deposited by `set_cplex_share', invoking
           `kdu_cplex_share::get_tile' and `kdu_cplex_share_tile::get_cfrag'
           to gain access to a thread-safe resource for sharing statistics for
           a particular fragment (simultaneous processing fragment) of any
           given tile-component between (usually) consecutive frames.
      */
    void set_cplex_share(kdu_cplex_share *ref)
      { cplex_share_ref = ref; }
      /* [SYNOPSIS]
           Video encoding applications are strongly recommended to create a
           single instance of the `kdu_cplex_share' class and make it available
           to all `kdu_multi_analysis' engines by creating an externally
           managed `kdu_push_pull_params' object, setting a reference to the
           `kdu_cplex_share' object via this function, and passing the
           `kdu_push_pull_params' object to `kdu_multi_analysis::create',
           as its `extra_params' argument, or to `kdu_stripe_compressor::start'
           as its `multi_xform_extra_params' argument.
           [//]
           The object referenced by `ref' is not owned (or currently
           reference-counted) by `kdu_push_pull_params', so it is your
           responsibility to clean it up, and you should not do this until
           all processing is complete.
      */
    kdu_cplex_bkgnd *get_cplex_bkgnd() const
      { return cplex_bkgnd_ref; }
      /* [SYNOPSIS]
           Used within `kdu_multi_analysis' and `kdu_fusion_analysis' to
           retrieve any reference deposited by `set_cplex_bkgnd', invoking
           `kdu_cplex_bkgnd::access_component' and
           `kdu_cplex_share_comp::access_level' to gain access to any
           existing background statistics (typically collected from other
           images or videos) and to gain access to a thread-safe resource for
           aggregating background statistics, if the `kdu_cplex_bkgnd' object
           was not constructed in the read-only mode.
      */
    void set_cplex_bkgnd(kdu_cplex_bkgnd *ref)
      { cplex_bkgnd_ref = ref; }
      /* [SYNOPSIS]
           Image encoders, and perhaps video encoders, are recommended to
           supply background statistics when using the Cplex-EST complexity
           constraint algorithm (see the `Cplex' attribute managed by
           `cod_params').  Background statistics can be collected from the
           compression of other images or video, using the same mechanism.
           To do this, an instance of `kdu_cplex_bkgnd' is constructed and
           a reference to it is passed to this function before passing a
           reference to the `kdu_push_pull_params' object to
           `kdu_multi_analysis::start', as its `extra_params' argument, or to
           `kdu_stripe_compressor::start' as `multi_xform_extra_params'
           argument.
           [//]
           The object referenced by `ref' is not owned (or currently
           reference-counted) by `kdu_push_pull_params', so it is your
           responsibility to clean it up, and you should not do this until
           all processing is complete.
           [//]
           This object is usually associated with a mechanism for
           importing and/or exporting background statistics from /to an
           external repository (e.g., a background statistics file), but
           we do not define the format for such files in the Kakadu core
           system.
      */
    kdu_push_ifc get_analysis_fusion_interface() const
      { return analysis_fusion_ifc; }
      /* [SYNOPSIS]
           Used inside `kdu_analysis' to discover any
           existing `kdu_analysis_fusion'
           interface that may have been created for a separate simultaneous
           processing fragment, so that a new interface to the same underlying
           object can be created for a new fragment.
           [//]
           Returns an empty interface if `set_analysis_fusion_interface' has
           not been called since this object was constructed.
      */
    void set_analysis_fusion_interface(const kdu_push_ifc ifc)
      { analysis_fusion_ifc = ifc; }
      /* [SYNOPSIS]
           Used inside `kdu_analysis' to store the
           interface to a `kdu_analysis_fusion'
           object that can be recovered by `get_analysis_fusion_interface'
           inside a separate processing fragment.
           [//]
           You can use this object to reset the internal record of the
           `fusion' interface, by passing in an empty interface.  This is
           important if you intend to use a single `kdu_push_pull_params'
           instance to construct multiple tile-components, since the
           `fusion' interface must be empty when constructing the first
           simultaneous fragment processor for a tile-component.
           [//]
           Alternatively, the `copy_params' function can be used to copy
           any other parameters from a master `kdu_push_pull_params' object
           to a local one that is used to construct just one tile-component's
           processing machinery.  This approach is better than resetting the
           master object's interfaces after constructing each tile-component.
           Note that `copy_params' only copies parameters, not interfaces
           that are for internal sharing amongst processing machinery.
      */
    void set_preferred_job_samples(int log2_min_samples,
                                   int log2_ideal_samples)
      { 
        this->log2_min_block_samples = log2_min_samples;
        this->log2_ideal_block_samples = log2_ideal_samples;
      }
      /* [SYNOPSIS]
         This function allows you to override the default internal policy
         used to determine the number of code-blocks that are grouped
         together into a single encoding/decoding job, for multi-threaded
         processing.  The supplied parameters are only guidelines, as to
         the packaging of code-blocks into jobs in each subband.
         [ARG: log2_min_samples]
         Indicates the minimum number of code-block samples that should
         (ideally) be found in a single processing job.  Attempting to
         enforce this lower bound may cause each encoding/decoding job to
         span an entire row of code-blocks for some or even all subbands.
         In this case, the lower bound might not be met, of course.
         [//]
         Typical values for `log2_min_samples' would be 10 to 12.  The
         default value is 12, corresponding to 4096 samples/job.
         [ARG: log2_ideal_samples]
         Indicates a preferred number of code-block samples that should
         (ideally) be found in a single processing job.  This parameter
         works together with the value passed to `set_min_jobs_across'
         to determine the actual number of code-blocks in each job.
         [//]
         If 2^B is the number of code-blocks in each job for some subband,
         2^S is the number of samples in each code-block, and A is the
         number of code-blocks that span the width of the subband, the
         block encoding/decoding machinery endeavours to choose the
         largest value of B, such that
         [>>] B+S ~ `log2_ideal_samples'; and
         [>>] 2^B * `min_jobs_across' <~ A
         [//]
         The value of B suggested by the above constraints is hard
         limited by the lower bound constraint:
         [>>] B >= max{0, `log2_min_samples' - S}
         [//]
         The idea is that putting more code-blocks into a single job
         reduces the impact job scheduling/launching overhead and
         improves cache utilization.  `log2_ideal_samples' represents
         the point beyond which this ceases to be of significant value, at
         which point it is always better to have more jobs in the system.
         Between `log2_min_samples' and `log2_ideal_samples' the
         actual number of code-blocks packed into a single job is adjusted
         in such a way as to try to provide at least `min_jobs_across'
         processing jobs across a single row of code-blocks within the
         subband.
         [//]
         Typical values for `log2_ideal_samples' would be 12 to 14.
         The default value is 14, corresponding to 16384 samples/job.
      */
    void set_min_jobs_across(int min_jobs_across)
      { this->min_block_jobs_across = min_jobs_across; }
      /* [SYNOPSIS]
         See `set_preferred_job_samples' for an explanation of how the
         `min_jobs_across' value is used.  If you only have a single
         tile-component in an image, you would typically set this value
         to the number of worker threads in the multi-threaded processing
         system, or possibly a larger value.  If you have a large number
         of tile-components, you may reduce this value, allowing the
         `log2_ideal_job_samples' limit passed to `set_preferred_job_samples'
         to be achieved in more subbands.
         [//]
         If you do not call this function, a value will be selected
         automatically based on the number of threads in the
         multi-threaded environment.
      */
    void set_max_block_stripes(int max_hires_stripes,
                               bool extra_lowres_stripe)
      { this->max_hires_block_stripes = max_hires_stripes;
        this->extra_lowres_block_stripes = (extra_lowres_stripe)?1:0; }
      /* [SYNOPSIS]
         You can use this function to override the default buffering
         provided by the `kdu_encoder' or `kdu_decoder' objects associated
         with each subband.  These objects provide one or more stripe
         buffers, where each stripe buffer can store a whole row of
         code-blocks for the relevant subband.  In practice, the maximum
         number of stripe buffers supported is 4.  In single-threaded
         applications, only one stripe buffer is ever allocated.  In
         most applications, 2 stripe buffers is sufficient, but if you
         have a very large number of processing threads, adding extra
         stripe buffers can help.  Of course, this can be very costly
         in terms of memory consumption.  For this reason, this function
         supplies an upper bound `max_hires_stripes' for the highest
         resolution subbands and, optionally, allows the lower resolution
         subbands to be allocated one extra stripe (`extra_lowres_stripe'),
         since these are smaller.
         [//]
         If code-blocks are small (e.g. 32x32), and you have a very large
         number of processing threads, you might want to consider using
         the largest value of 4 for both `max_hires_stripes'.  With 64x64
         code-blocks you would not normally select anything other than
         2 or 3 for `max_hires_stripes' due to the high associated memory
         cost, and `extra_lowres_stripe' might be true.
         [//]
         In heavily memory constrained applications you might select
         `max_hires_stripes'=1 and `extra_lowres_stripe'=true.
         [//]
         The default policy (if this function is never called) usually
         selects 2 stripes at the top level and 2 or 3 stripes for
         lower levels, where the cost of the extra stripe is not
         substantial.
      */
    void copy_params(const kdu_push_pull_params &src)
      { 
        this->spatial_xform_dbuf_rows = src.spatial_xform_dbuf_rows;
        this->num_component_frags = src.num_component_frags;
        this->cplex_share_ref = src.cplex_share_ref;
        this->cplex_bkgnd_ref = src.cplex_bkgnd_ref;
        this->log2_min_block_samples = src.log2_min_block_samples;
        this->log2_ideal_block_samples = src.log2_ideal_block_samples;
        this->min_block_jobs_across = src.min_block_jobs_across;
        this->max_hires_block_stripes = src.max_hires_block_stripes;
        this->extra_lowres_block_stripes = src.extra_lowres_block_stripes;
      }
      /* [SYNOPSIS]
           Copies any parameters stored in `src' to the current object, without
           copying interfaces that are for sharing between elements of the
           processing machinery.
      */
  public: // Member functions used by data processing objects internally
    void get_preferred_job_samples(int &log2_min_samples,
                                   int &log2_ideal_samples)
      { log2_min_samples = this->log2_min_block_samples;
        log2_ideal_samples = this->log2_ideal_block_samples; }
    int get_min_jobs_across()
      { return this->min_block_jobs_across; }
    int get_max_block_stripes(int &extra_lowres_stripes)
      { extra_lowres_stripes=this->extra_lowres_block_stripes;
        return this->max_hires_block_stripes; }
  private:
    kdu_push_ifc analysis_fusion_ifc;
    kdu_cplex_share *cplex_share_ref;
    kdu_cplex_bkgnd *cplex_bkgnd_ref;
    int spatial_xform_dbuf_rows;
    int num_component_frags;
    int log2_min_block_samples;
    int log2_ideal_block_samples;
    int min_block_jobs_across;
    int max_hires_block_stripes;
    int extra_lowres_block_stripes;
  };

/*****************************************************************************/
/*                              kdu_analysis                                 */
/*****************************************************************************/

class kdu_analysis : public kdu_push_ifc { 
  /* [BIND: reference]
     [SYNOPSIS]
     Implements the subband analysis processes associated with a single
     DWT node (see `kdu_node').  A complete DWT decomposition tree is built
     from a collection of these objects, each containing a reference to
     the next stage.   The complete DWT tree and all required `kdu_encoder'
     objects may be created by a single call to the constructor,
     `kdu_analysis::kdu_analysis'.
  */
  public: // Member functions
    KDU_EXPORT
      kdu_analysis(kdu_node node, kdu_sample_allocator *allocator,
                   kdu_push_pull_params &params, bool use_shorts,
                   float normalization=1.0F, int push_offset=0,
                   kdu_roi_node *roi=NULL, kdu_cplex_analysis *cplex=NULL,
                   kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL,
                   int extended_info=0);
      /* [SYNOPSIS]
           Constructing an instance of this class for the primary node of
           a tile-component's highest visible resolution will cause the
           constructor to recursively create instances of the class for
           each successive DWT stage and also for the block encoding process.
           [//]
           The recursive construction process supports all wavelet
           decomposition structures allowed by the JPEG2000 standard,
           including packet wavelet transforms, and transforms with
           different horizontal and vertical downsampling factors.  The
           `node' object used to construct the top level `kdu_analysis'
           object will typically be the primary node of a `kdu_resolution'
           object, obtained by calling `kdu_resolution::access_node'.  In
           fact, for backward compatibility with Kakadu versions 4.5 and
           earlier, a second constructor is provided, which does just this.
           [//]
           The optional `env' and `env_queue' arguments support a variety
           of multi-threaded processing paradigms, to leverage the
           capabilities of multi-processor platforms.  To see how this works,
           consult the description of these arguments below.
         [ARG: node]
           Interface to the DWT decomposition node for which the object is
           being created.  The analysis stage decomposes the image entering
           that node into one subband for each of the node's children.
           If the child node is a leaf (a final subband), a `kdu_encoder'
           object is created to receive the data produced in that subband.
           Otherwise, another `kdu_analysis' object is recursively
           constructed to process the subband data produced by the present
           node.
         [ARG: allocator]
           A `kdu_sample_allocator' object whose `finalize' member function
           has not yet been called must be supplied for pre-allocation of the
           various sample buffering resources.  This same allocator will be
           shared by the entire DWT tree and by the `kdu_encoder' objects at
           its leaves.  Apart from the interface itself, which is the size of
           a single pointer and normally embedded directly inside other
           objects and structures, all memory associated with the
           `kdu_analysis' machine and its descendants is allocated either
           from sample buffer storage or separately but with permission
           first sought by calls to `allocator->seek_permit'.  All such
           memory is ultimately governed by any `kdu_membroker' object that
           was supplied to `kdu_sample_allocator::configure'.
         [ARG: params]
           This argument is new to Kakadu version 7.11, providing an
           extensible method for sharing special configuration parameters with
           the internal machinery created for spatial transformation and
           coding, but also for sharing interfaces that link processing
           engines created for tile-components or for simultaneous processing
           fragments within a tile-component.
           [//]
           You should ensure that the interface returned by
           `params.get_analysis_fusion_interface' is empty before you
           construct the first processing fragment for a tile-component.  As
           explained with `kdu_push_pull_params::set_analysis_fusion_interface'
           one way to do this is to use `kdu_push_pull_params::copy_params' to
           create a copy of any master parameter set before constructing
           the processing machinery for each tile-component.
         [ARG: use_shorts]
           Indicates whether 16-bit or 32-bit data representations are to be
           used.  The same type of representation must be used throughput the
           DWT processing chain and line buffers pushed into the DWT engine
           must use this representation.
         [ARG: normalization]
           Ignored for reversibly transformed data.  In the irreversible case,
           it indicates that the nominal range of data pushed into the
           `kdu_push_ifc::push' function will be from -0.5*R to 0.5*R, where
           R is the value of the `normalization' argument.  This
           capability is provided primarily to allow normalization steps to
           be skipped or approximated with simple powers of 2 during lifting
           implementations of the DWT; the factors can be folded into
           quantization step sizes.  The best way to use the normalization
           argument will generally depend upon the implementation of the DWT.
         [ARG: push_offset]
           This argument is important to the implementation of simultaneous
           processing fragments, but may have other applications.  When a
           line is pushed into this object's `kdu_push_ifc::push' function,
           the first `push_offset' samples of that line are not actually
           relevant to the receiver.  During fragmented processing of a
           tile-component, the fragments correspond to (apparent) horizontal
           partitions in the subband domain, but to produce the fragments of
           a subband requires access to a wider collection of samples at each
           higher level in the decomposition hierarchy -- this is because
           spatial wavelet transforms involve overlapping analysis basis
           functions.  As a result, the apparent dimensions of a fragment
           at any non-leaf node, projected onto the coordinate system of any
           of its descendants (subbands) can be larger (wider) than the
           (apparent) dimensions of each descendant.  The extra samples on the
           left are identified when constructing the descendant's
           `kdu_analysis' or `kdu_encoder' object as the `push_offset'.
           [//]
           The total number of extra samples may (optionally) be encoded within
           the `extended_info' argument so as to allow the encoder to maximize
           the compatibility of any internal line buffer storage it provides
           for use with `kdu_line_buf::exchange'.
         [ARG: roi]
           If non-NULL, this argument points to an appropriately
           derived ROI node object, which may be used to recover region of
           interest mask information for the present tile-component.  In this
           case, the present function will automatically construct an ROI
           processing tree to provide access to derived ROI information in
           each individual subband.  The `roi::release' function will
           be called when the present object is destroyed -- possibly
           sooner (if it can be determined that ROI information is no
           longer required).
           [//]
           Note that simultaneous processing fragments (an important
           technology) are not currently compatible with region-of-interest
           encoding (a relatively unimportant technology, considering that
           region-of-interest is usually best to managed at the decoder side
           only).  For this reason, if your application does pass a non-NULL
           `roi' argument in here, you must ensure that each tile-component
           is processed using only one fragment -- this is best done by calling
           `kdu_codestream::configure_simultaneous_processing_fragments'
           explicitly, with zero-valued arguments, before attempting
           region-of-interest driven encoding.
         [ARG: cplex]
           This argument is provided so that a complexity-analysis interface
           can easily be passed down the analysis hierarchy into each
           `kdu_encoder' object, giving the subband encoding machinery an
           opportunity to participate in complexity analysis and reep the
           benefits of complexity-constrained encoding.  Complexity-constrained
           encoding is currently only of interest when used with the HT
           (High Throughput) block encoder -- see the `Cmodes' attribute
           that is documented with `cod_params'.
         [ARG: env]
           Supply a non-NULL argument here if you want to enable
           multi-threaded processing.  After creating a cooperating
           thread group by following the procedure outlined in the
           description of `kdu_thread_env', any one of the threads may
           be used to construct this processing engine.  However, you
           MUST TAKE CARE to create all objects which share the same
           `allocator' object from the same thread.
           [//]
           Separate processing queues will automatically be created for each
           subband, allowing multiple threads to be scheduled
           simultaneously to process code-block data for the corresponding
           tile-component.  Also, multiple tile-components may be processed
           concurrently and the available thread resources will be allocated
           amongst the total collection of job queues as required.
           [//]
           By and large, you can use this object in exactly the same way
           when `env' is non-NULL as you would with a NULL `env' argument.
           That is, the use of multi-threaded processing can be largely
           transparent.  However, you must remember the following three
           points:
           [>>] All objects which share the same `allocator' must be created
                using the same thread.  Thereafter, the actual processing
                may proceed on different threads.
           [>>] You must supply a non-NULL `env' argument to the
                `kdu_push_ifc::push' function -- it need not refer to the
                same thread as the one used to create the object here, but
                it must belong to the same thread group.
           [>>] You cannot rely upon all processing being complete until you
                invoke the `kdu_thread_entity::join' or
                `kdu_thread_entity::terminate' function.
         [ARG: env_queue]
           This argument is ignored unless `env' is non-NULL.  When `env'
           is non-NULL, all thread queues which are created inside this object
           are added as sub-queues of `env_queue'.  If `env_queue' is NULL,
           all thread queues which are created inside this object are added
           as top-level queues in the multi-threaded queue hierarchy.  The
           `kdu_analysis' object does not directly create any
           `kdu_thread_queue' objects, but it passes `env_queue' along to
           the `kdu_encoder' objects that it constructs to process each
           transformed subband and each of those objects does create a
           thread queue which is made a descendant of `env_queue'.
           [//]
           One advantage of supplying a non-NULL `env_queue' is that it
           provides you with a single hook for joining with the completion
           of all the thread queues which are created by this object
           and its descendants -- see `kdu_thread_entity::join' for more on
           this.
           [//]
           A second advantage of supplying a non-NULL `env_queue' is that
           it allows you to manipulate the sequencing indices that are
           assigned by the thread queues created internally -- see
           `kdu_thread_entity::attach_queue' for more on the role played by
           sequencing indices in controlling the order in which work is
           actually done.  In particular, if `env_queue' is initially
           added to the thread group with a sequencing index of N >= 0, each
           `kdu_encoder' object created as a result of the present call will
           also have a sequencing index of N.
           [//]
           Finally, and perhaps most importantly, the `env_queue' object
           supplied here provides a mechanism to determine whether or not
           calls to `push' might potentially block.  This is achieved by
           means of the `env_queue->update_dependencies' function that is
           invoked from within the `kdu_encoder' object, following the
           conventions outlined with the definition of the `kdu_encoder'
           object's constructor.  What this means is that if
           `env_queue->check_dependencies' returns false, the next call to
           this object's `kdu_push_ifc::push' function should not block the
           caller.  The `kdu_multi_analysis' object uses a derived
           `kdu_thread_queue' object to automatically schedule DWT analysis
           jobs only once it knows that they will not be blocked by missing
           dependencies.
         [ARG: extended_info]
           Used to provide extended information to the encoder that may
           facilitate internal optimization.  The information carried by this
           argument may be extended in the future.  Currently, however, the
           following bit fields are defined:
           [>>] A multi-bit field, masked by macro `KDU_LINE_EXTRA_WIDTH_MASK',
                whose 1st bit position is at `KDU_LINE_EXTRA_WIDTH_POS',
                may be used to identify the total amount by which the width of
                lines pushed to the encoder exceed the width of the subband,
                as returned by `kdu_subband::get_dims'.  That is, the
                `kdu_line_buf::get_width' function should return a width that
                is at least as large as the subband width plus the extra
                width encoded here, when applied to the `kdu_line_buf'
                objects passed across `kdu_push_ifc::push' calls.  If non-zero,
                this extra width should include the `push_offset'.  Note that
                the use of simultaneous processing fragments is likely to
                result in pushed lines that are wider than the actual subband
                by an amount that is related to the support of the underlying
                spatial transform analysis operators.  The maximum extra
                width that can be encoded within the `extended)_info' argument
                is given by the `KDU_LINE_EXTRA_WIDTH_MAX' macro -- if the
                caller uses even larger widths for some reason, it cannot pass
                this information to the encoder, but the passing of such
                information is only for optimization purposes.  In particular,
                the extended information here allows the encoder to allocate
                internal `kdu_line_buf' objects in such a way that they are
                likely to be exchangeable with the lines pushed to the encoder,
                using `kdu_line_buf::exchange' -- the 1-bit flag below is
                provided to further increases the likelihood of a successful
                exchange.
           [>>] `KDU_LINE_WILL_BE_EXTENDED' -- if this flag is defined,
                the caller is intending to push in lines that have been
                created with an `extend_right' value of 1.  That is,
                `kdu_line_buf' objects supplied to the `push' function will
                have an extra sample beyond the nominal end of the
                line in question.  Knowing this, the current object may
                allocate internal storage to have the same extended length
                so as to maximize the chance that internal calls to
                `kdu_line_buf::exchange' will succeed in performing an
                efficient data exchange without copying of sample values.
                This flag is provided primarily to allow efficient DWT
                implementations to work with buffers that have an equal
                amount of storage for low- and high-pass horizontal subbands.
                In practice, the flag will only be set for horizontal low
                (resp. high) subbands that are shorter (by 1) than the
                corresponding horizontal high (resp. low) subband, where
                the longer subband cannot be spanned by the same number
                of octets as the shorter subband; this is a relatively unusual
                condition, but still worth catering for. The application
                itself would not normally set this flag.
      */
    KDU_EXPORT
      kdu_analysis(kdu_resolution resolution, kdu_sample_allocator *allocator,
                   kdu_push_pull_params &params,
                   bool use_shorts, float normalization=1.0,
                   int push_offset=0, kdu_roi_node *roi=NULL,
                   kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL);
      /* [SYNOPSIS]
           Same as the first form of the constructor, but the required
           `kdu_node' interface is recovered from `resolution.access_node'.
           [//]
           You are STRONGLY DISCOURAGED from using this version of the
           constructor, because it does not support simultaneous processing
           fragments, which are an important technology for encoding.  All
           `kdu_analysis' construction inside `kdu_multi_analysis' is done
           using only the first form of the constructor, whose `kdu_node'
           interface may identify a specific processing fragment.
         [ARG: resolution]
           Interface to the top visible resolution level from which
           DWT analysis is to be performed.  For analysis, this should almost
           invariably be the actual top level resolution of a tile-component.
           For synthesis, the corresponding constructor might be supplied
           a lower resolution object in order to obtain partial synthesis
           to that resolution.
      */
  };

/*****************************************************************************/
/*                           kdu_analysis_fusion                             */
/*****************************************************************************/

class kdu_analysis_fusion : public kdu_push_ifc { 
  /* [BIND: reference]
     [SYNOPSIS]
     Used in the compression processing hierarchy for a tile-component that
     has multiple processing fragments, at the resolution where fragmentation
     is stopped.  Processing fragments are described with the function
     `kdu_codestream::configure_processing_fragments'.  In general, all
     subbands above some point in the hierarchy are partitioned into an
     identical number of processing fragments for the benefit of enhanced
     multi-threaded parallelism, but at some point in the hierarchy this
     usually has to stop, since (for efficiency reasons) each fragment should
     correspond to a whole number of code-blocks in any fragmented subband.
     When it does stop, the analysis machinery needs to push lines from
     multiple horizontal fragments into the next resolution level, which
     has synchronization and other implications for multi-threaded processing.
     For this reason, at that level, a `kdu_analysis_fusion' object should
     be used, whose line pushing interface is capable of fusing together
     the multiple fragments, with some additional buffering to minimize the
     impact on throughput of synchronization-related dependencies.
     [//]
     `kdu_analysis_fusion' objects have two parts: an external (upward facing)
     interface which is different for each fragment and allows the fragment
     associated with a push call to be identified; and an internal object that
     is common to all fragments.  The internal object is destroyed only once
     all external fragment interfaces have been destroyed.  A separate
     `kdu_thread_queue'is created inside the internal object to collect calls
     from `kdu_encoder' objects to `kdu_thread_queue::update_dependencies' and
     massage them into upward dependency updates to each processing fragment's
     `kdu_thread_queue', as supplied in the constructors for each upward
     facing `kdu_analysis_fusion' interface, taking into account the progress
     that each such external push interface has made and the available
     internal buffering resources.
     [//]
     In general, `kdu_analysis_fusion' objects introduce a delay between the
     arrival of `kdu_push_ifc::push' calls on the upward facing interface
     and the pushing of fused samples to the downward facing `kdu_push_ifc'
     interface.  In multi-threaded environments, it is possible that all
     upward facing interfaces deliver their fragments of a given subband line
     well before the point at which the fused line is delivered to the downward
     facing interface - this can happen if the lower level interface is blocked
     waiting for subband encoding operations to complete, so that lines that
     are ready to be fused cannot be delivered immediately.  This means that
     it is possible that `kdu_push_ifc::push' calls arrive in objects deeper
     in the processing hierarchy after `kdu_thread_queue::request_termination'
     is used to request early termination of processing -- above the fusion
     object, however, this cannot happen, because DWT analysis jobs inside
     `kdu_multi_analysis' must have stopped and be no longer schedulable
     before termination requests will be delivered to descendant thread
     queues.  It is important that implementations of `kdu_encoder' and
     similar objects be prepared, therefore, for the new possibility that
     `push' calls arrive after an early termination request in a multi-threaded
     environment.
     [//]
     To avoid the possibility, in multi-threaded environments, that the delay
     mentioned here results in calls to `kdu_push_ifc::finish' arriving before
     all calls to `kdu_push_ifc::push' have completed, it is sufficient to
     ensure that the final call to the downward facing interface's `push'
     function is executed from within one of the upward facing interface's
     `push' calls.  Usually, downward `push' calls are invoked from within
     upward facing `push' calls, but it is possible that a downward `push'
     call is executed from within a block encoding job that completes and
     makes available sufficient resources to remove a dependency that was
     preventing the otherwise ready downward `push' call.  To avoid any such
     possibility, the internal implementation can treat specially the case in
     which the number of remaining lines to be pushed within any upward
     facing fragment interface reaches 1.  When this happens, the
     implementation can behave as if the interface were blocked, meaning that
     incoming calls must enter a working-wait, and
     `kdu_thread_queue::propagate_dependencies' is used to advertise a
     dependency that should prevent such calls arriving from any higher
     level thread queue.
  */
  public: // Member functions
    KDU_EXPORT
      kdu_analysis_fusion(kdu_node node, kdu_sample_allocator *allocator,
                          kdu_push_pull_params &params,
                          bool use_shorts, float normalization,
                          int push_offset, int push_width,
                          int invalid_extent_left, int invalid_extent_right,
                          kdu_roi_node *roi=NULL,
                          kdu_cplex_analysis *cplex=NULL,
                          kdu_thread_env *env=NULL,
                          kdu_thread_queue *env_queue=NULL,
                          int extended_info=0);
      /* [SYNOPSIS]
           The arguments here all have essentially the same interpretation as
           their namesakes in `kdu_analysis', except that there is an extra
           `push_width' argument, explained below.  The constructor uses the
           `params.get_analysis_fusion_interface' function to find any existing
           fusion interface, with which the internal implementation of this
           object needs to be shared, and it uses the
           `params.set_analysis_fusion_interface' function to share any
           interface that is created here with other fragments.
         [ARG: push_offset]
           Unlike `kdu_analysis', the `push_offset' argument to this function
           might be negative -- in fact it should be negative for non-initial
           fragments.  To understand this, note that `push_offset' should be
           equal to `x_min_node' minus `x_min_push', where `x_min_push' is the
           absolute (apparent) x coordinate of the first sample in each pushed
           line and `x_min_node' is the absolute (apparent) x coordinate of the
           first sample in the region returned by `node.get_dims'.  Normally,
           `x_min_push' <= `x_min_node' so that `push_offset' >= 0; however,
           if the caller belongs to a non-initial fragment, `x_min_push' will
           usually be much larger than `x_min_node'.  The fusion interface
           can use the `push_offset' values to work out how to fuse the line
           fragments that it receives.
         [ARG: push_width]
           This is the width of the lines that the constructed object can
           expect to receive across its `kdu_push_ifc::push' interface.
         [ARG: invalid_extent_left]
           This is the number of initial samples of each line pushed across the
           `kdu_push_ifc::push' interface that might not be completely
           accurate, unless this is the first fragment.  In practice, the
           caller derives this value from the support of its horizontal
           analysis operators, taking sub-sampling and sample parity into
           account.  For the left-most fragment, this value may be non-zero,
           but should be ignored -- all values on the left are valid.
         [ARG: invalid_extent_right]
           This is the number of final samples of each line pushed across
           the `kdu_push_ifc::push' interface that might not be completely
           accurate, unless this is the last fragment.  In practice, the
           caller derives this value from the support of its horizontal
           analysis operators, taking sub-sampling and sample parity into
           account.  For the right-most fragment, this value may be non-zero,
           but should be ignored -- all values on the right are valid.
           [//]
           After ignoring the invalid extents associated with boundary
           fragments (first and last) as indicated above, you can be quite
           sure that the sum of the left and right invalid extents will
           be smaller than `push_width'.
         [ARG: cplex]
           This argument is provided so that a complexity-analysis interface
           can easily be passed down the analysis hierarchy into each
           `kdu_encoder' object, giving the subband encoding machinery an
           opportunity to participate in complexity analysis and reep the
           benefits of complexity-constrained encoding.  Complexity-constrained
           encoding is currently only of interest when used with the HT
           (High Throughput) block encoder -- see the `Cmodes' attribute
           that is documented with `cod_params'.
           [//]
           In practice, the `cplex' object should be different for each
           fragment, but the first fragment to construct this object creates
           the underlying fusion machinery that is common to all fragments
           of a tile-component, along with an internal
           `kdu_cplex_analysis' object that linked to `cplex' via
           `cplex->link_descendant' and passed to all subband encoders further
           down the hierarchy below the fusion point.
      */
  };

/*****************************************************************************/
/*                              kdu_synthesis                                */
/*****************************************************************************/

class kdu_synthesis : public kdu_pull_ifc { 
  /* [BIND: reference]
     [SYNOPSIS]
     Implements the subband synthesis processes associated with a single
     DWT node (see `kdu_node').  A complete DWT synthesis tree is built
     from a collection of these objects, each containing a reference to
     the next stage.   The complete DWT tree and all required `kdu_decoder'
     objects may be created by a single call to the constructor,
     `kdu_synthesis::kdu_synthesis'.
  */
  public: // Member functions
    KDU_EXPORT
      kdu_synthesis(kdu_node node, kdu_sample_allocator *allocator,
                    kdu_push_pull_params &params, bool use_shorts,
                    float normalization=1.0F, int pull_offset=0,
                    kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL,
                    int extended_info=0);
      /* [SYNOPSIS]
           Constructing an instance of this class for the primary node of
           a tile-component's highest visible resolution, will cause the
           constructor to recursively create instances of the class for
           each successive DWT stage and also for the block decoding process.
           [//]
           The recursive construction process supports all wavelet
           decomposition structures allowed by the JPEG2000 standard,
           including packet wavelet transforms, and transforms with
           different horizontal and vertical downsampling factors.  The
           `node' object used to construct the top level `kdu_synthesis'
           object will typically be the primary node of a `kdu_resolution'
           object, obtained by calling `kdu_resolution::access_node'.  In
           fact, for backward compatibility with Kakadu versions 4.5 and
           earlier, a second constructor is provided, which does just this.
           [//]
           This function takes optional `env' and `env_queue' arguments to
           support a variety of multi-threaded processing paradigms, to
           leverage the capabilities of multi-processor platforms.  To see
           how this works, consult the description of these arguments below.
           To initiate processing as soon as possible, you might like to
           call `kdu_pull_ifc::start' once you have finished creating all
           objects which share the supplied `allocator' object and invoked
           its `kdu_sample_allocator::finalize' function.  Otherwise,
           background processing (on other threads) will not commence until
           the first call to `kdu_pull_ifc::pull'.
         [ARG: node]
           Interface to the DWT decomposition node for which the object is
           being created.  The synthesis stage reconstructs the image
           associated with that node by combining the subband images
           produced by each of the node's children.  If the child node is
           a leaf (a final subband), a `kdu_decoder' object is created to
           recover the data for that subband.  Otherwise, another
           `kdu_synthesis' object is recursively constructed to retrieve
           the child's subband data.
         [ARG: allocator]
           A `kdu_sample_allocator' object whose `finalize' member function
           has not yet been called must be supplied for pre-allocation of the
           various sample buffering resources.  This same allocator will be
           shared by the entire DWT tree and by the `kdu_decoder' objects at
           its leaves.  Apart from the interface itself, which is the size of
           a single pointer and normally embedded directly inside other
           objects and structures, all memory associated with the
           `kdu_synthesis' machine and its descendants is allocated either
           from sample buffer storage or separately but with permission
           first sought by calls to `allocator->seek_permit'.  All such
           memory is ultimately governed by any `kdu_membroker' object that
           was supplied to `kdu_sample_allocator::configure'.
         [ARG: params]
           This argument is new to Kakadu version 7.11, providing an
           extensible method for sharing special configuration parameters with
           the internal machinery created for inverse transformation and
           decoding, but also for sharing interfaces that link processing
           engines created for tile-components or for simultaneous processing
           fragments within a tile-component.
         [ARG: use_shorts]
           Indicates whether 16-bit or 32-bit data representations are to be
           used.  The same type of representation must be used throughput the
           DWT processing chain and line buffers pulled from the DWT synthesis
           engine must use this representation.
         [ARG: normalization]
           Ignored for reversibly transformed data.  In the irreversible case,
           it indicates that the nominal range of data recovered from the
           `kdu_pull_ifc::pull' function will be from -0.5*R to 0.5*R, where
           R is the value of the `normalization' argument.  This
           capability is provided primarily to allow normalization steps to
           be skipped or approximated with simple powers of 2 during lifting
           implementations of the DWT; the factors can be folded into
           quantization step sizes.  The best way to use the normalization
           argument will generally depend upon the implementation of the DWT.
         [ARG: pull_offset]
           Applications should leave this argument set to 0.  The internal
           implementation uses this to maintain horizontal alignment
           properties for efficient memory access, when synthesizing a
           region of interest within the image.  The first `pull_offset'
           entries in each `kdu_line_buf' object supplied to the `pull'
           function are not used; the function should write the requested
           sample values into the remainder of the line buffer, whose
           width (`kdu_line_buf::get_width') is guaranteed to be `pull_offset'
           samples larger than the width of the region in that subband.
           In any event, offsets should be small, since the internal
           representation stores them and various derived quantities
           using 8-bit fields to keep the memory footprint as small
           as possible.
         [ARG: env]
           Supply a non-NULL argument here if you want to enable
           multi-threaded processing.  After creating a cooperating
           thread group by following the procedure outlined in the
           description of `kdu_thread_env', any one of the threads may
           be used to construct this processing engine.  However, you
           MUST TAKE CARE to create all objects which share the same
           `allocator' object from the same thread.
           [//]
           Separate processing queues will automatically be created for each
           subband, allowing multiple threads to be scheduled simultaneously
           to process code-block samples for the corresponding
           tile-component.  Also, multiple tile-components may be processed
           concurrently and the available thread resources will be allocated
           amongst the total collection of job queues as required.
           [//]
           By and large, you can use this object in exactly the same way
           when `env' is non-NULL as you would with a NULL `env' argument.
           That is, the use of multi-threaded processing can be largely
           transparent.  However, you must remember the following two
           points:
           [>>] All objects which share the same `allocator' must be created
                using the same thread.  Thereafter, the actual processing
                may proceed on different threads.
           [>>] You must supply a non-NULL `env' argument to the
                `kdu_push_ifc::push' function -- it need not refer to the
                same thread as the one used to create the object here, but
                it must belong to the same thread group.
         [ARG: env_queue]
           This argument is ignored unless `env' is non-NULL.  When `env'
           is non-NULL, all job queues which are created inside this object
           are added as sub-queues of `env_queue'.  If `env_queue' is NULL,
           all thread queues which are created inside this object are added
           as top-level queues in the multi-threaded queue hierarchy.  The
           `kdu_synthesis' object does not directly create any
           `kdu_thread_queue' objects, but it passes `env_queue' along to
           the `kdu_decoder' objects that it constructs to decode each
           subband and each of those objects does create a
           thread queue which is made a descendant of `env_queue'.
           [//]
           One advantage of supplying a non-NULL `env_queue' is that it
           provides you with a single hook for joining with the completion
           of all the thread queues which are created by this object
           and its descendants -- see `kdu_thread_entity::join' for more on
           this.
           [//]
           A second advantage of supplying a non-NULL `env_queue' is that
           it allows you to manipulate the sequencing indices that are
           assigned by the thread queues created internally -- see
           `kdu_thread_entity::attach_queue' for more on the role played by
           sequencing indices in controlling the order in which work is
           actually done.  In particular, if `env_queue' is initially
           added to the thread group with a sequencing index of N >= 0, each
           `kdu_decoder' object created as a result of the present call will
           also have a sequencing index of N.
           [//]
           Finally, and perhaps most importantly, the `env_queue' object
           supplied here provides a mechanism to determine whether or not
           calls to `pull' might potentially block.  This is achieved by
           means of the `env_queue->update_dependencies' function that is
           invoked from within the `kdu_decoder' object, following the
           conventions outlined with the definition of the `kdu_decoder'
           object's constructor.  What this means is that if
           `env_queue->check_dependencies' returns false, the next call to
           this object's `kdu_pull_ifc::pull' function should not block the
           caller.  The `kdu_multi_synthesis' object uses a derived
           `kdu_thread_queue' object to automatically schedule DWT synthesis
           jobs only once it knows that they will not be blocked by missing
           dependencies.
         [ARG: extended_info]
           Used to provide extended information to the encoder that may
           facilitate internal optimization.   Currently, only one flag is
           defined for this use, as follows:
           [>>] `KDU_LINE_WILL_BE_EXTENDED' -- if this flag is defined,
                the caller is intending to pull data into line buffers that
                have been created with an `extend_right' value of 1.  That is,
                `kdu_line_buf' objects supplied to the `pull' function will
                have an extra sample beyond the nominal end of the
                line in question.  Knowing this, the current object may
                allocate internal storage to have the same extended length
                so as to maximize the chance that internal calls to
                `kdu_line_buf::exchange' will succeed in performing an
                efficient data exchange without copying of sample values.
                This flag is provided primarily to allow efficient DWT
                implementations to work with buffers that have an equal
                amount of storage for low- and high-pass horizontal subbands.
                In practice, the flag will only be set for horizontal low
                (resp. high) subbands that are shorter (by 1) than the
                corresponding horizontal high (resp. low) subband, where
                the longer subband cannot be spanned by the same number
                of octets as the shorter subband; this is a relatively unusual
                condition, but still worth catering for. The application
                itself would not normally set this flag.
      */
    KDU_EXPORT
      kdu_synthesis(kdu_resolution resolution, kdu_sample_allocator *allocator,
                    kdu_push_pull_params &params, bool use_shorts,
                    float normalization=1.0F, kdu_thread_env *env=NULL,
                    kdu_thread_queue *env_queue=NULL);
      /* [SYNOPSIS]
           Same as the first form of the constructor, but the required
           `kdu_node' interface is recovered from `resolution.access_node'.
         [ARG: resolution]
           Interface to the top visible resolution level to which DWT synthesis
           is to be performed.  This need not necessarily be the
           highest available resolution, so that partial synthesis to some
           lower resolution is supported -- in fact, common in Kakadu.
      */
  };

/*****************************************************************************/
/*                              kdu_encoder                                  */
/*****************************************************************************/

class kdu_encoder: public kdu_push_ifc { 
    /* [BIND: reference]
       [SYNTHESIS]
       Implements quantization and block encoding for a single subband,
       inside a single tile-component.
    */
  public: // Member functions
    KDU_EXPORT
      kdu_encoder(kdu_subband subband, kdu_sample_allocator *allocator,
                  kdu_push_pull_params &params,
                  bool use_shorts, float normalization=1.0F,
                  int push_offset=0, kdu_roi_node *roi=NULL,
                  kdu_cplex_analysis *cplex=NULL,
                  kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL,
                  int extended_info=0, float *prequant_scale=NULL);
      /* [SYNOPSIS]
           Constructs an instance of this class for a specific subband;
           in cases where there is no wavelet transform, this is also the
           primary node of a tile-component's data processing hierarchy;
           in all other cases, it is constructed within an instance of
           `kdu_analysis'.
           [//]
           The optional `env' and `env_queue' arguments support a variety
           of multi-threaded processing paradigms, to leverage the
           capabilities of multi-processor platforms.  To see how this works,
           consult the description of these arguments below.
         [ARG: allocator]
           A `kdu_sample_allocator' object whose `finalize' member function
           has not yet been called must be supplied for pre-allocation of the
           various sample buffering resources.  Apart from the interface
           itself, which is the size of a single pointer and normally embedded
           directly inside other objects and structures, all memory associated
           with the `kdu_encoder' machine is allocated either from sample
           buffer storage or separately but with permission first sought by
           calls to `allocator->seek_permit'.  All such memory is ultimately
           governed by any `kdu_membroker' object that was supplied to
           `kdu_sample_allocator::configure'.
         [ARG: params]
           This argument is new to Kakadu version 7.11, providing an
           extensible method for sharing special configuration parameters with
           the internal machinery created for spatial transformation and
           coding.
         [ARG: normalization]
           Informs the encoder that data supplied via its `kdu_push_ifc::push'
           function will have a nominal range from -0.5*R to +0.5*R where R
           is the value of `normalization'.
         [ARG: push_offset]
           This argument is important to the implementation of simultaneous
           processing fragments, but may have other applications.  When a
           line is pushed into this object's `kdu_push_ifc::push' function,
           the first `push_offset' samples of that line are not actually
           relevant to the receiver.  During fragmented processing of a
           tile-component, the fragments correspond to (apparent) horizontal
           partitions in the subband domain, but to produce the fragments of
           a subband requires access to a wider collection of samples at each
           higher level in the decomposition hierarchy -- this is because
           spatial wavelet transforms involve overlapping analysis basis
           functions.  As a result, the apparent dimensions of a fragment
           at any non-leaf node, projected onto the coordinate system of any
           of its descendants (subbands) can be larger (wider) than the
           (apparent) dimensions of each descendant.  The extra samples on the
           left are identified when constructing the descendant's
           `kdu_analysis' or `kdu_encoder' object as the `push_offset'.
           [//]
           The total number of extra samples may (optionally) be encoded within
           the `extended_info' argument so as to allow the encoder to maximize
           the compatibility of any internal line buffer storage it provides
           for use with `kdu_line_buf::exchange'.
         [ARG: roi]
           The `roi' argument, if non-NULL, provides an appropriately derived
           `kdu_roi_node' object whose `kdu_roi_node::pull' function may be
           used to recover ROI mask information for this subband.  Its
           `kdu_roi::release' function will be called when the encoder is
           destroyed -- possibly sooner, if it can be determined that ROI
           information is no longer required.
           [//]
           As explained with the first form of the `kdu_analysis' constructor,
           region-of-interest encoding is not compatible with the use of the
           much more important simultaneous processing fragments technology.
         [ARG: cplex]
           If non-NULL, this argument provides the encoding machinery for the
           current subband an opportunity to participate in complexity analysis
           and reep the benefits of complexity-constrained encoding.
           Complexity-constrained encoding is currently only of interest when
           used with the HT (High Throughput) block encoder -- see the `Cmodes'
           attribute that is documented with `cod_params'.  To take advantage
           of the opportunity, the implementation invokes `cplex->attach'.
         [ARG: env]
           If non-NULL, the behaviour of the underlying
           `kdu_push_ifc::push' function is changed radically.  In
           particular, a job queue is created by this constructor, to enable
           asynchronous multi-threaded processing of the code-block samples.
           Once sufficient lines have been pushed to the subband to enable
           the encoding of a row of code-blocks, the processing of these
           code-blocks is not done immediately, as it is if `env' is NULL.
           Instead, one or more jobs are added to the mentioned queue,
           to be serviced by any available thread in the group to which
           `env' belongs.  You should remember the following three
           points:
           [>>] All objects which share the same `allocator' must be created
                using the same thread.  Thereafter, the actual processing
                may proceed on different threads.
           [>>] You must supply a non-NULL `env' argument to the
                `kdu_push_ifc::push' function -- it need not refer to the
                same thread as the one used to create the object here, but
                it must belong to the same thread group.
           [>>] You cannot rely upon all processing being complete until you
                invoke the `kdu_thread_entity::join' or
                `kdu_thread_entity::terminate' function.
         [ARG: env_queue]
           If `env' is NULL, this argument is ignored; otherwise, the job
           queue which is created by this constructor will be made a
           sub-queue of any supplied `env_queue'.  If `env_queue' is NULL,
           the queue created to process code-blocks within this
           tile-component-subband will be a top-level queue in the
           multi-threaded queue hierarchy.
           [//]
           The `env_queue->update_dependencies' function is invoked with
           a `new_dependencies' value of 1 whenever a call to `push'
           causes this object's internal subband sample buffer to become
           full, so that a subsequent call to `push' might require the caller
           to block until the buffer has been cleared by block encoding
           operations.  The `env_queue->update_dependencies' function is
           invoked with a `new_dependencies' value of -1 whenever block
           encoding operations cause a previously full subband sample buffer
           to become available to receive new data, so that a subsequent
           call to `push' will not block the caller.
           [//]
           For more on the role and benefits of the `env_queue' argument,
           see the discussion of this argument's namesake within the
           `kdu_analysis' constructor, as well as the discussion that appears
           with the definition of `kdu_thread_queue::update_dependencies'.
         [ARG: extended_info]
           Used to provide extended information to the encoder that may
           facilitate internal optimization.  The information carried by this
           argument may be extended in the future.  Currently, however, the
           following bit fields are defined:
           [>>] A multi-bit field, masked by macro `KDU_LINE_EXTRA_WIDTH_MASK',
                whose 1st bit position is at `KDU_LINE_EXTRA_WIDTH_POS',
                may be used to identify the total amount by which the width of
                lines pushed to the encoder exceed the width of the subband,
                as returned by `kdu_subband::get_dims'.  That is, the
                `kdu_line_buf::get_width' function should return a width that
                is at least as large as the subband width plus the extra
                width encoded here, when applied to the `kdu_line_buf'
                objects passed across `kdu_push_ifc::push' calls.  If non-zero,
                this extra width should include the `push_offset'.  Note that
                the use of simultaneous processing fragments is likely to
                result in pushed lines that are wider than the actual subband
                by an amount that is related to the support of the underlying
                spatial transform analysis operators.  The maximum extra
                width that can be encoded within the `extended)_info' argument
                is given by the `KDU_LINE_EXTRA_WIDTH_MAX' macro -- if the
                caller uses even larger widths for some reason, it cannot pass
                this information to the encoder, but the passing of such
                information is only for optimization purposes.  In particular,
                the extended information here allows the encoder to allocate
                internal `kdu_line_buf' objects in such a way that they are
                likely to be exchangeable with the lines pushed to the encoder,
                using `kdu_line_buf::exchange' -- the 1-bit flag below is
                provided to further increases the likelihood of a successful
                exchange.
           [>>] A 1-bit flag, given by the macro `KDU_LINE_WILL_BE_EXTENDED',
                may be set if the caller is intending to push in line buffers
                that have been created with an `extend_right' value of 1.  That
                is, `kdu_line_buf' objects supplied to the `push' function will
                have an extra sample beyond the nominal end of the
                line in question.  Knowing this, the current object may
                allocate internal storage to have the same extended length
                so as to maximize the chance that internal calls to
                `kdu_line_buf::exchange' will succeed in performing an
                efficient data exchange without copying of sample values.
                This flag is provided primarily to allow efficient DWT
                implementations to work with buffers that have an equal
                amount of storage for low- and high-pass horizontal subbands.
                In practice, the flag will only be set for horizontal low
                (resp. high) subbands that are shorter (by 1) than the
                corresponding horizontal high (resp. low) subband, where
                the longer subband cannot be spanned by the same number
                of octets as the shorter subband; this is a relatively unusual
                condition, but still worth catering for. The application
                itself would not normally set this flag.
         [ARG: prequant_scale]
           If non-NULL, this argument should reference a variable that is
           initialized to a non-positive value.  The argument is relevant
           only when `use_shorts' is false and the data processing path is
           irreversible, so that floating point sample values would normally
           be expected to arrive via the `kdu_push_ifc::push' interface.
           [//]
           If the subband encoder object is willing to store the floating
           point subband samples as pre-quantized 16-bit integers, saving 50%
           of the storage and (potentially) external memory bandwidth, it may
           set the value at `prequant_scale' equal to the reciprocal of the
           quantization step size (taking `normalization' into account).
           [//]
           If the caller (almost certainly a DWT analysis object) finds
           a positive value at `prequant_scale' upon return, it is obliged to
           pre-multiply the floating point sample values by the value at
           `prequant_scale', round the result (towards zero) to 2's
           complement 16-bit sample values, and pass `kdu_line_buf' objects
           that identify themselves as holding 16-bit absolute integers
           to the `kdu_push_ifc::push' interface.
           [//]
           Even without this argument, the internal encoder implementation is
           at liberty to perform its own conversions to an internal 16-bit
           representation from floats, but this might not be efficient.  The
           reason is that a DWT analysis engine is in a position to perform
           the conversion on-the-fly, potentially avoiding a high volume
           of external memory transactions.
           [//]
           Note that the "pre-quant" feature can be used if the number of
           magnitude bit-planes for the subband is certain to be 15 or less.
      */
  };

/*****************************************************************************/
/*                              kdu_decoder                                  */
/*****************************************************************************/

class kdu_decoder: public kdu_pull_ifc { 
    /* [BIND: reference]
       [SYNOPSIS]
       Implements the block decoding for a single subband, inside a single
       tile-component.
    */
  public: // Member functions
    KDU_EXPORT
      kdu_decoder(kdu_subband subband, kdu_sample_allocator *allocator,
                  kdu_push_pull_params &params, bool use_shorts,
                  float normalization=1.0F, int pull_offset=0,
                  kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL,
                  int extended_info=0, float *postquant_scale=NULL);
      /* [SYNOPSIS]
           Constructs an instance of this class for a specific subband;
           in cases where there is no wavelet transform, this is also the
           primary node of a tile-component's data processing hierarchy;
           in all other cases, it is constructed within an instance of
           `kdu_synthesis'.
           [//]
           The optional `env' and `env_queue' arguments support a variety
           of multi-threaded processing paradigms, to leverage the
           capabilities of multi-processor platforms.  To see how this works,
           consult the description of these arguments below.
         [ARG: allocator]
           A `kdu_sample_allocator' object whose `finalize' member function
           has not yet been called must be supplied for pre-allocation of the
           various sample buffering resources.  Apart from the interface
           itself, which is the size of a single pointer and normally embedded
           directly inside other objects and structures, all memory associated
           with the `kdu_decoder' machine is allocated either from sample
           buffer storage or separately but with permission first sought by
           calls to `allocator->seek_permit'.  All such memory is ultimately
           governed by any `kdu_membroker' object that was supplied to
           `kdu_sample_allocator::configure'.
         [ARG: params]
           This argument is new to Kakadu version 7.11, providing an
           extensible method for sharing special configuration parameters with
           the internal machinery created for inverse transformation and
           decoding.
         [ARG: normalization]
           Informs the decoder that data retrieved via its `kdu_pull_ifc::pull'
           function should have a nominal range from -0.5*R to +0.5*R, where
           R is the value of `normalization'.
         [ARG: pull_offset]
           The `pull_offset' member should be left as zero when invoking
           this constructor directly from an application.  Internally,
           however, when a `kdu_decoder' object must be constructed within
           a `kdu_synthesis' object, the `pull_offset' value may be set to
           a non-zero value to ensure alignment properties required for
           efficient memory access during horizontal DWT synthesis.  When
           this happens, the width of the line buffer supplied to `pull',
           as returned via `kdu_line_buf::get_width' will be `pull_offset'
           samples larger than the actual width of the subband data being
           requested, and the data will be written starting from location
           `pull_offset', rather than location 0.
         [ARG: env]
           If non-NULL, the behaviour of the underlying
           `kdu_pull_ifc::pull' function is changed radically.  In
           particular, a job queue is created by this constructor, to enable
           asynchronous multi-threaded processing of the code-block samples.
           Processing of code-blocks commences once the first call to
           `kdu_pull_ifc::pull' or `kdu_pull_ifc::start' arrives.  The latter
           approach is preferred, since it allows parallel processing of
           the various subbands in a tile-component to commence immediately
           without waiting for DWT dependencies to be satisfied.  You should
           remember the following two points:
           [>>] All objects which share the same `allocator' must be created
                using the same thread.  Thereafter, the actual processing
                may proceed on different threads.
           [>>] You must supply a non-NULL `env' argument to the
                `kdu_push_ifc::pull' function -- it need not refer to the
                same thread as the one used to create the object here, but
                it must belong to the same thread group.
         [ARG: env_queue]
           If `env' is NULL, this argument is ignored; otherwise, the job
           queue which is created by this constructor will be made a
           sub-queue of any supplied `env_queue'.  If `env_queue' is NULL,
           the queue created to process code-blocks within this
           tile-component-subband will be a top-level queue.
           [//]
           The `env_queue->update_dependencies' function is invoked with
           a `new_dependencies' value of 1 both from within this constructor
           and then whenever a call to `pull' causes this object's internal
           subband sample buffer to become empty, so that a subsequent call
           to `pull' might require the caller to block until the buffer has
           been re-filled by block decoding operations.
           The `env_queue->update_dependencies' function is
           invoked with a `new_dependencies' value of -1 whenever block
           decoding operations cause a previously empty subband sample buffer
           to hold one or more complete lines of decoded subband samples,
           so that a subsequent call to `pull' will not block the caller.
           [//]
           For more on the role and benefits of the `env_queue' argument,
           see the discussion of this argument's namesake within the
           `kdu_synthesis' constructor, as well as the discussion that appears
           with the definition of `kdu_thread_queue::update_dependencies'.
         [ARG: extended_info]
           Used to provide extended information to the encoder that may
           facilitate internal optimization.   Currently, only one flag is
           defined for this use, as follows:
           [>>] `KDU_LINE_WILL_BE_EXTENDED' -- if this flag is defined,
                the caller is intending to pull data into line buffers that
                have been created with an `extend_right' value of 1.  That is,
                `kdu_line_buf' objects supplied to the `pull' function will
                have an extra sample beyond the nominal end of the
                line in question.  Knowing this, the current object may
                allocate internal storage to have the same extended length
                so as to maximize the chance that internal calls to
                `kdu_line_buf::exchange' will succeed in performing an
                efficient data exchange without copying of sample values.
                This flag is provided primarily to allow efficient DWT
                implementations to work with buffers that have an equal
                amount of storage for low- and high-pass horizontal subbands.
                In practice, the flag will only be set for horizontal low
                (resp. high) subbands that are shorter (by 1) than the
                corresponding horizontal high (resp. low) subband, where
                the longer subband cannot be spanned by the same number
                of octets as the shorter subband; this is a relatively unusual
                condition, but still worth catering for. The application
                itself would not normally set this flag.
         [ARG: postquant_scale]
           If non-NULL, this argument should reference a variable that is
           initialized to a non-positive value.  The argument is relevant
           only when `use_shorts' is false and the data processing path is
           irreversible, so that floating point sample values would normally
           be retrieved via the `kdu_pull_ifc::pull' interface.
           [//]
           If the subband decoder object is willing to buffer the decoded
           values as quantized 16-bit integers (with the mid-point rounding
           bit in the least significant bit position) as opposed to 32-bit
           floats, it may set the value at `postquant_scale' equal
           to half the quantization step size (taking `normalization' into
           account).
           [//]
           If the caller (almost certainly a DWT synthesis object) finds
           a positive value at `postquant_scale' upon return, the
           `kdu_line_buf' objects that it passes to `kdu_pull_ifc::pull' are
           expected to identify an absolute 16-bit integer representation.
           The caller is then responsible for converting the signed 2's
           complement integers returned via these line buffers to floating
           point and multiplying them by the value at `postquant_scale', in
           order to recover the values that would normally be passed across
           the `kdu_pull_ifc::pull' interface, had the `postquant_scale'
           argument not been used.  The use of a 16-bit internal
           representation can be very helpful in reducing memory consumption
           and external memory bandwidth (often the subband sample values
           produced by block decoding cannot reside in on-chip caches).
           [//]
           Even without this argument, the internal decoder implementation is
           at liberty to perform its own conversions from an internal 16-bit
           representation to floats, but this might not be efficient.  The
           reason is that a DWT synthesis engine is in a position to perform
           the conversion on-the-fly, potentially avoiding a high volume of
           external memory transactions.
           [//]
           Note that the interpretation of the 16-bit integers passed across
           the `kdu_pull_ifc::pull' interface when the "post-quant" option is
           used differs from the interpretation of those that are passed
           across `kdu_push_ifc::push' to a `kdu_encoder' object that was
           constructed with the "pre-quant" option.  For encoding, the
           16-bit integers are exactly the quantized values that must be
           encoded, while for decoding they are doubled and the least
           significant bit position is used to communicate a mid-point
           rounding rule -- value will be 1 for samples that became
           significant or experienced magnitude refinement in their least
           significant magnitude bit-plane.  As a result, the post-quant
           feature can only be used if the number of magnitude bit-planes
           for the subband is 14 or less.
      */
  };

/*****************************************************************************/
/*                             kdu_cplex_analysis                            */
/*****************************************************************************/

class kdu_cplex_analysis { 
  /* [SYNOPSIS]
       This object is normally created to implement the functionality
       associated with the `Cplex' parameter attribute's "EST" method -- see
       the introductory notes to `cod_params' for a discussion of this and
       all other coding-related parameter attributes.
       [//]
       This functionality is primarily of interest for constraining the
       complexity of the HT (High Throughput) block encoding algorithm, as
       identified by the presence of the `Cmodes_HT' flag within the `Cmodes'
       parameter attribute -- again, see `cod_params' for a discussion of this.
       In particular, the complexity analysis performed by this object is used
       to estimate the statistics that are used to assign dynamic values to
       the `kdu_block::constraint_base' member that is used to determine the
       first (coasest) Cleanup pass that needs to be performed by an HT
       block encoder.
       [//]
       Normally, applications themselves do not construct instances of this
       object, leaving `kdu_multi_analysis' to do this automatically, passing
       a reference to any such object down into the DWT analysis, fusion
       and encoding hierarchy represented by the `kdu_analysis',
       `kdu_analysis_fusion' and `kdu_encoder' constructors.
       [//]
       Upon receiving an active `kdu_cplex_analysis' object during
       construction, the `kdu_encoder' object associated with each subband
       can (not required) invoke the `attach' function to obtain an interface
       that it then must (required) use to pass estimated byte count
       statistics back to this object; this is done with the aid of calls to
       the `kdu_cplex_ifc::note_updates' function.  The provision of this
       information ultimately results in the passing of collected statistics
       to the `kdu_node::exchange_cplex_records' function.  The `attach'
       function invokes `kdu_subband::start_cplex_analysis', which
       commits this object to providing statistics about that subband via
       its `kdu_node::exchange_cplex_records' calls.  Each individual
       call to that function might provide digested and inferred estimated
       byte count statistics for only some of the relevant subbands, but the
       information will need to eventually be provided for use in the
       estimation of encoding complexity constraints returned via
       `kdu_subband::open_block'.
       [//]
       When complexity constraints are required, one instance of this object
       will be managed on behalf of each tile-component-fragment -- fragments
       here correspond to simultaneous processing fragments, as explained in
       the documentation of `kdu_node', but the use of fragments is not
       required.  Each of these instances is associated (via `init') with the
       top-level `kdu_node' interface for the tile-component-fragment -- the
       one corresponding to the top-level `kdu_push_ifc' interface into which
       tile-component sample data is pushed.
       [//]
       Additionally, where a tile-component has multiple simultaneous
       processing fragments that must be fused at some point in the hierarchy
       via a `kdu_analysis_fusion' object, the fusion object creates an
       additional `kdu_cplex_analysis' object to pass down to its descendants,
       attaching it to the consolidated (fragment-free) `kdu_node' with which
       the `kdu_analysis_fusion' object is associated.  As a matter of
       convenience, any such additional object is passed to the
       `link_descendant' function.  The reason for doing this is to ensure that
       calls to `finish' are passed on immediately to the descendant object's
       `finish' function, without the `kdu_multi_analysis' object needing to be
       explicitly aware of the existence of extra instances of the
       complexity analysis machinery.
  */
  public: // Member functions
    kdu_cplex_analysis();
    ~kdu_cplex_analysis();
    void init(kdu_node node, kdu_cplex_share_cfrag *cfrag,
              kdu_cplex_bkgnd_comp *bkgnd_comp);
      /* [SYNOPSIS]
           Uses `node.start_cplex_analysis' to initiate an interaction
           with the core codestream management machinery, if appropriate,
           receiving a 0 response if it is not.  If no complexity analysis
           is required, the object remains in the inactive state, meaning that
           `is_active' will continue to return false.  In that case, there is
           no need to pass a reference to this object into the `kdu_analysis',
           `kdu_analysis_fusion' or `kdu_encoder' constructors, but there is
           no harm in doing so.
         [ARG: node]
           Must identify the primary node of a `kdu_resolution' interface.
           The reason for supplying a `kdu_node' rather than `kdu_resolution'
           interface here is that `kdu_node' interfaces can carry embedded
           fragment identifiers, and you will generally want separate
           `kdu_cplex_analysis' objects for each simultaneous processing
           fragment.
           [//]
           The resolution associated with the `node' must be one of the
           following:
           [>>] The top resolution in a tile-component (in this case, the
                `node' interface can identify any fragment).
           [>>] The resolution (if any) at which a tile-component's fragments
                must be fused together (`kdu_analysis_fusion' interfaces
                are constructed at such nodes).
         [ARG: cfrag]
           This argument is derived from `kdu_cplex_share_tile::get_cfrag', if
           there is any `kdu_cplex_share' object available to share statistics
           between frames in a video sequence, noting tht the present
           `kdu_cplex_analysis' object is used to analyze a single fragment (in
           the sense of simultaneous processing fragments) of a single
           tile-component -- i.e., a single "cfrag", where "c" is for component
           and "frag" for fragment.
         [ARG: bkgnd_comp]
           This argument is derived from `kdu_cplex_bkgnd::access_component'
           if possible, but only if the `bkgnd_comp->configure' function
           returns true.  One, none or both of `cfrag' and `bkgnd_comp' can
           be non-NULL, allowing for statistics to be shared between video
           frames and/or a background repository.
      */
    bool is_active() const { return (max_bands != 0); }
      /* [SYNOPSIS]
           Returns false if `init' has not been called, or if it has been
           called but `kdu_hode::start_cplex_analysis' returned 0.
      */
    bool operator!() const { return !is_active(); }
      /* [SYNOPSIS]
           Returns true if and only if `is_activer' returns false.
      */
    void link_descendant(kdu_cplex_analysis *descendant)
      { 
        assert(this->linked_descendant == NULL);
        this->linked_descendant = descendant;
      }
      /* [SYNOPSIS]
           Links `descendant' as a descendant of the current object, so that
           calls to `finish' are passed to `descendant->finish'.  There can
           be only one descendant, but they can be chained.  A descendant
           can also be removed simply by pasing a NULL argument.  In practice,
           this function is used to link at most one descendant, associated
           with the consolidated output node of a `kdu_analysis_fusion' object.
           In the future, if we enable multiple levels of fusion, it might make
           sense to have deeper descendant chains.
           [//]
           Note that the `descendant' object is not considered to be owned
           by this object; it will not be deleted by this object's destructor.
      */
    kdu_cplex_ifc *attach(kdu_subband band, int &early_gate_delay,
                          int &late_gate_delay);
      /* [SYNOPSIS]
           This function is invoked within the constructor of `kdu_encoder'
           objects that are passed a non-NULL `cplex' argument,
           wherever the encoder implementation supports the generation of
           estimated coded byte lengths from subband sample data.  The
           function invokes `band.start_cplex_analysis', returning
           the early and late gate delays it recovers from that function,
           so that the encoder that is being constructed can configure
           itself to implement those delays.
           [//]
           If `is_active' returns false, this function immediately returns
           NULL.
      */
    void service(kdu_thread_env *env)
      { 
        if (need_exchange)
          do_exchange(env);
      }
      /* [SYNOPSIS]
           Called each time the top-level `kdu_push_ifc::push' call
           associated with the same `kdu_node' interface as this object
           returns.  The function simply invokes `do_exchange' if the need for
           exchange of statistics has been noted in any of the calls to
           `kdu_cplex_ifc::note_updates'.
      */
    void finish(kdu_thread_env *env);
      /* [SYNOPSIS]
           Called once all subband data has been generated, this function
           makes sure that all remaining statistics are transferred and
           exchanged with the core `kdu_codestream' machinery via
           `kdu_node::exchange_cplex_records'.  This is invoked within
           `kdu_multi_analysis' immediately before the `kdu_push_ifc::finish'
           calls that are required to complete the sample data push process.
           In practice, this is done only once all fragments of all components
           have finished their push processes within any given tile.
      */
  private: // Functions
    void note_stripe_completed(kdu_cplex_ifc *ifc);
      /* Called when `ifc->note_updates' is notified that a stripe has
         completed.  This function performs any outstanding transfer
         (`ifc->transfer'), moves the completed byte count estimates
         into a new complexity record and prepares `ifc->working_rec' for the
         next stripe. */
    void do_exchange(kdu_thread_env *env);
      /* This function builds a list of records and then exchanges them in one
         go via a call to `kdu_node::exchange_cplex_records'. */
    kdu_cplex_record *get_free_record()
      { 
        if (num_free_records == 0) allocate_record_block();
        kdu_cplex_record *rec = free_records;
        free_records = rec->next;  num_free_records--;
        return rec;
      }
    void allocate_record_block();
      /* Allocates a new block of aligned complexity records and puts them
         onto the `free_records' list. */
  private: // Internal structures
    struct kd_record_block { 
      kd_record_block *next;
      kdu_byte handle[128*31+KDU_MAX_L2_CACHE_LINE];
    };
  private: // Data
    friend class kdu_cplex_ifc;
    kdu_node node;
    bool need_exchange; // If any completed record has reached its late_gate.
    bool cfrag_just_started; // Used when initializing `shared_interfaces'
    kdu_cplex_record *free_records;
    kd_record_block *record_blocks;
    int num_free_records; // Total number of records on the free list
    int num_alloc_records; // Total number of records we have ever allocated
    kdu_uint16 next_generation; // Written to records in `do_exchange'
    int min_abs_band_idx; // Obtained from `kdu_node::start_cplex_analysis'
    int max_bands; // Obtained from `kdu_node::start_cplex_analysis'
    int num_active_interfaces; // Actual number of subband interfaces used
    float *shared_6strips; // Obtained from `kdu_cplex_share_cfrag::attach'
    kdu_cplex_share_cfrag *share_cfrag; // Needed for `detach' call
    kdu_cplex_bkgnd_comp *bkgnd_comp; // To provide/accumulate bkgnd stats
    kdu_cplex_ifc *plain_interfaces; // This is used if `shared_6strips'==NULL
    kdu_cplex_shared_ifc *shared_interfaces; // else, this pointer is used.
    kdu_cplex_analysis *linked_descendant; // See `link_descendant'.
  };

/*****************************************************************************/
/*                               kdu_cplex_ifc                               */
/*****************************************************************************/

class kdu_cplex_ifc { 
  protected: // Functions used only by `kdu_cplex_analysis' or internally
    friend class kdu_cplex_analysis;
    kdu_cplex_ifc()
      { 
        tmp_rows = 0;
        early_gate = late_gate = 0;
        memset(tmp_estimated_bytes,0,sizeof(float)*KDU_CPLEX_REC_MAX_ENTRIES);
        memset(&working_rec,0,sizeof(working_rec));
        completed_head = completed_tail = NULL;  owner = NULL;
        valid_entries = 0; cum_weights = 0.0f;
        memset(&cum_weighted_bytes,0,sizeof(float)*KDU_CPLEX_REC_MAX_ENTRIES);
        remaining_rows = 0;  stripes_left_to_exchange = 0;
        last_noted_tmp_rows = 0;  forecast_refresh_interval = 0;
        forecast_refresh_downcounter = 0;
        have_stats = have_new_stats = need_exchange = false;
        res_id = res_bands_without_stats = 0;
        head_ifc = NULL;
      }
    virtual ~kdu_cplex_ifc() { return; }
    virtual void do_transfer();
      /* Transfer the temporary expected byte counts in `tmp_estimated_bytes',
         representing `tmp_rows' subband rows, to the `working_rec' member,
         resetting `tmp_rows' and `last_noted_tmp_rows'.  At the same time,
         this function updates the `cum_weighted_bytes' and `cum_weights'
         members that are used to generate forecasts.  This function is
         overridden by `kdu_cplex_shared_ifc' for cases in which information
         can be shared from a previous frame (or tile thereof), so as to
         update this shared information and generate summary data that is
         used to improve forecasts. */
    virtual void forecast_remaining_rows(kdu_cplex_record *rec);
      /* This function is called as the last step in building a forecast
         record in `rec', after configuring the record to include all
         known statistics.  From the perspective of the core codestream
         machinery, forecasts cover all subband rows that have not yet
         been described by completed records that it knows about, but there
         are generally subband rows that we know about but are not yet
         ready to share with the codestream machinery's constraint generation
         machinery.  For this reason, forecasts are constructed by first
         including all known data and then including estimates for the
         expected byte counts of subband rows that we have not yet seen.
         This function is responsible only for this last stage, adding in
         the relevant expected byte counts and augmenting `rec->num_rows'
         by `remaining_rows' to reflect this.  This function is overridden
         by `kdu_cplex_shared_ifc' for cases in which information may be
         available from a previous frame (or tile thereof), to use this
         information (where appropriate) to improve its estimates. */
    void schedule_forecast()
      { // Arranges for a forecast record to be generated when
        // `owner->service' is next called.
        forecast_refresh_downcounter = 0;
        owner->need_exchange = true;
      }
  public: // Data members and functions accessed directly by the encoder
    kdu_int32 tmp_rows; // Rows accumulated in `tmp_estimated_bytes'
    kdu_int32 early_gate;
    kdu_int32 late_gate;
    float tmp_estimated_bytes[KDU_CPLEX_REC_MAX_ENTRIES];
    void advise_num_entries(int num_entries)
      { /* This function must be called exactly once, from within the
           encoder's initialization routine -- i.e., before any calls to
           `note_updates' can appear for any interface belonging to the
           same `kdu_cplex_analysis' object.  The function advertises the
           number of entries within the `tmp_estimated_bytes' array that
           will contain valid data when `note_udpates' is called -- entries
           beyond that point should be ignored. */
        assert((this->valid_entries == 0) && (num_entries > 0) &&
               (num_entries <= KDU_CPLEX_REC_MAX_ENTRIES));
        this->valid_entries = num_entries;
        this->working_rec.num_entries = (kdu_byte)num_entries;
      }
    void note_updates(bool stripe_completed, int num_entries)
      { /* Invoked only from within the internal implementation of
           `kdu_encoder' after updating the `tmp_estimated_bytes' and
           `tmp_rows' members with new estimated byte count statistics
           and/or modifying `early_gate' or `late_gate'.  It is
           harmless, however, to call the function even if there have been
           no changes to any of these members.  If `stripe_completed' is
           true, a row of code-blocks (a.k.a. stripe here) has been completed,
           and all associated samples have made their contribution to the
           estimated byte count values in the `tmp_estimated_byets' array.
              The `num_entries' member is the number of initial entries of
           the `tmp_estimated_bytes' array, to which the caller is writing
           valid data.  This value must agree exactly with that passed to
           `advise_num_entries' -- i.e., the number of entries for which
           estimates are formed cannot change dynamically within any
           given subband-fragment. */
        assert(num_entries == this->valid_entries);
        if (tmp_rows != last_noted_tmp_rows)
          { 
            have_new_stats = true;
            if (stripe_completed)
              owner->note_stripe_completed(this); // Calls `do_transfer'
            else if (tmp_rows >= 4)
              do_transfer();
            last_noted_tmp_rows = tmp_rows;
            if (!have_stats)
              { 
                have_stats = true;
                assert(head_ifc->res_bands_without_stats != 0);
                if ((--(head_ifc->res_bands_without_stats)) == 0)
                  owner->need_exchange = true;
              }
          }
        else if (stripe_completed)
          { 
            owner->note_stripe_completed(this); // Calls `do_transfer'
            last_noted_tmp_rows = tmp_rows;
          }
        if ((completed_head != NULL) && (late_gate > completed_head->idx))
          owner->need_exchange = this->need_exchange = true;
      }
    void clear_outstanding_service(kdu_thread_env *env)
      { /* This function does nothing unless a recent call to `note_updates'
           set `owner->need_exchange' to true, expecting the
           `kdu_cplex_analysis::service' function to be called when the
           DWT analysis machinery fully returns, but the `service' call
           has not happened for one reason or another, and is now urgently
           required.  This can be important when working with images with
           unusual dimensions, and low latency Cplex-EST based complexity
           constraints, because computation of the constraint may be
           required in order to complete the coding of a previous row of
           code-blocks before a new row can be started, and for one
           reason or another the DWT analysis machinery pushed multiple
           subband lines to an encoder before fully returning to
           service outstanding cplex-record exchange requests. */
        if (!need_exchange)
          return;
        owner->do_exchange(env);
        assert(!need_exchange);
      }
  protected: // Data members manipulated by `kdu_cplex_analysis' only
    kdu_cplex_record working_rec;
    kdu_cplex_record *completed_head;
    kdu_cplex_record *completed_tail;
    kdu_cplex_analysis *owner;
    kdu_int32 valid_entries; // See `advise_num_entries'
    float cum_weights;
    float cum_weighted_bytes[KDU_CPLEX_REC_MAX_ENTRIES];
    kdu_int32 remaining_rows; // Rows not yet moved into `working_rec'
    kdu_int32 stripes_left_to_exchange; // R such that forecast `idx'=-R
    kdu_int32 last_noted_tmp_rows;
    kdu_uint16 forecast_refresh_interval;    // Used to update forecasts before
    kdu_uint16 forecast_refresh_downcounter; // first completed record exchange
    bool have_stats;
    bool have_new_stats;
    bool need_exchange; // True if we recently set `owner->need_exchange' for
                        // the purpose of exchanging completed records, as
                        // opposed to just forecasts.
    kdu_byte res_id; // Unique value common to all bands of same resolution
    kdu_byte res_bands_without_stats; // Only valid in resolution's head ifc
    kdu_cplex_ifc *head_ifc; // First interface with same `res_id'
  };
  /* Notes:
        You should not need to access instances of this class directly at
     the application level.  It is an integral component of the
     `kdu_cplex_analysis' object, and instances can only be obtained
     by invoking `kdu_cplex_analysis::attach'.  Beyond this, the few
     public members of the class are used only from within the
     internal implementation of `kdu_encoder', which is expected to set
     `valid_entries' equal to K_est = min{K_max,KDU_CPLEX_REC_MAX_ENTRIES}
     prior to its first call to `note_updates'.
        Estimated byte counts, following the principles outlined in the notes
     following `kdu_cplex_record', are accumulated in the first instance
     within the `tmp_estimated_bytes' array, representing `tmp_rows' lines
     from the relevant subband-fragment.  Periodically, these temporary
     statistics are moved across into the `working_rec' member, subtracting
     `tmp_rows' from `remaining_rows' and resetting both `tmp_rows' and
     the `tmp_estimated_bytes' array.  These operations are performed by
     the private `do_transfer' functionm which may be invoked either by
     `kdu_cplex_ifc::note_updates' or
     `kdu_cplex_analysis::do_exchange'.
        During the `do_transfer' call, a scaling factor W is generated from
     the reciprocal of `remaining_rows' right before the transfer, and the
     transferred estimated byte counts are scaled by W and accumulated also
     in `cum_weighted_bytes', while accumulating W*`tmp_rows' in `cum_weights'.
     These weighted accumulators are used to form forecasts.  At the point when
     a forecast record needs to be generated, we first transfer any temporary
     stats that have not already been transferred, then scale the
     `cum_weighted_bytes' values by `remaining_rows' / `cum_weights', then
     we add in the contributions from all completed stripes that are not
     yet ready to be exchanged, plus any partial stripe byte counts from
     `working_rec', since all of this corresponds to real data for us that
     will be not be known yet to the core system.
        Completed records are held within a list headed by `completed_head',
     which is organized starting from the least recently completed record.
        The `early_gate' member holds the total number of records (from the
     start of the subband) that have passed their "early gate", meaning that
     they can potentially be passed to the core codestream machinery via a
     call to `kdu_node::exchange_cplex_records'.  Any record for which
     `kdu_cplex_record::idx' < `early_gate' can be exchanged.
        The `late_gate' member is similar to `early_gate', but identifies the
     total number of records (from the start of the subband) that must be
     exchanged.  If `completed_tail->idx' < `late_gate', then one or more
     complete records must be exchanged when `kdu_cplex_analysis::service'
     is next invoked.  Of course, we must have `early_gate' >= `late_gate'.
     These gates are incremented directly from within the internal
     implementation of `kdu_encoder' objects, but take effect within
     calls to `kdu_cplex_ifc:note_updates' or
     `kdu_cplex_analysis::do_exchange'.
        The `have_new_stats' member is set to true each time a call to
     `note_updates' finds that the `tmp_rows' value has changed.  Changes are
     identified by comparing `tmp_rows' with `last_noted_tmp_rows', which is
     updated at the end of each `note_updates' call.  If
     `kdu_cplex_analysis::do_exchange' finds `have_new_stats' to
     be true, it must at least include a forecast record for the subband within
     the list of records exchanged via `kdu_node::exchange_cplex_records'.
        The `forecast_refresh_interval' is configured during initialization to
     equal 1/4 of the number of subband lines associated with the first early
     gate.  Each time a forecast is issued, if no completed records have yet
     been exchanged, the `forecast_refresh_interval' is copied to
     `forecast_refresh_downcounter'.  Each time new rows of statistics become
     available, `forecast_refresh_downcounter' is decremented, if non-zero,
     and if this leaves it <= 0, an exchange is arranged, which will result in
     the generation of new forecasts.  These features have no effect unless a
     substantial amount of the tile (perhaps the whole thing) is being buffered
     up first, before exchanging completed stripe statistics.  The purpose is
     to avoid a situation in which the statistics used for one fragment or
     fused set of subbands may be extremely old by the time others are ready
     for constraint analysis.  As soon as a competed stripe is exchanged, the
     `forecast_refresh_interval' is set to 0.  `forecast_refresh_downcounter'
     is initialized to 0 so that the refresh downcounting process does not
     start until after the first forecast has been generated, which is managed
     by the four members described below so as to synchronize across all
     subbands of a resolution.
        The last four members implement a mechanism for getting early forecasts
     to be as comprehensive as possible, when we have only just started
     pushing image samples into the transform hierarchy associated with the
     node associated with our `kdu_cplex_analysis' owner.  Each interface
     is associated with a collection of interfaces that belong to the same
     resolution, using the `res_id' value that is actually just the
     resolution level index.  For each collection, there is a head interface,
     being the first one within the array of interfaces found in
     `kdu_cplex_analysis' that has the same `res_id'.  This head
     interface keeps track of the number of interfaces from the same
     resolution that have not yet had any statistics noted -- i.e.,
     `tmp_rows' has not yet been noted to become non-zero in a call to
     `note_updates'; once this count reduces to 0, a call to
     `kdu_cplex_analysis::do_exchange' is arranged to occur as soon
     as possible, which might only exchange forecast records rather than
     completed records.
  */

/*****************************************************************************/
/*                           kdu_cplex_shared_ifc                            */
/*****************************************************************************/

class kdu_cplex_shared_ifc : public kdu_cplex_ifc { 
  protected: // Member functions used only by `kdu_cplex_analysis'
    friend class kdu_cplex_analysis;
    kdu_cplex_shared_ifc()
      { 
        just_started = false;
        bkgnd_band_idx = 255; bkgnd_eps_mu = 0; bkgnd_stats = NULL;
        strip = 0;  strip_rows = 0;
        memset(strip_accumulator,0,sizeof(float)*KDU_CPLEX_REC_MAX_ENTRIES);
        cum_overwritten_strip_summary = cum_overwritten_weight = 0.0f;
        prev_accumulated_strip_summary = 0.0f;
        prev_accumulated_strip_rows = 0;
        shared_6strips = NULL;  bkgnd_level = NULL;
      }
    virtual void do_transfer();
      /* Overrides the base `do_transfer' function to also update the
         state information here and, if appropriate, update a
         strip within the `shared' object, advancing the `strip' index
         and preparing to accumulate the next strip. */
    virtual void forecast_remaining_rows(kdu_cplex_record *rec);
      /* Overrides the base `forecast_remaining_rows' function to use
         information from the `shared' resource when forming forecasts. */
  private: // Data
    bool just_started; // True for the first frame
    kdu_byte bkgnd_band_idx;
    kdu_uint16 bkgnd_eps_mu;
    const float *bkgnd_stats;
    int strip; // Index of strip currently being accumulated
    int strip_rows; // Num rows accumulated so far for `strip'
    int strip_heights[6];
    float strip_accumulator[KDU_CPLEX_REC_MAX_ENTRIES];
    float cum_overwritten_strip_summary;
    float cum_overwritten_weight;
    float prev_accumulated_strip_summary;
    int prev_accumulated_strip_rows;
    float *shared_6strips; // 6 sets of `KDU_CPLEX_REC_MAX_ENTRIES' floats,
                           // unless `single_strip' is true.
    kdu_cplex_bkgnd_level *bkgnd_level;
  };
  /* Notes:
        This class is used by `kdu_cplex_analysis' for subbands instead of
     the base `kd_cplex_ifc' class, if interaction with `kdu_cplex_share'
     is required.  This object provides additional members for keeping track
     of a current `strip' for which statistics are being accumulated, and
     managing the transfer of normalized accumulated statistics to the
     relevant strip's elements within the `shared_6strips' array.  This is
     all done by overriding the base virtual `do_transfer' function.
        The `strip' member keeps track of the strip that is being
     generated within a current frame, while the information for the
     corresponding strip within the `shared' object describes the
     previous frame (or tile thereof).
        To update the strip statistics, expected byte counts within the
     current frame (or tile thereof) are accumulated within the
     `strip_accumulator' array, keeping track of the total number of
     accumulated rows in `strip_rows' until it reaches or exceeds
     `strip_heights'[`strip'], at which point the accumulated data is
     normalized and used to overwrite the relevant entries within the
     `shared_6strips' array.
        Note that if more rows are accumulated than `strip_heights'[`strip'] --
     can happen since multiple rows are aggregated at a time -- the height of
     the next strip is simply adjusted to compensate; strip heights do not
     need to be exact because each strip's expected byte counts are
     normalized.  In rare circumstances, it may be necessary to propagate
     the adjustment to even later strips.
        The `shared_6strips' array manages storage that is shared between
     consecutive frames (or tiles thereof).   This information consists of
     6 strips that collectively represent the subband.  The strips are expected
     to represent the following regions of the frame (or tile), but all that
     really matters is that they are generated and used in a reasonably
     consistent way:
        Strip 0: top 6.25% of the frame or tile
        Strip 1: next 6.25% of the frame or tile
        strip 2: next 25% of the frame or tile
        Strip 3: next 25% of the frame or tile
        Strip 4: next 25% of the frame or tile
        Strip 5: last 12.5% of the frame or tile
     For each strip, we maintain `KDU_CPLEX_REC_MAX_ENTRIES' normalized
     expected byte counts -- these are correspond to the entries of the
     `kdu_cplex_record::expected_bytes' member array, accumulated over the
     relevant image strip, and divided by the total number of samples within
     that image strip.
        Note that we do not allow any strip to hold invalid data, even if
     the subband height becomes so small that some strips have no associated
     subband rows.  If no statistics are collected for a strip, it simply
     takes on the same byte/sample statistics as its predecessor.
        Scalar summary values are used to adaptively determine whether
     forecasts for unseen content in the current frame (or tile thereof)
     should be formed using already seen content from the same frame (spatial
     forecasting) or content from a previous frame, as recorded within the
     strip buffers (temporal forecasting).  Actually, we use both spatial and
     temporal information for forecasts, but use the summary statistics to
     determine which of the two types of information is expected to be more
     reliable.  The summary statistic for an entire collection of
     `valid_entries' <= `KDU_CPLEX_REC_MAX_ENTRIES' expected byte counts B[i]
     is given by S = sum_{0 <= i < N} 2^{-i} B[i], where N=`valid_entries'.
        Since strips are overwritten with statistics for the current frame
     (or tile thereof) as soon as they are completed, it is helpful to keep
     track of the summary scalar values associated with those overwritten
     strips as we go.  This is the role of `cum_overwritten_strip_summary'
     and `cum_overwritten_strip_weight'.  The first member accumulates the
     W_strip * S_strip for each strip whose statistics have been overwritten,
     where S_strip is the scalar summary for a given strip in the previous
     frame (or tile thereof), i.e., immediately before overwriting, and
     W_strip is proportional to the height of the strip.  Meanwhile,
     `cum_overwritten_strip_weight' accumulates the W_strip values.
        It turns out to be convenient to also keep track of a cumulative
     summary value for the overwriting statistics in the current frame (or tile
     thereof), even though they can be recovered from the relevant entries in
     the `shared_6strips' array.  To this end, `prev_accumulated_strip_summary'
     accumulates summary values S_k that are derived from the
     `strip_accumulator', prior to any normalization, immediately before
     writing normalized statistics to the `shared_6strips' buffer, and
     `prev_accumulated_strip_rows' keeps track of the total number of
     subband rows represented by such samples.  The ratio of these two
     quantities, divided by the subband width, is a scalar summary expressed
     in bytes/sample that can be compared with the ratio between
     `cum_overwritten_strip_summary' / `cum_overwritten_strip_weights'.
        If `just_started' is true, there is no previous frame, and so
     temporal forecasts are not possible.  In this case, we only update the
     strip statistics within `shared_6strips', but have no need for
     `cum_overwritten_strip_summary' or `cum_overwritten_strip_weights'.
     Moreover, each time we complete a strip, if `just_started' is true we
     copy the normalized statistics to all future strips also, so that
     all strips are certain to hold meaningful values when the next frame
     (or tile thereof) is started.
        This class also manages background statistics that might have been
     accumulated from separate images, whether for video or just image
     compression.  If we are only doing image compression, the `shared_6strips'
     array will be NULL, and the `strip_rows' and `strip_heights' members will
     be configured so that no attempt is made to update entries in the
     `shared_6strips' array -- i.e., `strip_heights'[0] is strictly
     larger than the entire subband height.  The background statistics are
     accessed via a non-NULL `bkgnd_stats' pointer, which points to an array
     of `KDU_CPLEX_REC_MAX_ENTRIES' values that can be used to improve the
     robustness of forecasts.  Once the first frame of a video has been
     processed, `just_started' becomes false and `bkgnd_stats' is set to NULL,
     so the background statistics are not used for forecasts in the future.
     Nonetheless, background statistics can be updated after the completion of
     each video frame, or a single image subband -- this is accomplished via
     the `bkgnd_level' reference.
  */

/*****************************************************************************/
/*                          kdu_cplex_share_cfrag                            */
/*****************************************************************************/

class kdu_cplex_share_cfrag { 
  /* [SYNOPSIS]
       You would not normally need to work with this object at the application
       level, so we provide no foreign language bindings here.  References to
       a `kdu_cplex_share_cfrag' object are obtained by calling
       `kdu_cplex_share_tile::get_cfrag' and they are passed only to
       `kdu_cplex_analysis::init'.  Moreover, `kdu_cplex_analysis' is an
       object you would not normally create or touch at the application level.
       These things are likely to be done exclusively within the implementation
       of `kdu_multi_analysis' and `kdu_analysis_fusion'.
  */
  public: // Member functions
    kdu_cplex_share_cfrag()
      { 
        state[0].set(0);  state[1].set(0);
        band_6strips[0] = band_6strips[1] = NULL;
      }
    ~kdu_cplex_share_cfrag();
    float *attach(int min_abs_band_idx, int num_bands, bool &just_started);
      /* [SYNOPSIS]
           This function is called from within `kdu_cplex_analysis::init'
           once it determines the range of subband indices for which it may
           manage statistics -- this range is from `min_abs_band_idx' to
           `min_abs_band_idx'+`num_bands'-1 and is discovered by calling
           `kdu_node::start_cplex_analysis'.
           [//]
           The function returns an array containing 6*`num_bands' groups of
           `KDU_CPLEX_REC_MAX_ENTRIES' floats, where each subband uses 6
           such groups, the meaning of which is important only within the
           implementation of `kdu_cplex_analysis'.
           [//]
           The function also sets `just_started' to true if the array has just
           been created (either for the first time, or due to a change in the
           number of bands of `min_abs_band_idx' value), else it is set to
           false.
           [//]
           Normally, all calls to `attach' return the same array, but if the
           values of `min_abs_band_idx' or `num_bands' change between calls,
           a new array is allocated and statistics gathering starts from
           scratch.  In this case, the previuos array is deallocated after
           the relevant call to `detach'.
           [//]
           We currently allow only two `kdu_cplex_analysis' objects to be
           attached to any given `kdu_cplex_share_frag' object at a time.
           It is very important, therefore, that the `kdu_cplex_analysis'
           object invokes `detach' when it is done using the returned array.
           If there are too many attachments, this function simply returns
           NULL.
           [//]
           Note that calls to `attach' and `detach' are thread safe.
      */
    void detach(float *attach_result);
      /* [SYNOPSIS]
           Each call to `attach' must be matched by a `detach' call that
           passes the same array that was returned by `attach'.  This is
           normally done by `kdu_cplex_analysis::finish', but otherwise should
           be done within the `kdu_cplex_analysis' object's destructor.
           [//]
           Note that calls to `attach' and `detach' are thread safe.
      */
  private: // Data
    kdu_interlocked_int32 state[2]; // See below
    float * band_6strips[2];
  };
  /* Notes:
        To avoid race conditions, each slot keeps track of its state within a
     single interlocked atomic variable, with the following structure:
     Bits 0-1: number of attachments
     Bit 2: set if the slot should be cleaned up once attachments go to 0
     Bit 3: set if the slot is active
     Bits  4-17: `min_abs_band_idx'
     Bits 18-31: `num_bands'
        Note that a 0 value for `state' means that the slot is not in use, so
     its `band_6strips' pointer should be NULL.
        The `band_6strips' array has 6*`num_bands'*`KDU_CPLEX_REC_MAX_ENTRIES'
     elements in each entry, but one or both entries may be NULL.
  */

/*****************************************************************************/
/*                              kdu_cplex_share_tile                         */
/*****************************************************************************/

class kdu_cplex_share_tile { 
  /* [SYNOPSIS]
       You should have no need to create or access instances of this class
       at the application level, so we provide no foreign language bindings
       here.  The `kdu_cplex_share_tile' class exists only as an intermediary
       between the global `kdu_cplex_share' object, that you should create and
       own at the application level, and the low level `kdu_cplex_share_cfrag'
       object that forms the thread-safe broker between consecutive frames of
       video, for a specific tile-component-fragment.
  */
  public: // Member functions
    kdu_cplex_share_tile()
      { max_comps = max_cfrags = 0;  cfrag_refs = NULL; }
    ~kdu_cplex_share_tile();
    void configure_cfrag_bounds(int comps, int max_cfrags);
      /* [SYNOPSIS]
           This function must be called before any calls to `get_cfrag'.  It
           dimensions the internal resources.  If anything changes between
           frames, so that calls to this function are inconsistent, more
           internal resources may be allocated, but the existing ones will
           not be cleaned up, since they may still be in use within an
           earlier frame.
           [//]
           `max_cfrags' here is the maximum number of distinct
           `kdu_cplex_share_cfrag' objects that may be required for any given
           component, while `comps' is the number of components:
           [>>] If simultaneous processing fragments are not used, `max_cfrags'
                can be 1, and `get_cfrag' should always be called with a
                `cfrag_idx' value of 0.
           [>>] Otherwise, `max_cfrags' should actually be 1 larger than the
                maximum number of fragments for any given component,
                reserving `cfrag_idx' 0 exclusively for use within any
                `kdu_analysis_fusion' object that may be required to fuse
                fragments at a lower level in the DWT hierarchy.  At the
                top level of the DWT hierarchy, the `cfrag_idx' value passed
                to `get_cfrag' should then be 1 larger than the actual
                fragment index.  This ensures that the processing machinery for
                a single tile-component attaches at most once to any given
                `kdu_cplex_share_cfrag' object -- at most 2 attachments are
                allowed in total, but these are expected to correspond to
                distinct frames in a video sequence.
           [//]
           This function and `get_cfrag' are not entirely thread safe, but
           the `kdu_cplex_share_cfrag' objects returned by `get_cfrag' are
           thread safe.  This should be completely sufficient, since in most
           applications all configuration of processing machinery is done from
           a single thread.  Moreover, this is only done once per frame for
           video processing, so it does not make sense for multiple threads
           to be doing it concurrently.
      */
    kdu_cplex_share_cfrag *get_cfrag(int comp_idx, int cfrag_idx);
      /* [SYNOPSIS]
           This function is invoked from within `kdu_multi_analysis' to
           recover the set of statistics that are relevant to a particular
           tile-component, or a fragment thereof.
           [//]
           As explained with `kdu_node', when simultaneous processing fragments
           are employed, the `cfrag_idx' should actually be 1 more than the
           index of the relevant processing fragment, at the top level of the
           DWT hierarchy, reserving `cfrag_idx'=0 exclusively for use within
           any `kdu_fusion_analysis' object that may be needed to fuse
           fragments at a lower level in the hierarchy.
           [//]
           The returned `kdu_cplex_share_cfrag' object is passed to
           `kdu_cplex_analysis::init' to determine if statistical complexity
           analysis is required.
           [//]
           It is an error to pass `comp_idx' or `frag_idx' values smaller than
           0 or larger than C-1 of F-1, respectively, where C and F are the
           `max_comps' and `max_cfrags' arguments passed in the most recent
           call to `set_cfrag_bounds' -- however, if you do so, the function
           may simply return NULL.
      */
  private: // Data
    int max_comps;
    int max_cfrags;
    kdu_cplex_share_cfrag **cfrag_refs; // `max_comps' x `max_cfrags' entries
  };

/*****************************************************************************/
/*                                kdu_cplex_share                            */
/*****************************************************************************/

class kdu_cplex_share { 
  /* [BIND: reference]
     [SYNOPSIS]
       This object plays an important role in sharing subband statistics
       between (usually) consecutive frames in a video sequence.  These
       statistics are used with the `Cplex' attribute's `EST' method, for
       estimating the most appropriate complexity constraints to apply
       during the low level block encoding processes.  It is not necessary to
       share statistics between video frames, nor are the shared statistics
       used without care.  Inter-frame changes are detected as early as
       possible and used to modify the way in which statistics gathered in
       the previous frame are used, if at all.  However, if you can share
       statistics between frames it is a very good idea -- in particular,
       there is no significant risk that sharing statistics from a previous
       frame will damage the current frame being compressed.  Also, it is
       always safe to share statistics between frames even if the `Cplex'
       attribute is not defined, or its `EST' method is not employed -- but
       it will do nothing in this case.
       [//]
       Statistics are shared between frames by constructing a single instance
       of and passing a reference to this instance into each
       `kdu_multi_analysis' engine that you create.  This is done indirectly
       by supplying the reference to `kdu_push_pull_params::set_cplex_share'
       and then passing the `kdu_push_pull_params' object to
       `kdu_multi_analysis::create'.  A single `kdu_cplex_share' object is
       able to manage multiple tiles, so even if you have multiple concurrent
       tiles being processed by `kdu_multi_analysis' engines, there is no
       need to use more than a single `kdu_cplex_share' object to connect
       everything together.  At a high level, this can be done by passing the
       `kdu_cplex_share' reference, via a `kdu_push_pull_params' object, to
       the high level `kdu_stripe_compressor' API via the
       `multi_xform_extra_params' argument to `kdu_stripe_compressor::start'.
       [//]
       It is expected that you will share statistics between video frames,
       in such a way that one video frame is at least mostly processed before
       the next one starts.  The `kdu_cplex_share' mechanism is not appropriate
       for sharing information between frames that are being processed
       completely in parallel -- at best, in such circumstances it would be
       useless.  Typically, video encoders are constructed from one or more
       engines, each processing a sub-sequence of the video frames, where each
       engine processes frames sequentially, perhaps with some overlap.  For
       example, the "kdu_v_compress" demo-app creates one such engine, while
       "kdu_vcom_fast" demonstrates an approach in which multiple engines
       can be created.  Where there are multiple completely parallel engines,
       you should create one `kdu_cplex_share' instance for each engine, to
       separately share statistics between the sequentially processed frames
       that are handled by each engine.
  */
  public: // Member functions
    kdu_cplex_share()
      { max_tiles = 0; tile_refs = NULL; }
    KDU_EXPORT ~kdu_cplex_share();
    KDU_EXPORT void set_max_tiles(int limit);
      /* [SYNOPSIS]
           This function allows you to pre-specify a bound on the range of
           tile numbers t that you expect to pass to `get_tile'.  Specifically,
           it is expected that 0 <= t < `limit'.  Note, that no JPEG 2000
           codestream can have more than 2^16 tiles, but most codestreams will
           only have 1.
           [//]
           If this function is never called, or `limit' is too small, internal
           resources are dimensioned dynamically, which is fine except in the
           rare circumstance that separate `kdu_multi_analysis::create' calls
           might invoke the same `kdu_cplex_share' object's `get_tile'
           function from different threads.
           [//]
           Note that `kdu_stripe_compressor::start' automatically calls this
           function to avoid the above-mentioned problem.  As a result,
           unless you are not using `kdu_stripe_compressor' and you are
           creating `kdu_multi_analysis' objects that share the same
           `kdu_cplex_share' on different threads, you do not need to worry
           about calling this function yourself.
      */
    kdu_cplex_share_tile *get_tile(int tnum);
      /* [SYNOPSIS]
           This function is used internally within the Kakadu core system to
           access shared statistics for a specific tile.  The tile is specified
           in terms of its tile number, which is the value returned by
           `kdu_tile::get_tnum'.
           [//]
           An internal array of tile references may need to be re-dimensioned
           on demand to accommodate the request, which is not entirely thread
           safe.  In the event that separate calls to
           `kdu_multi_analysis::create' may be invoked concurrently from
           different threads, accessing the same `kdu_cplex_share' object's
           tiles via this function, you should be sure to call `set_max_tiles'
           first, to identify the number of tiles that are associated with the
           codestream.  In general, one would not expect the number or
           dimensions of tiles to change from frame to frame during video
           encoding, but if that does happen nothing will fundamentally go
           wrong -- there may be some transient invalid statistics that should
           mostly get filtered out by the discriminative nature of the
           internal constraint analysis and forecasting mechanisms.
      */
  public: // Data
    int max_tiles;
    kdu_cplex_share_tile **tile_refs;
  };

/*****************************************************************************/
/*                                kdu_cplex_bkgnd                            */
/*****************************************************************************/

class kdu_cplex_bkgnd { 
  /* [BIND: reference]
     [SYNOPSIS]
       This class is used to provide and/or collect background statistics that
       can be used by `kdu_cplex_analysis' to make the Cplex-EST coding
       complexity estimation algorithm more robust, especially when compressing
       individual images or the first frame of a video sequence in a minimum
       memory configuration, where the forecasting of future scene complexity
       is most critical.
       [//]
       Like `kdu_cplex_share', instances of `kdu_cplex_bkgnd' are shared with
       the `kdu_cplex_analysis' machinery via `kdu_push_pull_params'.  In
       fact, both types of resources can be used at the same time, if desired.
       Specifically, one passes a reference to a `kdu_cplex_bkgnd' object
       into the compression machinery managed by `kdu_multi_analysis', by
       invoking `kdu_push_pull_params::set_cplex_bkgnd' on a
       `kdu_push_pull_params' object that is passed to
       `kdu_multi_analysis::start'.  At a high level, you can pass a single
       `kdu_cplex_bkgnd' object via `kdu_push_pull_params' to the
       `kdu_stripe_compressor' API that manages compression of an entire
       image, whether it has one or more tiles.
       [//]
       This object and its descendants are designed to be thread safe, so that
       they can support asynchronous compression of multiple tiles or
       multiple images.  In particular, `kdu_cplex_bkgnd::configure',
       `kdu_cplex_bkgnd_comp::configure' and `kdu_cplex_bkgnd_level::configure'
       are all thread safe, as is `kdu_cplex_bkgnd_level::store_new_stats'.
       The only other function that modifies state is
       `kdu_cplex_bkgnd_level::store_bkgnd_stats', but that function is
       invoked only when reading statistics from an external repository,
       which should happen before any compression is performed that might
       attempt to use the statistics.
       [//]
       Instances of this class, and its descendants, can be placed in a
       `read_only' state, meaning that they will only be used to record
       background statistics originating from an external provider (typically
       a file that has previously been created by collecting statistics in
       a write-only or read-write mode), making those statistics available as
       background information for the `kdu_cplex_analysis' machinery.
       [//]
       If not in the `read_only' state, the number of DWT levels for a
       component may be increased by re-configuring the
       `kdu_cplex_bkgnd_comp' descendants of this object, and the
       `kdu_cplex_bkgnd_level::store_new_stats' function will update the
       internal statistics based on information that was collected via
       individual band-fragment interfaces of the `kdu_cplex_analysis'
       machinery. Moreover, the aggegated new statistics can be recovered
       via `kdu_cplex_bkgnd_level::get_accumulated_stats' for externalizing
       to a background statistics file or some other resource that can be
       made available to compressors.
  */
  public: // Member functions
    KDU_EXPORT kdu_cplex_bkgnd();
    KDU_EXPORT ~kdu_cplex_bkgnd();
    bool set_read_only()
      { 
        if (comps != NULL)
          return false;
        this->read_only = true;
        return true;
      }
      /* [SYNOPSIS]
           Call this function prior to the first call to `configure' if you
           want the object to be in the "read-only" state -- this state
           requires less memory and does not record any statistics collected by
           the `kdu_cplex_analysis' machinery -- in particular, calls to
           `kdu_cplex_bkgnd_level::store_new_stats' will do nothing, returning
           false, as will calls to `kdu_cplex_bkgnd_level::get_new_stats'.
         [RETURNS]
           False if `configure' has already been called.
      */
    bool get_read_only() const
      { return read_only; }
      /* [SYNOPSIS]
           Returns true if `set_read_only' has been called prior to the
           first `configure' call.
      */
    KDU_EXPORT bool configure(int num_comps, kdu_uint32 mct_hash);
      /* [SYNOPSIS]
           This function can be called multiple times, but non-initial calls
           return false unless the number of components (`num_comps') and
           the `mct_hash' value are identical to those passed in the first
           call.  If there is an external representation of stored background
           statistics to begin with, the first call to this function will be
           to deposit those statistics in this object and its descendants, and
           the second call will occur when `kdu_cplex_analysis' tries to use
           the background statistics, failing if they are not compatible.  If
           there was no external representation, the first call occurs when
           a `kdu_cplex_analysis' object first tries to discover background
           statistics.  In any case, the first call to this function configures
           the internal `kdu_cplex_bkgnd_comp' resources, initially in an
           empty state, after which they can be accesed via `access_component'.
           [//]
           The function is thread-safe, so you it is acceptable to have
           multiple threads asynchronously competing to be the first to call
           `configure'.
         [ARG: mct_hash]
           Ultimately derived from `kdu_tile::get_global_attributes', this
           argument allows for a reasonably robust check of whether background
           statistics accumulated from tiles of some images are potentially
           usable in the compression of new tiles.
      */
    int get_num_components(kdu_uint32 &mct_hash)
      { mct_hash = this->mct_hash_value; return num_components; }
      /* [SYNOPSIS]
           Provided as a consistent mechanism to facilitate externalizing
           statistics that might be accumulated within descendant
           `kdu_cplex_bkgnd_level' objects, so that they can be made available
           as background statistics for other compression tasks.
           Typically, the external format would record the number of components
           and the `mct_hash' value, so that they can be used to
           `configure' an instance of this object in the future.
         [RETURNS]
           0 if `configure' has not yet been called.
         [ARG: mct_hash]
           Used to return the hash code passed to `configure' that identifies
           the nature of any multi-component transform.
      */
    kdu_cplex_bkgnd_comp *access_component(int comp_idx)
      { 
        if ((comp_idx < 0) || (comp_idx >= num_components)) return NULL;
        return comps[comp_idx];
      }
      /* [SYNOPSIS]
           Returns NULL if `configure' has not been called, or if `comp_idx'
           lies outside the range 0 to C-1, where C is the `num_comps'
           value passed to `configure'.  Otherwise, the function returns a
           `kdu_cplex_bkgnd_comp' object, that itself needs to be
           `configure'd at least once.
      */
    KDU_EXPORT bool
      get_info(kdu_uint32 &num_levels,
               kdu_uint32 &incompatible_non_ll_subband_tally,
               kdu_uint32 &incompatible_component_tally,
               kdu_uint32 &incompatible_get_rel_stats_calls,
               kdu_uint32 &store_new_stats_calls,
               kdu_uint32 &incompatible_store_new_stats_calls);
      /* [SYNOPSIS]
           You can use this function at any time, to collect a snapshot of
           the way in which statistics are being used and/or updated.
           Normally, however, you would call the function at the end, after
           all processing is complete.
         [RETURNS]
           True unless the `configure' function has not yet been called.
         [ARG: incompatible_non_ll_subband_tally]
           Used to return the total number of subbands, other than LL
           subbands, that have been noted as incompatible, either by calling
           `kdu_cplex_bkgnd_level::note_incompatibility', or by calling
           `kdu_cplex_bkgnd_comp::note_incompatibility'.  In practice, the
           former is invoked from within the `kdu_cplex_analysis' machinery,
           while the latter is invoked from `kdu_multi_analysis::create'.
           If there are multiple tiles, this tally can be much larger than
           the total number of non-LL subbands managed by
           `kdu_cplex_bkgnd_level' objects, since these calls may be issued
           separately in each tile.
         [ARG: incompatible_component_tally]
           Used to return the total number of calls to
           `kdu_cplex_bkgnd_comp::note_incompatibility', which is typically
           invoked only from `kdu_multi_analysis::create' when it finds that a
           background component record is structurally incompatible with the
           tile-component that is being initialized.  If there are multiple
           tiles, this tally can be larger than the total number of
           components, as returned by `get_num_components'.
         [ARG: incompatible_get_rel_stats_calls]
           Used to return the number of calls to the
           `kdu_cplex_bkgnd_level::get_rel_stats' function that have returned
           false because the supplied `eps_mu' argument was deemed
           incompatible with the recorded background statistics -- bands for
           which `kdu_cplex_bkgnd_level::set_bkgnd_stats' has not been called
           are not counted in this tally. These calls only apply to
           subbands belonging to compatible levels of compatible components.
         [ARG: store_new_stats_calls]
           Used to return the total number of calls to the
           `kdu_cplex_bkgnd_level::store_new_stats', excluding only calls
           that supplied invalid subband indices.
         [ARG: incompatible_store_new_stats_calls]
           Used to return the number of calls to the
           `kdu_cplex_bkgnd_level::store_new_stats' function whose `eps_mu'
           value is incompatible with that of any recorded background
           statistics for the same band, supplied previously via
           `kdu_cplex_bkgnd_level::store_bkgnd_stats'.  The new statistics
           are still recorded and ultimately may be saved in an external
           repository, but being incompatible with the original background
           statistics means that the original ones will be discarded.
      */
  private: // Helper functions used only by `kdu_cplex_bkgnd_comp'
    friend class kdu_cplex_bkgnd_comp;
    void acquire_configure_lock()
      { if (!have_configure_lock) configure_mutex.lock(); }
    void release_configure_lock()
      { if (!have_configure_lock) configure_mutex.unlock(); }
  private: // Data
    bool read_only;
    bool have_configure_lock; // Prevent recursive attempts to lock the mutex
    int num_components;
    kdu_uint32 mct_hash_value; // Identifies the multi-component transform
    kdu_cplex_bkgnd_comp **comps;
    kdu_mutex configure_mutex; // Only used to protect `configure' calls
  };

/*****************************************************************************/
/*                              kdu_cplex_bkgnd_comp                         */
/*****************************************************************************/

class kdu_cplex_bkgnd_comp { 
  /* [SYNOPSIS]
       Instances of this object are obtained exclusively via
       `kdu_cplex_bkgnd::access_component'; you cannot construct them
       yourself from an application.  Typically, at the application level
       you would only invoke `kdu_cplex_bkgnd::access_component' to
       import background statistics from an external repository (e.g., a file)
       or export newly accumulated statistics to such a repository.  The
       internal machinery also uses this function to access and/or accumulate
       background scene complexity statistics.
  */
  private: // Inaccessible lifecycle functions
    friend class kdu_cplex_bkgnd;
    kdu_cplex_bkgnd_comp(kdu_cplex_bkgnd *owner);
    ~kdu_cplex_bkgnd_comp();
  public: // Member functions
    KDU_EXPORT bool configure(int num_levels, kdu_uint32 dwt_hash,
                              int guard_bits, int reversible_precision);
      /* [SYNOPSIS]
           This function can be called multiple times, but non-initial calls
           return false unless the `dwt_hash', `guard_bits' and
           `reversible_precision' value are consistent with those pased in the
           first call.  If there is an external representation of stored
           background statistics to begin with, the first call to this function
           will be to deposit those statistics in this object and its
           descendants, and the second call will occur when
           `kdu_cplex_analysis' tries to use the background statistics,
           failing if they are not compatible.  If there was no
           external representation, the first call occurs when a
           `kdu_cplex_analysis' object first tries to discover background
           statistics.
           [//]
           The first call to this function configures `num_levels'
           `kdu_cplex_bkgnd_level' objects.  If the owning
           `kdu_cplex_bkgnd' object is in the `read_only' state,
           subsequent `configure' calls have no impact upon the number of
           such level objects.  If not in the `read_only' state, however,
           each call to this function may augment the number of available
           levels, without invalidating or altering any of the existing
           `kdu_cplex_bkgnd_level' objects.
           [//]
           All arguments to this function can be obtained using the function
           `kdu_tile_comp::get_global_attributes'.
         [ARG: num_levels]
           This is the number of levels of spatial decomposition.  The levels
           are accessed by depth d in the decomposition, with d=1
           corresponding to the first level and d=`num_levels' being the
           last level.  Each level includes an LL subband, which might or might
           not have statistics, allowing images/tiles with different numbers of
           DWT levels to share a common set of background statistics.  The
           degenerate case d=0 has only the LL subband, providing any
           statistics that might be available for encoding image component
           samples without any spatial transformation at all.
         [ARG: dwt_hash]
           See `kdu_tile_comp::get_global_attributes' for a description.  This
           argument allows for a reasonably robust check of whether background
           statistics accumulated from tile-components of other images are
           potentially usable in the compression of a current image.
         [ARG: guard_bits]
           This is ultimately obtained from the `Qguard' parameter attribute.
           The number of guard bits affects the number of empty MSBs one can
           expect in a code-block, which strongy affects the meaning of
           accumulated background statistics.
         [ARG: reversible_precision]
           This is 0 if the spatial transform is irreversible, in which case
           the meaning of the statistics of the bits coded within any given
           code-block depends primarily on the number of `guard_bits' and the
           `eps_mu' quantization parameters managed by `kdu_cplex_bkgnd_level'.
           Otherwise, the spatial transform is reversible and the
           `reversible_precision' value is the bit-depth of the original
           compressed samples.
           [//]
           It is possible to accommodate the statistics produced by reversibly
           compressing images with one bit-depth to the reversible compression
           of images with another bit-depth, by adjusting the `eps_mu'
           parameters accordingly.  For the moment, however, we just insist
           that reversibly compressed content with different bit-depths
           yields incompatible statistics.
      */
    void note_incompatibility()
      { incompatible_cnt.exchange_add(1); }
      /* [SYNOPSIS]
           This function is normally called only from within
           `kdu_multi_analysis::create', upon a false return from
           `configure' -- counting the number of false returns from `configure'
           is less informative, since such calls may fail elsewhere, such as
           within `kdu_analysis_fusion'.  The tally here is reported by calls
           to `kdu_cplex_bkgnd::get_info'.
      */
    bool get_read_only() const
      { return read_only; }
      /* [SYNOPSIS]
           Returns the same value as `kdu_cplex_bkgnd::get_read_only'.
      */
    bool get_reversible() const
      { return (rev_prec != 0); }
      /* [SYNOPSIS]
           Returns true if this component uses a reversible transform.
      */
    int get_num_levels(kdu_uint32 &dwt_hash, int &num_guard_bits,
                       int &reversible_precision)
      { 
        dwt_hash = this->dwt_hash_value;
        num_guard_bits = this->guard_bits;
        reversible_precision = this->rev_prec;
        return this->max_depth;
      }
      /* [SYNOPSIS]
           Provided as a consistent mechanism to facilitate externalizing
           statistics that might be accumulated within descendant
           `kdu_cplex_bkgnd_level' objects, so that they can be made available
           as background statistics for other compression tasks.
           Typically, the external format would record the number of levels,
           the `dwt_hash', the number of `guard_bits' and the
           `reversible_precision', so that they can be used to
           `configure' an instance of this object in the future.
         [RETURNS]
           -1 if `configure' has not yet been called.  Note that 0 levels can
           correspond to a legitimate situation, in which there is no
           wavelet decomposition at all.  As explained with `configure', the
           depth d at which subband statistics are accessed via
           `access_level' can range from 0 to `num_levels', inclusive, with
           d=0 returning statistics for a (usually hypothetical) LL0 band that
           corresponds to the original image component samples, without any
           spatial transformation.
         [ARG: dwt_hash]
           Used to return the hash code passed to `configure' that identifies
           the nature of the spatial transform that produces this object's
           levels.
         [ARG: num_guard_bits]
           Used to return the number of guard bits passed to `configure'.
         [ARG: reversible_precision]
           Used to return the reversible precision value passed to
           `configure', which is 0 for non-reversible transforms and the
           sample bit-depth for reversible transforms.
      */
    kdu_cplex_bkgnd_level *access_level(int depth)
      { 
        if ((depth < 0) || (depth > max_depth))
          return NULL;
        return levels[depth];
      }
      /* [SYNOPSIS]
           Returns the relevant `kdu_cplex_bkgnd_level' object, through which
           you access and manage background statistics.  You should not be
           calling this function with values outside the range 0 to L, where
           L is the `num_levels' value passed to configure, but if you do the
           return value will be NULL.
           [//]
           Newly configured levels are initially empty, meaning that they
           require their own `configure' call.  The only exception to this is
           the level at `depth'=0, which has already been configured with a
           `type' value of 0 and a `num_bands' value of 1, which means that it
           has only the LL0 band, that represents the component samples prior
           to any spatial transformation at all.
      */
  private: // Helper functions used only by `kdu_cplex_bkgnd_level'
    friend class kdu_cplex_bkgnd_level;
    void acquire_configure_lock()
      { if (!have_configure_lock) owner->acquire_configure_lock(); }
    void release_configure_lock()
      { if (!have_configure_lock) owner->release_configure_lock(); }
  private: // Data
    kdu_cplex_bkgnd *owner;
    bool read_only;
    bool have_configure_lock; // Avoid recursive lock acquisition calls.
    int max_depth; // Identifies the number of decomposition levels
    kdu_uint32 dwt_hash_value; // Identifies the spatial transform
    int guard_bits; // This is the Qguard value
    int rev_prec; // 0 if transform not reversible, else bit-depth
    kdu_cplex_bkgnd_level *levels[33]; // Valid entries from 0 to `max_depth'
    kdu_interlocked_int32 incompatible_cnt;
  };

/*****************************************************************************/
/*                             kdu_cplex_bkgnd_level                         */
/*****************************************************************************/

class kdu_cplex_bkgnd_level { 
  private: // Inaccessible lifecycle functions
    friend class kdu_cplex_bkgnd;
    friend class kdu_cplex_bkgnd_comp;
    kdu_cplex_bkgnd_level(kdu_cplex_bkgnd_comp *owner);
    ~kdu_cplex_bkgnd_level();
  public: // Member functions
    KDU_EXPORT bool
      configure(int num_bands, kdu_uint32 style, kdu_coords depth);
      /* [SYNOPSIS]
           This function can be called multiple times, but non-initial calls
           return false unless the `num_bands', `style' and `depth'
           values are consistent with those pased in the first call.  If there
           is an external representation of stored background statistics to
           begin with, the first call to this function will be to deposit those
           statistics in this object, and the second call will occur when
           `kdu_cplex_analysis' tries to use the background statistics,
           failing if they are not compatible.  If there was no
           external representation, the first call occurs when a
           `kdu_cplex_analysis' object first tries to discover background
           statistics.
           [//]
           The first call to this function configures resources to store
           statistics for `num_bands' subbands, noting that the first band
           is always the LL subband for the level, even if that subband is
           passed on to lower levels in the DWT hierarchy so that it has no
           statistics of its own.
         [ARG: num_bands]
           Note that `num_bands' is actually derivable from `style', as the
           value returned by `cod_params::expand_decomp_bands', when invoked
           with `style' as its `decomp_val' argument. The band count for each
           level always includes the LL subband produced by that level, even
           though the LL band usually further decomposed by the next level.
         [ARG: style]
           This is the `Cdecomp' value for the level in question, as returned
           by `kdu_resolution::get_decomp_structure' function.
         [ARG: depth]
           This argument identifies the number of horizontal and vertical
           low-pass filtering and subsampling stages that have been applied
           to the original tile-component, in each direction, prior to the
           point at which this level's subband decomposition is applied, as
           returned by `kdu_resolution::get_decomp_structure'.
      */
    void note_incompatibility(int band_idx)
      { 
        if (band_idx < 0)
          incompatible_non_ll_cnt.exchange_add(num_bands-1);
        else if ((band_idx > 0) && (band_idx < num_bands))
          incompatible_non_ll_cnt.exchange_add(1);
      }
      /* [SYNOPSIS]
           Usually called only from within `kdu_cplex_analysis' when
           `configure' returns false.  The purpose is to gather information
           that is ultimately reported via the `kdu_cplex_bkgnd::get_info'
           function.  If `band_idx' is 0 or greater than or equal to B, the
           number of bands originally passed to `configure', the function does
           nothing.  A -ve value add all non-LL subbands (i.e., B-1) to the
           internal tally that is ultimately reported as the incompatible
           non-LL tally by `kdu_cplex_bkgnd::get_info'.  All other values
           (valid non-LL band indices) add 1 to this internal tally.
      */
    int get_num_bands(kdu_uint32 &style, kdu_coords &depth)
      { 
        style = this->decomp_style;
        depth.x = this->decomp_depth_x;
        depth.y = this->decomp_depth_y;
        return num_bands;
      }
      /* [SYNOPSIS]
           Provided as a consistent mechanism to facilitate externalizing
           the statistics collected, so that they can be made available
           as background statistics for other compression tasks.
           [//]
           Returns 0 only if `configure' has not been called.  For the
           special level at depth d=0, `configure' does not need to be called
           and can have no effect - for that level, this function always
           returns 1, setting `style' to 0.
      */
    kdu_uint32 get_compatibility_stats(kdu_uint32 &new_stats_cnt,
                                       kdu_uint32 &incompatible_new_stats_cnt)
      { 
        new_stats_cnt = this->new_stats_count;
        incompatible_new_stats_cnt = this->incompatible_new_stats_count;
        return (kdu_uint32) this->incompatible_rel_stats_count.get();
      }
      /* [SYNOPSIS]
           These functions are provided for the benefit of a background
           statistics manager that may be responsible for importing existing
           statistics from an external repository and/or updating that
           repository.  They report the values of internal counters, whose
           purpose is to help flag incompatible quantization parameters.
           Without these counters, incompatible qantization parameters simply
           result in failure to include existing background statistics, which
           might go unnoticed by a user who has perhaps supplied the wrong
           background statistics database.
         [RETURNS]
           The number of calls to `get_bkgnd_stats' that returned NULL because
           the supplied `eps_mu' value was incompatible with that supplied
           to `store_bkgnd_stats'.
         [ARG: new_stats_cnt]
           Used to return the total number of successful calls to
           `store_new_stats'.
         [ARG: incompatible_new_stats_cnt]
           Used to return the number of successful calls to `store_new_stats'
           whose `eps_mu' value was deemed incompatible with that of any
           background statistics supplied to `store_bkgnd_stats'.  If
           `store_bkgnd_stats' was never called for a subband, new statistics
           stored for that subband are deemed compatible.
      */
    KDU_EXPORT bool store_bkgnd_stats(int band_idx, kdu_uint16 eps_mu,
                                      int num_entries, const float *abs_stats,
                                      const float *abs_samples);
      /* [SYNOPSIS]
           If there is an external source of background statistics, this
           function should be called for each available band, right after
           `configure'; it creates and populates the array returned by
           `get_rel_stats', which is used by `kdu_cplex_analysis' to discover
           any available background statistics to assist in the generation of
           robust forecasts in calls to `kdu_node::exchange_cplex_records'.
         [RETURNS]
           False if the band does not exist or has already been used to store
           background statistics.
         [ARG: band_idx]
           Must lie in the range 0 to B-1, where B was the `num_bands' argument
           to `configure'.  Otherwise, the function returns false.
         [ARG: eps_mu]
           This value is ultimately derived from `kdu_subband::get_eps_mu'.
           It effectively records the quantization conditions.  For reversible
           transforms, the `mu' part of `eps_mu' (11 LSBs) should be 0, but is
           ignored in any case.
         [ARG: num_entries]
           This is the number of entries in each of the `abs_stats' and
           `abs_samples' arrays.  Normally, this would be equal to
           `KDU_CPLEX_REC_MAX_ENTRIES' but it does not have to be.  If fewer
           entries are supplied, any extra ones are deemed to have no
           samples -- i.e., no basis for complexity estimates.
         [ARG: abs_stats]
           This array supplies expected compressed byte counts for the number
           of samples supplied via the `abs_samples' array.  Internally, the
           ratio is taken to produce the array returned by `get_rel_stats'.
         [ARG: abs_samples]
           See `abs_stats' -- each entry in the array can record a different
           number of samples, meaning that some bit-planes might have been
           seen more often than others during the collection of background
           statistics, but in most cases all entries will be the same.  No
           entry should be less than 0.  Entries in the `abs_stats' array that
           are meaningless, should have a corresponding `abs_samples' value
           of 0; once this happens, however, all subsequent entries are also
           considered meaningless.
           [//]
           If the first entries of the `abs_samples' array are 0, the supplied
           information is discarded, as if the call had never been made.
      */
    const float *get_rel_stats(int band_idx, kdu_uint16 eps_mu);
      /* [SYNOPSIS]
           Used by `kdu_cplex_analysis' to recover relative (normalized)
           background statistics, expressed in bytes/sample.  The returned
           array has `KDU_CPLEX_REC_MAX_ENTRIES' entries, if non-NULL, and
           can be used to assist in forecasting coding costs for samples that
           have not yet been seen.  A typical approach is to forecast coding
           costs by extrapolating from already seen samples of the same
           subband, in the same image, but to derive a conservative lower bound
           for this forecast coding cost from the background statistics
           returned here.
           [//]
           Note that this function is used only internally, so might not be
           exported from the kakadu core system.
         [RETURNS]
           NULL if `store_bkgnd_stats' has not been invoked with valid
           statistics for this subband, or if `band_idx' is invalid, or
           `eps_mu' is not sufficiently close to the value passed to
           `store_bkgnd_stats'.
           [//]
           Otherwise, the array contains valid expected bytes/sample counts,
           for each magnitude bit-plane, starting from the coarsest one.  All
           `KDU_CPLEX_REC_MAX_ENTRIES' entries of the array are populated with
           monotonically increasing values, some of which may have been
           synthesized following the procedure described with the definition
           of `kdu_cplex_record', assuming that each successive bit-plane adds
           an expected coding cost of 1.25 bits per sample.
         [ARG: band_idx]
           Must lie in the range 0 to B-1, where B was the `num_bands' argument
           to `configure'.  Otherwise, the function returns NULL.
         [ARG: eps_mu]
           This value is derived from `kdu_subband::get_eps_mu'; it is compared
           against the value passed to `store_bkgnd_stats'. If the two do not
           sufficiently agree (small differences in \mu are acceptable, but not
           the \eps component in the 5 MSBs), the function returns NULL.
      */
    bool store_new_stats(int band_idx, kdu_uint16 eps_mu, int num_entries,
                         const float *new_stats, float new_samples);
      /* [SYNOPSIS]
           This function is used by `kdu_cplex_analysis' to record accumulated
           statistics for a subband, once compression for a tile is complete.
           The `new_stats' array has `num_entries' entries, holding total
           expected byte counts (per bit-plane) associated with observed
           subband samples.  The `new_samples' argument holds the corresponding
           number of subband samples that gave rise to these expected coding
           bytes.
           [//]
           The function does nothing if `kdu_cplex_bkgnd' is in
           the `read_only' state; otherwise, any updates are made within a
           protected critical section, allowing updates to be made
           asynchronously as processing completes for different tiles of an
           image that is being encoded in a multi-threaded way.
           [//]
           Note that this function is used only internally, so might not be
           exported from the kakadu core system.
         [RETURNS]
           The function does nothing, returning false, if the controlling
           `kdu_cplex_bkgnd' object is in the `read_only' state.
           Similarly, if `band_idx' is invalid, the function does nothing,
           returning false.
         [ARG: band_idx]
           Must lie in the range 0 to B-1, where B was the `num_bands' argument
           to `configure'.  Otherwise, the function returns false, doing
           nothing.
         [ARG: eps_mu]
           This value is derived from `kdu_subband::get_eps_mu'.  It does not
           need to agree with the `eps_mu' value passed to `configure', but
           in this case the accumulated statistics returned by `get_acc_stats'
           cannot include a contribution from any pre-existing background
           statistics that were passed to `store_bkgnd_stats'.
      */
    KDU_EXPORT int
      get_acc_stats(int band_idx, kdu_uint16 &eps_mu, int max_entries,
                    float *acc_stats, float *acc_samples);
      /* [SYNOPSIS]
           This function is provided to support externalization of collected
           background statistics.  If the `kdu_cplex_bkgnd' object was
           constructed in the `read_ony' state, or neither of the
           `store_bkgnd_stats' nor `store_new_stats' functions has been called
           for `band_idx', the function returns 0, meaning that there is no
           information.
           [//]
           In all other cases, the function writes meaningful information to
           the first N entries of the `acc_stats' and `acc_samples' arrays,
           where N is the function's return value and will not exceed
           `max_entries', and `eps_mu' is set equal to an appropriate
           non-zero value that describes these statistics.
           [//]
           If there has been a call to `store_bkgnd_stats' for the indicated
           band, but not for `store_new_stats', the `eps_mu' value is set to
           the same value passed to `store_bkgnd_stats', and the returned
           statistics will essentially be identical to thsoe passed to
           `store_bkgnd_stats', except that if `eps_mu' was 0, it is
           converted to 1 here.
           [//]
           The same applies if only `store_new_stats' has been called, and
           not `store_bkgnd_stats'.
           [//]
           If both functions have been called, the `eps_mu' value remains
           equal to the one passed to `store_bkgnd_stats' for so long as
           the one passed to `store_new_stats' can be considered compatible,
           but if not, then the `eps_mu' value is taken from the information
           passed to `store_new_stats' and the accumulated statistics returned
           here do not include the background statistics originally passed to
           `store_bkgnd_stats'.
         [RETURNS]
           The number of valid entries written to the `acc_stats' and
           `acc_samples' arrays.  All entries written to `acc_samples' are
           non-zero, so if a 0 is encountered internally, the presence of that
           0 restricts the number of valid entries returned here.
         [ARG: band_idx]
           Must lie in the range 0 to B-1, where B was the `num_bands' argument
           to `configure'.  Otherwise, the function returns 0, doing
           nothing.
         [ARG: eps_mu]
           Used to return the `eps_mu' value that best describes the
           statistics recorded in `acc_stats' and `acc_samples'.
         [ARG: max_entries]
           Maximum number of entries that can be written to the `acc_stats'
           and `acc_samples' arrays.
         [ARG: acc_stats]
           Returns expected coding costs for each magnitude bit-plane, starting
           from the most significant one, expressed in total bytes across all
           of the samples returned via the corresponding entries of the
           `acc_samples' array.
         [ARG: acc_samples]
           See `acc_stats' for an explanation.  In most cases, all entries of
           this array will be identical, but it can happen that more
           information is available for some bit-planes than others so that
           `acc_samples' entries differ.  No valid entry in this array will
           be zero.
      */

  private: // Data
    kdu_cplex_bkgnd_comp *owner;
    kdu_byte decomp_depth_x; // These are the decomposition depth values
    kdu_byte decomp_depth_y; // from `kdu_resolution::get_decomp_structure'.
    kdu_uint32 decomp_style; // The `Cdecomp' value for this DWT level
    int num_bands; // Num bands in the level, always starting with LL
    kdu_uint32 *band_eps_mu; // `num_bands' entries -- see below for format
    float *rel_stats; // KDU_CPLEX_REC_MAX_ENTRIES x `num_bands' (bytes/sample)
    float *acc_stats; // KDU_CPLEX_REC_MAX_ENTRIES x `num_bands' (acc bytes)
    float *acc_samples;  // KDU_CPLEX_REC_MAX_ENTRIES x `num_bands' entries
    kdu_mutex accumulation_mutex; // Protects calls to `store_new_stats'
    kdu_interlocked_int32 incompatible_rel_stats_count;
    kdu_uint32 new_stats_count; // Number of times `store_new_stats is called
    kdu_uint32 incompatible_new_stats_count; // Num times `store_new_stats'
                                 // calls eps-mu incompatible with rel-eps-mu.
    kdu_interlocked_int32 incompatible_non_ll_cnt; // This one counts bands
                                     // identified by `note_incompatibility'.
  };
  /* Notes:
       The low 16 bits of each `band_eps_mu' entry hold the eps-mu value
     associated with the `rel_stats' data, which is also copied to
     `acc_stats' and `acc_samples' when `store_bkgnd_stats' is called and
     we are not in read-only mode.  The high 16 bits of each entry hold
     the eps-mu value last passed to the `store_new_stats' function, if  any,
     for the same band.  Usually, these should be identical, but they might not
     be.  We do not mix statistics from different eps-mu pairs, so if
     `store_new_stats' was called with a different `eps_mu' value, the
     accumulated statistics do not include the original background statistics
     stored via `store_bkgnd_stats', and this information is reported via the
     `get_acc_stats' function's return value.
        Internally, we treat 0 as an invalid eps-mu parameter to
     identify values that have not yet been set.  If the `eps_mu' argument to
     `store_bkgnd_stats', `get_rel_stats' or `store_new_stats' happens to equal
     0, it is converted to 1 internally, which will not cause any significant
     issues.
  */

/*****************************************************************************/
/*                kdu_multi_analysis/synthesis create options                */
/*****************************************************************************/

#define KDU_MULTI_XFORM_PRECISE                      ((int) 0x00000001)
#define KDU_MULTI_XFORM_FAST                         ((int) 0x00000002)
#define KDU_MULTI_XFORM_SKIPYCC                      ((int) 0x00000004)

#define KDU_MULTI_XFORM_DBUF                         ((int) 0x00000100)
#define KDU_MULTI_XFORM_MT_DWT                       ((int) 0x00000100)
#define KDU_MULTI_XFORM_DELAYED_START                ((int) 0x00000400)

#define KDU_MULTI_XFORM_DEFAULT_FLAGS 0

/*****************************************************************************/
/*                          kd_multi_analysis_base                           */
/*****************************************************************************/

class kd_multi_analysis_base { 
  public: // Member functions
    virtual ~kd_multi_analysis_base() { return; }
    virtual void terminate_queues(kdu_thread_env *env) = 0;
      /* Called prior to the destructor if `env' is non-NULL, this function
         terminates tile-component queues created for multi-threading. */
    virtual kdu_coords get_size(int comp_idx) = 0;
    virtual kdu_line_buf *
      exchange_line(int comp_idx, kdu_line_buf *written,
                    kdu_thread_env *env) = 0;
    virtual bool is_line_precise(int comp_idx) = 0;
    virtual bool is_line_absolute(int comp_idx) = 0;
    virtual kdu_byte get_line_flags(int comp_idx) = 0;
  };

/*****************************************************************************/
/*                           kdu_multi_analysis                              */
/*****************************************************************************/

class kdu_multi_analysis { 
  /* [BIND: interface]
     [SYNOPSIS]
       This powerful object generalizes the functionality of `kdu_analysis'
       to the processing of multiple image components, allowing all the
       data for a tile to be managed by a single object.  The object
       creates the `kdu_analysis' objects required to process each
       codestream image component, but it also implements Part-2 non-linear
       point transforms (NLT), if applicable, plus Part-1 colour
       decorrelation transforms and Part-2 generalized multi-component
       transforms, as required.
       [//]
       Objects of this class serve as interfaces.  The constructor
       simply creates an empty interface, and there is no meaningful
       destructor.  This means that you may copy and transfer objects
       of this class at will, without any impact on internal resources.
       To create a meaningful insance of the internal machine, you must
       use the `create' member.  To destroy the internal machine you
       must use the `destroy' member.
       [//]
       From Kakadu version 7.5, you can pass a reference to an external
       `kdu_sample_allocator' object to be used in place of the internal
       one.  This is useful in NUMA multi-processor systems where the
       haphazard creation and destruction of `kdu_multi_analysis' engines
       might otherwise result in the sample data memory being allocated
       disadvantageously.  By retaining an allocated memory buffer inside
       an external `kdu_sample_allocator' you can arrange for that buffer to
       reside in memory that is both stable across multiple create/destroy
       events and close to a processor that may be associated with the
       threads in a `kdu_thread_env' multi-threaded environment that you
       are using.  In the future, `kdu_sample_allocator' may be extended to
       include additional customization tools that could be brought to bear
       upon the `kdu_multi_analysis' engine by use of this new external
       allocator feature.
    */
  public: // Member functions
    kdu_multi_analysis() { state = NULL; }
      /* [SYNOPSIS]
           Leaves the interface empty, meaning that the `exists' member
           returns false.
      */
    bool exists() { return (state != NULL); }
      /* [SYNOPSIS]
           Returns true after `create' has been called; you may also
           copy an interface whose `create' function has been called. */
    bool operator!() { return (state == NULL); }
      /* [SYNOPSIS]
           Opposite of `exists', returning false if the object represents
           an interface to instantiated internal processing machinery.
      */
    KDU_EXPORT kdu_long
      create(kdu_codestream codestream, kdu_tile tile,
             kdu_thread_env *env, kdu_thread_queue *env_queue,
             int flags, kdu_roi_image *roi=NULL, int buffer_rows=1,
             kdu_sample_allocator *external_sample_allocator=NULL,
             const kdu_push_pull_params *extra_params=NULL,
             kdu_membroker *membroker=NULL);
      /* [SYNOPSIS]
           Use this function to create an instance of the internal
           processing machinery, for compressing data for the supplied
           open `tile' interface.  Until you call this function (or copy
           nother object which has been created), the `exists' function
           will return false.
           [//]
           Multi-component transformations performed by this function are
           affected by previous `kdu_tile::set_components_of_interest'
           calls.  In particular, you need only supply those components
           which have been marked of interest via the `exchange_line'
           function.  Those components marked as uninteresting are
           ignored -- you can pass them in via `exchange_line' if you like,
           but they will have no impact on the way in which codestream
           components are generated and subjected to spatial wavelet
           transformation and coding.
           [//]
           If insufficient components are currently marked as being
           of interest (i.e., too many components were excluded in a
           previous call to `kdu_tile::set_components_of_interest'), the
           present object might not be able to find a way of inverting
           the multi-component transformation network, so as to work back
           to codestream image components.  In this case, an informative
           error message will be generated through `kdu_error'.
           [//]
           This function takes optional `env' and `env_queue' arguments to
           support a variety of multi-threaded processing paradigms, to
           leverage the capabilities of multi-processor platforms.  To see
           how this works, consult the description of these arguments below.
           Also, pay close attention to the use of `env' arguments with the
           `exchange_line' and `destroy' functions.
           [//]
           The `flags' argument works together with subsequent arguments
           to control internal memory allocation, flow control and
           optimization policies.  See below for a description of the
           available flags.
           [//]
           The `external_sample_allocator' and `membroker' arguments provide
           rich options for taking control over memory allocation, as explained
           below.  Memory brokering services can be supplied either through an
           explicit `membroker' or by providing the broker to an
           `external_sample_allocator' via a prior call to
           `kdu_sample_allocator::configure'.  Ultimately, however, the
           use of an `external_sample_allocator' provides you with the most
           options.  In particular, this is the only way to explicitly specify
           the fragment size used during the allocation of sample data
           blocks within `kdu_sample_allocator'.
         [RETURNS]
           Returns the number of bytes which have been allocated internally
           for the processing of multi-component transformations,
           spatial wavelet transforms and intermediate buffering between
           the wavelet and block coder engines.  Essentially, this
           includes all memory resources for the tile.
         [ARG: env]
           Supply a non-NULL argument here if you want to enable
           multi-threaded processing.  After creating a cooperating
           thread group by following the procedure outlined in the
           description of `kdu_thread_env', any one of the threads may
           be used to construct this processing engine.  Separate
           processing queues will automatically be created for each
           image component.
           [//]
           If the `KDU_MULTI_XFORM_MT_DWT' flag (or the `KDU_MULTI_XFORM_DBUF'
           flag) is supplied, these queues will also be used to schedule the
           spatial wavelet transform operations associated with each image
           component as jobs to be processed asynchronously by different
           threads.  Regardless multi-threaded DWT processing is requested,
           within each tile-component, separate queues are created to allow
           simultaneous processing of code-blocks from different subbands.
           [//]
           By and large, you can use this object in exactly the same way
           when `env' is non-NULL as you would with a NULL `env' argument.
           That is, the use of multi-threaded processing can be largely
           transparent.  However, you must remember the following two
           points:
           [>>] You must supply a non-NULL `env' argument to the
                `exchange_line' function -- it need not refer to the same
                thread as the one used to create the object here, but it
                must belong to the same thread group.
           [>>] You cannot rely upon all processing being complete until you
                invoke the `kdu_thread_entity::join' or
                `kdu_thread_entity::terminate' function.
         [ARG: env_queue]
           This argument is ignored unless `env' is non-NULL.  When `env'
           is non-NULL, all thread queues that are created inside this object
           are added as sub-queues of `env_queue'.  If `env_queue' is NULL,
           they are added as top-level queues in the multi-threaded queue
           hierarchy.  The present object creates one internal queue for each
           tile-component, to which each subband adds a sub-queue managed by
           `kdu_encoder'.
           [//]
           One advantage of supplying a non-NULL `env_queue' is that it
           provides you with a single hook for joining with the completion
           of all the thread queues which are created by this object
           and its descendants -- see `kdu_thread_entity::join' for more on
           this.
           [//]
           A second advantage of supplying a non-NULL `env_queue' is that
           it allows you to manipulate the sequencing indices that are
           assigned by the thread queues created internally -- see
           `kdu_thread_entity::attach_queue' for more on the role played by
           sequencing indices in controlling the order in which work is
           actually done.  In particular, if `env_queue' is initially
           added to the thread group with a sequencing index of N >= 0,
           all processing within all tile-components of the tile associated
           with this object will proceed with jobs having a sequence index
           of N.
           [//]
           Finally, the `env_queue' object supplied here provides a
           mechanism to determine whether or not calls to `excahnge_line'
           might potentially block.  This may be achieved by supplying
           an `env_queue' object whose `kdu_thread_queue::update_dependencies'
           function has been overridden, or by registering a dependency
           monitor (see `kdu_thread_queue::set_dependency_monitor') with
           the supplied `env_queue'.  If the number of potentially blocking
           dependencies identified by either of these mechanisms is 0,
           calls to `exchange_line' can be invoked at least once for each
           component index, without blocking the caller.  Otherwise, the
           caller might be temporarily blocked while waiting for
           dependencies to be satisfied by DWT analysis and/or subband
           encoding operations that are still in progress.  This temporary
           blocking is not a huge concern, since threads actually enter
           what we call a "working wait", using
           `kdu_thread_entity::wait_for_condition', during which they will
           often perform other tasks.  However, working waits can adversely
           affect cache utilization and often cause work to be done in a
           less than ideal sequence, so that other threads might go idle
           while waiting for jobs to be scheduled by a thread that is
           unduly delayed in a working wait.  For this reason, advanced
           implementations are offered the option of using the dependency
           analysis methods associated with an `env_queue' to schedule jobs
           only when it is known that they are fully ready to proceed.
         [ARG: flags]
           Controls the internal memory allocation, buffer management
           and various processing options.  This argument may be the
           logical OR of any appropriate combination of the flags listed
           below.  For convenience, the `KDU_MULTI_XFORM_DEFAULT_FLAGS'
           value may be used as a starting point, which will supply the
           flags that should always be present unless you have good reason
           not to include them.
           [>>] `KDU_MULTI_XFORM_PRECISE':  This option requests
                the internal machinery to work with 32-bit representations
                for all image component samples.  Otherwise, the internal
                machinery will determine a suitable representation precision,
                making every attempt to use lower precision processing
                paths, which are faster and consume less memory, so long
                as this does not unduly compromise quality.
           [>>] `KDU_MULTI_XFORM_FAST':  This option represents
                the opposite extreme in precision selection to that
                associated with `KDU_MULTI_XFORM_PRECISE'.  In fact,
                if both flags are supplied, the present one will be ignored.
                Otherwise, if the present flag is supplied, the function
                selects the lowest possible internal processing precision,
                even if this does sacrifice some image quality.
           [>>] `KDU_MULTI_XFORM_DBUF': Same as `KDU_MULTI_XFORM_MT_DWT' --
                see below.
           [>>] `KDU_MULTI_XFORM_MT_DWT': This flag is ignored
                unless `env' is non-NULL.  It specifies that the
                spatial DWT operations associated with each codestream
                image component should be executed as asynchronous jobs, as
                opposed to synchronously when new imagery is pushed in
                via the `exchange_line' function.  When the flag is present,
                the object allocates an internal buffer which can hold
                (approximately) 2*`buffer_rows' lines for each
                codestream tile-component.  In the simplest case (double
                buffering), this buffer is partitioned into two parts.  More
                generally, though, the implementation may partition the
                memory into a larger number of parts to improve
                performance.  At any given time, the MCT machinery may be
                writing data to one part while the DWT and block coding
                machinery may be supplied with data from another part.
                Multi-threaded DWT processing provides a useful
                mechanism for minimizing CPU idle time, since multiple threads
                can be scheduled not only to block encoding operations, but
                also to DWT analysis operations.  If DWT analysis is
                the bottleneck, it fundamentally limits the rate at which
                block encoding jobs can be made available to the
                multi-threaded machinery; the only way to overcome this
                bottleneck, if it exists, is to allocate multiple threads to
                DWT analysis via this flag.
         [ARG: roi]
           A non-NULL object may be passed in via this argument to
           allow for region-of-interest driven encoding.  Note carefully,
           however, that the component indices supplied to the
           `kdu_roi_image::acquire_node' function correspond to
           codestream image components.  It is up to you to ensure that
           the correct geometry is returned for each codestream image
           component, in the event that the source image components do not
           map directly (component for component) to codestream image
           components.
           [//]
           Note that simultaneous processing fragments (an important
           technology) are not currently compatible with region-of-interest
           encoding (a relatively unimportant technology, considering that
           region-of-interest is usually best to managed at the decoder side
           only).  For this reason, if your application does pass a non-NULL
           `roi' argument in here, you must ensure that each tile-component
           is processed using only one fragment -- this is best done by calling
           `kdu_codestream::configure_simultaneous_processing_fragments'
           explicitly, with zero-valued arguments, before attempting
           region-of-interest driven encoding.
         [ARG: buffer_rows]
           This argument must be strictly positive.  It is used to size the
           internal buffers used to hold data that finds its way to the
           individual codestream tile-components, while it is waiting to be
           passed to the DWT analysis and block coding machinery.
           [//]
           From KDU-7.2.1 on, a value of 0 is automatically translated to 1,
           while a -ve value may be supplied for this argument, in which case
           the buffers will be dimensioned automatically, taking the tile
           width and number of components into account.
           [//]
           There are two important cases to understand:
           [>>] If the DWT processing is performed in-line, in the same thread
                as that which calls `exchange_line', then there is only one
                buffer, with this number of rows, and the buffer must be
                filled (or the tile-component exhausted) before DWT processing
                is performed.  In this case, it is usually best to select
                the default value of `buffer_rows'=1 so that processing can
                start as early as possible and there is less chance that the
                image component data will be evicted from lower levels of the
                CPU cache before it is subjected to DWT analysis.  However,
                in the special case where multi-threaded processing is
                employed, with both `env' AND `env_queue' arguments to this
                function being non-NULL, and where the `env_queue' or
                one of the super-queues in its ancestry advertises its
                interest in propagated dependency information (through an
                overridden `kdu_thread_queue::update_dependencies' function
                that returns true, for example, or an installed dependency
                monitor), then there are some multi-threaded synchronization
                overheads associated with the onset and completion of the
                internal DWT analysis machinery (perhaps amounting to several
                hundred or even a thousand clock cycles), so in this case it
                may be better to select a larger value for `buffer_rows',
                especially where the line width is small.
           [>>] If the `KDU_MULTI_XFORM_MT_DWT' or `KDU_MULTI_XFORM_DBUF' flag
                is present, the DWT operations are performed by jobs that may
                be executed on any thread within the multi-threaded environment
                associated with the `env' argument.  In this case, for reasons
                of backward compatibility, the total amount of memory
                allocated for buffering tile-component lines is given (at
                least approximately) by 2*`buffer_rows', since this is what
                would be consumed if a double buffering strategy were
                employed.  In reality, the internal implementation may
                partition this total amount of memory into a larger number
                of smaller buffers so that processing can start earlier and
                the memory can be used more effectively to manage interruption
                of threads, or scheduling of thread resources temporarily to
                other jobs.
           [//]
           You may need to play with this parameter to optimize
           processing efficiency for particular applications.
           [//]
           You should be aware that the actual number of buffer lines
           allocated internally may be smaller than that requested.  For
           example, the implementation may limit the total amount of
           buffer memory to 256 lines.
         [ARG: external_sample_allocator]
           If desired, you may pass your own `kdu_sample_allocator' object in
           via this argument.  The most immediate benefit of doing this is
           that the allocator will not be deleted when the current object
           is `destroy'ed so you can pass the same allocator back into a newly
           created instances of the same object and hope to wind up using
           exactly the same memory space for sample data processing and
           subband buffers for block coding.  Since the buffers allocated by
           `kdu_sample_allocator' tend to be large, this may be very
           important, especially in NUMA (Non-Uniform Machine Architecture)
           platforms where most operating systems assign physical pages in
           memory based on the location of the CPUs that first touch them.
           [//]
           The `external_sample_allocator' may not safely be shared with any
           other object, since the internal machinery here invokes
           `kdu_sample_allocator::restart' and
           `kdu_sample_allocator::finalize'.
           [//]
           The `destroy' function will not delete any memory retained within
           an `external_sample_allocator' supplied here.
         [ARG: extra_params]
           This argument can be used to pass in additional parameters and
           even object references that customize the compression process.
           The most important uses of this argument are to pass references to
           `kdu_cplex_share' and `kdu_cplex_bkgnd' objects that improve the
           robustness of the Cplex-EST dynamic complexity constraint
           algorithm, when operated in low memory configurations.  See
           those objects for more information.
         [ARG: membroker]
           This optional argument allows you to supply an explicit memory
           broker, which will govern all memory allocation performed within
           this object in the event that `external_sample_allocator' is NULL.
           [//]
           If this argument is NULL, but `external_sample_allocator' is
           non-NULL, all memory allocation performed within this object is
           governed by any memory broker that was aupplied to
           `external_sample_allocator->configure'.
           [//]
           If both arguments are non-NULL, the `membroker' supplied here is
           used only to govern internal memory allocations associated with
           multi-component transforms and the `kdu_multi_analysis' framework
           itself.  The memory associated with spatial transformation and
           encoding processes, however, will be governed by the memory broker
           that was supplied to `external_sample_allocator->configure'; if
           there is none, this memory will not be brokered.
      */
    kdu_long
      create(kdu_codestream codestream, kdu_tile tile,
             bool force_precise=false, kdu_roi_image *roi=NULL,
             bool want_fastest=false, int buffer_rows=1,
             kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL,
             bool multi_threaded_dwt=false)
        { 
          int flags = KDU_MULTI_XFORM_DEFAULT_FLAGS;
          if (force_precise) flags |= KDU_MULTI_XFORM_PRECISE;
          if (want_fastest) flags |= KDU_MULTI_XFORM_FAST;
          if (multi_threaded_dwt) flags |= KDU_MULTI_XFORM_MT_DWT;
          return create(codestream,tile,env,env_queue,flags,roi,buffer_rows);
        }
        /* [SYNOPSIS]
             This form of the `create' function is provided for compatibility
             with applications created for Kakadu v6.x and earlier.  It
             simply invokes the first form of the function with a
             set of flags and parameter values that is usually appropriate.
             [//]
             This simplified form of the function lacks much of the
             flexibility and control offered by the first version of
             `create'.  As one important example, this version does not
             offer any mechanism to take control over memory allocation,
             since it offers neither the external `kdu_sample_allocator'
             option nor an explicit `kdu_membroker' reference, which are
             valuable features.  Taking these things into consideration,
             developers are strongly recommended to use the first version
             of `create' in all new application development.
             [//]
             Note that the `buffer_rows' argument may be known as
             `processing_stripe_height' in other versions of Kakadu, but
             it has the same interpretation -- see the first form of
             `create' for an explanation.
             [//]
             Similarly, the `multi_threaded_dwt' argument may be known as
             `double_buffering' in other versions of Kakadu, but the memory
             consumption and multi-threading implications are the same.
        */
    KDU_EXPORT void destroy(kdu_thread_env *env=NULL);
      /* [SYNOPSIS]
           Use this function to destroy the internal processing machine
           created using `create'.  The function may be invoked on any
           copy of the original object whose `create' function was called,
           so be careful.
         [ARG: env]
           If this argument is non-NULL, this function will terminate all
           work within the object before cleaning up its resources; this
           is done using `kdu_thread_entity::terminate'.
           [//]
           Otherwise, so long as you have explicitly used
           `kdu_thread_entity::join' or `kdu_thread_entity::terminate'
           already, it is OK to invoke this function with a NULL `env'
           argument.  There is nothing stopping you from joining multiple
           times on a queue, so you can use both approaches without any
           problems.
      */
    kdu_coords get_size(int comp_idx) { return state->get_size(comp_idx); }
      /* [SYNOPSIS]
           This is a convenience function to return the size of the image
           component identified by `comp_idx', as seen within the present
           tile.  The same information may be obtained by invoking
           `kdu_codestream::get_tile_dims' with its `want_output_comps'
           argument set to true.
      */
    kdu_line_buf *exchange_line(int comp_idx, kdu_line_buf *written,
                                kdu_thread_env *env=NULL)
      { return state->exchange_line(comp_idx,written,env); }
      /* [SYNOPSIS]
           Use this function to exchange image data with the processing
           engine.  If `written' is NULL, you are only asking for access
           to a line buffer, into which to write a new line of image
           data for the component in question.  Once you have written
           to the supplied line buffer, you pass it back as the `written'
           argument in a subsequent call to this function.  Regardless
           of whether `written' is NULL, the function returns a pointer
           to the single internal line buffer which it maintains for each
           original image component, if and only if that line buffer is
           currently available for writing.  It will not be available if
           the internal machinery is waiting for a line of another component
           before it can process the data which has already been supplied.
           Thus, if a newly written line can be processed immediately, the
           function will return a non-NULL pointer even in the call with
           `written' non-NULL.  If it must wait for other component lines
           to arrive, however, it will return NULL.  Once returning non-NULL,
           the function will continue to return the same line buffer at
           least until the next call which supplies a non-NULL `written'
           argument.  This is because the current line number is incremented
           only by calls which supply a non-NULL `written' argument.
           [//]
           Note that all lines processed by this function should have a
           signed representation, regardless of whether or not
           `kdu_codestream::get_signed' reports that the components
           are signed.
         [RETURNS]
           Non-NULL if a line is available for writing.  That same line
           should be passed back to the function as its `written' argument
           in a subsequent call to the function (not necessarily the next
           one) in order to advance to a new line.  If the function returns
           NULL, you may have reached the end of the tile (you
           should know this), or else the object may be waiting for you
           to supply new lines for other image components which must
           be processed together with this one.
         [ARG: comp_idx]
           Index of the component for which a line is being written or
           requested.  This index must lie in the range 0 to Cs-1, where
           Cs is the `num_source_components' value supplied to
           `create'.
         [ARG: written]
           If non-NULL, this argument must be identical to the line buffer
           which was previously returned by the function, using the same
           `comp_idx' value.  In this case, the line is deemed to contain
           valid image data and the internal line counter for this component
           will be incremented before the function returns.  Otherwise,
           you are just asking the function to give you access to the
           internal line buffer so that you can write to it.
         [ARG: env]
           Must be non-NULL if and only if a non-NULL `env' argument was
           passed into `create'.  Any non-NULL `env' argument must identify
           the calling thread, which need not necessarily be the one used
           to create the object in the first place.
      */
    bool is_line_precise(int comp_idx)
      { return state->is_line_precise(comp_idx); }
      /* [SYNOPSIS]
           Returns true if the indicated line has been assigned a
           precise (32-bit) representation by the `create' function.
           Otherwise, calls to `exchange_line' will return lines which
           have a 16-bit representation.  This function is provided as
           a courtesy so that applications which need to allocate
           auxiliary lines with compatible precisions will be able to
           do so.
      */
    bool is_line_absolute(int comp_idx)
      { return state->is_line_absolute(comp_idx); }
      /* [SYNOPSIS]
           Returns true if the indicated line has been assigned an
           absolute integer representation by the `create' function.
           Otherwise, calls to `exchange_line' will return lines whose
           `kdu_line_buf::is_absolute' function returns false.  This function
           is provided as a courtesy, so that applications can know ahead
           of time what the type of the data associated with a line will be.
           In the presence of multi-component and/or non-linear point
           transforms, this can be non-trivial to figure out based solely
           on the output component index.
           [//]
           It is worth noting that any component that has an associated
           non-linear point transform with one of the types `NLType_SMAG'
           or `NLType_UMAG' will appear to the application with an absolute
           integer representation, even if all compression processing is
           irreversible.  This is done intentionally to facilitate the use of
           the SMAG and UMAG transforms in compressing float or half-float
           original image samples that have been cast (re-interpreted) as
           integers.  In such cases, all that need be done with the line
           buffers returned by `exchange_line' is to fill their sample values
           with original float or half-float values that have been
           re-interpreted as integers, possibly with some bit-shifting to
           accommodate any differences in exponent and mantissa precision
           from those of the standard IEEE float or half-float models.
           Components identified as unsigned must also be level shifted in
           the usual way after being re-interpreted as integers.
      */
    kdu_byte get_line_flags(int comp_idx)
      { return state->get_line_flags(comp_idx); }
      /* [SYNOPSIS]
           Returns the set of flags that are passed to the second form of
           the `kdu_line_buf::pre_create' function when creating the line
           buffers returned by `exchange_line'.  The return value is the
           logical OR of zero or more of `KDU_LINE_BUF_SHORTS' or
           `KDU_LINE_BUF_ABSOLUTE'.  The `KDU_LINE_BUF_SHORTS' flag will be
           present if and only if `is_line_precise' returns false, while the
           `KDU_LINE_BUF_ABSOLUTE' flag will be present if and only if
           `is_line_absolute' returns true.
      */
  private: // Data
    kd_multi_analysis_base *state;
  };

/*****************************************************************************/
/*                         kd_multi_synthesis_base                           */
/*****************************************************************************/

class kd_multi_synthesis_base { 
  public: // Member functions
    virtual ~kd_multi_synthesis_base() { return; }
    virtual void terminate_queues(kdu_thread_env *env) = 0;
      /* Called prior to the destructor if `env' is non-NULL, this function
         terminates the tile-component queues created for multi-threading. */
    virtual bool start(kdu_thread_env *env) = 0;
    virtual kdu_coords get_size(int comp_idx) = 0;
    virtual kdu_line_buf *get_line(int comp_idx, kdu_thread_env *env) = 0;
    virtual bool is_line_precise(int comp_idx) = 0;
    virtual bool is_line_absolute(int comp_idx) = 0;
    virtual kdu_byte get_line_flags(int comp_idx) = 0;
  };

/*****************************************************************************/
/*                           kdu_multi_synthesis                             */
/*****************************************************************************/

class kdu_multi_synthesis { 
  /* [BIND: interface]
     [SYNOPSIS]
       This powerful object generalizes the functionality of `kdu_synthesis'
       to the processing of multiple image components, allowing all the
       data for a tile (or any subset thereof) to be reconstructed by a
       single object.  The object creates the `kdu_synthesis' objects
       required to process each required codestream image component, but it
       also inverts any Part-1 colour decorrelation transforms (RCT and ICT)
       and Part-2 generalized multi-component transforms, as required, and
       passes the result through any Part-2 non-linear point transforms (NLT)
       that might be applicable.
       [//]
       Objects of this class serve as interfaces.  The constructor
       simply creates an empty interface, and there is no meaningful
       destructor.  This means that you may copy and transfer objects
       of this class at will, without any impact on internal resources.
       To create a meaningful insance of the internal machine, you must
       use the `create' member.  To destroy the internal machine you
       must use the `destroy' member.
       [//]
       To use this object, one typically invokes `create', followed by
       calls to `get_line' and then ultimately `destroy'.  In multi-threaded
       processing environments, one typically passes a `kdu_thread_queue'
       super-queue to the `create' function and invokes
       `kdu_thread_entity::terminate' on this super-queue prior to the
       call to `destroy'.  In multi-threaded multi-tiled environments,
       where multiple tiles are to be processed concurrently (as opposed
       to sequentially), it is generally preferable to include the
       `KDU_MULTI_XFORM_DELAYED_START' flag in the call to `create' and
       use the `start' function in the recommended manner prior to the
       first call to `get_line', although this is not required and might
       not make much difference in many cases, due to the ability of the
       internal machinery to compensate.
       [//]
       From Kakadu version 7.5, you can pass a reference to an external
       `kdu_sample_allocator' object to be used in place of the internal
       one.  This is useful in NUMA multi-processor systems where the
       haphazard creation and destruction of `kdu_multi_synthesis' engines
       might otherwise result in the sample data memory being allocated
       disadvantageously.  By retaining an allocated memory buffer inside
       an external `kdu_sample_allocator' you can arrange for that buffer to
       reside in memory that is both stable across multiple create/destroy
       events and close to a processor that may be associated with the
       threads in a `kdu_thread_env' multi-threaded environment that you
       are using.  In the future, `kdu_sample_allocator' may be extended to
       include additional customization tools that could be brought to bear
       upon the `kdu_multi_synthesis' engine by use of this new external
       allocator feature.
       [//]
       The `external_sample_allocator' and `membroker' arguments provide
       rich options for taking control over memory allocation, as explained
       below.  Memory brokering services can be supplied either through an
       explicit `membroker' or by providing the broker to an
       `external_sample_allocator' via a prior call to
       `kdu_sample_allocator::configure'.  Ultimately, however, the
       use of an `external_sample_allocator' provides you with the most
       options.  In particular, this is the only way to explicitly specify
       the fragment size used during the allocation of sample data
       blocks within `kdu_sample_allocator'.
  */
  public: // Member functions
    kdu_multi_synthesis() { state = NULL; }
      /* [SYNOPSIS]
           Leaves the interface empty, meaning that the `exists' member
           returns false.
      */
    bool exists() { return (state != NULL); }
      /* [SYNOPSIS]
           Returns true after `create' has been called; you may also
           copy an interface whose `create' function has been called. */
    bool operator!() { return (state == NULL); }
      /* [SYNOPSIS]
           Opposite of `exists', returning false if the object represents
           an interface to instantiated internal processing machinery.
      */

    KDU_EXPORT kdu_long
      create(kdu_codestream codestream, kdu_tile tile,
             kdu_thread_env *env, kdu_thread_queue *env_queue,
             int flags, int buffer_rows=1,
             kdu_sample_allocator *external_sample_allocator=NULL,
             const kdu_push_pull_params *extra_params=NULL,
             kdu_membroker *membroker=NULL);
      /* [SYNOPSIS]
           Use this function to create an instance of the internal
           processing machinery, for decompressing data associated with
           the supplied open `tile' interface.  Until you call this
           function (or copy another object which has been created), the
           `exists' function will return false.
           [//]
           Note carefully that the output components which will be
           decompressed are affected by any previous calls to
           `kdu_codestream::apply_input_restrictions'.  You may use either
           of the two forms of that function to modify the set of output
           components which appear to be present.  If called with an
           `access_mode' argument of `KDU_WANT_CODESTREAM_COMPONENTS', the
           present object will present codestream image components as though
           they were the final output image components.  If, however,
           `kdu_codestream::apply_input_restrictions' was called with a
           component `access_mode' argument of `KDU_WANT_OUTPUT_COMPONENTS',
           or if it has never been called, the present object will present
           output components in their fullest form, after processing by any
           required inverse multi-component decomposition, if necessary.  In
           either case, the set of components which is presented is
           identical to that which appears via the various `kdu_codestream'
           interface functions, such as `kdu_codestream::get_num_components',
           `kdu_codestream::get_bit_depth', and so forth, in each case
           with the optional `want_output_comps' argument set to true.
           [//]
           The behaviour of this function is affected by calls to
           `kdu_tile::set_components_of_interest'.  In particular, any of the
           apparent output components which have been identified as
           uninteresting, will not be generated by the multi-component
           transformation network -- they will, instead, appear to contain
           constant sample values.   Of course, you will probably not want
           to access these constant components, or else you would not have
           marked them as uninteresting; however, you can access them if you
           wish without incurring any processing overhead.
           [//]
           The `env' and `env_queue' arguments may be used to implement a
           a variety of multi-threaded processing paradigms, to leverage
           the capabilities of multi-processor platforms.  To see how this
           works, consult the description of these arguments below.  Also,
           play close attention to the use of `env' arguments with the
           `get_line' and `destroy' functions.
           [//]
           The `flags' argument works together with subsequent arguments
           to control internal memory allocation, flow control and
           optimization policies.  See below for a description of the
           available flags.
           [//]
           If you are working with tiled images and you are intending to
           create multiple `kdu_multi_synthesis' engines from which image
           component lines will be retrieved in round-robbin fashion (typical
           for scan-line oriented processing of tiled imagery), it may be
           worth paying attention to the `KDU_MULTI_XFORM_DELAYED_START'
           flag and the `start' function, both of which are new to Kakadu
           v7, where they play an important role in maximizing the efficiency
           for multi-threaded processing environments involving a large number
           of threads.
         [RETURNS]
           Returns the number of bytes which have been allocated internally
           for the processing of multi-component transformations,
           spatial wavelet transforms and intermediate buffering between
           the wavelet and block decoder engines.  Essentially, this
           includes all memory resources for the tile.
         [ARG: env]
           Supply a non-NULL argument here if you want to enable
           multi-threaded processing.  After creating a cooperating
           thread group by following the procedure outlined in the
           description of `kdu_thread_env', any one of the threads may
           be used to construct this processing engine.  Separate
           processing queues will automatically be created for each
           image component.
           [//]
           If the `KDU_MULTI_XFORM_MT_DWT' flag (or the `KDU_MULTI_XFORM_DBUF'
           flag) is supplied, these queues will also be used to schedule the
           spatial wavelet transform operations associated with each image
           component as jobs to be processed asynchronously by different
           threads.  Regardless multi-threaded DWT processing is requested,
           within each tile-component, separate queues are created to allow
           simultaneous processing of code-blocks from different subbands.
           [//]
           By and large, you can use this object in exactly the same way
           when `env' is non-NULL as you would with a NULL `env' argument.
           That is, the use of multi-threaded processing can be largely
           transparent.  However, you must remember the following three
           points:
             [>>] You must supply a non-NULL `env' argument to the
                  `get_line' function -- it need not belong to the same
                  thread as the one used to create the object here, but it
                  must belong to the same thread group.
             [>>] Whereas single-threaded processing commences only with the
                  first call to `get_line', multi-threaded processing
                  may have already commenced before this function returns.
                  That is, other threads may be working in the background
                  to decode code-blocks, perform DWT synthesis and so forth,
                  and this may start happening even before the present
                  function returns.  Of course, this is exactly what you
                  want, to fully exploit the availability of multiple
                  processing resources.
             [>>] If you are creating multiple `kdu_multi_synthesis' objects
                  that you intend to use concurrently (retrieving lines from
                  their `get_line' functions in round-robbin fashion), you
                  are recommended to follow the startup protocol
                  associated with the `start' function and the
                  `KDU_MULTI_XFORM_DELAYED_START' creation flag.
         [ARG: env_queue]
           This argument is ignored unless `env' is non-NULL.  When `env'
           is non-NULL, all thread queues that are created inside this object
           are added as sub-queues of `env_queue'.  If `env_queue' is NULL,
           they are added as top-level queues in the multi-threaded queue
           hierarchy.  The present object creates one internal queue for each
           tile-component, to which each subband adds a sub-queue managed by
           `kdu_decoder'.
           [//]
           One advantage of supplying a non-NULL `env_queue' is that it
           provides you with a single hook for joining with the completion
           of all the thread queues which are created by this object
           and its descendants -- see `kdu_thread_entity::join' for more on
           this.
           [//]
           A second advantage of supplying a non-NULL `env_queue' is that
           it allows you to manipulate the sequencing indices that are
           assigned by the thread queues created internally -- see
           `kdu_thread_entity::attach_queue' for more on the role played by
           sequencing indices in controlling the order in which work is
           actually done.  In particular, if `env_queue' is initially
           added to the thread group with a sequencing index of N >= 0,
           all processing within all tile-components of the tile associated
           with this object will proceed with jobs having a sequence index
           of N.
           [//]
           Finally, the `env_queue' object supplied here provides a
           mechanism to determine whether or not calls to `get_line'
           might potentially block.  This may be achieved by supplying
           an `env_queue' object whose `kdu_thread_queue::update_dependencies'
           function has been overridden, or by registering a dependency
           monitor (see `kdu_thread_queue::set_dependency_monitor') with
           the supplied `env_queue'.  If the number of potentially blocking
           dependencies identified by either of these mechanisms is 0,
           calls to `get_line' can be invoked at least once for each
           component index, without blocking the caller.  Otherwise, the
           caller might be temporarily blocked while waiting for
           dependencies to be satisfied by DWT synthesis and/or subband
           decoding operations that are still in progress.  This temporary
           blocking is not a huge concern, since threads actually enter
           what we call a "working wait", using
           `kdu_thread_entity::wait_for_condition', during which they will
           often perform other tasks.  However, working waits can adversely
           affect cache utilization and often cause work to be done in a
           less than ideal sequence, so that other threads might go idle
           while waiting for jobs to be scheduled by a thread that is
           unduly delayed in a working wait.  For this reason, advanced
           implementations are offered the option of using the dependency
           analysis methods associated with an `env_queue' to schedule jobs
           only when it is known that they are fully ready to proceed.
         [ARG: flags]
           Controls the internal memory allocation, buffer management
           and various processing and optimization options.  This argument
           may be the logical OR of any appropriate combination of the flags
           listed below.  For convenience, the `KDU_MULTI_XFORM_DEFAULT_FLAGS'
           value may be used as a starting point, which will supply the
           flags that should always be present unless you have good reason
           not to include them.
           [>>] `KDU_MULTI_XFORM_DELAYED_START': This option is important
                in heavily multi-threading environments, if you are creating
                multiple `kdu_multi_synthesis' objects and you wish to use
                them concurrently -- i.e., you wish to retrieve lines from
                them in round-robbin fasion, as opposed to pulling all the
                data from one `kdu_multi_synthesis' engine via its `get_line'
                function and then moving onto the next one.  By default (i.e.,
                without this flag), the `create' function causes as many
                processing jobs as possible to be scheduled as soon as
                possible to service this `kdu_multi_synthesis' object.
                However, if you want to work concurrently with multiple
                `kdu_multi_synthesis' engines (typically to retrieve data from
                a collection of adjacent tiles), it is better if the
                processing jobs that service all these engines are scheduled
                in an interleaved fashion.  To achieve this, you may supply
                this flag during the `create' call and then you should invoke
                the `start' function before attempting to retrieve image
                component lines with `get_line'.  The `start' function should
                be invoked repeatedly on all concurrent `kdu_multi_synthesis'
                objects, in round-robbin fashion, until all calls to `start'
                return true.
           [>>] `KDU_MULTI_XFORM_PRECISE':  This option requests
                the internal machinery to work with 32-bit representations
                for all image component samples.  Otherwise, the internal
                machinery will determine a suitable representation precision,
                making every attempt to use lower precision processing
                paths, which are faster and consume less memory, so long
                as this does not unduly compromise quality.
           [>>] `KDU_MULTI_XFORM_FAST':  This option represents
                the opposite extreme in precision selection to that
                associated with `KDU_MULTI_XFORM_PRECISE'.  In fact,
                if both flags are supplied, the present one will be ignored.
                Otherwise, if the present flag is supplied, the function
                selects the lowest possible internal processing precision,
                even if this does sacrifice some image quality.  This mode
                is normally acceptable for rendering to low precision
                displays (e.g., 8 to 12 bits/sample).
           [>>] `KDU_MULTI_XFORM_SKIPYCC': Omit this flag unless you
                are quite sure that you want to retrieve raw codestream
                components without inverting any Part-1 decorrelating
                transform (inverse RCT or inverse ICT) which might otherwise
                be involved.  For this to make sense, you should be sure
                that the `kdu_codestream::apply_input_restrictions' function
                has been used to set the component access mode to
                `KDU_WANT_CODESTREAM_COMPONENTS' rather than
                `KDU_WANT_OUTPUT_COMPONENTS'.  This means that any
                non-linear point transforms (NLT) have already been
                disabled -- their effect is only seen if you are synthesizing
                output components, as opposed to codestream components.
           [>>] `KDU_MULTI_XFORM_DBUF': Same as `KDU_MULTI_XFORM_MT_DWT' --
                see below.
           [>>] `KDU_MULTI_XFORM_MT_DWT': This flag is ignored
                unless `env' is non-NULL.  It specifies that the
                spatial DWT operations associated with each codestream
                image component should be executed as asynchronous jobs, as
                opposed to synchronously when new imagery is pushed in
                via the `exchange_line' function.  When the flag is present,
                the object allocates an internal buffer which can hold
                (approximately) 2*`buffer_rows' lines for each
                codestream tile-component.  In the simplest case (double
                buffering), this buffer is partitioned into two parts.  More
                generally, though, the implementation may partition the
                memory into a larger number of parts to improve
                performance.  At any given time, the MCT machinery may be
                reading data from one part while the DWT and block decoding
                machinery may be working to write data to another part.
                Multi-threaded DWT processing provides a useful
                mechanism for minimizing CPU idle time, since multiple threads
                can be scheduled not only to block decoding operations, but
                also to DWT synthesis operations.  If DWT synthesis is
                the bottleneck, it fundamentally limits the rate at which
                block decoding jobs can be made available to the
                multi-threaded machinery; the only way to overcome this
                bottleneck, if it exists, is to allocate multiple threads to
                DWT synthesis via this flag.
         [ARG: buffer_rows]
           This argument must be strictly positive.  It is used to size the
           internal buffers used to hold data that is obtained via DWT
           synthesis and/or block decoding within individual codestream
           tile-components, while it is waiting to be processed by the MCT
           machinery.
           [//]
           From KDU-7.2.1 on, a value of 0 is automatically translated to 1,
           while a -ve value may be supplied for this argument, in which case
           the buffers will be dimensioned automatically, taking the tile
           width and number of components into account.
           [//]
           There are two important cases to understand:
           [>>] If the DWT processing is performed in-line, in the same thread
                as that which calls `pull_line', then there is only one
                buffer, with this number of rows, and the buffer must be
                empty before DWT processing is performed.  In this case, it
                is usually best to select the default value of `buffer_rows'=1
                so that DWT processing occurs as close as possible to the
                point at which the synthesized sample values are consumed.
                This minimizes the chance that the DWT synthesis results
                will be evicted from lower levels of the CPU cache before
                they are consumed via the `pull_line' function.  However,
                in the special case where multi-threaded processing is
                employed, with both `env' AND `env_queue' arguments to this
                function being non-NULL, and where the `env_queue' or one
                of the super-queues in its ancestry advertises its interest
                in propagated dependency information (through an overridden
                `kdu_thread_queue::update_dependencies' function that
                returns true, for example, or an installed dependency
                monitor), then there are some multi-threaded
                synchronization overheads associated with the onset and
                completion of the internal DWT synthesis machinery (perhaps
                amounting to several hundred or even a thousand clock cycles),
                so in this case it may be better to select a larger value for
                `buffer_rows', especially where the line width is small.
           [>>] If the `KDU_MULTI_XFORM_MT_DWT' or `KDU_MULTI_XFORM_DBUF' flag
                is present, the DWT operations are performed by jobs that may
                be executed on any thread within the multi-threaded environment
                associated with the `env' argument.  In this case, for reasons
                of backward compatibility, the total amount of memory
                allocated for buffering tile-component lines is given (at
                least approximately) by 2*`buffer_rows', since this is what
                would be consumed if a double buffering strategy were
                employed.  In reality, the internal implementation may
                partition this total amount of memory into a larger number
                of smaller buffers so that processing can start earlier and
                the memory can be used more effectively to manage interruption
                of threads, or scheduling of thread resources temporarily to
                other jobs.
           [//]
           You may need to play with this parameter to optimize
           processing efficiency for particular applications.
           [//]
           You should be aware that the actual number of buffer lines
           allocated internally may be smaller than that requested.  For
           example, the implementation may limit the total amount of
           buffer memory to 256 lines.
         [ARG: external_sample_allocator]
           If desired, you may pass your own `kdu_sample_allocator' object in
           via this argument.  The most immediate benefit of doing this is
           that the allocator will not be deleted when the current object
           is `destroy'ed so you can pass the same allocator back into a newly
           created instances of the same object and hope to wind up using
           exactly the same memory space for sample data processing and
           subband buffers for block decoding.  Since the buffers allocated by
           `kdu_sample_allocator' tend to be large, this may be very
           important, especially in NUMA (Non-Uniform Machine Architecture)
           platforms where most operating systems assign physical pages in
           memory based on the location of the CPUs that first touch them.
           [//]
           The `external_sample_allocator' may not safely be shared with any
           other object, since the internal machinery here invokes
           `kdu_sample_allocator::restart' and
           `kdu_sample_allocator::finalize'.
           [//]
           The `destroy' function will not delete any memory retained within
           an `external_sample_allocator' supplied here.
         [ARG: extra_params]
           This argument can be used to pass in additional parameters that
           can provide custom control over the decompression processes.
         [ARG: membroker]
           This optional argument allows you to supply an explicit memory
           broker, which will govern all memory allocation performed within
           this object in the event that `external_sample_allocator' is NULL.
           [//]
           If this argument is NULL, but `external_sample_allocator' is
           non-NULL, all memory allocation performed within this object is
           governed by any memory broker that was aupplied to
           `external_sample_allocator->configure'.
           [//]
           If both arguments are non-NULL, the `membroker' supplied here is
           used only to govern internal memory allocations associated with
           multi-component transforms and the `kdu_multi_synthesis' framework
           itself.  The memory associated with spatial transformation and
           decoding processes, however, will be governed by the memory broker
           that was supplied to `external_sample_allocator->configure'; if
           there is none, this memory will not be brokered.
      */
    kdu_long
      create(kdu_codestream codestream, kdu_tile tile,
             bool force_precise=false, bool skip_ycc=false,
             bool want_fastest=false, int buffer_rows=1,
             kdu_thread_env *env=NULL, kdu_thread_queue *env_queue=NULL,
             bool multi_threaded_dwt=false)
        { 
          int flags = KDU_MULTI_XFORM_DEFAULT_FLAGS;
          if (force_precise) flags |= KDU_MULTI_XFORM_PRECISE;
          if (skip_ycc) flags |= KDU_MULTI_XFORM_SKIPYCC;
          if (want_fastest) flags |= KDU_MULTI_XFORM_FAST;
          if (multi_threaded_dwt) flags |= KDU_MULTI_XFORM_MT_DWT;
          return create(codestream,tile,env,env_queue,flags,buffer_rows);
        }
        /* [SYNOPSIS]
             This form of the `create' function is provided for compatibility
             with applications created with other versions of Kakadu.  It
             simply invokes the first form of the function with an appropriate
             set of flags and parameter values.
             [//]
             However, this function is NOT IDEAL if you wish to
             process MULTIPLE TILES CONCURRENTLY -- i.e., pulling image
             component lines from multiple `kdu_multi_synthesis' objects'
             `get_line' functions in round-robbin fashion.  In that case,
             you are recommended to invoke the first form of the `create'
             function with the `KDU_MULTI_XFORM_DELAYED_START' flag and you
             should be following the efficient startup protocol described with
             the `start' function.
             [//]
             This simplified form of the function lacks much of the
             flexibility and control offered by the first version of
             `create'.  As one important example, this version does not
             offer any mechanism to take control over memory allocation,
             since it offers neither the external `kdu_sample_allocator'
             option nor an explicit `kdu_membroker' reference, which are
             valuable features.  Taking these things into consideration,
             developers are strongly recommended to use the first version
             of `create' in all new application development.
             [//]
             Note that the `buffer_rows' argument may be known as
             `processing_stripe_height' in other versions of Kakadu, but
             it has the same interpretation -- see the first form of
             `create' for an explanation.
             [//]
             Similarly, the `multi_threaded_dwt' argument may be known as
             `double_buffering' in other versions of Kakadu, but the memory
             consumption and multi-threading implications are the same.
        */
    bool start(kdu_thread_env *env=NULL)
      { return (state == NULL)?true:state->start(env); }
      /* [SYNOPSIS]
           It is always safe to call this function, but unless the
           `KDU_MULTI_XFORM_DELAYED_START' flag was passed to `create',
           the function will simply return true, doing nothing.
           [//]
           Delayed starts are useful only when using a multi-threaded
           processing environment (of which `env' must be a member) and
           then only when you wish to work with multiple `kdu_multi_synthesis'
           engines concurrently, retrieving image lines via their `get_line'
           interfaces in round-robbin fashion.  In this case, multi-threaded
           processing can be more efficient if you follow the startup
           protocol described below:
           [>>] Let E_1, E_2, ..., E_N denote N `kdu_multi_synthesis'
                processing engines that you wish to use concurrently.  Almost
                invariably, these correspond to N horizontally adjacent tiles
                from the source image that you wish to decompress
                "concurrently", so that the complete image (or a region of
                interest) can be recovered progressively, in raster scan
                fashion.
           [>>] First, you should invoke `E_1->create', `E_2->create', ...,
                `E_N->create', passing the `KDU_MULTI_XFORM_DELAYED_START'
                flag to each `create' function.  This actually does initiate
                the scheduling of compressed data parsing and block decoding
                jobs to some extent, but it does not generally cause all
                possible jobs to be scheduled and it does not activate any
                internal asynchronous DWT synthesis processing jobs.
           [>>] Next, you should invoke `E_1->start', `E_2->start', ...,
                `E_N->start' in sequence, paying attention to the return
                values from these functions.  If any of the functions returned
                false, you should repeat the process, until all of the calls
                to `start' return false.
           [//]
           If you fail to complete the above protocol before calling `get_line'
           for the first time, the protocol will be completed for you, but
           processing jobs will not generally be ideally interleaved between
           the `kdu_multi_synthesis' processing engines that you wish to use
           concurrently.  As a result, working threads may not be utilized
           as fully as possible.
           [//]
           If you fail to complete the above protocol and you have your
           own processing queue (passed to `create') that is waiting for
           an overridden `kdu_thread_queue::update_dependencies' function to
           signal the availability of data before scheduling its own
           processing jobs to retrieve image component lines via `get_line',
           that event will never occur and your application will become
           deadlocked -- of course, this is an exotic way to use the
           `kdu_multi_synthesis' machinery, but it is one that can be
           particularly efficient in heavily multi-threaded applications.
           [//]
           In any event, the message is that you should follow the protocol
           described above or else you should not pass the
           `KDU_MULTI_XFORM_DELAYED_START' flag to `create'.
      */
    KDU_EXPORT void destroy(kdu_thread_env *env=NULL);
      /* [SYNOPSIS]
           Use this function to destroy the internal processing machine
           created using `create'.  The function may be invoked on any
           copy of the original object whose `create' function was called,
           so be careful.
         [ARG: env]
           If this argument is non-NULL, this function will automatically
           terminate all work within the object before cleaning up its
           resources.  This is done using `kdu_thread_entity::terminate'.
           [//]
           Otherwise, so long as you have explicitly used
           `kdu_thread_entity::join' or `kdu_thread_entity::terminate'
           already, it is OK to invoke this function with a NULL `env'
           argument.  There is nothing stopping you from joining multiple
           times on a queue, so you can use both approaches without any
           problems.
      */
    kdu_coords get_size(int comp_idx) { return state->get_size(comp_idx); }
      /* [SYNOPSIS]
           This is a convenience function to return the size of the image
           component identified by `comp_idx', as seen within the present
           tile.  The same information may be obtained by invoking
           `kdu_codestream::get_tile_dims' with its `want_output_comps'
           argument set to true.
      */
    kdu_line_buf *get_line(int comp_idx, kdu_thread_env *env=NULL)
      { return state->get_line(comp_idx,env); }
      /* [SYNOPSIS]
           Use this function to get the next line of image data from
           the indicated component.  The function will return NULL if
           you have already reached the end of the tile, or if the next
           component cannot be decompressed without first retrieving
           new lines from one or more other components.  This latter
           condition may arise if the components are coupled through a
           multi-component transform, in which case the components must
           be accessed in an interleaved fashion -- otherwise, the object
           would need to allocate additional internal buffering resources.
           If there is a component that you are not interested in, you
           should declare that using either
           `kdu_codestream::apply_input_restrictions' and/or
           `kdu_tile::set_components_of_interest', before creating
           the present object.
           [//]
           Note that all lines returned by this function have a signed
           representation, regardless of whether or not
           `kdu_codestream::get_signed' reports that the components are
           signed.  In most cases, this minimizes the number of memory
           accesses which are required, deferring any required offsets
           until rendering (or saving to a file).
         [RETURNS]
           Non-NULL if a new decompressed line is available for the
           indicated component.  Each call to this function which returns
           a non-NULL pointer causes an internal line counter to be
           incremented for the component in question.
         [ARG: comp_idx]
           Index of the component for which a new line is being requested.
           This index musts lie in the range 0 to Co-1, where Co is
           the value returned by `kdu_codestream::get_num_components',
           with its `want_output_comps' argument set to true.  The number
           of these components may be affected by calls to
           `kdu_codestream::apply_input_restrictions' -- such calls must
           have been made prior to the point at which this object's
           `create' function was called.
         [ARG: env]
           Must be non-NULL if and only if a non-NULL `env' argument was
           passed into `create'.  Any non-NULL `env' argument must identify
           the calling thread, which need not necessarily be the one used
           to create the object in the first place.
      */
    bool is_line_precise(int comp_idx)
      { return state->is_line_precise(comp_idx); }
      /* [SYNOPSIS]
           Returns true if the indicated line has been assigned a
           precise (32-bit) representation by the `create' function.
           Otherwise, calls to `get_line' will return lines which
           have a 16-bit representation.  This function is provided as
           a courtesy so that applications which need to allocate
           auxiliary lines with compatible precisions will be able to
           do so.
      */
    bool is_line_absolute(int comp_idx)
      { return state->is_line_absolute(comp_idx); }
      /* [SYNOPSIS]
           Returns true if the indicated line has been assigned an
           absolute integer representation by the `create' function.
           Otherwise, calls to `get_line' will return lines whose
           `kdu_line_buf::is_absolute' function returns false.  This function
           is provided as a courtesy, so that applications can know ahead
           of time what the type of the data associated with a line will be.
           In the presence of multi-component and/or non-linear point
           transforms, this can be non-trivial to figure out based solely
           on the output component index.
           [//]
           It is worth noting that any component that has an associated
           non-linear point transform with one of the types `NLType_SMAG'
           or `NLType_UMAG' will appear to the application with an absolute
           integer representation, even if all decompression processing is
           irreversible.  This is done intentionally to facilitate the use of
           the SMAG and UMAG transforms in handling float or half-float
           original image samples that have been cast (re-interpreted) as
           integers.  In such cases, all that need be done with the returned
           data is to cast or re-interpret the samples values as floats or
           half-floats, as appropriate, possibly with some bit-shifting to
           accommodate any differences in exponent and mantissa precision
           from those of the standard IEEE float or half-float models --
           unsigned components must also be level shifted as usual before
           being interpreted as floats.
      */
    kdu_byte get_line_flags(int comp_idx)
      { return state->get_line_flags(comp_idx); }
      /* [SYNOPSIS]
           Returns the set of flags that are passed to the second form of
           the `kdu_line_buf::pre_create' function when creating the line
           buffers returned by `get_line'.  The return value is the
           logical OR of zero or more of `KDU_LINE_BUF_SHORTS' or
           `KDU_LINE_BUF_ABSOLUTE'.  The `KDU_LINE_BUF_SHORTS' flag will be
           present if and only if `is_line_precise' returns false, while the
           `KDU_LINE_BUF_ABSOLUTE' flag will be present if and only if
           `is_line_absolute' returns true.
      */
  private: // Data
    kd_multi_synthesis_base *state;
  };

/*****************************************************************************/
/*                    Base Casting Assignment Operators                      */
/*****************************************************************************/

inline kdu_push_ifc &kdu_push_ifc::operator=(kdu_analysis rhs)
  { state = rhs.state; return *this; }
inline kdu_push_ifc &kdu_push_ifc::operator=(kdu_encoder rhs)
  { state = rhs.state; return *this; }

inline kdu_pull_ifc &kdu_pull_ifc::operator=(kdu_synthesis rhs)
  { state = rhs.state; return *this; }
inline kdu_pull_ifc &kdu_pull_ifc::operator=(kdu_decoder rhs)
  { state = rhs.state; return *this; }


/* ========================================================================= */
/*                     External Function Declarations                        */
/* ========================================================================= */

KDU_EXPORT extern void
  (*kdu_convert_rgb_to_ycc_rev16)(kdu_int16*, kdu_int16*, kdu_int16*, int);
KDU_EXPORT extern void
  (*kdu_convert_rgb_to_ycc_irrev16)(kdu_int16*, kdu_int16*, kdu_int16*, int);
KDU_EXPORT extern void
  (*kdu_convert_rgb_to_ycc_rev32)(kdu_int32*, kdu_int32*, kdu_int32*, int);
KDU_EXPORT extern void
  (*kdu_convert_rgb_to_ycc_irrev32)(float*, float*, float*, int);

KDU_EXPORT extern void
  (*kdu_convert_ycc_to_rgb_rev16)(kdu_int16*, kdu_int16*, kdu_int16*, int);
KDU_EXPORT extern void
  (*kdu_convert_ycc_to_rgb_irrev16)(kdu_int16*, kdu_int16*, kdu_int16*, int);
KDU_EXPORT extern void
  (*kdu_convert_ycc_to_rgb_rev32)(kdu_int32*, kdu_int32*, kdu_int32*, int);
KDU_EXPORT extern void
  (*kdu_convert_ycc_to_rgb_irrev32)(float*, float*, float*, int);

static inline void
  kdu_convert_rgb_to_ycc(kdu_line_buf &c1, kdu_line_buf &c2, kdu_line_buf &c3)
  { 
    int n = c1.get_width();
    assert((c2.get_width() == n) && (c3.get_width() == n));
    assert((c1.is_absolute() == c2.is_absolute()) &&
           (c1.is_absolute() == c3.is_absolute()));
    if ((c1.get_buf16() != NULL) && c1.is_absolute())
      kdu_convert_rgb_to_ycc_rev16(&(c1.get_buf16()->ival),
                                   &(c2.get_buf16()->ival),
                                   &(c3.get_buf16()->ival),n);
    else if (c1.get_buf16() != NULL)
      kdu_convert_rgb_to_ycc_irrev16(&(c1.get_buf16()->ival),
                                     &(c2.get_buf16()->ival),
                                     &(c3.get_buf16()->ival),n);
    else if (c1.is_absolute())
      kdu_convert_rgb_to_ycc_rev32(&(c1.get_buf32()->ival),
                                   &(c2.get_buf32()->ival),
                                   &(c3.get_buf32()->ival),n);
    else
      kdu_convert_rgb_to_ycc_irrev32(&(c1.get_buf32()->fval),
                                     &(c2.get_buf32()->fval),
                                     &(c3.get_buf32()->fval),n);
  }
  /* [SYNOPSIS]
       The line buffers must be compatible with respect to dimensions and data
       type.  The forward ICT (RGB to YCbCr transform) is performed if the data
       is normalized (i.e. `kdu_line_buf::is_absolute' returns false).
       Otherwise, the RCT is performed.
  */
static inline void
  kdu_convert_ycc_to_rgb(kdu_line_buf &c1, kdu_line_buf &c2, kdu_line_buf &c3,
                         int width=-1)
  { 
    int n = (width < 0)?c1.get_width():width;
    assert((c1.get_width() >= n) && (c2.get_width() >= n) &&
           (c3.get_width() >= n));
    assert((c1.is_absolute() == c2.is_absolute()) &&
           (c1.is_absolute() == c3.is_absolute()));
    if ((c1.get_buf16() != NULL) && c1.is_absolute())
      kdu_convert_ycc_to_rgb_rev16(&(c1.get_buf16()->ival),
                                   &(c2.get_buf16()->ival),
                                   &(c3.get_buf16()->ival),n);
    else if (c1.get_buf16() != NULL)
      kdu_convert_ycc_to_rgb_irrev16(&(c1.get_buf16()->ival),
                                     &(c2.get_buf16()->ival),
                                     &(c3.get_buf16()->ival),n);
    else if (c1.is_absolute())
      kdu_convert_ycc_to_rgb_rev32(&(c1.get_buf32()->ival),
                                   &(c2.get_buf32()->ival),
                                   &(c3.get_buf32()->ival),n);
    else
      kdu_convert_ycc_to_rgb_irrev32(&(c1.get_buf32()->fval),
                                     &(c2.get_buf32()->fval),
                                     &(c3.get_buf32()->fval),n);
  }
  /* [SYNOPSIS]
       Inverts the effects of the forward transform performed by
       `kdu_convert_rgb_to_ycc'.  If `width' is negative, the number of
       samples in each line is determined from the line buffers themselves.
       Otherwise, only the first `width' samples in each line are actually
       processed.
  */

} // namespace kdu_core

#endif // KDU_SAMPLE_PROCESSING
