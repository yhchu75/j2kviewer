#include "stdafx.h"
#include "SVGExporter.h"
#include "DrawObjMgr.h"


void	InsertHeader(FILE *fp, int org_w, int org_h, int w, int h, const TCHAR *szFileName)
{
	_ftprintf(fp,  _T("<?xml version=\"1.0\"?> \n"));
	_ftprintf(fp, _T("<!DOCTYPE svg PUBLIC \" -//W3C//DTD SVG 1.0//EN\" \n"));
	_ftprintf(fp, _T("\"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd \"> \n"));
	_ftprintf(fp, _T(" <svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\"> \n"), w, h);
	_ftprintf(fp, _T("<desc>Export Draw Object [%s] Original Size [%d:%d]</desc>   \n"), szFileName, org_w, org_h);
	_ftprintf(fp, _T("<g>\n"));
}


void	InsertTail(FILE *fp)
{
	_ftprintf(fp, _T("</g>\n"));
	_ftprintf(fp, _T("</svg>\n"));
}


void	InsertMeasuerInfo(FILE *fp, CMeasureObj *pMeasureObj, BOOL bExportMeasure, int w, int h)
{
	CString strLine;
	CString strTemp;

	//need crop region ????.... 
	MeasureInfo *pMeasureInfo = pMeasureObj->getInfo();
	strTemp.Format( _T("M %d %d "), (int)(pMeasureInfo->pts[0].x * w), (int)(pMeasureInfo->pts[0].y * h));
	strLine = strTemp;

	for (int i = 1; i < pMeasureInfo->pts.size(); i++){
		strTemp.Format(_T("L %d %d "), (int)(pMeasureInfo->pts[i].x * w), (int)(pMeasureInfo->pts[i].y * h));
		strLine += strTemp;
	}

	//<path d = "M 100 100 L 300 50 L 300 250 L 100 300 " fill = "red" stroke = "blue" stroke - width = "3" / >
	if (pMeasureInfo->type == MEASURE_LINE || pMeasureInfo->type == MEASURE_CURVE){
		_ftprintf(fp, _T("<path d = \" %s \"  stroke-width=\"2\" fill=\"none\" stroke=\"%s\" />\n"), 
			strLine, g_PENColorString[pMeasureInfo->color]);
	}
	else if (pMeasureInfo->type == MEASURE_AREA){
		strLine += _T(" Z");
		_ftprintf(fp, _T("<path d = \" %s \"  stroke-width=\"2\" fill=\"none\" stroke=\"%s\" />\n"), 
			strLine, g_PENColorString[pMeasureInfo->color]);
	}

	if (bExportMeasure){
		//font - size. 
		int font_size = 20;
		if (w < 1000 || h < 1000)
			font_size = 10;
		if (w < 5000 || h < 5000)
			font_size = 20;
		else if (w < 10000 || h < 10000)
			font_size = 30;
		else if (w < 20000 || h < 20000)
			font_size = 40;
		else if (w < 40000 || h < 40000)
			font_size = 50;
		else
			font_size = 60;

		_ftprintf(fp, _T("<text x = \"%d\" y =  \"%d\"  fill = \"%s\"  font-size = \"%d\">  %s </text>\n"),
			(int)(pMeasureInfo->pts[0].x * w), (int)(pMeasureInfo->pts[0].y * h), g_PENColorString[pMeasureInfo->color], 
			font_size, pMeasureObj->getLengthXMLOutputStringWithUnit());
	}
}


static float fRATIO[6] = { 1.0f, 0.5f, 0.25f, 0.1f, 0.05f, 0.01f };
bool SVGExport(const TCHAR *szFile, const TCHAR *szOrgFile, CDrawObjMgr *pDrawObjMgr, BOOL bExportMeasure, int ratio, int org_w, int org_h)
{
	FILE * fp = _tfopen(szFile, _T("w+t"));
	if (NULL == fp){
		return false;
	}

	std::vector<CMeasureObj*> MeasureObjList = pDrawObjMgr->GetMeasureObjList();

	//resize fit image
	int w = org_w * fRATIO[ratio];
	int h = org_h * fRATIO[ratio];
	
	InsertHeader(fp, org_w, org_h, w, h, szFile);
	for (CMeasureObj* pMeasureInfo : MeasureObjList){
		InsertMeasuerInfo(fp, pMeasureInfo, bExportMeasure, w, h);
	}

	InsertTail(fp);
	fclose(fp);

	return true;
}
