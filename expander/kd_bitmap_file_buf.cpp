#include "stdafx.h"
#include "kd_bitmap_file_buf.h"

kd_bitmap_file_buf* kd_bitmap_file_buf::_instance = nullptr;

void kd_bitmap_file_buf::create_bitmap(kdu_coords size)
{
	if (buf != NULL)
	{
		assert(this->size == size);
		return;
	}

	/* If file created, continue to map file. */
	file_hande = CreateFile(m_file_path, GENERIC_WRITE | GENERIC_READ, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, NULL);
	if (file_hande == INVALID_HANDLE_VALUE){
		goto EXIT;
	}

	//Open Large File
	ULARGE_INTEGER map_size;
	int image_header_pos;
	//check tiff and bmp 
	if (_tcsstr(m_file_path, _T(".tiff"))){	//add tiff header
				
		kdu_tiffdir tiffdir;
		bool use_bigtiff = (kdu_long)(size.y * size.x * (kdu_long)4) > ((kdu_long)1800000000);
		tiffdir.init(tiffdir.is_native_littlendian(), use_bigtiff);
		int rows = size.y;
		int cols = size.x;
		int num_components = 4; //(RGB+ALPHA)
		int scanline_width = cols*num_components;

		kdu_uint16 photometric_type = KDU_TIFF_PhotometricInterp_RGB;
		tiffdir.write_tag(KDU_TIFFTAG_ImageWidth32, (kdu_uint32)size.x);
		tiffdir.write_tag(KDU_TIFFTAG_ImageHeight32, (kdu_uint32)size.y);
		tiffdir.write_tag(KDU_TIFFTAG_SamplesPerPixel, (kdu_uint16)num_components);
		tiffdir.write_tag(KDU_TIFFTAG_PhotometricInterp, photometric_type);
		tiffdir.write_tag(KDU_TIFFTAG_PlanarConfig, KDU_TIFF_PlanarConfig_CONTIG);
		tiffdir.write_tag(KDU_TIFFTAG_Compression, KDU_TIFF_Compression_NONE);
		tiffdir.write_tag(KDU_TIFFTAG_PlanarConfig, KDU_TIFF_PlanarConfig_CONTIG);
		tiffdir.write_tag(KDU_TIFFTAG_Compression, KDU_TIFF_Compression_NONE);

		tiffdir.write_tag(KDU_TIFFTAG_ExtraSamples, (kdu_uint16)1);
		for (int n = 0; n < 3; n++)
		{
			tiffdir.write_tag(KDU_TIFFTAG_BitsPerSample, (kdu_uint16)8);
			tiffdir.write_tag(KDU_TIFFTAG_SampleFormat, KDU_TIFF_SampleFormat_UNSIGNED);
		}
		
		int rows_per_strip = (1 << 24) / scanline_width; // 16MB strips seems reasonable
		if (rows_per_strip < 1)
			rows_per_strip = 1;
		if (rows_per_strip > rows)
			rows_per_strip = rows;
		int strip_idx, num_strips = 1 + ((rows - 1) / rows_per_strip);
		kdu_long strip_bytes = ((kdu_long)rows_per_strip) * scanline_width;
		kdu_long last_strip_bytes =
			(kdu_long)(rows - (num_strips - 1)*rows_per_strip) * scanline_width;
		tiffdir.write_tag(KDU_TIFFTAG_RowsPerStrip32, (kdu_uint32)rows_per_strip);
		kdu_uint32 header_length = (use_bigtiff) ? 16 : 8;
		kdu_long image_pos;
		if (use_bigtiff)
		{ // Generate and write the strip properties for 64-bit TIFF files
			kdu_long *offsets = new kdu_long[2 * num_strips];
			kdu_long *byte_counts = offsets + num_strips;
			memset(offsets, 0, sizeof(kdu_long)*(size_t)num_strips);
			for (strip_idx = 0; strip_idx < num_strips; strip_idx++)
				byte_counts[strip_idx] = strip_bytes;
			byte_counts[num_strips - 1] = last_strip_bytes;
			tiffdir.write_tag(KDU_TIFFTAG_StripOffsets64, num_strips, offsets);
			tiffdir.write_tag(KDU_TIFFTAG_StripByteCounts64, num_strips, byte_counts);
			image_pos = tiffdir.get_dirlength() + header_length;
			tiffdir.create_tag(KDU_TIFFTAG_StripOffsets64); // Reset this tag
			for (strip_idx = 0; strip_idx < num_strips; strip_idx++)
				offsets[strip_idx] = image_pos + strip_bytes * strip_idx;
			tiffdir.write_tag(KDU_TIFFTAG_StripOffsets64, num_strips, offsets);
			delete[] offsets;
			assert(image_pos == (tiffdir.get_dirlength() + header_length));
		}
		else
		{ // Generate and write the strip properties for 32-bit TIFF files
			kdu_uint32 *offsets = new kdu_uint32[2 * num_strips];
			kdu_uint32 *byte_counts = offsets + num_strips;
			memset(offsets, 0, sizeof(kdu_uint32)*(size_t)num_strips);
			for (strip_idx = 0; strip_idx < num_strips; strip_idx++)
				byte_counts[strip_idx] = (kdu_uint32)strip_bytes;
			byte_counts[num_strips - 1] = (kdu_uint32)last_strip_bytes;
			tiffdir.write_tag(KDU_TIFFTAG_StripOffsets32, num_strips, offsets);
			tiffdir.write_tag(KDU_TIFFTAG_StripByteCounts32, num_strips, byte_counts);
			image_pos = tiffdir.get_dirlength() + header_length;
			tiffdir.create_tag(KDU_TIFFTAG_StripOffsets32); // Reset this tag
			for (strip_idx = 0; strip_idx < num_strips; strip_idx++)
				offsets[strip_idx] = (kdu_uint32)(image_pos + strip_bytes * strip_idx);
			tiffdir.write_tag(KDU_TIFFTAG_StripOffsets32, num_strips, offsets);
			delete[] offsets;
			assert(image_pos == (tiffdir.get_dirlength() + header_length));
		}

		out.open(file_hande);
		tiffdir.write_header(&out, header_length);
		tiffdir.writedir(&out, header_length);

		ULONGLONG width = cols;
		ULONGLONG height = cols;

		map_size.QuadPart = width * (height + 64) * 4 + image_pos; //file size and bitmap header
		file_mm_handle = CreateFileMapping(file_hande, NULL, PAGE_READWRITE, map_size.HighPart, map_size.LowPart, NULL);	//16GByte open
		if (file_mm_handle == NULL){
			goto EXIT;
		}

		buf_addr = (char *)MapViewOfFile(file_mm_handle, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
		if (buf_addr == NULL){
			goto EXIT;
		}

		image_header_pos = image_pos;
	}
	else{	//bitmap...
		ULONGLONG width = size.x;
		ULONGLONG height = size.y;
		int bitmap_file_header_size = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

		map_size.QuadPart = width * height * 4 + bitmap_file_header_size; //file size and bitmap header
		file_mm_handle = CreateFileMapping(file_hande, NULL, PAGE_READWRITE, map_size.HighPart, map_size.LowPart, NULL);	//16GByte open
		if (file_mm_handle == NULL){
			goto EXIT;
		}
				
		buf_addr = (char *)MapViewOfFile(file_mm_handle, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
		if (buf_addr == NULL){
			goto EXIT;
		}

		//write bitmap header 
		BITMAPFILEHEADER bmfh;
		BITMAPINFOHEADER info;
		memset(&bmfh, 0, sizeof(BITMAPFILEHEADER));
		memset(&info, 0, sizeof(BITMAPINFOHEADER));

		//	Next we fill the file header with data : 
		bmfh.bfType = 0x4d42;       // 0x4d42 = 'BM'
		bmfh.bfReserved1 = 0;
		bmfh.bfReserved2 = 0;
		bmfh.bfSize = bitmap_file_header_size + width * height * 4;	//over 4G? 
		bmfh.bfOffBits = 0x36;

		//	and the info header : 
		info.biSize = sizeof(BITMAPINFOHEADER);
		info.biWidth = width;
		info.biHeight = -height;
		info.biPlanes = 1;
		info.biBitCount = 32;
		info.biCompression = BI_RGB;
		info.biSizeImage = 0;
		info.biXPelsPerMeter = 0;
		info.biYPelsPerMeter = 0;
		info.biClrUsed = 0;
		info.biClrImportant = 0;

		DWORD bwritten = 0;
		if (WriteFile(file_hande, &bmfh, sizeof(BITMAPFILEHEADER), &bwritten, NULL) == false)
		{
			goto EXIT;
		}

		if (WriteFile(file_hande, &info, sizeof(BITMAPINFOHEADER), &bwritten, NULL) == false)
		{
			goto EXIT;
		}

		image_header_pos = bitmap_file_header_size;
	}

	FlushFileBuffers(file_hande);
	written_size.QuadPart = image_header_pos;
	decode_size.QuadPart = 0;
	base_addr = buf_addr;

	TRACE("kd_bitmap_file_buf create : [%s], buf [%p] size [%d:%d] \n", m_file_path, buf_addr, size.x, size.y);

	this->buf = (kdu_uint32 *)(buf_addr + image_header_pos);
	this->row_gap = size.x;
	this->size = size;


	return;

EXIT:
	assert(0);
	destory_filemap();
}


void	kd_bitmap_file_buf::destory_filemap()
{
	if (buf_addr) {
		UnmapViewOfFile(buf_addr); base_addr = buf_addr = NULL;
	}
	if (file_hande) {
		CloseHandle(file_hande); file_hande = NULL;
	}
	if (file_mm_handle){
		CloseHandle(file_mm_handle);
		file_mm_handle = NULL;
	}

	this->buf = 0;
	this->row_gap = 0;

	TRACE("kd_bitmap_file_buf destroy : [%s]\n", m_file_path);

}


void  kd_bitmap_file_buf::FlashBuf(int decoded_size)
{	
	if (buf_addr) {
		//decoded_size *= 4;
		written_size.QuadPart += decoded_size;
		decode_size.QuadPart += decoded_size;

		char*	old_buf_addr = buf_addr;

		//align 
		ULARGE_INTEGER map_position;
		map_position.QuadPart = (written_size.QuadPart & 0xFFFFFFFFFFFF0000);
		UnmapViewOfFile(buf_addr);
		buf_addr = (char *)MapViewOfFile(file_mm_handle, FILE_MAP_WRITE | FILE_MAP_READ, map_position.HighPart, map_position.LowPart, 0);
		DWORD err = GetLastError();
		if (err || buf_addr == NULL){
			TRACE("kd_bitmap_file_buf error %d \n", err);
		}

		if (old_buf_addr != buf_addr){
			TRACE("kd_bitmap_file_buf changed : \n");
			base_addr = buf_addr;
		}

		int pos = written_size.QuadPart & 0xFFFF;
		this->buf = (kdu_uint32 *)((base_addr - decode_size.QuadPart) + pos);
		TRACE("kd_bitmap_file_buf FlashBuf : [%s], map buf [%p] buf [%p] mapposition : %llu  Write Size : %llu  Deocde Size : %llu \n", 
			m_file_path, buf_addr, this->buf, map_position.QuadPart, written_size.QuadPart, decode_size.QuadPart);
	}

}