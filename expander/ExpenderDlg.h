#pragma once

#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"
// CExpenderDlg dialog

class CKdu_showApp;
class CExpenderDlg : public CDialog
{
	DECLARE_DYNAMIC(CExpenderDlg)

public:
	CExpenderDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExpenderDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_SAVE_IMAGE_PROGRESS };

	void	SetCropImageInfo(CKdu_showApp *app, const TCHAR* path_name, int image_width, int image_height, POINTF& pt1, POINTF& pt2, int nTotalLyaer, int nSelectLayer);
	void	SetProgress(int percent, int decode_size);
	BOOL	IsExpandSuccess(){ return m_bSuccess; }

protected:
	virtual void	DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL	OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonPath();
	afx_msg void OnCbnSelchangeComboType();
	afx_msg void OnBnClickedCheckAllImage();
	afx_msg void OnCbnSelchangeComboLayer();
	afx_msg void OnBnClickedCheckAllLayer();
	afx_msg void OnEnChangeEditFilename();
	afx_msg void OnEnChangeEditX();
	afx_msg void OnEnChangeEditY();
	afx_msg void OnEnChangeEditW();
	afx_msg void OnEnChangeEditH();

private:
	CKdu_showApp *m_app;
	int			m_crop_pos_x, m_crop_pos_y;
	int			m_crop_width, m_crop_height;
	int			m_image_width, m_image_height;
	CComboBox	m_ComboType;
	CComboBox	m_ComboLayer;
	CProgressCtrl m_ExpanderProgress;

	int			m_nTotalLayer;
	int			m_nSelectLayer;
	CString		m_org_file_name;
	CString		m_file_name;
	CString		m_path_name;
	CString		m_exp;
	BOOL		m_bSuccess;
	BOOL		m_bInit;
	POINTF		m_pt1, m_pt2;

	void	UpdateFileName(BOOL bForceInsertLayerInfo = FALSE);
	bool	m_bOverallImageExport;
	bool	m_bChangedFileNameByUser;
	bool	m_bUpdateByFileNameEdit;

	void	UpdateSelectRegion();
};
