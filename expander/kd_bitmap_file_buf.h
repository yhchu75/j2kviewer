#pragma once

#include "../VPViewer/kdu_show.h"
#include "image_local.h"


/*****************************************************************************/
/*                          kdu_simple_file_target                           */
/*****************************************************************************/

class kdu_windows_file_target : public kdu_compressed_target {
	/* [BIND: reference] */
public: // Member functions
	kdu_windows_file_target() { file = NULL; can_strip_tail = false; }
	kdu_windows_file_target(HANDLE fHandle)
	{
		file = fHandle;
	}
	/* [SYNOPSIS] Convenience constructor, which also calls `open'. */
	~kdu_windows_file_target() { }
	/* [SYNOPSIS] Automatically calls `close'. */
	bool exists() { return (file != NULL); }
	/* [SYNOPSIS]
	Returns true if there is an open file associated with the object.
	*/
	bool operator!() { return (file == NULL); }
	/* [SYNOPSIS]
	Opposite of `exists', returning false if there is an open file
	associated with the object.
	*/
	bool open(HANDLE fHandle)
	{
		file = fHandle;
		return true;
	}

	void	kdu_fseek(HANDLE fHandle, kdu_long pos)
	{
		SetFilePointer(fHandle, pos, NULL, FILE_BEGIN);
	}
	
	
	bool strip_tail(kdu_byte *buf, int num_bytes)
	{
		/* [SYNOPSIS]
		This function may be used only if the file was opened with
		`append_to_existing' equal to true, and the file was found to
		exist already, and the `write' function has not yet been used.
		In that case, the function reads the final `num_bytes' of the
		original file into `buf' and adjusts the existing file length
		to reflect the fact that these bytes have been removed.  If
		fewer than `num_bytes' were in the original file, or if any
		of the above conditions were not met, the function returns
		false immediately.
		*/
		if ((((kdu_long)num_bytes) > cur_pos) || !can_strip_tail)
			return false;
		cur_pos -= num_bytes;  kdu_fseek(file, cur_pos);
		DWORD read_byte;
		if (ReadFile(file, buf, (DWORD)num_bytes, &read_byte, NULL) != (size_t)num_bytes)
			return false;
		can_strip_tail = false;  kdu_fseek(file, cur_pos);
		return true;
	}
	virtual bool write(const kdu_byte *buf, int num_bytes)
	{ /* [SYNOPSIS] See `kdu_compressed_target::write' for an explanation. */
		int write_bytes = num_bytes;
		if ((restore_pos >= 0) && ((cur_pos + write_bytes) > restore_pos))
			write_bytes = (int)(restore_pos - cur_pos);
		if (write_bytes > 0)
		{
			DWORD written_bytes;
			WriteFile(file, buf, (size_t)write_bytes, &written_bytes, NULL );
			write_bytes = written_bytes;
			cur_pos += write_bytes;
		}
		can_strip_tail = false;
		return (write_bytes == num_bytes);
	}
	virtual bool close()
	{ /* [SYNOPSIS]
	  It is safe to call this function, even if no file has yet been
	  opened.
	  */
		file = NULL;
		can_strip_tail = false;
		return true;
	}
	virtual bool start_rewrite(kdu_long backtrack)
	{
		if ((file == NULL) || (restore_pos >= 0) ||
			(backtrack < 0) || (backtrack > cur_pos))
			return false;
		FlushFileBuffers(file);
		restore_pos = cur_pos;
		if (backtrack > 0)
		{
			cur_pos -= backtrack;  kdu_fseek(file, cur_pos);
		}
		return true;
	}
	virtual bool end_rewrite()
	{
		if (restore_pos < 0)
			return false;
		kdu_long advance = restore_pos - cur_pos;
		restore_pos = -1;
		if (advance != 0)
		{
			cur_pos += advance;  FlushFileBuffers(file);  kdu_fseek(file, cur_pos);
		}
		return true;
	}

private: // Data
	HANDLE		file;
	kdu_long	restore_pos; // -ve if not in a rewrite
	kdu_long	cur_pos; // Current position in file
	bool		can_strip_tail; // If file is open for update
};


class kd_bitmap_file_buf : public kd_bitmap_buf
{
public:
	kd_bitmap_file_buf(){
		next = NULL; 
		buf_addr = NULL;
		file_hande = NULL; file_mm_handle = NULL;
		_instance = this;
	}
	virtual ~kd_bitmap_file_buf()
	{
		destory_filemap();
		buf = NULL; // Remember to do this whenever overriding the base class
	}

	bool is_filemap_buf(){ return true; }

	void set_file_path(const TCHAR * file_name){
		_tcscpy(m_file_path, file_name);
	}

	void create_bitmap(kdu_coords size);
	/* If this function finds that a bitmap buffer is already available,
	it returns without doing anything. */

	//TODO retun HBITMAP HANDLE... 
	HBITMAP access_bitmap(kdu_uint32 * &buf, int &row_gap)
	{
		buf = this->buf; row_gap = this->row_gap; return NULL;
	}

	void  FlashBuf(int decoded_size);
	void  destory_filemap();
	static kd_bitmap_file_buf* getInstance(){ return  _instance; }

	
public: // Additional data
	friend class kd_bitmap_compositor;
	kdu_coords size;
	kd_bitmap_buf *next;

	TCHAR	m_file_path[MAX_PATH];
	HANDLE	file_hande, file_mm_handle;
	TCHAR	mm_file_name[MAX_PATH];
	
	char*	buf_addr;
	char*	base_addr;

	ULARGE_INTEGER written_size;
	ULARGE_INTEGER decode_size;
	kdu_windows_file_target out;
	static kd_bitmap_file_buf* _instance;
	
};

