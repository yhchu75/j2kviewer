#pragma once

/////////////////////////////////////////////////////////////////////////////
// CSVGFileDialog dialog

class CSVGFileDialog : public CFileDialog
{
	DECLARE_DYNAMIC(CSVGFileDialog)

public:
	BOOL	m_bMeasureCheck;
	int		m_RatioIndex;
	
	CStatic m_Static;
	CComboBox m_Combo;
	CButton	m_CheckButton;
	CSVGFileDialog(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);

protected:
	//{{AFX_MSG(CSVGFileDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
