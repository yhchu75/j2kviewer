// ExpenderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExpenderDlg.h"
#include "afxdialogex.h"
#include "kdu_show.h"
#include "kd_bitmap_file_buf.h"
#include "SVGExporter.h"

static CExpenderDlg *s_ExpenderDlg = NULL;
void	OnCallbackDecompressStatus(int precent, int decode_size)
{ 
	if (s_ExpenderDlg != NULL){
		AtlTrace("Current process [%d] \n ", precent);
		s_ExpenderDlg->SetProgress(precent, decode_size);
	}
}

// CExpenderDlg dialog

IMPLEMENT_DYNAMIC(CExpenderDlg, CDialog)

CExpenderDlg::CExpenderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExpenderDlg::IDD, pParent)
{
	s_ExpenderDlg	= this;
	m_app = NULL;
}

CExpenderDlg::~CExpenderDlg()
{
	s_ExpenderDlg = NULL;
}

void CExpenderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_ComboType);
	DDX_Control(pDX, IDC_COMBO_LAYER, m_ComboLayer);
	DDX_Control(pDX, IDC_PROGRESS1, m_ExpanderProgress);
}


BEGIN_MESSAGE_MAP(CExpenderDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CExpenderDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_PATH, &CExpenderDlg::OnBnClickedButtonPath)
	ON_CBN_SELCHANGE(IDC_COMBO_TYPE, &CExpenderDlg::OnCbnSelchangeComboType)
	ON_BN_CLICKED(IDC_CHECK_ALL_IMAGE, &CExpenderDlg::OnBnClickedCheckAllImage)
	ON_CBN_SELCHANGE(IDC_COMBO_LAYER, &CExpenderDlg::OnCbnSelchangeComboLayer)
	ON_BN_CLICKED(IDC_CHECK_ALL_LAYER, &CExpenderDlg::OnBnClickedCheckAllLayer)
	ON_EN_CHANGE(IDC_EDIT_FILENAME, &CExpenderDlg::OnEnChangeEditFilename)
	ON_EN_CHANGE(IDC_EDIT_X, &CExpenderDlg::OnEnChangeEditX)
	ON_EN_CHANGE(IDC_EDIT_Y, &CExpenderDlg::OnEnChangeEditY)
	ON_EN_CHANGE(IDC_EDIT_W, &CExpenderDlg::OnEnChangeEditW)
	ON_EN_CHANGE(IDC_EDIT_H, &CExpenderDlg::OnEnChangeEditH)
END_MESSAGE_MAP()


BOOL CExpenderDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_bInit = false;
	m_bOverallImageExport	= false;
	m_bChangedFileNameByUser= false;
	m_bUpdateByFileNameEdit = false;

	SetDlgItemInt(IDC_EDIT_X, m_crop_pos_x);
	SetDlgItemInt(IDC_EDIT_Y, m_crop_pos_y);
	SetDlgItemInt(IDC_EDIT_W, m_crop_width);
	SetDlgItemInt(IDC_EDIT_H, m_crop_height);
	m_bInit = true;

	m_ComboType.AddString(_T("bmp"));
	m_ComboType.AddString(_T("tiff"));
	m_ComboType.SetCurSel(0);

	TCHAR strTemp[120];

	for (int i = 0; i < m_nTotalLayer; i++){
		if (m_nTotalLayer < 10){
			_stprintf(strTemp, _T("Layer %d"), i + 1);
		}
		else if (m_nTotalLayer < 100){
			_stprintf(strTemp, _T("Layer %02d"), i + 1);
		}
		else{
			_stprintf(strTemp, _T("Layer %03d"), i + 1);
		}
		
		m_ComboLayer.AddString(strTemp);
	}
	m_ComboLayer.SetCurSel(m_nSelectLayer);

	m_ExpanderProgress.SetRange(0, 100);
		
	UpdateFileName();
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void	CExpenderDlg::UpdateFileName(BOOL bForceInsertLayerInfo)
{
	//Update file name
	//m_file_name
	if (m_nSelectLayer == m_nTotalLayer){
		m_file_name.Format(L"%s%s", m_path_name, L".svg");
	}
	else{
		if (!m_bChangedFileNameByUser){
			if (m_bOverallImageExport){
				m_file_name.Format(L"%s_(%d_%d)_(%d_%d)_%d_crop%s", m_path_name, 0, 0, m_image_width, m_image_height,
					m_nSelectLayer + 1, (m_ComboType.GetCurSel() == 0) ? L".bmp" : L".tiff");
			}
			else{
				m_file_name.Format(L"%s_(%d_%d)_(%d_%d)_%d_crop%s", m_path_name,
					(int)(m_pt1.x * m_image_width), (int)(m_pt1.y * m_image_height), (int)(m_pt2.x * m_image_width), (int)(m_pt2.y * m_image_height),
					m_nSelectLayer + 1, (m_ComboType.GetCurSel() == 0) ? L".bmp" : L".tiff");
			}
		}
		else{
			if (m_ComboType.GetCurSel() == 0){
				if (m_file_name.Right(5) == ".tiff"){
					m_file_name.TrimRight(L".tiff");
					m_file_name.Insert(m_file_name.GetLength(), L".bmp");
				}
			}
			else{
				if (m_file_name.Right(4) == ".bmp"){
					m_file_name.TrimRight(L".bmp");
					m_file_name.Insert(m_file_name.GetLength(), L".tiff");
				}
			}

			if (bForceInsertLayerInfo){
				m_file_name = m_org_file_name;
				TCHAR layer_info_with_ext[16];
				if (m_file_name.Right(5) == ".tiff"){
					m_file_name.TrimRight(L".tiff");
					_stprintf(layer_info_with_ext, _T("_%d.tiff"), m_nSelectLayer + 1);
					m_file_name.Insert(m_file_name.GetLength(), layer_info_with_ext);
				}
				else if (m_file_name.Right(4) == ".bmp"){
					m_file_name.TrimRight(L".bmp");
					_stprintf(layer_info_with_ext, _T("_%d.bmp"), m_nSelectLayer + 1);
					m_file_name.Insert(m_file_name.GetLength(), layer_info_with_ext);
				}
			}
		}
	}
	
	m_bUpdateByFileNameEdit = FALSE;
	SetDlgItemText(IDC_EDIT_FILENAME, m_file_name);
	m_bUpdateByFileNameEdit = TRUE;
}


static TCHAR* GetThisPath(TCHAR* dest, DWORD destSize)
{
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);
	return dest;
}


// CExpenderDlg message handlers
void	CExpenderDlg::SetCropImageInfo(CKdu_showApp *app, const TCHAR* path_name, int image_width, int image_height, POINTF& pt1, POINTF& pt2, int nTotalLyaer, int nSelectLayer)
{
	m_app = app;

	if (_tcsstr(path_name, _T("jpip"))){
		TCHAR strTemp[1024];
		size_t nStrLen = _tcslen(path_name);
		int i;
		for (i = 0; i < nStrLen; i++){
			if (path_name[i + 7] == '.' || path_name[i + 7] == '/' || path_name[i + 7] == ':'){
				strTemp[i] = '_';
			}
			else{
				strTemp[i] = path_name[i + 7];
			}
		}

		strTemp[i] = NULL;
		TCHAR strPath[1024];

		GetThisPath(strPath, MAX_PATH);
		_tcscat(strPath, _T("\\"));
		_tcscat(strPath, strTemp);

		m_path_name = strPath;
	}
	else{
		m_path_name = path_name;
	}

		
	m_crop_pos_x = pt1.x * image_width;
	m_crop_pos_y = pt1.y * image_height;
	m_crop_width = abs((pt2.x - pt1.x) * image_width);
	m_crop_height = abs((pt2.y - pt1.y) * image_height);

	m_image_width = image_width;
	m_image_height = image_height;

	m_nTotalLayer = nTotalLyaer+1;
	m_nSelectLayer = nSelectLayer;

	m_pt1 = pt1;
	m_pt2 = pt2;

	TRACE("SetCrop Region [%f:%f] [%f:%f]\n", m_pt1.x, m_pt1.y, m_pt2.x, m_pt2.y);
	m_bInit = true;
}


void CExpenderDlg::SetProgress(int percent, int decode_size)
{
	m_ExpanderProgress.SetPos(percent);
	TCHAR strTemp[32];

	if (IsDlgButtonChecked(IDC_CHECK_ALL_LAYER)){
		_stprintf(strTemp, _T("%d %% (%d/%d)"), percent, m_nSelectLayer + 1, m_nTotalLayer);
	}
	else{
		_stprintf(strTemp, _T("%d %% (%d layer)"), percent, m_nSelectLayer + 1);
	}
	SetDlgItemText(IDC_STATIC_PROGRESS, strTemp);

	//process windows message 
	if (decode_size != 0){
		kd_bitmap_file_buf::getInstance()->FlashBuf(decode_size);
	}
}


void CExpenderDlg::OnBnClickedButtonSave()
{
	if (m_app == NULL){
		AfxMessageBox(_T("Fail Crop Image Saved (app not assigned before)"));
		return;
	}

	if (m_bOverallImageExport){
		m_pt1.x = 0;
		m_pt1.y = 0;

		m_pt2.x = 1;
		m_pt2.y = 1;
	}

	if (IsDlgButtonChecked(IDC_CHECK_ALL_LAYER)){
		int i;
		m_org_file_name = m_file_name;
		m_app->SetExportMode(TRUE);

		for ( i = 0; i < m_nTotalLayer; i++){
			m_nSelectLayer = i;
			UpdateFileName(TRUE);
			SetProgress(0, 0);
			if (!m_app->UpdateCropImage(m_pt1, m_pt2, m_nSelectLayer, m_file_name)){
				AfxMessageBox(_T("Fail Crop Image Saved"));
				break;
			}
		}
		
		if (i == m_nTotalLayer) MessageBox(_T("Export Completed."), _T("VP Viewer"));
		m_app->SetExportMode(FALSE);
	}
	else{
		/* testing point Demo_1502_90m_plxn.jpx file point, filemapper poision will be crash when erease buffer
		m_pt1.x = 0.697154462;
		m_pt1.y = 0.148543686;

		m_pt2.x = 0.995934963;
		m_pt2.y = 0.520388365;
		*/
		m_app->SetExportMode(TRUE);
		if (m_app->UpdateCropImage(m_pt1, m_pt2, m_nSelectLayer, m_file_name)){
			MessageBox(_T("Export Completed."), _T("VP Viewer"));
		}
		else{
			AfxMessageBox(_T("Fail Crop Image Saved"));
		}
		m_app->SetExportMode(FALSE);
	}

	OnOK();
}


void CExpenderDlg::OnBnClickedButtonPath()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString m_strPath;
	CStdioFile file;
	// CFile file;
	CFileException ex;
		
	//int row_gap;
	if (m_ComboType.GetCurSel() == 0){

		CFileDialog dlg(FALSE, _T("*.bmp"), m_file_name, OFN_OVERWRITEPROMPT, _T("Bitmap Images(*.bmp)|*.bmp|"), NULL);
		if (dlg.DoModal() == IDOK)
		{
			m_file_name = dlg.GetPathName();
			m_bChangedFileNameByUser = true;
		}
	}
	else
	{
		CFileDialog dlg(FALSE, _T("*.tiff"), m_file_name, OFN_OVERWRITEPROMPT, _T("TIFF Images(*.tiff)|*.tiff|"), NULL);
		if (dlg.DoModal() == IDOK)
		{
			m_file_name = dlg.GetPathName();
			m_bChangedFileNameByUser = true;
		}
	}
	UpdateFileName();
}


void CExpenderDlg::OnCbnSelchangeComboType()
{
	UpdateFileName();
}


void CExpenderDlg::OnCbnSelchangeComboLayer()
{
	m_nSelectLayer = m_ComboLayer.GetCurSel();
	UpdateFileName();
}


void CExpenderDlg::OnBnClickedCheckAllImage()
{
	m_bOverallImageExport = IsDlgButtonChecked(IDC_CHECK_ALL);

	GetDlgItem(IDC_EDIT_X)->EnableWindow(!m_bOverallImageExport);
	GetDlgItem(IDC_EDIT_Y)->EnableWindow(!m_bOverallImageExport);
	GetDlgItem(IDC_EDIT_H)->EnableWindow(!m_bOverallImageExport);
	GetDlgItem(IDC_EDIT_W)->EnableWindow(!m_bOverallImageExport);
	UpdateFileName();
}


void CExpenderDlg::OnBnClickedCheckAllLayer()
{
	GetDlgItem(IDC_COMBO_LAYER)->EnableWindow(!IsDlgButtonChecked(IDC_CHECK_ALL_LAYER));
}


void CExpenderDlg::OnEnChangeEditFilename()
{
	GetDlgItemText(IDC_EDIT_FILENAME, m_file_name);
	if (m_bUpdateByFileNameEdit)	m_bChangedFileNameByUser = TRUE;
}


void CExpenderDlg::OnEnChangeEditX()
{
	UINT x = GetDlgItemInt(IDC_EDIT_X);
	m_crop_pos_x = x;

	m_pt1.x = ((float)m_crop_pos_x)/m_image_width;
	m_pt2.x = ((float)m_crop_width) / m_image_width + m_pt1.x;
	UpdateFileName();

	UpdateSelectRegion();
}


void CExpenderDlg::OnEnChangeEditY()
{
	UINT y = GetDlgItemInt(IDC_EDIT_Y);
	m_crop_pos_y = y;

	m_pt1.y = ((float)m_crop_pos_y) / m_image_height;
	m_pt2.y = ((float)m_crop_height) / m_image_height + m_pt1.y;
	UpdateFileName();

	UpdateSelectRegion();
}


void CExpenderDlg::OnEnChangeEditW()
{
	UINT w = GetDlgItemInt(IDC_EDIT_W);
	m_crop_width = w;

	m_pt2.x = ((float)m_crop_width) / m_image_width + m_pt1.x;
	UpdateFileName();

	UpdateSelectRegion();
}


void CExpenderDlg::OnEnChangeEditH()
{
	UINT h = GetDlgItemInt(IDC_EDIT_H);
	m_crop_height = h;

	m_pt2.y = ((float)m_crop_height) / m_image_height + m_pt1.y;
	UpdateFileName();

	UpdateSelectRegion();
}


void CExpenderDlg::UpdateSelectRegion()
{
	if (m_bInit)
		m_app->child_wnd->UpdateSelectRegion(m_pt1, m_pt2);
}
