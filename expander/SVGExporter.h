#pragma once
#include "SVGFileDialog.h"

class CDrawObjMgr;
bool SVGExport(const TCHAR *szFile, const TCHAR *szOrgFile, CDrawObjMgr *pDrawObjMgr, BOOL bExportMeasure, int ratio, int w, int h);
