// FileDialogEx.cpp : implementation file
//

#include "stdafx.h"
#include "SVGFileDialog.h"
#include "resource.h"
#include <dlgs.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSVGFileDialog

IMPLEMENT_DYNAMIC(CSVGFileDialog, CFileDialog)

CSVGFileDialog::CSVGFileDialog(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd)
{
	m_bVistaStyle = FALSE;
}


BEGIN_MESSAGE_MAP(CSVGFileDialog, CFileDialog)
	//{{AFX_MSG_MAP(CSVGFileDialog)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CSVGFileDialog::OnInitDialog() 
{
	CFileDialog::OnInitDialog();
		
	// This variable should be changed acording to your wishes
	const UINT iExtraSize = 30;
	// Get a pointer to the original dialog box.
	CWnd *wndDlg = GetParent();
	RECT Rect, rcStaticCtrl;

	wndDlg->GetWindowRect(&Rect);
	// Change the size of FileOpen dialog
	wndDlg->SetWindowPos(NULL, 0, 0, 
                        Rect.right - Rect.left, 
                        Rect.bottom - Rect.top + iExtraSize, 
                        SWP_NOMOVE);

	// Standard CFileDialog control ID's are defined in <dlgs.h>
	// Do not forget to include <dlgs.h> in implementation file
	// We also need Static Control. Get coordinates from stc3 control
	CWnd *wndStaticCtrl = wndDlg->GetDlgItem(stc2);
	CWnd *wndComboCtrl = wndDlg->GetDlgItem(cmb1);	// cmb1 - standard file name combo box control

	wndStaticCtrl->GetWindowRect(&rcStaticCtrl);
	wndDlg->ScreenToClient(&rcStaticCtrl);
	
	Rect = rcStaticCtrl;
	Rect.top += 30;
	Rect.bottom += 50;
	Rect.right = 80 + Rect.left;

	// our static control
	m_Static.Create(_T("Display Ratio : "), WS_CHILD | WS_VISIBLE, Rect, wndDlg, IDC_STATIC);
	m_Static.SetFont(wndComboCtrl->GetFont(), TRUE);
	
	// Add some general text for static box
	//sprintf(szText, "File SubType Format");
	//wndDlg->SetDlgItemText(IDC_MYSTATIC, szText);
	
	wndComboCtrl->GetWindowRect(&Rect);
	wndDlg->ScreenToClient(&Rect);
	Rect.top += 30;
	Rect.bottom += 80;
	Rect.right -= 40;
	m_Combo.Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP,
		Rect, wndDlg, IDC_SVGCOMBOBOX);
	m_Combo.SetFont(wndComboCtrl->GetFont(), TRUE);
	m_Combo.SetFocus();

	m_Combo.AddString(_T("100%"));
	m_Combo.AddString(_T("50%"));
	m_Combo.AddString(_T("25%"));
	m_Combo.AddString(_T("10%"));
	m_Combo.AddString(_T("5%"));
	m_Combo.AddString(_T("1%"));
	m_Combo.SetCurSel(0);
		
	Rect.top = rcStaticCtrl.top + 20;
	Rect.bottom = rcStaticCtrl.bottom + 40;
	Rect.left = Rect.right + 10;
	Rect.right = Rect.left + 120;
		
	m_CheckButton.Create(_T("Include Measured Values"), WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, Rect, wndDlg, IDC_SVGCHECK);
	m_CheckButton.SetFont(wndComboCtrl->GetFont(), TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSVGFileDialog::OnDestroy() 
{
	CFileDialog::OnDestroy();
	
	//Update control value
	m_bMeasureCheck = (m_CheckButton.GetCheck() == BST_CHECKED);
	m_RatioIndex = m_Combo.GetCurSel();


	InternalRelease(); 
}
