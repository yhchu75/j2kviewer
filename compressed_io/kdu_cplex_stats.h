/*****************************************************************************/
// File: kdu_cplex_stats.h [scope = APPS/COMPRESSED-IO]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/*****************************************************************************
Description:
    Defines classes derived from the core `kdu_cplex_bkgnd_stats', that can
be used to read and save complexity statistics to be used as background
information for the Cplex-EST algorithm.  For the moment, the simple
`kdu_cplex_bkgnd_store' class is implemented entirely inline, so no separate
source files are required at all.
******************************************************************************/

#ifndef CPLEX_STATS_H
#define CPLEX_STATS_H

#include "kdu_sample_processing.h"
#include <stdio.h>

// Classes defined here
namespace kdu_supp { 
  class kdu_cplex_bkgnd_store;
}

namespace kdu_supp { 
  using namespace kdu_core;


/* ========================================================================= */
/*                                 Classes                                   */
/* ========================================================================= */

/*****************************************************************************/
/*                          kdu_cplex_bkgnd_store                            */
/*****************************************************************************/

class kdu_cplex_bkgnd_store : public kdu_cplex_bkgnd { 
  /* [BIND: reference]
     [SYNOPSIS]
       This object adds file storage support to the core `kdu_cplex_bkgnd'
       class that is itself independent of any I/O system.  The present class
       provides a simple human readable file format for recording and importing
       subband complexity statistics to use as background information to
       improve the robustness of the forecasting mechanisms employed by the
       `kdu_cplex_analysis' object.
       [//]
       As explained with `kdu_cplex_bkgnd', all you need to do is to construct
       an instance of this object, pass a reference to it to
       `kdu_push_pull_params::set_cplex_bkgnd', and pass the
       `kdu_push_pull_params' object to the `kdu_multi_analysis::create'
       function (low level) or the `kdu_stripe_compressor::start' function
       (high level).
       [//]
       At the application level, the functions you might wish to invoke
       explicitly are: `load', `save' and `kdu_cplex_bkgnd::get_info'.
  */
  // --------------------------------------------------------------------------
  public: // Member functions
    kdu_cplex_bkgnd_store() { loaded = false; }
    bool load(const char *import_pathname, bool read_only)
      { 
        if (loaded) return false;
        if (read_only) this->set_read_only();
        char line[81];  line[80] = '\0';
        FILE *fp = fopen(import_pathname,"r");
        if (fp == NULL)
          return false;
        int num_comps=0;  kdu_uint32 hash;
        if ((fgets(line,80,fp) == NULL) ||
            (sscanf(line,"KDU-CPLEX-BKGND: C=%d; MCT-hash=0x%8x",
                    &num_comps,&hash) != 2) ||
            (num_comps < 1) || (num_comps > 16384))
          { fclose(fp); return false; }
        if (!configure(num_comps,hash))
          { fclose(fp); return false; }
        for (int c=0; c < num_comps; c++)
          { 
            kdu_cplex_bkgnd_comp *comp = this->access_component(c);
            if (comp == NULL) return false; // Should never happen
            int c_val=0, num_levels=0, g_bits=0, rev_prec=0;
            if ((fgets(line,80,fp) == NULL) ||
                (sscanf(line,"  c=%d: L=%d; G=%d; RevPrec=%d; DWT-hash=0x%8x",
                        &c_val,&num_levels,&g_bits,&rev_prec,&hash) != 5) ||
                (c_val != c) || (num_levels < 0) || (num_levels > 32) ||
                (g_bits < 0) || (g_bits > 7) || (rev_prec < 0) ||
                (rev_prec > 38))
              { fclose(fp); return false; }
            if (!comp->configure(num_levels,hash,g_bits,rev_prec))
              { fclose(fp); return false; }
            for (int d=0; d <= num_levels; d++)
              { 
                kdu_cplex_bkgnd_level *level = comp->access_level(d);
                if (level == NULL) return false;
                int d_val=0, num_bands=0; kdu_uint32 styl=0; kdu_coords depth;
                if ((fgets(line,80,fp) == NULL) ||
                    (sscanf(line,"    d=%d: B=%d; STYLE=0x%8x; DEPTH=(%d,%d)",
                            &d_val,&num_bands,&styl,&depth.x,&depth.y) != 5) ||
                    (d_val != d) || (num_bands < 1) || (num_bands > 49))
                  { fclose(fp); return false; }
                if (!level->configure(num_bands,styl,depth))
                  { fclose(fp); return false; }
                for (int b=0; b < num_bands; b++)
                  if (!load_band(level,b,fp))
                    {fclose(fp); return false; }
              }
          }
        fclose(fp);
        return (loaded=true);
      }
      /* [SYNOPSIS]
           You would only call this function once -- subsequent calls will
           return false, doing nothing.  However, if all you want to do is to
           accumulate complexity statistics, possibly saving them to a file,
           you don't need to call this function at all.
         [RETURNS]
           False if the function has been called before, or no file can be
           opened at `import_pathname', or if the file format is invalid.
         [ARG: import_pathname]
           Path to a file from which to load background statistics.  If
           the file does not exist or otherwise cannot be opened, the function
           returns false, but this is not of itself an error.
         [ARG: read_only]
           If true, the base `kdu_cplex_bkgnd::set_read_only' function is
           called before loading anything from the file at
           `import_pathname'.  You could also do this yourself explicitly,
           but making the option an argument to this function avoids the
           need to remember that the read-only state needs to be configured
           first, before configuring attributes of the `kdu_cplex_bkgnd'
           machinery based on the source file.
      */
    bool save(const char *export_pathname)
      { 
        FILE *fp=NULL;
        int num_comps; kdu_uint32 mct_hash=0;
        if (get_read_only() ||
            ((num_comps = get_num_components(mct_hash)) <= 0) ||
            ((fp = fopen(export_pathname,"w")) == NULL))
          return false;
        fprintf(fp,"KDU-CPLEX-BKGND: C=%d; MCT-hash=0x%08x\n",
                num_comps,mct_hash);
        for (int c=0; c < num_comps; c++)
          { 
            kdu_cplex_bkgnd_comp *comp = this->access_component(c);
            int num_levels=0, g_bits=0, rev_prec=0; kdu_uint32 dwt_hash=0;
            if (comp != NULL)
              num_levels = comp->get_num_levels(dwt_hash,g_bits,rev_prec);
            fprintf(fp," c=%d: L=%d; G=%d; RevPrec=%d; DWT-hash=0x%08x\n",
                    c,num_levels,g_bits,rev_prec,dwt_hash);
            for (int d=0; d <= num_levels; d++)
              { 
                kdu_cplex_bkgnd_level *level=NULL;
                int num_bands=0; kdu_uint32 style=0; kdu_coords depth;
                if ((comp != NULL) && ((level=comp->access_level(d)) != NULL))
                  num_bands = level->get_num_bands(style,depth);
                fprintf(fp," d=%d: B=%d; STYLE=0x%08x; DEPTH=(%d,%d)\n",
                        d,num_bands,style,depth.x,depth.y);
                for (int b=0; b < num_bands; b++)
                  save_band(level,b,fp);
              }
          }
        fclose(fp);
        return true;
      }
      /* [SYNOPSIS]
           You can optionally call this function once all processing has
           completed, to save any statistics collected while in operation.
           If the object was in the read-only mode (see `load'), then there
           is nothing to save, and so this function returns false immediately.
           Otherwise, it opens a file at `export_pathname' and attempts to
           write to it.
         [RETURNS]
           False if the a file at `export_pathname' cannot be opened, or if
           the object was in the read-only state, or
           `kdu_cplex_bkgnd::configure' has never been called -- i.e., no
           processing has happened at all.
      */
  // --------------------------------------------------------------------------
  private: // Helper functions
    bool load_band(kdu_cplex_bkgnd_level *lev, int b, FILE *fp)
      { 
        char line[81]; line[80] = '\0';
        float abs_stats[128], abs_samples[128];
        int b_val=0, eps_mu=0, num_entries=0;
        if ((fgets(line,80,fp) == NULL) ||
            (sscanf(line," b=%d: EpsMu=0x%4x; NUM=%d",
                    &b_val,&eps_mu,&num_entries) != 3) ||
            (b_val != b) || (eps_mu < 0) || (eps_mu >= (1<<16)) ||
            (num_entries < 0) || (num_entries > 128))
          return false;
        if (num_entries == 0) return true;
        for (int n=0; n < num_entries; n++)
          { 
            float val, cnt;
            if ((fgets(line,80,fp) == NULL) ||
                (sscanf(line," (%f,%f)",&val,&cnt) != 2) ||
                (val < 0.0f) || (cnt < 0.0f))
            return false;
            abs_stats[n] = val*cnt;  abs_samples[n] = cnt;
          }
        return lev->store_bkgnd_stats(b,(kdu_uint16)eps_mu,num_entries,
                                      abs_stats,abs_samples);
      }
    void save_band(kdu_cplex_bkgnd_level *lev, int b, FILE *fp)
      { 
        float abs_stats[KDU_CPLEX_REC_MAX_ENTRIES];
        float abs_samples[KDU_CPLEX_REC_MAX_ENTRIES];
        kdu_uint16 eps_mu=0;
        int num_entries=lev->get_acc_stats(b,eps_mu,KDU_CPLEX_REC_MAX_ENTRIES,
                                           abs_stats,abs_samples);
        fprintf(fp,"      b=%d: EpsMu=0x%04x; NUM=%d\n",b,(int)eps_mu,num_entries);
        for (int n=0; n < num_entries; n++)
          { 
            float val=abs_stats[n], cnt=abs_samples[n];  assert(cnt > 0.0f);
            fprintf(fp,"        (%f,%f)\n",val/cnt,cnt);
          }
      }
  // --------------------------------------------------------------------------
  private: // Data
    bool loaded;
  };

} // namespace kdu_supp

#endif // JPB_H
