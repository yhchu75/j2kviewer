/*****************************************************************************/
// File: core_local.h [scope = APPS/SUPPORT]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Common local definitions employed by the classes defined in "apps/support".
These definitions should not be included directly by an application.
******************************************************************************/

#ifndef SUPP_LOCAL_H
#define SUPP_LOCAL_H

#include "kdu_compressed.h"

/******************************************************************************
Notes on Memory Management
   This file defines `kd_suppmem', the analog of `kd_coremem' which is private
to the core codestream management machinery.  As `kd_coremem' manages most
memory allocation/deallocation processes in the Kakadu core system,
`kd_suppmem' manages expicitly allocated memory within the various classes
and structs that are defined within the "apps/support" directory.
   Both `kd_suppmem' and `kd_coremem' are private to the Kakadu toolkit, not
meant to be shared directly with applications, but instances of both classes
can be attached to a common `kdu_membroker' hierarchy, which is public and
derivable.
   The design patterns for classes/structs that are allocated via `kd_suppmem'
instances are the same as those for the core classes/structs that are
allocated via `kd_coremem', as outlined at the start of "compressed_local.h".
In particular, we use placement new/delete overrides for single instantiation,
while objects that are instantiated in arrays offer `create_n' and `destroy_n'
member functions.  Objects that allocate memory themselves keep a
`kd_suppmem' member as a reference, and this is retrieved by a `destroy'
function that directly instantiates the destructor and releases the memory,
rather than overriding the non-placement delete operator.  Objects that
do not need to allocate memory themselves have the `kd_suppmem' reference
passed to their `destroy' function.  In this way, a global plain text
search of the source files should reveal that there are almost no cases
in which operator delete is invoked or in which a naked (non-placement) new
operator is used.
   Managing all memory through a well defined interface has a number of
advantages, but the most important one is that it allows the application to
strictly limit the amount of memory that can be allocated for certain tasks.
This can prevent a larger system from being halted by a malicious file or
an ill-conceived employment of the enormous flexibility offered by the
JPEG2000 standards.
******************************************************************************/


// The following classes and structures are defined here:
namespace kd_supp_local { 
  class kd_suppmem;
} // namespace kd_supp_local


namespace kd_supp_local { 
  using namespace kdu_core;

/* ========================================================================= */
/*                                 Classes                                   */
/* ========================================================================= */

/*****************************************************************************/
/*                                kd_suppmem                                 */
/*****************************************************************************/

class kd_suppmem { 
  /* Notes:
       This is a private allocator, instances of which are responsible for
       most memory allocation within Kakadu's supplementary sub-systems.
       [//]
       The `kd_suppmem' class is not thread safe, so each instance needs to
       be bound to a specific thread or operate within a suitable critical
       section.  However, `kd_suppmem' objects obtain any required allocation
       permissions from the thread-safe `kdu_membroker' interface, allowing
       the memory associated with multiple `kd_suppmem' objects to be pooled
       in a way that supports concurrency.
  */
  public: // Management functions
    kd_suppmem(const char *phrase)
      { /* The `phrase' string is a constant resource that is inserted into
           error/warning messages in a sentence of the form:
              "... memory allocator used to manage <phrase> resources ..."
           Recommended examples are:
           1. "stripe-compressor";  2. "stripe-decompressor";
           3. "region-decompressor"; or 4. "region-compositor".
         */
        broker = NULL; limit_bytes = (kdu_uint64)KDU_INT64_MAX;
        alloc_bytes = 0;  free_failures = 0;  this->msg_phrase = phrase;
      }
    ~kd_suppmem();
    kdu_int64 attach_to_broker(kdu_membroker *new_broker);
      /* Analogous to `kdu_membroker::attach_to_parent'.  Returns 0 if
         successful, or else the number of bytes of already allocated
         content for which permission could not be granted by `new_broker';
         in this event, no attachment actually takes place and any memory
         allocation permission granted to us by `new_broker' is returned.
         If `new_broker' is NULL, this function is equivalent to
         `detach_from_broker'. */
    bool detach_from_broker();
      /* Similar to `kdu_membroker::detach_from_parent', except that once
         detached, the current object is left free from any allocation
         constraints, rather than being prevented from allocating any further
         memory.  Does nothing if not currently attached. */
    kdu_membroker *get_membroker() const { return broker; }
      /* Convenient member to get the attached memory broker, if any, so we
         do not have to store a redundant copy. */
  public: // Overflow-safe arithmetic for use in preparing for alloc calls
    size_t safe_add(size_t x, size_t y)
      { /* Use before allocating memory based on a sum that might overflow */
        size_t result = x+y;
        if (result < y) handle_failed_alloc(0);
        return result;
      }
    size_t safe_mul(size_t x, size_t y)
      { /* Use before allocating memory based on a product that might
           overflow.  If one multiplicand is a constant, try to make this y. */
        if (((x | y) > KDU_SAFE_ROOT_SIZE_MAX) && (y != 0) &&
            (x > (KDU_SIZE_MAX / y)))
          handle_failed_alloc(0);
        return x*y;
      }
    size_t safe_mulo(size_t x, size_t y, size_t off)
      { /* As above, but for a product, followed by an offset. */
        return safe_add(safe_mul(x,y),off);
      }
  public: // Inline alloc functions
    void *alloc(size_t elt_bytes, size_t alignment, size_t num_elts=1)
    { /* Currently supports `alignment' values up to 8 and refuses to
         allocate more 2GB in a single request -- the latter provides
         security against the risk of overflow during index calculations
         that might be performed using signed 32-bit arithmetic.  The
         constraint can be removed if we are sure that all such calculations
         are performed using `kdu_idx_t' rather than `int' or `kdu_int32',
         but there is no real harm in preventing ridiculously large
         allocations here, since `kd_suppmem' is used only for allocating
         supplementary blocks of memory that do not scale with the area
         of an image -- supplementary objects such as `kdu_region_compositor',
         which allocate imagery buffers, do not allocate them via
         `kd_suppmem'. */
        if (num_elts != 1)
          { // Allocating an array -- need special overflow check
            if (num_elts > (KDU_SIZE_MAX/elt_bytes)) // RHS usually a constant
              handle_failed_alloc(0);
            elt_bytes *= num_elts;
          }
        kdu_uint64 old_alloc_bytes = this->alloc_bytes;
        if (alignment > 8) abort();
        void *result=NULL;
        if ((alignment > 4) || (elt_bytes >= (size_t)0xFFFFFFFF))
          { // Record size in 8-byte prefix
            kdu_uint64 new_bytes = ((kdu_uint64) elt_bytes) + 8;
            if ((new_bytes < 8) || (new_bytes > (kdu_uint64)KDU_INT64_MAX))
              handle_failed_alloc(0);
            else if (((alloc_bytes+=new_bytes) > limit_bytes) ||
                     (alloc_bytes < old_alloc_bytes))
              handle_overlimit_alloc(new_bytes);
            if ((new_bytes > (size_t)KDU_INT32_MAX) || // 2GB limit; see above
                ((result = malloc((size_t)new_bytes)) == NULL))
              handle_failed_alloc(new_bytes);
            assert(!(_addr_to_kdu_int32(result) & 7));
            result = ((kdu_byte *)result) + 8;
            ((size_t *)result)[-1] = elt_bytes;
          }
        else if ((alignment == 1) && (elt_bytes < 256))
          { // Record size in 1-byte prefix
            kdu_uint64 new_bytes = ((kdu_uint64) elt_bytes) + 1;
            if ((new_bytes < 1) || (new_bytes > (kdu_uint64)KDU_INT64_MAX))
              handle_failed_alloc(0);
            else if (((alloc_bytes+=new_bytes) > limit_bytes) ||
                     (alloc_bytes < old_alloc_bytes))
              handle_overlimit_alloc(new_bytes);
            if ((new_bytes > (size_t)KDU_INT32_MAX) || // 2GB limit; see above
                ((result = malloc((size_t)new_bytes)) == NULL))
              handle_failed_alloc(new_bytes);
            assert(!(_addr_to_kdu_int32(result) & 7));
            result = ((kdu_byte *)result) + 1;
            ((kdu_byte *)result)[-1] = (kdu_byte)elt_bytes;
          }
        else
          { // Record size in 4-byte prefix
            kdu_uint64 new_bytes = ((kdu_int64) elt_bytes) + 4;
            if ((new_bytes < 4) || (new_bytes > (kdu_uint64)KDU_INT64_MAX))
              handle_failed_alloc(0);
            else if (((alloc_bytes+=new_bytes) > limit_bytes) ||
                     (alloc_bytes < old_alloc_bytes))
              handle_overlimit_alloc(new_bytes);
            if ((new_bytes > (size_t)KDU_INT32_MAX) || // 2GB limit; see above
                ((result = malloc((size_t)new_bytes)) == NULL))
              handle_failed_alloc(new_bytes);
            assert(!(_addr_to_kdu_int32(result) & 7));
            result = ((kdu_byte *)result) + 4;
            ((kdu_uint32 *)result)[-1] = (kdu_uint32)elt_bytes;
          }
        return result;
      }
    kdu_byte *alloc_uint8(size_t num_bytes)
      { return (kdu_byte *)alloc(num_bytes,1); }
    bool *alloc_bool(size_t num_bools)
      { 
        if (num_bools > (KDU_SIZE_MAX/sizeof(bool)))
          handle_failed_alloc(0);
        return (bool *)alloc(num_bools*sizeof(bool),sizeof(bool));
      }
    kdu_int16 *alloc_int16(size_t num_shorts)
      { 
        if (num_shorts > (KDU_SIZE_MAX/sizeof(kdu_int16)))
          handle_failed_alloc(0);
        return (kdu_int16 *)alloc(num_shorts*sizeof(kdu_int16),
                                  sizeof(kdu_int16));
      }
    kdu_uint16 *alloc_uint16(size_t num_shorts)
      { 
        if (num_shorts > (KDU_SIZE_MAX/sizeof(kdu_uint16)))
          handle_failed_alloc(0);
        return (kdu_uint16 *)alloc(num_shorts*sizeof(kdu_uint16),
                                   sizeof(kdu_uint16));
      }
    kdu_int32 *alloc_int32(size_t num_ints)
      { 
        if (num_ints > (KDU_SIZE_MAX/sizeof(kdu_int32)))
          handle_failed_alloc(0);
        return (kdu_int32 *)alloc(num_ints*sizeof(kdu_int32),
                                  sizeof(kdu_int32));
      }
    kdu_uint32 *alloc_uint32(size_t num_ints)
      { 
        if (num_ints > (KDU_SIZE_MAX/sizeof(kdu_uint32)))
          handle_failed_alloc(0);
        return (kdu_uint32 *)alloc(num_ints*sizeof(kdu_uint32),
                                  sizeof(kdu_uint32));
      }
    float *alloc_float(size_t num_floats)
      { 
        if (num_floats > (KDU_SIZE_MAX/sizeof(float)))
          handle_failed_alloc(0);
        return (float *)alloc(num_floats*sizeof(float),sizeof(float));
      }
    double *alloc_double(size_t num_doubles)
      { 
        if (num_doubles > (KDU_SIZE_MAX/sizeof(double)))
          handle_failed_alloc(0);
        return (double *)alloc(num_doubles*sizeof(double),sizeof(double));
      }
    kdu_long *alloc_long(size_t num_longs)
      { 
        if (num_longs > (KDU_SIZE_MAX/sizeof(kdu_long)))
          handle_failed_alloc(0);
        return (kdu_long *)alloc(num_longs*sizeof(kdu_long),sizeof(kdu_long));
      }
    char *alloc_string(size_t length, const char *copy_src=NULL)
      { /* NB: `length' is the length of the string, so one extra character is
         allocated.  For convenience, if `copy_src' is non-NULL, we also
         copy up to a maximum of `length' characters to the allocated string
         before returning.  The returned character array is always
         null-terminated. */
        if (length >= (KDU_SIZE_MAX/sizeof(char)))
          handle_failed_alloc(0);
        char *buf = (char *)alloc((length+1)*sizeof(char),sizeof(char));
        buf[0] = '\0';
        if (copy_src != NULL)
          { strncpy(buf,copy_src,length); buf[length]='\0'; }
        return buf;
      }
    void **calloc_ptrs(size_t num_ptrs)
      { /* This function initializes all pointers to NULL. */
        if (num_ptrs > (KDU_SIZE_MAX/sizeof(void *)))
          handle_failed_alloc(0);
        void **buf = (void **)alloc(num_ptrs*sizeof(void *),sizeof(void *));
        memset(buf,0,num_ptrs*sizeof(void *));
        return buf;
      }
    void *calloc_structs(size_t elt_size, size_t num_elts)
      { /* This function allocates and clears an array of structures (objects
           that do not have constructors that do anything other than clear
           all members) and do not have destructors.  Alignment is always 8
           bytes. */
        void *buf = alloc(elt_size,8,num_elts);
        memset(buf,0,elt_size*num_elts);
        return buf;
      }
  public: // Functions to free or query memory blocks
    void free(void *ptr)
      { /* Use this function or `free_inactive' to free all memory that was
           allocated via one of the above functions. */
        kdu_int32 align_probe = _addr_to_kdu_int32(ptr) & 7;
        kdu_uint64 num_bytes=0, hdr_bytes=0;
        if (align_probe == 1)
          { // Size is recorded in 1 byte
            hdr_bytes = 1;
            num_bytes = (kdu_int64) ((kdu_byte *)ptr)[-1];
            ::free(((kdu_byte *)ptr) - 1);
          }
        else if (align_probe == 4)
          { // Size is recorded in 4 bytes
            hdr_bytes = 4;
            num_bytes = (kdu_int64) ((kdu_uint32 *)ptr)[-1];
            ::free(((kdu_byte *)ptr) - 4);
          }
        else if (align_probe == 0)
          { // Size is recorded in 8 bytes
            hdr_bytes = 8;
            num_bytes = (kdu_int64) ((size_t *)ptr)[-1];
            ::free(((kdu_byte *)ptr) - 8);
          }
        else
          handle_failed_free(ptr);
        num_bytes += hdr_bytes;
        if ((num_bytes < hdr_bytes) || (alloc_bytes < num_bytes))
          handle_failed_free(ptr);
        alloc_bytes -= num_bytes;
      }
    size_t get_num_elts(void *ptr, size_t elt_bytes)
      { 
        kdu_int32 align_probe = _addr_to_kdu_int32(ptr) & 7;
        size_t num_bytes = 0;
        if (align_probe == 1)
          num_bytes = (size_t) ((kdu_byte *)ptr)[-1];
        else if (align_probe == 4)
          num_bytes = (size_t) ((kdu_uint32 *)ptr)[-1];
        else if (align_probe == 0)
          num_bytes = (size_t) ((size_t *)ptr)[-1];
        size_t num_elts = num_bytes / elt_bytes;
        if (num_bytes != (num_elts * elt_bytes))
          handle_failed_free(ptr);
        return num_elts;
      }
  private: // Helper functions
    void handle_overlimit_alloc(kdu_uint64 new_bytes);
      /* Called if the `alloc_bytes' member has been incremented to a value
         exceeding `max_allocated_bytes'.  If the problem cannot be corrected
         by a successful call to `responder->overlimit', the function generates
         an error, never returning, but first removes `new_bytes' from the
         tally found in `alloc_bytes'. */
    void handle_failed_alloc(kdu_uint64 new_bytes);
      /* Similar to `handle_overlimit_request', but called in the event that
         a memory allocation request fails; in this case, the function always
         removes `new_bytes' from `alloc_bytes' before returning.  If the
         function is called with `new_bytes'=0, the allocation failed
         immediately due to invalid arguments -- e.g., overflow in numerical
         calculation of the number of bytes required. */
    void handle_failed_free(void *ptr);
      /* Called if it appears that more memory is being released than was
         actually allocated, or if there is an alignment problem with the
         memory block being released.  This function never returns. */
  private: // Data
    kdu_membroker *broker;
    kdu_uint64 limit_bytes; // Max bytes allowed; won't exceed KDU_INT64_MAX
    kdu_uint64 alloc_bytes; // Total bytes we have allocated already
    kdu_int64 free_failures; // Avoids excessive warning/error messages
    const char *msg_phrase;
  };

} // namespace kd_supp_local

#endif // SUPP_LOCAL_H
