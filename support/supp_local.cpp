/*****************************************************************************/
// File: core_local.cpp [scope = APPS/SUPPORT]
// Version: Kakadu, V8.0.2
// Author: David Taubman
// Last Revised: 13 January, 2020
/*****************************************************************************/
// Copyright 2001, David Taubman.  The copyright to this file is owned by
// Kakadu R&D Pty Ltd and is licensed through Kakadu Software Pty Ltd.
// Neither this copyright statement, nor the licensing details below
// may be removed from this file or dissociated from its contents.
/*****************************************************************************/
// Licensee: Innova Plex
// License number: 01642
// The licensee has been granted a (non-HT) COMMERCIAL license to the contents
// of this source file.  A brief summary of this license appears below.  This
// summary is not to be relied upon in preference to the full text of the
// license agreement, accepted at purchase of the license.
// 1. The Licensee has the right to Deploy Applications built using the Kakadu
//    software to whomsoever the Licensee chooses, whether for commercial
//    return or otherwise, subject to the restriction that Kakadu's HT
//    block encoder/decoder implementation remains disabled.
// 2. The Licensee has the right to Development Use of the Kakadu software,
//    including use by employees of the Licensee, for the purpose of
//    Developing Applications on behalf of the Licensee or in the performance
//    of services for Third Parties who engage Licensee for such services,
//    subject to the restriction that Kakadu's HT block encoder/decoder
//    implementation remains disabled.
// 3. The Licensee has the right to distribute Reusable Code (including
//    source code and dynamically or statically linked libraries) to a Third
//    Party who possesses a suitable license to use the Kakadu software, or to
//    a subcontractor who is participating in the development of Applications
//    by the Licensee (not for the subcontractor's independent use), subject
//    to the restriction that Kakadu's HT block encoder/decoder implementation
//    remains disabled.
// 4. The Licensee has the right to enable Kakadu's HT block encoder/decoder
//    implementation for evaluation and internal development purposes, but
//    not for deployed Applications.
/******************************************************************************
Description:
   Implements common local objects for classes defined in "apps/support".
******************************************************************************/

#include "supp_local.h"
using namespace kd_supp_local;

/* Note Carefully:
      If you want to be able to use the "kdu_text_extractor" tool to
   extract text from calls to `kdu_error' and `kdu_warning' so that it
   can be separately registered (possibly in a variety of different
   languages), you should carefully preserve the form of the definitions
   below, starting from #ifdef KDU_CUSTOM_TEXT and extending to the
   definitions of KDU_WARNING_DEV and KDU_ERROR_DEV.  All of these
   definitions are expected by the current, reasonably inflexible
   implementation of "kdu_text_extractor".
      The only things you should change when these definitions are ported to
   different source files are the strings found inside the `kdu_error'
   and `kdu_warning' constructors.  These strings may be arbitrarily
   defined, as far as "kdu_text_extractor" is concerned, except that they
   must not occupy more than one line of text.
*/
#ifdef KDU_CUSTOM_TEXT
#  define KDU_ERROR(_name,_id) \
     kdu_error _name("E(supp_local.cpp)",_id);
#  define KDU_WARNING(_name,_id) \
     kdu_warning _name("W(supp_local.cpp)",_id);
#  define KDU_TXT(_string) "<#>" // Special replacement pattern
#else // !KDU_CUSTOM_TEXT
#  define KDU_ERROR(_name,_id) \
     kdu_error _name("Error in Kakadu Support:\n");
#  define KDU_WARNING(_name,_id) \
     kdu_warning _name("Warning in Kakadu Support:\n");
#  define KDU_TXT(_string) _string
#endif // !KDU_CUSTOM_TEXT

#define KDU_ERROR_DEV(_name,_id) KDU_ERROR(_name,_id)
 // Use the above version for errors which are of interest only to developers
#define KDU_WARNING_DEV(_name,_id) KDU_WARNING(_name,_id)
 // Use the above version for warnings which are of interest only to developers


/* ========================================================================= */
/*                                kd_suppmem                                 */
/* ========================================================================= */

/*****************************************************************************/
/*                         kd_suppmem::~kd_suppmem                           */
/*****************************************************************************/

kd_suppmem::~kd_suppmem()
{
  if (free_failures > 1)
    { KDU_WARNING_DEV(w,0x28061700); w <<
      KDU_TXT("Multiple memory deallocation failures detected when freeing "
              "memory allocated for")
      << " " << msg_phrase << " " <<
      KDU_TXT("support resources.  Beyond the first (explicitly reported) "
              "failure,")
      << " " << free_failures-1 << " " <<
      KDU_TXT("additional failures were detected.");
    }
  else if ((free_failures == 0) && (alloc_bytes != 0))
    { KDU_WARNING_DEV(w,0x28061701); w <<
      KDU_TXT("Destroying the memory allocator used to manage")
      << " " << msg_phrase << " " <<
      KDU_TXT("support resources without first releasing all the relevant "
              "allocated memory.");
    }
  if (broker != NULL)
    detach_from_broker();
}

/*****************************************************************************/
/*                       kd_suppmem::attach_to_broker                        */
/*****************************************************************************/

kdu_int64 kd_suppmem::attach_to_broker(kdu_membroker *new_broker)
{
  if (new_broker == this->broker)
    return 0;
  detach_from_broker();
  if (new_broker == NULL)
    return 0;
  this->limit_bytes = this->alloc_bytes;
  kdu_int64 limit=(kdu_int64)limit_bytes; // Should always be <= KDU_INT64_MAX
  kdu_int64 granted = 0;
  if ((limit_bytes > 0) &&
      ((granted = new_broker->request(limit,limit,-1)) == 0) &&
      ((granted = new_broker->request(1,limit,-1)) < limit))
    { // Attachment failed
      new_broker->release(granted);
      return limit - granted;
    }
  else
    { 
      assert(granted == limit);
      this->broker = new_broker;
      return 0;
    }
}

/*****************************************************************************/
/*                      kd_suppmem::detach_from_broker                       */
/*****************************************************************************/

bool kd_suppmem::detach_from_broker()
{
  if (broker == NULL)
    return false;
  kdu_int64 limit=(kdu_int64)limit_bytes; // Should always be <= KDU_INT64_MAX
  if (limit > 0)
    broker->release(limit);
  broker = NULL;
  limit = (kdu_uint64)KDU_INT64_MAX;
  return true;
}

/*****************************************************************************/
/*                    kd_suppmem::handle_overlimit_alloc                     */
/*****************************************************************************/

void kd_suppmem::handle_overlimit_alloc(kdu_uint64 new_bytes)
{
  kdu_uint64 failed_alloc_bytes = alloc_bytes;
  alloc_bytes -= new_bytes;
  if (alloc_bytes > failed_alloc_bytes)
    handle_failed_alloc(0); // Numerical wrap-around problem
  if (broker != NULL)
    { 
      kdu_uint64 min_bytes = failed_alloc_bytes - limit_bytes;
      kdu_uint64 max_bytes = ((kdu_uint64)KDU_INT64_MAX) - limit_bytes;
      kdu_uint64 pref_bytes = min_bytes + (alloc_bytes >> 2);
      if (max_bytes > (kdu_uint64)KDU_INT64_MAX) // Unsigned overflow
        max_bytes = 0; // `limit_bytes' is already too big!
      if (pref_bytes < min_bytes)
        pref_bytes = max_bytes; // Numeric overflow
      if (min_bytes > max_bytes)
        min_bytes = max_bytes;
      if (pref_bytes > max_bytes)
        pref_bytes = max_bytes;
      if (min_bytes > 0)
        limit_bytes += (kdu_uint64)broker->request((kdu_int64)min_bytes,
                                                   (kdu_int64)pref_bytes,
                                                   (kdu_int64)limit_bytes);
      if (limit_bytes >= failed_alloc_bytes)
        { // We can continue
          alloc_bytes = failed_alloc_bytes;
          return;
        }
      broker->note_allocation_failure((kdu_int64)(failed_alloc_bytes -
                                                  limit_bytes),
                                      (kdu_int64)limit_bytes,false);
    }

  // If we get here, we must generate an error and exit
  KDU_ERROR(e,0x28061702); e <<
  KDU_TXT("Attempt to exceed application-imposed memory limit while "
          "allocating ")
  << " " << msg_phrase << " " <<
  KDU_TXT("support resources.") << "\n\t\t" <<
  "Requested bytes = " << (kdu_int64)new_bytes << "\n\t\t" <<
  "Available limit = " << (kdu_int64)limit_bytes << "\n\t\t" <<
  "Already allocated bytes = " << (kdu_int64)alloc_bytes;
}

/*****************************************************************************/
/*                      kd_suppmem::handle_failed_alloc                      */
/*****************************************************************************/

void kd_suppmem::handle_failed_alloc(kdu_uint64 new_bytes)
{
  if (new_bytes == 0)
    { KDU_ERROR(e,0x28061703); e <<
      KDU_TXT("Memory allocation failure detected while allocating")
      << " " << msg_phrase << " " <<
      KDU_TXT("support resources.  Immediate cause appears to be "
              "numerical overflow.  Ultimate cause might be invalid or "
              "corrupted file metadata or codestream coding parameters.");
    }
  else
    { 
      alloc_bytes -= new_bytes;
      if (broker != NULL)
        broker->note_allocation_failure(new_bytes,alloc_bytes,true);
      KDU_ERROR(e,0x28061704); e <<
      KDU_TXT("Memory allocation failure detected while allocating")
      << " " << msg_phrase << " " <<
      KDU_TXT("support resources.  Immediate cause is that the system has "
              "insufficient resources, or is not prepared to allocate a "
              "large contiguous block of memory.  The ultimate cause "
              "might be invalid or corrupted file metadata or codestream "
              "coding parameters.");
    }
}

/*****************************************************************************/
/*                      kd_suppmem::handle_failed_free                       */
/*****************************************************************************/

void kd_suppmem::handle_failed_free(void *ptr)
{
  free_failures++;
  if (free_failures != 1)
    return;
  KDU_WARNING_DEV(w,0x28061705); w <<
  KDU_TXT("Memory deallocation failure detected while freeing")
  << " " << msg_phrase << " " <<
  KDU_TXT("support resources.  The memory appears to have been allocated "
          "using a different mechanism.  This is an internal implementation "
          "error that will result in memory leaks.");
}
